import axios from 'axios';
import Auth from '../auth/index';
import {
  AS_USER_TOKEN,
  SCHEDULE_STAY,
  UPLOAD_IMAGE,
  U_ADD_PASSPORT,
  U_ADD_PAYMENT_CARD,
  U_ADD_TRAVELLER,
  U_BASE,
  U_BOOK_PACKAGE,
  U_DELETE_TRAVELLER,
  U_FORGOT_PASS,
  U_GET_ALL_PACKAGES,
  U_GET_BED_PREFERENCES,
  U_GET_BOOK_PACKAGE,
  U_GET_CABIN_TYPE,
  U_GET_CAR_FINANCE_RESPONSIBILITY,
  U_GET_CAR_POLICY,
  U_GET_CAR_SPECIFICATIONS,
  U_GET_CAR_SUPPLIER,
  U_GET_CAR_TYPE,
  U_GET_CRUISE_FEATURE,
  U_GET_CRUISE_SUPPLIER,
  U_GET_CURRENCIES,
  U_GET_FLIGHT_AIRLINES,
  U_GET_FLIGHT_CLASSES,
  U_GET_FLIGHT_EQUIPMENTS,
  U_GET_FLIGHT_MODES,
  U_GET_FLIGHT_POLICIES,
  U_GET_FLIGHT_SEATINGS,
  U_GET_FLIGHT_STOPS,
  U_GET_FLIGHT_TIMES,
  U_GET_LANGUAGES,
  U_GET_MANUAL_TRIP,
  U_GET_NAVIGATION_APPS,
  U_GET_PAYMENT_CARD,
  U_GET_PAYMENT_METHODS,
  U_GET_REQUESTED_TRIPS,
  U_GET_ROOM_TYPES,
  U_GET_SCHEDULED_FLIGHT,
  U_GET_SINGLE_FLIGHT,
  U_GET_SINGLE_PACKAGE,
  U_GET_STAY_POLICY,
  U_GET_STAY_PROPERTY_TYPE,
  U_GET_STAY_SERVICES,
  U_GET_TEMPERATURE,
  U_GET_TIME_FORMAT,
  U_GET_TRAIN_CLASSES,
  U_GET_TRAIN_POLICIES,
  U_GET_TRAVELLER,
  U_GET_UNITS,
  U_IMAGE_BASE,
  U_LOGIN,
  U_OTP_VERIFICATION,
  U_REQUEST_TRIP,
  U_RESEND_OTP,
  U_RESET_PASS,
  U_SCHEDULE_CAR,
  U_SCHEDULE_CRUISE,
  U_SCHEDULE_FLIGHT,
  U_SCHEDULE_MANUAL_TRIP,
  U_SCHEDULE_TRAIN,
  U_UPDATE_CREATE_PASSPORT,
  U_UPDATE_PROFILE,
  U_USER_PROFILE,
  U_GET_SCHEDULED_CRUISE,
  U_GET_SCHEDULED_TRAINS,
  U_GET_SCHEDULED_STAY,
  U_DELETE_STAY,
  U_DELETE_SCHEDULED_TRAIN,
  U_GET_PACKAGE_SUMMARY,
  U_GET_COUNTRIES,
  U_GET_ADMINISTRATIVE_FEES,
  U_REQUEST_PACKAGE,
  U_SAVE_PREFERENCE_SETTING,
  U_GET_PREFERENCE_SETTING,
  U_REGISTER,
  U_GET_SPECIAL_NEEDS,
  U_GET_REQUESTED_SUMMARY,
  U_SERVICE_PAYMENT,
  U_UPDATE_SCHEDULED_FLIGHT,
  U_UPDATE_SCHEDULED_CRUISE,
  U_GET_SINGLE_CRUISE,
  U_GET_SINGLE_TRAIN,
  U_UPDATE_TRAIN,
  U_GET_SINGLE_CAR,
  U_UPDATE_CAR,
  U_GET_SINGLE_STAY,
  UPDATE_STAY,
  U_CREATE_PACKAGE_REQUEST,
  U_GET_SINGLE_PACKAGE_REQUEST,
  U_GET_SINGLE_TRAVELLER,
  U_REJECT_OFFER,
  U_GET_SINGLE_TRIP_OFFER,
  U_GET_SINGLE_PACKAGE_OFFER,
  U_REJECT_OFFER_PACKAGE,
  U_TRIP_P2,
  U_PACKAGE_P2,
  U_GET_SCHEDULED_CARS,
  U_DELETE_CAR,
  U_UPDATE_TRAVELLER_DETAILS,
  U_ADD_UPDATE_TRAVELLER_PASSPORT,
  U_GET_ATTRACTION_SHOW,
  U_GET_CUISINE_TYPES,
  U_GET_ACTIVITY_EXPERIENCES,
  U_GET_SEA_ACTIVITIES,
  U_GET_INDOOR_ACTIVITIES,
  U_GET_OUTDOOR_ACTIVITIES,
  U_GET_SPORTS,
  U_GET_AIR_ACTIVITIES,
  U_GET_FOOD_EXPERIENCE,
  U_GET_WORKSHOPS,
  U_GET_LOCAL_TRIPS,
  U_REMOVE_LOCAL_TRIP,
  U_SCHEDULE_LOCAL_TRIP,
  U_UPDATE_LOCAL_TRIP,
  U_GET_SINGLE_LOCAL_TRIP,
  U_GET_STAY_LOCAL_TRIP,
  U_GET_TRIP_TRANSPORTATION,
  U_ADD_ADDONS,
  U_REMOVE_ADDONS,
  U_GET_TRIP_ADDONS,
  U_GET_TRAVELLER_VISA,
  U_DELETE_TRAVELLER_VISA,
  U_GET_VISA_PREFERENCE,
  U_GET_TRAVELLERS_INSURANCE,
  U_GET_DELETE_TRAVELLER_INSURANCE,
  U_DELETE_TRAVELLER_INSURANCE,
  U_GET_TRAVELLERS_LICENCE,
  U_DELETE_TRAVELLER_LICENCE,
  U_ADD_TRANSFER,
  U_GET_TRANSFER,
  U_DELETE_TRANSFER,
  U_UPDATE_TRIP,
  U_GET_HELP_TIPS,
  U_ADD_REMOVE_ADDONS,
  U_GET_OFFERS,
  U_GET_CREDIT,
  U_SET_CREDIT_PAY,
} from '../helper/Constants';
import {Alert} from 'react-native';
import Toast from 'react-native-simple-toast';

const headers = async () => {
  const headers = {
    'Content-Type': 'application/json',
  };

  const auth = new Auth();
  const token = await auth.getValue(AS_USER_TOKEN);

 // await console.log('user token is:-', token);
  if (token) {
    headers.Authorization = `Bearer ${token}`;
    console.log('user token is:-', token);
  }
  return headers;
};

const request = async (method, path, body) => {
  const url = `${U_BASE}${path}`;

  const options = {method, url, headers: await headers()};
  console.log('url', url)
  if (body) {
    options.data = body;
  }
  console.log('params', body)
  return axios(options);
};

export default class API {
  loginAPI(data) {
    return request('POST', U_LOGIN, data);
  }

  signUpApi(data) {
    return request('POST', U_REGISTER, data);
  }

  forgetPassword(data) {
    return request('POST', U_FORGOT_PASS, data);
  }

  resetPassword(data) {
    return request('POST', U_RESET_PASS, data);
  }

  resendOtp(data) {
    return request('POST', U_RESEND_OTP, data);
  }

  otpVerification(data) {
    return request('POST', U_OTP_VERIFICATION, data);
  }

  getSinglePackageOffer(val) {
    return request('GET', U_GET_SINGLE_PACKAGE_OFFER + val);
  }

  requestPackage(data) {
    return request('POST', U_REQUEST_PACKAGE, data);
  }

  packageP2(data) {
    return request('POST', U_PACKAGE_P2, data);
  }

  getUserDetail() {
    return request('GET', U_USER_PROFILE);
  }

  getSpecialNeeds() {
    return request('GET', U_GET_SPECIAL_NEEDS);
  }

  getPreferenceSetting(val) {
    return request('GET', U_GET_PREFERENCE_SETTING);
  }

  savePreferenceSetting(data) {
    return request('POST', U_SAVE_PREFERENCE_SETTING, data);
  }

  getCountries() {
    return request('GET', U_GET_COUNTRIES);
  }

  updateProfile(data, val, id) {
    return request('POST', U_UPDATE_PROFILE, data);
  }

  addTraveller(data) {
    return request('POST', U_ADD_TRAVELLER, data);
  }

  updateTravellerDetails(data, val) {
    return request('POST', U_UPDATE_TRAVELLER_DETAILS + val, data);
  }

  addUpdateTravellerPassport(data, val) {
    return request('POST', U_ADD_UPDATE_TRAVELLER_PASSPORT + val, data);
  }

  updateOrCreatePassport(data, val, id) {
    if (val == 1) {
      return request('POST', U_ADD_PASSPORT + id, data);
    } else {
      return request('POST', U_UPDATE_CREATE_PASSPORT, data);
    }
  }

  getTravellers() {
    return request('GET', U_GET_TRAVELLER);
  }

  getCreditData() {
    return request('GET', U_GET_CREDIT);
  }

  getSingleTraveller(val) {
    return request('GET', U_GET_SINGLE_TRAVELLER + val);
  }

  deleteTraveller(val) {
    return request('DELETE', U_DELETE_TRAVELLER + val);
  }

  getAllPacakges() {
    return request('GET', U_GET_ALL_PACKAGES);
  }

  getAllOffers() {
    return request('GET', U_GET_OFFERS);
  }

  getRequestedSummary(val) {
    return request('GET', U_GET_REQUESTED_SUMMARY + val);
  }
  createPackageRequest(data) {
    return request('POST', U_CREATE_PACKAGE_REQUEST, data);
  }
  getSinglePackageRequest(id) {
    return request('GET', U_GET_SINGLE_PACKAGE_REQUEST + id);
  }

  getLanguages() {
    return request('GET', U_GET_LANGUAGES);
  }

  getCurrencies() {
    return request('GET', U_GET_CURRENCIES);
  }

  getUnits() {
    return request('GET', U_GET_UNITS);
  }

  getTemperature() {
    return request('GET', U_GET_TEMPERATURE);
  }

  getTimeFormat() {
    return request('GET', U_GET_TIME_FORMAT);
  }

  getNavigationApps() {
    return request('GET', U_GET_NAVIGATION_APPS);
  }

  //PAYMENT_CARDS
  getPaymentMethods() {
    return request('GET', U_GET_PAYMENT_METHODS);
  }

  getPaymentCards() {
    return request('GET', U_GET_PAYMENT_CARD);
  }

  addPaymentCard(data) {
    if (data) {
      return request('POST', U_ADD_PAYMENT_CARD, data);
    }
  }

  servicePayment(data) {
    return request('POST', U_SERVICE_PAYMENT, data);
  }

  tripP2(data) {
    return request('POST', U_TRIP_P2, data);
  }

  //TRIPS
  getAdministrativeFees(val) {
    return request('GET', U_GET_ADMINISTRATIVE_FEES + val);
  }

  getUpcomingTrips(data) {
    return request('POST', U_GET_REQUESTED_TRIPS, data);
  }

  rejectOffer(val, data) {
    return request('POST', U_REJECT_OFFER + val, data);
  }

  rejectOfferPackage(val, data) {
    return request('POST', U_REJECT_OFFER_PACKAGE + val, data);
  }

  getBookedTrips(data) {
    return request('POST', U_GET_BOOK_PACKAGE, data);
  }

  getSinglePackage(id) {
    return request('GET', U_GET_SINGLE_PACKAGE + id);
  }

  requestTrip(data) {
    return request('POST', U_REQUEST_TRIP, data);
  }

  updateRequestedTrip(val,data) {
    return request('POST', U_UPDATE_TRIP + val, data);
  }

  getSingleTripOffer(val) {
    return request('GET', U_GET_SINGLE_TRIP_OFFER + val);
  }

  setCreditPay(val) {
    return request('GET', U_SET_CREDIT_PAY + val);
  }

  addRemoveAddons(data) {
    return request('POST', U_ADD_REMOVE_ADDONS,data);
  }

  

  //MANUAL TRIP
  getManualTrips(data) {
    return request('POST', U_GET_MANUAL_TRIP,data);
  }

  scheduleManualTrip(data, val) {
    return request('POST', U_SCHEDULE_MANUAL_TRIP + val, data);
  }

  //FLIGHT_APIS
  getAirlineData() {
    return request('GET', U_GET_FLIGHT_AIRLINES);
  }

  getSingleFlight(id) {
    return request('GET', U_GET_SINGLE_FLIGHT + id);
  }

  getScheduledFlight(id) {
    return request('GET', U_GET_SCHEDULED_FLIGHT + id);
  }

  updateSheduledFlight(data, id) {
    return request('POST', U_UPDATE_SCHEDULED_FLIGHT + id, data);
  }

  getEquipmentData() {
    return request('GET', U_GET_FLIGHT_EQUIPMENTS);
  }

  getFlightStops() {
    return request('GET', U_GET_FLIGHT_STOPS);
  }

  getFlightSeatings() {
    return request('GET', U_GET_FLIGHT_SEATINGS);
  }

  getFlightTimes() {
    return request('GET', U_GET_FLIGHT_TIMES);
  }

  getFlightPolicies() {
    return request('GET', U_GET_FLIGHT_POLICIES);
  }

  getFlightClasses() {
    return request('GET', U_GET_FLIGHT_CLASSES);
  }

  getFlightModes() {
    return request('GET', U_GET_FLIGHT_MODES);
  }

  scheduleFlight(data, id) {
    return request('POST', U_SCHEDULE_FLIGHT + id, data);
  }

  //STAY_APIS
  getStayServices() {
    return request('GET', U_GET_STAY_SERVICES);
  }

  getStayPolicy() {
    return request('GET', U_GET_STAY_POLICY);
  }

  getStayProperty() {
    return request('GET', U_GET_STAY_PROPERTY_TYPE);
  }

  getRoomTypes() {
    return request('GET', U_GET_ROOM_TYPES);
  }

  getBedPreferences() {
    return request('GET', U_GET_BED_PREFERENCES);
  }

  scheduleStay(data, id) {
    return request('POST', SCHEDULE_STAY + id, data);
  }

  updateStay(data, id) {
    return request('POST', UPDATE_STAY + id, data);
  }

  getScheduledStay(id) {
    return request('GET', U_GET_SCHEDULED_STAY + id);
  }

  deleteScheduleStay(id) {
    return request('DELETE', U_DELETE_STAY + id);
  }

  getSingleStay(id) {
    return request('GET', U_GET_SINGLE_STAY + id);
  }

  //CRUISE_TYPES
  getCruiseCabinData() {
    return request('GET', U_GET_CABIN_TYPE);
  }

  getCruiseSupplies() {
    return request('GET', U_GET_CRUISE_SUPPLIER);
  }

  getCruiseFeatures() {
    return request('GET', U_GET_CRUISE_FEATURE);
  }

  schduleCruise(data, id) {
    return request('POST', U_SCHEDULE_CRUISE + id, data);
  }

  getScheduledCruise(val) {
    return request('GET', U_GET_SCHEDULED_CRUISE + val);
  }

  updateCruise(data, id) {
    return request('POST', U_UPDATE_SCHEDULED_CRUISE + id, data);
  }

  getSingleCruise(id) {
    return request('GET', U_GET_SINGLE_CRUISE + id);
  }

  //Car
  getCarType() {
    return request('GET', U_GET_CAR_TYPE);
  }

  getCarSpecifications() {
    return request('GET', U_GET_CAR_SPECIFICATIONS);
  }

  getCarFinanceResponsibility() {
    return request('GET', U_GET_CAR_FINANCE_RESPONSIBILITY);
  }

  getCarSupplier() {
    return request('GET', U_GET_CAR_SUPPLIER);
  }

  getCarPolicy() {
    return request('GET', U_GET_CAR_POLICY);
  }

  scheduleCar(data, id) {
    return request('POST', U_SCHEDULE_CAR + id, data);
  }

  getSingleCar(id) {
    return request('GET', U_GET_SINGLE_CAR + id);
  }

  deleteCar(id) {
    return request('DELETE', U_DELETE_CAR + id);
  }

  updateCar(data, id) {
    return request('POST', U_UPDATE_CAR + id, data);
  }

  getCarSchedules(id) {
    return request('GET', U_GET_SCHEDULED_CARS + id);
  }
  //Train
  getTrainClasses() {
    return request('GET', U_GET_TRAIN_CLASSES);
  }

  getTrainPolicies() {
    return request('GET', U_GET_TRAIN_POLICIES);
  }

  scheduleTrain(data, id) {
    return request('POST', U_SCHEDULE_TRAIN + id, data);
  }

  updateTrain(data, id) {
    return request('POST', U_UPDATE_TRAIN + id, data);
  }

  getScheduledTrains(val) {
    return request('GET', U_GET_SCHEDULED_TRAINS + val);
  }

  deleteScheduledTrain(id) {
    return request('DELETE', U_DELETE_SCHEDULED_TRAIN + id);
  }

  getSingleTrain(id) {
    return request('GET', U_GET_SINGLE_TRAIN + id);
  }

  //Upload Image
  uploadImage(data) {
    return request('POST', UPLOAD_IMAGE, data);
  }

  //TRIP PLAN
  getAttractionShow() {
    return request('GET', U_GET_ATTRACTION_SHOW);
  }

  getCuisines() {
    return request('GET', U_GET_CUISINE_TYPES);
  }

  getActivityExperiences() {
    return request('GET', U_GET_ACTIVITY_EXPERIENCES);
  }

  getSeaActivities() {
    return request('GET', U_GET_SEA_ACTIVITIES);
  }

  getIndoorActivities() {
    return request('GET', U_GET_INDOOR_ACTIVITIES);
  }

  getOutdoorActivities() {
    return request('GET', U_GET_OUTDOOR_ACTIVITIES);
  }

  getSports() {
    return request('GET', U_GET_SPORTS);
  }

  getAirActivities() {
    return request('GET', U_GET_AIR_ACTIVITIES);
  }

  getFoodExperiences() {
    return request('GET', U_GET_FOOD_EXPERIENCE);
  }

  getWorkshops() {
    return request('GET', U_GET_WORKSHOPS);
  }

  getLocalTrips(val) {
    return request('GET', U_GET_LOCAL_TRIPS + val);
  }

  removeLocalTrip(val) {
    return request('DELETE', U_REMOVE_LOCAL_TRIP + val);
  }

  scheduleLocalTrip(id, data) {
    return request('POST', U_SCHEDULE_LOCAL_TRIP + id, data);
  }

  updateLocalTrip(id, data) {
    return request('POST', U_UPDATE_LOCAL_TRIP + id, data);
  }

  getSingleLocalTrip(id) {
    return request('GET', U_GET_SINGLE_LOCAL_TRIP + id);
  }

  getStayLocalTrip(id, data) {
    return request('POST', U_GET_STAY_LOCAL_TRIP + id, data);
  }

  //Addons

  getTripTransportation(val, type) {
    return request('GET', U_GET_TRIP_TRANSPORTATION + val + '/' + type);
  }

  addAddons(type, val, data) {
    return request('POST', U_ADD_ADDONS + type + '/' + val, data);
  }

  removeAddons(type, val, id) {
    return request('DELETE', U_REMOVE_ADDONS + type + '/' + val + '/' + id);
  }

  getTripAddons(type, val) {
    return request('GET', U_GET_TRIP_ADDONS + type + '/' + val);
  }

  getTravellerVisa(val, type) {
    return request('GET', U_GET_TRAVELLER_VISA + val + '/' + type);
  }

  deleteTravellerVisa(val, id, type) {
    return request(
      'DELETE',
      U_DELETE_TRAVELLER_VISA + val + '/' + id + '/' + type,
    );
  }

  getVisaPreference(id) {
    return request('GET', U_GET_VISA_PREFERENCE + id);
  }

  getTravellersInsurance(val, type) {
    return request('GET', U_GET_TRAVELLERS_INSURANCE + val + '/' + type);
  }

  deleteTravellerInsurance(val, id, type) {
    return request(
      'DELETE',
      U_DELETE_TRAVELLER_INSURANCE + val + '/' + id + '/' + type,
    );
  }

  getTravellersLicence(val, type) {
    return request('GET', U_GET_TRAVELLERS_LICENCE + val + '/' + type);
  }

  deleteTravellerLicence(val, id, type) {
    return request(
      'DELETE',
      U_DELETE_TRAVELLER_LICENCE + val + '/' + id + '/' + type,
    );
  }

  //Transfer
  addTransfer(val, data) {
    return request('POST', U_ADD_TRANSFER + val, data);
  }

  getTransfers(val) {
    return request('GET', U_GET_TRANSFER + val);
  }

  deleteTransfer(val) {
    return request('DELETE', U_DELETE_TRANSFER + val);
  }


  //Help and tips
  getHelpDetails(id,type) {
    return request('GET', U_GET_HELP_TIPS + `${id}` + '/' +`${type}`);
  }
}
