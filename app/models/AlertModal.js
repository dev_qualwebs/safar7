import React from 'react';
import { Platform } from 'react-native';
import { Modal, Text, View, StyleSheet, Image } from 'react-native';
import { TouchableOpacity } from 'react-native';
import { openSettings, PERMISSIONS, request } from 'react-native-permissions';
import { widthPercentageToDP } from 'react-native-responsive-screen';
import COLOR from '../components/styles/Color';
import FONTS from '../components/styles/Fonts';
import IMAGES from '../components/styles/Images';
import Styles from '../components/styles/Styles';

export default AlertModal = (props) => {
  return (
    <View style={Styles.container}>
      <Modal
        animationType={'slide'}
        transparent={true}
        style={{ backgroundColor: COLOR.BLACK }}
        visible={props.modalVisible}
        onRequestClose={() => {
          console.log('Modal has been closed.');
        }}>
        <View style={styles.modal}>
          <View style={styles.modalBody}>
            <Image source={require('../components/styles/assets/map.png')} resizeMode={'contain'} style={styles.image} />
            <Text
              style={[
                Styles.subheading_label,
                {
                  alignSelf: 'center',
                  width: null,
                  fontSize: FONTS.HEADING,
                  fontFamily: FONTS.FAMILY_BOLD,
                },
              ]}>
              Allow location to let us{'\n'}guide for during your trip
            </Text>
            <Text
              style={[
                {
                  fontSize: FONTS.REGULAR,
                  fontFamily: FONTS.FAMILY_REGULAR,
                  marginTop: 10,
                  alignSelf: 'center',
                  marginHorizontal: 10,
                },
              ]}>
              This state is indicating that your location permission is turned
              off.
            </Text>
            <View style={{ marginVertical: 20 }}>
              <TouchableOpacity
                style={{
                  alignItems: 'center',
                  justifyContent: 'center',
                  height: 50,
                  borderRadius: 10,
                  width: widthPercentageToDP(70),
                  backgroundColor: COLOR.GREEN,
                }}
                onPress={() => {
                  props.updateMasterState("showAlertModal", false);
                  props.checkNotificationPermission();
                  if (Platform.OS == 'ios') {
                    openSettings().catch(() => console.log("can't open settings"));
                  } else {
                    request(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION).then((result) => {
                      // …
                    });
                  }

                }}
              >
                <Text style={{ color: COLOR.WHITE }}>Allow Location Access</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={{
                  alignItems: 'center',
                  justifyContent: 'center',
                  height: 50,
                  borderRadius: 10,
                  width: widthPercentageToDP(70),
                  backgroundColor: COLOR.WHITE,
                }}
                onPress={() => {
                  props.updateMasterState("showAlertModal", false);
                  props.checkNotificationPermission();
                }}
              >
                <Text style={{ color: COLOR.BLACK }}>Remind me later</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
    </View>
  );
};

const styles = StyleSheet.create({
  modal: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
  },
  modalBody: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    backgroundColor: COLOR.WHITE,
    borderWidth: 1,
    width: widthPercentageToDP(80),
    borderColor: COLOR.BORDER,
    borderRadius: 10,
  },
  image: {
    height: 120,
    width: 120,
    alignSelf: 'center',
    marginVertical: 20,
  },
});
