import React from 'react';
import { Modal, Text, View, StyleSheet, Image } from 'react-native';
import { TouchableOpacity } from 'react-native';
import { openSettings, requestNotifications } from 'react-native-permissions';
import { widthPercentageToDP } from 'react-native-responsive-screen';
import COLOR from '../../components/styles/Color';
import FONTS from '../../components/styles/Fonts';
import IMAGES from '../../components/styles/Images';
import Styles from '../../components/styles/Styles';

export default NotificationModal = (props) => {
  return (
    <View style={Styles.container}>
      <Modal
        animationType={'slide'}
        transparent={true}
        style={{ backgroundColor: COLOR.BLACK }}
        visible={props.modalVisible}
        onRequestClose={() => {
          console.log('Modal has been closed.');
        }}>
        <View style={styles.modal}>
          <View style={styles.modalBody}>
            <Image source={require('../../components/styles/assets/bell.png')} resizeMode={'contain'} style={styles.image} />
            <Text
              style={[
                Styles.subheading_label,
                {
                  alignSelf: 'center',
                  textAlign: 'center',
                  width: null,
                  fontSize: FONTS.HEADING,
                  fontFamily: FONTS.FAMILY_BOLD,
                },
              ]}>
              Allow notification to catch {'\n'}
              all the alerts and offers
            </Text>
            <Text
              style={[
                {
                  fontSize: FONTS.REGULAR,
                  fontFamily: FONTS.FAMILY_REGULAR,
                  marginTop: 10,
                  textAlign: 'center',
                  alignSelf: 'center',
                  marginHorizontal: 10,
                },
              ]}>
              This state is indicating that your notifications is disabled.
            </Text>
            <View style={{ marginVertical: 20 }}>
              <TouchableOpacity
                onPress={() => {
                  props.updateMasterState("showNotificationModal", false);

                  if (Platform.OS == 'ios') {
                    openSettings().catch(() => console.log("can't open settings"));
                  } else {
                    requestNotifications(['alert', 'sound']).then(({ status, settings }) => {
                      // …
                    });
                  }

                }}
                style={{
                  alignItems: 'center',
                  justifyContent: 'center',
                  height: 50,
                  borderRadius: 10,
                  width: widthPercentageToDP(70),
                  backgroundColor: COLOR.GREEN,
                }}>
                <Text style={{ color: COLOR.WHITE }}>Allow Notifications</Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => props.updateMasterState("showNotificationModal", false)}
                style={{
                  alignItems: 'center',
                  justifyContent: 'center',
                  height: 50,
                  borderRadius: 10,
                  width: widthPercentageToDP(70),
                  backgroundColor: COLOR.WHITE,
                }}>
                <Text style={{ color: COLOR.BLACK }}>Remind me later</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
    </View >
  );
};

const styles = StyleSheet.create({
  modal: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
  },
  modalBody: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    backgroundColor: COLOR.WHITE,
    borderWidth: 1,
    width: widthPercentageToDP(80),
    borderColor: COLOR.BORDER,
    borderRadius: 10,
  },
  image: {
    height: 120,
    width: 120,
    alignSelf: 'center',
    marginVertical: 20,
  },
});
