'use strict';

import COLOR from '../components/styles/Color';
import IMAGES from '../components/styles/Images';

//URLS
export const U_BASE = 'http://54.176.179.158/safar7/public/api/v1/';
export const U_IMAGE_BASE = 'http://54.176.179.158/safar7/public/images/';

export const U_LOGIN = 'auth/login';
export const U_REGISTER = 'auth/register';
export const U_OTP_VERIFICATION = 'auth/verify';
export const U_RESEND_OTP = 'auth/resend';
export const U_FORGOT_PASS = 'auth/forget';
export const U_RESET_PASS = 'auth/reset';
export const U_UPDATE_PROFILE = 'auth/profile';
export const U_UPDATE_CREATE_PASSPORT = 'auth/passport';
export const U_GET_OFFERS = 'offer-and-discounts';
export const U_USER_PROFILE = 'auth/me';
export const U_GET_PREFERENCE_SETTING = 'auth/preference';
export const U_SAVE_PREFERENCE_SETTING = 'auth/preference';
export const U_UPDATE_PASSWORD = 'auth/passport';
export const U_PREFERENCE_SETTING = 'auth/preference';

export const U_GET_COUNTRIES = 'countries';
export const U_GET_DIAL_CODE = 'dial-code/';
export const U_GET_CURRENCIES = 'currencies';
export const U_GET_TEMPERATURE = 'temperatures';
export const U_GET_UNITS = 'units';
export const U_GET_LANGUAGES = 'languages';
export const U_GET_NAVIGATION_APPS = 'navigation-apps';
export const U_GET_TIME_FORMAT = 'formats';
export const U_GET_SPECIAL_NEEDS = 'special-needs';
export const U_GET_PAYMENT_METHODS = 'payment-methods';

export const U_ADD_TRAVELLER = 'user/traveller';
export const U_ADD_PASSPORT = 'user/traveller-passport/';
export const U_UPDATE_TRAVELLER_DETAILS = 'user/traveller/';
export const U_GET_TRAVELLER = 'user/travellers';
export const U_GET_CREDIT = 'user/earn-credits';
export const U_GET_SINGLE_TRAVELLER = 'user/travellers/';
export const U_DELETE_TRAVELLER = 'user/travellers/';
export const U_ADD_UPDATE_TRAVELLER_PASSPORT = 'user/traveller-passport/';

export const U_GET_ADMINISTRATIVE_FEES = 'fees/';
export const U_BOOK_PACKAGE = 'book/package';
export const U_REQUEST_PACKAGE = 'package-p1-payment';
export const U_PACKAGE_P2 = 'package-p2-payment';
export const U_GET_BOOK_PACKAGE = 'request/packages';
export const U_GET_SINGLE_PACKAGE = 'package/';
export const U_GET_SINGLE_PACKAGE_OFFER = 'offer-package/';

export const U_ADD_PAYMENT_CARD = 'user/payment-card';
export const U_GET_PAYMENT_CARD = 'user/payment-card';
export const U_SERVICE_PAYMENT = 'trip-p1-payment';
export const U_TRIP_P2 = 'trip-p2-payment';

export const U_GET_SINGLE_TRIP_OFFER = 'offer-trip/';
export const U_SET_CREDIT_PAY = 'offer-trip-discount/'

export const U_ADD_REMOVE_ADDONS = 'trip/addon-remove';

//PACKAGE
export const U_GET_ALL_PACKAGES = 'packages';
export const U_GET_PACKAGE_SUMMARY = 'user/package-trip/';
export const U_GET_REQUESTED_SUMMARY = 'user/request-summary/';
export const U_GET_SINGLE_PACKAGES_DETAIL = 'package/';
export const U_CREATE_PACKAGE_REQUEST = 'request/create';
export const U_GET_SINGLE_PACKAGE_REQUEST = 'request/packages/';

//Add Flight
export const U_GET_FLIGHT_EQUIPMENTS = 'flight-equipments';
export const U_GET_FLIGHT_STOPS = 'flight-stops';
export const U_GET_FLIGHT_SEATINGS = 'flight-seating';
export const U_GET_FLIGHT_TIMES = 'flight-times';
export const U_GET_FLIGHT_POLICIES = 'flight-policies';
export const U_GET_FLIGHT_AIRLINES = 'flight-airlines';
export const U_GET_FLIGHT_CLASSES = 'flight-classes';
export const U_GET_FLIGHT_MODES = 'flight-modes';
export const U_SCHEDULE_FLIGHT = 'schedule/flight/';
export const U_GET_SINGLE_FLIGHT = 'schedule/flight/';
export const U_GET_SCHEDULED_FLIGHT = 'schedule/flight-schedules/';
export const U_UPDATE_SCHEDULED_FLIGHT = 'schedule/u-flight/';

//Add Stay
export const U_GET_ROOM_TYPES = 'room-types';
export const U_GET_BED_PREFERENCES = 'bed-preference';
export const U_GET_STAY_POLICY = 'stay-policies';
export const U_GET_STAY_PROPERTY_TYPE = 'property-types';
export const U_GET_STAY_SERVICES = 'stay-services';
export const SCHEDULE_STAY = 'schedule/stay/';
export const UPDATE_STAY = 'schedule/u-stay/';
export const U_GET_SCHEDULED_STAY = 'schedule/stay-schedules/';
export const U_DELETE_STAY = 'schedule/stay/';
export const U_GET_SINGLE_STAY = 'schedule/stay/';

//Add Cruise
export const U_GET_CABIN_TYPE = 'cabin-types';
export const U_GET_CRUISE_SUPPLIER = 'cruise-suppliers';
export const U_GET_CRUISE_FEATURE = 'cruise-features';
export const U_SCHEDULE_CRUISE = 'schedule/cruise/';
export const U_GET_SCHEDULED_CRUISE = 'schedule/cruise-schedules/';
export const U_UPDATE_SCHEDULED_CRUISE = 'schedule/u-cruise/';
export const U_GET_SINGLE_CRUISE = 'schedule/cruise/';

//Add Train
export const U_GET_TRAIN_CLASSES = 'train-classes';
export const U_GET_TRAIN_POLICIES = 'train-policies';
export const U_SCHEDULE_TRAIN = 'schedule/train/';
export const U_UPDATE_TRAIN = 'schedule/u-train/';
export const U_GET_SCHEDULED_TRAINS = 'schedule/train-schedules/';
export const U_DELETE_SCHEDULED_TRAIN = 'schedule/train/';
export const U_GET_SINGLE_TRAIN = 'schedule/train/';

//Upload Image
export const UPLOAD_IMAGE = 'upload';

//Trips
export const U_REQUEST_TRIP = 'request-trip';
export const U_UPDATE_TRIP = 'request-trip/';
export const U_GET_REQUESTED_TRIPS = 'user-request-trip';
export const U_REJECT_OFFER = 'reject-offer/';
export const U_REJECT_OFFER_PACKAGE = 'reject-package-offer/';

//MANUAL TRIP
export const U_GET_MANUAL_TRIP = 'manual-trip';
export const U_SCHEDULE_MANUAL_TRIP = 'manual-schedule/trip/';

//Add Car
export const U_GET_CAR_TYPE = 'car-types';
export const U_GET_CAR_SPECIFICATIONS = 'car-specifications';
export const U_GET_CAR_FINANCE_RESPONSIBILITY = 'car-finance';
export const U_GET_CAR_SUPPLIER = 'car-suppliers';
export const U_GET_CAR_POLICY = 'car-policies';
export const U_SCHEDULE_CAR = 'schedule/car/';
export const U_GET_SINGLE_CAR = 'schedule/car/';
export const U_UPDATE_CAR = 'schedule/u-car/';
export const U_DELETE_CAR = 'schedule/car/';
export const U_GET_SCHEDULED_CARS = 'schedule/car-schedules/';

//CURRENT_TAB
export const T_REQUEST_TRIP_TAB = 'request_trip_tab';
export const T_REQUEST_OR_PACKAGE_TAB = 'request_package_tab';
export const T_SCHEDULE_TRIP_TAB = 'schedule_trip_tab';
export const T_SCHEDULE_FLIGHT_TAB = 'schedule_flight_tab';
export const T_SCHEDULE_CAR_TAB = 'schedule_car_tab';
export const T_PROFILE_TAB = 'profile_tab';
export const T_TRAVELLER_TAB = 'traveller_tab';
export const T_PACKAGE_DETAIL_TAB = 'package_detail_tab';

//Userdefaults
export const AS_INITIAL_ROUTE = 'initial_route_name';
export const AS_USER_DETAIL = 'user_details';
export const AS_USER_TOKEN = 'user_access_token';
export const AS_SHOW_BACK_CONTACT = 'show_back_button_contact';
export const AS_SELECTED_CONTACT = 'selected_contact';
export const AS_PINCODE_VALUE = 'pincode_value';
export const AS_FCM_TOKEN = 'fcm_token';

//TRIP PLAN
export const U_GET_ATTRACTION_SHOW = 'attraction-shows';
export const U_GET_CUISINE_TYPES = 'cuisines';
export const U_GET_ACTIVITY_EXPERIENCES = 'activities-experiences';

export const U_GET_AIR_ACTIVITIES = 'air-activities';
export const U_GET_SEA_ACTIVITIES = 'sea-activities';
export const U_GET_SPORTS = 'sports';
export const U_GET_FOOD_EXPERIENCE = 'food-experiences';
export const U_GET_INDOOR_ACTIVITIES = 'indoor-activities';
export const U_GET_OUTDOOR_ACTIVITIES = 'outdoor-activities';
export const U_GET_WORKSHOPS = 'workshops';

export const U_GET_LOCAL_TRIPS = 'schedule/local-schedules/';
export const U_REMOVE_LOCAL_TRIP = 'schedule/local/';
export const U_SCHEDULE_LOCAL_TRIP = 'schedule/local/';
export const U_UPDATE_LOCAL_TRIP = 'schedule/u-local/';
export const U_GET_SINGLE_LOCAL_TRIP = 'schedule/local/';
export const U_GET_STAY_LOCAL_TRIP = 'schedule/stay-local/';

//Addons
export const U_GET_TRIP_TRANSPORTATION = 'addon/transport/';
export const U_ADD_ADDONS = 'trip/addon/';
export const U_REMOVE_ADDONS = 'trip/addon/';
export const U_GET_TRIP_ADDONS = 'trip/addon/';
export const U_GET_TRAVELLER_VISA = 'addon/visa/';
export const U_DELETE_TRAVELLER_VISA = 'addon/visa/';
export const U_GET_VISA_PREFERENCE = 'visa-preference/';
export const U_GET_TRAVELLERS_INSURANCE = 'addon/insurance/';
export const U_DELETE_TRAVELLER_INSURANCE = 'addon/insurance/';
export const U_GET_TRAVELLERS_LICENCE = 'addon/licence/';
export const U_DELETE_TRAVELLER_LICENCE = 'addon/licence/';

//TRANSFER
export const U_ADD_TRANSFER = 'schedule/transfer/';
export const U_GET_TRANSFER = 'schedule/transfer-schedules/';
export const U_DELETE_TRANSFER = 'schedule/transfer/';


//Help and Tips
export const U_GET_HELP_TIPS = 'country-tips/'

//Selection Data
export const D_DEPARTURE_TIME = [
  {name: 'Any', time: '', image: '', bg: COLOR.WHITE},
  {name: 'Morning', time: '06:00 - 12:00', image: ''},
  {name: 'Midday', time: '12:00 - 18:00', image: ''},
  {name: 'Evening', time: '18:00 - 00:00', image: ''},
  {name: 'Night', time: '00:00 -06:00', image: ''},
];

export const D_PREFERED_CLASS = [
  {name: 'Economy', time: '', image: ''},
  {name: 'Economy Premium', time: '', image: ''},
  {name: 'Business Class', time: '', image: ''},
  {name: 'First Class', time: '', image: ''},
];

export const D_RESERVATION_POLICY = [
  {name: 'Any', time: '', image: ''},
  {name: 'Free Cancellation', time: '', image: ''},
  {name: 'Subject to Cancellation', time: '', image: ''},
  {name: 'Non-Refundable', time: '', image: ''},
];

export const D_EQUIPMENTS = [
  {name: 'None', time: '', image: ''},
  {name: 'Baby Equipments', time: '', image: ''},
  {name: 'Medical Equipments', time: '', image: ''},
  {name: 'Sports Equipments', time: '', image: ''},
  {name: 'Music Equipments', time: '', image: ''},
  {name: 'Other Equipments', time: '', image: ''},
];

export const D_PROPERTY_TYPES = [
  {name: 'Any', time: '', image: ''},
  {name: 'Hotels', time: '', image: ''},
  {name: 'Apartments', time: '', image: ''},
  {name: 'Resorts', time: '', image: ''},
  {name: 'Chalets', time: '', image: ''},
  {name: 'Cottages', time: '', image: ''},
  {name: 'Farm Stay', time: '', image: ''},
];

export const D_BED_PREFERNCES = [
  {name: 'Any', time: '', image: ''},
  {name: 'Single Bed', time: '', image: ''},
  {name: 'Twin Bed', time: '', image: ''},
  {name: 'Double Bed (Queen or King bed)', time: '', image: ''},
];

export const D_SERVICES_FACILITES = [
  {name: 'None', time: '', image: ''},
  {name: 'Air Conditioning', time: '', image: IMAGES.ac},
  {name: 'Breakfast included', time: '', image: IMAGES.breakfast_included},
  {name: 'Free Wi-Fi', time: '', image: IMAGES.wifi},
  {name: 'Free Parking', time: '', image: ''},
  {name: 'Balcony', time: '', image: IMAGES.three_pillar},
  {name: 'Bathtub', time: '', image: IMAGES.bathtub},
  {name: 'Washing Machine', time: '', image: IMAGES.washing_machine},
  {name: 'Coffee Machine', time: '', image: IMAGES.hot_tea_cup},
  {name: 'Swimming Pool', time: '', image: IMAGES.swimming_pool},
  {name: 'Spa', time: '', image: IMAGES.leaves},
  {name: 'Fitness Center', time: '', image: IMAGES.dumble},
  {name: 'Pets Allowed', time: '', image: IMAGES.dog_leg},
  {name: 'Smoking Allowed', time: '', image: ''},
  {name: 'Indoor Fireplace', time: '', image: IMAGES.indoor_fire},
  {name: 'Iron and Hair Dryer', time: '', image: IMAGES.dryer},
  {name: 'Elevator', time: '', image: IMAGES.elevator},
  {name: 'Soundproof', time: '', image: IMAGES.no_sound},
  {name: 'Beach Front', time: '', image: IMAGES.palm_tree},
  {name: 'Water Front', time: '', image: IMAGES.water_drop},
  {name: 'Mountain Front', time: '', image: IMAGES.mountain},
];

export const D_TRAIN_CLASS = [
  {name: 'Any', time: '', image: ''},
  {name: 'Second Class', time: '', image: ''},
  {name: 'First Class', time: '', image: ''},
];

export const D_TRAIN_SEATING = [
  {id: 1, position: 'Any'},
  {id: 2, position: 'Aisle'},
  {id: 3, position: 'Window'},
];

export const D_TRAIN_SEATING_TYPE = [
  {id: 1, type: 'Any'},
  {id: 2, type: 'Compartment with 6 seats'},
  {id: 3, type: 'Side-by-side seats'},
  {id: 4, type: '4 seater booth'},
];

export const D_CAR_TYPE = [
  {name: 'Any', time: '', image: ''},
  {name: 'Economy', time: '', image: IMAGES.car1},
  {name: 'Full Size', time: '', image: IMAGES.car1},
  {name: 'Premium', time: '', image: IMAGES.car2},
  {name: 'SUV', time: '', image: IMAGES.car4},
  {name: 'Large', time: '', image: IMAGES.car3},
  {name: 'Coupe', time: '', image: IMAGES.car5},
];

export const D_CAR_SPECIFICATION = [
  {name: 'Any', time: '', image: ''},
  {name: 'Automatic Transmission', time: '', image: ''},
  {name: 'Diesel Engine', time: '', image: ''},
  {name: 'GPS Included', time: '', image: ''},
];

export const D_CAR_RESERVATION_POLICY = [
  {name: 'None', time: '', image: ''},
  {name: 'Unlimited Mil/KM', time: '', image: ''},
  {name: 'Free Cancelllation', time: '', image: ''},
  {name: 'Payment with Debit Card', time: '', image: ''},
  {name: 'Pay Later', time: '', image: ''},
  {name: 'Third Party Insurance', time: '', image: ''},
  {name: 'Loss Damage Waiver', time: '', image: ''},
  {name: 'Personal Accident Protection', time: '', image: ''},
  {name: 'Roadside Protection', time: '', image: ''},
];

export const D_CAR_FINANCIAL_RESPONSIBILITY = [
  {name: 'None', time: '', image: ''},
  {name: 'Fully Responsible', time: '', image: ''},
  {name: 'Medium Responsibility', time: '', image: ''},
  {name: 'Low Responsibility', time: '', image: ''},
  {name: 'No Responsibility', time: '', image: ''},
];

export const D_CABIN_TYPE = [
  {name: 'Any', time: '', image: ''},
  {name: 'Interior', time: '', image: ''},
  {name: 'Ocean View', time: '', image: ''},
  {name: 'Balcony', time: '', image: ''},
  {name: 'Suite', time: '', image: ''},
];

export const D_CRUISE_FEATURES = [
  {name: 'None', time: '', image: ''},
  {name: 'Transfer to the port', time: '', image: IMAGES.car_image},
  {name: 'Wi-Fi Included', time: '', image: IMAGES.wifi},
  {name: 'Spa and Beauty Salon', time: '', image: IMAGES.leaves},
  {name: 'Fitness Center', time: '', image: IMAGES.dumble},
  {name: 'Outdoor Activities  ', time: '', image: IMAGES.running_man},
  {name: 'Shore Excursions', time: '', image: IMAGES.palm_tree},
  {name: 'Room Service', time: '', image: IMAGES.room_service},
  {name: 'Babysitting', time: '', image: IMAGES.babysitting},
  {name: 'Beverage, Dinning Package', time: '', image: IMAGES.spoon_fork},
];

export const D_TOURISM_TYPE_DATA = [
  {name: 'Any', time: '', image: ''},
  {name: 'Solo', time: '', image: ''},
  {name: 'Couple', time: '', image: ''},
  {name: 'Company', time: '', image: ''},
  {name: 'Friends', time: '', image: ''},
  {name: 'Business', time: '', image: ''},
];

export const D_FOOD_EXPERIENCE = [
  {name: 'None', time: '', image: ''},
  {name: 'Cooking Class', time: '', image: ''},
  {name: 'Baking Class', time: '', image: ''},
  {name: 'Gourmet Experience', time: '', image: ''},
  {name: 'Food Tours', time: '', image: ''},
  {name: 'Food Tasting', time: '', image: ''},
  {name: 'Dessert Tasting', time: '', image: ''},
  {name: 'Cheese Tasting', time: '', image: ''},
  {name: 'Tea Tasting', time: '', image: ''},
  {name: 'Dinner Party', time: '', image: ''},
];

export const D_SPORTS_DATA = [
  {name: 'Any', time: '', image: ''},
  {name: 'Football Match', time: '', image: ''},
  {name: 'Hiking', time: '', image: ''},
  {name: 'Mountain', time: '', image: ''},
  {name: 'Motorcycle Ride', time: '', image: ''},
  {name: 'Tennis Lesson', time: '', image: ''},
  {name: 'Skateboarding', time: '', image: ''},
  {name: 'Fitness Class', time: '', image: ''},
  {name: 'Outdoor Run', time: '', image: ''},
  {name: 'Bicycling', time: '', image: ''},
];

export const D_CLASSES_WORKSHOP = [
  {name: 'Any', time: '', image: ''},
  {name: 'Art/Craft Class', time: '', image: ''},
  {name: 'Makeover Class', time: '', image: ''},
  {name: 'Fashion Class', time: '', image: ''},
  {name: 'Language Class', time: '', image: ''},
  {name: 'Welness Class', time: '', image: ''},
  {name: 'Yoga Class', time: '', image: ''},
  {name: 'Music Class', time: '', image: ''},
  {name: 'Dance Class', time: '', image: ''},
  {name: 'Perfume Workshop', time: '', image: ''},
];

export const D_OUTDOOR_EXP = [
  {name: 'Any', time: '', image: ''},
  {name: 'Hisotry & Culture Walk', time: '', image: ''},
  {name: 'Photo Shoot/Walk', time: '', image: ''},
  {name: 'Art Walk', time: '', image: ''},
  {name: 'Nature Walk', time: '', image: ''},
  {name: 'Horseback Ride', time: '', image: ''},
  {name: 'Farm Visit', time: '', image: ''},
  {name: 'Animal Care', time: '', image: ''},
  {name: 'Personal Styling', time: '', image: ''},
  {name: 'Live Music', time: '', image: ''},
  {name: 'Ziplining', time: '', image: ''},
  {name: 'Social Gathering', time: '', image: ''},
];

export const D_INDOOR_ACTIVITIES = [
  {name: 'Any', time: '', image: ''},
  {name: 'Shopping Experience', time: '', image: ''},
  {name: 'Magic Shows', time: '', image: ''},
  {name: 'Class and Workshop', time: '', image: ''},
  {name: 'karoake Session', time: '', image: ''},
  {name: 'Indoor Skydiving', time: '', image: ''},
  {name: 'Indoor Games', time: '', image: ''},
  {name: 'Night Life', time: '', image: ''},
];

export const D_AIR_ACTIVITIES = [
  {name: 'None', time: '', image: ''},
  {name: 'Plane Ride', time: '', image: ''},
  {name: 'Helicopter Ride', time: '', image: ''},
  {name: 'Paragliding', time: '', image: ''},
  {name: 'Skydiving', time: '', image: ''},
  {name: 'Air Balloon', time: '', image: ''},
  {name: 'Indoor Games', time: '', image: ''},
  {name: 'bungee Jumping', time: '', image: ''},
];

export const D_SEA_ACTIVITY = [
  {name: 'None', time: '', image: ''},
  {name: 'Jestski, Personal Watercraft', time: '', image: ''},
  {name: 'Powerboats, Pontoons, RIBs', time: '', image: ''},
  {name: 'Para-sailing', time: '', image: ''},
  {name: 'Skydiving', time: '', image: ''},
  {name: 'Sailing', time: '', image: ''},
  {name: 'Fishing', time: '', image: ''},
  {name: 'Tours, Lessons (Diving-Surfing)', time: '', image: ''},
  {name: 'Boards, Paddles', time: '', image: ''},
  {name: 'Rafting', time: '', image: ''},
];

export const D_CURRENCY_DATA = [
  {name: 'SAR Saudi Riyal', time: '', image: ''},
  {name: 'AED United Arab Emirated Dirham', time: '', image: ''},
  {name: 'KWD Kuwaiti Dinar', time: '', image: ''},
  {name: 'BHD Bahrini Dinar', time: '', image: ''},
  {name: 'USD US Dollar', time: '', image: ''},
  {name: 'EUR Euro', time: '', image: ''},
  {name: 'GBP British Pound Sterling', time: '', image: ''},
  {name: 'CHF Swiss Franc', time: '', image: ''},
];

export const D_ATTRACTION_SHOWS = [
  {name: '', time: '', image: IMAGES.attraction1},
  {name: 'Top Local\nAttraction', time: '', image: IMAGES.attraction2},
  {name: 'Exhibits Museum\nArchaelogy', time: '', image: IMAGES.attraction3},
  {name: 'Heritage', time: '', image: IMAGES.attraction4},
  {name: 'Parks & Garden', time: '', image: IMAGES.attraction5},
  {name: 'Natural\nLandscape', time: '', image: IMAGES.attraction6},
  {name: 'Environmental\nBeaches', time: '', image: IMAGES.attraction7},
  {name: 'Zoo Aquarium', time: '', image: IMAGES.attraction8},
  {name: 'Shooping\n(Markets & Malls)', time: '', image: IMAGES.attraction9},
  {name: 'Religious', time: '', image: IMAGES.attraction10},
  {name: 'Theaters', time: '', image: IMAGES.attraction11},
  {name: 'Show\nPerformances', time: '', image: IMAGES.attraction12},
  {name: 'Sports', time: '', image: IMAGES.attraction13},
  {name: 'Medical', time: '', image: IMAGES.attraction14},
  {name: 'Conferences\nBusiness', time: '', image: IMAGES.attraction15},
];

export const D_CUISINE_DATA = [
  {name: '', time: '', image: IMAGES.attraction1},
  {name: 'American', time: '', image: IMAGES.cuisine1},
  {name: 'Arabic', time: '', image: IMAGES.cuisine2},
  {name: 'Asian', time: '', image: IMAGES.cuisine3},
  {name: 'Brazilian', time: '', image: IMAGES.cuisine4},
  {name: 'Buffet', time: '', image: IMAGES.cuisine5},
  {name: 'Burger', time: '', image: IMAGES.cuisine6},
  {name: 'Chinese', time: '', image: IMAGES.cuisine7},
  {name: 'Egyptian', time: '', image: IMAGES.cuisine8},
  {name: 'European', time: '', image: IMAGES.cuisine9},
  {name: 'Fast Food', time: '', image: IMAGES.cuisine10},
  {name: 'Gulf Food', time: '', image: IMAGES.cuisine11},
  {name: 'Halal', time: '', image: IMAGES.cuisine12},
  {name: 'Indian', time: '', image: IMAGES.cuisine13},
  {name: 'International', time: '', image: IMAGES.cuisine14},
  {name: 'Italian', time: '', image: IMAGES.cuisine15},
  {name: 'Iranian', time: '', image: IMAGES.cuisine16},
  {name: 'Japanese', time: '', image: IMAGES.cuisine17},
  {name: 'Korean', time: '', image: IMAGES.cuisine18},
  {name: 'Lebanese', time: '', image: IMAGES.cuisine19},
  {name: 'Mediteranean', time: '', image: IMAGES.cuisine20},
  {name: 'Maxican', time: '', image: IMAGES.cuisine21},
  {name: 'Middle Eastern', time: '', image: IMAGES.cuisine22},
  {name: 'Morrocon', time: '', image: IMAGES.cuisine23},
  {name: 'Pizza', time: '', image: IMAGES.cuisine24},
  {name: 'Seafood', time: '', image: IMAGES.cuisine25},
  {name: 'Turkish', time: '', image: IMAGES.cuisine26},
  {name: 'Vegetarian', time: '', image: IMAGES.cuisine27},
];

export const D_ACTIVITY_DATA = [
  {name: '', time: '', image: IMAGES.attraction1},
  {name: 'Food Experiences', time: '', image: IMAGES.activity1},
  {name: 'Sports', time: '', image: IMAGES.activity2},
  {name: 'Classes\n& Workshops  ', time: '', image: IMAGES.activity3},
  {name: 'Air\nActivities', time: '', image: IMAGES.activity4},
  {name: 'Sea\nActivities', time: '', image: IMAGES.activity5},
  {name: 'Outdoor\nExperiences', time: '', image: IMAGES.activity6},
  {name: 'Indoor\nActivities', time: '', image: IMAGES.activity7},
  {name: 'Designed for\naccessebility', time: '', image: IMAGES.activity8},
];

export const D_CRUISE_SUPPLIER_DATA = [
  {name: '', time: '', image: IMAGES.attraction1},
  {name: 'AMA Waterways', time: '', image: IMAGES.cruise1},
  {name: 'American\nCruise Lines', time: '', image: IMAGES.cruise2},
  {name: 'American Queen\nStreamboat', time: '', image: IMAGES.cruise3},
  {name: 'A-Rosa\nCruises', time: '', image: IMAGES.cruise4},
  {name: 'Avalon\nWaterways', time: '', image: IMAGES.cruise5},
  {name: 'Azmara\nClub', time: '', image: IMAGES.cruise6},
  {name: 'Brahmas Paradise', time: '', image: IMAGES.cruise7},
  {name: 'Carnival\nCruise', time: '', image: IMAGES.cruise8},
  {name: 'Celibrity\nCruise', time: '', image: IMAGES.cruise9},
  {name: 'Celestyle\nCruises', time: '', image: IMAGES.cruise10},
  {name: 'Costa\nCruises', time: '', image: IMAGES.cruise11},
  {name: 'Crystal\nCruisesudia', time: '', image: IMAGES.cruise12},
  {name: 'Holland\nAmerica Line', time: '', image: IMAGES.cruise13},
  {name: 'Emerald\nWaterways', time: '', image: IMAGES.cruise14},
  {name: 'Cunard\nLine', time: '', image: IMAGES.cruise15},
  {name: 'Disney\nCruises', time: '', image: IMAGES.cruise16},
  {name: 'Hrutigruten\nCruise Line', time: '', image: IMAGES.cruise17},
  {name: 'Lindblad\nExpeditions', time: '', image: IMAGES.cruise18},
  {name: 'MSC\nCuises', time: '', image: IMAGES.cruise19},
  {name: 'Norwegian\nCruise Line', time: '', image: IMAGES.cruise20},
  {name: 'Oceania\nCruises', time: '', image: IMAGES.cruise21},
  {name: 'Paul\nGauguin Cruises', time: '', image: IMAGES.cruise22},
  {name: 'Ponant\nCruises', time: '', image: IMAGES.cruise23},
  {name: 'Princess\nCruises', time: '', image: IMAGES.cruise24},
  {name: 'Quark\nExpeditions', time: '', image: IMAGES.cruise25},
  {name: 'Ritz-Carlton\nYatch Cruises', time: '', image: IMAGES.cruise26},
  {name: 'Regent Seven\nSeas Cruises', time: '', image: IMAGES.cruise27},
  {name: 'Royal Caribbean\nCruises', time: '', image: IMAGES.cruise28},
  {name: 'Scenic\nCruises', time: '', image: IMAGES.cruise29},
  {name: 'Sea Dream\nYatch Club', time: '', image: IMAGES.cruise30},
  {name: 'Star Clipper\nCruise', time: '', image: IMAGES.cruise31},
  {name: 'Tauck Tours', time: '', image: IMAGES.cruise32},
  {name: 'Un-Cruise\nAdventures', time: '', image: IMAGES.cruise33},
  {name: 'Uniworld\nBOutique River', time: '', image: IMAGES.cruise34},
  {name: 'Viking\nRiver and Ocean\nCruise', time: '', image: IMAGES.cruise35},
  {name: 'Virgin\nVoyages', time: '', image: IMAGES.cruise36},
  {name: 'Silversea\nCruises', time: '', image: IMAGES.cruise37},
  {name: 'Seabourn\nCruise', time: '', image: IMAGES.cruise38},
  {name: 'Windstar\nCruises', time: '', image: IMAGES.cruise39},
];

export const D_CAR_SUPPLIER_DATA = [
  {name: '', time: '', image: IMAGES.attraction1},
  {name: '', time: '', image: IMAGES.carsupplier1},
  {name: '', time: '', image: IMAGES.carsupplier2},
  {name: '', time: '', image: IMAGES.carsupplier3},
  {name: '', time: '', image: IMAGES.carsupplier4},
  {name: '', time: '', image: IMAGES.carsupplier5},
  {name: '', time: '', image: IMAGES.carsupplier6},
  {name: '', time: '', image: IMAGES.carsupplier7},
  {name: '', time: '', image: IMAGES.carsupplier8},
  {name: '', time: '', image: IMAGES.carsupplier9},
  {name: '', time: '', image: IMAGES.carsupplier10},
  {name: '', time: '', image: IMAGES.carsupplier11},
  {name: '', time: '', image: IMAGES.carsupplier12},
  {name: '', time: '', image: IMAGES.carsupplier13},
  {name: '', time: '', image: IMAGES.carsupplier14},
  {name: '', time: '', image: IMAGES.carsupplier15},
  {name: '', time: '', image: IMAGES.carsupplier16},
  {name: '', time: '', image: IMAGES.carsupplier17},
  {name: '', time: '', image: IMAGES.carsupplier18},
  {name: '', time: '', image: IMAGES.carsupplier19},
];

export const D_AEROPLANE_SUPPLIER_DATA = [
  {name: '', time: '', image: IMAGES.attraction1},
  {name: 'Saudia', time: '', image: IMAGES.airline1},
  {name: 'Emirates', time: '', image: IMAGES.airline2},
  {name: 'Etihad', time: '', image: IMAGES.airline3},
  {name: 'Gulf Air', time: '', image: IMAGES.airline4},
  {name: 'Turkish', time: '', image: IMAGES.airline5},
  {name: 'Oman Air', time: '', image: IMAGES.airline6},
  {name: 'British Airways', time: '', image: IMAGES.airline7},
  {name: 'KLM\nRoyal Dutch', time: '', image: IMAGES.airline8},
  {name: 'Royal\nJordanian', time: '', image: IMAGES.airline9},
  {name: 'EgyptAir', time: '', image: IMAGES.airline10},
  {name: 'Air France', time: '', image: IMAGES.airline11},
  {name: 'Keny Airways', time: '', image: IMAGES.airline12},
  {name: 'Deutsche\nLufthansa', time: '', image: IMAGES.airline14},
  {name: 'Middle East\nAirlines', time: '', image: IMAGES.airline15},
  {name: 'Swiss\nInternational', time: '', image: IMAGES.airline16},
  {name: 'Ethiopian', time: '', image: IMAGES.airline17},
  {name: 'Pegasus', time: '', image: IMAGES.airline18},
  {name: 'Thai Airways', time: '', image: IMAGES.airline19},
  {name: 'Aeroflot', time: '', image: IMAGES.airline20},
  {name: 'Cathay Pacific', time: '', image: IMAGES.airline21},
  {name: 'Kuwait\nAirways', time: '', image: IMAGES.airline22},
  {name: 'United\nAirlines', time: '', image: IMAGES.airline23},
  {name: 'Singapore\nInternationals', time: '', image: IMAGES.airline24},
  {name: 'Philippine\nAirlines', time: '', image: IMAGES.airline25},
  {name: 'American\nAirlines', time: '', image: IMAGES.airline26},
  {name: 'Alitalia', time: '', image: IMAGES.airline27},
  {name: 'Other Airlines', time: '', image: IMAGES.airline28},
];

export const DAY = [
  {day: 'Mon', date: 1,isSelected:false},
  {day: 'Tue', date: 2,isSelected:false},
  {day: 'Wed', date: 3,isSelected:false},
  {day: 'Thu', date: 4,isSelected:false},
  {day: 'Fri', date: 5,isSelected:false},
  {day: 'Sat', date: 6,isSelected:false},
];

export const getSettingPrefs = (val) => {
  return {
    title:
        val == 1
            ? 'Select Language'
            : val == 2
            ? 'Select Currency'
            : val == 3
                ? 'Units'
                : val == 4
                    ? 'Temperature'
                    : val == 5
                        ? 'Time Format'
                        : 'Select Navigation App',
    modalVisible: true,
    label:
        val == 1
            ? 'languages'
            : val == 2
            ? 'currencies'
            : val == 3
                ? 'units'
                : val == 4
                    ? 'temperature'
        : val == 5
        ? 'timeFormat'
        : 'navigationApps',
    selectedPref: val,
  };
};
