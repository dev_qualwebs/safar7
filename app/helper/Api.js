import axios from 'axios';
import Auth from "../auth";
import { U_BASE, U_LOGIN } from './Constants';
import { Alert } from 'react-native';




const headers = async () => {
    const headers = {
        'Content-Type': 'application/json'
    };

    const auth = new Auth();
    const token = await auth.getValue(AS_USER_TOKEN);

    if (token) {
        headers['Authorization'] = `Bearer ${token}`;
    }
    return headers;
};

const request = async (method, path, body) => {

    const url = `${U_BASE}${path}`;
    const options = { method, url, headers: await headers() };

    if (body) {
        options.data = body;
    }
    return axios(options);
};

export default class API {
    loginAPI(data) {
        return request('POST', U_LOGIN, data);
	}
	
    // getAllTransactions() {
    //     return request('GET', U_GET_ALL_TRANSACTIONS)
    // }

    // getSingleTransction(data) {
    //     return request('GET', U_GET_SINGLE_TRANSACTION,data) 
    // }
}