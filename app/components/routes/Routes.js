/* eslint-disable prettier/prettier */
/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createStackNavigator} from '@react-navigation/stack';
import {NavigationContainer, DefaultTheme} from '@react-navigation/native';
import {
  View,
  Text,
  TouchableOpacity,
  SafeAreaView,
  Image,
  StyleSheet,
} from 'react-native';
import {widthPercentageToDP} from 'react-native-responsive-screen';
import COLOR from '../styles/Color';
import InitialLoadingScreen from '../../components/splash/InitialLoadingScreen';
import FONTS from '../styles/Fonts';
import SplashScreen from '../../components/splash/SplashView';
import LoginScreen from '../registration/LoginScreen';
import MobileVerification from '../registration/MobileVerification';
import ForgotPassword from '../registration/ForgotPassword';
import NewPassword from '../registration/NewPassword';
import Apps from '../registration/Signup';
import AlertModal from '../../models/AlertModal';
import HomeScreen from '../tabs/home/HomeScreen';
import IMAGES from '../styles/Images';
import MessageScreen from '../tabs/message/MessageScreen';
import PackageScreen from '../tabs/package/PackageScreen';
import ProfileScreen from '../tabs/profile/ProfileScreen';
import TripDetail from '../tabs/home/trip/TripDetail';
import NotificationScreen from '../notification/NotificationScreen';
import NotificationSettings from '../notification/NotificationSettings';
import SingleTrip from '../tabs/home/trip/SingleTrip';
import AddNewTrip from '../tabs/home/trip/AddNewTrip';
import PastTrips from '../tabs/home/trip/PastTrips';
import UpcomingTrips from '../tabs/home/trip/UpcomingTrips';
import AddNewReservation from '../tabs/home/trip/AddNewReservation';
import AddPlace from '../tabs/home/trip/AddPlace';
import BookTicket from '../tabs/home/trip/BookTicket';
import AddGuest from '../tabs/home/trip/AddGuest';
import {NoRequestsView} from '../tabs/message/NoRequestsView';
import UpcomingRequest from '../tabs/message/UpcomingRequest';
import PastRequest from '../tabs/message/PastRequest';
import HelpTrip from '../tabs/home/trip/HelpTrip';
import PlanNewTrip from '../tabs/message/PlanNewTrip';
import SelectDate from '../tabs/calendar/SelectDate';
import NoticeScreen from '../tabs/message/NoticeScreen';
import PlanTripSplash from '../tabs/message/PlanTripSplash';
import BookFlight from '../tabs/bookFlight/BookFlight';
import OneWayFlight from '../tabs/bookFlight/OneWayFlight';
import SearchPlace from '../tabs/bookFlight/SearchPlace';
import FlightOption from '../tabs/bookFlight/FlightOption';
import DepartureTime from '../tabs/bookFlight/DepartureTime';
import ReservationPolicy from '../tabs/bookFlight/ReservationPolicy';
import PreferedClass from '../tabs/bookFlight/PreferedClass';
import SeatingPreferences from '../tabs/bookFlight/SeatingPreference';
import Equipments from '../tabs/bookFlight/Equipments';
import StayScreen from '../tabs/stay/StayScreen';
import StayOption from '../tabs/stay/StayOption';
import PropertyType from '../tabs/stay/PropertyType';
import EditStay from '../tabs/stay/EditStay';
import Rooms from '../tabs/stay/Rooms';
import AddRoom from '../tabs/stay/AddRoom';
import ServicesAndFacilities from '../tabs/stay/ServicesAndFacilities';
import TrainScreen from '../tabs/train/TrainScreen';
import AddTrain from '../tabs/train/AddTrain';
import TrainOptions from '../tabs/train/TrainOptions';
import TrainClass from '../tabs/train/TrainClass';
import TrainSeatingOption from '../tabs/train/TrainSeatingOption';
import CarScreen from '../tabs/car/CarScreen';
import SameDropoff from '../tabs/car/SameDropoff';
import DifferentDropoff from '../tabs/car/DifferentDropoff';
import AddCar from '../tabs/car/AddCar';
import CarOption from '../tabs/car/CarOption';
import CarType from '../tabs/car/CarType';
import SpecificationCar from '../tabs/car/SpecificationCar';
import CarExtra from '../tabs/car/CarExtra';
import CarFinancialResponsibility from '../tabs/car/CarFinancialResponsibilities';
import CruiseScreen from '../tabs/cruise/CruiseScreen';
import CruiseOption from '../tabs/cruise/CruiseOption';
import CabinType from '../tabs/cruise/CruiseType';
import CruiseFeatures from '../tabs/cruise/CruiseFeatures';
import TransferScreen from '../tabs/transfer/TransferScreen';
import AddnewTransfer from '../tabs/transfer/AddNewTransfer';
import TransferBetween from '../tabs/transfer/TransferBetween';
import TripPlanScreen from '../tabs/tripPlan/TripPlanScreen';
import Destinations from '../tabs/tripPlan/Destinations';
import AddNewCity from '../tabs/tripPlan/AddNewCity';
import TripPlanOption from '../tabs/tripPlan/TripPlanOption';
import TourismType from '../tabs/tripPlan/TourismType';
import CreditRewards from '../tabs/profile/CreditReward';
import ReferFriend from '../tabs/profile/ReferFriend';
import PersonalDetail from '../tabs/profile/PersonalDetail';
import PassportDetail from '../tabs/profile/PassportDetail';
import ProfileDetail from '../tabs/profile/ProfileDetail';
import SpecialNeeds from '../tabs/profile/SpecialNeed';
import NameGuideline from '../tabs/profile/NameGuidelines';
import ProfileTraveller from '../tabs/profile/ProfileTraveller';
import ProfileSetting from '../tabs/profile/ProfileSetting';
import ProfileDataSharing from '../tabs/profile/ProfileDataSharing';
import SelectCurrency from '../tabs/profile/SelectCurrency';
import ProfileSupport from '../tabs/profile/ProfileSupport';
import PaymentAddon from '../tabs/payments/PaymentAddon';
import PaymentScreen from '../tabs/payments/PaymentScreen';
import ChoosePaymentMethod from '../tabs/payments/ChoosePaymentMethod';
import AddCard from '../tabs/payments/AddCard';
import BankTransfer from '../tabs/payments/BankTransfer';
import BankTransferSuccess from '../tabs/payments/BankTransferSuccess';
import TripReady from '../tabs/payments/TripReady';
import TransportationScreen from '../tabs/transportation/TransportationScreen';
import AddTransportation from '../tabs/transportation/AddTransportation';
import TransportationAddress from '../tabs/transportation/TransportationAddress';
import AttractionShow from '../tabs/tripPlan/AttractionShows';
import CuisineType from '../tabs/tripPlan/CuisineType';
import ActivityExperience from '../tabs/tripPlan/ActivitiesExperience';
import CruiseSupplier from '../tabs/cruise/CruiseSupplier';
import CarSupplier from '../tabs/car/CarSupplier';
import Airlines from '../tabs/bookFlight/Airlines';
import DiscountScreen from '../tabs/home/DiscountScreen';
import AllPackages from '../tabs/package/AllPackages';
import OfferScreen from '../tabs/home/OfferScreen';
import SinglePackageDetail from '../tabs/package/SinglePackageDetail';
import Styles from '../styles/Styles';
import PackageSelection from '../tabs/package/PackageSelection';
import TripAddon from '../tabs/home/trip/TripAddon';
import Checklist from '../tabs/home/trip/Checklist';
import PackageHightlights from '../tabs/package/PackageHighlights';
import PackageServices from '../tabs/package/PackagesServices';
import PackagePlans from '../tabs/package/PackagePlans';
import PackageDetails from '../tabs/package/PackageDetails';
import AddNewStay from '../tabs/stay/AddNewStay';
import AddNewFlight from '../tabs/bookFlight/AddNewFlight';
import AddNewCruise from '../tabs/cruise/AddNewCruise';
import TrainPolicies from '../tabs/train/TrainPolicies';
import TrainExtra from '../tabs/train/TrainExtra';
import CarReservationPolicy from '../tabs/car/CarReservationPolicy';
import StayPolicy from '../tabs/stay/StayPolicy';
import SignUpScreen from '../registration/SignUpScreen';
import GooglePlacesInput from '../commonView/GooglePlaces';
import TripTimeline from '../tabs/home/trip/TripTimeline';
import SingleTripScreen from '../tabs/home/trip/SingleTripScreen';
import BookedPackage from '../tabs/package/BookedPackage';
import AddNewMultipleFlight from '../tabs/bookFlight/AddNewMultipleFlight';
import NewReservationPackage from '../tabs/package/NewReservationPackage';
import RequestedPackagesScreen from '../tabs/requestedPackages/RequestedPackagesScreen';
import {setNavigationRef} from './RootNavigation';
import PaymentSecond from '../tabs/payments/PaymentSecond';
import PackagePayment from '../tabs/package/PackagePayment';
import SuccessScreen from '../tabs/payments/SuccessScreen';
import {PaymentCompleteScreen} from '../tabs/payments/PaymentCompleteScreen';
import TravellersDetail from '../tabs/profile/TravellersDetail';
import ExperienceScreen from '../tabs/tripPlan/ExperienceScreen';
import TravellersSelection from '../tabs/addons/TravellersSelection';
import TravellerInsurance from '../tabs/addons/TravellerInsurance';
import InsuranceAddress from '../tabs/addons/InsuranceAddress';
import TravelVisa from '../tabs/addons/TravelVisa';
import TravellersVisaDetails from '../tabs/addons/TravellersVisaDetails';
import InternationalDrivingLicense from '../tabs/addons/InternationalDrivingLicense';
import VisaFinalStep from '../tabs/addons/VisaFinalStep';
import VisaSponsor from '../tabs/addons/VisaSponsor';
import TravellerInsuranceSecondary from '../tabs/addons/TravellerInsuranceSecondary';
import PdfView from '../tabs/payments/PdfView';
import PolicyPage from '../tabs/home/trip/PolicyPage';
import TripSummary from '../tabs/payments/TripSummary';
import HelpDetails from '../tabs/home/trip/HelpDetails';
import ScanDocument from '../tabs/profile/ScanDocument';
import CompleteProfile from '../tabs/profile/CompleteProfile';
import TripStatusView from '../tabs/message/TripStatusView';
import RejectedTripDetails from '../tabs/message/RejectedTripDetails';
import EmergencyHelpView from '../tabs/home/trip/EmergencyHelpView';
import EmergencyMainView from '../tabs/home/trip/EmergencyMainView';
import ForgotPasswordMiddleScreen from '../registration/ForgotPasswordMiddleScreen';
import MapViewWithDates from '../tabs/package/MapViewWithDates';
import AdminChatScreen from '../tabs/home/AdminChatScreen';
import GooglePlacesImagesInput from '../commonView/GooglePlacesImages';

const BottonStack = createBottomTabNavigator();
const Stack = createStackNavigator();
const images = [
  IMAGES.home_image,
  IMAGES.message_image,
  IMAGES.bag_image,
  // IMAGES.calendar_arrow,
  IMAGES.profile,
];

export function MyTabBar({state, descriptors, navigation}) {
  return (
    <View style={style.bottom_tab_container}>
      {state.routes.map((route, index) => {
        console.log('route is', route);
        const {options} = descriptors[route.key];
        const label =
          options.tabBarLabel !== undefined
            ? options.tabBarLabel
            : options.title !== undefined
            ? options.title
            : route.name;
        const isFocused = state.index === index;

        const onPress = () => {
          const event = navigation.emit({
            type: 'tabPress',
            target: route.key,
          });

          if (!isFocused && !event.defaultPrevented) {
            navigation.navigate(route.name);
          }
        };

        const onLongPress = () => {
          navigation.emit({
            type: 'tabLongPress',
            target: route.key,
          });
        };

        return (
          <TouchableOpacity
            accessibilityRole="button"
            accessibilityStates={isFocused ? ['selected'] : []}
            accessibilityLabel={options.tabBarAccessibilityLabel}
            testID={options.tabBarTestID}
            onPress={onPress}
            onLongPress={onLongPress}
            style={style.bottom_tab_text}>
            <View
              style={{
                height: 60,
                justifyContent: 'center',
                alignItems: 'center',
                alignSelf: 'center',
                flex: 1,
              }}>
              <Image
                style={{
                  width: 25,
                  height: 25,
                  alignItems: 'center',
                  alignSelf: 'center',
                  tintColor: isFocused ? COLOR.GREEN : COLOR.VOILET,
                }}
                resizeMode={'cover'}
                source={images[index]}
              />
              <View
                style={[
                  {
                    width: 40,
                    bottom: 0,
                    height: 2,
                    position: 'absolute',
                    backgroundColor: isFocused ? COLOR.GREEN : COLOR.WHITE,
                  },
                ]}
              />
            </View>
          </TouchableOpacity>
        );
      })}
    </View>
  );
}

const MyTheme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    background: '#ffffff',
  },
};

export default function RootStack(props) {
  return (
    <NavigationContainer ref={(ref) => setNavigationRef(ref)} theme={MyTheme}>
      <Stack.Navigator initialRouteName={'Trip'} headerMode="none">
        <Stack.Screen
          name="InitialLoadingScreen"
          component={InitialLoadingScreen}
        />
        <Stack.Screen name="PolicyPage" component={PolicyPage} />
        <Stack.Screen name="GooglePlaces" component={GooglePlacesInput} options={{gestureEnabled: false}}/>
        <Stack.Screen name="GooglePlacesImages" component={GooglePlacesImagesInput} options={{gestureEnabled: false}}/>
        <Stack.Screen name="SplashScreen" component={SplashScreen} />
        <Stack.Screen name="LoginScreen" component={LoginScreen} />
        <Stack.Screen
          name="MobileVerification"
          component={MobileVerification}
        />
        <Stack.Screen name="SignUpScreen" component={SignUpScreen} />
        <Stack.Screen name="ForgotPassword" component={ForgotPassword} />
        <Stack.Screen name="HomeStack" component={HomeStack} />
        <Stack.Screen name="AllPackages" component={AllPackages} />
        <Stack.Screen name="PackageSelection" component={PackageSelection} />
        <Stack.Screen
          name="PackageHightlights"
          component={PackageHightlights}
        />
        <Stack.Screen name="PackageServices" component={PackageServices} />
        <Stack.Screen name="PackagePlans" component={PackagePlans} />
        <Stack.Screen name="PackageDetails" component={PackageDetails} />
        <Stack.Screen
          name="SinglePackageDetail"
          component={SinglePackageDetail}
        />
        <Stack.Screen name="PackageScreen" component={PackageScreen} />

        <Stack.Screen
          name="TransportationScreen"
          component={TransportationScreen}
        />
        <Stack.Screen name="AddTransportation" component={AddTransportation} />
        <Stack.Screen
          name="TransportationAddress"
          component={TransportationAddress}
        />
        <Stack.Screen name="DiscountScreen" component={DiscountScreen} />
        <Stack.Screen name="MapViewWithDates" component={MapViewWithDates} />
        <Stack.Screen name="Airlines" component={Airlines} />
        <Stack.Screen name="CruiseSupplier" component={CruiseSupplier} />
        <Stack.Screen name="CarSupplier" component={CarSupplier} />
        <Stack.Screen name="OfferScreen" component={OfferScreen} />
        <Stack.Screen name="TripStatusView" component={TripStatusView} />
        <Stack.Screen
          name="RejectedTripDetails"
          component={RejectedTripDetails}
        />

        <Stack.Screen
          name="ActivityExperience"
          component={ActivityExperience}
        />
        <Stack.Screen name="CuisineType" component={CuisineType} />

        <Stack.Screen name="UpcomingTrips" component={UpcomingTrips} />
        <Stack.Screen name="PastTrips" component={PastTrips} />
        <Stack.Screen name="SingleTrip" component={SingleTrip} />

        <Stack.Screen name="TripDetail" component={TripDetail} />
        <Stack.Screen name="TripTimeline" component={TripTimeline} />
        <Stack.Screen name="SingleTripScreen" component={SingleTripScreen} />

        <Stack.Screen name="Apps" component={Apps} />

        <Stack.Screen name="AlertModal" component={AlertModal} />
        <Stack.Screen name="NewPassword" component={NewPassword} />

        <Stack.Screen
          name="NotificationSettings"
          component={NotificationSettings}
        />
        <Stack.Screen
          name="NotificationScreen"
          component={NotificationScreen}
        />
        <Stack.Screen name="AddNewTrip" component={AddNewTrip} />
        <Stack.Screen name="AddNewReservation" component={AddNewReservation} />
        <Stack.Screen name="AddPlace" component={AddPlace} />
        <Stack.Screen name="BookTicket" component={BookTicket} />
        <Stack.Screen name="AddGuest" component={AddGuest} />
        <Stack.Screen name="NoRequestsView" component={NoRequestsView} />
        <Stack.Screen name="UpcomingRequest" component={UpcomingRequest} />
        <Stack.Screen name="PastRequest" component={PastRequest} />
        <Stack.Screen name="HelpTrip" component={HelpTrip} />
        <Stack.Screen name="PlanNewTrip" component={PlanNewTrip} />
        <Stack.Screen name="SelectDate" component={SelectDate} />
        <Stack.Screen name="NoticeScreen" component={NoticeScreen} />
        <Stack.Screen name="PlanTripSplash" component={PlanTripSplash} />
        <Stack.Screen name="BookFlight" component={BookFlight} />
        <Stack.Screen name="OneWayFlight" component={OneWayFlight} />
        <Stack.Screen
          name="AddNewMultipleFlight"
          component={AddNewMultipleFlight}
        />
        <Stack.Screen name="SearchPlace" component={SearchPlace} />
        <Stack.Screen name="AddNewFlight" component={AddNewFlight} />
        <Stack.Screen name="FlightOption" component={FlightOption} />
        <Stack.Screen name="DepartureTime" component={DepartureTime} />
        <Stack.Screen name="Equipments" component={Equipments} />

        <Stack.Screen
          name="SeatingPreferences"
          component={SeatingPreferences}
        />
        <Stack.Screen name="ReservationPolicy" component={ReservationPolicy} />
        <Stack.Screen name="PreferedClass" component={PreferedClass} />

        <Stack.Screen name="StayScreen" component={StayScreen} />
        <Stack.Screen name="StayPolicy" component={StayPolicy} />
        <Stack.Screen name="AddNewStay" component={AddNewStay} />
        <Stack.Screen name="StayOption" component={StayOption} />
        <Stack.Screen name="PropertyType" component={PropertyType} />
        <Stack.Screen name="EditStay" component={EditStay} />
        <Stack.Screen name="Rooms" component={Rooms} />
        <Stack.Screen name="AddRoom" component={AddRoom} />
        <Stack.Screen
          name="ServicesAndFacilities"
          component={ServicesAndFacilities}
        />

        <Stack.Screen name="TrainScreen" component={TrainScreen} />
        <Stack.Screen name="AddTrain" component={AddTrain} />
        <Stack.Screen name="TrainOptions" component={TrainOptions} />
        <Stack.Screen name="TrainClass" component={TrainClass} />
        <Stack.Screen name="TrainPolicies" component={TrainPolicies} />
        <Stack.Screen name="TrainExtra" component={TrainExtra} />
        <Stack.Screen
          name="TrainSeatingOption"
          component={TrainSeatingOption}
        />

        <Stack.Screen name="CompleteProfile" component={CompleteProfile} />
        <Stack.Screen name="CarScreen" component={CarScreen} />
        <Stack.Screen name="SameDropoff" component={SameDropoff} />
        <Stack.Screen name="DifferentDropoff" component={DifferentDropoff} />
        <Stack.Screen name="AddCar" component={AddCar} />
        <Stack.Screen name="CarOption" component={CarOption} />
        <Stack.Screen name="CarType" component={CarType} />
        <Stack.Screen name="SpecificationCar" component={SpecificationCar} />
        <Stack.Screen
          name="CarFinancialResponsibility"
          component={CarFinancialResponsibility}
        />
        <Stack.Screen name="CarExtra" component={CarExtra} />
        <Stack.Screen
          name="CarReservationPolicy"
          component={CarReservationPolicy}
        />

        <Stack.Screen name="AddNewCruise" component={AddNewCruise} />
        <Stack.Screen name="CruiseScreen" component={CruiseScreen} />
        <Stack.Screen name="CruiseOption" component={CruiseOption} />
        <Stack.Screen name="CabinType" component={CabinType} />
        <Stack.Screen name="CruiseFeatures" component={CruiseFeatures} />

        <Stack.Screen name="TransferScreen" component={TransferScreen} />
        <Stack.Screen name="AddnewTransfer" component={AddnewTransfer} />
        <Stack.Screen name="TransferBetween" component={TransferBetween} />

        <Stack.Screen name="TripPlanScreen" component={TripPlanScreen} />
        <Stack.Screen name="TripAddon" component={TripAddon} />
        <Stack.Screen name="Destinations" component={Destinations} />
        <Stack.Screen name="AddNewCity" component={AddNewCity} />
        <Stack.Screen name="TripPlanOption" component={TripPlanOption} />
        <Stack.Screen name="TourismType" component={TourismType} />
        <Stack.Screen name="CreditRewards" component={CreditRewards} />
        <Stack.Screen name="Checklist" component={Checklist} />
        <Stack.Screen name="TravellersDetail" component={TravellersDetail} />
        <Stack.Screen name="ExperienceScreen" component={ExperienceScreen} />
        <Stack.Screen name="ReferFriend" component={ReferFriend} />
        <Stack.Screen name="PersonalDetail" component={PersonalDetail} />
        <Stack.Screen name="PassportDetail" component={PassportDetail} />
        <Stack.Screen name="ProfileDetail" component={ProfileDetail} />

        <Stack.Screen name="SpecialNeeds" component={SpecialNeeds} />
        <Stack.Screen name="NameGuideline" component={NameGuideline} />
        <Stack.Screen name="ProfileTraveller" component={ProfileTraveller} />
        <Stack.Screen name="ProfileSetting" component={ProfileSetting} />
        <Stack.Screen
          name="ProfileDataSharing"
          component={ProfileDataSharing}
        />
        <Stack.Screen name="SelectCurrency" component={SelectCurrency} />
        <Stack.Screen name="ProfileSupport" component={ProfileSupport} />

        <Stack.Screen name="PaymentAddon" component={PaymentAddon} />
        <Stack.Screen name="PaymentScreen" component={PaymentScreen} />
        <Stack.Screen name="AddCard" component={AddCard} />
        <Stack.Screen name="BankTransfer" component={BankTransfer} />
        <Stack.Screen name="TripReady" component={TripReady} />
        <Stack.Screen name="ForgotPasswordMiddleScreen" component={ForgotPasswordMiddleScreen} />

        <Stack.Screen
          name="BankTransferSuccess"
          component={BankTransferSuccess}
        />
        <Stack.Screen
          name="ChoosePaymentMethod"
          component={ChoosePaymentMethod}
        />
        <Stack.Screen name="AttractionShow" component={AttractionShow} />
        <Stack.Screen name="BookedPackage" component={BookedPackage} />
        <Stack.Screen
          name={'NewReservationPackage'}
          component={NewReservationPackage}
        />
        <Stack.Screen name={'PaymentSecond'} component={PaymentSecond} />
        <Stack.Screen name={'PackagePayment'} component={PackagePayment} />
        <Stack.Screen name={'SuccessScreen'} component={SuccessScreen} />
        <Stack.Screen
          name={'PaymentCompleteScreen'}
          component={PaymentCompleteScreen}
        />
        <Stack.Screen
          name="TravellersSelection"
          component={TravellersSelection}
        />
        <Stack.Screen
          name="TravellerInsurance"
          component={TravellerInsurance}
        />
        <Stack.Screen name="InsuranceAddress" component={InsuranceAddress} />
        <Stack.Screen name="TravelVisa" component={TravelVisa} />
        <Stack.Screen
          name="TravellersVisaDetails"
          component={TravellersVisaDetails}
        />
        <Stack.Screen
          name="InternationalDrivingLicense"
          component={InternationalDrivingLicense}
        />
        <Stack.Screen name="VisaFinalStep" component={VisaFinalStep} />
        <Stack.Screen name="VisaSponsor" component={VisaSponsor} />
        <Stack.Screen
          name="TravellerInsuranceSecondary"
          component={TravellerInsuranceSecondary}
        />
        <Stack.Screen name="PdfView" component={PdfView} />
        <Stack.Screen name="HelpDetails" component={HelpDetails} />
        <Stack.Screen name="EmergencyHelpView" component={EmergencyHelpView} />
        <Stack.Screen name="EmergencyMainView" component={EmergencyMainView} />
        <Stack.Screen name="AdminChatScreen" component={AdminChatScreen} />
        {/* <Stack.Screen name="ScanDocument" component={ScanDocument} /> */}
      </Stack.Navigator>
    </NavigationContainer>
  );
}

function HomeStack() {
  return (
    <BottonStack.Navigator
      style={{backgroundColor: '#ffffff'}}
      tabBar={(props) => <MyTabBar {...props} />}>
      <BottonStack.Screen key={1} name="Home" component={HomeScreen} />
      <BottonStack.Screen key={2} name="Message" component={MessageScreen} />
      <BottonStack.Screen
        key={3}
        name="Trip"
        component={TripDetail}
        initialParams={{packageTab: false}}
      />
      {/* <BottonStack.Screen
        key={4}
        name="BookedPackages"
        component={RequestedPackagesScreen}
        initialParams={{packageTab: true}}
      /> */}
      <BottonStack.Screen key={4} name="Profile" component={ProfileScreen} />
    </BottonStack.Navigator>
  );
}

const style = StyleSheet.create({
  bottom_tab_container: {
    flexDirection: 'row',
    height: 50,
    alignSelf: 'center',
    width: widthPercentageToDP(90),
    marginBottom: 20,
    justifyContent: 'center',
    backgroundColor: '#ffffff',
    borderRadius: 10,
    shadowColor: COLOR.VOILET,
    shadowOffset: {width: 1, height: 1},
    shadowOpacity: 0.4,
    shadowRadius: 5,
    elevation: 5,
    alignItems: 'center',
  },
  bottom_tab_text: {
    flex: 1,
  },
});
