
export let navigationRef;

export const setNavigationRef = (navigator) => {
    navigationRef = navigator;
}

export function navigate(name, params) {
    if (navigationRef.isReady()) {
        navigationRef.navigate(name, params);
    }
}

export function goBack() {
    navigationRef.goBack();
}