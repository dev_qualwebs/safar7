import React from 'react';
import {StyleSheet, View, TextInput, Text} from 'react-native';
import {widthPercentageToDP} from 'react-native-responsive-screen';
import COLOR from '../styles/Color';
import FONTS from '../styles/Fonts';
import { DoneButtonKeyboard } from './CommonView';

export default function CustomTextInput({
  label,
  value,
  updateState,
  number = false,
}) {
  return (
    <View style={styles.borderView}>
      <Text style={styles.label}>{label}</Text>
      <TextInput
      inputAccessoryViewID={'uniqueID'}
        value={value}
        keyboardType={number ? 'numeric' : 'default'}
        style={styles.value}
        onChangeText={(text) => {
          updateState(text);
        }}
      />
      <DoneButtonKeyboard />
    </View>
  );
}

const styles = StyleSheet.create({
  borderView: {
    borderRadius: 10,
    borderWidth: 1,
    width: widthPercentageToDP(90),
    borderColor: '#C0C7CB',
    alignSelf: 'center',
    paddingHorizontal: 10,
    paddingVertical: 15,
    marginVertical: 10,
  },
  label: {
    fontFamily: FONTS.FAMILY_REGULAR,
    fontSize: 12,
    color: '#C0C7CB',
  },
  value: {
    fontSize: 16,
    fontFamily: FONTS.FAMILY_SEMIBOLD,
    fontWeight: '600',
  },
});
