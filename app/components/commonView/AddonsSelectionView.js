import React from 'react';
import {
  StyleSheet,
  View,
  TextInput,
  Text,
  Image,
  TouchableOpacity,
} from 'react-native';
import {widthPercentageToDP} from 'react-native-responsive-screen';
import COLOR from '../styles/Color';
import FONTS from '../styles/Fonts';

export default function AddonsSelectionView({label, onPress, selected}) {
  return (
    <TouchableOpacity
      onPress={onPress}
      style={[
        styles.borderView,
        {borderColor: selected ? COLOR.GREEN : '#C0C7CB'},
      ]}>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'center',
          alignSelf: 'center',
        }}>
        <Image
          source={require('../styles/assets/chain.png')}
          style={{
            width: 18,
            height: 18,
            paddingHorizontal: 15,
          }}
          resizeMode={'contain'}
        />
        <Text style={styles.value}>{label}</Text>
      </View>
      <Image
        source={require('../styles/assets/rArrow.png')}
        style={{
          width: 15,
          height: 15,
          paddingHorizontal: 15,
        }}
        resizeMode={'contain'}
      />
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  borderView: {
    borderRadius: 10,
    borderWidth: 1,
    height: 60,
    width: widthPercentageToDP(90),
    borderColor: '#C0C7CB',
    alignSelf: 'center',
    padding: 10,
    marginVertical: 10,
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
  },

  value: {
    fontSize: 16,
    fontFamily: FONTS.FAMILY_SEMIBOLD,
    fontWeight: '600',
  },
});
