import React from 'react';
import {
  View,
  StyleSheet,
  Modal,
  Alert,
  Image,
  Button,
  TouchableOpacity,
} from 'react-native';

import IMAGES from '../res/styles/Images';
import Styles from '../res/styles/Styles';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
  heightPercentageToDP,
} from 'react-native-responsive-screen';
import COLOR from '../res/styles/Color';
import FONTS from '../res/styles/Fonts';
import {FloatingTitleTextInputField} from '../commonView/FloatingTextField';
import {CustomButton} from '../commonView/CustomButton';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import Gallery from 'react-native-image-gallery';

export default GalleryView = (props) => {
  return (
    <Modal
      animationType={'slide'}
      transparent={true}
      visible={props.modalVisible}
      onRequestClose={() => {
        console.log('Modal has been closed.');
      }}>
      <View style={styles.modal}>
        <View style={styles.modalBody}>
          <TouchableOpacity
            style={{
              alignItems: 'center',
              justifyContent: 'center',
              flex: 1,
            }}
            onPress={() => {
              props.handleGalleryView();
            }}>
            <View
              style={{
                width: wp(100),
                height: heightPercentageToDP(50),
                backgroundColor: COLOR.WHITE,
                alignSelf: 'center',
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <Gallery
                style={{
                  backgroundColor: '#ffffff',
                  flex: 1,
                }}
                images={props.data.map((val) => {
                  return {source: {uri: val}};
                })}
              />
            </View>
          </TouchableOpacity>
        </View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  modal: {
    flex: 1,

    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  modalBody: {
    justifyContent: 'center',
    backgroundColor: '#00000050',
    alignItems: 'center',
    alignSelf: 'center',
    flex: 1,
  },
});

//  handleGalleryView() {
//     this.setState({isGalleryViewVisible: false});
//   }

//   <GalleryView
//           data={this.state.inspectionData.files}
//           handleGalleryView={this.handleGalleryView}
//           modalVisible={this.state.isGalleryViewVisible}
//         />
