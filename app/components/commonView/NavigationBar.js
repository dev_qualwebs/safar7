'use strict';

import React, { useState } from 'react';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scrollview';
import {
  Text,
  View,
  SafeAreaView,
  TouchableOpacity,
  Alert,
  TextInput,
  StyleSheet,
  Image,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import COLOR from '../styles/Color';
import Styles from '../styles/Styles';
import IMAGES from '../styles/Images';
import FONTS from '../styles/Fonts';

const NavigationBar = (props) => {
  return (
    <View style={{ marginBottom: 0 }}>
      <View
        style={[
          Styles.mainView,
          {
            height: 50,
            justifyContent: 'space-between',
            alignSelf: 'center',
            alignItems: 'center',
            flexDirection: 'row',
            width: wp(95),
          },
        ]}>
        <TouchableOpacity
          style={{
            height: 50,
            width: 45,
            justifyContent: 'center',
            alignItems: 'center',
          }}
          onPress={() => {

            { props.handleBack? props.handleBack(): props.isModal ? props.isModal.handleModalVisibility() : props.prop.navigation.goBack() }

          }}>
          {!props.isMainHeading && (
            <Image
              style={{
                height: props.left_image ? 15 : 25,
                width: props.left_image ? 15 : 25,
              }}
              source={props.left_image ? props.left_image : IMAGES.close}
            />
          )}
        </TouchableOpacity>
        <View>
          {!props.dontshowTitleAboveImage && !(props.isMainHeading || props.left_image == IMAGES.back_arrow) && (

            <View
              style={{
                width: 40,
                height: 5,
                top: 5,
                position: 'absolute',
                alignSelf: 'center',
                justifyContent: 'center',
                borderRadius: 2.5,
                backgroundColor: '#c4cdd2',
              }}
            />
          )}
          <Text
            style={[
              props.isMainHeading
                ? Styles.splash_heading_label
                : Styles.subheading_label,
              {
                  textAlign: 'center',
                  color: props.isMainHeading ? COLOR.GREEN : COLOR.BLACK,
                  fontSize: props.isMainHeading ? FONTS.EXTRA_LARGE : 20,
                  fontFamily: props.isMainHeading ? FONTS.FAMILY_EXTRA_BOLD : FONTS.FAMILY_BOLD,
                  width: null,
                  flex: 1,
                  marginTop: props.isMainHeading ? 0 : 10,
              },
            ]}>
              {props.name}
          </Text>
            {props.subtitle ? <Text
                style={[
                    Styles.small_label,
                    {width: null, textAlign: 'center', margin: 0},
                ]}>
                {props.subtitle}
            </Text> : null}
        </View>
          <TouchableOpacity
              onPress={() => {
                  props.right_image == IMAGES.notification_bell
                      ? props.prop.navigation.navigate('NotificationScreen')
                      : props.right_image == IMAGES.help ?
                      props.prop.navigation.navigate('HelpTrip') : props.isFunctionCall ? props.settingAction() : props.prop.navigation.navigate(props.rightNav ? props.rightNav : '')

              }}>
              <View
            style={{
              alignItems: 'center',
              justifyContent: 'flex-start',
              alignSelf: 'center',
              height: 50,
              width: 45,
            
            }}>
            {props.right_image && (
              <Image
                source={props.right_image}
                 resizeMode={'contain'}
                style={{
                  width: props.rightImageWidth ? props.rightImageWidth : 20,
                  height: props.rightImageHeight ? props.rightImageHeight : 25,
                 marginTop:10
                }}
              />
            )}
          </View>
        </TouchableOpacity>
      </View>
      <View
        style={{
          borderBottomColor: COLOR.GRAY,
          borderBottomWidth: 1,
        }}
      />
    </View>
  );
};
const CELL_COUNT = 4;

const styles = StyleSheet.create({
  codeFieldRoot: {
    marginTop: 50,
    width: wp(70),
    marginLeft: 'auto',
    marginRight: 'auto',
  },
  cellRoot: {
    width: 60,
    height: 60,
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomColor: '#ccc',
    borderBottomWidth: 4,
  },
  cellText: {
    color: COLOR.GREEN,
    fontSize: 36,
    textAlign: 'center',
  },
  focusCell: {
    borderBottomColor: COLOR.GREEN,
    borderBottomWidth: 4,
  },
});

export default NavigationBar;
