import React, {useEffect, useState} from 'react';
import {
  Modal,
  Text,
  View,
  StyleSheet,
  Image,
  FlatList,
  TouchableOpacity,
  TouchableWithoutFeedback,
  TextInput,
} from 'react-native';
import { KeyboardAwareFlatList } from 'react-native-keyboard-aware-scroll-view';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import COLOR from '../../components/styles/Color';
import FONTS from '../../components/styles/Fonts';
import IMAGES from '../../components/styles/Images';
import Styles from '../../components/styles/Styles';
import NavigationBar from '../commonView/NavigationBar';
import { DoneButtonKeyboard } from './CommonView';
import Switch from './Switch';

export const MultiSelectView = (props) => {
  const [selectedArray, setSelectedArray] = useState(props.selectedItem);
  const [seatTogether, setSeatTogether] = useState(props.seatTogether);

  useEffect(() => {
    setSelectedArray(props.selectedItem);
    console.log(props.selectedItem);
  }, [props.selectedItem]);

  useEffect(() => {
    setSeatTogether(props.seatTogether);
  }, [props.seatTogether]);

  const addSelection = (item) => {
    let nonObject = props.data.filter((value) => value.name == 'Non');
    let nonId = nonObject[0] ? nonObject[0].id : -1;

    console.log(nonId);
    let nonIndexId = props.data.findIndex((value, index) => {
      if (item == value.id || item == value.name) {
        if (index == 0) {
          return true;
        } else {
          return index;
        }
      }
    });

    let array = [...selectedArray];

    let index = array.findIndex((value) => value === item);
    if (index != -1) {
      array.splice(index, 1);
      setSelectedArray(array);
    } else {
      if (nonIndexId != -1) {
        let object = props.data[nonIndexId];

        if (
          (object.name && object.name == 'Non') ||
          (object.id && object.id == 'Non')
        ) {
          array = [];
        } else if (nonId) {
          let removeNonIndex = array.findIndex((value) => value == nonId);
          console.log(removeNonIndex);
          removeNonIndex != -1 ? array.splice(removeNonIndex, 1) : null;
        }
      }
      array.push(item);
      setSelectedArray(array);
    }
  };

  return (
    <View>
      <Modal
        animationType={'slide'}
        transparent={props.modalVisible}
        style={{backgroundColor: COLOR.BLACK}}
        visible={props.modalVisible}
        onRequestClose={() => props.handleModalVisibility()}>
        <View style={styles.modal}>
          <View style={styles.modalBody}>
            <NavigationBar
              right_image={IMAGES.filter_image}
              prop={props.props}
              isModal={props}
              navHeight={40}
              name={props.title}
            />
            {props.title == 'Seating Preferences' && (
              <View
                style={{
                  width: wp(90),
                  alignSelf: 'center',
                  marginTop: 20,
                }}>
                <Text
                  style={[Styles.body_label, {fontFamily: FONTS.FAMILY_BOLD}]}>
                  Seating Position
                </Text>
                <Text style={Styles.small_label}>
                  *Choosing seating position may cost extra-fees
                </Text>
              </View>
            )}
            <View
              style={{
                // flex: 1,
                width: wp(90),
                alignSelf: 'center',
                paddingVertical: 10,
              }}>
              <KeyboardAwareFlatList
                data={props.data}
                renderItem={({item, index}) => (
                  <TouchableWithoutFeedback>
                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        marginVertical: 10,
                        alignItems: 'center',
                      }}>
                      {props.showImage && (
                        <Image
                          style={{height: 20, width: 20, marginRight: 15}}
                          resizeMode={'contain'}
                          source={item.image}
                        />
                      )}
                      <Text
                        style={[
                          Styles.body_label,
                          {width: 170, textAlign: 'left', flex: 1},
                        ]}>
                        {item.name
                          ? item.name
                          : item.time
                          ? item.time
                          : item.preference
                          ? item.preference
                          : item.policy}
                      </Text>

                      <TouchableOpacity
                        onPress={() => {
                          addSelection(
                            item.id
                              ? item.id
                              : item.name
                              ? item.name
                              : item.time
                              ? item.time
                              : item.preference
                              ? item.preference
                              : item.policy,
                          );
                        }}
                        style={{
                          height: 30,
                          width: 30,
                          borderRadius: 5,
                          backgroundColor: selectedArray.some(
                            (value) =>
                              value ===
                              (item.id
                                ? item.id
                                : item.name
                                ? item.name
                                : item.time
                                ? item.time
                                : item.preference
                                ? item.preference
                                : item.policy),
                          )
                            ? COLOR.GREEN
                            : COLOR.WHITE,
                          borderColor: COLOR.GRAY,
                          justifyContent: 'center',
                          alignItems: 'center',
                          borderWidth: 1,
                        }}>
                        <Image
                          resizeMode={'contain'}
                          style={{height: 20, width: 20, alignSelf: 'center'}}
                          source={IMAGES.tick_image}
                        />
                      </TouchableOpacity>
                    </View>
                  </TouchableWithoutFeedback>
                )}
                numColumns={'1'}
                removeClippedSubviews={false}
                showsVerticalScrollIndicator={false}
                ListFooterComponent={() =>
                  props.title == 'Equipment' ? (
                    <View style={{marginTop: 10}}>
                      <Text
                        style={{
                          fontSize: 16,
                          fontFamily: FONTS.FAMILY_SEMIBOLD,
                        }}>
                        Comment
                      </Text>
                      <View
                        style={{
                          width: wp(90),
                          alignSelf: 'center',
                          height: 120,
                          borderWidth: 0.5,
                          borderColor: COLOR.GRAY,
                          borderRadius: 10,
                          marginVertical: 10,
                        }}>
                        <TextInput
                        inputAccessoryViewID={'uniqueID'}
                          style={[Styles.body_label, {padding: 15}]}
                          placeholder={
                            'Please specify if you plan to bring any equipments'
                          }
                          placeholderTextColor={COLOR.LIGHT_TEXT}
                          multiline={true}
                        />
                        <DoneButtonKeyboard />
                      </View>
                    </View>
                  ) : null
                }
                showsHorizontalScrollIndicator={false}
                keyExtractor={(item, index) => index.toString()}
              />
              
              {props.showBottomNote && (
                <Text style={[Styles.small_label, {marginVertical: 10}]}>
                  ** All Features are subject to availability and may cost
                  extra-charge
                </Text>
              )}
              {props.title == 'Seating Preferences' && (
                <View>
                  <View
                    style={[
                      Styles.line_view,
                      {marginVertical: 20, width: wp(90)},
                    ]}
                  />
                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      justifyContent: 'space-between',
                      alignSelf: 'center',
                      marginVertical: 10,
                      width: wp(90),
                    }}>
                    <View
                      style={{
                        flexDirection: 'row',
                      }}>
                      <Text
                        style={[
                          Styles.body_label,
                          {width: null, fontFamily: FONTS.FAMILY_BOLD},
                        ]}>
                        Seat Together
                      </Text>
                      <Text
                        style={[
                          Styles.body_label,
                          {
                            width: null,
                            marginRight: 5,
                            alignItems: 'flex-start',
                            color: COLOR.LIGHT_TEXT_COLOR,
                          },
                        ]}>
                        (upon availablity)
                      </Text>
                    </View>
                    <Switch
                      isChecked={seatTogether}
                      onCheckedListener={() => setSeatTogether(!seatTogether)}
                    />
                  </View>
                  <View
                    style={[
                      Styles.line_view,
                      {marginVertical: 20, width: wp(90)},
                    ]}
                  />
                </View>
              )}
            </View>

            <TouchableOpacity
              style={{
                width: wp(90),
                alignSelf: 'center',
                height: 50,
                alignItems: 'center',
                justifyContent: 'center',
                backgroundColor: COLOR.GREEN,
                borderRadius: 10,
                // marginBottom: 20,
                position:'absolute',
                bottom:25,
              }}
              onPress={() => {
                props.handleSelection(selectedArray);
                props.handleModalVisibility();
                props.setSeatTogether(seatTogether);
              }}>
              <Text
                style={[
                  Styles.button_font,
                  {
                    color: COLOR.WHITE,
                    width: null,
                    textAlign: 'center',
                  },
                ]}>
                Apply
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    </View>
  );
};

const styles = StyleSheet.create({
  modal: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
  },
  modalBody: {
    alignItems: 'center',
    alignSelf: 'center',
    backgroundColor: COLOR.WHITE,
    borderWidth: 1,
    width: wp(100),
    marginTop: 50,
    borderColor: COLOR.BORDER,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    flex: 1,
  },
  image: {
    height: 120,
    width: 120,
    alignSelf: 'center',
    marginVertical: 20,
  },
  headingLabel: {
    fontFamily: FONTS.FAMILY_BOLD,
    fontSize: FONTS.MEDIUM,
    color: COLOR.BLACK,
  },
  singleLineView: {
    marginVertical: 20,
    borderBottomColor: COLOR.GRAY,
    width: wp(100),
    alignSelf: 'center',
    borderBottomWidth: 1,
  },
  borderView: {
    borderRadius: 10,
    borderWidth: 3,
    width: wp(90),
    borderColor: COLOR.GRAY,
    alignSelf: 'center',
  },
});
