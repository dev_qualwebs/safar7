import React from 'react';
import {View, Text, Platform} from 'react-native';
import DropDownPicker from 'react-native-dropdown-picker';
import COLOR from '../styles/Color';
import FONTS from '../styles/Fonts';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import Styles from '../styles/Styles';

export const Dropdown = (props) => {
  return (
    <View
      style={{
        ...(Platform.OS !== 'android' && {
          zIndex: props.zIndex,
        }),
      }}>
      <Text
        style={[
          Styles.body_label,
          {
            paddingTop: 10,
            marginBottom: 0,
            color: COLOR.BLACK,
            backgroundColor: COLOR.WHITE,
            fontFamily: FONTS.FAMILY_BOLD,
          },
        ]}>
        {props.placeholder}
      </Text>

      <View
        style={{
          height: 50,
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        <DropDownPicker
          items={props.data ? props.data : []}
          searchable={false}
          searchableError={() => <Text>Not Found</Text>}
          searchablePlaceholderTextColor={COLOR.LIGHT_TEXT}
          defaultValue={props.name ? props.name : null}
          placeholder={''}
          zIndex={props.zIndex}
          placeholderStyle={{color: COLOR.LIGHT_TEXT, fontSize: 14}}
          labelStyle={{
            fontSize: FONTS.MEDIUM,
            fontFamily: FONTS.FAMILY_MEDIUM,
            color: COLOR.BLACK,
            marginLeft: -5,
          }}
          dropDownMaxHeight={300}
          style={{
            backgroundColor: '#ffffff',
            shadowOpacity: 0,
            borderWidth: 0,
            height: 50,
            alignSelf: 'center',
            borderTopLeftRadius: 0,
            borderTopRightRadius: 0,
            borderBottomLeftRadius: 0,
            borderBottomRightRadius: 0,
          }}
          dropDownStyle={{
            backgroundColor: '#fff',
          }}
          containerStyle={{
            borderRadius: 0,
          }}
          onChangeItem={(item) => {
            props.handleSelectedDropdownVal(item);
          }}
        />
      </View>
    </View>
  );
};

export const handleDropdownValue = (val, key) => {
  var dropdownData = [];
  for (let i = 0; i < val.length; i++) {
    if (key == 'preference') {
      dropdownData.push({
        label: val[i].preference,
        value: val[i].id,
        id: val[i].id,
        key: key,
      });
    } else if (key == 'roomType') {
      dropdownData.push({
        label: val[i].name,
        value: val[i].id,
        id: val[i].id,
        key: key,
      });
    }
  }
  return dropdownData;
};
