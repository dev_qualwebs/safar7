import {useNavigation} from '@react-navigation/core';
import React from 'react';
import {SafeAreaView,TouchableOpacity,Image,Text} from 'react-native';
import {View} from 'react-native';
import {GooglePlacesAutocomplete} from 'react-native-google-places-autocomplete';
import {widthPercentageToDP} from 'react-native-responsive-screen';
import COLOR from '../styles/Color';
import FONTS from '../styles/Fonts';
import IMAGES from '../styles/Images';

const GooglePlacesImagesInput = (props) => {
  const navigation = useNavigation();
  const attrName = props.route.params.attrName;
  const attr2Name = props.route.params.attr2Name;
  return (
    <SafeAreaView style={{flex: 1}}>

<View
        style={{
          height: 50,
          flexDirection: 'row',
          justifyContent: 'space-between',
          width: widthPercentageToDP(88),
          alignSelf: 'center',
          marginVertical: 0,
          alignItems: 'center',
        }}>
        <TouchableOpacity onPress={() => {
             props.route.params.updateMasterState('modalVisible', true);
             props.route.params.updateMasterState('openPlaces', false);
            navigation.goBack()}}>
          <Image style={{height: 15, width: 15}} source={IMAGES.back_arrow} />
        </TouchableOpacity>
        <Text
          style={[
            {width: null, fontSize: 20, fontFamily: FONTS.FAMILY_BOLD},
          ]}></Text>
        <Image style={{height: 20, width: 20}} source={''} />
      </View>

      <GooglePlacesAutocomplete
        fetchDetails={true}
        styles={{
          textInputContainer: {
            borderWidth: 1,
            borderColor: COLOR.VOILET,
            marginTop: 10,
            width: widthPercentageToDP(90),
            alignSelf: 'center',
            height: 40,
            borderRadius: 4,
            backgroundColor: '#F6F6F6',
            zIndex: 10000,
          },
          textInput: {
            marginHorizontal: 5,
            height: 38,
            borderRadius: 20,
            backgroundColor: '#F6F6F6',
          },
          listView: {
            width: widthPercentageToDP(90),
            alignSelf: 'center',
            zIndex: 10000,
          },
        }}
        placeholder="Search"
        onPress={(data, details = null) => {
            console.log('data',details.photos[0].photo_reference);
          props.route.params.updateMasterState('modalVisible', true);
          props.route.params.updateMasterState(attrName,details.photos && details.photos[0] && details.photos[0].photo_reference);
          props.route.params.updateMasterState(
            attr2Name,
            data.structured_formatting.main_text,
          );
          navigation.goBack();
        }}
        onFail={(error) => console.error(error)}
        query={{
            key: 'AIzaSyAgF4sT8oFMjhAtGWzfyMKKqaEOWaqLV2c',
        //   key: 'AIzaSyAZTQHLuxlQSaiYnb3l_nkQX96bHbhbYEo',
          language: 'en',
        }}
      />
    </SafeAreaView>
  );
};

export default GooglePlacesImagesInput;
