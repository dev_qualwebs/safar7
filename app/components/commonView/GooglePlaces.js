import {useNavigation} from '@react-navigation/core';
import React from 'react';
import {SafeAreaView, TouchableOpacity,Image,Text} from 'react-native';
import {View} from 'react-native';
import {GooglePlacesAutocomplete} from 'react-native-google-places-autocomplete';
import {widthPercentageToDP} from 'react-native-responsive-screen';
import COLOR from '../styles/Color';
import FONTS from '../styles/Fonts';
import IMAGES from '../styles/Images';

const GooglePlacesInput = (props) => {
  const navigation = useNavigation();
  const attrName = props.route.params.attrName;
  return (
    <SafeAreaView style={{flex: 1}}>
      <View
        style={{
          height: 50,
          flexDirection: 'row',
          justifyContent: 'space-between',
          width: widthPercentageToDP(88),
          alignSelf: 'center',
          marginVertical: 0,
          alignItems: 'center',
        }}>
        <TouchableOpacity onPress={() => {
             props.route.params.updateMasterState('modalVisible', true);
             props.route.params.updateMasterState('openPlaces', false);
            navigation.goBack()}}>
          <Image style={{height: 15, width: 15}} source={IMAGES.back_arrow} />
        </TouchableOpacity>
        <Text
          style={[
            {width: null, fontSize: 20, fontFamily: FONTS.FAMILY_BOLD},
          ]}></Text>
        <Image style={{height: 20, width: 20}} source={''} />
      </View>

      <GooglePlacesAutocomplete
        styles={{
          textInputContainer: {
            borderWidth: 1,
            borderColor: COLOR.VOILET,
            marginTop: 10,
            marginHorizontal: 15,
            borderRadius: 10,
          },
          textInput: {
            marginHorizontal: 5,
          },
        }}
        placeholder="Search"
        placeholderTextColor={'black'}
        onPress={(data, details = null) => {
          props.route.params.updateMasterState('modalVisible', true);
          props.route.params.updateMasterState('openPlaces', false);
          props.route.params.updateMasterState(attrName, data.description);
          navigation.goBack();
        }}
        onFail={(error) => console.error(error)}
        query={{
          // key: 'AIzaSyAZTQHLuxlQSaiYnb3l_nkQX96bHbhbYEo',
          key: 'AIzaSyBujNm7-f-Ol6Du3QtawPsOwDLYbdkFAek',
          language: 'en',
        }}
      />
    </SafeAreaView>
  );
};

export default GooglePlacesInput;
