import moment from 'moment';
import React, {useEffect, useState} from 'react';
import {
  TouchableOpacity,
  Text,
  StyleSheet,
  InputAccessoryView,
  View,
  Keyboard,
  TouchableWithoutFeedback,
} from 'react-native';
import DateTimePicker from 'react-native-modal-datetime-picker';
import {FloatingTitleTextInputField} from '../../helper/FloatingTitleTextInputField';
import COLOR from '../styles/Color';
import FONTS from '../styles/Fonts';
import IMAGES from '../styles/Images';
import Styles from '../styles/Styles';

export const DateView = (props) => {
  const [isSelected, setSelected] = useState(false);

  return (
    <TouchableOpacity
      style={[
        styles.dateView,
        {backgroundColor: isSelected ? COLOR.GREEN : COLOR.WHITE},
      ]}
      onPress={() => {
        setSelected(!isSelected);
      }}>
      <Text
        style={[
          styles.commonView,
          {color: isSelected ? COLOR.WHITE : COLOR.BLACK},
        ]}>
        {props.day}
      </Text>
      <Text
        style={[
          styles.commonView,
          {color: isSelected ? COLOR.WHITE : COLOR.BLACK},
        ]}>
        {props.date}
      </Text>
    </TouchableOpacity>
  );
};

export const DoneButtonKeyboard = (props) => {
  return (
    <InputAccessoryView nativeID={'uniqueID'}>
      <View style={{alignItems: 'flex-end', backgroundColor: COLOR.WHITE}}>
        <TouchableOpacity
          onPress={() => {
            Keyboard.dismiss();
          }}>
          <View
            style={{
              height: 30,
              width: 60,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Text
              style={[
                Styles.medium_label,
                {color: '#0E86D4', fontWeight: 'bold'},
              ]}>
              Done
            </Text>
          </View>
        </TouchableOpacity>
      </View>
    </InputAccessoryView>
  );
};

export const GOOGLE_IMAGE =
  'https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photo_reference=';

export const GOOGLE_PLACES_API = 'AIzaSyBujNm7-f-Ol6Du3QtawPsOwDLYbdkFAek';

export const ModalDatePicker = (props) => {
  const [isDatePickerVisible, setDatePickerVisibility] = useState(false);

  const hideDatePicker = () => {
    setDatePickerVisibility(false);
  };

  const handleConfirm = (date) => {
    if (props.mode === 'time') {
      console.log(date);
      props.onChange(moment(date).format('HH:mm'));
    } else if (props.mode === 'date') {
      props.onChange(moment(date).format('YYYY-MM-DD'));
    }
    hideDatePicker();
  };

  return (
    <View style={{flex: 1, height: 50}}>
      <TouchableOpacity onPress={() => setDatePickerVisibility(true)}>
        <View pointerEvents={'none'} style={{height: 50}}>
          <FloatingTitleTextInputField
            attrName="time"
            title="Time"
            value={props.value}
          />
        </View>
      </TouchableOpacity>
      <DateTimePicker
        // ref={props.setRef}
        isVisible={isDatePickerVisible}
        mode={props.mode}
        onConfirm={handleConfirm}
        onCancel={hideDatePicker}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  dateView: {
    marginHorizontal: 3,
    borderColor: COLOR.BORDER,
    borderWidth: 1,
    width: 50,
    height: 60,
    borderRadius: 5,
    justifyContent: 'space-evenly',
  },
  commonView: {
    fontFamily: FONTS.FAMILY_REGULAR,
    alignSelf: 'center',
  },
});
