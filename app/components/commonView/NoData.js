'use strict';

import React, {useState} from 'react';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview';
import {
  Text,
  View,
  SafeAreaView,
  TouchableOpacity,
  Alert,
  TextInput,
  StyleSheet,
  Image,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import COLOR from '../styles/Color';
import Styles from '../styles/Styles';
import IMAGES from '../styles/Images';

const NoData = (props) => {
  return (
    <View style={Styles.container}>
      <View style={styles.noDataView}>
        <Image
          source={props.image}
          resizeMode={'contain'}
          style={{
            height: 200,
            width: 200,
            alignSelf: 'center',
            tintColor: COLOR.GRAY,
          }}
        />
        <Text
          style={[
            Styles.body_label,
            {textAlign: 'center', marginTop: 10, color: COLOR.VOILET},
          ]}>
          {props.message}
        </Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  noDataView: {
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
});

export default NoData;
