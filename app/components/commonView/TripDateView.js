import React from 'react';
import {StyleSheet, View, Image, Text} from 'react-native';
import Styles from '../styles/Styles';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import COLOR from '../styles/Color';
import FONTS from '../styles/Fonts';
import IMAGES from '../styles/Images';

const TripDateView = ({
  departDate,
  returnDate,
  departFlexibility,
  returnFlexibility,
  marginTop,
}) => {
  if (departDate) {
    return (
      <View
        style={[
          Styles.shadow_view,
          styles.container,
          {marginTop: marginTop ? marginTop : 20},
        ]}>
        <Text style={[Styles.body_label, styles.dateTitle]}>Trip Dates</Text>
        <View style={styles.vr} />
        <View>
          <Text style={[Styles.small_label, styles.dateText]}>
            {departDate}
          </Text>
          {departFlexibility && departFlexibility != 0 ? (
            <Text style={[Styles.small_label, styles.flexibilityText]}>
              {`± ${departFlexibility} days`}
            </Text>
          ) : null}
        </View>
        <Image
          source={IMAGES.dot_back}
          style={{height: 20, width: 20}}
          resizeMode={'cover'}
        />

        <View>
          <Text style={[Styles.small_label, styles.dateText]}>
            {returnDate}
          </Text>
          {returnFlexibility && returnFlexibility != 0 ? (
            <Text style={[Styles.small_label, styles.flexibilityText]}>
              {`± ${returnFlexibility} days`}
            </Text>
          ) : null}
        </View>
      </View>
    );
  } else {
    return null;
  }
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    height: 45,
    justifyContent: 'space-between',
    width: wp(90),
    alignItems: 'center',
    borderRadius: 22.5,
    backgroundColor: COLOR.WHITE,
    paddingHorizontal: 10,
    alignSelf: 'center',
    marginTop: 20,
  },
  dateTitle: {
    width: null,
    textAlign: 'center',
    color: COLOR.BLACK,
    fontFamily: FONTS.FAMILY_BOLD,
    fontSize: 14,
  },
  vr: {width: 1, backgroundColor: COLOR.GRAY, height: 35},
  dateText: {
    width: null,
    textAlign: 'center',
    color: COLOR.BLACK,
    fontFamily: FONTS.FAMILY_SEMIBOLD,
  },
  flexibilityText: {
    width: null,
    textAlign: 'center',
    color: COLOR.VOILET,
  },
});

export default TripDateView;
