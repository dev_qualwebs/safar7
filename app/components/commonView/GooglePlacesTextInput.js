import React, {useState} from 'react';
import {GooglePlacesAutocomplete} from 'react-native-google-places-autocomplete';
import {widthPercentageToDP} from 'react-native-responsive-screen';
import COLOR from '../styles/Color';
import FONTS from '../styles/Fonts';

export default function GooglePlacesTextInput({value, onValueChange,placeholder}) {
  let [isFocused, setIsFocused] = useState(false);
  return (
    <GooglePlacesAutocomplete
      styles={{
        textInputContainer: {
          borderColor: COLOR.GRAY,
          borderRadius: 0,
          height: 40,
        },
        textInput: {
          fontSize: FONTS.MEDIUM,
          fontFamily: FONTS.FAMILY_SEMIBOLD,
          paddingLeft: 0,
          paddingVertical: 0,
        },
        listView: {
          position: 'absolute',
          zIndex: 9999,
          top: 40,
          left: 0,
          width: widthPercentageToDP(70),
        },
      }}
      enablePoweredByContainer={false}
      textInputProps={{
        clearButtonMode: 'never',
        placeholderTextColor: '#8A9AA2',
        onFocus: () => setIsFocused(true),
        onBlur: () => setIsFocused(false),
        onChangeText: (text) => {
          if (isFocused) {
            onValueChange(text);
          }
        },
        value: value,
      }}
      
      placeholder={placeholder ? placeholder : "Search"}
      onPress={(data, details = null) => {
        // 'details' is provided when fetchDetails = true
        onValueChange(data.description);
      }}
      onFail={(error) => console.error(error)}
      query={{
        key: 'AIzaSyAZTQHLuxlQSaiYnb3l_nkQX96bHbhbYEo',
        language: 'en',
        type: '(cities)',
      }}
    />
  );
}
