import {View, Image} from 'react-native';
import React from 'react';

export default WaitingView = () => {
  return (
    <View
      style={{
        width: 30,
        height: 30,
        borderRadius: 15,
        backgroundColor: '#899AA2',
        justifyContent: 'center',
      }}>
      <Image
        style={{
          width: 15,
          height: 15,
          alignSelf: 'center',
        }}
        resizeMode={'contain'}
        source={require('../styles/assets/waiting.png')}
      />
    </View>
  );
};
