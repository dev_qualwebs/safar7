import React, {useEffect} from 'react';
import {Image} from 'react-native';
import {View} from 'react-native-animatable';
import {TouchableOpacity} from 'react-native-gesture-handler';
import COLOR from '../styles/Color';
import IMAGES from '../styles/Images';

const Switch = ({isChecked, onCheckedListener}) => {
  useEffect(() => {}, [isChecked]);
  return (
    <View style={{height: 30, width: 50, alignSelf: 'flex-end',backgroundColor:COLOR.WHITE}}>
      <TouchableOpacity
        style={{height: 30, width: 50, alignSelf: 'flex-end'}}
        onPress={onCheckedListener}>
        <Image
          style={{height: 30, width: 50}}
          source={isChecked ? IMAGES.radio_button_on : IMAGES.radio_button_off}
          resizeMode={'contain'}
        />
      </TouchableOpacity>
    </View>
  );
};

export default Switch;
