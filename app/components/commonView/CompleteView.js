import {View, Image} from 'react-native';
import React from 'react';
import COLOR from '../styles/Color';

export default WaitingView = () => {
  return (
    <View
      style={{
        width: 30,
        height: 30,
        borderRadius: 15,
        backgroundColor: COLOR.GREEN,
        justifyContent: 'center',
      }}>
      <Image
        style={{
          width: 15,
          height: 15,
          alignSelf: 'center',
        }}
        resizeMode={'contain'}
        source={require('../styles/assets/checked_right.png')}
      />
    </View>
  );
};
