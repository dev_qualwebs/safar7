'use strict';
import {Dimensions} from 'react-native';
const {width} = Dimensions.get('window');

const FONTS = {
  FAMILY_BOLD: 'NunitoSans-Bold',
  FAMILY_SEMIBOLD: 'NunitoSans-SemiBold',
  FAMILY_EXTRA_BOLD: 'NunitoSans-ExtraBold',
  FAMILY_REGULAR: 'NunitoSans-Regular',
  EXTRA_LARGE: fontSizer(1, width),
  LARGE: fontSizer(2, width),
  MEDIUM: fontSizer(3, width),
  REGULAR: fontSizer(4, width),
  HEADING: fontSizer(6, width),
  SMALL: fontSizer(5, width),
  EXTRA_SMALL: fontSizer(7, width),
};

function fontSizer(type, width) {
  if (width > 400) {
    switch (type) {
      case 1:
        return 28;
        break;
      case 2:
        return 26;
        break;
      case 3:
        return 18;
        break;
      case 4:
        return 16;
        break;
      case 5:
        return 13;
        break;
      case 6:
        return 22;
      case 7:
        return 12;
        break;
      default:
        break;
    }
  } else if (width > 250) {
    switch (type) {
      case 1:
        return 26;
        break;
      case 2:
        return 24;
        break;
      case 3:
        return 18;
        break;
      case 4:
        return 16;
        break;
      case 5:
        return 13;
      case 6:
        return 20;
        break;
      case 7:
        return 12;
        break;
      default:
        break;
    }
  } else {
    switch (type) {
      case 1:
        return 26;
        break;
      case 2:
        return 22;
        break;
      case 3:
        return 16;
        break;
      case 4:
        return 16;
        break;
      case 5:
        return 13;
        break;
      case 6:
        return 20;
      case 7:
        return 12;
        break;
      default:
        break;
    }
  }
}

export default FONTS;
