/*
 * Copyright (c) 2011-2018, Zingaya, Inc. All rights reserved.
 */

'use strict';

import {StyleSheet} from 'react-native';
import COLOR from './Color';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
  widthPercentageToDP,
} from 'react-native-responsive-screen';
import FONTS from './Fonts';

export default StyleSheet.create({
  //UIView
  container: {
   flex: 1,
    backgroundColor: COLOR.WHITE,
  },
  mainView: {
    width: widthPercentageToDP('90%'),
    marginTop: 0,
    alignSelf: 'center',
  },
  navbarView: {
    marginVertical: 10,
    height: 50,
    alignSelf: 'flex-start',
    width: 45,
  },
  line_view: {
    borderBottomColor: COLOR.GRAY,
    width: wp(100),
    alignSelf: 'center',
    borderBottomWidth: 1,
  },

  //UIBUttons
  button_font: {
    color: COLOR.WHITE,
    fontSize: FONTS.REGULAR,
    fontFamily: FONTS.FAMILY_BOLD,
    alignSelf: 'center',
  },
  button_content: {
    height: 50,
    width: wp(90),
    backgroundColor: COLOR.GREEN,
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
  },

  //UILabel
  heading_label: {
    textAlign: 'center',
    fontSize: FONTS.EXTRA_LARGE,
    fontFamily: FONTS.FAMILY_SEMIBOLD,
    marginVertical: 20,
    width: wp(90),
    alignSelf: 'center',
    color: COLOR.BLACK,
  },
  splash_heading_label: {
    fontSize: FONTS.EXTRA_LARGE,
    fontFamily: FONTS.FAMILY_EXTRA_BOLD,
    width: wp(90),
    alignSelf: 'center',
    color: COLOR.BLACK,
  },
  subheading_label: {
    fontSize: FONTS.MEDIUM,
    fontFamily: FONTS.FAMILY_BOLD,
    width: wp(90),
    alignSelf: 'center',
    color: COLOR.BLACK,
  },
  body_label: {
    fontFamily: FONTS.FAMILY_REGULAR,
    fontSize: FONTS.REGULAR,
    width: wp(90),
    alignSelf: 'center',
    color: COLOR.TEXT_COLOR,
  },
  small_label: {
    fontFamily: FONTS.FAMILY_REGULAR,
    fontSize: FONTS.SMALL,
    width: wp(90),
    alignSelf: 'center',
    color: COLOR.VOILET,
  },
  extra_small_label: {
    fontFamily: FONTS.FAMILY_REGULAR,
    fontSize: FONTS.EXTRA_SMALL,
    width: wp(90),
    alignSelf: 'center',
    color: COLOR.VOILET,
  },
  link_label: {
    textDecorationLine: 'underline',
    color: COLOR.WHITE,
    fontFamily: FONTS.FAMILY_SEMIBOLD,
    fontSize: FONTS.REGULAR,
    width: wp(90),
    alignSelf: 'center',
  },

  //UIText field
  text_input_text: {
    color: COLOR.BLACK,
    marginLeft: -15,
    fontFamily: FONTS.FAMILY_REGULAR,
    fontSize: FONTS.REGULAR,
  },
  text_input_heading: {
    fontSize: FONTS.SMALL,
    marginLeft: -15,
    fontFamily: FONTS.FAMILY_REGULAR,
    color: COLOR.LIGHT_TEXT,
  },

  //UIImage
  image: {
    width: wp('50%'),
    height: wp('50%'),
    borderRadius: wp('25%'),
    overflow: 'hidden',
    backgroundColor: COLOR.WHITE,
    resizeMode: 'cover',
  },
  backgroundImage: {
    flex: 1,
    width: null,
    height: null,
    overflow: 'hidden',
  },
  shadow_view: {
    shadowColor: COLOR.VOILET,
    shadowOffset: {width: 0.2, height: 0.2},
    shadowOpacity: 0.2,
    elevation:4,
  },
});
