import React, {useEffect, useRef, useState} from 'react';
import {Modal, Text, View, StyleSheet, Image} from 'react-native';
import {FlatList, TouchableOpacity} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import COLOR from '../../styles/Color';
import FONTS from '../../styles/Fonts';
import IMAGES from '../../styles/Images';
import Styles from '../../styles/Styles';
import NavigationBar from '../../commonView/NavigationBar';
import {useNavigation, useRoute} from '@react-navigation/native';
import CustomTextInput from '../../commonView/CustomTextInput';
import {TextInput} from 'react-native-gesture-handler';
import AddonsSelectionView from '../../commonView/AddonsSelectionView';
import {useDispatch, useSelector} from 'react-redux';
import ProfileAction from '../../../redux/action/ProfileAction';
import DropDownPicker from 'react-native-dropdown-picker';
import SelectDate from '../calendar/SelectDate';
import SimpleToast from 'react-native-simple-toast';
import { DoneButtonKeyboard } from '../../commonView/CommonView';

const VisaTypes = [
  {label: 'Entry Visa', value: 1},
  {label: 'Project Visa', value: 2},
  {label: 'Employement Visa', value: 3},
  {label: 'Business Visa', value: 4},
  {label: 'Tourist Visa', value: 5},
  {label: 'Research Visa', value: 6},
  {label: 'Transit Visa', value: 7},
  {label: 'Conference Visa', value: 8},
  {label: 'Medical Visa', value: 9},
];
export default function TravelVisa(props) {
  const dispatch = useDispatch();
  const route = useRoute();
  const countryData = useSelector((state) => state.profileReducer.countries);
  const [modalVisible, setModalVisible] = useState(true);
  const navigation = useNavigation();
  const [countries, setCountries] = useState([]);
  const [country, setCountry] = useState(null);
  const [dropdownVisibility, setDropdownVisibility] = useState(false);
  const [attrName, setAttrName] = useState('');
  const [openCalendar, setOpenCalendar] = useState(false);
  const [from, setFrom] = useState();
  const [to, setTo] = useState();
  const [visaDropdown, setVisadropdown] = useState(false);
  const [visaType, setVisaType] = useState(null);
  const [biometricsCity, setBiometricsCity] = useState('');

  const fromRef = useRef();
  const toRef = useRef();

  useEffect(() => {
    dispatch(ProfileAction.getCountries());
  }, []);

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      setModalVisible(true);
    });
    return unsubscribe;
  }, [navigation]);

  useEffect(() => {
    if (countryData) {
      const countries = countryData.map(function (value) {
        let object = {
          label: value.name,
          value: value.id,
          id: value.id,
        };
        return object;
      });
      setCountries(countries);
    }
  }, [countryData]);

  const updateMasterState = (attrName, value) => {
    setOpenCalendar(false);
    switch (attrName) {
      case 'from':
        setFrom(value);
        break;
      case 'to':
        setTo(value);
        break;
    }
  };

  const handleNavigation = () => {
    if (country == null) {
      SimpleToast.show('Select Country');
    } else if (visaType == null) {
      SimpleToast.show('Select Visa type');
    } else if (biometricsCity == '') {
      SimpleToast.show('Enter Biometric City');
    } else if (from == null || from == undefined) {
      SimpleToast.show('Select From');
    } else if (to == null || to == undefined) {
      SimpleToast.show('Select To');
    } else {
      setModalVisible(false);
      navigation.navigate('TravellersVisaDetails', {
        item: route.params.item,
        travel_country_id: country,
        visa_type_id: visaType,
        biometric_city: biometricsCity,
        start_date: from,
        end_date: to,
        val: route.params.val,
        type: route.params.type,
        traveller: false,
      });
    }
  };

  return (
    <View style={Styles.container}>
      {openCalendar ? (
        <SelectDate
          allowRangeSelection={false}
          attrName={attrName}
          updateMasterState={updateMasterState}
          props={props}
          modal={true}
        />
      ) : (
        <Modal
          animationType={'slide'}
          transparent={true}
          style={{backgroundColor: COLOR.BLACK}}
          visible={modalVisible}
          onRequestClose={() => {
            console.log('Modal has been closed.');
          }}>
          <View style={styles.modal}>
            <View style={styles.modalBody}>
              <NavigationBar
                prop={props}
                handleBack={() => {
                  setModalVisible(false);
                  navigation.goBack();
                }}
                left_image={IMAGES.back_arrow}
                navHeight={40}
                name={'Travel Visa'}
              />
              <View style={{flex: 1}}>
                <View
                  style={[
                    styles.borderView,
                    {zIndex: 1000, marginVertical: 15},
                  ]}>
                  <DropDownPicker
                    showArrowIcon={true}
                    open={dropdownVisibility}
                    setOpen={() => setDropdownVisibility(!dropdownVisibility)}
                    labelStyle={{
                      fontSize: FONTS.MEDIUM,
                      fontFamily: FONTS.FAMILY_REGULAR,
                      color: COLOR.BLACK,
                      marginLeft: -5,
                    }}
                    dropDownMaxHeight={300}
                    style={{
                      backgroundColor: '#ffffff',
                      shadowOpacity: 0,
                      borderWidth: 0,
                      height: 50,
                      alignSelf: 'center',
                      borderTopLeftRadius: 10,
                      borderTopRightRadius: 10,
                      borderBottomLeftRadius: 10,
                      borderBottomRightRadius: 10,
                      paddingHorizontal: 10,
                    }}
                    dropDownStyle={{
                      backgroundColor: '#fff',
                    }}
                    containerStyle={{
                      borderRadius: 0,
                    }}
                    setValue={setCountry}
                    value={country}
                    textStyle={{
                      fontFamily: FONTS.FAMILY_REGULAR,
                    }}
                    placeholder={'Select Country'}
                    labelStyle={{
                      fontFamily: FONTS.FAMILY_REGULAR,
                    }}
                    items={countries}
                  />
                </View>

                <View
                  style={[styles.borderView, {marginBottom: 15, zIndex: 900}]}>
                  <DropDownPicker
                    showArrowIcon={true}
                    open={visaDropdown}
                    setOpen={() => setVisadropdown(!visaDropdown)}
                    labelStyle={{
                      fontSize: FONTS.MEDIUM,
                      fontFamily: FONTS.FAMILY_REGULAR,
                      color: COLOR.BLACK,
                      marginLeft: -5,
                    }}
                    dropDownMaxHeight={300}
                    style={{
                      backgroundColor: '#ffffff',
                      shadowOpacity: 0,
                      borderWidth: 0,
                      height: 50,
                      alignSelf: 'center',
                      borderTopLeftRadius: 10,
                      borderTopRightRadius: 10,
                      borderBottomLeftRadius: 10,
                      borderBottomRightRadius: 10,
                      paddingHorizontal: 10,
                    }}
                    dropDownStyle={{
                      backgroundColor: '#fff',
                    }}
                    containerStyle={{
                      borderRadius: 0,
                    }}
                    setValue={setVisaType}
                    value={visaType}
                    textStyle={{
                      fontFamily: FONTS.FAMILY_REGULAR,
                    }}
                    placeholder={'Select Visa Type'}
                    labelStyle={{
                      fontFamily: FONTS.FAMILY_REGULAR,
                    }}
                    items={VisaTypes}
                  />
                  <View
                    style={{
                      width: '100%',
                      height: 1,
                      backgroundColor: '#C0C7CB',
                    }}
                  />
                  <TextInput
                  inputAccessoryViewID={'uniqueID'}
                    style={styles.textInput}
                    placeholder={'Biometrics City'}
                    value={biometricsCity}
                    onChangeText={(text) => setBiometricsCity(text)}
                    placeholderTextColor={COLOR.LIGHT_TEXT_COLOR}
                  />
                  <DoneButtonKeyboard />
                </View>
                <Text
                  style={{
                    fontSize: 13,
                    fontFamily: FONTS.FAMILY_SEMIBOLD,
                    marginVertical: 5,
                  }}>
                  Travel Dates
                </Text>
                <View style={[styles.borderView, {marginBottom: 15}]}>
                  <TextInput
                  inputAccessoryViewID={'uniqueID'}
                    ref={fromRef}
                    style={styles.textInput}
                    placeholder={'From'}
                    value={from}
                    onFocus={() => {
                      setAttrName('from');
                      setOpenCalendar(true);
                      fromRef.current.blur();
                    }}
                    placeholderTextColor={COLOR.LIGHT_TEXT_COLOR}
                  />
                  <View
                    style={{
                      width: '100%',
                      height: 1,
                      backgroundColor: '#C0C7CB',
                    }}
                  />
                  <TextInput
                  inputAccessoryViewID={'uniqueID'}
                    ref={toRef}
                    style={styles.textInput}
                    placeholder={'To'}
                    value={to}
                    onFocus={() => {
                      setAttrName('to');
                      setOpenCalendar(true);
                      toRef.current.blur();
                    }}
                    placeholderTextColor={COLOR.LIGHT_TEXT_COLOR}
                  />
                </View>
              </View>

              <TouchableOpacity
                style={{
                  width: wp(90),
                  alignSelf: 'center',
                  height: 50,
                  alignItems: 'center',
                  justifyContent: 'center',
                  backgroundColor: COLOR.GREEN,
                  borderRadius: 10,
                  marginBottom: 20,
                }}
                onPress={() => {
                  handleNavigation();
                }}>
                <Text
                  style={[
                    Styles.button_font,
                    {
                      color: COLOR.WHITE,
                      width: null,
                      textAlign: 'center',
                    },
                  ]}>
                  Continue
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  modal: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
  },
  modalBody: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    backgroundColor: COLOR.WHITE,
    borderWidth: 1,
    width: wp(100),
    marginTop: 50,
    borderColor: COLOR.BORDER,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    flex: 1,
  },
  image: {
    height: 120,
    width: 120,
    alignSelf: 'center',
    marginVertical: 20,
  },

  borderView: {
    borderRadius: 10,
    borderWidth: 1,
    width: wp(90),
    borderColor: '#C0C7CB',
    alignSelf: 'center',
  },
  textInput: {
    height: 50,
    fontFamily: FONTS.FAMILY_REGULAR,
    fontSize: 14,
    paddingHorizontal: 10,
  },
});
