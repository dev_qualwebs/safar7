import React, {useEffect, useState} from 'react';
import {Modal, Text, View, StyleSheet, Image} from 'react-native';
import {FlatList, TouchableOpacity} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import COLOR from '../../styles/Color';
import FONTS from '../../styles/Fonts';
import IMAGES from '../../styles/Images';
import Styles from '../../styles/Styles';
import NavigationBar from '../../commonView/NavigationBar';
import {connect, useDispatch, useSelector} from 'react-redux';
import ProfileAction from '../../../redux/action/ProfileAction';
import {useNavigation, useRoute} from '@react-navigation/native';
import API from '../../../api/Api';
import RequestAction from '../../../redux/action/RequestAction';

const api = new API();

function TravellersSelection(props) {
  const travellers = useSelector(
    (state) => state.profileReducer.travellersData,
  );
  const dispatch = useDispatch();
  const route = useRoute();
  const navigation = useNavigation();
  const [modalVisible, setModalVisible] = useState(true);
  const [selectedTravellers, setSelectedTravellers] = useState([]);

  useEffect(() => {
    dispatch(ProfileAction.getTravellers());
  }, []);

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      setModalVisible(true);
      getData(route.params.name);
    });
    return unsubscribe;
  }, [navigation]);

  const handleAddonsNavigation = (item) => {
    setModalVisible(false);
    let routeName, traveller;
    switch (route.params.name) {
      case 'TravelVisa':
        if (item.is_default_user == 1) {
          routeName = 'TravelVisa';
          traveller = false;
        } else {
          routeName = 'TravellersVisaDetails';
          traveller = true;
        }
        break;
      case 'TravellerInsurance':
        if (item.is_default_user == 1) {
          routeName = 'TravellerInsurance';
          traveller = false;
        } else {
          routeName = 'TravellerInsuranceSecondary';
          traveller = false;
        }
        break;
      case 'InternationalDrivingLicense':
        routeName = 'InternationalDrivingLicense';
        traveller = false;
        break;
    }
    navigation.navigate(routeName, {
      val: route.params.val,
      type: route.params.type,
      item: item,
      traveller: traveller,
    });
  };

  const getData = (name) => {
    switch (name) {
      case 'TravelVisa':
        api
          .getTravellerVisa(route.params.val, route.params.type)
          .then((json) => {
            console.log('visa', json);
            if (json.data.response) {
              setSelectedTravellers(json.data.response);
            }
          })
          .catch((error) => {
            console.log(error.response.data);
          });
        break;
      case 'TravellerInsurance':
        api
          .getTravellersInsurance(route.params.val, route.params.type)
          .then((json) => {
            console.log('insurance', json.data.response);
            if (json.data.response) {
              setSelectedTravellers(json.data.response);
            }
          })
          .catch((error) => {
            console.log(error.response.data);
          });
        break;
      case 'InternationalDrivingLicense':
        api
          .getTravellersLicence(route.params.val, route.params.type)
          .then((json) => {
            console.log('licence', json.data.response);
            if (json.data.response) {
              setSelectedTravellers(json.data.response);
            }
          })
          .catch((error) => {
            console.log(error.response.data);
          });
        break;
    }
  };

  const handleDelete = (item) => {
    switch (route.params.name) {
      case 'TravelVisa':
        api
          .deleteTravellerVisa(route.params.val, item.id, route.params.type)
          .then((json) => {
            getData(route.params.name);
          })
          .catch((error) => {
            console.log(error.response.data);
          });
        break;
      case 'TravellerInsurance':
        api
          .deleteTravellerInsurance(
            route.params.val,
            item.id,
            route.params.type,
          )
          .then((json) => {
            getData(route.params.name);
          })
          .catch((error) => {
            console.log(error.response.data);
          });
        break;
      case 'InternationalDrivingLicense':
        api
          .deleteTravellerLicence(route.params.val, item.id, route.params.type)
          .then((json) => {
            getData(route.params.name);
          })
          .catch((error) => {
            console.log(error.response.data);
          });
        break;
    }
  };

  return (
    <View style={Styles.container}>
      <Modal
        animationType={'slide'}
        transparent={true}
        style={{backgroundColor: COLOR.BLACK}}
        visible={modalVisible}
        onRequestClose={() => {
          console.log('Modal has been closed.');
        }}>
        <View style={styles.modal}>
          <View style={styles.modalBody}>
            <NavigationBar
              prop={props}
              left_image={IMAGES.back_arrow}
              navHeight={40}
              name={'Travellers Selection'}
            />
            <View
              style={{
                width: wp(90),
                alignSelf: 'center',
                flex: 1,
              }}>
              <Text style={styles.greenText}>
                Choose from the travellers who you want to include in this addon
                service:
              </Text>
              <View>
                <FlatList
                  numColumns={1}
                  showsHorizontalScrollIndicator={false}
                  showsVerticalScrollIndicator={false}
                  data={travellers}
                  keyExtractor={(index) => index}
                  renderItem={({item}) => {
                    const selected = selectedTravellers.findIndex(
                      (value) => value.traveller_id == item.id,
                    );

                    return (
                      <View>
                        <View style={styles.travellerView}>
                          <TouchableOpacity
                            onPress={() => {
                              selected != -1
                                ? handleDelete(item)
                                : handleAddonsNavigation(item);
                            }}
                            style={{
                              height: 30,
                              width: 30,
                              marginHorizontal: 5,
                              borderRadius: 5,
                              backgroundColor:
                                selected != -1 ? COLOR.GREEN : COLOR.WHITE,
                              borderColor: COLOR.GRAY,
                              justifyContent: 'center',
                              alignItems: 'center',
                              borderWidth: 1,
                            }}>
                            <Image
                              resizeMode={'contain'}
                              style={{
                                height: 20,
                                width: 20,
                                alignSelf: 'center',
                              }}
                              source={IMAGES.tick_image}
                            />
                          </TouchableOpacity>
                          <Text
                            style={[Styles.body_label, {width: null, flex: 1}]}>
                            {item.prefix + ' ' + item.first_name}
                          </Text>
                        </View>
                        <View style={Styles.line_view} />
                      </View>
                    );
                  }}
                  keyExtractor={(item, index) => index.toString()}
                />
              </View>
            </View>

            <TouchableOpacity
              onPress={() => {
                props.getAddons(route.params.type,route.params.val)
                props.navigation.goBack();
              }}
              style={{
                width: wp(90),
                alignSelf: 'center',
                height: 50,
                alignItems: 'center',
                justifyContent: 'center',
                backgroundColor: COLOR.GREEN,
                borderRadius: 10,
                marginBottom: 20,
              }}>
              <Text
                style={[
                  Styles.button_font,
                  {
                    color: COLOR.WHITE,
                    width: null,
                    textAlign: 'center',
                  },
                ]}>
                Continue
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    </View>
  );
}

const styles = StyleSheet.create({
  modal: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
  },
  modalBody: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    backgroundColor: COLOR.WHITE,
    borderWidth: 1,
    width: wp(100),
    marginTop: 50,
    borderColor: COLOR.BORDER,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    flex: 1,
  },
  image: {
    height: 120,
    width: 120,
    alignSelf: 'center',
    marginVertical: 20,
  },
  headingLabel: {
    fontFamily: FONTS.FAMILY_BOLD,
    fontSize: FONTS.MEDIUM,
    color: COLOR.BLACK,
  },
  singleLineView: {
    marginVertical: 20,
    borderBottomColor: COLOR.GRAY,
    width: wp(100),
    alignSelf: 'center',
    borderBottomWidth: 1,
  },
  borderView: {
    borderRadius: 10,
    borderWidth: 3,
    width: wp(90),
    borderColor: COLOR.GRAY,
    alignSelf: 'center',
  },
  back_view: {
    backgroundColor: COLOR.WHITE,
    borderRadius: 10,
    shadowColor: COLOR.WHITE,
    shadowOffset: {width: 0.2, height: 0.2},
    shadowOpacity: 0.2,
    paddingHorizontal: 10,
    paddingVertical: 20,
    borderWidth: 1,
    borderColor: COLOR.GRAY,
    elevation: 3,
  },
  greenText: {
    color: COLOR.GREEN,
    fontFamily: FONTS.FAMILY_REGULAR,
    marginVertical: 10,
  },
  travellerView: {
    flexDirection: 'row',
    alignSelf: 'center',
    width: wp(90),
    alignItems: 'center',
    marginVertical: 10,
  },
});

const mapDispatchToProps = (dispatch) => {
  return {
    getAddons: (type, val) => {
      dispatch(RequestAction.getTripAddons(type, val));
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(TravellersSelection);
