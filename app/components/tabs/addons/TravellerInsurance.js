import React, {useEffect, useRef, useState} from 'react';
import {Modal, Text, View, StyleSheet, Image} from 'react-native';
import {FlatList, TouchableOpacity} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import COLOR from '../../styles/Color';
import FONTS from '../../styles/Fonts';
import IMAGES from '../../styles/Images';
import Styles from '../../styles/Styles';
import NavigationBar from '../../commonView/NavigationBar';
import {useNavigation, useRoute} from '@react-navigation/native';
import CustomTextInput from '../../commonView/CustomTextInput';
import {ScrollView, TextInput} from 'react-native-gesture-handler';
import AddonsSelectionView from '../../commonView/AddonsSelectionView';
import SelectDate from '../calendar/SelectDate';
import ImagePicker from 'react-native-image-crop-picker';
import API from '../../../api/Api';
import ActionSheet from '../../commonView/ActionSheet';
import SimpleToast from 'react-native-simple-toast';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scrollview';
import { DoneButtonKeyboard } from '../../commonView/CommonView';

export default function TravellerInsurance(props) {
  const [modalVisible, setModalVisible] = useState(true);
  const navigation = useNavigation();
  const route = useRoute();

  const [geographicalLimit, setGeographicalLimit] = useState('');
  const [employer, setEmployer] = useState('');
  const [from, setFrom] = useState();
  const [to, setTo] = useState();
  const [openCalendar, setOpenCalendar] = useState(false);
  const [attrName, setAttrName] = useState('');
  const [imageModal, setImageModal] = useState(false);
  const [imageIndex, setImageIndex] = useState();
  const [documents, setDocuments] = useState([]);

  const fromRef = useRef();
  const toRef = useRef();

  const updateMasterState = (attrName, value) => {
    setOpenCalendar(false);
    switch (attrName) {
      case 'from':
        setFrom(value);
        break;
      case 'to':
        setTo(value);
        break;
    }
  };

  const handleSheet = (val) => {
    if (val == 1) {
      chooseImage(1);
    } else if (val == 2) {
      chooseImage(2);
    } else if (val == 3) {
      setImageModal(false);
    }
  };

  const chooseImage = (val) => {
    let options = {
      includeBase64:true,
      title: 'Select Image',
      width: 1630,
      height: 750,
      compressImageMaxWidth: 1630,
      compressImageMaxHeight: 750,
      multiple: false,
    };

    if (val == 1) {
      ImagePicker.openCamera({
        title: 'Select Image',
        width: 1024,
        height: 472,
        compressImageMaxWidth: 1024,
        compressImageMaxHeight: 472,
        includeBase64:true,
      }).then((response) => {
        console.log('image local reposnse', response);
        setImageModal(false);
        // callUploadImage(response, val);
        callUploadImage(['data:image/jpeg;base64,' + response.data], val);
      });
    } else if (val == 2) {
      ImagePicker.openPicker(options).then((response) => {
        console.log('image local reposnse', response);
        setImageModal(false);

        callUploadImage(['data:image/jpeg;base64,' + response.data], val);
      });
    }
  };

  const callUploadImage = (res, val) => {
    // var data = new FormData();
    // var photo = {
    //   uri: res.path,
    //   type: res.type ? res.type : res.mime,
    //   name: 'imgs.jpg',
    // };

    // data.append('path', 'passport');
    // data.append('images[]', photo);
    // console.log('image uploading data is:-', photo);

    let data = {
      images : res,
      path : 'passport'
    }
  
    const api = new API();
    api
      .uploadImage(data)
      .then((json) => {
        console.log('upload responce:-', json.data);

        if (json.status == 200) {
          console.log('Success');
          let object = {
            type: imageIndex,
            path: json.data.response[0],
          };
          console.log(object);
          let index = documents.findIndex((value) => value.type == imageIndex);
          if (index == -1) {
            let array = [...documents];
            array.push(object);
            setDocuments(array);
          } else {
            let array = [...documents];
            array[index] = object;
            setDocuments(array);
          }
        }
      })
      .catch((error) => {
        console.log('error:-', error);
      });
  };

  const openImageModal = (id) => {
    setImageIndex(id);
    setImageModal(true);
  };

  const handleNavigation = () => {
    if (geographicalLimit == '') {
      SimpleToast.show('Enter Geographical Limit');
    } else if (from == null || from == undefined) {
      SimpleToast.show('Select Start Date');
    } else if (to == null || to == undefined) {
      SimpleToast.show('Select End Date');
    } else if (employer == '') {
      SimpleToast.show('Enter Employer');
    } else if (documents.length != 3) {
      SimpleToast.show('Select All Document');
    } else {
      setModalVisible(false);
      navigation.navigate('InsuranceAddress', {
        ...route.params,
        traveller_id: route.params.item.id,
        geographical_limit: geographicalLimit,
        start_date: from,
        end_date: to,
        employer: employer,
        documents: documents,
      });
    }
  };

  return (
    <View style={Styles.container}>
      {openCalendar ? (
        <SelectDate
          allowRangeSelection={false}
          attrName={attrName}
          updateMasterState={updateMasterState}
          props={props}
          modal={true}
        />
      ) : imageModal ? (
        <ActionSheet modalVisible={true} handleSheet={handleSheet} />
      ) : (
        <Modal
          animationType={'slide'}
          transparent={true}
          style={{backgroundColor: COLOR.BLACK}}
          visible={modalVisible}
          onRequestClose={() => {
            console.log('Modal has been closed.');
          }}>
          <View style={styles.modal}>
            <View style={styles.modalBody}>
              <NavigationBar
                prop={props}
                handleBack={() => {
                  setModalVisible(false);
                  navigation.goBack();
                }}
                left_image={IMAGES.back_arrow}
                navHeight={40}
                name={'Travellers Insurance'}
              />
               <KeyboardAwareScrollView
            // style={{flex:1}}
            // containerStyles={{flex:1}}
            >
              <View style={{flex: 1}}>
                <CustomTextInput
                  value={geographicalLimit}
                  updateState={setGeographicalLimit}
                  label={'Geographical Limit'}
                />
                <Text
                  style={{
                    fontSize: 13,
                    fontFamily: FONTS.FAMILY_SEMIBOLD,
                    marginVertical: 5,
                  }}>
                  INSURANCE DATES
                </Text>
                <View style={[styles.borderView, {marginBottom: 15}]}>
                  <TextInput
                  inputAccessoryViewID={'uniqueID'}
                    ref={fromRef}
                    style={styles.textInput}
                    placeholder={'Start Date'}
                    value={from}
                    onFocus={() => {
                      setAttrName('from');
                      setOpenCalendar(true);
                      fromRef.current.blur();
                    }}
                    placeholderTextColor={COLOR.LIGHT_TEXT_COLOR}
                  />
                  <View
                    style={{
                      width: '100%',
                      height: 1,
                      backgroundColor: '#C0C7CB',
                    }}
                  />
                  <TextInput
                  inputAccessoryViewID={'uniqueID'}
                    ref={toRef}
                    style={styles.textInput}
                    placeholder={'End Date'}
                    value={to}
                    onFocus={() => {
                      setAttrName('to');
                      setOpenCalendar(true);
                      toRef.current.blur();
                    }}
                    placeholderTextColor={COLOR.LIGHT_TEXT_COLOR}
                  />
                  <DoneButtonKeyboard />
                </View>
                <CustomTextInput
                  value={employer}
                  updateState={setEmployer}
                  label={'Employer '}
                />
                <AddonsSelectionView
                  selected={documents.findIndex((v) => v.type == 1) != -1}
                  label={'National ID'}
                  onPress={() => openImageModal(1)}
                />
                <AddonsSelectionView
                  selected={documents.findIndex((v) => v.type == 2) != -1}
                  label={'Family ID (If Applicable)'}
                  onPress={() => openImageModal(2)}
                />
                <AddonsSelectionView
                  selected={documents.findIndex((v) => v.type == 3) != -1}
                  label={'Passport'}
                  onPress={() => openImageModal(3)}
                />
              </View>
              </KeyboardAwareScrollView>
              <TouchableOpacity
                style={{
                  width: wp(90),
                  alignSelf: 'center',
                  height: 50,
                  alignItems: 'center',
                  justifyContent: 'center',
                  backgroundColor: COLOR.GREEN,
                  borderRadius: 10,
                  marginBottom: 20,
                }}
                onPress={() => {
                  handleNavigation();
                }}>
                <Text
                  style={[
                    Styles.button_font,
                    {
                      color: COLOR.WHITE,
                      width: null,
                      textAlign: 'center',
                    },
                  ]}>
                  Continue
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        
        </Modal>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  modal: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
  },
  modalBody: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    backgroundColor: COLOR.WHITE,
    borderWidth: 1,
    width: wp(100),
    marginTop: 50,
    borderColor: COLOR.BORDER,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    flex: 1,
  },
  image: {
    height: 120,
    width: 120,
    alignSelf: 'center',
    marginVertical: 20,
  },

  borderView: {
    borderRadius: 10,
    borderWidth: 1,
    width: wp(90),
    borderColor: '#C0C7CB',
    alignSelf: 'center',
  },
  textInput: {
    height: 50,
    fontFamily: FONTS.FAMILY_REGULAR,
    fontSize: 14,
    paddingHorizontal: 10,
  },
});
