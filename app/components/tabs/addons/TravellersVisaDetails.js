import React, {useEffect, useState} from 'react';
import {Modal, Text, View, StyleSheet, Image} from 'react-native';
import {TouchableOpacity} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import COLOR from '../../styles/Color';
import FONTS from '../../styles/Fonts';
import IMAGES from '../../styles/Images';
import Styles from '../../styles/Styles';
import NavigationBar from '../../commonView/NavigationBar';
import {useNavigation, useRoute} from '@react-navigation/native';
import ActionSheet from '../../commonView/ActionSheet';
import {TextInput} from 'react-native-gesture-handler';
import AddonsSelectionView from '../../commonView/AddonsSelectionView';
import DropDownPicker from 'react-native-dropdown-picker';
import {useDispatch, useSelector} from 'react-redux';
import ProfileAction from '../../../redux/action/ProfileAction';
import ImagePicker from 'react-native-image-crop-picker';
import API from '../../../api/Api';
import SimpleToast from 'react-native-simple-toast';
import { DoneButtonKeyboard } from '../../commonView/CommonView';

const api = new API();
export default function TravellersVisaDetails(props) {
  const dispatch = useDispatch();
  const countryData = useSelector((state) => state.profileReducer.countries);
  const navigation = useNavigation();
  const routes = useRoute();
  const traveller = routes.params.item;
  const [modalVisible, setModalVisible] = useState(true);
  const [countries, setCountries] = useState([]);
  const [dropdownVisibility, setDropdownVisibility] = useState(false);
  const [nationality, setNationality] = useState();
  const [documents, setDocuments] = useState([]);
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [imageModal, setImageModal] = useState(false);
  const [imageIndex, setImageIndex] = useState();

  useEffect(() => {
    dispatch(ProfileAction.getCountries());
  }, []);

  useEffect(() => {
    if (countryData) {
      const countries = countryData.map(function (value) {
        let object = {
          label: value.name,
          value: value.id,
          id: value.id,
        };
        return object;
      });
      setCountries(countries);
    }
  }, [countryData]);

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      setModalVisible(true);
    });
    return unsubscribe;
  }, [navigation]);

  const handleSheet = (val) => {
    if (val == 1) {
      chooseImage(1);
    } else if (val == 2) {
      chooseImage(2);
    } else if (val == 3) {
      setImageModal(false);
    }
  };

  const chooseImage = (val) => {
    let options = {
      includeBase64:true,
      title: 'Select Image',
      width: 1630,
      height: 750,
      compressImageMaxWidth: 1630,
      compressImageMaxHeight: 750,
      multiple: true,
    };

    if (val == 1) {
      ImagePicker.openCamera({
        title: 'Select Image',
        width: 1024,
        height: 472,
        compressImageMaxWidth: 1024,
        compressImageMaxHeight: 472,
        includeBase64:true,
      }).then((response) => {
        console.log('image local reposnse', response);
        setImageModal(false);
        callUploadImage(['data:image/jpeg;base64,' + response.data], val);
      });
    } else if (val == 2) {
      ImagePicker.openPicker(options).then((response) => {
        console.log('image local reposnse', response);
        setImageModal(false);
        let base64Response = response.map((val)=>{
          return 'data:image/jpeg;base64,' + val.data
      });
      callUploadImage(base64Response, val);  
      });
    }
  };

  const callUploadImage = (res, val) => {
    // var data = new FormData();
    // var photo = {
    //   uri: res.path,
    //   type: res.type ? res.type : res.mime,
    //   name: 'imgs.jpg',
    // };

    // data.append('path', 'passport');
    // data.append('images[]', photo);
    // console.log('image uploading data is:-', photo);

    let data = {
      images : res,
      path : 'passport'
    }

    const api = new API();
    api
      .uploadImage(data)
      .then((json) => {
        console.log('upload responce:-', json.data);

        if (json.status == 200) {
          console.log('Success');
          let object = {
            type: imageIndex,
            path: json.data.response[0],
          };
          console.log(object);
          let index = documents.findIndex((value) => value.type == imageIndex);
          if (index == -1) {
            let array = [...documents];
            array.push(object);
            setDocuments(array);
          } else {
            let array = [...documents];
            array[index] = object;
            setDocuments(array);
          }
        }
      })
      .catch((error) => {
        console.log('error:-', error);
      });
  };

  const openImageModal = (id) => {
    setImageIndex(id);
    setImageModal(true);
  };

  const handleNavigation = () => {
    if (firstName == '') {
      SimpleToast.show("Enter Mother's first Name");
    } else if (lastName == '') {
      SimpleToast.show("Enter Mother's last Name");
    } else if (nationality == null || nationality == undefined) {
      SimpleToast.show('Select Nationality');
    } else {
      setModalVisible(false);
      let payload = {
        trip_addon_id: 2,
        documents: documents,
        mother_first_name: firstName,
        mother_last_name: lastName,
        base_country_id: nationality,
        traveller_id: traveller.id,
        val: routes.params.val,
        type: routes.params.type,
        pop: routes.params.pop,
      };
      if (routes.params.traveller) {
        api
          .addAddons(
            routes.params.type,
            routes.params.val,
            JSON.stringify(payload),
          )
          .then((json) => {
            setModalVisible(false);
            console.log('visa ', json.data.response);
            SimpleToast.show('Added Successfully');
            navigation.goBack();
          })
          .catch((error) => {
            console.log(error.response.data);
          });
      } else {
        navigation.navigate('VisaSponsor', {...payload, ...routes.params});
      }
    }
  };

  return (
    <View style={Styles.container}>
      {imageModal ? (
        <ActionSheet modalVisible={true} handleSheet={handleSheet} />
      ) : (
        <Modal
          animationType={'slide'}
          transparent={true}
          style={{backgroundColor: COLOR.BLACK}}
          visible={modalVisible}
          onRequestClose={() => {
            console.log('Modal has been closed.');
          }}>
          <View style={styles.modal}>
            <View style={styles.modalBody}>
              <NavigationBar
                prop={props}
                handleBack={() => {
                  setModalVisible(false);
                  navigation.goBack();
                }}
                left_image={IMAGES.back_arrow}
                navHeight={40}
                name={'Travellers Details'}
              />
              <View style={{flex: 1}}>
                <Text
                  style={{
                    fontSize: 13,
                    fontFamily: FONTS.FAMILY_SEMIBOLD,
                    marginVertical: 10,
                  }}>
                  TRAVELLER
                </Text>
                <View style={styles.borderView}>
                  <Text
                    style={{
                      fontSize: 16,
                      fontFamily: FONTS.FAMILY_SEMIBOLD,
                      marginVertical: 15,
                      marginHorizontal: 10,
                    }}>
                    {traveller.first_name + ' ' + traveller.last_name}
                  </Text>
                </View>
                <Text
                  style={{
                    fontSize: 13,
                    fontFamily: FONTS.FAMILY_SEMIBOLD,
                    marginTop: 15,
                    marginBottom: 5,
                  }}>
                  MOTHER'S DETAILS
                </Text>
                <View
                  style={[styles.borderView, {marginBottom: 15, zIndex: 1000}]}>
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                    }}>
                    <TextInput
                    inputAccessoryViewID={'uniqueID'}
                      style={[styles.textInput, {width: '49%'}]}
                      placeholder={'First Name'}
                      value={firstName}
                      onChangeText={(text) => setFirstName(text)}
                      placeholderTextColor={COLOR.LIGHT_TEXT_COLOR}
                    />
                    <View
                      style={{
                        height: 35,
                        width: 1,
                        backgroundColor: '#C0C7CB',
                        alignSelf: 'center',
                      }}
                    />
                    <DoneButtonKeyboard />
                    <TextInput
                    inputAccessoryViewID={'uniqueID'}
                      style={[styles.textInput, {width: '49%'}]}
                      placeholder={'Last Name'}
                      value={lastName}
                      onChangeText={(text) => setLastName(text)}
                      placeholderTextColor={COLOR.LIGHT_TEXT_COLOR}
                    />
                  </View>

                  <View
                    style={{
                      width: '100%',
                      height: 1,
                      backgroundColor: '#C0C7CB',
                    }}
                  />
                  <DropDownPicker
                    showArrowIcon={false}
                    open={dropdownVisibility}
                    setOpen={() => setDropdownVisibility(!dropdownVisibility)}
                    labelStyle={{
                      fontSize: FONTS.MEDIUM,
                      fontFamily: FONTS.FAMILY_REGULAR,
                      color: COLOR.BLACK,
                      marginLeft: -5,
                    }}
                    dropDownMaxHeight={300}
                    style={{
                      backgroundColor: '#ffffff',
                      shadowOpacity: 0,
                      borderWidth: 0,
                      height: 50,
                      alignSelf: 'center',
                      borderTopLeftRadius: 0,
                      borderTopRightRadius: 0,
                      borderBottomLeftRadius: 10,
                      borderBottomRightRadius: 10,
                      paddingHorizontal: 10,
                    }}
                    dropDownStyle={{
                      backgroundColor: '#fff',
                    }}
                    containerStyle={{
                      borderRadius: 0,
                    }}
                    setValue={setNationality}
                    value={nationality}
                    textStyle={{
                      fontFamily: FONTS.FAMILY_REGULAR,
                    }}
                    placeholder={'Select Nationality'}
                    labelStyle={{
                      fontFamily: FONTS.FAMILY_REGULAR,
                    }}
                    items={countries}
                  />
                </View>
                <AddonsSelectionView
                  selected={documents.findIndex((v) => v.type == 1) != -1}
                  label={'Personal Photo'}
                  onPress={() => openImageModal(1)}
                />
                <AddonsSelectionView
                  selected={documents.findIndex((v) => v.type == 2) != -1}
                  label={'National ID'}
                  onPress={() => openImageModal(2)}
                />
                <AddonsSelectionView
                  selected={documents.findIndex((v) => v.type == 3) != -1}
                  label={'Family ID (If Applicable)'}
                  onPress={() => openImageModal(3)}
                />
                <AddonsSelectionView
                  selected={documents.findIndex((v) => v.type == 4) != -1}
                  label={'Passport'}
                  onPress={() => openImageModal(4)}
                />
              </View>
              <TouchableOpacity
                style={{
                  width: wp(90),
                  alignSelf: 'center',
                  height: 50,
                  alignItems: 'center',
                  justifyContent: 'center',
                  backgroundColor: COLOR.GREEN,
                  borderRadius: 10,
                  marginBottom: 20,
                }}
                onPress={() => handleNavigation()}>
                <Text
                  style={[
                    Styles.button_font,
                    {
                      color: COLOR.WHITE,
                      width: null,
                      textAlign: 'center',
                    },
                  ]}>
                  Continue
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  modal: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
  },
  modalBody: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    backgroundColor: COLOR.WHITE,
    borderWidth: 1,
    width: wp(100),
    marginTop: 50,
    borderColor: COLOR.BORDER,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    flex: 1,
  },
  image: {
    height: 120,
    width: 120,
    alignSelf: 'center',
    marginVertical: 20,
  },

  borderView: {
    borderRadius: 10,
    borderWidth: 1,
    width: wp(90),
    borderColor: '#C0C7CB',
    alignSelf: 'center',
  },
  textInput: {
    height: 50,
    fontFamily: FONTS.FAMILY_REGULAR,
    fontSize: 14,
    paddingHorizontal: 10,
  },
});
