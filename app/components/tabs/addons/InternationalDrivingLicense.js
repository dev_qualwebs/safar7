import React, {useEffect, useState} from 'react';
import {Modal, Text, View, StyleSheet, Image} from 'react-native';
import {FlatList, TouchableOpacity} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import COLOR from '../../styles/Color';
import FONTS from '../../styles/Fonts';
import IMAGES from '../../styles/Images';
import Styles from '../../styles/Styles';
import NavigationBar from '../../commonView/NavigationBar';
import {useNavigation, useRoute} from '@react-navigation/native';
import CustomTextInput from '../../commonView/CustomTextInput';
import {TextInput} from 'react-native-gesture-handler';
import AddonsSelectionView from '../../commonView/AddonsSelectionView';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview';
import ImagePicker from 'react-native-image-crop-picker';
import API from '../../../api/Api';
import ActionSheet from '../../commonView/ActionSheet';
import SimpleToast from 'react-native-simple-toast';
import ProfileAction from '../../../redux/action/ProfileAction';
import {useDispatch, useSelector} from 'react-redux';
import DropDownPicker from 'react-native-dropdown-picker';

const api = new API();
export default function InternationalDrivingLicense(props) {
  const navigation = useNavigation();
  const route = useRoute();
  const dispatch = useDispatch();

  const countryData = useSelector((state) => state.profileReducer.countries);
  const [modalVisible, setModalVisible] = useState(true);
  const [name, setName] = useState('');
  const [arabicName, setArabicName] = useState('');
  const [country, setCountry] = useState(null);
  const [nationality, setNationality] = useState(null);
  const [countries, setCountries] = useState([]);
  const [mobile, setMobile] = useState('');
  const [imageModal, setImageModal] = useState(false);
  const [imageIndex, setImageIndex] = useState();
  const [documents, setDocuments] = useState([]);
  const [countryDropdown, setCountryDropdown] = useState(false);
  const [nationalityDropdown, setNationalityDropdown] = useState(false);

  useEffect(() => {
    dispatch(ProfileAction.getCountries());
  }, []);

  useEffect(() => {
    if (countryData) {
      const countries = countryData.map(function (value) {
        let object = {
          label: value.name,
          value: value.id,
          id: value.id,
        };
        return object;
      });
      setCountries(countries);
    }
  }, [countryData]);

  const handleSheet = (val) => {
    if (val == 1) {
      chooseImage(1);
    } else if (val == 2) {
      chooseImage(2);
    } else if (val == 3) {
      setImageModal(false);
    }
  };

  const chooseImage = (val) => {
    let options = {
      includeBase64:true,
      title: 'Select Image',
      width: 1630,
      height: 750,
      compressImageMaxWidth: 1630,
      compressImageMaxHeight: 750,
      multiple: false,
    };

    if (val == 1) {
      ImagePicker.openCamera({
        title: 'Select Image',
        width: 1024,
        height: 472,
        compressImageMaxWidth: 1024,
        compressImageMaxHeight: 472,
        includeBase64:true,
      }).then((response) => {
        console.log('image local reposnse', response);
        setImageModal(false);
        callUploadImage(['data:image/jpeg;base64,' + response.data], val);
      });
    } else if (val == 2) {
      ImagePicker.openPicker(options).then((response) => {
        console.log('image local reposnse', response);
        setImageModal(false);

        callUploadImage(['data:image/jpeg;base64,' + response.data], val);
      });
    }
  };

  const callUploadImage = (res, val) => {
    // var data = new FormData();
    // var photo = {
    //   uri: res.path,
    //   type: res.type ? res.type : res.mime,
    //   name: 'imgs.jpg',
    // };

    // data.append('path', 'passport');
    // data.append('images[]', photo);
    // console.log('image uploading data is:-', photo);

    let data = {
      images : res,
      path : 'passport'
    }

    const api = new API();
    api
      .uploadImage(data)
      .then((json) => {
        console.log('upload responce:-', json.data);

        if (json.status == 200) {
          console.log('Success');
          let object = {
            type: imageIndex,
            path: json.data.response[0],
          };
          console.log(object);
          let index = documents.findIndex((value) => value.type == imageIndex);
          if (index == -1) {
            let array = [...documents];
            array.push(object);
            setDocuments(array);
          } else {
            let array = [...documents];
            array[index] = object;
            setDocuments(array);
          }
        }
      })
      .catch((error) => {
        console.log('error:-', error);
      });
  };

  const openImageModal = (id) => {
    setImageIndex(id);
    setImageModal(true);
  };

  const handleNavigation = () => {
    if (name == '') {
      SimpleToast.show('Enter Name');
    } else if (arabicName == '') {
      SimpleToast.show('Enter Arabic Name');
    } else if (nationality == null || nationality == undefined) {
      SimpleToast.show('Select Nationality');
    } else if (country == null || country == undefined) {
      SimpleToast.show('Select Country');
    } else if (mobile == '') {
      SimpleToast.show('Enter Mobile Number');
    } else if (documents.length != 3) {
      SimpleToast.show('Upload All Documents');
    } else {
      let payload = JSON.stringify({
        trip_addon_id: 10,
        traveller_id: route.params.item.id,
        name_in_base_country: name,
        name_in_other_country: arabicName,
        base_country: country,
        nationality: nationality,
        phone: mobile,
        documents: documents,
      });

      api
        .addAddons(route.params.type, route.params.val, payload)
        .then((json) => {
          console.log('licence', json.data.response);
          setModalVisible(false);
          navigation.goBack();
        })
        .catch((error) => {
          console.log(error);
          SimpleToast.show(error.response.data.message);
        });
    }
  };

  return (
    <View style={Styles.container}>
      {imageModal ? (
        <ActionSheet modalVisible={true} handleSheet={handleSheet} />
      ) : (
        <Modal
          animationType={'slide'}
          transparent={true}
          style={{backgroundColor: COLOR.BLACK}}
          visible={modalVisible}
          onRequestClose={() => {
            console.log('Modal has been closed.');
          }}>
          <View style={styles.modal}>
            <View style={styles.modalBody}>
              <NavigationBar
                prop={props}
                handleBack={() => {
                  setModalVisible(false);
                  navigation.goBack();
                }}
                left_image={IMAGES.back_arrow}
                navHeight={40}
                name={'Travellers Details'}
              />
              <KeyboardAwareScrollView style={{flex: 1}}>
                <CustomTextInput
                  value={name}
                  updateState={setName}
                  label={'Name in English'}
                />
                <CustomTextInput
                  value={arabicName}
                  updateState={setArabicName}
                  label={'Name in Arabic'}
                />
                <View
                  style={[
                    styles.borderView,
                    {zIndex: 1000, marginVertical: 10},
                  ]}>
                  <DropDownPicker
                    showArrowIcon={false}
                    open={nationalityDropdown}
                    setOpen={() => setNationalityDropdown(!nationalityDropdown)}
                    labelStyle={{
                      fontSize: FONTS.MEDIUM,
                      fontFamily: FONTS.FAMILY_REGULAR,
                      color: COLOR.BLACK,
                      marginLeft: -5,
                    }}
                    dropDownMaxHeight={300}
                    style={{
                      backgroundColor: '#ffffff',
                      shadowOpacity: 0,
                      borderWidth: 0,
                      height: 50,
                      alignSelf: 'center',
                      borderTopLeftRadius: 10,
                      borderTopRightRadius: 10,
                      borderBottomLeftRadius: 10,
                      borderBottomRightRadius: 10,
                      paddingHorizontal: 10,
                    }}
                    dropDownStyle={{
                      backgroundColor: '#fff',
                    }}
                    containerStyle={{
                      borderRadius: 0,
                    }}
                    setValue={setNationality}
                    value={nationality}
                    textStyle={{
                      fontFamily: FONTS.FAMILY_REGULAR,
                    }}
                    placeholder={'Select Nationality'}
                    labelStyle={{
                      fontFamily: FONTS.FAMILY_REGULAR,
                    }}
                    items={countries}
                  />
                </View>
                <View
                  style={[
                    styles.borderView,
                    {zIndex: 900, marginVertical: 10},
                  ]}>
                  <DropDownPicker
                    showArrowIcon={false}
                    open={countryDropdown}
                    setOpen={() => setCountryDropdown(!countryDropdown)}
                    labelStyle={{
                      fontSize: FONTS.MEDIUM,
                      fontFamily: FONTS.FAMILY_REGULAR,
                      color: COLOR.BLACK,
                      marginLeft: -5,
                    }}
                    dropDownMaxHeight={300}
                    style={{
                      backgroundColor: '#ffffff',
                      shadowOpacity: 0,
                      borderWidth: 0,
                      height: 50,
                      alignSelf: 'center',
                      borderTopLeftRadius: 10,
                      borderTopRightRadius: 10,
                      borderBottomLeftRadius: 10,
                      borderBottomRightRadius: 10,
                      paddingHorizontal: 10,
                    }}
                    dropDownStyle={{
                      backgroundColor: '#fff',
                    }}
                    containerStyle={{
                      borderRadius: 0,
                    }}
                    setValue={setCountry}
                    value={country}
                    textStyle={{
                      fontFamily: FONTS.FAMILY_REGULAR,
                    }}
                    placeholder={'Select Country of Residency'}
                    labelStyle={{
                      fontFamily: FONTS.FAMILY_REGULAR,
                    }}
                    items={countries}
                  />
                </View>

                <CustomTextInput
                  value={mobile}
                  updateState={setMobile}
                  label={'Mobile Number'}
                />

                <AddonsSelectionView
                  selected={documents.findIndex((v) => v.type == 1) != -1}
                  label={'Photo'}
                  onPress={() => openImageModal(1)}
                />
                <AddonsSelectionView
                  selected={documents.findIndex((v) => v.type == 2) != -1}
                  label={'Driving License'}
                  onPress={() => openImageModal(2)}
                />
                <AddonsSelectionView
                  selected={documents.findIndex((v) => v.type == 3) != -1}
                  label={'National ID'}
                  onPress={() => openImageModal(3)}
                />
              </KeyboardAwareScrollView>
              <TouchableOpacity
                style={{
                  width: wp(90),
                  alignSelf: 'center',
                  height: 50,
                  alignItems: 'center',
                  justifyContent: 'center',
                  backgroundColor: COLOR.GREEN,
                  borderRadius: 10,
                  marginBottom: 20,
                }}
                onPress={() => {
                  handleNavigation();
                }}>
                <Text
                  style={[
                    Styles.button_font,
                    {
                      color: COLOR.WHITE,
                      width: null,
                      textAlign: 'center',
                    },
                  ]}>
                  Continue
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  modal: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
  },
  modalBody: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    backgroundColor: COLOR.WHITE,
    borderWidth: 1,
    width: wp(100),
    marginTop: 50,
    borderColor: COLOR.BORDER,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    flex: 1,
  },
  image: {
    height: 120,
    width: 120,
    alignSelf: 'center',
    marginVertical: 20,
  },

  borderView: {
    borderRadius: 10,
    borderWidth: 1,
    width: wp(90),
    borderColor: '#C0C7CB',
    alignSelf: 'center',
  },
  textInput: {
    height: 50,
    fontFamily: FONTS.FAMILY_REGULAR,
    fontSize: 14,
    paddingHorizontal: 10,
  },
});
