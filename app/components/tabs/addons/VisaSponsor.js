import React, {useState, useEffect} from 'react';
import {StyleSheet, View, Modal, Text, TouchableOpacity} from 'react-native';
import COLOR from '../../styles/Color';
import {
  widthPercentageToDP,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import FONTS from '../../styles/Fonts';
import IMAGES from '../../styles/Images';
import Styles from '../../styles/Styles';
import NavigationBar from '../../commonView/NavigationBar';
import {useNavigation, useRoute} from '@react-navigation/native';
import API from '../../../api/Api';
import DropDownPicker from 'react-native-dropdown-picker';
import {ScrollView} from 'react-native';
import {useSelector} from 'react-redux';
import ImagePicker from 'react-native-image-crop-picker';
import AddonsSelectionView from '../../commonView/AddonsSelectionView';
import SimpleToast from 'react-native-simple-toast';

const relationData = [
  {label: 'Family', value: 1},
  {label: 'Friend', value: 2},
];
const api = new API();
export default function VisaSponsor(props) {
  const [modalVisible, setModalVisible] = useState(true);
  const navigation = useNavigation();
  const routes = useRoute();
  const [relationIndex, setRelationIndex] = useState(-1);
  const travellers = useSelector(
    (state) => state.profileReducer.travellersData,
  );
  const [travellersData, setTravellersData] = useState([]);
  const [sponsor, setSponsor] = useState();
  const [sponsorDropdown, setSponsorDropdown] = useState(false);
  const [imageModal, setImageModal] = useState(false);
  const [imageIndex, setImageIndex] = useState();
  const [documents, setDocuments] = useState([]);

  useEffect(() => {
    let mapData = [...travellers];
    let index = mapData.findIndex((value) => value.is_default_user == 1);
    if (index != -1) {
      mapData.splice(index, 1);
    }
    let data = mapData.map((value) => {
      let object = {
        traveller_id: value.id,
        label: value.first_name + ' ' + value.last_name,
        relation_id: 0,
        value: value.id,
      };
      return object;
    });

    setTravellersData(data);
  }, [travellers]);

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      setModalVisible(true);
    });
    return unsubscribe;
  }, [navigation]);

  const handleSheet = (val) => {
    if (val == 1) {
      chooseImage(1);
    } else if (val == 2) {
      chooseImage(2);
    } else if (val == 3) {
      setImageModal(false);
    }
  };

  const chooseImage = (val) => {
    let options = {
      includeBase64:true,
      title: 'Select Image',
      width: 1630,
      height: 750,
      compressImageMaxWidth: 1630,
      compressImageMaxHeight: 750,
      multiple: true,
    };

    if (val == 1) {
      ImagePicker.openCamera({
        title: 'Select Image',
        width: 1024,
        height: 472,
        compressImageMaxWidth: 1024,
        compressImageMaxHeight: 472,
        includeBase64:true,
      }).then((response) => {
        console.log('image local reposnse', response);
        setImageModal(false);
        callUploadImage(['data:image/jpeg;base64,' + response.data], val);
      });
    } else if (val == 2) {
      ImagePicker.openPicker(options).then((response) => {
        console.log('image local reposnse', response);
        setImageModal(false);
        let base64Response = response.map((val)=>{
          return 'data:image/jpeg;base64,' + val.data
      });
      callUploadImage(base64Response, val);  
      });
    }
  };

  const callUploadImage = (res, val) => {
    let data = {
      images : res,
      path : 'passport'
    }

    const api = new API();
    api
      .uploadImage(data)
      .then((json) => {
        console.log('upload responce:-', json.data);

        if (json.status == 200) {
          console.log('Success');
          let object = {
            type: imageIndex,
            path: json.data.response[0],
          };
          console.log(object);
          let index = documents.findIndex((value) => value.type == imageIndex);
          if (index == -1) {
            let array = [...documents];
            array.push(object);
            setDocuments(array);
          } else {
            let array = [...documents];
            array[index] = object;
            setDocuments(array);
          }
        }
      })
      .catch((error) => {
        console.log('error:-', error);
      });
  };

  const openImageModal = (id) => {
    setImageIndex(id);
    setImageModal(true);
  };

  const handleNavigation = () => {
    if (travellersData.some((value) => value.relation_id == 0)) {
      SimpleToast.show('Select Relation for all Travellers');
    } else if (sponsor == null || sponsor == undefined) {
      SimpleToast.show('Select Sponsor');
    } else {
      setModalVisible(false);
      let sponsorObject = {
        sponsor_id: sponsor,
        relations: travellersData,
        documents: documents,
      };

      navigation.navigate('VisaFinalStep', {
        ...routes.params,
        sponsor: sponsorObject,
      });
    }
  };

  return (
    <View style={Styles.container}>
      {imageModal ? (
        <ActionSheet modalVisible={true} handleSheet={handleSheet} />
      ) : (
        <Modal
          animationType={'slide'}
          transparent={true}
          style={{backgroundColor: COLOR.BLACK}}
          visible={modalVisible}
          onRequestClose={() => {
            console.log('Modal has been closed.');
          }}>
          <View style={styles.modal}>
            <View style={styles.modalBody}>
              <NavigationBar
                prop={props}
                left_image={IMAGES.back_arrow}
                navHeight={40}
                handleBack={() => {
                  setModalVisible(false);
                  navigation.goBack();
                }}
                name={'Sponsor'}
              />
              <ScrollView
                style={{
                  alignSelf: 'center',
                  flex: 1,
                }}
                contentContainerStyle={{flex: 1}}>
                <View
                  style={[
                    styles.borderView,
                    {marginVertical: 15, zIndex: 100},
                  ]}>
                  <Text
                    style={{
                      fontFamily: FONTS.FAMILY_REGULAR,
                      color: '#8A9AA2',
                      marginVertical: 10,
                      marginHorizontal: 12,
                    }}>
                    Relation with Travellers
                  </Text>
                  {travellersData.length > 0 && travellersData
                    ? travellersData.map((item, index) => {
                        return (
                          <View style={{zIndex: 500 - index}}>
                            <View
                              style={{
                                width: widthPercentageToDP(90),
                                alignSelf: 'center',
                                justifyContent: 'space-between',
                                alignItems: 'center',
                                zIndex: 1000,
                                flexDirection: 'row',
                                paddingHorizontal: 10,
                                marginVertical: 8,
                              }}>
                              <Text
                                style={{
                                  fontFamily: FONTS.FAMILY_REGULAR,
                                }}>
                                {item ? item.label : ''}
                              </Text>
                              <DropDownPicker
                                showArrowIcon={false}
                                open={relationIndex == index}
                                setOpen={() =>
                                  relationIndex == index
                                    ? setRelationIndex(-1)
                                    : setRelationIndex(index)
                                }
                                labelStyle={{
                                  fontSize: FONTS.MEDIUM,
                                  fontFamily: FONTS.FAMILY_REGULAR,
                                  color: COLOR.BLACK,
                                  marginLeft: -5,
                                }}
                                dropDownMaxHeight={300}
                                style={{
                                  backgroundColor: '#ffffff',
                                  shadowOpacity: 0,
                                  borderWidth: 1,
                                  borderColor: COLOR.BORDER,
                                  height: 50,
                                  width: widthPercentageToDP(40),
                                  alignSelf: 'center',
                                  borderTopLeftRadius: 10,
                                  borderTopRightRadius: 10,
                                  borderBottomLeftRadius: 10,
                                  borderBottomRightRadius: 10,
                                  paddingHorizontal: 10,
                                }}
                                dropDownStyle={{
                                  backgroundColor: '#fff',
                                  width: widthPercentageToDP(40),
                                }}
                                containerStyle={{
                                  borderRadius: 0,
                                  width: widthPercentageToDP(40),
                                }}
                                setValue={(data) => {
                                  let values = [...travellersData];
                                  values[index].relation_id = data();

                                  setTravellersData(values);
                                }}
                                value={item.relation_id}
                                textStyle={{
                                  fontFamily: FONTS.FAMILY_REGULAR,
                                }}
                                placeholder={'Select Relation'}
                                labelStyle={{
                                  fontFamily: FONTS.FAMILY_REGULAR,
                                }}
                                items={relationData}
                              />
                            </View>
                            <View style={styles.singleLineView} />
                          </View>
                        );
                      })
                    : null}
                </View>
                <View
                  style={[styles.borderView, {marginBottom: 15, zIndex: 50}]}>
                  <DropDownPicker
                    showArrowIcon={false}
                    open={sponsorDropdown}
                    setOpen={() => setSponsorDropdown(!sponsorDropdown)}
                    labelStyle={{
                      fontSize: FONTS.MEDIUM,
                      fontFamily: FONTS.FAMILY_REGULAR,
                      color: COLOR.BLACK,
                      marginLeft: -5,
                    }}
                    dropDownMaxHeight={300}
                    style={{
                      backgroundColor: '#ffffff',
                      shadowOpacity: 0,
                      borderWidth: 0,
                      height: 50,
                      alignSelf: 'center',
                      borderTopLeftRadius: 10,
                      borderTopRightRadius: 10,
                      borderBottomLeftRadius: 10,
                      borderBottomRightRadius: 10,
                      paddingHorizontal: 10,
                    }}
                    dropDownStyle={{
                      backgroundColor: '#fff',
                    }}
                    containerStyle={{
                      borderRadius: 0,
                    }}
                    setValue={setSponsor}
                    value={sponsor}
                    textStyle={{
                      fontFamily: FONTS.FAMILY_REGULAR,
                    }}
                    placeholder={'Select Sponsor'}
                    labelStyle={{
                      fontFamily: FONTS.FAMILY_REGULAR,
                    }}
                    items={travellersData}
                  />
                </View>
                <AddonsSelectionView
                  selected={documents.findIndex((v) => v.type == 1) != -1}
                  label={'Passport of the Sponsor'}
                  onPress={() => openImageModal(1)}
                />
                <AddonsSelectionView
                  selected={documents.findIndex((v) => v.type == 2) != -1}
                  label={'Job Letter'}
                  onPress={() => openImageModal(2)}
                />
                <AddonsSelectionView
                  selected={documents.findIndex((v) => v.type == 2) != -1}
                  label={'Bank Statement (Last Page Only)'}
                  onPress={() => openImageModal(2)}
                />
              </ScrollView>

              <TouchableOpacity
                onPress={() => handleNavigation()}
                style={{
                  width: wp(90),
                  alignSelf: 'center',
                  height: 50,
                  alignItems: 'center',
                  justifyContent: 'center',
                  backgroundColor: COLOR.GREEN,
                  borderRadius: 10,
                  marginBottom: 20,
                }}>
                <Text
                  style={[
                    Styles.button_font,
                    {
                      color: COLOR.WHITE,
                      width: null,
                      textAlign: 'center',
                    },
                  ]}>
                  Apply
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  modal: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
  },
  modalBody: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    backgroundColor: COLOR.WHITE,
    borderWidth: 1,
    width: wp(100),
    marginTop: 50,
    borderColor: COLOR.BORDER,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    flex: 1,
  },

  headingLabel: {
    fontFamily: FONTS.FAMILY_BOLD,
    fontSize: FONTS.MEDIUM,
    color: COLOR.BLACK,
  },
  borderView: {
    borderRadius: 10,
    borderWidth: 1,
    width: wp(90),
    borderColor: '#C0C7CB',
    alignSelf: 'center',
  },
  singleLineView: {
    borderBottomColor: COLOR.GRAY,
    width: wp(100),
    alignSelf: 'center',
    borderBottomWidth: 1,
  },
});
