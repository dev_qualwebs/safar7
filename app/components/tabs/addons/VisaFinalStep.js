import React, {useState, useEffect} from 'react';
import {StyleSheet, View, Modal, Text, TouchableOpacity} from 'react-native';
import COLOR from '../../styles/Color';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import FONTS from '../../styles/Fonts';
import IMAGES from '../../styles/Images';
import Styles from '../../styles/Styles';
import NavigationBar from '../../commonView/NavigationBar';
import Switch from '../../commonView/Switch';
import {useNavigation, useRoute} from '@react-navigation/native';
import API from '../../../api/Api';
import SimpleToast from 'react-native-simple-toast';
import {FlatList} from 'react-native-gesture-handler';

const api = new API();
export default function VisaFinalStep(props) {
  const [modalVisible, setModalVisible] = useState(true);
  const navigation = useNavigation();
  const routes = useRoute();
  const [query, setQuery] = useState([]);

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      setModalVisible(true);
      console.log(routes.params);
      //routes.params.travel_country_id
      api
        .getVisaPreference('236') 
        .then((json) => {
          setQuery(
            json.data.response.map((value) => {
              value.active = 0;
              return value;
            }),
          );
        })
        .catch((error) => {
          console.log(error.response.data);
        });
    });
    return unsubscribe;
  }, [navigation]);

  const applyVisa = async () => {
    let payload = JSON.stringify({
      ...routes.params,
      preferences: query,
    });
    api
      .addAddons(routes.params.type, routes.params.val, payload)
      .then((json) => {
        console.log('visa ', json.data.response);
        SimpleToast.show('Added Successfully');
        setModalVisible(false);
        navigation.pop(4);
      })
      .catch((error) => {
        console.log(error.response.data);
      });
  };

  return (
    <View style={Styles.container}>
      <Modal
        animationType={'slide'}
        transparent={true}
        style={{backgroundColor: COLOR.BLACK}}
        visible={modalVisible}
        onRequestClose={() => {
          console.log('Modal has been closed.');
        }}>
        <View style={styles.modal}>
          <View style={styles.modalBody}>
            <NavigationBar
              prop={props}
              left_image={IMAGES.back_arrow}
              navHeight={40}
              handleBack={() => {
                setModalVisible(false);
                navigation.goBack();
              }}
              name={'Final Step'}
            />
            <FlatList
              data={query}
              renderItem={({item, index}) => (
                <>
                  <View style={styles.queryView}>
                    <Text
                      style={{
                        fontFamily: FONTS.FAMILY_SEMIBOLD,
                        fontSize: 16,
                        flex: 1,
                      }}>
                      {item.name}
                    </Text>
                    <Switch
                      isChecked={item.active == 1 ? true : false}
                      onCheckedListener={() => {
                        let data = [...query];
                        let object = item;
                        if (object.active == 1) {
                          object.active = 0;
                        } else {
                          object.active = 1;
                        }

                        data[index] = object;
                        setQuery([...data]);
                      }}
                    />
                  </View>
                  <View style={styles.singleLineView} />
                </>
              )}
              style={{
                alignSelf: 'center',
                flex: 1,
              }}
            />

            <TouchableOpacity
              onPress={() => applyVisa()}
              style={{
                width: wp(90),
                alignSelf: 'center',
                height: 50,
                alignItems: 'center',
                justifyContent: 'center',
                backgroundColor: COLOR.GREEN,
                borderRadius: 10,
                marginBottom: 20,
              }}>
              <Text
                style={[
                  Styles.button_font,
                  {
                    color: COLOR.WHITE,
                    width: null,
                    textAlign: 'center',
                  },
                ]}>
                Apply
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    </View>
  );
}

const styles = StyleSheet.create({
  modal: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
  },
  modalBody: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    backgroundColor: COLOR.WHITE,
    borderWidth: 1,
    width: wp(100),
    marginTop: 50,
    borderColor: COLOR.BORDER,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    flex: 1,
  },

  headingLabel: {
    fontFamily: FONTS.FAMILY_BOLD,
    fontSize: FONTS.MEDIUM,
    color: COLOR.BLACK,
  },
  singleLineView: {
    borderBottomColor: COLOR.GRAY,
    width: wp(100),
    alignSelf: 'center',
    borderBottomWidth: 1,
  },

  queryView: {
    width: wp(90),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginVertical: 15,
    alignSelf: 'center',
  },
});
