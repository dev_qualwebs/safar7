import React, {useEffect, useState} from 'react';
import {Modal, Text, View, StyleSheet, Image, Alert} from 'react-native';
import {FlatList, TouchableOpacity} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import COLOR from '../../styles/Color';
import FONTS from '../../styles/Fonts';
import IMAGES from '../../styles/Images';
import Styles from '../../styles/Styles';
import NavigationBar from '../../commonView/NavigationBar';
import {useNavigation, useRoute} from '@react-navigation/native';
import CustomTextInput from '../../commonView/CustomTextInput';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview';
import SimpleToast from 'react-native-simple-toast';
import API from '../../../api/Api';
import { connect } from 'react-redux';
import RequestAction from '../../../redux/action/RequestAction';

const api = new API();
function InsuranceAddress(props) {
  const navigation = useNavigation();
  const route = useRoute();

  const [modalVisible, setModalVisible] = useState(true);
  const [buildingNumber, setBuildingNumber] = useState('');
  const [additionalNumber, setAdditionalNumber] = useState('');
  const [street, setStreet] = useState('');
  const [address, setAddress] = useState('');
  const [city, setCity] = useState('');
  const [zipCode, setZipCode] = useState('');
  const [box, setBox] = useState('');

  const handleNavigation = () => {
    if (buildingNumber == '') {
      SimpleToast.show('Enter Building Number');
    } else if (additionalNumber == '') {
      SimpleToast.show('Enter Additional Number');
    } else if (street == '') {
      SimpleToast.show('Enter Street Address');
    } else if (address == '') {
      SimpleToast.show('Enter Address');
    } else if (city == '') {
      SimpleToast.show('Enter City');
    } else if (zipCode == '') {
      SimpleToast.show('Enter zipcode');
    } else if (box == '') {
      SimpleToast.show('Enter P.O Box');
    } else {
      let payload = JSON.stringify({
        ...route.params,
        trip_addon_id: 1,
        address: {
          building_number: buildingNumber,
          additional_number: additionalNumber,
          street: street,
          address: address,
          city: city,
          p_o_box: box,
          zip_code: zipCode,
        },
      });
      setModalVisible(false);
      api
        .addAddons(route.params.type, route.params.val, payload)
        .then((json) => {
          props.getAddons(route.params.type,route.params.val);
          navigation.pop(2);
        })
        .catch((error) => {
          console.log(error.response.data.message);
          SimpleToast.show(error.response.data.message);
        });
    }
  };

  return (
    <View style={Styles.container}>
      <Modal
        animationType={'slide'}
        transparent={true}
        style={{backgroundColor: COLOR.BLACK}}
        visible={modalVisible}
        onRequestClose={() => {
          console.log('Modal has been closed.');
        }}>
        <View style={styles.modal}>
          <View style={styles.modalBody}>
            <NavigationBar
              prop={props}
              handleBack={() => {
                setModalVisible(false);
                navigation.goBack();
              }}
              left_image={IMAGES.back_arrow}
              navHeight={40}
              name={'Address'}
            />
            <KeyboardAwareScrollView style={{flex: 1}}>
              <CustomTextInput
                value={buildingNumber}
                number={true}
                updateState={setBuildingNumber}
                label={'Building Number'}
              />
              <CustomTextInput
                value={additionalNumber}
                number={true}
                updateState={setAdditionalNumber}
                label={'Additional Number'}
              />
              <CustomTextInput
                value={street}
                updateState={setStreet}
                label={'Street Name'}
              />
              <CustomTextInput
                value={address}
                updateState={setAddress}
                label={'Address'}
              />
              <CustomTextInput
                value={city}
                updateState={setCity}
                label={'City'}
              />
              <CustomTextInput
                value={zipCode}
                number={true}
                updateState={setZipCode}
                label={'Zip Code'}
              />
              <CustomTextInput
                value={box}
                number={true}
                updateState={setBox}
                label={'P.O. Box'}
              />
            </KeyboardAwareScrollView>
            <TouchableOpacity
              style={{
                width: wp(90),
                alignSelf: 'center',
                height: 50,
                alignItems: 'center',
                justifyContent: 'center',
                backgroundColor: COLOR.GREEN,
                borderRadius: 10,
                marginBottom: 20,
              }}
              onPress={() => {
                handleNavigation();
              }}>
              <Text
                style={[
                  Styles.button_font,
                  {
                    color: COLOR.WHITE,
                    width: null,
                    textAlign: 'center',
                  },
                ]}>
                Continue
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    </View>
  );
}

const styles = StyleSheet.create({
  modal: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
  },
  modalBody: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    backgroundColor: COLOR.WHITE,
    borderWidth: 1,
    width: wp(100),
    marginTop: 50,
    borderColor: COLOR.BORDER,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    flex: 1,
  },
  image: {
    height: 120,
    width: 120,
    alignSelf: 'center',
    marginVertical: 20,
  },

  borderView: {
    borderRadius: 10,
    borderWidth: 1,
    width: wp(90),
    borderColor: '#C0C7CB',
    alignSelf: 'center',
  },
  textInput: {
    height: 50,
    fontFamily: FONTS.FAMILY_REGULAR,
    fontSize: 14,
    paddingHorizontal: 10,
  },
});

const mapDispatchToProps = (dispatch) => {
  return {
    getAddons: (type,val) => {
      dispatch(RequestAction.getTripAddons(type,val));
    },
  };
};

export default connect(null, mapDispatchToProps)(InsuranceAddress);
