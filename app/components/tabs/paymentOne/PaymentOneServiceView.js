import {useNavigation} from '@react-navigation/core';
import React, {useEffect} from 'react';
import {View, Image, Text, StyleSheet, TouchableOpacity} from 'react-native';
import {FlatList} from 'react-native';
import {TouchableWithoutFeedback} from 'react-native-gesture-handler';
import COLOR from '../../styles/Color';
import FONTS from '../../styles/Fonts';
import IMAGES from '../../styles/Images';
import Styles from '../../styles/Styles';
import {widthPercentageToDP} from 'react-native-responsive-screen';
import SimpleToast from 'react-native-simple-toast';

export default PaymentOneServiceView = (props) => {
  const id = props.item.mode.id;
  const navigation = useNavigation();

  const getData = (id) => {
    switch (id) {
      case 1:
        return props.item.schedule_flight;
      case 2:
        return props.item.schedule_car;
      case 3:
        return props.item.schedule_stay;
      case 4:
        return props.item.schedule_train;
      case 5:
        return props.item.schedule_cruise;
        case 6:
        return props.item.schedule_transfer;
      case 7:
        return props.item.schedule_trip_plan;
      default:
        return null;
    }
  };

  const handleOnClick = (id, item) => {
    let object = {
      data: item,
      id: props.tripId,
      depart: props.depart,
      return: props.return,
    };
    if (props.package_request_id) {
      object.package_request_id = props.package_request_id;
    }
    switch (id) {
      case 1:
        return null;
      case 2:
        return navigation.navigate('AddCar', object);
      case 3:
        return navigation.navigate('Rooms', object);
      case 4:
        return navigation.navigate('AddTrain', object);
      case 5:
        return navigation.navigate('CruiseScreen', object);
        case 6:
         object.isEdit = true
        return navigation.navigate('TransferScreen', object);;
      default:
        return null;
    }
  };

  const handleFightNavigation = (flight_id, modeId, scheduled_modes) => {
    navigation.navigate('BookFlight', {
      flight_id: flight_id,
      mode: modeId,
      data: scheduled_modes,
      id: props.tripId,
      depart: props.depart,
      return: props.return,
      package_request_id: props.package_request_id,
    });
  };

  const getViewById = (id, item) => {
    switch (id) {
      case 2:
        return <CarView item={item} />;
      case 3:
        return <StayView item={item} />;
      case 4:
        return <TrainView item={item} />;
      case 5:
        return <CruiseView item={item} />;
      case 6:
        return;
      case 7:
        return <TripPlanView item={item} />;
      default:
        return null;
    }
  };

  const getDates = (id, item) => {
    if (
      item.check_in == null &&
      item.depart == null &&
      item.pickup_date == null
    ) {
      return null;
    } else {
      return id == 3
        ? item.check_in + '  -  ' + item.check_out
        : id == 4
        ? item.depart + '  -  ' + item.return
        : id == 5
        ? item.depart
        : id == 2
        ? item.pickup_date + ' - ' + item.drop_off_date
        : id == 7
        ? item.start_date + ' - ' + item.end_date
        : null;
    }
  };

  const HeaderView = ({name, onPress,isTransfer,imagePath}) => {
    return (
      <View style={styles.header}>
        <View style={styles.borderView}>
          <Image style={styles.image} source={{uri: isTransfer ? imagePath : props.item.mode.image}} />
        </View>
        <View style={styles.headingView}>
          <Text style={styles.headerMode}>{props.item.mode.mode}</Text>
          <Text>{name ? name : ''}</Text>
        </View>
        <TouchableOpacity
          onPress={onPress}
          style={{
            position: 'absolute',
            top: 5,
            right: 15,
          }}>
        {props.isPackage == 1 ? null : <Image
            resizeMode={'contain'}
            source={IMAGES.menu_horizontal}
            style={{
              width: 20,
              height: 20,
              alignSelf: 'flex-start',
            }}
          />}
        </TouchableOpacity>
      </View>
    );
  };

  const TripPlanView = ({item}) => (
    <>
      <View style={[styles.borderView, styles.stayView]}>
        <Image
          style={styles.pinPoint}
          source={IMAGES.pinpoint}
          resizeMode={'contain'}
        />
        <Text style={([Styles.body_label], {width: null, textAlign: 'left'})}>
          {item.city}
        </Text>
      </View>
      {getDates(id, item) ? (
        <View style={[styles.dateView, styles.stayView]}>
          <Image
            style={styles.dateIcon}
            source={IMAGES.calendar_small}
            resizeMode={'contain'}
          />
          <Text style={([Styles.body_label], {width: null, textAlign: 'left'})}>
            {getDates(id, item)}
          </Text>
        </View>
      ) : null}
    </>
  );

  const StayView = ({item}) => (
    <>
      <View style={[styles.borderView, styles.stayView]}>
        <Image
          style={styles.pinPoint}
          source={IMAGES.pinpoint}
          resizeMode={'contain'}
        />
        <Text style={([Styles.body_label], {width: null, textAlign: 'left'})}>
          {item.city}
        </Text>
      </View>
      {getDates(id, item) ? (
        <View style={[styles.dateView, styles.stayView]}>
          <Image
            style={styles.dateIcon}
            source={IMAGES.calendar_small}
            resizeMode={'contain'}
          />
          <Text style={([Styles.body_label], {width: null, textAlign: 'left'})}>
            {getDates(id, item)}
          </Text>
        </View>
      ) : null}
    </>
  );

  const CruiseView = ({item}) => (
    <>
      <View style={styles.optionView}>
        <View style={[styles.borderView, {width: '30%', minHeight: 60}]}>
          <Text style={([Styles.small_label], {textAlign: 'center'})}>
            {item.from ? item.from : ''}
          </Text>
        </View>
        <View>
          <Image
            style={styles.modeImage}
            source={{uri: props.item.mode.image}}
          />
          {getDates(id, item) ? (
            <View style={[styles.dateView, {flexDirection: 'column'}]}>
              <Text
                style={
                  ([Styles.body_label],
                  {
                    width: null,
                    textAlign: 'center',
                    fontSize: 10,
                  })
                }>
                {getDates(id, item)}
              </Text>
            </View>
          ) : null}
        </View>
        <View style={[styles.borderView, {width: '30%', minHeight: 60}]}>
          <Text style={([Styles.small_label], {textAlign: 'center'})}>
            {item.to ? item.to : ''}
          </Text>
        </View>
      </View>
    </>
  );

  const TrainView = ({item}) => (
    <>
      <View style={styles.optionView}>
        <View style={[styles.borderView, {width: '30%', minHeight: 60}]}>
          <Text style={([Styles.small_label], {textAlign: 'center'})}>
            {item.from ? item.from : ''}
          </Text>
        </View>
        <Image style={styles.modeImage} source={{uri: props.item.mode.image}} />
        <View style={[styles.borderView, {width: '30%', minHeight: 60}]}>
          <Text style={([Styles.small_label], {textAlign: 'center'})}>
            {item.to ? item.to : ''}
          </Text>
        </View>
      </View>
      {getDates(id, item) ? (
        <View style={[styles.dateView, styles.stayView]}>
          <Image
            style={styles.dateIcon}
            source={IMAGES.calendar_small}
            resizeMode={'contain'}
          />
          <Text style={([Styles.body_label], {width: null, textAlign: 'left'})}>
            {getDates(id, item)}
          </Text>
        </View>
      ) : null}
    </>
  );

  const CarView = ({item}) => (
    <>
      <View>
        <View style={styles.carRowView}>
          <Text style={{alignSelf: 'center', marginHorizontal: 10}}>
            {'Pickup'}
          </Text>
          <View style={[styles.borderView, styles.carPickView]}>
            <Image
              style={styles.pinPoint}
              source={IMAGES.pinpoint}
              resizeMode={'contain'}
            />
            <Text style={[{textAlign: 'center', width: null}]}>
              {item.pickup_location ? item.pickup_location : ''}
            </Text>
          </View>
        </View>
        <View style={styles.carRowView}>
          <Text style={{alignSelf: 'center', marginHorizontal: 10}}>
            {'Drop-off'}
          </Text>

          <View style={[styles.borderView, styles.carPickView]}>
            <Image
              style={styles.pinPoint}
              source={IMAGES.pinpoint}
              resizeMode={'contain'}
            />
            <Text style={[{textAlign: 'center', width: null}]}>
              {item.drop_off_location ? item.drop_off_location : ''}
            </Text>
          </View>
        </View>
      </View>

      {getDates(id, item) ? (
        <View style={[styles.dateView, styles.stayView, {marginTop: 15}]}>
          <Image
            style={styles.dateIcon}
            source={IMAGES.calendar_small}
            resizeMode={'contain'}
          />
          <Text style={([Styles.body_label], {width: null, textAlign: 'left'})}>
            {getDates(id, item)}
          </Text>
        </View>
      ) : null}
    </>
  );

  return (
    <View>
      {id == 1 ? (
        <FlatList
          showsVerticalScrollIndicator={false}
          data={props.item.schedule_flight}
          renderItem={({item}) => {
            let flightModeName = item.flight_mode.name;
            let flight_id = item.id;
            let modeId = item.flight_mode_id;
            let scheduled_modes = item.schedule_modes;
            return (
              <FlatList
                showsVerticalScrollIndicator={false}
                data={item.schedule_modes}
                renderItem={({item}) => (
                  <View style={styles.serviceContainer}>
                    <View style={styles.valueBackground}>
                      <HeaderView
                        onPress={() =>
                          props.package_request_id
                            ? null
                            : handleFightNavigation(
                                flight_id,
                                modeId,
                                scheduled_modes,
                              )
                        }
                        name={flightModeName}
                      />
                      <View style={styles.optionView}>
                        <View
                          style={[
                            styles.borderView,
                            {width: '30%', minHeight: 60},
                          ]}>
                          <Text
                            style={
                              ([Styles.small_label], {textAlign: 'center'})
                            }>
                            {item.from}
                          </Text>
                        </View>
                        <View>
                          <Image
                            style={styles.modeImage}
                            source={require('../../styles/assets/plane_horizontal.png')}
                          />
                          {item.depart ? (
                            <View
                              style={[
                                styles.dateView,
                                {flexDirection: 'column'},
                              ]}>
                              <Text
                                style={
                                  ([Styles.body_label],
                                  {
                                    width: null,
                                    textAlign: 'center',
                                    fontSize: 10,
                                  })
                                }>
                                {item.depart}
                              </Text>
                              <Text
                                style={
                                  ([Styles.body_label],
                                  {
                                    width: null,
                                    textAlign: 'center',
                                    fontSize: 10,
                                  })
                                }>
                                {item.return ? ' - ' + item.return : ''}
                              </Text>
                            </View>
                          ) : null}
                        </View>
                        <View
                          style={[
                            styles.borderView,
                            {width: '30%', minHeight: 60},
                          ]}>
                          <Text
                            style={
                              ([Styles.small_label], {textAlign: 'center'})
                            }>
                            {item.to}
                          </Text>
                        </View>
                      </View>
                    </View>
                  </View>
                )}
                numColumns={1}
                ListEmptyComponent={() => null}
                keyExtractor={(item, index) => index.toString()}
              />
            );
          }}
          numColumns={1}
          ListEmptyComponent={() => null}
          keyExtractor={(item, index) => index.toString()}
        />
      ) : id == 6 ? (
        <FlatList
          showsVerticalScrollIndicator={false}
          data={props.item.schedule_transfer}
          renderItem={({item}) => {
            let flightModeName = item.mode.name;
            let flight_id = item.id;
            let modeId = item.mode.id;
            let scheduled_modes = item.mode;
            return (
              // <FlatList
              //   showsVerticalScrollIndicator={false}
              //   data={item.schedule_modes}
              //   renderItem={({item}) => (
                  <View style={styles.serviceContainer}>
                    <View style={styles.valueBackground}>
                      <HeaderView
                      imagePath={item.mode.path}
                       isTransfer={true}
                        onPress={() =>
                          props.package_request_id
                            ? null
                            : handleOnClick(id, item)
                        }
                        name={flightModeName}
                      />
                      <View style={styles.optionView}>
                        <View
                          style={[
                            styles.borderView,
                            {width: '30%', minHeight: 60},
                          ]}>
                          <Text
                            style={
                              ([Styles.small_label], {textAlign: 'center'})
                            }>
                            {item.from}
                          </Text>
                        </View>
                        <View>
                          <Image
                            style={styles.modeImage}
                            source={require('../../styles/assets/plane_horizontal.png')}
                          />
                          {item.depart ? (
                            <View
                              style={[
                                styles.dateView,
                                {flexDirection: 'column'},
                              ]}>
                              <Text
                                style={
                                  ([Styles.body_label],
                                  {
                                    width: null,
                                    textAlign: 'center',
                                    fontSize: 10,
                                  })
                                }>
                                {item.depart}
                              </Text>
                              <Text
                                style={
                                  ([Styles.body_label],
                                  {
                                    width: null,
                                    textAlign: 'center',
                                    fontSize: 10,
                                  })
                                }>
                                {item.return ? ' - ' + item.return : ''}
                              </Text>
                            </View>
                          ) : null}
                        </View>
                        <View
                          style={[
                            styles.borderView,
                            {width: '30%', minHeight: 60},
                          ]}>
                          <Text
                            style={
                              ([Styles.small_label], {textAlign: 'center'})
                            }>
                            {item.to}
                          </Text>
                        </View>
                      </View>
                    </View>
                  </View>
                )}
          }
          numColumns={1}
          ListEmptyComponent={() => null}
          keyExtractor={(item, index) => index.toString()}
        />
      ) : (
        <FlatList
          showsVerticalScrollIndicator={false}
          data={getData(id)}
          renderItem={({item}) => (
            <View style={styles.serviceContainer}>
              <View style={styles.valueBackground}>
                <HeaderView
                  onPress={() =>
                    props.package_request_id ? null : handleOnClick(id, item)
                  }
                />

                {getViewById(id, item)}
              </View>
            </View>
          )}
          numColumns={1}
          ListEmptyComponent={() => null}
          keyExtractor={(item, index) => index.toString()}
        />
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: COLOR.WHITE,
    borderRadius: 15,
    paddingVertical: 5,
  },
  header: {
    flexDirection: 'row',
    width: '100%',
  },
  headingView: {
    marginTop: 10,
    marginLeft: 5,
  },
  priceView: {
    alignItems: 'center',
    position: 'absolute',
    right: 10,
    top: 10,
  },
  image: {tintColor: COLOR.GREEN, height: 40, width: 40},

  dateView: {
    alignSelf: 'center',
    flexDirection: 'row',
    paddingHorizontal: 5,
  },
  dateIcon: {
    height: 20,
    width: 20,
    marginRight: 5,
  },

  serviceContainer: {
    marginVertical: 5,
    minHeight: 120,
    borderRadius: 10,
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: COLOR.GREEN,
    alignSelf: 'center',
    width: widthPercentageToDP(90),
  },
  modeImage: {
    height: 20,
    width: 20,
    marginHorizontal: 10,
    alignSelf: 'center',
  },
  optionView: {
    flexDirection: 'row',
    alignSelf: 'center',
    justifyContent: 'space-between',
    width: '90%',
    borderRadius: 20,
    alignItems: 'center',
  },
  ticketText: {
    transform: [{rotate: '90deg'}],
    fontFamily: FONTS.FAMILY_REGULAR,
    fontSize: 18,
    position: 'absolute',
    width: 120,
    height: 40,
    right: -40,
    paddingTop: 5,
    justifyContent: 'center',
    textAlign: 'center',
    color: COLOR.WHITE,
    alignSelf: 'center',
  },
  valueBackground: {
    minHeight: 120,
    backgroundColor: COLOR.WHITE,
    paddingVertical: 5,
    borderRadius: 10,
    width: '100%',
    alignSelf: 'flex-start',
    justifyContent: 'center',
    alignItems: 'center',
  },
  borderView: {
    borderWidth: 1,
    borderColor: COLOR.BORDER,
    borderRadius: 5,
    alignItems: 'center',
    justifyContent: 'center',
    marginHorizontal: 10,
    padding: 5,
    marginVertical: 5,
  },
  arrowView: {
    width: 10,
    height: 10,
    position: 'absolute',
    top: '45%',
    alignSelf: 'center',
    justifyContent: 'center',
    bottom: '45%',
    right: 5,
  },
  headerMode: {
    fontFamily: FONTS.FAMILY_SEMIBOLD,
    fontSize: 18,
  },
  pinPoint: {
    height: 20,
    width: 20,
    marginRight: 5,
  },
  stayView: {
    flexDirection: 'row',
    marginTop: 0,
    marginBottom: 10,
    marginHorizontal: 30,
    width: '75%',
    alignSelf: 'flex-end',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  carPickView: {
    marginHorizontal: 0,
    alignSelf: 'center',
    marginVertical: 0,
    flexDirection: 'row',
    width: '70%',
  },
  carRowView: {
    flexDirection: 'row',
    marginVertical: 5,
    width: '90%',
    justifyContent: 'space-between',
  },
});
