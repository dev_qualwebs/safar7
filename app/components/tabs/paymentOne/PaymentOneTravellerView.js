import {useEffect, useState} from 'react';
import React from 'react';
import {Image, View, Text, StyleSheet, FlatList, Alert} from 'react-native';
import {TouchableOpacity} from 'react-native';
import {
  heightPercentageToDP,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import COLOR from '../../styles/Color';
import IMAGES from '../../styles/Images';
import Styles from '../../styles/Styles';
import {useNavigation} from '@react-navigation/core';
import SimpleToast from 'react-native-simple-toast';
import CompleteProfile from '../profile/CompleteProfile';
import TravellerNumberModal from '../message/travellerNumberModal';

const PaymentOneTravellerView = (props) => {
  const [modalVisible, setModalVisible] = useState(false);
  const navigation = useNavigation();

  const goToProfile = () => {
    setModalVisible(false);
    navigation.navigate('ProfileDetail', {val: 0});
  };

  useEffect(() => {}, [props]);
  return (
    <View style={{width: wp(90), alignSelf: 'center', flex: 1}}>
      <Text
        style={{
          fontSize: 18,
          marginVertical: 15,
        }}>
        Travellers
      </Text>
      <View style={{flexDirection: 'row', marginVertical: 10}}>
        <Image
          source={IMAGES.shield}
          style={{
            width: 25,
            height: 25,
            marginRight: 10,
          }}
          resizeMode={'contain'}
        />
        <Text style={{color: COLOR.LIGHT_GREEN}}>
          WE PROTECT YOUR PERSONAL INFORMATION
        </Text>
      </View>
      <FlatList
      bounces={false}
        numColumns={1}
        style={{height: heightPercentageToDP(45)}}
        showsHorizontalScrollIndicator={false}
        showsVerticalScrollIndicator={false}
        data={props.travellersData}
        keyExtractor={(index) => index}
        renderItem={({item}) => {
          const selected = props.selectedTravellers.findIndex(
            (value) => value.id == item.id,
          );

          return (
            <View>
              <View style={styles.travellerView}>
               
             {item.is_default_user == 1 ? null :     <TouchableOpacity
                  onPress={() => {
                    selected != -1
                      ? props.removeTraveller(item)
                      : props.addTraveller(item)
                  }}
                  style={{
                    height: 30,
                    width: 30,
                    marginHorizontal: 5,
                    borderRadius: 5,
                    backgroundColor: selected != -1 ? COLOR.GREEN : COLOR.WHITE,
                    borderColor: COLOR.GRAY,
                    justifyContent: 'center',
                    alignItems: 'center',
                    borderWidth: 1,
                  }}>
                  <Image
                    resizeMode={'contain'}
                    style={{height: 20, width: 20, alignSelf: 'center'}}
                    source={IMAGES.tick_image}
                  />
                </TouchableOpacity>}

                {item.is_default_user == 1 ? (<Text style={[Styles.body_label, {width: null, flex: 1}]}>
                 {'         '} ME ({item.prefix + ' ' + item.first_name})
                </Text>) : ( <Text style={[Styles.body_label, {width: null, flex: 1}]}>
                  {item.prefix + ' ' + item.first_name}
                </Text>)}
                
                {item.is_default_user == 1 ? null : (
                  <>
                    <TouchableOpacity
                      onPress={() =>
                        navigation.navigate('TravellersDetail', {
                          data: item,
                        })
                      }
                      style={{
                        height: 40,
                        width: 40,
                        justifyContent: 'center',
                        alignItems: 'center',
                      }}>
                      <Image
                        source={IMAGES.edit}
                        style={{height: 20, width: 20}}
                        resizeMode={'contain'}
                      />
                    </TouchableOpacity>
                    {/* <TouchableOpacity
                      onPress={() => {
                        props.deleteTraveller(item.id);
                        props.removeTraveller(item);
                      }}
                      style={{
                        height: 40,
                        width: 40,
                        justifyContent: 'center',
                        alignItems: 'center',
                      }}>
                      <Image
                        source={IMAGES.cross_icon}
                        style={{height: 20, width: 20}}
                        resizeMode={'contain'}
                      />
                    </TouchableOpacity> */}
                  </>
                )}
              </View>
              <View style={Styles.line_view} />
            </View>
          );
        }}
        keyExtractor={(item, index) => index.toString()}
      />
      <TouchableOpacity
        onPress={() => {
          if (
            props.userDetail.email == null ||
            props.userDetail.phone == null ||
            props.userDetail.country_id == null
          ) {
            setModalVisible(true);
          } else {
            navigation.navigate('TravellersDetail');
          }
        }}
        style={styles.addButton}>
        <View style={styles.buttonContainer}>
          <View style={styles.addButtonView}>
            <Image style={styles.plusImage} source={IMAGES.plus_image} />
          </View>

          <Text style={[Styles.button_font, {color: COLOR.BLACK}]}>
            Add New Traveller
          </Text>
        </View>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={props.handleTravelerSelection}
        style={styles.continueButton}>
        <Text style={[Styles.button_font, {color: COLOR.WHITE}]}>Continue</Text>
      </TouchableOpacity>
      {modalVisible ? (
        <CompleteProfile
          props={props}
          goToProfile={goToProfile}
          hideModal={() => setModalVisible(false)}
          modal={true}
        />
      ) : null}
    </View>
  );
};

const styles = StyleSheet.create({
  continueButton: {
    alignItems: 'center',
    justifyContent: 'center',
    height: 50,
    borderRadius: 10,
    width: wp(90),
    marginVertical: 10,
    backgroundColor: COLOR.GREEN,
    alignSelf: 'center',
  },
  plusImage: {
    width: 15,
    height: 15,
    borderRadius: 10,
    tintColor: COLOR.WHITE,
  },
  addButtonView: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 25,
    height: 25,
    borderRadius: 15,
    backgroundColor: COLOR.GREEN,
    marginRight: 10,
  },
  buttonContainer: {flexDirection: 'row', alignItems: 'center'},
  addButton: {
    alignItems: 'center',
    justifyContent: 'center',
    height: 50,
    borderRadius: 10,
    borderColor: COLOR.GRAY,
    borderWidth: 1,
    width: wp(90),
    marginVertical: 10,
    backgroundColor: COLOR.WHITE,
    alignSelf: 'center',
  },
  travellerView: {
    flexDirection: 'row',
    alignSelf: 'center',
    width: wp(90),
    alignItems: 'center',
    marginVertical: 10,
  },
});

export default PaymentOneTravellerView;
