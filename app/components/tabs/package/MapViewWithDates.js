import React from 'react';
import {
  View,
  StyleSheet,
  Dimensions,
  Text,
  Image,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import API from '../../../api/Api';
import Styles from '../../styles/Styles';
import MapView, {Marker, Polygon, Polyline} from 'react-native-maps';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';
import COLOR from '../../styles/Color';
import IMAGES from '../../styles/Images';
import FONTS from '../../styles/Fonts';

const {width, height} = Dimensions.get('window');
const ASPECT_RATIO = width / height;
const LATITUDE = 12;
const LONGITUDE = 76;
const LATITUDE_DELTA = 16;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

let api = new API();
export default class MapViewWithDates extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      region: {
        latitude: LATITUDE,
        longitude: LONGITUDE,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA,
      },
      coordinates: [],
      packageDetail: null,
      selectedIndex: 0,
      placesData: [],
    };
  }

  componentDidMount() {
    if (this.props.route.params && this.props.route.params.data) {
      this.setState({packageDetail: this.props.route.params.data}, () => {
        if (
          this.state.packageDetail &&
          this.state.packageDetail.package_trip_plan &&
          this.state.packageDetail.package_trip_plan.package_trip_details
            .length > 0
        ) {
          let tripDat = [];
          this.state.packageDetail.package_trip_plan.package_trip_details.map(
            (item, index) => {
              let pushItem = [];
              item.day_plan.map((val) => {
                pushItem.push(val);
              });
              tripDat.push(pushItem);
            },
          );

          this.setState({placesData: tripDat}, () => {
            this.setMapPlaces();
          });
          
        }
      });
    }
  }


  setMapPlaces = () =>{
      if (this.state.placesData.length > this.state.selectedIndex) {
        console.log(this.state.placesData);
        let reg = this.state.placesData[this.state.selectedIndex];
        if (reg.length > 0) {
          let dats = {
            latitude: parseInt(reg[0].latitude ? reg[0].latitude : '0'),
            longitude: parseInt(reg[0].longitude ? reg[0].longitude : '0'),
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA,
          };
          this.setState(
            {
              region: dats,
            },
            () => {
              
            },
          );
        }
        let coord = [];
        this.state.placesData[this.state.selectedIndex].map((val) => {
          coord.push({
            latitude: val.latitude,
            longitude: val.longitude,
            description: val.places,
          });
        });
        this.setState({coordinates: coord});
      }
  }

  ordinal_suffix_of(i) {
    var j = i % 10,
      k = i % 100;
    if (j == 1 && k != 11) {
      return i + 'st';
    }
    if (j == 2 && k != 12) {
      return i + 'nd';
    }
    if (j == 3 && k != 13) {
      return i + 'rd';
    }
    return i + 'th';
  }

  commonView = (props) => {
    return (
      <View
        style={[
          Styles.shadow_view,
          {
            backgroundColor: props.bgColor,
            paddingVertical: 10,
            justifyContent: 'center',
            alignItems: 'center',
            borderRadius: 10,
            width: 50,
            height: 50,
            marginRight: 20,
          },
        ]}>
        <Text
          style={[
            Styles.small_label,
            {
              color:
                props.bgColor == COLOR.WHITE ? COLOR.LIGHT_TEXT : COLOR.WHITE,
              width: null,
            },
          ]}>
          {this.ordinal_suffix_of(props.day)}
        </Text>
        <Text
          style={[
            Styles.body_label,
            {
              fontFamily: FONTS.FAMILY_BOLD,
              color: props.bgColor == COLOR.WHITE ? COLOR.BLACK : COLOR.WHITE,
              width: null,
            },
          ]}>
          Day
        </Text>
      </View>
    );
  };

  render() {
    const {packageDetail, selectedIndex} = this.state;
    return (
      <View style={Styles.container}>
      <View>
        <MapView
          provider={this.props.provider}
          style={styles.map}
          initialRegion={this.state.region}>
          <Polyline
            strokeColor={'#48A651'}
            strokeWidth={3}
            coordinates={this.state.coordinates}
          />
          {this.state.coordinates.map((val, index) => {
            return (
              <Marker
              
                image={IMAGES.customMarker}
                key={index}
                coordinate={{latitude: 18.557740 + 10.5 + index, longitude: 25.393636 + 10.5 + index}}
                title={`PLACE ${index + 1}`}
                description={val.description}
              />
            );
          })}
        </MapView>
        </View>
        <View style={{position: 'absolute', top: 45, left: 15}}>
          <TouchableOpacity
            onPress={() => {
              this.props.navigation.goBack();
            }}>
            <View>
              <Image source={IMAGES.close} style={{height: 25, width: 25,tintColor:COLOR.WHITE}} />
            </View>
          </TouchableOpacity>
        </View>
        <View
          style={[
            {
              height: heightPercentageToDP(15),
              backgroundColor: '#f4f4f4',
              borderTopLeftRadius: 20,
              borderTopRightRadius: 20,
              paddingTop: 20,
              marginTop:-30
            },
          ]}>
          <View
            style={{
              flexDirection: 'row',
              width: widthPercentageToDP(90),
              alignSelf: 'center',
              alignItems: 'center',
            }}>
            <FlatList
              style={{
                width: widthPercentageToDP(90),
                alignSelf: 'center',
                marginTop: 10,
              }}
              data={
                packageDetail && packageDetail.package_trip_plan
                  ? packageDetail.package_trip_plan.package_trip_details
                  : []
              }
              renderItem={({item, index}) => (
                <TouchableOpacity
                  onPress={() => {
                    this.setState({selectedIndex: index},()=>{
                      this.setMapPlaces()
                    });
                  }}>
                  <this.commonView
                    bgColor={selectedIndex == index ? COLOR.GREEN : COLOR.WHITE}
                    day={item.days}
                  />
                </TouchableOpacity>
              )}
              horizontal={true}
              showsVerticalScrollIndicator={false}
              showsHorizontalScrollIndicator={false}
              keyExtractor={(item, index) => index.toString()}
            />
            {/* <TouchableOpacity
              style={{
                height: 50,
                width: 50,
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <Image
                source={IMAGES.zig_zag}
                resizeMode={'contain'}
                style={{height: 30, width: 30}}
              />
            </TouchableOpacity> */}
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  map: {
    height: heightPercentageToDP(90),
    borderRadius: 10,
  },
});
