import React from 'react';
import {View, Text, FlatList, Image, TouchableOpacity} from 'react-native';
import FONTS from '../../styles/Fonts';
import IMAGES from '../../styles/Images';
import Styles from '../../styles/Styles';
import COLOR from '../../styles/Color';
import {widthPercentageToDP} from 'react-native-responsive-screen';
import FastImage from 'react-native-fast-image';

export default class PackageServices extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      packageServices: null,
    };
  }

  componentDidMount() {
    if (this.props.data.route.params.data) {

      this.setState({
        packageServices: this.props.data.route.params.data,
      });
    }
  }

  pointView = (prop) => {
    return (
      <View style={{flexDirection: 'row', alignItems: 'center'}}>
        <Text
          style={[
            Styles.small_label,
            {width: null, alignSelf: 'flex-start', fontSize: 14},
          ]}>
          •{'  '}
          {prop.text}
        </Text>
      </View>
    );
  };

  render() {
    let {packageServices} = this.state;
    return (
      <View style={{paddingVertical: 20}}>
        <Text
          style={[
            Styles.small_label,
            {
              alignSelf: 'center',
              marginVertical: 10,
              fontFamily: FONTS.FAMILY_BOLD,
            },
          ]}>
          SERVICES INCLUDED
        </Text>
        <FlatList
        bounces={false}
        showsVerticalScrollIndicator={false}
        showsHorizontalScrollIndicator={false}
          style={{width: widthPercentageToDP(90), alignSelf: 'center'}}
          data={
            packageServices && packageServices.package_exclude_include.package_services
              ? packageServices.package_exclude_include.package_services.service.package_travelling_modes
              : []
          }
          renderItem={({item}) => (
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate('');
              }}>
              <View
                style={{
                  marginHorizontal: 10,
                  alignItems: 'center',
                  alignSelf: 'center',
                  justifyContent: 'center',
                  marginVertical: 0,
                }}>
                {/* <FastImage
                  style={{
                    height: 35,
                    width: 35,
                    marginBottom: 10,
                    tintColor: COLOR.VOILET,
                    alignSelf: 'center',
                    overflow: 'hidden',
                    backgroundColor: '#00000065',
                    borderRadius:20
                  }}
                  source={{
                    uri: item.mode.image,
                    priority: FastImage.priority.normal,
                  }}
                  resizeMode={FastImage.resizeMode.contain  }
                /> */}
                <Image  style={{
                    height: 35,
                    width: 35,
                    marginBottom: 10,
                    tintColor: COLOR.VOILET,
                    alignSelf: 'center',
                    overflow: 'hidden',
                  }}  source={{uri:item.mode.image}}/>
                <Text
                  style={{
                    textAlign: 'center',
                    color: COLOR.VOILET,
                    fontSize: 12,
                  }}>
                  {item.mode.mode}
                </Text>
              </View>
            </TouchableOpacity>
          )}
          horizontal={true}
          showsVerticalScrollIndicator={false}
          showsHorizontalScrollIndicator={false}
          keyExtractor={(item, index) => index.toString()}
        />
        <View style={[Styles.line_view, {marginVertical: 10}]} />
        <Text
          style={[
            Styles.small_label,
            {
              alignSelf: 'center',
              marginBottom: 10,
              fontFamily: FONTS.FAMILY_BOLD,
            },
          ]}>
          ADDONS INCLUDED
        </Text>
        <FlatList
          style={{width: widthPercentageToDP(90), alignSelf: 'center'}}
          data={
            packageServices
              ? packageServices.package_exclude_include.package_addons
              : []
          }
          renderItem={({item}) => (
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate('');
              }}>
              <View
                style={{
                  alignItems: 'center',
                  justifyContent: 'center',
                  alignSelf: 'center',
                  marginBottom: 20,
                  width: widthPercentageToDP(90) / 4,
                }}>
                {/* <FastImage
                  style={{
                    height: 25,
                    width: 25,
                    marginBottom: 10,
                    tintColor: COLOR.VOILET,
                  }}
                  source={{
                    uri: item.addon.image,
                    priority: FastImage.priority.normal,
                  }}
                  resizeMode={FastImage.resizeMode.contain}
                /> */}
                 <Image  style={{
                    height: 25,
                    width: 25,
                    marginBottom: 10,
                    tintColor: COLOR.VOILET,
                    alignSelf: 'center',
                    overflow: 'hidden',
                  }}  source={{uri:item.addon.image}}/>

                <Text
                  style={{
                    textAlign: 'center',
                    color: COLOR.VOILET,
                    fontSize: 12,
                  }}>
                  {item.addon.name}
                </Text>
              </View>
            </TouchableOpacity>
          )}
          numColumns={4}
          showsVerticalScrollIndicator={false}
          showsHorizontalScrollIndicator={false}
          keyExtractor={(item, index) => index.toString()}
        />
      </View>
    );
  }
}
