import React from 'react';
import {
  Modal,
  Text,
  View,
  StyleSheet,
  Image,
  Alert,
  TouchableOpacity,
  FlatList,
} from 'react-native';

import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import COLOR from '../../styles/Color';
import FONTS from '../../styles/Fonts';
import IMAGES from '../../styles/Images';
import Styles from '../../styles/Styles';
import NavigationBar from '../../commonView/NavigationBar';

export default class NewReservationPackage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: true,
      departFlexibility: this.props.route.params.departFlexibility,
      returnFlexibility: this.props.route.params.returnFlexibility,
      depart: this.props.route.params.depart,
      return: this.props.route.params.return,
      isPackage: this.props.route.params.isPackage,
      selectTransport: [
        {
          id: 1,
          name: 'Flight',
          image: IMAGES.plane,
        },
        {
          id: 2,
          name: 'Stay',
          image: IMAGES.bed,
        },
        {
          id: 3,
          name: 'Car',
          image: IMAGES.car_image,
        },
        {
          id: 4,
          name: 'Cruise',
          image: IMAGES.cruise,
        },
        {
          id: 5,
          name: 'Train',
          image: IMAGES.train,
        },
        {
          id: 6,
          name: 'Transfer',
          image: IMAGES.train,
        },
        {
          id: 7,
          name: 'Trip Plan',
          image: IMAGES.token,
        },
      ],
      selectedIndex: null,
      isFlightAdded: this.props.route.params.isFlightAdded,
      isCruiseAdded: this.props.route.params.isCruiseAdded,
      package_request_id: this.props.route.params.package_request_id,
    };
    this.selectTransport = this.selectTransport.bind(this);
    this.handleNavigation = this.handleNavigation.bind(this);
    this.tripId = this.props.route.params.tripId;
  }

  selectTransport = (item) => {
    this.setState({selectedIndex: item.id});
  };

  handleNavigation = () => {
    if (this.state.selectedIndex) {
      this.setState({modalVisible: false}, (val) => {
        let object = {
          index: 0,
          isPackage: this.state.isPackage,
          package_request_id: this.state.package_request_id,
          id: this.tripId,
          depart: this.state.depart,
          return: this.state.return,
          departFlexibility: this.state.departFlexibility,
          returnFlexibility: this.state.returnFlexibility,
        };
        switch (this.state.selectedIndex) {
          case 1:
            (object.screens = ['BookFlight']),
              this.props.navigation.replace('BookFlight', object);
            break;
          case 2:
            object.screens = ['StayScreen'];
            this.props.navigation.replace('StayScreen', object);
            break;
          case 3:
            object.screens = ['CarScreen'];
            this.props.navigation.replace('CarScreen', object);
            break;
          case 4:
            object.screens = ['CruiseScreen'];
            this.props.navigation.replace('CruiseScreen', object);
            break;
          case 5:
            object.screens = ['TrainScreen'];
            this.props.navigation.replace('TrainScreen', object);
            break;
          case 6:
            object.screens = ['AddnewTransfer'];
            this.props.navigation.replace('AddnewTransfer', object);
            break;
          case 7:
            object.screens = ['TripPlanScreen'];
            this.props.navigation.replace('TripPlanScreen', object);

            break;

          default:
            break;
        }
      });
    } else {
      Alert.alert('Select an Option');
    }
  };

  render() {
    const {selectedIndex, modalVisible} = this.state;
    return (
      <View style={Styles.container}>
        <Modal
          animationType={'slide'}
          transparent={true}
          style={{backgroundColor: COLOR.BLACK}}
          visible={modalVisible}
          onRequestClose={() => {
            console.log('Modal has been closed.');
          }}>
          <View style={styles.modal}>
            <View style={styles.modalBody}>
              <NavigationBar
                prop={this.props}
                navHeight={40}
                name={'Add a New Reservation'}
              />
              <FlatList
                style={{backgroundColor: COLOR.WHITE}}
                data={this.state.selectTransport}
                renderItem={({item}) => (
                  <TouchableOpacity
                    disabled={
                      (item.id == 1 && this.state.isFlightAdded) ||
                      (item.id == 4 && this.state.isCruiseAdded)
                    }
                    style={{
                      opacity:
                        (item.id == 1 && this.state.isFlightAdded) ||
                        (item.id == 4 && this.state.isCruiseAdded)
                          ? 0.5
                          : 1,
                    }}
                    onPress={() => {
                      console.log('called');
                      this.selectTransport(item);
                    }}>
                    <View
                      style={{
                        flex: 1,
                        borderRadius: 10,
                        borderWidth: 1,
                        height: 120,
                        margin: 5,
                        borderColor:
                          selectedIndex == item.id ? COLOR.GREEN : COLOR.GRAY,
                        backgroundColor:
                          selectedIndex == item.id ? '#05778F10' : COLOR.WHITE,
                        alignSelf: 'center',
                        alignItems: 'center',
                        justifyContent: 'center',
                        width: wp(43.5),
                      }}>
                      <Image
                        source={item.image}
                        style={{
                          height: 50,
                          width: 50,
                          marginVertical: 10,
                          tintColor:
                            selectedIndex == item.id
                              ? COLOR.GREEN
                              : COLOR.VOILET,
                        }}
                      />
                      <Text style={{marginBottom: 10, textAlign: 'center'}}>
                        {item.name}
                      </Text>
                    </View>
                  </TouchableOpacity>
                )}
                horizontal={false}
                numColumns={2}
                showsVerticalScrollIndicator={false}
                showsHorizontalScrollIndicator={false}
                keyExtractor={(item, index) => index.toString()}
              />
              <TouchableOpacity
                onPress={() => {
                  this.handleNavigation();
                }}
                style={{
                  alignItems: 'center',
                  justifyContent: 'center',
                  height: 50,
                  borderRadius: 10,

                  width: wp(90),
                  marginVertical: 20,
                  backgroundColor: COLOR.GREEN,
                }}>
                <Text style={[Styles.button_font, {color: COLOR.WHITE}]}>
                  Next
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  modal: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
  },
  modalBody: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    backgroundColor: COLOR.WHITE,
    borderWidth: 1,
    width: wp(100),
    borderColor: COLOR.BORDER,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    flex: 1,
    marginTop: 50,
  },
  image: {
    height: 120,
    width: 120,
    alignSelf: 'center',
    marginVertical: 20,
  },
  headingLabel: {
    fontFamily: FONTS.FAMILY_BOLD,
    fontSize: FONTS.MEDIUM,
    color: COLOR.BLACK,
  },
  singleLineView: {
    marginVertical: 20,
    borderBottomColor: COLOR.GRAY,
    width: wp(100),
    alignSelf: 'center',
    borderBottomWidth: 1,
  },
  offer_view: {
    marginVertical: 0,
    width: wp(100),
    marginBottom: 20,
    height: 100,
    marginHorizontal: 10,
  },
  flatlist_view: {
    width: wp(90),
    marginVertical: 10,
    alignSelf: 'center',
  },
});
