import React from 'react';
import {StyleSheet, View, Text, Image, TouchableOpacity, Modal, SafeAreaView} from 'react-native';
import COLOR from '../../styles/Color';
import FONTS from '../../styles/Fonts';
import Styles from '../../styles/Styles';
import {heightPercentageToDP as hp, widthPercentageToDP} from 'react-native-responsive-screen';
import IMAGES from '../../styles/Images';
import {Platform} from 'react-native';
import PaymentTravellersView from '../payments/PaymentTravellersView';
import PaymentSecondView from '../payments/PaymentSecondView';
import {connect} from 'react-redux';
import TripDateView from '../../commonView/TripDateView';
import PackagePaymentSummary from './PackagePaymentSummary';
import PackageAction from '../../../redux/action/PackageAction';

class PackagePayment extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      screen: 1,
      offer: null,
      offerPayment:0,
     
    };

    this.changeScreen = this.changeScreen.bind(this);
    this.getScreenFromState = this.getScreenFromState.bind(this);
    this.getTitleFromState = this.getTitleFromState.bind(this);
    this.props.getSinglePackageOffer(this.props.route.params.id);
  }

  componentDidUpdate(prevProps) {
    if (this.props.singlePackageOffer !== prevProps.singlePackageOffer) {
      this.setState({
        offer: this.props.singlePackageOffer,
        offerPayment: this.props.singlePackageOffer.offer_payment,
      },()=>{
      });
    }
  }

  changeScreen = (screen) => {
    this.setState({
      screen: screen,
    });
  };

  getScreenFromState(state) {
    switch (state) {
      case 1:
        return (
          <PackagePaymentSummary
            travellingModes={
              this.state.offer.package.trip.active_offer_travelling_modes
            }
            changeScreen={this.changeScreen}
            offerPayment={this.state.offerPayment}
          />
        );
      case 2:
        return (
          <PaymentTravellersView
            travellers={this.state.offer.booked_travellers}
            changeScreen={this.changeScreen}
            offerPayment={this.state.offerPayment}
          />
        );
      case 3:
        return (
          <PaymentSecondView
            isPackage={true}
            id={this.props.route.params.id}
            navigation={this.props.navigation}
            offer={this.state.offer}
            activeOffer={this.state.offer.active_offers[0]}
          />
        );
    }
  }
  getTitleFromState(state) {
    switch (state) {
      case 1:
        return 'Trip Summary';
      case 2:
        return 'Travellers';
      case 3:
        return 'Payment';
    }
  }

  render() {
    return (
      <View style={Styles.container}>
        <View style={styles.container}>
          <View style={styles.statusBar}>
            <Text style={styles.title}>
              {this.getTitleFromState(this.state.screen)}
            </Text>
            <View style={styles.statusView}>
              <View style={{width: 80}}>
                <View
                  style={{
                    width: 20,
                    height: 20,
                    backgroundColor: COLOR.WHITE,
                    borderRadius: 10,
                    justifyContent: 'center',
                    alignSelf: 'center',
                  }}>
                  <Image
                    source={require('../../styles/assets/checked.png')}
                    style={{width: 10, height: 10, alignSelf: 'center'}}
                    resizeMode={'contain'}
                  />
                </View>
                <Text
                  style={{
                    fontSize: 12,
                    color: COLOR.WHITE,
                    alignSelf: 'center',
                    marginTop: 3,
                  }}>
                  Trip Summary
                </Text>
              </View>
              <View style={{width: 80}}>
                <View
                  style={{
                    width: 20,
                    height: 20,
                    borderWidth: this.state.screen > 1 ? 0 : 2,
                    backgroundColor: this.state.screen > 1 ? COLOR.WHITE : null,
                    borderColor: '#82BBC7',
                    borderRadius: 10,
                    justifyContent: 'center',
                    alignSelf: 'center',
                  }}>
                  {this.state.screen > 1 ? (
                    <Image
                      source={require('../../styles/assets/checked.png')}
                      style={{width: 10, height: 10, alignSelf: 'center'}}
                      resizeMode={'contain'}
                    />
                  ) : (
                    <Text
                      style={{
                        alignSelf: 'center',
                        fontSize: 12,
                        color: COLOR.WHITE,
                      }}>
                      2
                    </Text>
                  )}
                </View>
                <Text
                  style={{
                    fontSize: 12,
                    color: COLOR.WHITE,
                    alignSelf: 'center',
                    marginTop: 3,
                  }}>
                  Travellers
                </Text>
              </View>
              {this.state.offerPayment == 1 ? null : <View style={{width: 80}}>
                <View
                  style={{
                    width: 20,
                    height: 20,
                    borderWidth: this.state.screen == 3 ? 0 : 2,
                    backgroundColor:
                      this.state.screen == 3 ? COLOR.WHITE : null,
                    borderColor: '#82BBC7',
                    borderRadius: 10,
                    justifyContent: 'center',
                    alignSelf: 'center',
                  }}>
                  {this.state.screen == 3 ? (
                    <Image
                      source={require('../../styles/assets/checked.png')}
                      style={{width: 10, height: 10, alignSelf: 'center'}}
                      resizeMode={'contain'}
                    />
                  ) : (
                    <Text
                      style={{
                        alignSelf: 'center',
                        fontSize: 12,
                        color: COLOR.WHITE,
                      }}>
                      3
                    </Text>
                  )}
                </View>
                <Text
                  style={{
                    fontSize: 12,
                    color: COLOR.WHITE,
                    alignSelf: 'center',
                    marginTop: 3,
                  }}>
                  Payment
                </Text>
              </View>}
              <View
                style={{
                  position: 'absolute',
                  top: 10,
                  left: 50,
                  // width: 100,
                  width: this.state.offerPayment == 1 ? 220 : 100,
                  height: 3,
                  backgroundColor:
                    this.state.screen > 1 ? COLOR.WHITE : '#82BBC7',
                }}
              />
              <View
                style={{
                  position: 'absolute',
                  top: 10,
                  right: 50,
                  // width: 100,
                  width: this.state.offerPayment == 1 ? 0 : 100,
                  height: 3,
                  backgroundColor:
                    this.state.screen == 3 ? COLOR.WHITE : '#82BBC7',
                }}
              />
            </View>
            {this.state.screen == 1 && this.state.offer ? (
              <TripDateView
                marginTop={1}
                departDate={this.state.offer.start_date}
                returnDate={this.state.offer.end_date}
              />
            ) : null}
            <TouchableOpacity
              onPress={() => {
                if (this.state.screen == 3) {
                  this.setState({screen: 2});
                } else if (this.state.screen == 2) {
                  this.setState({screen: 1});
                } else {
                  this.props.navigation.goBack();
                }
              }}
              style={styles.backButton}>
              <Image style={styles.backImage} source={IMAGES.back_arrow} />
            </TouchableOpacity>
          </View>

          {this.state.offer ? this.getScreenFromState(this.state.screen) : null}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexGrow: 1,
    backgroundColor: COLOR.BACKGROUND,
  },
  backButton: {
    alignSelf: 'center',
    height: 15,
    width: 15,
    top: Platform.OS === 'ios' ? 50 : 30,
    left: 20,
    position: 'absolute',
  },
  backImage: {
    tintColor: COLOR.WHITE,
    alignSelf: 'center',
    height: 15,
    width: 15,
  },
  statusBar: {
    height: hp(20),
    backgroundColor: COLOR.GREEN,
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
    paddingTop: Platform.OS === 'ios' ? 40 : 20,
  },
  title: {
    textAlign: 'center',
    color: COLOR.WHITE,
    marginVertical: 0,
    fontSize: FONTS.EXTRA_LARGE,
    fontFamily: FONTS.FAMILY_BOLD,
  },
  statusView: {
    width: 320,
    flexDirection: 'row',
    alignSelf: 'center',
    justifyContent: 'space-between',
    marginVertical: 10,
  },
  modal: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
    backgroundColor: '#00000030',
  },
  modalBody: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    flex: 1,
  },
  appicon_image: {
    height: 90,
    width: 90,
    marginTop: 50,
    marginLeft: 20,
    overflow: 'hidden',
  },
  white_view: {
    width: widthPercentageToDP(90),
    alignSelf: 'center',
    alignItems: 'center',
    backgroundColor: COLOR.WHITE,
    bottom: 20,
    position: 'absolute',
    borderRadius: 20,
  },
  button_content: {
    height: 45,
    width: '100%',
    borderRadius: 22.5,
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
  },
  image_view: {height: 15, width: 15, marginRight: 10},
});

const mapStateToProps = (state, ownProps) => {
  return {
    singlePackageOffer: state.pacakgeReducer.singlePackageOffer,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getSinglePackageOffer: (val) => {
      dispatch(PackageAction.getSinglePackageOffer(val));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(PackagePayment);
