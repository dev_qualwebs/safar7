import React from 'react';
import {View, Text, FlatList} from 'react-native';
import FONTS from '../../styles/Fonts';
import Styles from '../../styles/Styles';
import IMAGES from '../../styles/Images';
import {widthPercentageToDP} from 'react-native-responsive-screen';
import COLOR from '../../styles/Color';
import {ScrollView, TouchableOpacity} from 'react-native-gesture-handler';
import {Image} from 'react-native';
import FastImage from 'react-native-fast-image';

export default class PackagePlans extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      packageDetail: null,
      selectedIndex: 0,
    };
  }

  componentDidMount() {
    console.log('props.route.params.data', this.props.data.route.params.data);
    if (this.props.data.route.params.data) {
      this.setState({
        packageDetail: this.props.data.route.params.data,
      });
    }
  }

  commonView = (props) => {
    return (
      <View
        style={[
          Styles.shadow_view,
          {
            backgroundColor: props.bgColor,
            paddingVertical: 10,
            justifyContent: 'center',
            alignItems: 'center',
            borderRadius: 10,
            width: 50,
            height: 50,
            marginRight: 20,
          },
        ]}>
        <Text
          style={[
            Styles.small_label,
            {
              color:
                props.bgColor == COLOR.WHITE ? COLOR.LIGHT_TEXT : COLOR.WHITE,
              width: null,
            },
          ]}>
          {this.ordinal_suffix_of(props.day)}
        </Text>
        <Text
          style={[
            Styles.body_label,
            {
              fontFamily: FONTS.FAMILY_BOLD,
              color: props.bgColor == COLOR.WHITE ? COLOR.BLACK : COLOR.WHITE,
              width: null,
            },
          ]}>
          Day
        </Text>
      </View>
    );
  };

  ordinal_suffix_of(i) {
    var j = i % 10,
        k = i % 100;
    if (j == 1 && k != 11) {
        return i + "st";
    }
    if (j == 2 && k != 12) {
        return i + "nd";
    }
    if (j == 3 && k != 13) {
        return i + "rd";
    }
    return i + "th";
}

  render() {
    let {packageDetail, selectedIndex} = this.state;
    return (
      <View style={Styles.container}>
        <View
          style={{
            flexDirection: 'row',
            width: widthPercentageToDP(90),
            alignSelf: 'center',
            alignItems: 'center',
          }}>
          <FlatList
          bounces={false}
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}
            style={{
              width: widthPercentageToDP(90),
              alignSelf: 'center',
              marginTop: 10,
            }}
            data={
              packageDetail && packageDetail.package_trip_plan
                ? packageDetail.package_trip_plan.package_trip_details
                : []
            }
            renderItem={({item, index}) => (
              <TouchableOpacity
                onPress={() => {
                  this.setState({selectedIndex: index});
                }}>
                <this.commonView
                  bgColor={selectedIndex == index ? COLOR.GREEN : COLOR.WHITE}
                  day={item.days}
                />
              </TouchableOpacity>
            )}
            horizontal={true}
            keyExtractor={(item, index) => index.toString()}
          />
          <TouchableOpacity
            style={{
              height: 50,
              width: 50,
              alignItems: 'center',
              justifyContent: 'center',
            }}
            onPress={()=>{
              this.props.navigation.navigate('MapViewWithDates',{data:this.state.packageDetail})
            }}
            >
            <Image
              source={IMAGES.zig_zag}
              resizeMode={'contain'}
              style={{height: 30, width: 30}}
            />
          </TouchableOpacity>
        </View>
        <FlatList
        bounces={false}
        showsHorizontalScrollIndicator={false}
        showsVerticalScrollIndicator={false}
          style={{
            width: widthPercentageToDP(90),
            alignSelf: 'center',
            marginTop: 20,
          }}
          data={
            packageDetail && packageDetail.package_trip_plan
              ? packageDetail.package_trip_plan.package_trip_details[
                  selectedIndex
                ].day_plan
              : []
          }
          renderItem={({item}) => (
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate('PackageDetails', {data: item});
              }}>
              <View
                style={{
                  alignItems: 'center',
                  justifyContent: 'center',
                  alignSelf: 'center',
                  marginBottom: 20,
                  height: 55,
                  width: widthPercentageToDP(90),
                  borderRadius: 5,
                  overflow: 'hidden',
                }}>
                <FastImage
                  style={{
                    flex: 1,
                    width: widthPercentageToDP(90),
                  }}
                  source={{
                    uri:
                      item.day_place_plan && item.day_place_plan.place_images.length > 0
                        ? item.day_place_plan.place_images[0].path
                        : 'http://54.176.179.158/safar7/public/images/aa.png',
                    priority: FastImage.priority.normal,
                  }}
                  resizeMode={FastImage.resizeMode.cover}>
                  <View
                    style={{
                      height: 35,
                      top: 10,
                      marginLeft: 10,
                      marginRight: 10,
                      backgroundColor: '#ffffff95',
                      borderRadius: 5,
                      justifyContent: 'center',
                    }}>
                    <Text
                      style={[
                        Styles.body_label,
                        {
                          width: null,
                          alignSelf: 'flex-start',
                          marginHorizontal: 10,
                          fontFamily: FONTS.FAMILY_EXTRA_BOLD,
                        },
                      ]}>
                      {item.places}
                    </Text>
                  </View>
                </FastImage>
              </View>
            </TouchableOpacity>
          )}
          keyExtractor={(item, index) => index.toString()}
        />
      </View>
    );
  }
}
