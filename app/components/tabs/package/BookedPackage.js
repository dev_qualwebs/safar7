import React from 'react';
import {
  ImageBackground,
  View,
  StyleSheet,
  Text,
  Image,
  FlatList,
  TouchableOpacity,
  ScrollView,
  SafeAreaView,
} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import API from '../../../api/Api';
import {T_PACKAGE_DETAIL_TAB} from '../../../helper/Constants';
import COLOR from '../../styles/Color';
import FONTS from '../../styles/Fonts';
import IMAGES from '../../styles/Images';
import Styles from '../../styles/Styles';
import TabBarView from '../home/trip/TabBarView';

let api = new API();
export default class BookedPackage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      tripData: [],
      addonData: [],
      packageDetail: null,
      personCount: 1,
    };

    this.handlePlusMinus = this.handlePlusMinus.bind(this);
  }

  componentDidMount() {
    if (this.props.route.params.data) {
      this.setState({
        packageDetail: this.props.route.params.data,
      });
    }
  }

  handlePlusMinus(val) {
    let {packageDetail} = this.state;
    if (packageDetail != null) {
      if (val == 0) {
        if (this.state.personCount > 0) {
          this.setState({
            personCount: this.state.personCount - 1,
            packageDetail: {
              ...this.state.packageDetail,
              personCount: this.state.personCount - 1,
            },
          });
        }
      } else if (val == 1) {
        if (packageDetail.minimum_travellers > this.state.personCount) {
          this.setState({
            personCount: this.state.personCount + 1,
            packageDetail: {
              ...this.state.packageDetail,
              personCount: this.state.personCount + 1,
            },
          });
        }
      }
    }
  }

  render() {
    const {packageDetail} = this.state;

    return (
      <View style={Styles.container}>
        
        <ImageBackground
          source={{uri: packageDetail ? packageDetail.package_image : ''}}
          resizeMode={'cover'}
          style={styles.back_image}
        />
        <ImageBackground
          source={IMAGES.price_bg}
          style={{
            width: 130,
            height: 50,
            right: 0,
            top: getStatusBarHeight(),
            position: 'absolute',
            alignItems: 'center',
          }}>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Text
              style={[
                {
                  color: COLOR.WHITE,
                  fontSize: FONTS.LARGE,
                  fontFamily: FONTS.FAMILY_BOLD,
                },
              ]}>
              {packageDetail ? parseInt(packageDetail.price) : ''}
            </Text>
            <Text
              style={[
                Styles.small_label,
                {
                  color: COLOR.WHITE,
                  width: null,
                  fontSize: 12,
                  marginTop: 15,
                  marginLeft: 5,
                },
              ]}>
              {packageDetail
                ? packageDetail.currency
                  ? packageDetail.currency.iso4217_code
                  : 'SAR'
                : 'SAR'}
              {'\n'}/ 2 person
            </Text>
          </View>
        </ImageBackground>
        <View
          style={{
            width: 90,
            height: 30,
            backgroundColor: COLOR.RED,
            top: hp(28),
            right: 0,
            position: 'absolute',
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <Text style={[Styles.small_label, {color: COLOR.WHITE, width: null}]}>
            Only {packageDetail ? packageDetail.availability : ''} Left
          </Text>
        </View>
        <View style={{top: 30, position: 'absolute', marginLeft: 20}}>
          <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
            <View
              style={{
                height: 50,
                width: 45,
                alignItems: 'flex-start',
                justifyContent: 'center',
                marginBottom: hp(10),
              }}>
              <Image
                source={IMAGES.back_arrow}
                style={{height: 15, width: 15, tintColor: COLOR.WHITE}}
              />
            </View>
          </TouchableOpacity>
          <Text
            style={[
              Styles.heading_label,
              {
                width: null,
                color: COLOR.WHITE,
                marginVertical: 0,
                marginBottom: 0,
                alignSelf: 'flex-start',
                fontFamily: FONTS.FAMILY_EXTRA_BOLD,
                fontSize: 36,
              },
            ]}>
            {packageDetail ? packageDetail.package_name : ''}
          </Text>
          <Text
            style={[
              Styles.small_label,
              {
                width: null,
                color: COLOR.WHITE,

                fontFamily: FONTS.FAMILY_SEMIBOLD,
                alignSelf: 'flex-start',
              },
            ]}>
            {packageDetail ? packageDetail.duration : ''} Days Trip,{' '}
            {packageDetail ? packageDetail.period_of_travel : ''}
          </Text>
        </View>

        <SafeAreaView style={styles.main_view}>
          <ScrollView
            bounces={false}
            showsVerticalScrollIndicator={false}
            showsHorizontalScrollIndicator={false}
            contentContainerStyle={{flexGrow: 1}}
            style={{flex: 1, marginBottom: 0}}>
            <View
              style={{
                marginVertical: 0,
                width: wp(100),
                alignSelf: 'center',
                flex: 1,
              }}>
              <TabBarView
                currentTab={T_PACKAGE_DETAIL_TAB}
                props={this.props}
              />
              <View style={Styles.line_view} />
              <Text
                style={[
                  Styles.extra_small_label,
                  {
                    alignSelf: 'center',
                    color: COLOR.VOILET,
                    marginVertical: 10,
                  },
                ]}>
                *Please review
                <TouchableOpacity
                  onPress={() => {
                    this.props.navigation.navigate('PolicyPage', {
                      screenTitle: 'Terms and Condition',
                    });
                  }}>
                  <Text
                    style={[
                      Styles.extra_small_label,
                      {
                        width: null,
                        marginBottom:-3,
                        color: COLOR.GREEN,
                      },
                    ]}>
                    {' '}
                    Terms & Conditions
                  </Text>
                </TouchableOpacity>{' '}
                for this package
              </Text>
              <View style={Styles.line_view} />
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  width: wp(90),
                  alignSelf: 'center',
                  marginTop: 20,
                }}>
                <View></View>
              </View>
            </View>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
                marginVertical: 0,
                width: wp(90),
                alignSelf: 'center',
              }}>
              <View style={{marginVertical: 20}}>
                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                  <Text
                    style={[
                      Styles.small_label,
                      {
                        width: null,
                        alignSelf: 'flex-start',
                        color: COLOR.VOILET,
                      },
                    ]}>
                    Starting from:
                  </Text>
                  <Text
                    style={[
                      Styles.small_label,
                      {
                        width: null,

                        color: COLOR.VOILET,
                        fontSize: 10,
                      },
                    ]}>
                    {' '}
                    (Including Tax)
                  </Text>
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    marginVertical: 0,
                  }}>
                  <Text
                    style={[
                      Styles.heading_label,

                      {
                        width: null,
                        color: COLOR.GREEN,
                        marginVertical: 0,
                        alignSelf: 'flex-end',
                        fontFamily: FONTS.FAMILY_EXTRA_BOLD,
                      },
                    ]}>
                    {packageDetail ? parseInt(packageDetail.price) : ''}
                  </Text>
                  <Text
                    style={[
                      Styles.body_label,
                      {
                        width: null,
                        color: COLOR.BLUE,

                        marginLeft: 5,
                        marginBottom: -5,
                      },
                    ]}>
                    SAR
                  </Text>
                  <Text
                    style={[
                      Styles.extra_small_label,
                      {
                        width: null,
                        color: COLOR.BLUE,

                        marginLeft: 5,
                        marginBottom: -10,
                      },
                    ]}>
                    / 2 person
                  </Text>
                </View>
              </View>
            </View>
          </ScrollView>
        </SafeAreaView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  back_image: {
    width: '100%',
    height: hp(40),
  },
  main_view: {
    flex: 1,
    marginTop: -60,
    backgroundColor: COLOR.WHITE,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
  circle_image: {
    height: 10,
    width: 10,
    borderRadius: 5,
    backgroundColor: COLOR.BLACK,
    marginRight: 10,
  },
});
