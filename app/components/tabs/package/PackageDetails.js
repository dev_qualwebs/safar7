import React from 'react';
import {
  ImageBackground,
  View,
  StyleSheet,
  Text,
  Image,
  FlatList,
  TouchableOpacity,
  ScrollView,
  SafeAreaView,
  TouchableWithoutFeedback,
} from 'react-native';
import FastImage from 'react-native-fast-image';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import COLOR from '../../styles/Color';
import FONTS from '../../styles/Fonts';
import IMAGES from '../../styles/Images';
import Styles from '../../styles/Styles';
import ImageView from 'react-native-image-viewing';

export default class PackageDetails extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      tripDetail: null,
      images:[]
    };
  }

  componentDidMount() {
    if (this.props.route.params.data) {
      this.setState({tripDetail: this.props.route.params.data},()=>{
        let imageData = [];
        console.log(this.state.tripDetail.day_place_plan)
        this.state.tripDetail && this.state.tripDetail.day_place_plan && this.state.tripDetail.day_place_plan.place_images && this.state.tripDetail.day_place_plan.place_images.map((val)=>{
                  imageData.push({uri:val.path});
                  console.log(val.path);
                })

                this.setState({images:imageData});
      });
    }
  }

  render() {
    let {tripDetail} = this.state;
    return (
      <View style={Styles.container}>
      <TouchableWithoutFeedback
      onPress={()=>{
        this.setState({isVisible:true})
      }}
      >
        <FastImage
          source={{
            uri: tripDetail
              ? tripDetail.day_place_plan.place_images
                ? tripDetail.day_place_plan.place_images[0].path
                : ''
              : '',
            priority: FastImage.priority.normal,
          }}
          resizeMode={FastImage.resizeMode.cover}
          style={styles.back_image}>
          <Text
            style={[
              Styles.heading_label,
              {
                width: wp(100),
                color: COLOR.WHITE,
                marginVertical: 0,
                marginBottom: 0,
                alignSelf: 'center',
                height: 50,
                fontFamily: FONTS.FAMILY_EXTRA_BOLD,
                fontSize: 36,
                top: getStatusBarHeight(true) + hp(21),
                position: 'absolute',
              },
            ]}>
            {tripDetail ? tripDetail.places : ''}
          </Text>
        </FastImage>
        </TouchableWithoutFeedback>
        <View style={{top: 40, position: 'absolute'}}>
          <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
            <View
              style={{
                height: 50,
                width: 45,
                alignItems: 'center',
                justifyContent: 'center',
                textAlign: 'center',
              }}>
              <Image
                source={IMAGES.back_arrow}
                style={{height: 15, width: 15, tintColor: COLOR.WHITE}}
              />
            </View>
          </TouchableOpacity>
        </View>
        <SafeAreaView style={styles.main_view}>
          <ScrollView
            bounces={false}
            showsVerticalScrollIndicator={false}
            showsHorizontalScrollIndicator={false}
            contentContainerStyle={{flexGrow: 1}}
            style={{flex: 1, marginBottom: 0}}>
            <View
              style={{
                marginVertical: 0,
                width: wp(100),
                alignSelf: 'center',
                flexDirection: 'row',
                backgroundColor: COLOR.GRAY,
                borderTopLeftRadius: 20,
                borderTopRightRadius: 20,
                alignItems: 'flex-start',
                justifyContent: 'space-between',
                paddingHorizontal: 50,
              }}>
              <View
                style={{
                  alignItems: 'flex-start',
                  justifyContent: 'flex-start',
                  marginVertical: 20,
                }}>
                <View style={{flexDirection: 'row'}}>
                  <Image
                    style={{
                      height: 20,
                      width: 20,
                      tintColor: COLOR.GREEN,
                      marginRight: 10,
                    }}
                    source={IMAGES.calendar_cut}
                  />
                  <Text
                    style={[
                      Styles.small_label,
                      {
                        fontFamily: FONTS.FAMILY_BOLD,
                        color: COLOR.BLACK,
                        width: null,
                      },
                    ]}>
                    OPENING DAY
                  </Text>
                </View>
                <Text
                  style={[
                    Styles.small_label,
                    {
                      fontFamily: FONTS.FAMILY_SEMIBOLD,
                      color: COLOR.LIGHT_TEXT,
                      fontSize: 14,
                      width: null,
                      marginTop: 5,
                    },
                  ]}>
                  {tripDetail ? tripDetail.day_place_plan.opening_days : ''}
                </Text>
              </View>
              <View
                style={{
                  height: '100%',
                  width: 1,
                  backgroundColor: '#d8d8d8',
                }}
              />
              <View
                style={{
                  alignItems: 'center',
                  justifyContent: 'center',
                  marginVertical: 20,
                }}>
                <View style={{flexDirection: 'row'}}>
                  <Image
                    style={{
                      height: 20,
                      width: 20,
                      tintColor: COLOR.GREEN,
                      marginRight: 10,
                    }}
                    source={IMAGES.time}
                  />
                  <Text
                    style={[
                      Styles.small_label,
                      {
                        fontFamily: FONTS.FAMILY_BOLD,
                        color: COLOR.BLACK,
                        width: null,
                      },
                    ]}>
                    OPENING TIME
                  </Text>
                </View>
                <Text
                  style={[
                    Styles.small_label,
                    {
                      width: null,
                      fontFamily: FONTS.FAMILY_SEMIBOLD,
                      color: COLOR.LIGHT_TEXT,
                      fontSize: 14,
                      marginTop: 5,
                    },
                  ]}>
                  {tripDetail ? tripDetail.day_place_plan.opening_time : ''}
                </Text>
                {/* <Text
                  style={[
                    Styles.small_label,
                    {
                      width: null,
                      fontFamily: FONTS.FAMILY_SEMIBOLD,
                      color: COLOR.LIGHT_TEXT,
                      fontSize: 14,
                      marginTop: 5,
                    },
                  ]}>
                  (GMT +1)
                </Text> */}
              </View>
            </View>
            <Text
              style={[
                Styles.small_label,
                {
                  color: COLOR.GREEN,
                  marginTop: 20,
                  marginBottom: 10,
                  fontFamily: FONTS.FAMILY_BOLD,
                },
              ]}>
              Description
            </Text>
            <Text style={[Styles.small_label, {fontSize: 14, width: wp(80)}]}>
              {tripDetail ? tripDetail.day_place_plan.description : ''}
            </Text>
            <Text
              style={[
                Styles.small_label,
                {
                  color: COLOR.GREEN,
                  marginTop: 20,
                  marginBottom: 10,
                  fontFamily: FONTS.FAMILY_BOLD,
                },
              ]}>
              How To Get There
            </Text>
            <Text style={[Styles.small_label, {fontSize: 14, width: wp(80)}]}>
              {tripDetail ? tripDetail.day_place_plan.route : ''}
            </Text>
            <View
              style={{
                flexDirection: 'row',
                width: wp(90),
                alignSelf: 'center',
                marginTop: 20,
              }}>
              <Image
                style={{
                  height: 20,
                  width: 20,
                  tintColor: COLOR.GREEN,
                  marginRight: 10,
                }}
                source={IMAGES.info_up}
                resizeMode={'contain'}
              />
              <Text
                style={[
                  Styles.body_label,
                  {
                    color: COLOR.BLACK,
                    width: null,
                    fontSize: 14,
                  },
                ]}>
                Ticket can be reserved individually,
                <Text style={{color: COLOR.GREEN}}> Click Here</Text>
              </Text>
            </View>
            <View style={[Styles.line_view, {marginTop: 10}]} />
            <View
              style={{
                flexDirection: 'row',
                width: wp(90),
                alignSelf: 'center',
                marginTop: 20,
              }}>
              <Image
                style={{
                  height: 20,
                  width: 20,
                  tintColor: COLOR.GREEN,
                  marginRight: 10,
                }}
                source={IMAGES.open_website}
                resizeMode={'contain'}
              />
              <Text
                style={[
                  Styles.body_label,
                  {
                    color: COLOR.BLACK,
                    width: null,
                    fontSize: 14,
                  },
                ]}>
                {tripDetail ? tripDetail.day_place_plan.website : ''}
              </Text>
            </View>
            <View
              style={{
                flexDirection: 'row',
                width: wp(90),
                alignSelf: 'center',
                marginTop: 20,
              }}>
              <Image
                style={{
                  height: 20,
                  width: 20,
                  marginRight: 10,
                }}
                source={IMAGES.pinpoint}
                resizeMode={'cover'}
              />
              <Text
                style={[
                  Styles.body_label,
                  {
                    color: COLOR.BLACK,
                    width: null,
                    fontSize: 14,
                    marginRight: 20,
                  },
                ]}>
                {tripDetail ? tripDetail.day_place_plan.location : ''}
              </Text>
            </View>
            <View
              style={{
                flexDirection: 'row',
                width: wp(90),
                alignSelf: 'center',
                marginTop: 20,
              }}>
              <Image
                style={{
                  height: 20,
                  width: 20,
                  tintColor: COLOR.GREEN,
                  marginRight: 10,
                }}
                source={IMAGES.call}
                resizeMode={'contain'}
              />
              <Text
                style={[
                  Styles.body_label,
                  {
                    color: COLOR.BLACK,
                    width: null,
                    fontSize: 14,
                  },
                ]}>
                {tripDetail ? tripDetail.day_place_plan.phone : ''}
              </Text>
            </View>
          </ScrollView>
          <ImageView
          animationType={'fade'}
            images={this.state.images}
            imageIndex={0}
            visible={this.state.isVisible}
            onRequestClose={() => this.setState({isVisible: false})}
          />
        </SafeAreaView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  back_image: {
    width: '100%',
    height: hp(40),
  },
  main_view: {
    flex: 1,
    marginTop: -60,
    backgroundColor: COLOR.WHITE,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
  circle_image: {
    height: 10,
    width: 10,
    borderRadius: 5,
    backgroundColor: COLOR.BLACK,
    marginRight: 10,
  },
});
