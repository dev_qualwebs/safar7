import React from 'react';
import {StyleSheet} from 'react-native';
import {View, Text, FlatList} from 'react-native';
import {TouchableOpacity} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import COLOR from '../../styles/Color';
import Styles from '../../styles/Styles';
import PaymentServiceView from '../payments/PaymentServiceView';

export default class PackagePaymentSummary extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modes: this.props.travellingModes,
    };
  }

  render() {
    return (
      <View style={styles.container}>
        <FlatList
        bounces={false}
        showsVerticalScrollIndicator={false}
          data={this.state.modes}
          renderItem={({item}) => (
            <View style={{marginHorizontal: 15, marginVertical: 7.5}}>
              <PaymentServiceView item={item} />
            </View>
          )}
          keyExtractor={(val, index) => index}
        />

        <TouchableOpacity
          onPress={() => this.props.changeScreen(2)}
          style={styles.continueButton}>
          <Text style={[Styles.button_font, {color: COLOR.WHITE}]}>
           {this.props.offerPayment == 1 ? 'Next' : 'Continue'}
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {flex: 1, paddingTop: 15},
  bookingButton: {
    backgroundColor: COLOR.WHITE,
    borderRadius: 30,
    width: 150,
    height: 35,
    marginTop: 20,

    marginHorizontal: 20,
    flexDirection: 'row',
    justifyContent: 'space-evenly',
  },
  bookingText: {
    color: COLOR.BLUE,
    alignSelf: 'center',
  },
  rArrow: {alignSelf: 'center', width: 15, height: 15},
  continueButton: {
    alignItems: 'center',
    justifyContent: 'center',
    height: 50,
    borderRadius: 10,
    width: wp(90),
    marginTop: 10,
    marginBottom: 20,
    backgroundColor: COLOR.GREEN,
    alignSelf: 'center',
  },
});
