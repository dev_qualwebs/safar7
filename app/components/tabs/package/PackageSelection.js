import React from 'react';
import {View, Text, SafeAreaView, Image, StyleSheet} from 'react-native';
import {TouchableOpacity} from 'react-native';
import IMAGES from '../../styles/Images';
import Styles from '../../styles/Styles';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import COLOR from '../../styles/Color';
import NavigationBar from '../../commonView/NavigationBar';
import FONTS from '../../styles/Fonts';
import SelectDate from '../calendar/SelectDate';
import moment from 'moment';

export default class PackageSelection extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      packageDetail: null,
      dateIndex: 1,
      checkInDate: null,
      checkOutDate: null,
      openCalendar: false,
      minDate: moment().valueOf(),
    };

    this.handleNextButton = this.handleNextButton.bind(this);
  }

  componentDidMount() {
    console.log(Math.round(moment().add(10, 'days').valueOf() / 1000));
    if (this.props.route.params.data) {
      this.setState({
        packageDetail: this.props.route.params.data,
      });
    }
  }

  updateMasterState = (attrName, value) => {
    this.setState({[attrName]: value}, () => {
      if (this.state.checkInDate && this.state.checkOutDate) {
        this.props.navigation.navigate('PaymentScreen', {
          is_package: 1,
          is_date_change: '1',
          start_date: this.state.checkInDate,
          end_date: this.state.checkOutDate,
          data: this.state.packageDetail,
        });
      }
    });
  };

  commonView = (props) => {
    let {dateIndex} = this.state;
    return (
      <View
        style={[
          styles.selectionView,
          {
            borderColor: props.index == dateIndex ? COLOR.GREEN : COLOR.GRAY,

            backgroundColor:
              props.index == dateIndex ? '#05778F10' : COLOR.WHITE,
            elevation: 3,
          },
        ]}>
        <TouchableOpacity
          onPress={() => {
            this.setState({dateIndex: props.index});
          }}
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <View
            style={{
              height: 70,
              width: 70,
              backgroundColor: COLOR.GRAY,
              alignItems: 'center',
              justifyContent: 'center',
              borderRadius: 10,
              marginBottom: 10,
            }}>
            <Image
              style={styles.calendarImage}
              source={props.image}
              resizeMode={'contain'}
            />
          </View>
          <Text style={[Styles.body_label, {width: null}]}>{props.name}</Text>
        </TouchableOpacity>
      </View>
    );
  };

  handleNextButton() {
    if (this.state.dateIndex == 2) {
      this.setState({openCalendar: true});
    } else {
      this.props.navigation.navigate('PaymentScreen', {
        is_package: 1,
        data: this.state.packageDetail,
        is_date_change: '0',
      });
    }
  }

  render() {
    let {packageDetail, dateIndex} = this.state;
    return (
      <SafeAreaView style={Styles.container}>
        <NavigationBar
          left_image={IMAGES.back_arrow}
          right_image={IMAGES.help}
          prop={this.props}
          navHeight={45}
          name={'Package Selection'}
        />
        <View style={{alignSelf: 'center', width: wp(90), flex: 1}}>
          <View>
            <Text
              style={[
                styles.headingLabel,
                {
                  width: wp(90),
                  alignSelf: 'center',
                  marginTop: 20,
                  marginBottom: 10,
                },
              ]}>
              Package Name
            </Text>
            <Text style={Styles.body_label}>
              {packageDetail ? packageDetail.package_name : ''}
            </Text>
          </View>
          <View
            style={[
              styles.singleLineView,
              {width: wp(90), alignSelf: 'center'},
            ]}
          />

          <View
            style={{
              justifyContent: 'space-between',
              flexDirection: 'row',
              alignItems: 'center',
              marginTop: 50,
            }}>
            <this.commonView
              index={1}
              image={IMAGES.calendar_arrow}
              name={'Same Dates'}
            />
            <this.commonView
              index={2}
              image={IMAGES.calendar_tick}
              name={'Change Dates'}
            />
          </View>
          {dateIndex == 2 && (
            <Text style={[Styles.small_label, {color: COLOR.RED}]}>
              Changes of Dates may affect the price of the package.
            </Text>
          )}
          <TouchableOpacity
            onPress={this.handleNextButton}
            style={[Styles.button_content, {bottom: 20, position: 'absolute'}]}>
            <Text style={Styles.button_font}>Next</Text>
          </TouchableOpacity>
        </View>
        {this.state.openCalendar ? (
          <SelectDate
            props={this.props}
            updateMasterState={this.updateMasterState}
            modal={true}
            minDate={this.state.minDate}
          />
        ) : null}
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  headingLabel: {
    fontFamily: FONTS.FAMILY_REGULAR,
    fontSize: FONTS.SMALL,
    color: COLOR.LIGHT_TEXT_COLOR,
  },
  singleLineView: {
    marginVertical: 20,
    borderBottomColor: COLOR.GRAY,
    width: wp(100),
    alignSelf: 'center',
    borderBottomWidth: 1,
  },
  selectionView: {
    borderColor: COLOR.GRAY,
    borderWidth: 1,
    borderRadius: 10,
    shadowColor: COLOR.VOILET,
    shadowOpacity: 0.1,
    marginBottom: 20,
    width: wp(43),
    height: 130,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: COLOR.WHITE,
    elevation: 3,
  },
  calendarImage: {height: 50, aspectRatio: 1},
});
