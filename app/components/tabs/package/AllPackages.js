import React from 'react';
import {View, Text, Image, SafeAreaView} from 'react-native';
import {FlatList, TouchableWithoutFeedback} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {widthPercentageToDP, widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {connect} from 'react-redux';
import PackageAction from '../../../redux/action/PackageAction';
import NavigationBar from '../../commonView/NavigationBar';
import COLOR from '../../styles/Color';
import FONTS from '../../styles/Fonts';
import IMAGES from '../../styles/Images';
import Styles from '../../styles/Styles';

class AllPackages extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      packageData: [],
    };
  }

  componentDidMount() {
    if (this.props.allPackageData) {
      this.setState(
        {
          packageData: this.props.allPackageData,
        },
        () => {},
      );
    }
  }

  horizontalView = (props) => {
    return (
      <View
      style={{
        flexDirection: 'row',
        justifyContent: 'space-between',
        height: 30,
        // backgroundColor: COLOR.GRAY,
        marginRight: 10,
        marginLeft:5,
        alignItems: 'center',
        alignSelf: 'center',
        // width:widthPercentageToDP(90)
      }}>
      <Image
      source={{uri: props.item.addon.image ? props.item.addon.image : ''}}
      style={{height: 10, width: 10,marginRight:5}}
      resizeMode={'contain'}
    /> 
    <Text style={[Styles.small_label, {width: null}]}>
     {props.item.addon.name ? props.item.addon.name : ''}
    </Text>
    </View>
    );
  }

  componentWillReceiveProps(props) {
    if (props.allPackageData != this.props.allPackageData) {
      this.setState({
        packageData: props.allPackageData,
      });
    }
  }

  render() {
    return (
      <SafeAreaView style={Styles.container}>
        <NavigationBar
          right_image={IMAGES.filter_funnel}
          left_image={IMAGES.back_arrow}
          prop={this.props}
          navHeight={40}
          name={'Safar7 Packages'}
          rightNav={''}
        />
        <View style={{flex: 1}}>
          <FlatList
          bounces={false}
            data={this.state.packageData}
            renderItem={({item}) => (
              <TouchableOpacity
                onPress={() =>
                  this.props.navigation.navigate('SinglePackageDetail', {
                    data: item,
                  })
                }>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    marginTop: 0,

                    width: wp(90),
                    alignSelf: 'center',
                    alignItems: 'center',
                  }}>
                  <Image
                    style={{height: 90, width: 90, marginVertical: 10,borderRadius:5}}
                    source={{uri: item.package_image}}
                  />
                  <View style={{flex: 1, marginLeft: 10}}>
                    <Text
                      style={[
                        Styles.subheading_label,
                        {
                          width: null,
                          alignSelf: 'flex-start',
                          fontFamily: FONTS.FAMILY_SEMIBOLD,
                          marginTop: 5,
                        },
                      ]}>
                      {item.package_name}
                    </Text>
                    <Text
                      style={[
                        Styles.small_label,
                        {width: null, alignSelf: 'flex-start'},
                      ]}>
                      {item.duration} Day Trip | {item.period_of_travel}
                    </Text>
                    <View
                      style={{
                        flexDirection: 'row',
                        marginTop: 10,
                        alignItems: 'center',
                      }}>
                      <Text
                        style={[
                          Styles.heading_label,
                          {
                            width: null,
                            color: COLOR.BLUE,
                            marginVertical: 0,
                            fontFamily: FONTS.FAMILY_BOLD,
                          },
                        ]}>
                        {item.price}
                      </Text>
                      <Text
                        style={[
                          Styles.extra_small_label,
                          {width: null, marginBottom: -5, color: COLOR.BLUE},
                        ]}>
                        {'   '}
                        SAR{' '}
                      </Text>
                      <Text
                        style={[
                          Styles.extra_small_label,
                          {width: null, marginBottom: -5},
                        ]}>
                        / Person
                      </Text>
                    </View>
                  </View>

                  <Image
                    resizeMode={'contain'}
                    style={{height: 10, width: 10, alignSelf: 'center'}}
                    source={IMAGES.right_arrow}
                  />
                </View>
                {/* <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    height: 30,
                    backgroundColor: COLOR.GRAY,
                    paddingHorizontal: 20,
                    alignItems: 'center',
                    width: wp(90),
                    alignSelf: 'center',
                    marginBottom: 20,
                  }}>
                  <Text style={[Styles.small_label, {width: null}]}>
                    • Direct
                  </Text>
                  <Text style={[Styles.small_label, {width: null}]}>
                    • 4 Start
                  </Text>
                  <Text style={[Styles.small_label, {width: null}]}>
                    • Economy Car
                  </Text>
                  <Text style={[Styles.small_label, {width: null}]}>
                    • 10 Attractions
                  </Text>
                </View> */}
                 {/* <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      height: 30,
                      backgroundColor: COLOR.GRAY,
                      paddingHorizontal: 20,
                      alignItems: 'center',
                      alignSelf: 'center',
                      marginBottom: 20,
                      width:widthPercentageToDP(90)
                    }}>
                {item.package_exclude_include.package_addons.map((val) => {
                  <>
                     <Image
                      source={{uri: ''}}
                      style={{height: 10, width: 10}}
                      resizeMode={'contain'}
                    /> 
                    <Text style={[Styles.small_label, {width: null}]}>
                      hello
                    </Text>
                    </>
                })}
                 </View> */}
                    <View
      style={{
         backgroundColor: COLOR.GRAY,
         height:30,
         alignItems: 'center',
        flexDirection:'row',
        alignSelf: 'center',
         width:widthPercentageToDP(90),
         marginBottom: 20,
      }}>
                 <FlatList 
                 horizontal
                 bounces={false}
                 data={item.package_exclude_include.package_addons}
                 renderItem={this.horizontalView}
                 />
                 </View>
              </TouchableOpacity>
            )}
            numColumns={'1'}
            removeClippedSubviews={false}
            showsVerticalScrollIndicator={false}
            showsHorizontalScrollIndicator={false}
            keyExtractor={(item, index) => index.toString()}
          />
        </View>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    allPackageData: state.pacakgeReducer.allPackageData,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getAllPackages: (val) => {
      dispatch(PackageAction.getAllPackages(val));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(AllPackages);
