import React from 'react';
import { View, Text } from 'react-native';
import FONTS from '../../styles/Fonts';
import Styles from '../../styles/Styles';
import { widthPercentageToDP } from 'react-native-responsive-screen';


export default class PackageHightlights extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      packageHighlight: null,
    };
  }

  componentDidMount() {
    if (this.props.data.route.params.data) {
      this.setState({
        packageHighlight: this.props.data.route.params.data,
      });
    }
  }

  pointView = (prop) => {
    return (
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          width: widthPercentageToDP(90),
          alignSelf: 'center',
          marginBottom: 10,
        }}>
        <Text
          style={[
            Styles.body_label,
            { width: null, alignSelf: 'flex-start', fontSize: 14 },
          ]}>
          •{'  '}
          {prop.text}
        </Text>
      </View>
    );
  };

  render() {
    let { packageHighlight } = this.state;
    return (
      <View style={Styles.container}>
        <Text
          style={[
            Styles.small_label,
            {
              width: widthPercentageToDP(90),
              alignSelf: 'center',
              fontFamily: FONTS.FAMILY_BOLD,
              marginVertical: 20,
            },
          ]}>
          HIGHLIGHTS
        </Text>
        {packageHighlight
          ? packageHighlight.package_detail ? packageHighlight.package_detail.english_highlight.map((item) => {
            return <this.pointView text={item.highlight} />;
          })
            : null : null}
      </View>
    );
  }
}
