import React from 'react';
import {
  View,
  SafeAreaView,
  StyleSheet,
} from 'react-native';
import Styles from '../../styles/Styles';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import NavigationBar from '../../commonView/NavigationBar';
import TopTabBar from '../home/trip/TabBarView';
import { T_SCHEDULE_TRIP_TAB } from '../../../helper/Constants';
import IMAGES from '../../styles/Images';

export default class PackageScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <View style={Styles.container}>
        <SafeAreaView>
          <NavigationBar
            prop={this.props}
            isMainHeading={true}
            right_image={IMAGES.notification_bell}
            navHeight={'45'}
            name={'Trips'}
          />
          <TopTabBar currentTab={T_SCHEDULE_TRIP_TAB} />
        </SafeAreaView>
      </View>
    );
  }
}

const classStyle = StyleSheet.create({
  flatlist_view: {
    marginTop: 0,
    width: wp(100),
    marginBottom: 0,
    height: hp(50),
    marginHorizontal: 10,
  },
  flatlist_image_view: {
    height: hp(50),
    width: wp(70),
    marginRight: 0,
  },
  offer_view: {
    marginVertical: 0,
    width: wp(100),
    marginBottom: 20,
    height: 100,
    marginHorizontal: 10,
  },
});
