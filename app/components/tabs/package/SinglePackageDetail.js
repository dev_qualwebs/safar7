import React from 'react';
import {
  ImageBackground,
  View,
  StyleSheet,
  Text,
  Image,
  FlatList,
  TouchableOpacity,
  ScrollView,
  SafeAreaView,
} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import {T_PACKAGE_DETAIL_TAB} from '../../../helper/Constants';
import COLOR from '../../styles/Color';
import FONTS from '../../styles/Fonts';
import IMAGES from '../../styles/Images';
import Styles from '../../styles/Styles';
import TabBarView from '../home/trip/TabBarView';

export default class SinglePackageDetail extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      addonData: [],
      packageDetail: null,
      personCount: 1,
      minimumCount: 1,
    };

    this.handlePlusMinus = this.handlePlusMinus.bind(this);
  }

  componentDidMount() {
    if (this.props.route.params.data) {
      this.setState(
        {
          packageDetail: this.props.route.params.data,
          personCount: this.props.route.params.data.minimum_travellers,
          minimumCount: this.props.route.params.data.minimum_travellers,
        },
        () => {
          this.setState({
            packageDetail: {
              ...this.state.packageDetail,
              personCount: this.state.personCount,
            },
          });
        },
      );
    }
  }

  handlePlusMinus(val) {
    let {packageDetail} = this.state;
    if (packageDetail != null) {
      if (val == 0) {
        if (
          this.state.personCount > this.state.packageDetail.minimum_travellers
        ) {
          this.setState({
            personCount: this.state.personCount - 1,
            packageDetail: {
              ...this.state.packageDetail,
              personCount: this.state.personCount - 1,
            },
          });
        }
      } else if (val == 1) {
        if (packageDetail.maximum_travellers > this.state.personCount) {
          this.setState({
            personCount: this.state.personCount + 1,
            packageDetail: {
              ...this.state.packageDetail,
              personCount: this.state.personCount + 1,
            },
          });
        }
      }
    }
  }

  render() {
    const {packageDetail} = this.state;

    return (
      <ScrollView
      showsVerticalScrollIndicator={false}
bounces={false}

>
      <View style={Styles.container}>
        <ImageBackground
          source={{uri: packageDetail ? packageDetail.package_image : ''}}
          resizeMode={'cover'}
          style={[styles.back_image]}
        />
        <ImageBackground
          source={IMAGES.price_bg}
          style={{
            width: 130,
            height: 50,
            right: 0,
            top: getStatusBarHeight(),
            position: 'absolute',
            alignItems: 'center',
          }}>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Text
              style={[
                {
                  color: COLOR.WHITE,
                  fontSize: FONTS.LARGE,
                  fontFamily: FONTS.FAMILY_BOLD,
                },
              ]}>
              {packageDetail
                ? parseInt(packageDetail.price) * this.state.minimumCount
                : ''}
            </Text>
            <Text
              style={[
                Styles.small_label,
                {
                  color: COLOR.WHITE,
                  width: null,
                  fontSize: 12,
                  marginTop: 15,
                  marginLeft: 5,
                },
              ]}>
              {packageDetail
                ? packageDetail.currency
                  ? packageDetail.currency.iso4217_code
                  : 'SAR'
                : 'SAR'}
              {'\n'}/ {this.state.minimumCount} person
            </Text>
          </View>
        </ImageBackground>
        <View
          style={{
            width: 90,
            height: 30,
            backgroundColor: COLOR.RED,
            top: hp(28),
            right: 0,
            position: 'absolute',
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <Text style={[Styles.small_label, {color: COLOR.WHITE, width: null}]}>
            Only {packageDetail ? packageDetail.availability : ''} Left
          </Text>
        </View>
        <View style={{top: 45, position: 'absolute', marginLeft: 30}}>
          <View style={{height: 50, width: 45,  marginBottom: hp(10),}}>
            <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
              <View
                style={{
                  height: 50,
                  width: 45,
                  alignItems: 'flex-start',
                  justifyContent: 'center',
                }}>
                <Image
                  source={IMAGES.back_arrow}
                  style={{height: 15, width: 15, tintColor: COLOR.WHITE}}
                />
              </View>
            </TouchableOpacity>
          </View>
          <Text
            style={[
              Styles.heading_label,
              {
                width: null,
                color: COLOR.WHITE,
                marginVertical: 0,
                marginBottom: 0,
                alignSelf: 'flex-start',
                fontFamily: FONTS.FAMILY_EXTRA_BOLD,
                fontSize: 36,
              },
            ]}>
            {packageDetail ? packageDetail.package_name : ''}
          </Text>
          <View style={{flexDirection: 'row', margin: 10}}>
              <Text
                style={[
                  Styles.body_label,
                  {
                    borderRadius: 12.5,
                    height: 25,
                    borderWidth: 1,
                    width: null,
                    borderColor: COLOR.WHITE,
                    color: COLOR.WHITE,
                    paddingHorizontal: 10,
                    alignItems: 'center',
                  },
                ]}>
                {packageDetail ? packageDetail.duration : ''} Days Trip
              </Text>
              <Text
                style={[
                  Styles.body_label,
                  {
                    height: 25,
                    width: null,
                    color: COLOR.WHITE,
                    alignItems: 'center',
                    marginLeft: 10,
                  },
                ]}>
                {packageDetail ? packageDetail.period_of_travel : ''}
              </Text>
            </View>
        </View>

        <SafeAreaView style={styles.main_view}>
          <ScrollView
            bounces={false}
            showsVerticalScrollIndicator={false}
            showsHorizontalScrollIndicator={false}
            contentContainerStyle={{flexGrow: 1}}
            style={{flex: 1, marginBottom: 0}}>
            <View
              style={{
                marginVertical: 0,
                width: wp(100),
                alignSelf: 'center',
                flex: 1,
              }}>
              <TabBarView
                currentTab={T_PACKAGE_DETAIL_TAB}
                props={this.props}
              />
            </View>
          </ScrollView>
          <View style={Styles.line_view} />
          <Text
            style={[
              Styles.extra_small_label,
              {
                alignSelf: 'center',
                color: COLOR.VOILET,
                marginVertical: 10,
              },
            ]}>
            *Please review
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate('PolicyPage', {
                  screenTitle: 'Terms and Condition',
                });
              }}>
              <Text
                style={[
                  Styles.extra_small_label,
                  {
                    width: null,
                    marginBottom: -3,
                    color: COLOR.GREEN,
                  },
                ]}>
                {' '}
                Terms & Conditions
              </Text>
            </TouchableOpacity>{' '}
            for this package
          </Text>
          <View style={Styles.line_view} />
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              width: wp(90),
              alignSelf: 'center',
              marginTop: 20,
            }}>
            <View>
              <Text
                style={[
                  Styles.subheading_label,
                  {
                    width: null,
                    alignSelf: 'flex-start',
                    fontFamily: FONTS.FAMILY_BOLD,
                  },
                ]}>
                Person
              </Text>
            </View>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'flex-end',
              }}>
              <TouchableOpacity
                onPress={() => this.handlePlusMinus(0)}
                style={{
                  height: 30,
                  width: 30,
                  borderRadius: 15,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Image
                  style={{
                    height: 30,
                    width: 30,
                    opacity:
                      (packageDetail &&
                        packageDetail.minimum_travellers ==
                          this.state.personCount) ||
                      this.state.personCount == 0
                        ? 0.4
                        : 1,
                  }}
                  resizeMode={'contain'}
                  source={IMAGES.minus_image}
                />
              </TouchableOpacity>
              <Text
                style={[
                  Styles.button_font,
                  {width: 40, color: COLOR.BLACK, textAlign: 'center'},
                ]}>
                {this.state.personCount}
              </Text>
              <TouchableOpacity onPress={() => this.handlePlusMinus(1)}>
                <View
                  style={{
                    height: 30,
                    width: 30,
                    borderWidth: 1,
                    borderRadius: 15,
                    borderColor: COLOR.GREEN,
                    opacity:
                      packageDetail &&
                      this.state.personCount == packageDetail.maximum_travellers
                        ? 0.4
                        : 1,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Image
                    style={{
                      height: 20,
                      width: 20,
                      opacity:
                        packageDetail &&
                        this.state.personCount ==
                          packageDetail.maximum_travellers
                          ? 0.4
                          : 1,
                    }}
                    source={IMAGES.plus_image}
                  />
                </View>
              </TouchableOpacity>
            </View>
          </View>

          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
              marginVertical: 0,
              width: wp(90),
              alignSelf: 'center',
            }}>
            <View style={{marginVertical: 20}}>
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <Text
                  style={[
                    Styles.small_label,
                    {
                      width: null,
                      alignSelf: 'flex-start',
                      color: COLOR.VOILET,
                    },
                  ]}>
                  Starting from:
                </Text>
                <Text
                  style={[
                    Styles.small_label,
                    {
                      width: null,

                      color: COLOR.VOILET,
                      fontSize: 10,
                    },
                  ]}>
                  {' '}
                  (Including Tax)
                </Text>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  marginVertical: 0,
                }}>
                <Text
                  style={[
                    Styles.heading_label,

                    {
                      width: null,
                      color: COLOR.GREEN,
                      marginVertical: 0,
                      alignSelf: 'flex-end',
                      fontFamily: FONTS.FAMILY_EXTRA_BOLD,
                    },
                  ]}>
                  {packageDetail
                    ? parseInt(packageDetail.price) * this.state.personCount
                    : ''}
                </Text>
                <Text
                  style={[
                    Styles.body_label,
                    {
                      width: null,
                      color: COLOR.BLUE,
                      marginLeft: 5,
                      marginBottom: -5,
                    },
                  ]}>
                  SAR
                </Text>
                <Text
                  style={[
                    Styles.extra_small_label,
                    {
                      width: null,
                      color: COLOR.BLUE,

                      marginLeft: 5,
                      marginBottom: -10,
                    },
                  ]}>
                  {'/ ' + this.state.personCount + ' person'}
                </Text>
              </View>
            </View>
            <TouchableOpacity
              onPress={() =>
                this.props.navigation.navigate('PackageSelection', {
                  data: this.state.packageDetail,
                })
              }>
              <View
                style={[
                  Styles.button_content,
                  {backgroundColor: COLOR.GREEN, width: 150},
                ]}>
                <Text style={[Styles.button_font, {width: null}]}>
                  Continue
                </Text>
              </View>
            </TouchableOpacity>
          </View>
        </SafeAreaView>
      </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  back_image: {
    width: '100%',
    height: 350,
  },
  main_view: {
    flex: 1,
    marginTop: -60,
    backgroundColor: COLOR.WHITE,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
  circle_image: {
    height: 10,
    width: 10,
    borderRadius: 5,
    backgroundColor: COLOR.BLACK,
    marginRight: 10,
  },
});
