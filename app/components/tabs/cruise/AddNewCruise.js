import React from 'react';
import {Modal, Text, View, StyleSheet, Image, Button, Alert} from 'react-native';
import {KeyboardAvoidingView} from 'react-native';
import {TextInput, TouchableOpacity} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import SimpleToast from 'react-native-simple-toast';
import COLOR from '../../../components/styles/Color';
import FONTS from '../../../components/styles/Fonts';
import IMAGES from '../../../components/styles/Images';
import Styles from '../../../components/styles/Styles';
import { DoneButtonKeyboard } from '../../commonView/CommonView';
import {
  getEndDateTimestamp,
  getStartDateTimestamp,
} from '../../commonView/Helpers';
import NavigationBar from '../../commonView/NavigationBar';
import SelectDate from '../calendar/SelectDate';

export default class AddNewCruise extends React.Component {
  constructor(props) {
    super(props);
    let route = this.props.route.params;
    this.state = {
      modalVisible: true,
      openCalendar: false,
      from: '',
      to: '',
      checkInDate: '',
      days: 0,
      id: route.id,
      cruiseId: null,
      fromFlexibility: 0,
      toFlexibility: 0,
      minDate: getStartDateTimestamp(route.depart),
      maxDate: getEndDateTimestamp(route.return),
      tripDepart: route.depart,
      tripReturn: route.return,
      package_request_id: route.package_request_id,
    };
  }

  componentDidMount() {
    if (this.props.route.params.data) {
      let params = this.props.route.params;
      this.setState({
        from: params.data.from,
        to: params.data.to,
        checkInDate: params.data.depart,
        days: `${params.data.days}`,
        cruiseId: params.data.id,
        tripDepart: params.depart,
        tripReturn: params.return,
      });
    }
    console.log(this.props.route.params.depart);
  }

  updateMasterState = (attrName, value) => {
    this.setState({[attrName]: value});
  };

  handleNavigation = () => {
    if (this.state.from == '') {
      SimpleToast.show('Enter From location');
    } else if (this.state.checkInDate == '' || this.state.checkInDate == null) {
      SimpleToast.show('Select departure date');
    } else if (this.state.to == '') {
      SimpleToast.show('Enter to location');
    } else if (this.state.days == '') {
      SimpleToast.show('Enter days count');
    } else {
      this.setState({modalVisible: false});
      let params = {
        from: this.state.from,
        to: this.state.to,
        depart: this.state.checkInDate,
        days: this.state.days,
      };
      if (this.state.cruiseId) {
        this.props.navigation.replace('CruiseOption', {
          params: params,
          id: this.state.id,
          cruiseId: this.state.cruiseId,
          package_request_id: this.state.package_request_id,
        });
      } else {
        this.props.navigation.replace('CruiseOption', {
          params: params,
          id: this.state.id,
          package_request_id: this.state.package_request_id,
        });
      }
    }
  };

  render() {
    return (
      <View style={Styles.container}>
        {this.state.openCalendar ? (
          <SelectDate
            selector={this.state.selector}
            updateMasterState={this.updateMasterState}
            props={this.props.props}
            minDate={this.state.minDate}
            maxDate={this.state.maxDate}
            modal={true}
          />
        ) : (
          <Modal
            animationType={'slide'}
            transparent={true}
            style={{backgroundColor: COLOR.BLACK}}
            visible={this.state.modalVisible}
            isModal={this.props}
            onRequestClose={() => {
              console.log('Modal has been closed.');
            }}>
            <View style={styles.modal}>
              <KeyboardAvoidingView
                behavior={'padding'}
                style={styles.modalBody}>
                <NavigationBar
                  prop={this.props}
                  navHeight={40}
                  name={this.state.cruiseId ? 'Edit Cruise' : 'Add New Cruise'}
                />
                <View
                  style={{alignSelf: 'center', width: wp(90), marginTop: 10}}>
                  <View style={styles.editView}>
                    <Image
                      style={{height: 30, width: 30, marginRight: 10}}
                      source={IMAGES.pinpoint}
                      resizeMode={'contain'}
                    />
                    <View>
                      <Text style={Styles.small_label}>From</Text>
                      <TextInput
                      inputAccessoryViewID={'uniqueID'}
                        value={this.state.from}
                        onPress={() => {
                          this.props.navigation.navigate('GooglePlaces', {
                            updateMasterState: this.updateMasterState,
                            attrName: 'from',
                          });
                          this.setState({modalVisible: false});
                        }}
                        placeholder={'Select From'}
                        placeholderTextColor={COLOR.LIGHT_TEXT_COLOR}
                        onChangeText={(text) => this.setState({from: text})}
                        style={Styles.body_label}
                      />
                      <DoneButtonKeyboard />
                    </View>
                  </View>
                  <View
                    style={[
                      Styles.line_view,
                      {width: wp(90), marginVertical: 10},
                    ]}
                  />
                  <View style={styles.editView}>
                    <Image
                      style={{height: 30, width: 30, marginRight: 10}}
                      source={IMAGES.pinpoint}
                      resizeMode={'contain'}
                    />
                    <View>
                      <Text style={Styles.small_label}>To</Text>
                      <TextInput
                      inputAccessoryViewID={'uniqueID'}
                        onPress={() => {
                          this.props.navigation.navigate('GooglePlaces', {
                            updateMasterState: this.updateMasterState,
                            attrName: 'to',
                          });
                          this.setState({modalVisible: false});
                        }}
                        value={this.state.to}
                        onChangeText={(text) => this.setState({to: text})}
                        style={Styles.body_label}
                        placeholder={'Select From'}
                        placeholderTextColor={COLOR.LIGHT_TEXT_COLOR}
                      />
                    </View>
                  </View>

                  {this.state.tripDepart != null ? (
                    <View>
                      <View
                        style={[
                          Styles.line_view,
                          {width: wp(90), marginVertical: 10},
                        ]}
                      />
                      <View
                        style={{
                          flexDirection: 'row',
                          alignItems: 'center',
                          marginBottom: 0,
                        }}>
                        <Image
                          style={{height: 30, width: 30, marginRight: 10}}
                          source={IMAGES.departure_date}
                          resizeMode={'contain'}
                        />
                        <View>
                          <Text style={Styles.small_label}>Departure Date</Text>
                          <TouchableOpacity
                            onPress={() => {
                              this.setState({
                                selector: 0,
                                openCalendar: true,
                              });
                            }}>
                            <Text style={Styles.body_label}>
                              {this.state.checkInDate
                                ? this.state.checkInDate
                                : 'Select Dates'}
                            </Text>
                          </TouchableOpacity>
                        </View>
                      </View>
                    </View>
                  ) : null}
                  <View
                    style={[
                      Styles.line_view,
                      {width: wp(90), marginVertical: 10},
                    ]}
                  />
                  <View style={styles.editView}>
                    <Image
                      style={{height: 30, width: 30, marginRight: 10}}
                      source={IMAGES.pinpoint}
                      resizeMode={'contain'}
                    />
                    <View>
                      <Text style={Styles.small_label}>Days</Text>
                      <TextInput
                      inputAccessoryViewID={'uniqueID'}
                        value={this.state.days}
                        onChangeText={(text) => this.setState({days: text})}
                        style={Styles.body_label}
                        placeholder={'Days'}
                        placeholderTextColor={COLOR.LIGHT_TEXT_COLOR}
                      />
                    </View>
                  </View>
                </View>
                <View
                  style={{
                    width: wp(90),
                    marginTop: 20,
                    justifyContent: 'space-between',
                    flexDirection: 'row',
                    alignItems: 'center',
                  }}>
                  <TouchableOpacity
                    style={{
                      width: wp(42.5),
                      alignSelf: 'center',
                      height: 50,
                      alignItems: 'center',
                      justifyContent: 'center',
                      backgroundColor: COLOR.WHITE,
                      borderRadius: 10,
                      marginBottom: 20,
                      borderColor: COLOR.LIGHT_TEXT,
                      borderWidth: 1,
                    }}
                    onPress={() => {
                      this.setState({modalVisible: false}, (val) => {
                        this.props.navigation.goBack();
                      });
                    }}>
                    <Text
                      style={[
                        Styles.button_font,
                        {
                          color: COLOR.BLACK,
                          width: null,
                          textAlign: 'center',
                        },
                      ]}>
                      Cancel
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={{
                      width: wp(42.5),
                      alignSelf: 'center',
                      height: 50,
                      alignItems: 'center',
                      justifyContent: 'center',
                      backgroundColor: COLOR.GREEN,
                      borderRadius: 10,
                      marginBottom: 20,
                      borderColor: COLOR.LIGHT_TEXT,
                      borderWidth: 1,
                    }}
                    onPress={() => this.handleNavigation()}>
                    <Text
                      style={[
                        Styles.button_font,
                        {
                          color: COLOR.WHITE,
                          width: null,
                          textAlign: 'center',
                        },
                      ]}>
                      Continue
                    </Text>
                  </TouchableOpacity>
                </View>
              </KeyboardAvoidingView>
            </View>
          </Modal>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  modal: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
  },
  modalBody: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    backgroundColor: COLOR.WHITE,
    borderWidth: 1,
    width: wp(100),
    marginTop: 50,
    borderColor: COLOR.BORDER,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
  image: {
    height: 120,
    width: 120,
    alignSelf: 'center',
    marginVertical: 20,
  },
  headingLabel: {
    fontFamily: FONTS.FAMILY_BOLD,
    fontSize: FONTS.MEDIUM,
    color: COLOR.BLACK,
  },
  singleLineView: {
    marginVertical: 20,
    borderBottomColor: COLOR.GRAY,
    width: wp(100),
    alignSelf: 'center',
    borderBottomWidth: 1,
  },
  borderView: {
    borderRadius: 10,
    borderWidth: 3,
    width: wp(90),
    borderColor: COLOR.GRAY,
    alignSelf: 'center',
  },
  editView: {
    flexDirection: 'row',
    height: 50,
    alignItems: 'center',
    marginBottom: 0,
  },
});
