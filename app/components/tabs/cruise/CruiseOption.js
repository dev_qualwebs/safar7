'use strict';

import React from 'react';
import {View, Image, StyleSheet, Text, SafeAreaView} from 'react-native';
import {
  TouchableOpacity,
  TouchableWithoutFeedback,
  TextInput,
} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import COLOR from '../../../components/styles/Color';
import FONTS from '../../../components/styles/Fonts';
import IMAGES from '../../../components/styles/Images';
import Styles from '../../../components/styles/Styles';
import {connect} from 'react-redux';
import CruiseAction from '../../../redux/action/CruiseAction';
import SimpleToast from 'react-native-simple-toast';
import { DoneButtonKeyboard } from '../../commonView/CommonView';

class CruiseOption extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedIndex: 0,
      cabinData: [],
      featureData: [],
      supplierData: [],
      cabin: [],
      supplier: [],
      features: [],
      id: this.props.route.params.id,
      cruiseId: null,
      comment: '',
      budget_id: 1,
    };

    this.props.getCruiseCabin();
    this.props.getCruiseSupplier();
    this.props.getCruiseFeature();
  }

  updateMasterState = (attrName, value) => {
    this.setState({[attrName]: value});
  };

  callScheduleCruise() {
    const params = this.props.route.params.params;
    let data = JSON.stringify({
      tripId: this.state.id,
      from: params.from,
      to: params.to,
      depart: params.depart,
      days: params.days,
      cruise_budget_id: this.state.budget_id,
      cabin_types: this.state.cabin,
      suppliers: this.state.supplier,
      features: this.state.features,
      package_request_id: this.props.route.params.package_request_id,
    });

    if (this.state.cruiseId) {
      this.props.updateCruise(data, this.state.cruiseId);
    } else {
      this.props.scheduleCruise(data, this.state.id);
    }
  }

  componentDidMount() {
    if (this.props.route.params.cruiseId) {
      this.setState({
        cruiseId: this.props.route.params.cruiseId,
      });
      this.props.getSingleCruise(this.props.route.params.cruiseId);
    }
    if (this.props.cruiseCabinData.length) {
      this.setState({
        cabinData: this.props.cruiseCabinData,
      });
    }

    if (this.props.cruiseFeatureData.length) {
      this.setState({
        featureData: this.props.cruiseFeatureData,
      });
    }

    if (this.props.cruiseSupplierData.length) {
      this.setState({
        supplierData: this.props.cruiseSupplierData,
      });
    }
  }

  componentDidUpdate(prevProps) {
    if (prevProps.cruiseCabinData != this.props.cruiseCabinData) {
      this.setState({
        cabinData: this.props.cruiseCabinData,
      });
    }

    if (prevProps.cruiseFeatureData != this.props.cruiseFeatureData) {
      this.setState({
        featureData: this.props.cruiseFeatureData,
      });
    }

    if (prevProps.cruiseSupplierData != this.props.cruiseSupplierData) {
      this.setState({
        supplierData: this.props.cruiseSupplierData,
      });
    }

    if (
      JSON.stringify(this.props.singleCruise) !=
      JSON.stringify(prevProps.singleCruise)
    ) {
      let cruisePreference = this.props.singleCruise.cruise_preference;
      this.setState({
        cabin: cruisePreference.cruise_cabin_type.map((value) => value.id),
        supplier: cruisePreference.cruise_supplier.map((value) => value.id),
        features: cruisePreference.features.map((value) => value.id),
      });
    }
  }

  componentWillUnmount() {
    this.props.clearSingleCruise();
  }

  selectTransport = () => {
    // this.setState({selectedIndex: item.id});
  };

  selectionView = (prop) => {
    return (
      <TouchableWithoutFeedback style={{height: 130}} onPress={prop.onPress}>
        <View
          style={{
            flex: 1,
            borderRadius: 10,
            borderWidth: 1,
            height: 130,
            width: wp(28),
            margin: 5,
            borderColor: prop.hasData ? '#05778F' : COLOR.GRAY,
            alignSelf: 'center',
            alignItems: 'center',
            justifyContent: 'center',
            padding: 10,
            backgroundColor : prop.hasData ? COLOR.GREEN_OPACITY : COLOR.WHITE
          }}>
          <View
            style={{
              height: 60,
              width: 60,
              marginVertical: 10,
              backgroundColor: prop.hasData ? COLOR.WHITE : prop.bg,
              borderRadius: 5,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Image
              resizeMode={'contain'}
              source={prop.image}
              style={{
                height: 30,
                width: 30,
              }}
            />
          </View>
          <Text style={{marginBottom: 10, textAlign: 'center'}}>
            {prop.name}
          </Text>
        </View>
      </TouchableWithoutFeedback>
    );
  };

  handleNavigation = () => {
    let params = this.props.route.params;
    const screens = params.screens;
    const index = params.index;
    if (screens && screens[index + 1]) {
      this.props.navigation.replace(screens[index + 1], {
        screens: screens,
        index: index + 1,
        id: params.id,
        depart: params.depart,
        return: params.return,
      });
    } else if (this.state.isPackage) {
      this.props.navigation.goBack();
    } else {
      this.props.navigation.navigate('HomeStack');
    }
  };

  handleValidation = () => {
    if (this.state.cabin.length == 0) {
      SimpleToast.show('Select Cabin type');
    } else if (this.state.features.length == 0) {
      SimpleToast.show('Select Features');
    } else if (this.state.supplier.length == 0) {
      SimpleToast.show('Select Supplier');
    } else {
      this.setState({modalVisible: false}, (val) => {
        this.callScheduleCruise();
        this.handleNavigation();
      });
    }
  };

  render() {
    return (
      <SafeAreaView style={[Styles.container]}>
        <View>
          <View style={style.topBar}>
            <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
              <Image
                style={{height: 15, width: 15}}
                source={IMAGES.back_arrow}
              />
            </TouchableOpacity>
            <Text
              style={[
                Styles.subheading_label,
                {width: null, fontSize: 20, fontFamily: FONTS.FAMILY_BOLD},
              ]}>
              Cruise Options
            </Text>
            <TouchableOpacity>
              <Image
                style={{height: 20, width: 20}}
                source={IMAGES.filter_image}
              />
            </TouchableOpacity>
          </View>
          <View
            style={{
              flexDirection: 'row',
              width: wp(90),
              alignSelf: 'center',
              marginTop: 20,
              justifyContent: 'space-between',
            }}>
            <this.selectionView
              name={'Cabin Type'}
              image={IMAGES.cabin_type}
              bg={'#F0DEDC'}
              hasData={this.state.cabin.length > 0 ? true : false}
              onPress={() => {
                this.props.navigation.navigate('CabinType', {
                  updateMasterState: this.updateMasterState,
                  cabinData: this.state.cabinData,
                  selectedItem: this.state.cabin,
                });
              }}
            />
            <this.selectionView
              name={'Features'}
              image={IMAGES.features}
              bg={'#05778F20'}
              hasData={this.state.features.length > 0 ? true : false}
              onPress={() => {
                this.props.navigation.navigate('CruiseFeatures', {
                  updateMasterState: this.updateMasterState,
                  featureData: this.state.featureData,
                  selectedItem: this.state.features,
                });
              }}
            />
            <this.selectionView
              name={'Cruise\nSupplier'}
              image={IMAGES.home_yellow}
              hasData={this.state.supplier.length > 0 ? true : false}
              bg={'#80800020'}
              onPress={() => {
                this.props.navigation.navigate('CruiseSupplier', {
                  updateMasterState: this.updateMasterState,
                  supplierData: this.state.supplierData,
                  selectedItem: this.state.supplier,
                });
              }}
            />
          </View>
        </View>

        <View style={[Styles.line_view, {marginVertical: 20}]} />
        <Text style={[Styles.subheading_label, {fontSize: FONTS.REGULAR}]}>
          Budget
        </Text>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            width: wp(90),
            alignSelf: 'center',
            height: 60,
            marginVertical: 5,
          }}>
          <TouchableOpacity onPress={() => this.setState({budget_id: 1})}>
            <Text
              style={[
                Styles.subheading_label,
                {
                  color: this.state.budget_id == 1 ? COLOR.GREEN : COLOR.GOLD,
                  textAlign: 'center',
                  width: null,
                  fontFamily: FONTS.FAMILY_EXTRA_BOLD,
                },
              ]}>
              $
            </Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => this.setState({budget_id: 2})}>
            <Text
              style={[
                Styles.subheading_label,
                {
                  color: this.state.budget_id == 2 ? COLOR.GREEN : COLOR.GOLD,
                  textAlign: 'center',
                  width: null,
                  fontFamily: FONTS.FAMILY_EXTRA_BOLD,
                },
              ]}>
              $$
            </Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => this.setState({budget_id: 3})}>
            <Text
              style={[
                Styles.subheading_label,
                {
                  color: this.state.budget_id == 3 ? COLOR.GREEN : COLOR.GOLD,
                  textAlign: 'center',
                  width: null,
                  fontFamily: FONTS.FAMILY_EXTRA_BOLD,
                },
              ]}>
              $$$
            </Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => this.setState({budget_id: 4})}>
            <Text
              style={[
                Styles.subheading_label,
                {
                  color: this.state.budget_id == 4 ? COLOR.GREEN : COLOR.GOLD,
                  textAlign: 'center',
                  width: null,
                  fontFamily: FONTS.FAMILY_EXTRA_BOLD,
                },
              ]}>
              $$$$
            </Text>
          </TouchableOpacity>
        </View>
        <View style={[Styles.line_view, {marginVertical: 20}]} />
        <Text style={[Styles.subheading_label, {fontSize: FONTS.REGULAR}]}>
          Comments
        </Text>
        <TextInput
        inputAccessoryViewID={'uniqueID'}
          placeholder={'Write your comment'}
          placeholderTextColor={COLOR.LIGHT_TEXT_COLOR}
          style={[
            {
              height: 120,
              paddingHorizontal: 20,
              paddingVertical: 20,
              width: wp(90),
              alignSelf: 'center',
              borderRadius: 10,
              borderWidth: 1,
              borderColor: COLOR.GRAY,
              marginTop: 10,
            },
          ]}
          multiline={true}
          value={this.state.comment}
          onChangeText={(text) => this.setState({comment: text})}
        />
        <DoneButtonKeyboard />
        <View style={{bottom: 20, position: 'absolute', alignSelf: 'center'}}>
          <TouchableOpacity
            style={{
              width: wp(90),
              alignSelf: 'center',
              height: 50,
              alignItems: 'center',
              justifyContent: 'center',

              borderRadius: 10,
            }}
            onPress={() => {
              this.setState({cabin: []});
              this.setState({features: []});
              this.setState({supplier: []});
              this.setState({comment: ''});
              this.setState({budget_id: 1});
            }}>
            <Text
              style={[
                Styles.small_label,
                {
                  color: COLOR.LIGHT_TEXT,
                  width: null,
                  textAlign: 'right',
                },
              ]}>
              Clear all options
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              width: wp(90),
              alignSelf: 'center',
              height: 50,
              alignItems: 'center',
              justifyContent: 'center',
              backgroundColor: COLOR.GREEN,
              borderRadius: 10,
              marginTop: 0,
            }}
            onPress={() => {
              this.handleValidation();
            }}>
            <Text
              style={[
                Styles.button_font,
                {
                  color: COLOR.WHITE,
                  width: null,
                  textAlign: 'center',
                },
              ]}>
              Apply
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              width: wp(90),
              alignSelf: 'center',
              height: 50,
              alignItems: 'center',
              justifyContent: 'center',

              borderRadius: 10,
            }}>
            <Text
              style={[
                Styles.small_label,
                {
                  color: COLOR.BLACK,
                  width: null,
                  textAlign: 'right',
                },
              ]}>
              Skip cruise options
            </Text>
          </TouchableOpacity>
        </View>
      </SafeAreaView>
    );
  }
}

const style = StyleSheet.create({
  topBar: {
    height: 50,
    backgroundColor: COLOR.WHITE,
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: wp(90),
    alignSelf: 'center',
    marginVertical: 0,
    alignItems: 'center',
  },
});

const mapStateToProps = (state, ownProps) => {
  return {
    cruiseCabinData: state.cruiseReducer.cruiseCabinTypeData,
    cruiseFeatureData: state.cruiseReducer.creuiseFeatureData,
    cruiseSupplierData: state.cruiseReducer.cruiseSupplierData,
    singleCruise: state.cruiseReducer.singleCruise,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getCruiseCabin: (val) => {
      dispatch(CruiseAction.getCruiseCabin(val));
    },
    getCruiseFeature: (val) => {
      dispatch(CruiseAction.getCruiseFeature(val));
    },
    getCruiseSupplier: (val) => {
      dispatch(CruiseAction.getCruiseSupplier(val));
    },
    getSingleCruise: (id) => {
      dispatch(CruiseAction.getSingleCruise(id));
    },
    scheduleCruise: (data, id) => {
      dispatch(CruiseAction.scheduleCruise(data, id));
    },
    updateCruise: (data, id) => {
      dispatch(CruiseAction.updateCruise(data, id));
    },
    clearSingleCruise: () => {
      dispatch(CruiseAction.clearSingleCruise());
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(CruiseOption);
