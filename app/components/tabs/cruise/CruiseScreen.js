import {View, Image, Text, FlatList} from 'react-native';
import React from 'react';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import COLOR from '../../styles/Color';
import Styles from '../../styles/Styles';
import {TouchableOpacity} from 'react-native';
import IMAGES from '../../styles/Images';
import FONTS from '../../styles/Fonts';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import SimpleToast from 'react-native-simple-toast';
import TripDateView from '../../commonView/TripDateView';
import {TextInput} from 'react-native-gesture-handler';
import SelectDate from '../calendar/SelectDate';
import GooglePlacesTextInput from '../../commonView/GooglePlacesTextInput';
import {StyleSheet} from 'react-native';
import {request} from 'react-native-permissions';
import {DoneButtonKeyboard} from '../../commonView/CommonView';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview';
class CruiseScreen extends React.Component {
  constructor(props) {
    super(props);
    let route = this.props.route.params;
    this.state = {
      trainData: [],
      days: '',
      to: '',
      from: '',
      depart: '',
      isPackage: route.isPackage ? route.isPackage : false,
      tripDepart: route.depart,
      tripReturn: route.return,
      openCalendar: false,
      selector: 0,
      checkInDate: route.depart,
      departFlexibility: null,
      returnFlexibility: null,
      package_request_id: route.package_request_id,
      cruiseId: null,
    };
  }

  componentDidMount() {
    if (this.props.route.params.data) {
      const data = this.props.route.params.data;
      console.log(data);
      this.setState({
        days: `${data.days ? data.days : ''}`,
        to: data.to,
        from: data.from,
        checkInDate: data.depart,
        cruiseId: data.id,
      });
    }

    if (this.props.route.params.depart) {
      let data = this.props.route.params;
      this.setState({
        tripDepart: data.depart,
        tripReturn: data.return,
        departFlexibility: data.departFlexibility,
        returnFlexibility: data.returnFlexibility,
      });
    }
  }

  updateMasterState = (attrName, value) => {
    this.setState({[attrName]: value});
  };

  handleNavigation = () => {
    if (this.state.from == '') {
      SimpleToast.show('Enter From location');
    } else if (
      this.state.checkInDate == '' ||
      (this.state.checkInDate == null && this.state.tripDepart)
    ) {
      SimpleToast.show('Select departure date');
    } else if (this.state.to == '') {
      SimpleToast.show('Enter to location');
    } else if (this.state.days == '') {
      SimpleToast.show('Enter days count');
    } else {
      this.setState({modalVisible: false});
      let params = {
        from: this.state.from,
        to: this.state.to,
        depart: this.state.checkInDate,
        days: this.state.days,
      };
      const screens = this.props.route.params.screens;
      const index = this.props.route.params.index;

      this.props.navigation.replace('CruiseOption', {
        params: params,
        id: this.props.route.params.id,
        package_request_id: this.state.package_request_id,
        screens: screens,
        index: index,
        depart: this.state.tripDepart,
        return: this.state.tripReturn,
        cruiseId: this.state.cruiseId ? this.state.cruiseId : null,
      });
    }
  };

  render() {
    const id = this.props.route.params.id;
    return (
        <View style={[Styles.container, {backgroundColor: COLOR.BACKGROUND}]}>
          <KeyboardAwareScrollView
        bounces={false}
        showsHorizontalScrollIndicator={false}
        showsVerticalScrollIndicator={false}
        keyboardShouldPersistTaps={'always'}
        extraScrollHeight={180}
        style={[Styles.container, {backgroundColor: COLOR.BACKGROUND}]}>
          <View
            style={{
              height: 220,
              backgroundColor: COLOR.GREEN,
              borderBottomRightRadius: 15,
              borderBottomLeftRadius: 15,
            }}>
            <View
              style={[
                Styles.mainView,
                {
                  height: 45,
                  marginTop: getStatusBarHeight(true),
                  justifyContent: 'center',
                  alignSelf: 'center',
                  alignItems: 'center',
                  flexDirection: 'row',
                },
              ]}>
              <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                <Image
                  style={{height: 15, width: 15, tintColor: COLOR.WHITE}}
                  source={IMAGES.back_arrow}
                />
              </TouchableOpacity>
              <TouchableOpacity>
                <View>
                  <Text
                    style={[
                      Styles.heading_label,
                      {
                        textAlign: 'center',
                        color: COLOR.WHITE,
                        marginVertical: 0,
                        fontFamily: FONTS.FAMILY_BOLD,
                      },
                    ]}>
                    Cruise
                  </Text>
                  <Image
                    style={{
                      width: 20,
                      height: 20,
                      right: 0,
                      position: 'absolute',
                    }}
                  />
                </View>
              </TouchableOpacity>
            </View>
            <TripDateView
              departDate={this.state.tripDepart}
              returnDate={this.state.tripReturn}
              departFlexibility={this.state.departFlexibility}
              returnFlexibility={this.state.returnFlexibility}
            />
          </View>

          <View style={styles.cardView}>
            <View>
              <View
                style={{
                  width: wp(85),
                  alignSelf: 'center',
                }}>
                <View style={[styles.textinput, {zIndex: 1010}]}>
                  <Image
                    style={{height: 25, width: 25, marginRight: 20}}
                    source={IMAGES.pinpoint}
                    resizeMode={'contain'}
                  />
                  <View style={{width: widthPercentageToDP(75)}}>
                    <Text
                      style={[
                        Styles.small_label,
                        {width: null, alignSelf: 'flex-start', padding: 0},
                      ]}>
                      Depart Port
                    </Text>
                    <GooglePlacesTextInput
                      value={this.state.from}
                      onValueChange={(text) => this.setState({from: text})}
                      placeholder={'Enter depart port name'}
                    />
                  </View>
                </View>
                {this.state.tripDepart ? (
                  <>
                    <View
                      style={[
                        Styles.line_view,
                        {width: wp(80), marginVertical: 15},
                      ]}
                    />
                    <View style={[styles.textinput]}>
                      <Image
                        style={{height: 25, width: 25, marginRight: 20}}
                        source={require('../../styles/assets/calendar_icon.png')}
                        resizeMode={'contain'}
                      />
                      <View>
                        <Text style={Styles.small_label}>Departure Date</Text>
                        <TouchableOpacity
                          onPress={() =>
                            this.setState({
                              openCalendar: true,
                            })
                          }>
                          <Text
                            style={{
                              fontSize: FONTS.MEDIUM,
                              fontFamily: FONTS.FAMILY_SEMIBOLD,
                            }}>
                            {this.state.checkInDate
                              ? this.state.checkInDate
                              : 'Enter departure date'}
                          </Text>
                        </TouchableOpacity>
                      </View>
                    </View>
                  </>
                ) : null}
                <View
                  style={[
                    Styles.line_view,
                    {width: wp(80), marginVertical: 15},
                  ]}
                />
                <View style={[styles.textinput, {zIndex: 990}]}>
                  <Image
                    style={{height: 25, width: 25, marginRight: 20}}
                    source={IMAGES.dropoff}
                    resizeMode={'contain'}
                  />
                  <View style={{width: widthPercentageToDP(75)}}>
                    <Text
                      style={[
                        Styles.small_label,
                        {width: null, alignSelf: 'flex-start', padding: 0},
                      ]}>
                      Destination
                    </Text>
                    <GooglePlacesTextInput
                      value={this.state.to}
                      onValueChange={(text) => this.setState({to: text})}
                      placeholder={'Enter destination name'}
                    />
                  </View>
                </View>
                <View
                  style={[
                    Styles.line_view,
                    {width: wp(80), marginVertical: 15},
                  ]}
                />
                <View style={styles.textinput}>
                  <Image
                    style={{height: 25, width: 25, marginRight: 20}}
                    source={require('../../styles/assets/calendar_icon.png')}
                    resizeMode={'contain'}
                  />
                  <View>
                    <Text style={Styles.small_label}>Length of the Cruise</Text>
                    <TextInput
                      inputAccessoryViewID={'uniqueID'}
                      placeholder={'Days'}
                      placeholderTextColor={COLOR.LIGHT_TEXT_COLOR}
                      keyboardType={'number-pad'}
                      returnKeyType={'done'}
                      value={this.state.days}
                      onChangeText={(text) => {
                        this.setState({days: text});
                      }}
                      style={{
                        fontSize: FONTS.MEDIUM,
                        fontFamily: FONTS.FAMILY_SEMIBOLD,
                      }}
                    />
                    <DoneButtonKeyboard />
                  </View>
                </View>
              </View>
            </View>
          </View>

          <TouchableOpacity
            style={{
              width: wp(90),
              alignSelf: 'center',
              height: 50,
              alignItems: 'center',
              justifyContent: 'center',
              backgroundColor: COLOR.GREEN,
              borderRadius: 10,
              marginBottom: 40,
            }}
            onPress={() => {
              this.handleNavigation();
            }}>
            <Text
              style={[
                Styles.button_font,
                {
                  color: COLOR.WHITE,
                  width: null,
                  textAlign: 'center',
                },
              ]}>
              Next
            </Text>
          </TouchableOpacity>
          {this.state.openCalendar ? (
            <SelectDate
              selector={this.state.selector}
              updateMasterState={this.updateMasterState}
              props={this.props.props}
              allowRangeSelection={false}
              minDate={this.state.tripDepart}
              maxDate={this.state.tripReturn}
              modal={true}
            />
          ) : null}
      </KeyboardAwareScrollView>
        </View>
    );
  }

  CruiseObject = (props) => {
    return (
      <View
        style={{
          width: wp(90),
          alignSelf: 'center',
          borderRadius: 10,
          shadowColor: COLOR.VOILET,
          elevation: 3,
          shadowOffset: {width: 2, height: 2},
          shadowOpacity: 0.2,
          backgroundColor: COLOR.WHITE,
          padding: 20,
          marginBottom: 10,
        }}>
        <View>
          <View
            style={{
              width: wp(85),
              alignSelf: 'center',
            }}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
              }}>
              <Image
                style={{height: 25, width: 25, marginRight: 20}}
                source={IMAGES.pinpoint}
                resizeMode={'contain'}
              />
              <View>
                <Text style={Styles.small_label}>Depart Port</Text>
                <Text style={Styles.body_label}>{props.from}</Text>
              </View>
            </View>
            <View
              style={[Styles.line_view, {width: wp(80), marginVertical: 15}]}
            />
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
              }}>
              <Image
                style={{height: 25, width: 25, marginRight: 20}}
                source={IMAGES.calendar_small}
                resizeMode={'contain'}
              />
              <View>
                <Text style={Styles.small_label}>Departure Date</Text>

                <Text style={Styles.body_label}>{props.departDate}</Text>
              </View>
            </View>
            <View
              style={[Styles.line_view, {width: wp(80), marginVertical: 15}]}
            />
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
              }}>
              <Image
                style={{height: 25, width: 25, marginRight: 20}}
                source={IMAGES.dropoff}
                resizeMode={'contain'}
              />
              <View>
                <Text style={Styles.small_label}>Destination</Text>
                <Text style={Styles.body_label}>{props.to}</Text>
              </View>
            </View>
            <View
              style={[Styles.line_view, {width: wp(80), marginVertical: 15}]}
            />
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
              }}>
              <Image
                style={{height: 25, width: 25, marginRight: 20}}
                source={IMAGES.calendar_small}
                resizeMode={'contain'}
              />
              <View>
                <Text style={Styles.small_label}>Length of the Cruise</Text>
                <Text style={Styles.body_label}>{props.days}</Text>
              </View>
            </View>
          </View>
        </View>
      </View>
    );
  };
}

let styles = StyleSheet.create({
  textinput: {
    flexDirection: 'row',
    alignItems: 'center',
    height: 50,
  },
  cardView: {
    width: wp(90),
    alignSelf: 'center',
    borderRadius: 10,
    shadowColor: COLOR.VOILET,
    elevation: 3,
    shadowOffset: {width: 2, height: 2},
    shadowOpacity: 0.2,
    backgroundColor: COLOR.WHITE,
    paddingHorizontal: 20,
    paddingVertical: 10,
    marginBottom: 10,
    marginTop: -50,
    zIndex: 800,
  },
});

export default CruiseScreen;
