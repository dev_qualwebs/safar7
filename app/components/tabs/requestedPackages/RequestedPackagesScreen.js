import React, {Component} from 'react';
import {SafeAreaView, Alert, TouchableOpacity, View, Image} from 'react-native';
import TopTabBar from '../../tabs/home/trip/TabBarView';
import Styles from '../../styles/Styles';
import {T_REQUEST_TRIP_TAB} from '../../../helper/Constants';
import NavigationBar from '../../commonView/NavigationBar';
import IMAGES from '../../styles/Images';
import COLOR from '../../styles/Color';
import { HelpTrip } from '../home/trip/HelpTrip';

export default class RequestedPackagesScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      index: 0,
      modalVisible: false,
    };

    this.hideModal = this.hideModal.bind(this);
  }

  hideModal = () => {
    // Alert.alert('working');
    // this.setState({modalVisible: !this.state.modalVisible});
  };

  render() {
    return (
      <SafeAreaView style={Styles.container}>
        <NavigationBar
          prop={this.props}
          right_image={IMAGES.notification_bell}
          navHeight={45}
          isMainHeading={true}
          name={'Requests'}
        />
        <TopTabBar
          currentTab={T_REQUEST_TRIP_TAB}
          props={this.props}
          packages={true}
        />
        <TouchableOpacity
          style={{
            height: 44,
            width: 44,
            bottom: 8,
            left: 20,
            position: 'absolute',
            borderRadius: 22,
          }}
          onPress={() => this.props.navigation.navigate('HelpTrip')}
          >
          <View
            style={{
              height: 44,
              width: 44,
              borderRadius: 22,
              shadowColor: COLOR.VOILET,
              shadowOpacity: 0.2,
              shadowOffset: {width: 2, height: 2},
              backgroundColor: COLOR.WHITE,
              justifyContent: 'center',
              alignItems: 'center',
              elevation: 3,
            }}>
            <Image source={IMAGES.help} style={{height: 40, width: 40}} />
          </View>
        </TouchableOpacity>
      </SafeAreaView>
    );
  }
}
