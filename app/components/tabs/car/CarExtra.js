import React from 'react';
import {Modal, Text, View, StyleSheet, Image} from 'react-native';
import {ScrollView, TextInput, TouchableOpacity} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import COLOR from '../../../components/styles/Color';
import FONTS from '../../../components/styles/Fonts';
import IMAGES from '../../../components/styles/Images';
import Styles from '../../../components/styles/Styles';
import NavigationBar from '../../commonView/NavigationBar';

export default class CarExtra extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: true,
      child_seat: 0,
      baby_seat: 0,
      booster_seat: 0,
      addition_driver: 0,
    };
  }

  componentWillMount() {
    if (this.props.route.params.selected) {
      let selected = this.props.route.params.selected;
      this.setState({
        child_seat: selected.child_seat,
        baby_seat: selected.baby_seat,
        booster_seat: selected.booster_seat,
        addition_driver: selected.additional_driver,
      });
    }
  }

  increaseValue(val, selection) {
    const incresed = val + 1;
    console.log(selection);
    switch (selection) {
      case 1:
        this.setState({baby_seat: incresed});
        break;
      case 2:
        this.setState({child_seat: incresed});
        break;
      case 3:
        this.setState({booster_seat: incresed});
        break;
      case 4:
        this.setState({addition_driver: incresed});
        break;
    }
  }

  decreaseValue = (val, selection) => {
    if(val > 0){
    const incresed = val - 1;
    switch (selection) {
      case 1:
        this.setState({baby_seat: incresed});
        break;
      case 2:
        this.setState({child_seat: incresed});
        break;
      case 3:
        this.setState({booster_seat: incresed});
        break;
      case 4:
        this.setState({addition_driver: incresed});
        break;
    }
    }
  };

  CommonView = (props) => (
    <View
      style={{
        flexDirection: 'row',
        width: wp(90),
        justifyContent: 'space-between',
      }}>
      <View
        style={{
          flex: 1,
          alignSelf: 'center',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Text
          style={[
            Styles.body_label,
            {width: null, color: COLOR.BLACK, alignSelf: 'flex-start'},
          ]}>
          {props.category}
        </Text>
        {props.ageGroup != '' && (
          <Text
            style={[
              Styles.small_label,
              {width: null, color: COLOR.BLACK, alignSelf: 'flex-start'},
            ]}>
            {props.ageGroup}
          </Text>
        )}
      </View>
      <View
        style={{
          flex: 1,
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'flex-end',
        }}>
        <TouchableOpacity
          onPress={() => this.decreaseValue(props.value, props.selector)}
          style={{
            height: 25,
            width: 25,
            borderRadius: 15,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Image
            style={{height: 30, width: 30,opacity: props.value != 0 ? 1 : 0.4}}
            resizeMode={'contain'}
            source={IMAGES.minus_image}
          />
        </TouchableOpacity>
        <Text
          style={[
            Styles.button_font,
            {width: 40, color: COLOR.BLACK, textAlign: 'center'},
          ]}>
          {props.value}
        </Text>
        <TouchableOpacity
          onPress={() => this.increaseValue(props.value, props.selector)}
          style={{
            height: 30,
            width: 30,
            borderWidth: 1,
            borderRadius: 15,
            borderColor: COLOR.GREEN,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Image style={{height: 20, width: 20}} source={IMAGES.plus_image} />
        </TouchableOpacity>
      </View>
    </View>
  );

  SingleLineView = () => (
    <View
      style={{
        borderBottomColor: COLOR.GRAY,
        marginVertical: 10,
        borderBottomWidth: 1,
        width: wp(90),
      }}
    />
  );

  render() {
    return (
      <View style={Styles.container}>
        <Modal
          animationType={'slide'}
          transparent={true}
          style={{backgroundColor: COLOR.BLACK}}
          visible={this.state.modalVisible}
          onRequestClose={() => {
            console.log('Modal has been closed.');
          }}>
          <View style={styles.modal}>
            <View style={styles.modalBody}>
              <NavigationBar
                right_image={IMAGES.filter_image}
                prop={this.props}
                height={40}
                name={'Extras'}
              />
              <View style={{flex: 1}}>
                <Text
                  style={[
                    Styles.subheading_label,
                    {marginVertical: 20, fontSize: FONTS.REGULAR},
                  ]}>
                  Child Seat
                </Text>
                <this.SingleLineView />
                <this.CommonView
                  category={'Baby Seat'}
                  ageGroup={'Age 12+ / 13kg'}
                  value={this.state.baby_seat}
                  selector={1}
                />
                <this.SingleLineView />
                <this.CommonView
                  category={'Child Seat'}
                  ageGroup={'Age 1 - 4 (9-18kg)'}
                  value={this.state.child_seat}
                  selector={2}
                />
                <this.SingleLineView />
                <this.CommonView
                  category={'Booster Seat'}
                  ageGroup={'Age 4 - 12 (15-36kg)'}
                  value={this.state.booster_seat}
                  selector={3}
                />
                <this.SingleLineView />
                <View style={{marginVertical: 50}}>
                  <this.CommonView
                    category={'Additional Driver'}
                    ageGroup={''}
                    value={this.state.addition_driver}
                    selector={4}
                  />
                  <this.SingleLineView />
                  <Text style={[Styles.small_label, {color: COLOR.RED}]}>
                    Hint: “Car with a Driver” can be choosen if you select trip
                    plan, without rent a car.
                  </Text>
                </View>
              </View>
              <TouchableOpacity
                onPress={() => {
                  this.setState({modalVisible: false}, (val) => {
                    let data = {
                      child_seat: this.state.child_seat,
                      baby_seat: this.state.baby_seat,
                      booster_seat: this.state.booster_seat,
                      additional_driver: this.state.addition_driver,
                    };
                    this.props.route.params.updateMasterState('extras', data);
                    this.props.navigation.goBack();
                  });
                }}>
                <View
                  style={{
                    width: wp(90),
                    alignItems: 'center',
                    justifyContent: 'center',
                    borderColor: COLOR.WHITE,
                    borderWidth: 1,
                    height: 50,
                    backgroundColor: COLOR.GREEN,
                    marginVertical: 20,
                    borderRadius: 10,
                  }}>
                  <Text style={[Styles.button_font, {color: COLOR.WHITE}]}>
                    Apply
                  </Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  modal: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
  },
  modalBody: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    backgroundColor: COLOR.WHITE,
    borderWidth: 1,
    width: wp(100),
    borderColor: COLOR.BORDER,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    marginTop: 50,
    flex: 1,
  },
  image: {
    height: 120,
    width: 120,
    alignSelf: 'center',
    marginVertical: 20,
  },
  headingLabel: {
    fontFamily: FONTS.FAMILY_BOLD,
    fontSize: FONTS.MEDIUM,
    color: COLOR.BLACK,
  },
  singleLineView: {
    marginVertical: 20,
    borderBottomColor: COLOR.GRAY,
    width: wp(100),
    alignSelf: 'center',
    borderBottomWidth: 1,
  },
  borderView: {
    borderRadius: 10,
    borderWidth: 3,
    width: wp(90),
    borderColor: COLOR.GRAY,
    alignSelf: 'center',
  },
});
