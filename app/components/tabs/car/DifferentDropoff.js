import React from 'react';
import {SafeAreaView} from 'react-native';
import {Modal, Text, View, StyleSheet, Image, Button} from 'react-native';
import {TextInput, TouchableOpacity} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import SimpleToast from 'react-native-simple-toast';
import COLOR from '../../../components/styles/Color';
import FONTS from '../../../components/styles/Fonts';
import IMAGES from '../../../components/styles/Images';
import Styles from '../../../components/styles/Styles';
import { DoneButtonKeyboard } from '../../commonView/CommonView';
import GooglePlacesTextInput from '../../commonView/GooglePlacesTextInput';
import {
  getEndDateTimestamp,
  getStartDateTimestamp,
} from '../../commonView/Helpers';
import SelectDate from '../calendar/SelectDate';

export default class DifferentDropoff extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: true,
      selectedIndex: null,
      pickup_location: '',
      drop_off_location: '',
      is_drop_off_same: 0,
      checkInDate: null,
      checkOutDate: null,
      openCalendar: false,
      selector: 0,
      id: this.props.props.route.params.id,
      data: null,
      carId: null,
      days: '',
      minDate: getStartDateTimestamp(this.props.props.route.params.depart),
      maxDate: getEndDateTimestamp(this.props.props.route.params.return),
      tripDepart: this.props.props.route.params.depart,
      tripReturn: this.props.props.route.params.return,
      package_request_id: this.props.props.route.params.package_request_id,
    };
  }

  componentDidMount() {
    if (this.props.props.route.params.data) {
      let data = this.props.props.route.params.data;
      this.setState({
        data: data,
        carId: data.id,
        days: data.days ? `${data.days}` : '',
        checkInDate: data.pickup_date,
        checkOutDate: data.drop_off_date,
        pickup_location: data.pickup_location,
        drop_off_location: data.drop_off_location,
      });
    }
  }

  updateMasterState = (attrName, value) => {
    this.setState({[attrName]: value});
  };

  render() {
    const {selectedIndex} = this.state;
    return (
      <SafeAreaView style={{flex: 1}}>
        <View
          style={[
            Styles.shadow_view,
            {
              width: wp(90),
              marginVertical: 0,
              alignSelf: 'center',
              backgroundColor: COLOR.WHITE,
              borderBottomLeftRadius: 10,
              borderBottomRightRadius: 10,
              padding: 20,
            },
          ]}>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              height: 50,
              zIndex: 1000,
            }}>
            <Image
              style={{height: 25, width: 25, marginRight: 20}}
              source={IMAGES.pinpoint}
              resizeMode={'contain'}
            />
            <View>
              <Text style={[Styles.small_label]}>Pickup Location</Text>
              <GooglePlacesTextInput
                value={this.state.pickup_location}
                onValueChange={(text) => this.setState({pickup_location: text})}
              />
            </View>
          </View>
          <View
            style={[Styles.line_view, {width: wp(80), marginVertical: 15}]}
          />
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              height: 50,
              zIndex: 990,
            }}>
            <Image
              style={{height: 25, width: 25, marginRight: 20}}
              source={IMAGES.pinpoint}
              resizeMode={'contain'}
            />
            <View>
              <Text style={[Styles.small_label]}>Drop-off Location</Text>
              <GooglePlacesTextInput
                value={this.state.drop_off_location}
                onValueChange={(text) =>
                  this.setState({
                    drop_off_location: text,
                  })
                }
              />
            </View>
          </View>
          <View
            style={[Styles.line_view, {width: wp(80), marginVertical: 15}]}
          />
          {this.state.tripDepart ? (
            <View>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  height: 50,
                }}>
                <Image
                  style={{height: 25, width: 25, marginRight: 20}}
                  source={IMAGES.pinpoint}
                  resizeMode={'contain'}
                />
                <View>
                  <Text style={Styles.small_label}>Pickup Date</Text>
                  <TouchableOpacity
                    onPress={() => {
                      this.setState({
                        openCalendar: true,
                        attrName: 'checkInDate',
                        selector: 0,
                      });
                    }}>
                    <Text style={Styles.body_label}>
                      {this.state.checkInDate
                        ? this.state.checkInDate
                        : 'Select Pickup Date'}{' '}
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>
              <View
                style={[Styles.line_view, {width: wp(80), marginVertical: 15}]}
              />
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  height: 50,
                }}>
                <Image
                  style={{height: 25, width: 25, marginRight: 20}}
                  source={IMAGES.calendar_small}
                  resizeMode={'contain'}
                />
                <View>
                  <Text style={Styles.small_label}>Drop-off Date</Text>
                  <TouchableOpacity
                    onPress={() => {
                      this.setState({
                        openCalendar: true,
                        attrName: 'checkOutDate',
                        selector: 1,
                      });
                    }}>
                    <Text style={Styles.body_label}>
                      {this.state.checkOutDate
                        ? this.state.checkOutDate
                        : 'Select Drop-off Date'}{' '}
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          ) : (
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
              }}>
              <Image
                style={{height: 25, width: 25, marginRight: 20}}
                source={IMAGES.pinpoint}
                resizeMode={'contain'}
              />
              <View>
                <Text style={[Styles.small_label]}>Number of days</Text>
                <TextInput
                inputAccessoryViewID={'uniqueID'}
                  style={Styles.body_label}
                  placeholderTextColor={COLOR.LIGHT_TEXT}
                  value={this.state.days}
                  keyboardType={'numeric'}
                  returnKeyType={'done'}
                  blurOnSubmit={true}
                  placeholder={'Enter Number of Days'}
                  onChangeText={(text) => this.setState({days: text})}
                />
                <DoneButtonKeyboard />
              </View>
            </View>
          )}
        </View>

        <TouchableOpacity
          style={{
            width: wp(90),
            alignSelf: 'center',
            height: 50,
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: COLOR.GREEN,
            borderRadius: 10,
            bottom: 20,
            position: 'absolute',
          }}
          onPress={() => {
            if(this.state.pickup_location == ''){
              SimpleToast.show('Enter pickup location');
              return;
            } else if(this.state.drop_off_location == ''){
              SimpleToast.show('Enter drop-off location');
              return;
            } else if(this.state.checkInDate == '' || this.state.checkInDate == null && this.state.tripDepart){
              SimpleToast.show('Select pickup date');
              return;
            } else if(this.state.checkOutDate == '' || this.state.checkOutDate == null && this.state.tripDepart){
              SimpleToast.show('Select drop-off date');
              return;
            }  else if(!this.state.days && !this.state.tripDepart){
              SimpleToast.show('Select number of days');
              return;
            }

            this.setState({modalVisible: false}, (val) => {
              this.props.navigation.replace('CarOption', {params: this.state});
            });
          }}>
          <Text
            style={[
              Styles.button_font,
              {
                color: COLOR.WHITE,
                width: null,
                textAlign: 'center',
              },
            ]}>
            Next
          </Text>
        </TouchableOpacity>
        {this.state.openCalendar ? (
          <SelectDate
            modal={true}
            minDate={this.state.minDate}
            maxDate={this.state.maxDate}
            allowRangeSelection={false}
            attrName={this.state.attrName}
            selector={this.state.selector}
            updateMasterState={this.updateMasterState}
            props={this.props.props}
          />
        ) : null}
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  modal: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
  },
  modalBody: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    backgroundColor: COLOR.WHITE,
    borderWidth: 1,
    width: wp(100),
    marginTop: 50,
    borderColor: COLOR.BORDER,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    flex: 1,
  },
  image: {
    height: 120,
    width: 120,
    alignSelf: 'center',
    marginVertical: 20,
  },
  headingLabel: {
    fontFamily: FONTS.FAMILY_BOLD,
    fontSize: FONTS.MEDIUM,
    color: COLOR.BLACK,
  },
  singleLineView: {
    marginVertical: 20,
    borderBottomColor: COLOR.GRAY,
    width: wp(100),
    alignSelf: 'center',
    borderBottomWidth: 1,
  },
  borderView: {
    borderRadius: 10,
    borderWidth: 3,
    width: wp(90),
    borderColor: COLOR.GRAY,
    alignSelf: 'center',
  },
});
