import React from 'react';
import {SafeAreaView} from 'react-native';
import {Text, View, StyleSheet, Image, Button} from 'react-native';
import {TextInput, TouchableOpacity} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import SimpleToast from 'react-native-simple-toast';
import COLOR from '../../../components/styles/Color';
import IMAGES from '../../../components/styles/Images';
import Styles from '../../../components/styles/Styles';
import { DoneButtonKeyboard } from '../../commonView/CommonView';
import GooglePlacesTextInput from '../../commonView/GooglePlacesTextInput';
import {
  getEndDateTimestamp,
  getStartDateTimestamp,
} from '../../commonView/Helpers';
import FONTS from '../../styles/Fonts';
import SelectDate from '../calendar/SelectDate';

export default class SameDropoff extends React.Component {
  constructor(props) {
    super(props);
    let route = this.props.props.route.params;
    this.state = {
      modalVisible: true,
      selectedIndex: null,
      pickup_location: '',
      is_drop_off_same: 1,
      days: null,
      checkInDate: null,
      checkOutDate: null,
      openCalendar: false,
      id: route.id,
      attrName: '',
      data: null,
      carId: null,
      minDate: getStartDateTimestamp(route.depart),
      maxDate: getEndDateTimestamp(route.return),
      tripDepart: route.depart,
      tripReturn: route.return,
      package_request_id: route.package_request_id,
      selector: 0,
    };
  }

  componentDidMount() {
    if (this.props.props.route.params.data) {
      let data = this.props.props.route.params.data;
      console.log(data);
      this.setState({
        data: data,
        carId: data.id,
        days: data.days ? data.days : null,
        checkInDate: data.pickup_date,
        checkOutDate: data.drop_off_date,
        pickup_location: data.pickup_location,
      });
    }
  }

  updateMasterState = (attrName, value) => {
    this.setState({[attrName]: value});
  };

  render() {
    return (
      <SafeAreaView style={{flex: 1}}>
        <View
          style={[
            Styles.shadow_view,
            {
              width: wp(90),
              marginVertical: 0,
              alignSelf: 'center',
              backgroundColor: COLOR.WHITE,
              borderBottomLeftRadius: 10,
              borderBottomRightRadius: 10,
              padding: 20,
            },
          ]}>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              height: 50,
              zIndex: 1000,
            }}>
            <Image
              style={{height: 25, width: 25, marginRight: 20}}
              source={IMAGES.pinpoint}
              resizeMode={'contain'}
            />
            <View>
              <Text style={[Styles.small_label]}>Pickup Location</Text>
              <GooglePlacesTextInput
                value={this.state.pickup_location}
                onValueChange={(text) => this.setState({pickup_location: text})}
              />
            </View>
          </View>
          <View
            style={[Styles.line_view, {width: wp(80), marginVertical: 15}]}
          />
          {this.state.tripDepart ? (
            <View>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  height: 50,
                }}>
                <Image
                  style={{height: 25, width: 25, marginRight: 20}}
                  source={IMAGES.calendar_small}
                  resizeMode={'contain'}
                />
                <View>
                  <Text style={Styles.small_label}>Pickup Date</Text>
                  <TouchableOpacity
                    onPress={() => {
                      this.setState({
                        openCalendar: true,
                        attrName: 'checkInDate',
                        selector: 0,
                      });
                    }}>
                    <Text style={Styles.body_label}>
                      {this.state.checkInDate
                        ? this.state.checkInDate
                        : 'Select Pickup Date'}{' '}
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>
              <View
                style={[Styles.line_view, {width: wp(80), marginVertical: 15}]}
              />
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                }}>
                <Image
                  style={{height: 25, width: 25, marginRight: 20}}
                  source={IMAGES.calendar_small}
                  resizeMode={'contain'}
                />
                <View>
                  <Text style={Styles.small_label}>Drop-off Date</Text>
                  <TouchableOpacity
                    onPress={() => {
                      this.setState({
                        openCalendar: true,
                        attrName: 'checkOutDate',
                        selector: 1,
                      });
                    }}>
                    <Text style={Styles.body_label}>
                      {this.state.checkOutDate
                        ? this.state.checkOutDate
                        : 'Select Drop-off Date'}{' '}
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          ) : (
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                height: 50,
              }}>
              <Image
                style={{height: 25, width: 25, marginRight: 20}}
                source={IMAGES.calendar_small}
                resizeMode={'contain'}
              />
              <View>
                <Text style={[Styles.small_label]}>Number of Days</Text>
                <TextInput
                inputAccessoryViewID={'uniqueID'}
                  style={Styles.body_label}
                  placeholderTextColor={COLOR.LIGHT_TEXT}
                  value={this.state.days}
                  keyboardType={'name-phone-pad'}
                  returnKeyType={'done'}
                  blurOnSubmit={true}
                  placeholder={'Enter Number of Days'}
                  onChangeText={(text) => this.setState({days: text})}
                />
                <DoneButtonKeyboard />
              </View>
            </View>
          )}
        </View>

        <TouchableOpacity
          style={{
            width: wp(90),
            alignSelf: 'center',
            height: 50,
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: COLOR.GREEN,
            borderRadius: 10,
            bottom: 20,
            position: 'absolute',
          }}
          onPress={() => {
            if(this.state.pickup_location == ''){
              SimpleToast.show('Enter pickup location');
              return;
            } else if(this.state.checkInDate == '' || this.state.checkInDate == null && this.state.tripDepart){
              SimpleToast.show('Select pickup date');
              return;
            } else if(this.state.checkOutDate == '' || this.state.checkOutDate == null && this.state.tripDepart){
              SimpleToast.show('Select drop-off date');
              return;
            } else if(!this.state.days && !this.state.tripDepart){
              SimpleToast.show('Select number of days');
              return;
            }



            this.setState({modalVisible: false}, (val) => {
              this.props.navigation.replace('CarOption', {
                params: this.state,
              });
            });
          }}>
          <Text
            style={[
              Styles.button_font,
              {
                color: COLOR.WHITE,
                width: null,
                textAlign: 'center',
              },
            ]}>
            Next
          </Text>
        </TouchableOpacity>
        {this.state.openCalendar ? (
          <SelectDate
            minDate={this.state.minDate}
            maxDate={this.state.maxDate}
            modal={true}
            allowRangeSelection={false}
            selector={this.state.selector}
            updateMasterState={this.updateMasterState}
            props={this.props.props}
          />
        ) : null}
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  modal: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
  },
  modalBody: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    backgroundColor: COLOR.WHITE,
    borderWidth: 1,
    width: wp(100),
    marginTop: 50,
    borderColor: COLOR.BORDER,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    flex: 1,
  },
  image: {
    height: 120,
    width: 120,
    alignSelf: 'center',
    marginVertical: 20,
  },
  headingLabel: {
    fontFamily: FONTS.FAMILY_BOLD,
    fontSize: FONTS.MEDIUM,
    color: COLOR.BLACK,
  },
  singleLineView: {
    marginVertical: 20,
    borderBottomColor: COLOR.GRAY,
    width: wp(100),
    alignSelf: 'center',
    borderBottomWidth: 1,
  },
  borderView: {
    borderRadius: 10,
    borderWidth: 3,
    width: wp(90),
    borderColor: COLOR.GRAY,
    alignSelf: 'center',
  },
});
