'use strict';

import React from 'react';
import {View, Image, StyleSheet, Text, SafeAreaView} from 'react-native';
import {
  TouchableOpacity,
  TouchableWithoutFeedback,
  TextInput,
} from 'react-native';
import {
  heightPercentageToDP,
  heightPercentageToDP as hp,
  widthPercentageToDP,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import DatePicker from '../../commonView/DatePicker';
import COLOR from '../../../components/styles/Color';
import FONTS from '../../../components/styles/Fonts';
import IMAGES from '../../../components/styles/Images';
import Styles from '../../../components/styles/Styles';
import CarAction from '../../../redux/action/CarAction';
import {connect} from 'react-redux';
import SimpleToast from 'react-native-simple-toast';
import { DoneButtonKeyboard } from '../../commonView/CommonView';

class CarOption extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedIndex: 0,
      car_types: [],
      suppliers: [],
      policies: [],
      specifications: [],
      financial_responsibilities: [],
      extras: null,
      id: this.props.route.params.params.id,
      comment: '',
    };
    this.props.getCarType();
    this.props.getCarSpecifications();
    this.props.getCarFinanceResponsibility();
    this.props.getCarSupplier();
    this.props.getCarPolicy();
  }

  componentDidMount() {
    let params = this.props.route.params.params;
    if (params.carId) {
      this.props.getSingleCar(params.carId);
    }
  }

  componentDidUpdate(prevProps) {
    if (this.props.singleCarData !== prevProps.singleCarData) {
      let prefs = this.props.singleCarData.car_preference;
      this.setState({
        car_types: prefs.types.map((v) => v.id),
        suppliers: prefs.suppliers.map((v) => v.id),
        policies: prefs.policies.map((v) => v.id),
        specifications: prefs.specifications.map((v) => v.id),
        financial_responsibilities: prefs.responsibility.map((v) => v.id),
        extras: {
          child_seat: prefs.extra.child_seat,
          baby_seat: prefs.extra.baby_seat,
          booster_seat: prefs.extra.booster_seat,
          additional_driver: prefs.extra.addition_driver,
        },
        comment: this.props.singleCarData.comment,
      });
    }
  }

  componentWillUnmount() {
    this.props.clearSingleCar();
  }

  updateMasterState = (attrName, value) => {
    this.setState({[attrName]: value});
  };

  callScheduleCar = () => {
    let params = this.props.route.params.params;
    let data = JSON.stringify({
      tripId: this.state.id,
      pickup_location: params.pickup_location,
      drop_off_location: params.drop_off_location,
      is_drop_off_same: params.is_drop_off_same,
      pickup_date: params.checkInDate,
      drop_off_date: params.checkOutDate,
      days: params.days,
      package_request_id: params.package_request_id,
      car_types: this.state.car_types,
      suppliers: this.state.suppliers,
      policies: this.state.policies,
      specifications: this.state.specifications,
      financial_responsibilities: this.state.financial_responsibilities,
      extras: this.state.extras,
      commet: this.state.comment,
    });
    console.log(data);

    if (params.carId) {
      this.props.updateCar(data, params.carId);
    } else {
      this.props.scheduleCar(data, this.state.id);
    }
  };

  handleValidation = () => {
    if (this.state.car_types.length == 0) {
      SimpleToast.show('Select car type');
    } else if (this.state.specifications.length == 0) {
      SimpleToast.show('Select specification');
    } else if (this.state.policies.length == 0) {
      SimpleToast.show('Select policies');
    } else if (this.state.financial_responsibilities.length == 0) {
      SimpleToast.show('Select Financial Responsibilities');
    } else if (this.state.suppliers.length == 0) {
      SimpleToast.show('Select Car Supplier');
    } else if (this.state.extras.length == 0) {
      SimpleToast("Select Extra'");
    } else {
      this.setState({modalVisible: false}, (val) => {
        this.callScheduleCar();
        this.props.navigation.goBack();
      });
    }
  };

  selectionView = (prop) => {
    return (
      <TouchableWithoutFeedback style={{height: 130}} onPress={prop.onPress}>
        <View
          style={{
            flex: 1,
            borderRadius: 10,
            borderWidth: 1,
            height: 130,
            width: wp(28),
            margin: 5,
            borderColor: prop.hasData ? '#05778F' : COLOR.GRAY,
            alignSelf: 'center',
            alignItems: 'center',
            justifyContent: 'center',
            padding: 10,
            backgroundColor : prop.hasData ? COLOR.GREEN_OPACITY : COLOR.WHITE
          }}>
          <View
            style={{
              height: 60,
              width: 60,
              marginVertical: 10,
              backgroundColor: prop.hasData ? COLOR.WHITE : prop.bg,
              borderRadius: 5,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Image
              resizeMode={'contain'}
              source={prop.image}
              style={{
                height: 30,
                width: 30,
              }}
            />
          </View>
          <Text style={{marginBottom: 10, textAlign: 'center', fontSize: 14}}>
            {prop.name}
          </Text>
        </View>
      </TouchableWithoutFeedback>
    );
  };

  render() {
    return (
      <SafeAreaView style={[Styles.container]}>
        <View>
          <View style={style.topBar}>
            <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
              <Image
                style={{height: 15, width: 15}}
                source={IMAGES.back_arrow}
              />
            </TouchableOpacity>
            <Text
              style={[
                Styles.subheading_label,
                {width: null, fontSize: 20, fontFamily: FONTS.FAMILY_BOLD},
              ]}>
              Car Options
            </Text>
            <TouchableOpacity>
              <Image
                style={{height: 20, width: 20}}
                source={IMAGES.filter_image}
              />
            </TouchableOpacity>
          </View>
          <View
            style={{
              flexDirection: 'row',
              width: wp(90),
              alignSelf: 'center',
              marginTop: 20,
              justifyContent: 'space-between',
            }}>
            <this.selectionView
              name={'Car Type'}
              image={IMAGES.red_car}
              bg={'#CF212B20'}
              hasData={this.state.car_types.length > 0 ? true : false}
              onPress={() => {
                this.props.navigation.navigate('CarType', {
                  data: this.props.carType,
                  updateMasterState: this.updateMasterState,
                  selected: this.state.car_types,
                });
              }}
            />
            <this.selectionView
              name={'Specification'}
              image={IMAGES.yshape}
              bg={'#05778F20'}
              hasData={this.state.specifications.length > 0 ? true : false}
              onPress={() => {
                this.props.navigation.navigate('SpecificationCar', {
                  data: this.props.carSpecifications,
                  updateMasterState: this.updateMasterState,
                  selected: this.state.specifications,
                });
              }}
            />
            <this.selectionView
              name={'Policies'}
              image={IMAGES.shield}
              bg={'#02599520'}
              hasData={this.state.policies.length > 0 ? true : false}
              onPress={() => {
                this.props.navigation.navigate('CarReservationPolicy', {
                  data: this.props.carPolicy,
                  updateMasterState: this.updateMasterState,
                  selected: this.state.policies,
                });
              }}
            />
          </View>
          <View
            style={{
              flexDirection: 'row',
              width: wp(90),
              alignSelf: 'center',
              marginBottom: 20,
              justifyContent: 'space-between',
            }}>
            <this.selectionView
              name={'Financial\nResponsibilities'}
              image={IMAGES.financial}
              hasData={this.state.financial_responsibilities.length > 0 ? true : false}
              bg={'#02599520'}
              onPress={() => {
                this.props.navigation.navigate('CarFinancialResponsibility', {
                  data: this.props.carFinanceResponsibility,
                  updateMasterState: this.updateMasterState,
                  selected: this.state.financial_responsibilities,
                });
              }}
            />
            <this.selectionView
              name={'Car Supplier'}
              image={IMAGES.home_yellow}
              hasData={this.state.suppliers.length > 0 ? true : false}
              bg={'#02599520'}
              onPress={() => {
                this.props.navigation.navigate('CarSupplier', {
                  data: this.props.carSupplier,
                  updateMasterState: this.updateMasterState,
                  selected: this.state.suppliers,
                });
              }}
            />
            <this.selectionView
              name={'Extra'}
              image={IMAGES.extra_circle}
              hasData={this.state.extras ? true : false}
              bg={'#CF212B20'}
              onPress={() => {
                this.props.navigation.navigate('CarExtra', {
                  updateMasterState: this.updateMasterState,
                  selected: this.state.extras,
                });
              }}
            />
          </View>
        </View>

        <View style={[Styles.line_view, {marginVertical: 20, width: wp(90)}]} />

        <Text style={[Styles.subheading_label, {fontSize: FONTS.REGULAR}]}>
          Comments
        </Text>
        <TextInput
        inputAccessoryViewID={'uniqueID'}
          placeholder={'Write your comment'}
          placeholderTextColor={COLOR.LIGHT_TEXT}
          style={[
            {
              height: 120,
              paddingTop:15,
              alignSelf: 'center',
              paddingHorizontal: 20,
              paddingVertical: 20,
              width: wp(90),
              alignSelf: 'center',
              borderRadius: 10,
              borderWidth: 1,
              borderColor: COLOR.GRAY,
              marginTop: 10,
            },
          ]}
          multiline={true}
          value={this.state.comment}
          onChangeText={(text) => this.setState({comment: text})}
        />
        <DoneButtonKeyboard />
        <View
          style={{
            marginVertical: 20,
            alignSelf: 'center',
            bottom: 0,
            position: 'absolute',
          }}>
          {/* <TouchableOpacity
            style={{
              width: wp(90),
              alignSelf: 'center',
              height: 50,
              alignItems: 'center',
              justifyContent: 'center',

              borderRadius: 10,
            }}
            onPress={() => {
             
            }}>
            <Text
              style={[
                Styles.small_label,
                {
                  color: COLOR.LIGHT_TEXT,
                  width: null,
                  textAlign: 'right',
                },
              ]}>
              Clear all options
            </Text>
          </TouchableOpacity> */}
          <TouchableOpacity
            style={{
              width: wp(90),
              alignSelf: 'center',
              height: 50,
              alignItems: 'center',
              justifyContent: 'center',
              backgroundColor: COLOR.GREEN,
              borderRadius: 10,
              marginTop: 0,
            }}
            onPress={() => {
              this.handleValidation();
            }}>
            <Text
              style={[
                Styles.button_font,
                {
                  color: COLOR.WHITE,
                  width: null,
                  textAlign: 'center',
                },
              ]}>
              Apply
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              width: wp(90),
              alignSelf: 'center',
              height: 50,
              alignItems: 'center',
              justifyContent: 'center',

              borderRadius: 10,
            }}>
            <Text
              style={[
                Styles.small_label,
                {
                  color: COLOR.BLACK,
                  width: null,
                  textAlign: 'right',
                },
              ]}>
              Skip car options
            </Text>
          </TouchableOpacity>
        </View>
      </SafeAreaView>
    );
  }
}

const style = StyleSheet.create({
  topBar: {
    height: 50,
    backgroundColor: COLOR.WHITE,
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: wp(90),
    alignSelf: 'center',
    marginVertical: 0,
    alignItems: 'center',
  },
});

const mapDispatchToProps = (dispatch) => {
  return {
    getCarType: (val) => {
      dispatch(CarAction.getCarType(val));
    },
    getCarSpecifications: (val) => {
      dispatch(CarAction.getCarSpecifications(val));
    },
    getCarFinanceResponsibility: (val) => {
      dispatch(CarAction.getCarFinanceResponsibility(val));
    },
    getCarSupplier: (val) => {
      dispatch(CarAction.getCarSupplier(val));
    },
    getCarPolicy: (val) => {
      dispatch(CarAction.getCarPolicy(val));
    },
    scheduleCar: (data, id) => {
      dispatch(CarAction.scheduleCar(data, id));
    },
    getSingleCar: (id) => {
      dispatch(CarAction.getSingleCar(id));
    },
    updateCar: (data, id) => {
      dispatch(CarAction.updateCar(data, id));
    },
    clearSingleCar: () => {
      dispatch(CarAction.clearSingleCar());
    },
  };
};

const mapStateToProps = (state, ownProps) => {
  return {
    carType: state.carReducer.carTypes,
    carSpecifications: state.carReducer.carSpecifications,
    carFinanceResponsibility: state.carReducer.carFinanceResponsibility,
    carSupplier: state.carReducer.carSupplier,
    carPolicy: state.carReducer.carPolicy,
    singleCarData: state.carReducer.singleCarData,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(CarOption);
