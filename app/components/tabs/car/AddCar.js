import {View, Image, Text} from 'react-native';
import React from 'react';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import COLOR from '../../styles/Color';
import Styles from '../../styles/Styles';
import {TouchableOpacity} from 'react-native';
import IMAGES from '../../styles/Images';
import TopTabBar from '../home/trip/TabBarView';
import {T_SCHEDULE_CAR_TAB} from '../../../helper/Constants';
import FONTS from '../../styles/Fonts';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import {SafeAreaView} from 'react-native';
import DifferentDropoff from './DifferentDropoff';
import TripDateView from '../../commonView/TripDateView';

export default class AddCar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      depart: '',
      return: '',
      departFlexibility: '',
      returnFlexibility: '',
      data: null,
    };
  }
  componentDidMount() {
    if (this.props.route.params.data) {
      this.setState({
        data: this.props.route.params.data,
      });
    }
    if (this.props.route.params.depart) {
      let data = this.props.route.params;
      this.setState({
        depart: data.depart,
        return: data.return,
        departFlexibility: data.departFlexibility,
        returnFlexibility: data.returnFlexibility,
      });
    }
  }
  render() {
    return (
      <View style={[Styles.container, {backgroundColor: COLOR.BACKGROUND}]}>
        <View
          style={{
            height: 220,
            backgroundColor: COLOR.GREEN,
            borderBottomRightRadius: 15,
            borderBottomLeftRadius: 15,
          }}>
          <View
            style={[
              Styles.mainView,
              {
                height: 45,
                marginTop: getStatusBarHeight(true),
                justifyContent: 'space-between',
                alignSelf: 'center',
                alignItems: 'center',
                flexDirection: 'row',
              },
            ]}>
            <TouchableOpacity
              style={{padding: 10}}
              onPress={() => this.props.navigation.goBack()}>
              <Image
                style={{height: 15, width: 15, tintColor: COLOR.WHITE}}
                source={IMAGES.back_arrow}
              />
            </TouchableOpacity>

            <View>
              <Text
                style={[
                  Styles.heading_label,
                  {
                    textAlign: 'center',
                    color: COLOR.WHITE,
                    fontFamily: FONTS.FAMILY_BOLD,
                    marginVertical: 0,
                    width: null,
                  },
                ]}>
                Car
              </Text>
            </View>
            <Image
              style={{
                height: 15,
                width: 15,
                tintColor: COLOR.WHITE,
                padding: 10,
              }}
            />
          </View>
          <TripDateView
            departDate={this.state.depart}
            returnDate={this.state.return}
            departFlexibility={this.state.departFlexibility}
            returnFlexibility={this.state.returnFlexibility}
          />
        </View>
        <SafeAreaView
          style={[
            {
              marginTop: -50,
              width: wp(90),
              alignSelf: 'center',
              borderRadius: 10,
              flex: 1,
            },
          ]}>
          {this.state.data ? (
            <DifferentDropoff
              props={this.props}
              navigation={this.props.navigation}
            />
          ) : (
            <TopTabBar currentTab={T_SCHEDULE_CAR_TAB} props={this.props} />
          )}
        </SafeAreaView>
      </View>
    );
  }
}
