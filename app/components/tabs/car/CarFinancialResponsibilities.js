import React from 'react';
import {Modal, Text, View, StyleSheet, Image} from 'react-native';
import {
  FlatList,
  TouchableOpacity,
  TouchableWithoutFeedback,
} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import COLOR from '../../../components/styles/Color';
import FONTS from '../../../components/styles/Fonts';
import IMAGES from '../../../components/styles/Images';
import Styles from '../../../components/styles/Styles';
import NavigationBar from '../../commonView/NavigationBar';

export default class CarFinancialResponsibility extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: true,
      selectedItem: this.props.route.params.selected,
    };
  }

  updateMasterState = (attrName, value) => {
    this.setState({[attrName]: value});
  };

  SelectionView = (props) => {
    return (
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          marginTop: 10,
        }}>
        <Text style={[Styles.body_label, {width: 80}]}>{props.name}</Text>
        <Text style={[Styles.body_label, {width: null}]}>{props.time}</Text>
        <TouchableOpacity
          style={{
            height: 30,
            width: 30,
            borderRadius: 5,
            backgroundColor: props.bg,
            borderColor: COLOR.GRAY,
            justifyContent: 'center',
            alignItems: 'center',
            borderWidth: 1,
          }}>
          <Image
            resizeMode={'contain'}
            style={{height: 20, width: 20, alignSelf: 'center'}}
            source={props.image}
          />
        </TouchableOpacity>
      </View>
    );
  };

  addOrRemoveItem = (item) => {
    let array = [...this.state.selectedItem];
    let index = array.findIndex((value) => value == item);
    if (index != -1) {
      array.splice(index, 1);
    } else {
      array.push(item);
    }
    this.setState({
      selectedItem: array,
    });
  };

  render() {
    return (
      <View style={Styles.container}>
        <Modal
          animationType={'slide'}
          transparent={true}
          style={{backgroundColor: COLOR.BLACK}}
          visible={this.state.modalVisible}
          onRequestClose={() => {
            console.log('Modal has been closed.');
          }}>
          <View style={styles.modal}>
            <View style={styles.modalBody}>
              <NavigationBar
                right_image={IMAGES.filter_image}
                prop={this.props}
                navHeight={40}
                name={'Financial Responsibility'}
              />
              <View
                style={{
                  flex: 1,
                  width: wp(90),
                  alignSelf: 'center',
                  paddingVertical: 10,
                }}>
                <FlatList
                  style={{flex: 0}}
                  data={this.props.route.params.data}
                  renderItem={({item}) => (
                    <TouchableWithoutFeedback>
                      <View
                        style={{
                          flexDirection: 'row',
                          justifyContent: 'space-between',
                          marginTop: 10,
                        }}>
                        <Text style={[Styles.body_label, {width: 170}]}>
                          {item.responsibility}
                        </Text>

                        <TouchableOpacity
                          onPress={() => {
                            this.addOrRemoveItem(item.id);
                          }}
                          style={{
                            height: 30,
                            width: 30,
                            borderRadius: 5,
                            backgroundColor: this.state.selectedItem.some(
                              (value) => value == item.id,
                            )
                              ? COLOR.GREEN
                              : COLOR.WHITE,
                            borderColor: COLOR.GRAY,
                            justifyContent: 'center',
                            alignItems: 'center',
                            borderWidth: 1,
                          }}>
                          <Image
                            resizeMode={'contain'}
                            style={{height: 20, width: 20, alignSelf: 'center'}}
                            source={IMAGES.tick_image}
                          />
                        </TouchableOpacity>
                      </View>
                    </TouchableWithoutFeedback>
                  )}
                  numColumns={'1'}
                  removeClippedSubviews={false}
                  showsVerticalScrollIndicator={false}
                  showsHorizontalScrollIndicator={false}
                  keyExtractor={(item, index) => index.toString()}
                />
              </View>
              <TouchableOpacity
                style={{
                  width: wp(90),
                  alignSelf: 'center',
                  height: 50,
                  alignItems: 'center',
                  justifyContent: 'center',
                  backgroundColor: COLOR.GREEN,
                  borderRadius: 10,
                  marginBottom: 20,
                }}
                onPress={() => {
                  this.setState({modalVisible: false}, (val) => {
                    this.props.route.params.updateMasterState(
                      'financial_responsibilities',
                      this.state.selectedItem,
                    );
                    this.props.navigation.goBack();
                  });
                }}>
                <Text
                  style={[
                    Styles.button_font,
                    {
                      color: COLOR.WHITE,
                      width: null,
                      textAlign: 'center',
                    },
                  ]}>
                  Apply
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  modal: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
  },
  modalBody: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    backgroundColor: COLOR.WHITE,
    borderWidth: 1,
    width: wp(100),
    marginTop: 50,
    borderColor: COLOR.BORDER,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    flex: 1,
  },
  image: {
    height: 120,
    width: 120,
    alignSelf: 'center',
    marginVertical: 20,
  },
  headingLabel: {
    fontFamily: FONTS.FAMILY_BOLD,
    fontSize: FONTS.MEDIUM,
    color: COLOR.BLACK,
  },
  singleLineView: {
    marginVertical: 20,
    borderBottomColor: COLOR.GRAY,
    width: wp(100),
    alignSelf: 'center',
    borderBottomWidth: 1,
  },
  borderView: {
    borderRadius: 10,
    borderWidth: 3,
    width: wp(90),
    borderColor: COLOR.GRAY,
    alignSelf: 'center',
  },
});
