import {View, Image, Text} from 'react-native';
import React from 'react';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import COLOR from '../../styles/Color';

import Styles from '../../styles/Styles';
import {
  FlatList,
  TouchableOpacity,
  TouchableWithoutFeedback,
} from 'react-native';
import IMAGES from '../../styles/Images';
import FONTS from '../../styles/Fonts';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import {
  getEndDateTimestamp,
  getStartDateTimestamp,
} from '../../commonView/Helpers';
import TripDateView from '../../commonView/TripDateView';
import API from '../../../api/Api';
import {connect} from 'react-redux';
import CarAction from '../../../redux/action/CarAction';
import SimpleToast from 'react-native-simple-toast';

const api = new API();
class CarScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      minDate: getStartDateTimestamp(this.props.route.params.depart),
      maxDate: getEndDateTimestamp(this.props.route.params.return),
      tripDepart: this.props.route.params.depart,
      tripReturn: this.props.route.params.return,
      carData: [],
      isPackage: this.props.route.params.isPackage
        ? this.props.route.params.isPackage
        : false,
      departFlexibility: '',
      returnFlexibility: '',
      package_request_id: this.props.route.params.package_request_id,
    };

    this.handleNavigation = this.handleNavigation.bind(this);
    this.props.getScheduledCar(this.props.route.params.id);
  }

  componentDidMount() {
    if (this.props.route.params.depart) {
      let data = this.props.route.params;
      this.setState({
        departFlexibility: data.departFlexibility,
        returnFlexibility: data.returnFlexibility,
      });
    }

    if (this.props.carData) {
      this.setState({
        carData: this.props.carData,
      });
    }
  }

  componentDidUpdate(prevProps) {
    if (this.props.carData != prevProps.carData) {
      this.setState({
        carData: this.props.carData,
      });
    }
  }

  handleNavigation = () => {
    const screens = this.props.route.params.screens;
    const index = this.props.route.params.index;
    const id = this.props.route.params.id;
    if (screens[index + 1]) {
      console.log(screens.length);
      this.props.navigation.replace(screens[index + 1], {
        screens: screens,
        index: index + 1,
        id: id,
        depart: this.state.tripDepart,
        return: this.state.tripReturn,
        departFlexibility: this.state.departFlexibility,
        returnFlexibility: this.state.returnFlexibility,
      });
    } else if (this.state.isPackage) {
      this.props.navigation.goBack();
    } else {
      if(this.state.carData.length == 0){
SimpleToast.show('Add car to continue.')
      } else {
      this.props.navigation.navigate('HomeStack');}
    }
  };

  render() {
    const screens = this.props.route.params.screens;
    const index = this.props.route.params.index;
    const id = this.props.route.params.id;
    return (
      <View style={[Styles.container, {backgroundColor: COLOR.BACKGROUND}]}>
        <View
          style={{
            height: 220,
            backgroundColor: COLOR.GREEN,
            borderBottomRightRadius: 15,
            borderBottomLeftRadius: 15,
          }}>
          <View
            style={[
              Styles.mainView,
              {
                height: 45,
                marginTop: getStatusBarHeight(true),
                justifyContent: 'space-between',
                alignSelf: 'center',
                alignItems: 'center',
                flexDirection: 'row',
              },
            ]}>
            <TouchableOpacity
              style={{padding: 10}}
              onPress={() => this.props.navigation.goBack()}>
              <Image
                style={{
                  height: 15,
                  width: 15,
                  tintColor: COLOR.WHITE,
                }}
                source={IMAGES.back_arrow}
              />
            </TouchableOpacity>

            <View>
              <Text
                style={[
                  Styles.heading_label,
                  {
                    textAlign: 'center',
                    color: COLOR.WHITE,
                    marginVertical: 0,
                    width: null,
                    fontFamily: FONTS.FAMILY_BOLD,
                  },
                ]}>
                Car
              </Text>
            </View>
            <Image
              style={{
                width: 20,
                height: 20,
                padding: 10,
              }}
            />
          </View>
          <TripDateView
            departDate={this.state.tripDepart}
            returnDate={this.state.tripReturn}
            departFlexibility={this.state.departFlexibility}
            returnFlexibility={this.state.returnFlexibility}
          />
        </View>
        <View
          style={{
            marginTop: -50,
            width: wp(90),
            alignSelf: 'center',
            borderRadius: 10,
            shadowColor: COLOR.VOILET,
            elevation: 3,
            shadowOffset: {width: 2, height: 2},
            shadowOpacity: 0.2,
            backgroundColor: COLOR.WHITE,
          }}>
          <View>
            <FlatList
              showsVerticalScrollIndicator={false}
              data={this.state.carData}
              style={{paddingVertical: 10}}
              renderItem={({item}) => (
                <View>
                  <View
                    style={{
                      height: 40,
                      paddingHorizontal: 15,
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      alignSelf: 'center',
                      alignItems: 'center',
                    }}>
                    <Text style={[Styles.body_label, {width: null, flex: 1}]}>
                      {item.drop_off_location}
                    </Text>
                    <View style={{flexDirection: 'row'}}>
                      <TouchableOpacity
                        onPress={() =>
                          this.props.navigation.navigate('AddCar', {
                            data: item,
                            id: id,
                            depart: this.state.tripDepart,
                            return: this.state.tripReturn,
                          })
                        }
                        style={{
                          aspectRatio: 1,
                          height: 20,
                          alignItems: 'center',
                          justifyContent: 'center',
                          marginHorizontal: 10,
                        }}>
                        <Image
                          style={{
                            height: 20,
                            width: 20,
                          }}
                          source={IMAGES.edit}
                        />
                      </TouchableOpacity>
                      <TouchableOpacity
                        onPress={() => {
                          this.props.deleteCar(item.id, id);
                          let data = this.state.carData;
                          data.splice(index, 1);
                          this.setState({
                            carData: data,
                          });
                        }}
                        style={{
                          aspectRatio: 1,
                          height: 20,
                          alignItems: 'center',
                          justifyContent: 'center',
                        }}>
                        <Image
                          style={{
                            height: 20,
                            width: 20,
                          }}
                          source={IMAGES.cross_icon}
                        />
                      </TouchableOpacity>
                    </View>
                  </View>
                  <View
                    style={[
                      Styles.line_view,
                      {width: wp(80), alignSelf: 'center', marginVertical: 10},
                    ]}
                  />
                </View>
              )}
              numColumns={1}
              ListEmptyComponent={() =>
                !this.state.carData.length ? (
                  <Text
                    style={{
                      textAlign: 'center',
                      color: COLOR.VOILET,
                      marginVertical: 40,
                    }}>
                    You did not add any Car
                  </Text>
                ) : null
              }
              keyExtractor={(item, index) => index.toString()}
            />

            <TouchableOpacity
              style={{
                width: wp(85),
                alignSelf: 'center',
                height: 50,
                alignItems: 'center',
                justifyContent: 'center',
                backgroundColor: COLOR.WHITE,
                borderRadius: 10,
                marginBottom: 20,
                borderColor: COLOR.GRAY,
                borderWidth: 1,
                marginTop: 20,
              }}
              onPress={() => {
                this.setState({modalVisible: false}, (val) => {
                  this.props.navigation.navigate('AddCar', {
                    id: id,
                    depart: this.state.tripDepart,
                    return: this.state.tripReturn,
                    departFlexibility: this.state.departFlexibility,
                    returnFlexibility: this.state.returnFlexibility,
                    package_request_id: this.state.package_request_id,
                  });
                });
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <View
                  style={{
                    justifyContent: 'center',
                    alignItems: 'center',
                    width: 25,
                    height: 25,
                    borderRadius: 15,
                    backgroundColor: COLOR.GREEN,
                    marginRight: 10,
                  }}>
                  <Image
                    style={{
                      width: 15,
                      height: 15,
                      borderRadius: 10,

                      tintColor: COLOR.WHITE,
                    }}
                    source={IMAGES.plus_image}
                  />
                </View>
                <Text
                  style={[
                    Styles.button_font,
                    {
                      color: COLOR.BLACK,
                      width: null,
                      textAlign: 'center',
                    },
                  ]}>
                  Add New Car
                </Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
        <TouchableOpacity
          style={{
            width: wp(90),
            alignSelf: 'center',
            height: 50,
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: COLOR.GREEN,
            borderRadius: 10,
            marginBottom: 20,
            bottom: 20,
            position: 'absolute',
          }}
          onPress={this.handleNavigation}>
          <Text
            style={[
              Styles.button_font,
              {
                color: COLOR.WHITE,
                width: null,
                textAlign: 'center',
              },
            ]}>
            Next
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    carData: state.carReducer.scheduledCars,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getScheduledCar: (id) => {
      dispatch(CarAction.getCarSchedules(id));
    },
    deleteCar: (id) => {
      dispatch(CarAction.deleteCar(id));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(CarScreen);
