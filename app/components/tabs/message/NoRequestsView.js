import { ImageBackground, View, Image, Text } from 'react-native';
import React from 'react';

import IMAGES from '../../styles/Images';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import COLOR from '../../styles/Color';
import Styles from '../../styles/Styles';
import FONTS from '../../styles/Fonts';
import { TouchableOpacity } from 'react-native';
import { navigate } from '../../routes/RootNavigation';

export const NoRequestsView = (props) => (
  <View
    style={[
      {
        width: wp(90),
        alignSelf: 'center',
        marginTop: 20,
        backgroundColor: COLOR.WHITE,
        elevation: 4,
        shadowColor: COLOR.VOILET,
        shadowOpacity: 0.2,
        shadowOffset: { width: 0.2, height: 0.2 },
        borderRadius: 20,
        alignItems: 'center',
      },
    ]}>
    <View>
      <Image
        source={IMAGES.bitmap}
        resizeMode={'contain'}
        style={{ height: hp(11), marginTop: 70 }}
      />
      <Text
        style={[
          Styles.button_font,
          {
            width: null,
            alignSelf: 'center',
            marginTop: 50,
            marginBottom: 10,
            textAlign: 'center',
            fontSize: FONTS.MEDIUM,
            fontFamily: FONTS.FAMILY_BOLD,
            color: COLOR.BLACK,
            fontSize: 20
          },
        ]}>
        You have no {props.props.status == 0 ? 'Upcoming' : props.props.status == 6 ? 'Past' : 'Cancelled'}{'\n'}Requests
      </Text>
      <Text
        style={[
          Styles.small_label,
          {
            width: wp(65),
            alignSelf: 'center',
            marginVertical: 0,
            textAlign: 'center',
            color: COLOR.VOILET,
            fontSize: 14,
          },
        ]}>
        Submit your application and we will give you the most cheap and perfect
        trip on your budget.
      </Text>
      <View
        style={{
          flexDirection: 'row',
          height: 25,
          justifyContent: 'space-between',
          width: wp(95),
          alignSelf: 'center',
        }}>
        <View
          style={{
            height: 25,
            width: 25,
            borderRadius: 12.5,
            backgroundColor: COLOR.BACKGROUND,
          }}
        />
        <View
          style={{
            height: 25,
            width: 25,
            borderRadius: 12.5,
            backgroundColor: COLOR.BACKGROUND,
          }}
        />
      </View>
      <View>
      {props.props.status == 6 ? null :  <TouchableOpacity
          onPress={() => { props.props.navigation.navigate('PlanNewTrip') }}
          style={[
            Styles.button_content,
            {
              backgroundColor: COLOR.WHITE,
              borderRadius: 10,
              marginTop: 10,
              marginBottom: 20,
              borderWidth: 1,
              borderColor: COLOR.LIGHT_TEXT,
              width: wp(80),
            },
          ]}>
          <Text style={[Styles.button_font, { color: COLOR.BLACK }]}>
            Plan New Trip
          </Text>
        </TouchableOpacity>}
        <View
          style={{
            paddingHorizontal: 10,
            paddingVertical: 10,
            backgroundColor: '#EBEFF2',
            borderRadius: 5,
            flexDirection: 'row',
            alignSelf: 'center',
            alignItems: 'center',
            justifyContent: 'center',
            marginBottom: 20,
          }}>
          <Image
            style={{
              height: 15,
              width: 15,
              marginRight: 10,
            }}
            resizeMode={'contain'}
            source={IMAGES.info}
          />
          <Text
            style={[
              Styles.small_label,
              { width: null, alignSelf: 'flex-start' },
            ]}>
            For more information, visit{' '}
            <TouchableOpacity
            style={{marginBottom:-3}}
            onPress={()=>{
               props.navigation.navigate('PolicyPage',{screenTitle:'Terms and Condition'})
              // navigate('PolicyPage',{screenTitle:'Terms and Condition'})
            }}
            >
            <Text
              style={[
                Styles.small_label,
                { width: null, color: COLOR.GREEN, alignSelf: 'flex-start' },
              ]}>
              Terms and Conditions
            </Text>
            </TouchableOpacity>
          </Text>
        </View>
      </View>
    </View>
  </View>
);
