import React, {useState} from 'react';
import {Platform} from 'react-native';
import {KeyboardAvoidingView} from 'react-native';
import {Modal, Text, View, StyleSheet, Image, TextInput} from 'react-native';
import {TouchableOpacity} from 'react-native';
import {FlatList} from 'react-native-gesture-handler';
import {widthPercentageToDP} from 'react-native-responsive-screen';
import COLOR from '../../styles/Color';
import FONTS from '../../styles/Fonts';
import IMAGES from '../../styles/Images';
import {connect} from 'react-redux';
import Styles from '../../styles/Styles';
import RequestAction from '../../../redux/action/RequestAction';
import PackageAction from '../../../redux/action/PackageAction';
import SimpleToast from 'react-native-simple-toast';
import { DoneButtonKeyboard } from '../../commonView/CommonView';

const reasonArray = [
  'Price is too high',
  'The offer is not as I requested',
  'I changed my plan',
  'Other Reasons',
];

const RejectOfferObject = (props) => {
  const [selected, setSelected] = useState(3);
  const [comment, setComment] = useState('');

  const rejectOffer = () => {
if(comment == ''){
  SimpleToast.show('Please enter your reason details');
  return;
}

    let data = JSON.stringify({
      reject: {
        id: selected,
        comment: comment,
      },
    });
    if (props.isPackage) {
      props.rejectOfferPackage(props.selectedTrip.id, data);
    } else {
      props.rejectOffer(props.selectedTrip.id, data);
    }
    props.onClose();
    setComment('');
    setSelected(3);
  };
  return (
    <View style={Styles.container}>
      <Modal
        animationType={'slide'}
        transparent={true}
        style={{backgroundColor: COLOR.BLACK}}
        visible={props.modalVisible}
        onRequestClose={() => {
          console.log('Modal has been closed.');
        }}>
        <View style={styles.modal}>
          <KeyboardAvoidingView behavior={'padding'} style={styles.modalBody}>
            <TouchableOpacity
              onPress={() => {
                props.onClose();
                setComment('');
                setSelected(3);
              }}
              style={styles.closeButton}>
              <Image
                style={{
                  height: 25,
                  width: 25,
                }}
                source={IMAGES.close}
              />
            </TouchableOpacity>
            <Text style={styles.titleText}>Reject this offer</Text>

            <Text style={[Styles.body_label,{textAlign:'center',marginBottom:20,marginTop:10}]}>Select the reason to reject this offer:</Text>
            <FlatList
              style={{marginVertical: 5}}
              data={reasonArray}
              renderItem={({item, index}) => (
                <View
                  style={{
                    flexDirection: 'row',
                    width: '100%',
                    justifyContent: 'flex-start',
                    // marginTop: 10,
                    marginBottom:15
                  }}>
                  <TouchableOpacity
                    onPress={() => {
                      setSelected(index);
                    }}
                    style={{
                      height: 30,
                      width: 30,
                      marginHorizontal: 10,
                      borderRadius: 5,
                      backgroundColor:
                        selected == index ? COLOR.GREEN : COLOR.WHITE,
                      borderColor: COLOR.GRAY,
                      justifyContent: 'center',
                      alignItems: 'center',
                      borderWidth: 1,
                    }}>
                    <Image
                      resizeMode={'contain'}
                      style={{height: 20, width: 20, alignSelf: 'center'}}
                      source={IMAGES.tick_image}
                    />
                  </TouchableOpacity>
                  <Text
                    style={{
                      fontFamily: FONTS.FAMILY_REGULAR,
                      fontSize: FONTS.REGULAR,
                    }}>
                    {item}
                  </Text>
                </View>
              )}
            />

            <TextInput
            inputAccessoryViewID={'uniqueID'}
              keyboardType="ascii-capable"
              placeholder={'Write your comment'}
              placeholderTextColor={COLOR.LIGHT_TEXT}
              style={[
                {
                  height: 120,
                  padding: 15,
                  paddingTop:15,
                  borderRadius: 10,
                  borderWidth: 1,
                  marginVertical: 5,
                  width: widthPercentageToDP(80),
                  alignSelf: 'center',
                  borderColor: COLOR.GRAY,
                  marginTop: 10,
                },
              ]}
              multiline={true}
              value={comment}
              onChangeText={(text) => setComment(text)}
            />
            <DoneButtonKeyboard />
            <TouchableOpacity
              onPress={() => rejectOffer()}
              style={styles.submit}>
              <Text
                style={{
                  fontSize: 16,
                  color: COLOR.WHITE,
                  fontFamily: FONTS.FAMILY_BOLD,
                }}>
                Submit
              </Text>
            </TouchableOpacity>
          </KeyboardAvoidingView>
        </View>
      </Modal>
    </View>
  );
};

const styles = StyleSheet.create({
  modal: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
  },
  modalBody: {
    alignSelf: 'center',
    backgroundColor: COLOR.WHITE,
    borderWidth: 1,
    width: widthPercentageToDP(90),
    borderColor: COLOR.BORDER,
    borderRadius: 10,
  },
  image: {
    height: 120,
    width: 120,
    alignSelf: 'center',
    marginVertical: 20,
  },
  closeButton: {
    position: 'absolute',
    top: 10,
    left: 10,
  },
  titleText: {
    fontFamily: FONTS.FAMILY_BOLD,
    fontSize: 20,
    marginVertical: 10,
    alignSelf: 'center',
  },
  submit: {
    height: 45,
    width: widthPercentageToDP(80),
    backgroundColor: COLOR.GREEN,
    borderRadius: 10,
    alignSelf: 'center',
    marginVertical: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

const mapDispatchToProps = (dispatch) => {
  return {
    rejectOffer: (val, data) => {
      dispatch(RequestAction.rejectOffer(val, data));
    },
    rejectOfferPackage: (val, data) => {
      dispatch(PackageAction.rejectOfferPackage(val, data));
    },
  };
};

export default connect(null, mapDispatchToProps)(RejectOfferObject);
