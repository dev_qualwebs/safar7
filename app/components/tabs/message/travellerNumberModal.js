import React, {useState} from 'react';
import {ImageBackground, Platform} from 'react-native';
import {KeyboardAvoidingView} from 'react-native';
import {Modal, Text, View, StyleSheet, Image, TextInput} from 'react-native';
import {TouchableOpacity} from 'react-native';
import {FlatList} from 'react-native-gesture-handler';
import {widthPercentageToDP} from 'react-native-responsive-screen';
import COLOR from '../../styles/Color';
import FONTS from '../../styles/Fonts';
import IMAGES from '../../styles/Images';
import {connect} from 'react-redux';
import Styles from '../../styles/Styles';
import RequestAction from '../../../redux/action/RequestAction';
import PackageAction from '../../../redux/action/PackageAction';

const TravellerNumberModal = (props) => {
  const goBack = () => {
    props.hideModal();
  };

  return (
    <View style={Styles.container}>
      <Modal
        animationType={'slide'}
        transparent={true}
        style={{backgroundColor: COLOR.BLACK}}
        visible={props.handleVisible}
        onRequestClose={() => {
          console.log('Modal has been closed.');
        }}>
        <View style={styles.modal}>
          <KeyboardAvoidingView behavior={'padding'} style={styles.modalBody}>
            <View style={{alignItems: 'center', marginTop: 20}}>
              <ImageBackground
                resizeMode={'contain'}
                style={{
                  height: 140,
                  width: 140,
                  alignItems: 'center',
                  justifyContent: 'center',
                }}
                source={IMAGES.alertbackground}>
                <Image
                  resizeMode={'contain'}
                  source={IMAGES.alert}
                  style={{height: 100, width: 100, marginTop: 15}}
                />
              </ImageBackground>

              <Text
                style={[
                  Styles.subheading_label,
                  {textAlign: 'center', marginTop: 20},
                ]}>
                Number of Travellers!
              </Text>

              <Text
                style={[Styles.body_label, {textAlign: 'center', padding: 15}]}>
                The number of travellers is not the same number for the package,
                which may affect the final price for your request.
              </Text>
            </View>

            <TouchableOpacity
              onPress={props.hideModal}
              style={[styles.submit, {textAlign: 'center'}]}>
              <Text
                style={{
                  fontSize: 16,
                  color: COLOR.WHITE,
                  fontFamily: FONTS.FAMILY_BOLD,
                }}>
                I Will Change Travellers
              </Text>
            </TouchableOpacity>

            <TouchableOpacity onPress={props.hideModal} style={[{}]}>
              <Text
                style={{
                  fontSize: 14,
                  marginBottom: 20,
                  fontFamily: FONTS.FAMILY_REGULAR,
                  textAlign: 'center',
                }}>
                Continue with these travellers
              </Text>
            </TouchableOpacity>
          </KeyboardAvoidingView>
        </View>
      </Modal>
    </View>
  );
};

const styles = StyleSheet.create({
  modal: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
  },
  modalBody: {
    alignSelf: 'center',
    backgroundColor: COLOR.WHITE,
    borderWidth: 1,
    width: widthPercentageToDP(90),
    borderColor: COLOR.BORDER,
    borderRadius: 10,
  },
  image: {
    height: 120,
    width: 120,
    alignSelf: 'center',
    marginVertical: 20,
  },
  closeButton: {
    position: 'absolute',
    top: 10,
    left: 10,
  },
  titleText: {
    fontFamily: FONTS.FAMILY_BOLD,
    fontSize: 20,
    marginVertical: 10,
    alignSelf: 'center',
  },
  submit: {
    height: 45,
    width: widthPercentageToDP(80),
    backgroundColor: COLOR.GREEN,
    borderRadius: 10,
    alignSelf: 'center',
    marginVertical: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
});


export default TravellerNumberModal;
