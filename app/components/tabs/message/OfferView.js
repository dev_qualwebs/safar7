import React, {useState} from 'react';
import {View, Image, Text} from 'react-native';
import {TouchableOpacity, StyleSheet} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import COLOR from '../../styles/Color';
import FONTS from '../../styles/Fonts';
import IMAGES from '../../styles/Images';
import Styles from '../../styles/Styles';
import {SemicircleDashView} from '../home/trip/NoTripView';
import moment from 'moment';
import {useEffect} from 'react';
import {useNavigation} from '@react-navigation/native';

const OfferView = ({item, onReject, isPackage,isPastTrip}) => {
  const [remainingTime, setRemainingTime] = useState();
  const navigation = useNavigation();

  const getIntervalText = () => {
    var eventTime = moment(item.offered_time).add(1, 'days').valueOf();
    var currentTime = moment().valueOf();
    var diffTime = eventTime - currentTime;
    var duration = moment.duration(diffTime, 'milliseconds');
    var interval = 1000;

    setInterval(() => {
      duration = moment.duration(duration - interval, 'milliseconds');
      if (duration - interval < 0) {
        setRemainingTime('00:00:00');
      } else {
        setRemainingTime(
          duration.hours() +
            ':' +
            (duration.minutes().toString().length == 1
              ? '0' + duration.minutes()
              : duration.minutes()) +
            ':' +
            (duration.seconds().toString().length == 1
              ? '0' + duration.seconds()
              : duration.seconds()),
        );
      }
    }, 1000);
  };

  useEffect(() => {
    getIntervalText();
  }, []);
  return (
    <View>
      <View style={styles.container}>
        <View style={styles.mainView}>
          <View style={styles.tripNameView}>
            <Text
              style={[
                Styles.subheading_label,
                {
                  width: null,
                  alignSelf: 'flex-start',
                  marginVertical: 10,
                  fontFamily: FONTS.FAMILY_BOLD,
                },
              ]}>
              {isPackage ? item.package.package_name : item.trip_name}
            </Text>
          </View>
          <View style={styles.dateView}>
            {isPackage ? (
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-evenly',
                  width: '100%',
                }}>
                <Text
                  style={[
                    Styles.small_label,
                    {
                      width: null,
                      fontFamily: FONTS.FAMILY_SEMIBOLD,
                      textAlign: 'left',
                      color: COLOR.BLACK,
                    },
                  ]}>
                  {item.start_date}
                </Text>
                <Image
                  source={IMAGES.dot_back}
                  style={{height: 25, width: 25}}
                  resizeMode={'cover'}
                />

                <Text
                  style={[
                    Styles.small_label,
                    {
                      width: null,
                      fontFamily: FONTS.FAMILY_SEMIBOLD,
                      textAlign: 'left',
                      color: COLOR.BLACK,
                    },
                  ]}>
                  {item.end_date}
                </Text>
              </View>
            ) : (
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-evenly',
                  width: '100%',
                }}>
                <Text
                  style={[
                    Styles.small_label,
                    {
                      width: null,
                      fontFamily: FONTS.FAMILY_SEMIBOLD,
                      textAlign: 'left',
                      color: COLOR.BLACK,
                    },
                  ]}>
                  {item.trip_date_type.flexibility.depart}
                </Text>
                <Image
                  source={IMAGES.dot_back}
                  style={{height: 25, width: 25}}
                  resizeMode={'cover'}
                />

                <Text
                  style={[
                    Styles.small_label,
                    {
                      width: null,
                      fontFamily: FONTS.FAMILY_SEMIBOLD,
                      textAlign: 'left',
                      color: COLOR.BLACK,
                    },
                  ]}>
                  {item.trip_date_type.flexibility.return}
                </Text>
              </View>
            )}
          </View>
          <SemicircleDashView />
          <View style={styles.offerBackground}>
            <View style={[styles.offerTimeView,{justifyContent: isPastTrip || item.offer_payment == 1 ? 'center' : 'space-between'}]}>
              <Text style={styles.timeText}>
                {item.active_offers[0].total_amount +
                  ' ' +
                  item.currency.iso4217_code}
              </Text>
            {isPastTrip ? null : item.offer_payment == 1 ? null : <Text style={styles.timeText}>{remainingTime}</Text>}
            </View>
            <View style={styles.showDetailsView}>
              <TouchableOpacity
                onPress={() =>
                  isPackage
                    ? navigation.navigate('PackagePayment', {id: item.id})
                    : navigation.navigate('PaymentSecond', {id: item.id})
                }>
                <Text style={styles.showDetailsText}>
                  Show Details of the Offer
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        {isPastTrip ? null : item.offer_payment == 1 ? null : <TouchableOpacity onPress={onReject}>
            <Text style={styles.rejectOfferText}>Reject the Offer</Text>
          </TouchableOpacity> }
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  topBar: {
    height: 50,
    shadowColor: COLOR.VOILET,
    shadowOpacity: 0.2,
    backgroundColor: COLOR.WHITE,
    flexDirection: 'row',
    shadowOffset: {width: 2, height: 2},
    justifyContent: 'space-between',
    width: wp(95),
    alignSelf: 'center',
    marginVertical: 20,
    elevation: 3,
    alignItems: 'center',
  },
  statusText: {
    flex: 1,
    textAlign: 'center',
    fontSize: 10,
    marginHorizontal: 2,
  },
  detailButton: {
    alignItems: 'center',
    justifyContent: 'center',
    height: 50,
    borderRadius: 10,
    marginTop: 0,
    marginBottom: 10,
    borderColor: COLOR.LIGHT_TEXT,
    borderWidth: 1,
    width: widthPercentageToDP(80),
    backgroundColor: COLOR.WHITE,
  },
  statusView: {
    flex: 1,
    marginVertical: 10,
    alignSelf: 'center',
    flexDirection: 'row',
    alignItems: 'center',
  },
  hr: {
    height: 3,
    backgroundColor: COLOR.GRAY,
    flex: 1,
    alignSelf: 'center',
  },
  container: {
    width: widthPercentageToDP(90),
    alignSelf: 'center',
    marginVertical: 20,

    borderRadius: 20,
    shadowColor: COLOR.VOILET,
    elevation: 3,
    shadowOpacity: 0.2,
    shadowOffset: {width: 2, height: 2},
    backgroundColor: COLOR.WHITE,
  },
  dateView: {
    flexDirection: 'row',
    height: 45,
    justifyContent: 'space-between',
    width: wp(80),
    alignItems: 'center',
    marginBottom: 5,
    borderRadius: 10,
    borderWidth: 2,
    borderColor: COLOR.GRAY,
    paddingHorizontal: 10,
  },
  offerBackground: {
    backgroundColor: COLOR.GREEN_OPACITY,
    borderRadius: 8,
    width: '90%',
    marginBottom: 15,
  },
  offerTimeView: {
    backgroundColor: COLOR.GREEN,
    borderRadius: 8,
    paddingHorizontal: 15,
    paddingVertical: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  timeText: {
    fontFamily: FONTS.FAMILY_SEMIBOLD,
    color: COLOR.WHITE,
    fontSize: 16,
  },
  mainView: {
    flex: 1,
    alignItems: 'center',
    paddingHorizontal: 10,
  },
  tripNameView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: wp(80),
  },
  rejectOfferText: {
    color: COLOR.RED,
    fontSize: 14,
    fontFamily: FONTS.FAMILY_REGULAR,
    marginBottom: 15,
  },
  showDetailsText: {
    color: COLOR.GREEN,
    fontFamily: FONTS.FAMILY_SEMIBOLD,
    fontSize: 16,
  },
  showDetailsView: {
    paddingHorizontal: 15,
    paddingVertical: 8,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default OfferView;
