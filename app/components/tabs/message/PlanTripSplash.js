import React, {Component} from 'react';
import {
  StatusBar,
  Text,
  View,
  Image,
  SafeAreaView,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import {Pages} from 'react-native-pages';
import COLOR from '../../styles/Color';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import FONTS from '../../styles/Fonts';
import Styles from '../../styles/Styles';
import IMAGES from '../../styles/Images';
import Toast from 'react-native-simple-toast';
import TripDateView from '../../commonView/TripDateView';

Platform.select({
  ios: () => StatusBar.setBarStyle('light-content'),
  android: () => StatusBar.setBackgroundColor('#263238'),
})();

export default class PlanTripSplash extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      index: 0,
      tripPlan: false,
      flight: false,
      train: false,
      car: false,
      cruise: false,
      stay: false,
      transfer: false,
      stack: [],
      
    };

    this.updateRef = this.updateRef.bind(this);
  }

  componentDidMount(){
   
  }

  updateRef(ref) {
    this._pages = ref;
  }

  addToStack(name) {
    const stack = this.state.stack;
    stack.push(name);
    this.setState({
      stack: stack,
    });
    console.log(stack);
  }

  removeFromStack(name) {
    const stack = this.state.stack;
    const index = stack.indexOf(name);
    stack.splice(index, 1);
    this.setState({
      stack: stack,
    });
    console.log(stack);
  }

  changePage = () => {
    // console.log('reference is:-', index);
    const {index} = this.state;
    if (this._pages) {
      this.setState(
        {
          index: index + 1,
        },
        () => {
          if (index == 6 || index > 6) {
            if (this.state.stack.length > 0) {
              this.props.navigation.replace(this.state.stack[0], {
                index: 0,
                screens: this.state.stack,
                id: this.props.route.params.id,
                depart: this.props.route.params.depart,
                return: this.props.route.params.return,
                departFlexibility: this.props.route.params.departFlexibility,
                returnFlexibility: this.props.route.params.returnFlexibility,
              });
            } else {
              Toast.show('Please select atleast on option');
            }
          } else {
            this._pages.scrollToPage(this.state.index);
          }
        },
      );
    }
  };

  render() {
    return (
      <SafeAreaView style={[Styles.container]}>
        <View
          style={[
            Styles.mainView,
            {
              height: 50,
              justifyContent: 'space-between',
              alignSelf: 'center',
              alignItems: 'center',
              flexDirection: 'row',
              width: wp(98),
            },
          ]}>
          <TouchableOpacity
            style={{
              height: 50,
              width: 45,
              justifyContent: 'center',
              alignItems: 'center',
            }}
            onPress={() => this.props.navigation.goBack()}>
            <Image
              style={{
                height: 15,
                width: 15,
              }}
              source={IMAGES.back_arrow}
            />
          </TouchableOpacity>
          <View>
            <Text
              style={[
                Styles.subheading_label,
                {
                  textAlign: 'center',
                  color: COLOR.BLACK,
                  fontSize: 20,
                  fontFamily: FONTS.FAMILY_BOLD,
                  width: null,
                  flex: 1,
                  marginTop: 15,
                },
              ]}>
              Plan a New Trip
            </Text>
          </View>
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('HelpTrip')}>
            <View
              style={{
                alignItems: 'center',
                justifyContent: 'center',
                alignSelf: 'center',
                height: 50,
                width: 45,
              }}>
              <Image
                source={IMAGES.help}
                resizeMode={'contain'}
                style={{
                  width: 30,
                  height: 30,
                }}
              />
            </View>
          </TouchableOpacity>
        </View>
        <TripDateView
          departDate={this.props.route.params.depart}
          returnDate={this.props.route.params.return}
          departFlexibility={this.props.route.params.departFlexibility}
          returnFlexibility={this.props.route.params.returnFlexibility}
        />

        <View
          style={{
            marginTop: 10,
            flex: 1,
            backgroundColor: COLOR.BACKGROUND,
          }}>
          <Text
            style={[
              Styles.small_label,
              {
                textAlign: 'center',
                color: COLOR.BLACK,
                fontFamily: FONTS.FAMILY_BOLD,
                marginVertical: 20,
              },
            ]}>
            CHOOSE ONE OR MORE
          </Text>
          <View style={{marginBottom: 0, flex: 1}}>
            <Pages
              onScrollEnd={(index) => this.setState({index})}
              ref={this.updateRef}
              indicatorColor={COLOR.GREEN}
              indicatorOpacity={0.3}>
                
            <Splash
                onPress={() => {
                  this.setState({flight: !this.state.flight});
                  !this.state.flight
                    ? this.addToStack('BookFlight')
                    : this.removeFromStack('BookFlight');
                }}
                selected={this.state.flight}
                detail={'Book your best flight with us\nand get most benefits'}
                name={'Flight'}
                image={IMAGES.plan_blue}
              />  

              <Splash
                onPress={() => {
                  this.setState({stay: !this.state.stay});
                  !this.state.stay
                    ? this.addToStack('StayScreen')
                    : this.removeFromStack('StayScreen');
                }}
                selected={this.state.stay}
                detail={'Book your best stay with us\nand get most benefits'}
                name={'Stay'}
                image={IMAGES.building_blue}
              />
              <Splash
                onPress={() => {
                  this.setState({train: !this.state.train});
                  !this.state.train
                    ? this.addToStack('TrainScreen')
                    : this.removeFromStack('TrainScreen');
                }}
                selected={this.state.train}
                detail={'Book your best ride with us\nand get most benefits'}
                name={'Train'}
                image={IMAGES.train_blue}
              />
              <Splash
                onPress={() => {
                  this.setState({car: !this.state.car});
                  !this.state.car
                    ? this.addToStack('CarScreen')
                    : this.removeFromStack('CarScreen');
                }}
                selected={this.state.car}
                detail={'Book your best cruise with us\nand get most benefits'}
                name={'Car'}
                image={IMAGES.car_blue}
              />
              <Splash
                onPress={() => {
                  this.setState({cruise: !this.state.cruise});
                  !this.state.cruise
                    ? this.addToStack('CruiseScreen')
                    : this.removeFromStack('CruiseScreen');
                }}
                selected={this.state.cruise}
                detail={
                  'Book your best transfer with us\nand get most benefits'
                }
                name={'Cruise'}
                image={IMAGES.cruise_blue}
              />
              <Splash
                detail={
                  'Book your best transfer with us\nand get most benefits'
                }
                onPress={() => {
                  this.setState({transfer: !this.state.transfer});
                  !this.state.transfer
                    ? this.addToStack('TransferScreen')
                    : this.removeFromStack('TransferScreen');
                }}
                selected={this.state.transfer}
                name={'Transfer'}
                image={IMAGES.bus_blue}
              />
              <Splash
                onPress={() => {
                  this.setState({tripPlan: !this.state.tripPlan});
                  !this.state.tripPlan
                    ? this.addToStack('TripPlanScreen')
                    : this.removeFromStack('TripPlanScreen');
                }}
                selected={this.state.tripPlan}
                detail={'Arrange the best plan for your trip.'}
                name={'Trip Plan'}
                image={IMAGES.map_blue}
              /> 
            </Pages>
          </View>
        </View>
        <View
          style={{
            alignItems: 'center',
            height: 80,
            justifyContent: 'center',
            backgroundColor: COLOR.BACKGROUND,
          }}>
          <TouchableOpacity
            style={{
              alignSelf: 'center',
              backgroundColor: COLOR.BACKGROUND,
            }}
            onPress={this.changePage}>
            <View style={styles.bottomCircle}>
              <Text style={Styles.button_font}>Next</Text>
            </View>
          </TouchableOpacity>
        </View>
      </SafeAreaView>
    );
  }
}

const Splash = (props) => {
  return (
    <TouchableOpacity
      onPress={props.onPress}
      style={{
        marginVertical: 0,
        flex: 1,
        alignItems: 'center',
      }}>
      <View
        style={{
          marginVertical: 0,
          width: wp(80),
          borderWidth: 2,
          borderColor: props.selected ? COLOR.GREEN : 'transparent',
          borderRadius: 10,
          shadowOpacity: 0.2,
          shadowColor: COLOR.VOILET,
          shadowOffset: {width: 2, height: 2},
          backgroundColor: COLOR.WHITE,
          elevation: 3,
          alignSelf: 'center',
        }}>
        <Image style={styles.image} source={props.image} />
        <Text
          style={[
            Styles.splash_heading_label,
            {textAlign: 'center', marginTop: 20, width: null},
          ]}>
          {props.name}
        </Text>
        <Text
          style={[
            Styles.body_label,
            {
              color: COLOR.VOILET,
              marginTop: 10,
              width: wp(80),
              textAlign: 'center',
              marginBottom: 20,
            },
          ]}>
          {props.detail}
        </Text>
      </View>
      {props.selected ? (
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
            alignSelf: 'center',
            marginTop: 10,
          }}>
          <View
            style={{
              width: 30,
              height: 30,
              marginRight: 10,
              backgroundColor: COLOR.GREEN,
              borderRadius: 20,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Image
              style={{
                width: 15,
                height: 15,
              }}
              source={require('../../styles/assets/checked_right.png')}
              resizeMode={'contain'}
            />
          </View>

          <Text
            style={{
              fontSize: 16,
              color: COLOR.GREEN,
            }}>
            Checked
          </Text>
        </View>
      ) : null}
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  bottomCircle: {
    width: wp('90%'),
    borderRadius: 10,
    height: 50,
    backgroundColor: COLOR.GREEN,
    opacity: 1,
    shadowRadius: 0.3,
    alignItems: 'center',
    justifyContent: 'center',
  },
  circleImage: {
    alignSelf: 'center',
    width: 25,
    height: 25,
    resizeMode: 'contain',
  },
  image: {
    alignSelf: 'center',
    width: '100%',
    height: hp(32),
    borderRadius: 10,
    overflow: 'hidden',
  },
});
