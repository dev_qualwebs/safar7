import React from 'react';
import {Modal, Text, View, StyleSheet, Image} from 'react-native';
import {TouchableOpacity} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import COLOR from '../../../components/styles/Color';
import FONTS from '../../../components/styles/Fonts';
import IMAGES from '../../../components/styles/Images';
import Styles from '../../../components/styles/Styles';
import NavigationBar from '../../commonView/NavigationBar';
import {FloatingTitleTextInputField} from '../../../helper/FloatingTitleTextInputField';

export default class NoticeScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: true,
    };
  }

  updateMasterState = (attrName, value) => {
    this.setState({[attrName]: value});
  };

  TextFieldView = (props) => (
    <View style={{marginVertical: 5}}>
      <FloatingTitleTextInputField
        attrName={props.attributeName}
        title={props.title}
        value={this.state.countryCode}
        updateMasterState={this.updateMasterState}
        textInputStyles={{
          color: COLOR.BLACK,
          fontSize: 15,
        }}
        otherTextInputProps={{
          maxLength: 12,
        }}
      />
    </View>
  );

  dismissPopup = () => {
    this.setState(
      {
        modalVisible: false,
      },
      (val) => {
        this.props.navigation.navigate('PlanTripSplash');
      },
    );
  };

  SingleLineView = () => (
    <View
      style={{
        borderBottomColor: COLOR.GRAY,
        marginVertical: 5,
        borderBottomWidth: 3,
      }}
    />
  );

  render() {
    return (
      <View style={Styles.container}>
        <Modal
          animationType={'slide'}
          transparent={true}
          style={{backgroundColor: COLOR.BLACK}}
          visible={this.state.modalVisible}
          onRequestClose={() => {
            console.log('Modal has been closed.');
          }}>
          <View style={styles.modal}>
            <View style={styles.modalBody}>
              <NavigationBar prop={this.props} navHeight={45} name={'Notice'} />
              <View style={{flex: 1, height: 300}}>
                <Image
                  resizeMode={'cover'}
                  style={{
                    marginVertical: 20,
                    width: 120,
                    height: 120,
                    alignSelf: 'center',
                  }}
                  source={IMAGES.alert_image}
                />
                <Text
                  style={[
                    Styles.body_label,
                    {width: wp(90), textAlign: 'center'},
                  ]}>
                  We are sorry, your request had not met{'\n'}our criteria in
                  our Terms and Codition.
                </Text>
                <View
                  style={{
                    width: wp(90),
                    borderColor: COLOR.GRAY,
                    borderWidth: 0,
                    borderRadius: 10,
                    marginVertical: 20,
                  }}>
                  <Text
                    style={[
                      Styles.body_label,
                      {
                        width: wp(90),
                        textAlign: 'center',
                        paddingVertical: 10,
                        paddingHorizontal: 20,
                      },
                    ]}>
                    Your request is rejected due to one of{'\n'}these reason:
                    {'\n '} ● bla bla {'\n '} ● bla bla
                  </Text>
                </View>
                <Text
                  style={[
                    Styles.small_label,

                    {
                      width: wp(70),
                      textAlign: 'center',
                      marginVertical: 20,
                      fontSize: 14,
                    },
                  ]}>
                  We would be happy to assist you more if you contact us at
                  <Text style={{color: COLOR.GREEN}}>info@email.com</Text>
                </Text>
                <Text
                  style={[
                    Styles.small_label,
                    {width: wp(70), textAlign: 'center', fontSize: 14},
                  ]}>
                  According to our{' '}
                  <TouchableOpacity
                  onPress={()=>{
                    this.props.navigation.navigate('PolicyPage', {
                      screenTitle: 'Terms and Condition',
                    });
                  }}
                  >
                  <Text style={{color: COLOR.GREEN}}>Terms and Condition</Text>
                  </TouchableOpacity>
                  {' '}
                  you will receive the refunds within 14 days.
                </Text>
              </View>
              <TouchableOpacity
                onPress={this.dismissPopup}
                style={{
                  alignItems: 'center',
                  justifyContent: 'center',
                  height: 50,
                  borderRadius: 10,
                  marginVertical: 20,
                  borderColor: COLOR.LIGHT_TEXT,
                  borderWidth: 1,
                  width: wp(90),
                  backgroundColor: COLOR.WHITE,
                }}>
                <Text style={[Styles.button_font, {color: COLOR.VOILET}]}>
                  Ok
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  modal: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
  },
  modalBody: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    backgroundColor: COLOR.WHITE,
    borderWidth: 1,
    width: wp(100),
    marginTop: 50,
    borderColor: COLOR.BORDER,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    flex: 1,
  },
  image: {
    height: 120,
    width: 120,
    alignSelf: 'center',
    marginVertical: 20,
  },
  headingLabel: {
    fontFamily: FONTS.FAMILY_BOLD,
    fontSize: FONTS.MEDIUM,
    color: COLOR.BLACK,
  },
  singleLineView: {
    marginVertical: 20,
    borderBottomColor: COLOR.GRAY,
    width: wp(100),
    alignSelf: 'center',
    borderBottomWidth: 1,
  },
  borderView: {
    borderRadius: 10,
    borderWidth: 3,
    width: wp(90),
    borderColor: COLOR.GRAY,
    alignSelf: 'center',
  },
});
