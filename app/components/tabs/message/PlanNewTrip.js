import React, {createRef} from 'react';
import {View, Text, SafeAreaView, Image, StyleSheet, Alert, TouchableWithoutFeedback} from 'react-native';
import {TextInput, TouchableOpacity} from 'react-native';
import IMAGES from '../../styles/Images';
import Styles from '../../styles/Styles';
import {
  widthPercentageToDP,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import COLOR from '../../styles/Color';
import NavigationBar from '../../commonView/NavigationBar';
import FONTS from '../../styles/Fonts';
import SelectDate from '../calendar/SelectDate';
import RequestAction from '../../../redux/action/RequestAction';
import {connect} from 'react-redux';
import ActivityIndicator from '../../commonView/ActivityIndicator';
import NonSpecificDateSelector from './NonSpecificDateSelector';
import SimpleToast from 'react-native-simple-toast';
import {validateAlphabet} from '../../commonView/Helpers';
import {Popable} from 'react-native-popable';
import { DoneButtonKeyboard } from '../../commonView/CommonView';

class PlanNewTrip extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      trip_name: null,
      budget: null,
      selectedDateIndex: null,
      checkInDate: null,
      checkOutDate: null,
      fromFlexibility: 0,
      toFlexibility: 0,
      openCalendar: false,
      nonSpecificDates: false,
      monthPreference: null,
      days: null,
      loading: false,
      toolTipVisible: false,
      isEdit:false,
      editData:null
    };

    this.imageRef = createRef();
    this.viewRef = createRef();
    this.amountRef = React.createRef();
  }

  componentDidUpdate(prevProps) {
    if (
      JSON.stringify(this.props.requestTripData) !=
      JSON.stringify(prevProps.requestTripData)
    ) {
      let data = this.props.requestTripData;
      this.setState({
        loading: false,
      });
      this.props.navigation.navigate('PlanTripSplash', {
        id: data.id,
        departFlexibility:
          data.trip_date_type.flexibility.depart_flexibility_id,
        returnFlexibility:
          data.trip_date_type.flexibility.return_flexibility_id,
        depart: data.trip_date_type.flexibility.depart,
        return: data.trip_date_type.flexibility.return,
      });
    }
  }

  requestTrip = () => {
    if (this.state.trip_name == '' || this.state.trip_name == null) {
      SimpleToast.show('Enter Trip Name');
    } else if (!validateAlphabet(this.state.trip_name)) {
      SimpleToast.show('Enter valid trip name');
      return;
    } else if (this.state.budget == '' || this.state.budget == null) {
      SimpleToast.show('Enter Budget');
      return false;
    } else if (
      this.state.selectedDateIndex == '' ||
      this.state.selectedDateIndex == null
    ) {
      SimpleToast.show('Select date type');
      return false;
    } else if (
      this.state.days == '' ||
      (this.state.days == null && this.state.selectedDateIndex == 2)
    ) {
      SimpleToast.show('Select days');
      return false;
    } else if (
      this.state.checkInDate == 'Invalid date' ||
      this.state.checkInDate == '' ||
      (this.state.checkInDate == null && this.state.selectedDateIndex == 1)
    ) {
      SimpleToast.show('Select start date');
      return false;
    } else if (
      this.state.checkOutDate == 'Invalid date' ||
      this.state.checkOutDate == '' ||
      (this.state.checkOutDate == null && this.state.selectedDateIndex == 1)
    ) {
      SimpleToast.show('Select end date');
      return false;
    } else {
      const data = {
        trip_type: 1,
        trip_name: this.state.trip_name,
        budget: this.state.budget,
        currency_id: 141,
        selected_date_type_id: this.state.selectedDateIndex,
        depart: this.state.checkInDate,
        return: this.state.checkOutDate,
        depart_flexibility_id: this.state.fromFlexibility,
        return_flexibility_id: this.state.toFlexibility,
        within_month_preference: this.state.monthPreference,
        days: this.state.days,
      };
      // this.setState({
      //   loading: true,
      // });
      this.props.requestTrip(data);
    }
  };

  setNonSpecificVisibility = (val) => {};

  updateMasterState = (attrName, value) => {
    this.setState({[attrName]: value});
  };

  render() {
    return (
      <SafeAreaView style={Styles.container}>
        {this.state.openCalendar ? (
          <SelectDate
            updateMasterState={this.updateMasterState}
            props={this.props}
            minDate={Date()}
            maxDate={new Date(5147962581000)}
            allowRangeSelection={true}
            modal={true}
          />
        ) : (
          <View style={Styles.container}>
            <NonSpecificDateSelector
              updateMasterState={this.updateMasterState.bind(this)}
              visibility={this.state.nonSpecificDates}
            />
            <NavigationBar
            dontshowTitleAboveImage={true}
              right_image={IMAGES.help}
              rightImageWidth={25}
              rightImageHeight={25}
              prop={this.props}
              navHeight={45}
              name={'Plan a New Trip'}
            />
            <View style={{alignSelf: 'center', width: wp(90), flex: 1}}>
              <View>
                <Text
                  style={[
                    styles.headingLabel,
                    {
                      width: wp(90),
                      alignSelf: 'center',
                      marginTop: 20,
                      marginBottom: 5,
                    },
                  ]}>
                  Trip Name
                </Text>
                <TextInput
                inputAccessoryViewID={'uniqueID'}
                  value={this.state.trip_name}
                  onChangeText={(text) => {
                    this.setState({
                      trip_name: text,
                    });
                  }}
                  style={styles.body_label}></TextInput>
                  <DoneButtonKeyboard />
              </View>
              <View
                style={[
                  styles.singleLineView,
                  {width: wp(90), alignSelf: 'center'},
                ]}
              />
              <View>
                <View
                  ref={(target) => {
                    this.viewRef = target;
                  }}
                  style={{flexDirection: 'row',alignItems:'center'}}>
                  <Text
                    style={[
                      styles.headingLabel,
                      {
                        width: null,
                        alignSelf: 'center',
                        marginBottom: 0,
                        marginRight: 5,
                      },
                    ]}>
                    Average Budget
                  </Text>

                  <Popable
                  backgroundColor='#273238'
                    style={{color: COLOR.WHITE,fontSize:12, width: widthPercentageToDP(60)}}
                    content={
                      ' The budget including any reservations in your trip, including flights, hotels, cruise, cars, and any attractions that needs pre-arrival booking.'
                    }>
                    <Image
                      ref={(target) => {
                        this.imageRef = target;
                      }}
                      source={IMAGES.info}
                      style={{height: 12, width: 12}}
                    />
                  </Popable>
                </View>
                <TouchableWithoutFeedback 
                onPress={()=>{
                this.amountRef.focus()
                }}
                >
                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                  <TextInput
                  ref={(ref)=> this.amountRef = ref}
                  inputAccessoryViewID={'uniqueID'}
                    value={this.state.budget}
                    placeholder={'0'}
                    placeholderTextColor={COLOR.LIGHT_TEXT_COLOR}
                    onChangeText={(text) =>{
                      this.setState({
                        budget: text,
                      })
                    }}
                    keyboardType={'number-pad'}
                    returnKeyType={'done'}
                    style={{
                      fontFamily: FONTS.FAMILY_REGULAR,
                      fontSize: 16,
                      paddingTop: 8,
                      marginRight:5
                      // width: widthPercentageToDP(80),
                    }}></TextInput>
                  <Text
                    style={{
                      fontFamily: FONTS.FAMILY_REGULAR,
                      fontSize: 16,
                      marginBottom:-8
                    }}>
                    SAR
                  </Text>
                </View>
                </TouchableWithoutFeedback>
              </View>
              <View
                style={[
                  styles.singleLineView,
                  {width: wp(90), alignSelf: 'center'},
                ]}
              />
              <View
                style={{
                  justifyContent: 'space-between',
                  flexDirection: 'row',
                  alignItems: 'center',
                  marginTop: 20,
                }}>
                <View
                  style={[
                    styles.selectionView,
                    {
                      marginRight: 10,
                      borderWidth: 1,
                      borderColor:
                        this.state.selectedDateIndex == 2
                          ? COLOR.GREEN
                          : 'transparent',
                          backgroundColor : this.state.selectedDateIndex == 2
                          ? '#ECFCFF'
                          : COLOR.WHITE,
                    },
                  ]}>
                  <TouchableOpacity
                    onPress={() =>
                      this.setState({
                        selectedDateIndex: 2,
                        nonSpecificDates: true,
                      })
                    }
                    style={{
                      flex: 1,
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <View
                      style={{
                        width: 60,
                        height: 60,
                        alignItems: 'center',
                        justifyContent: 'center',
                        borderRadius: 10,
                        backgroundColor: COLOR.GRAY,
                        marginBottom: 10,
                        paddingTop: 10,
                      }}>
                      <Image
                        style={[styles.calendarImage]}
                        source={this.state.selectedDateIndex == 2 ? IMAGES.calendar_arrow : IMAGES.calendar_grey_arrow}
                        resizeMode={'contain'}
                      />
                    </View>
                    <Text>Non-Specific Date</Text>
                  </TouchableOpacity>
                </View>

                <View
                  style={[
                    styles.selectionView,
                    {
                      borderWidth: 1,
                      borderColor:
                        this.state.selectedDateIndex == 1
                          ? COLOR.GREEN
                          : 'transparent',
                          backgroundColor : this.state.selectedDateIndex == 1
                          ? '#ECFCFF'
                          : COLOR.WHITE,
                    },
                  ]}>
                  <TouchableOpacity
                    onPress={() =>
                      this.setState({
                        selectedDateIndex: 1,
                        openCalendar: true,
                      })
                    }
                    style={{
                      flex: 1,
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <View
                      style={{
                        width: 60,
                        height: 60,
                        alignItems: 'center',
                        justifyContent: 'center',
                        borderRadius: 10,
                        backgroundColor: COLOR.GRAY,
                        marginBottom: 10,
                        paddingTop: 10,
                      }}>
                      <Image
                        style={[styles.calendarImage]}
                        source={this.state.selectedDateIndex == 1 ? IMAGES.calendar_tick : IMAGES.calendar_grey_tick}
                        resizeMode={'contain'}
                      />
                    </View>
                    <Text>Specific Date</Text>
                  </TouchableOpacity>
                </View>
              </View>
              <TouchableOpacity
                onPress={() => {
                  this.requestTrip();
                }}
                style={[
                  Styles.button_content,
                  {bottom: 20, position: 'absolute'},
                ]}>
                <Text style={Styles.button_font}>Next</Text>
              </TouchableOpacity>
            </View>
          </View>
        )}
        <ActivityIndicator loading={this.state.loading} />
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  headingLabel: {
    fontFamily: FONTS.FAMILY_REGULAR,
    fontSize: FONTS.SMALL,
    color: COLOR.VOILET,
  },
  singleLineView: {
    marginVertical: 15,
    borderBottomColor: COLOR.GRAY,
    width: wp(100),
    alignSelf: 'center',
    borderBottomWidth: 1,
  },
  selectionView: {
    borderColor: COLOR.GRAY,
    borderWidth: 1,
    borderRadius: 10,
    shadowColor: COLOR.BLACK,
    shadowOpacity: 0.2,
    shadowOffset: {width: 0.2, height: 0.2},
    marginBottom: 20,
    width: wp(43),
    height: 140,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: COLOR.WHITE,
    elevation: 3,
  },
  calendarImage: {
    height: 40,
    aspectRatio: 1,
    marginBottom: 10,
    overflow: 'hidden',
  },
  body_label: {
    fontFamily: FONTS.FAMILY_REGULAR,
    fontSize: FONTS.REGULAR,
    width: wp(90),
    alignSelf: 'center',
    color: COLOR.TEXT_COLOR,
    padding: 0,
  },
});

mapStateToProps = (state, ownProps) => {
  return {
    requestTripData: state.requestReducer.requestTrip,
  };
};

mapDispatchToProps = (dispatch) => {
  return {
    requestTrip: (data) => {
      dispatch(RequestAction.requestTrip(data));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(PlanNewTrip);
