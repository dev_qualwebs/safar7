import {useNavigation} from '@react-navigation/core';
import React, {useEffect} from 'react';
import {View, Image, Text, StyleSheet, TouchableOpacity} from 'react-native';
import {FlatList} from 'react-native';
import {TouchableWithoutFeedback} from 'react-native-gesture-handler';
import COLOR from '../../styles/Color';
import FONTS from '../../styles/Fonts';
import IMAGES from '../../styles/Images';
import Styles from '../../styles/Styles';
import {widthPercentageToDP} from 'react-native-responsive-screen';
import SimpleToast from 'react-native-simple-toast';

export default BriefTripServiceView = (props) => {
  const id = props.item.mode.id;
  const navigation = useNavigation();

  const getData = (id) => {
    switch (id) {
      case 1:
        return props.item.schedule_flight;
      case 2:
        return props.item.schedule_car;
      case 3:
        return props.item.schedule_stay;
      case 4:
        return props.item.schedule_train;
      case 5:
        return props.item.schedule_cruise;
        case 6:
        return props.item.schedule_transfer;
      case 7:
        return props.item.schedule_trip_plan;
      default:
        return null;
    }
  };

  const handleOnClick = (id, item) => {
    let object = {
      data: item,
      id: props.tripId,
      depart: props.depart,
      return: props.return,
    };
    if (props.package_request_id) {
      object.package_request_id = props.package_request_id;
    }
    switch (id) {
      case 1:
        return null;
      case 2:
        return navigation.navigate('AddCar', object);
      case 3:
        return navigation.navigate('Rooms', object);
      case 4:
        return navigation.navigate('AddTrain', object);
      case 5:
        return navigation.navigate('CruiseScreen', object);
      default:
        return null;
    }
  };

  const handleFightNavigation = (flight_id, modeId, scheduled_modes) => {
    navigation.navigate('BookFlight', {
      flight_id: flight_id,
      mode: modeId,
      data: scheduled_modes,
      id: props.tripId,
      depart: props.depart,
      return: props.return,
      package_request_id: props.package_request_id,
    });
  };


  const HeaderView = ({name, onPress}) => {
    return (
      <View style={[styles.header,{borderRadius:30}]}>
        <View style={styles.borderView}>
          <Image style={styles.image} source={{uri: props.item.mode.image}} />
        </View>
        <View style={[styles.headingView]}>
          <Text style={styles.headerMode}>{props.item.mode.mode}</Text>
        </View>
      </View>
    );
  };

  return (
    <View style={{backgroundColor:'transparent'}}>
      {id == 1 ? (
        <FlatList
          showsVerticalScrollIndicator={false}
          data={props.item.schedule_flight}
          renderItem={({item}) => {
            let flightModeName = item.flight_mode.name;
            let flight_id = item.id;
            let modeId = item.flight_mode_id;
            let scheduled_modes = item.schedule_modes;
            return (
              <FlatList
                showsVerticalScrollIndicator={false}
                data={item.schedule_modes}
                renderItem={({item}) => (
                  <View style={styles.serviceContainer}>
                    <View style={styles.valueBackground}>
                      <HeaderView
                        onPress={() =>
                          props.package_request_id
                            ? null
                            : handleFightNavigation(
                                flight_id,
                                modeId,
                                scheduled_modes,
                              )
                        }
                        name={flightModeName}
                      />
                    </View>
                  </View>
                )}
                numColumns={1}
                ListEmptyComponent={() => null}
                keyExtractor={(item, index) => index.toString()}
              />
            );
          }}
          numColumns={1}
          ListEmptyComponent={() => null}
          keyExtractor={(item, index) => index.toString()}
        />
      ) : (
        <FlatList
          showsVerticalScrollIndicator={false}
          data={getData(id)}
          renderItem={({item}) => (
            <View style={[styles.serviceContainer,{ 
                borderWidth: 0.5,
                borderColor: '#ddd',
                borderBottomWidth: 0,
                shadowColor: '#000000',
                shadowOffset: {width: 0, height: 4},
                shadowOpacity: 0.3,
                shadowRadius: 3,
                elevation: 3,
                }]}>
              <View style={styles.valueBackground}>
                <HeaderView
                  onPress={() =>
                    props.package_request_id ? null : handleOnClick(id, item)
                  }
                />
              </View>
            </View>
          )}
          numColumns={1}
          ListEmptyComponent={() => null}
          keyExtractor={(item, index) => index.toString()}
        />
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: COLOR.WHITE,
    borderRadius: 15,
    paddingVertical: 5,
  },
  header: {
    flexDirection: 'row',
    width: '100%',
  },
  headingView: {
    marginTop: 10,
    marginLeft: 5,
  },
  priceView: {
    alignItems: 'center',
    position: 'absolute',
    right: 10,
    top: 10,
  },
  image: {tintColor: COLOR.GREEN, height: 16, width: 16},

  dateView: {
    alignSelf: 'center',
    flexDirection: 'row',
    paddingHorizontal: 5,
  },
  dateIcon: {
    height: 20,
    width: 20,
    marginRight: 5,
  },

  serviceContainer: {
    // marginVertical: 5,
    height: 42,
    borderRadius: 30,
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: COLOR.GREEN,
    alignSelf: 'center',
    width: '100%',
  },
  modeImage: {
    height: 20,
    width: 20,
    marginHorizontal: 10,
    alignSelf: 'center',
  },
  optionView: {
    flexDirection: 'row',
    alignSelf: 'center',
    justifyContent: 'space-between',
    width: '90%',
    borderRadius: 20,
    alignItems: 'center',
  },
  ticketText: {
    transform: [{rotate: '90deg'}],
    fontFamily: FONTS.FAMILY_REGULAR,
    fontSize: 18,
    position: 'absolute',
    width: 120,
    height: 40,
    right: -40,
    paddingTop: 5,
    justifyContent: 'center',
    textAlign: 'center',
    color: COLOR.WHITE,
    alignSelf: 'center',
  },
  valueBackground: {
    height: 42,
    backgroundColor: COLOR.WHITE,
    paddingVertical: 5,
    borderRadius: 30,
    width: '100%',
    alignSelf: 'flex-start',
    justifyContent: 'center',
    alignItems: 'center',
  },
  borderView: {
    borderWidth: 1,
    borderColor: COLOR.BORDER,
    borderRadius: 5,
    alignItems: 'center',
    justifyContent: 'center',
    marginHorizontal: 10,
    padding: 5,
    marginVertical: 10,
    marginLeft:15
  },
  arrowView: {
    width: 10,
    height: 10,
    position: 'absolute',
    top: '45%',
    alignSelf: 'center',
    justifyContent: 'center',
    bottom: '45%',
    right: 5,
  },
  headerMode: {
    fontFamily: FONTS.FAMILY_SEMIBOLD,
    fontSize: 18,
  },
  pinPoint: {
    height: 20,
    width: 20,
    marginRight: 5,
  },
  stayView: {
    flexDirection: 'row',
    marginTop: 0,
    marginBottom: 10,
    marginHorizontal: 30,
    width: '75%',
    alignSelf: 'flex-end',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  carPickView: {
    marginHorizontal: 0,
    alignSelf: 'center',
    marginVertical: 0,
    flexDirection: 'row',
    width: '70%',
  },
  carRowView: {
    flexDirection: 'row',
    marginVertical: 5,
    width: '90%',
    justifyContent: 'space-between',
  },
});
