import React from 'react';
import {Modal, Text, View, StyleSheet, Image, ScrollView} from 'react-native';
import {TouchableOpacity} from 'react-native';
import {widthPercentageToDP, widthPercentageToDP as wp} from 'react-native-responsive-screen';
import COLOR from '../../styles/Color';
import FONTS from '../../styles/Fonts';
import Styles from '../../styles/Styles';
import NavigationBar from '../../commonView/NavigationBar';
import moment from 'moment';
import IMAGES from '../../styles/Images';

export default class RejectedTripDetails extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: true,
      rejectedHeading: '',
      rejectedComment: '',
    };
  }

  componentDidMount() {
    if (this.props.route.params && this.props.route.params.rejectedId) {
      if (this.props.route.params.rejectedId == 0) {
        this.setState({rejectedHeading: 'Price was too high'});
      } else if (this.props.route.params.rejectedId == 1) {
        this.setState({
          rejectedHeading: 'The offer is not as the user requested',
        });
      } else if (this.props.route.params.rejectedId == 2) {
        this.setState({rejectedHeading: 'User changed his plan'});
      } else if (this.props.route.params.rejectedId == 3) {
        this.setState({rejectedHeading: 'Other reasons'});
      }
    }

    if (this.props.route.params && this.props.route.params.rejectedComment) {
      this.setState({rejectedComment: this.props.route.params.rejectedComment});
    }
  }

  render() {
    return (
      <View style={Styles.container}>
        <Modal
         animationType={'slide'}
          transparent={true}
          style={{backgroundColor: COLOR.BLACK}}
          visible={this.state.modalVisible}
          onRequestClose={() => {
            console.log('Modal has been closed.');
          }}>
          <View style={styles.modal}>
            <View style={styles.modalBody}>
              <NavigationBar
                handleBack={() => {
                  this.props.navigation.goBack();
                }}
                prop={this.props.props}
                navHeight={40}
                name={'Details'}
              />
              <ScrollView>
                <View style={{flex: 1, alignItems: 'center'}}>
                  <View
                    style={{
                      height: 120,
                      width: 120,
                      borderRadius: 60,
                      backgroundColor: '#EFE5E5',
                      alignItems: 'center',
                      justifyContent: 'center',
                      marginTop: 30,
                      marginBottom: 30,
                    }}>
                    <View
                      style={{
                        height: 90,
                        width: 90,
                        borderRadius: 45,
                        backgroundColor: '#CF212B',
                        alignItems: 'center',
                        justifyContent: 'center',
                      }}>
                      <Image
                        resizeMode={'contain'}
                        source={IMAGES.whiteAlert}
                        style={{height: 40, width: 25}}
                      />
                    </View>
                  </View>

                  <Text
                    style={[Styles.subheading_label, {textAlign: 'center'}]}>
                    We are sorry, your request had not met our criteria in our
                    Terms and Condition.
                  </Text>

                  <Text
                    style={[
                      Styles.body_label,
                      {textAlign: 'center', marginTop: 20,marginBottom:10},
                    ]}>
                    Your request is rejected due to one of these reasons:
                  </Text>
                  <Text style={[Styles.body_label, {width:widthPercentageToDP(80)}]}>
                    • {this.state.rejectedHeading}
                  </Text>

                  {this.state.rejectedComment ? (
                    <Text style={[Styles.body_label, {width:widthPercentageToDP(80)}]}>
                      • {this.state.rejectedComment}
                    </Text>
                  ) : null}

                  <Text
                    style={[
                      Styles.body_label,
                      {textAlign: 'center', padding: 15},
                    ]}>
                    We would be happy to assist you more if you contact us at
                    info@email.com According to Our Terms and Condition you will
                    receive the refunds within 14 days.
                  </Text>

                  <Text
                    style={[
                      Styles.body_label,
                      {textAlign: 'center', padding: 15},
                    ]}>
                    Full Details of your reservation or/and the trip plan will
                    be soon ready for your final review and payment.
                  </Text>
                </View>
              </ScrollView>
              <View
                style={{
                  marginVertical: 20,
                  height: 50,
                  flexDirection: 'row',
                  width: wp(90),
                  alignSelf: 'center',
                }}>
                <TouchableOpacity
                  onPress={() => {
                    this.props.navigation.goBack();
                  }}
                  style={{flex: 1, marginRight: 10}}>
                  <View
                    style={{
                      flex: 1,
                      alignItems: 'center',
                      justifyContent: 'center',
                      borderRadius: 10,
                      height: 50,
                      borderColor: COLOR.BORDER,
                      borderWidth: 1,
                      backgroundColor: COLOR.WHITE,
                    }}>
                    <Text style={[Styles.button_font, {color: COLOR.BLACK}]}>
                      I Understand
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  modal: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
  },
  modalBody: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    backgroundColor: COLOR.WHITE,
    borderWidth: 1,
    width: wp(100),
    borderColor: COLOR.BORDER,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    flex: 1,
    marginTop: 50,
  },
  image: {
    height: 120,
    width: 120,
    alignSelf: 'center',
    marginVertical: 20,
  },
  headingLabel: {
    fontFamily: FONTS.FAMILY_BOLD,
    fontSize: FONTS.MEDIUM,
    color: COLOR.BLACK,
  },
  singleLineView: {
    marginVertical: 20,
    borderBottomColor: COLOR.GRAY,
    width: wp(100),
    alignSelf: 'center',
    borderBottomWidth: 1,
  },
  borderView: {
    borderRadius: 10,
    borderWidth: 1,
    width: wp(90),
    borderColor: COLOR.GRAY,
    alignSelf: 'center',
  },
});
