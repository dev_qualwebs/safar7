import React from 'react';
import {View, Image, StyleSheet, Text, SafeAreaView} from 'react-native';
import {
  FlatList,
  TouchableOpacity,
  TouchableWithoutFeedback,
} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import COLOR from '../../styles/Color';
import FONTS from '../../styles/Fonts';
import IMAGES from '../../styles/Images';
import Styles from '../../styles/Styles';
import {RoundButton, SemicircleDashView} from '../home/trip/NoTripView';

export default class PastRequest extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      tripData: [
        {
          name: 'Bali Island Trip',
          image: IMAGES.maldives,
        },
      ],
    };
  }

  flightView = (props) => {
    return (
      <View
        style={{
          backgroundColor: COLOR.WHITE,
          borderRadius: 15,
          paddingVertical: 5,
        }}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}>
          <View style={[style.imageView]}>
            <Image
              style={[{tintColor: COLOR.GREEN, height: 25, width: 25}]}
              source={IMAGES.plane}
            />
          </View>
          <View
            style={{
              flex: 1,
              justifyContent: 'flex-start',
              alignItems: 'flex-start',
            }}>
            <Text
              style={
                ([Styles.subheading_label],
                {width: null, textAlign: 'left', fontFamily: FONTS.FAMILY_BOLD})
              }>
              FLIGHT
            </Text>
            <Text
              style={
                ([Styles.small_label],
                {width: null, textAlign: 'left', fontSize: 12})
              }>
              Multi-City
            </Text>
          </View>
          <View
            style={{
              height: 40,
              width: 40,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <TouchableOpacity style={{height: 40, width: 40}}>
              <Image
                style={{
                  height: 24,
                  width: 24,
                  top: 0,
                  right: 5,
                  tintColor: COLOR.RED,
                  position: 'absolute',
                }}
                source={IMAGES.cross_icon}
              />
            </TouchableOpacity>
          </View>
        </View>
        <FlatList
          showsVerticalScrollIndicator={false}
          data={this.state.tripData}
          renderItem={({item}) => (
            <TouchableWithoutFeedback
              onPress={() => this.props.navigation.navigate('AddNewTrip')}>
              <View
                style={{
                  marginVertical: 5,
                  marginLeft: 60,
                  alignSelf: 'flex-start',
                }}>
                <View
                  style={{
                    flexDirection: 'row',
                    borderWidth: 1,
                    borderColor: COLOR.GRAY,
                    alignSelf: 'flex-start',
                    paddingHorizontal: 15,
                    paddingVertical: 5,
                    borderRadius: 20,
                  }}>
                  <Text
                    style={
                      ([Styles.small_label], {width: null, textAlign: 'left'})
                    }>
                    Riyadh
                  </Text>
                  <Image
                    style={{
                      height: 20,
                      width: 20,
                      marginHorizontal: 10,
                    }}
                    source={IMAGES.plane_horizontal}
                  />
                  <Text
                    style={
                      ([Styles.small_label], {width: null, textAlign: 'left'})
                    }>
                    Bali
                  </Text>
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    alignSelf: 'center',
                    borderRadius: 20,
                    marginVertical: 10,
                  }}>
                  <Image
                    style={{
                      height: 20,
                      width: 20,
                      marginRight: 5,
                    }}
                    source={IMAGES.calendar_small}
                    resizeMode={'contain'}
                  />
                  <Text
                    style={
                      ([Styles.body_label], {width: null, textAlign: 'left'})
                    }>
                    8 Jan 2021 - 17 Jan 2021
                  </Text>
                </View>
              </View>
            </TouchableWithoutFeedback>
          )}
          numColumns={1}
          ListEmptyComponent={() =>
            !this.state.tripData.length ? (
              <Text
                style={{
                  textAlign: 'center',
                  color: COLOR.LIGHT_TEXT,
                  marginTop: 100,
                }}>
                No Transactions Found
              </Text>
            ) : null
          }
          keyExtractor={(item, index) => index.toString()}
        />
      </View>
    );
  };

  render() {
    return (
      <SafeAreaView
        style={[Styles.container, {backgroundColor: COLOR.BACKGROUND}]}>
        <View
          style={[
            Styles.container,
            {backgroundColor: COLOR.BACKGROUND, marginBottom: 10},
          ]}>
          <FlatList
            showsVerticalScrollIndicator={false}
            data={this.state.tripData}
            renderItem={({item}) => (
              <TouchableWithoutFeedback
                onPress={() => this.props.navigation.navigate('AddNewTrip')}>
                <View
                  style={{
                    width: widthPercentageToDP(90),
                    alignSelf: 'center',
                    marginTop: 20,
                    borderRadius: 20,
                    shadowColor: COLOR.VOILET,
                    elevation: 3,
                    shadowOpacity: 0.2,
                    shadowOffset: {width: 2, height: 2},
                    backgroundColor: COLOR.WHITE,
                  }}>
                  <View
                    style={{
                      flex: 1,
                      alignItems: 'center',
                      paddingHorizontal: 10,
                    }}>
                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        alignItems: 'center',
                        width: wp(80),
                      }}>
                      <Text
                        style={[
                          Styles.subheading_label,
                          {
                            width: null,
                            alignSelf: 'flex-start',
                            marginVertical: 10,
                            fontFamily: FONTS.FAMILY_BOLD,
                          },
                        ]}>
                        Bali Island Trip
                      </Text>
                    </View>
                    <View
                      style={{
                        flexDirection: 'row',
                        height: 45,
                        justifyContent: 'space-between',
                        width: wp(80),
                        alignItems: 'center',
                        marginBottom: 5,
                        borderRadius: 10,
                        borderWidth: 2,
                        borderColor: COLOR.GRAY,
                        paddingHorizontal: 10,
                      }}>
                      <Text
                        style={[
                          Styles.small_label,
                          {
                            width: null,
                            fontFamily: FONTS.FAMILY_SEMIBOLD,
                            textAlign: 'left',
                            color: COLOR.BLACK,
                          },
                        ]}>
                        20 Mar 2021
                      </Text>
                      <Image
                        source={IMAGES.dot_back}
                        style={{height: 25, width: 25}}
                        resizeMode={'cover'}
                      />

                      <Text
                        style={[
                          Styles.body_label,
                          {
                            width: null,
                            fontFamily: FONTS.FAMILY_SEMIBOLD,
                            textAlign: 'left',
                            color: COLOR.BLACK,
                          },
                        ]}>
                        7 Apr 2021
                      </Text>
                    </View>
                    <SemicircleDashView />
                    <View
                      style={{
                        alignSelf: 'center',
                        width: widthPercentageToDP(80),
                        justifyContent: 'center',
                        alignItems: 'center',
                        marginBottom: 10,
                      }}>
                      {/*<TouchableOpacity
                        style={{
                          alignItems: 'center',
                          justifyContent: 'center',
                          height: 50,
                          borderRadius: 10,
                          marginTop: 0,
                          borderColor: COLOR.LIGHT_TEXT,
                          borderWidth: 1,
                          width: widthPercentageToDP(80),
                          backgroundColor: COLOR.WHITE,
                        }}>
                        <Text
                          style={[Styles.button_font, {color: COLOR.BLACK}]}>
                          Details
                        </Text>
                      </TouchableOpacity>*/}

                      <View style={{marginTop: 10}}>
                        {/* <TouchableOpacity
                          onPress={() => {
                            this.setState({modalVisible: false}, (val) => {
                              this.props.navigation.navigate('PlanNewTrip');
                            });
                          }}
                          style={{
                            alignItems: 'center',
                            justifyContent: 'center',
                            height: 50,
                            borderRadius: 10,

                            width: widthPercentageToDP(80),
                            backgroundColor: COLOR.GREEN,
                          }}>
                          <Text
                            style={[Styles.button_font, {color: COLOR.WHITE}]}>
                            Continue my request
                          </Text>
                        </TouchableOpacity>*/}
                      </View>
                    </View>
                    <View
                      style={{
                        backgroundColor: COLOR.GREEN_OPACITY,
                        width: wp(90),
                        borderRadius: 10,
                      }}>
                      <View
                        style={{
                          margin: 10,
                          backgroundColor: COLOR.WHITE,
                          borderRadius: 10,
                        }}>
                        <this.flightView />
                      </View>
                      <TouchableOpacity>
                        <View
                          style={{
                            height: 40,
                            width: 40,
                            alignSelf: 'center',
                            borderRadius: 20,
                            backgroundColor: COLOR.WHITE,
                            shadowOpacity: 0.2,
                            justifyContent: 'center',
                            alignItems: 'center',
                            shadowColor: COLOR.VOILET,
                            elevation: 3,
                            marginBottom: -25,
                          }}>
                          <Image
                            source={IMAGES.up_arrow}
                            style={{height: 15, width: 15, alignSelf: 'center'}}
                            resizeMode={'cover'}
                          />
                        </View>
                      </TouchableOpacity>
                    </View>
                  </View>
                </View>
              </TouchableWithoutFeedback>
            )}
            numColumns={1}
            ListEmptyComponent={() =>
              !this.state.tripData.length ? (
                <Text
                  style={{
                    textAlign: 'center',
                    color: COLOR.LIGHT_TEXT,
                    marginTop: 100,
                  }}>
                  No Transactions Found
                </Text>
              ) : null
            }
            keyExtractor={(item, index) => index.toString()}
          />
        </View>
      </SafeAreaView>
    );
  }
}

const style = StyleSheet.create({
  topBar: {
    height: 50,
    shadowColor: COLOR.VOILET,
    shadowOpacity: 0.2,
    backgroundColor: COLOR.WHITE,
    flexDirection: 'row',
    shadowOffset: {width: 2, height: 2},
    justifyContent: 'space-between',
    width: wp(95),
    elevation: 3,
    alignSelf: 'center',
    marginVertical: 20,

    alignItems: 'center',
  },
  imageView: {
    height: 40,
    width: 40,
    borderRadius: 5,
    backgroundColor: COLOR.WHITE,
    shadowColor: COLOR.VOILET,
    shadowOpacity: 0.2,
    shadowOffset: {width: 2, height: 2},
    borderColor: COLOR.BORDER,
    borderWidth: 1,
    margin: 10,
    elevation: 3,
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 10,
  },
});
