import {useEffect, useState} from 'react';
import React from 'react';
import {Modal, Text, View, StyleSheet, FlatList, Image} from 'react-native';
import COLOR from '../../styles/Color';
import FONTS from '../../styles/Fonts';
import {
  heightPercentageToDP,
  widthPercentageToDP,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import NavigationBar from '../../commonView/NavigationBar';
import moment from 'moment';
import Styles from '../../styles/Styles';
import {TouchableOpacity} from 'react-native-gesture-handler';
import IMAGES from '../../styles/Images';
import SimpleToast from 'react-native-simple-toast';

export default function NonSpecificDateSelector({
  visibility,
  updateMasterState,
}) {
  const [months, setMonths] = useState([]);
  const [selectedOption, setSelectedOption] = useState(null);
  const [days, setDays] = useState(5);
  const currentMonth = moment().format('MMM');

  useEffect(() => {
    var months = [];
    for (let i = 1; i <= 6; i++) {
      let monthString = moment().add(i, 'month').format('MMM');
      months.push(monthString.toUpperCase());
    }
    setMonths(months);
  }, []);

  const handlePlusMinus = (val) => {
    if (val == 0) {
      if (days > 1) {
        setDays(days - 1);
      }
    } else if (val == 1) {
      setDays(days + 1);
    }
  };

  const onApply = () => {
    updateMasterState(
      'monthPreference',
      selectedOption == 1
        ? 'Any'
        : selectedOption == 2
        ? 'After ' + currentMonth + '  (With in 6 Months)'
        : null,
    );
    updateMasterState('days', days);
    if (selectedOption != null) {
      updateMasterState('nonSpecificDates', false);
    } else {
      SimpleToast.show('Select Option');
    }
  };

  const onReset = () => {
    setDays(5);
    setSelectedOption(null);
    updateMasterState(
      'monthPreference',
      null,
    );
    updateMasterState('days', null);
   
  };

  const OptionView = ({item, id}) => {
    return (
      <TouchableOpacity
        onPress={() => {
          setSelectedOption(id);
        }}
        style={[
          styles.buttonView,
          {
            backgroundColor: id == selectedOption ? COLOR.GREEN : COLOR.WHITE,
          },
        ]}>
        <Text
          style={[
            styles.optionText,
            {color: id == selectedOption ? COLOR.WHITE : COLOR.BLACK},
          ]}>
          {item}
        </Text>
        <View
          style={{
            borderBottomColor: COLOR.GRAY,
            width: wp(100),
            alignSelf: 'center',
            position: 'absolute',
            bottom: 0,
            borderBottomWidth: 1,
          }}
        />
      </TouchableOpacity>
    );
  };

  return (
    <View>
      <Modal
        animationType={'slide'}
        transparent={true}
        style={{backgroundColor: COLOR.BLACK}}
        visible={visibility}
        onRequestClose={() => {
          console.log('Modal has been closed.');
        }}>
        <View style={styles.modal}>
          <View style={styles.modalBody}>
            <NavigationBar
              navHeight={40}
              isModal={{
                handleModalVisibility: () => {
                  updateMasterState('nonSpecificDates', false);
                },
              }}
              name={'Select Dates'}
            />
            <View style={styles.monthSlider}>
              <FlatList
              bounces={false}
              showsHorizontalScrollIndicator={false}
              showsVerticalScrollIndicator={false}
                data={months}
                horizontal={true}
                renderItem={({item}) => (
                  <View style={styles.monthView}>
                    <Text style={styles.monthText}>{item}</Text>
                  </View>
                )}
              />
            </View>

            <OptionView
              item={'After ' + currentMonth + '  (Within 6 Months)'}
              id={2}
            />
            <OptionView item={'Anytime'} id={1} />
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                width: wp(90),
                alignSelf: 'center',
                marginTop: 20,
                borderRadius: 10,
                paddingHorizontal: 15,
                paddingVertical: 10,
                borderWidth: 1,

                borderColor: COLOR.BACKGROUND,
              }}>
              <View style={{alignSelf: 'center', justifyContent: 'center'}}>
                <Text
                  style={[
                    Styles.subheading_label,
                    {
                      width: null,

                      alignSelf: 'center',
                      fontFamily: FONTS.FAMILY_REGULAR,
                    },
                  ]}>
                  Days of your trip
                </Text>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'flex-end',
                }}>
                <TouchableOpacity
                  onPress={() => handlePlusMinus(0)}
                  style={{
                    height: 30,
                    width: 30,
                    borderRadius: 15,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Image
                    style={{height: 30, width: 30}}
                    resizeMode={'contain'}
                    source={IMAGES.minus_image}
                  />
                </TouchableOpacity>
                <Text
                  style={[
                    Styles.button_font,
                    {width: 40, color: COLOR.BLACK, textAlign: 'center'},
                  ]}>
                  {days}
                </Text>
                <TouchableOpacity
                  onPress={() => handlePlusMinus(1)}
                  style={{
                    height: 30,
                    width: 30,
                    borderWidth: 1,
                    borderRadius: 15,
                    borderColor: COLOR.GREEN,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Image
                    style={{height: 20, width: 20}}
                    source={IMAGES.plus_image}
                  />
                </TouchableOpacity>
              </View>
            </View>

            <View
              style={{
                marginVertical: 20,
                height: 50,
                flexDirection: 'row',
                width: wp(90),
                alignSelf: 'center',
                justifyContent: 'center',
              }}>
              <TouchableOpacity style={{flex: 1, marginRight: 10}}
              onPress={()=>{
                onReset()
              }}
              >
                <View
                  style={{
                    width: wp(40),
                    alignItems: 'center',
                    justifyContent: 'center',
                    borderRadius: 10,
                    height: 50,
                    borderColor: COLOR.GRAY,
                    borderWidth: 1,
                    backgroundColor: COLOR.WHITE,
                  }}>
                  <Text style={[Styles.button_font, {color: COLOR.BLACK}]}>
                    Clear All
                  </Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => onApply()} style={{flex: 1}}>
                <View
                  style={{
                    width: wp(40),
                    alignItems: 'center',
                    justifyContent: 'center',
                    borderRadius: 10,
                    height: 50,
                    backgroundColor: COLOR.GREEN,
                  }}>
                  <Text style={[Styles.button_font, {color: COLOR.WHITE}]}>
                    Apply
                  </Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
    </View>
  );
}

const styles = StyleSheet.create({
  modal: {
    height: heightPercentageToDP(100),
    alignItems: 'center',
    justifyContent: 'flex-end',
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
    alignSelf: 'flex-end',
  },
  modalBody: {
    alignSelf: 'center',
    backgroundColor: COLOR.WHITE,
    borderWidth: 1,
    width: wp(100),
    height: heightPercentageToDP(60),
    borderColor: COLOR.BORDER,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    marginTop: 50,
  },
  image: {
    height: 120,
    width: 120,
    alignSelf: 'center',
    marginVertical: 20,
  },
  headingLabel: {
    fontFamily: FONTS.FAMILY_BOLD,
    fontSize: FONTS.MEDIUM,
    color: COLOR.BLACK,
  },
  singleLineView: {
    marginVertical: 20,
    borderBottomColor: COLOR.GRAY,
    width: wp(100),
    alignSelf: 'center',
    borderBottomWidth: 1,
  },
  borderView: {
    borderRadius: 10,
    borderWidth: 1,
    width: wp(90),
    borderColor: COLOR.GRAY,
    alignSelf: 'center',
  },
  monthSlider: {
    height: 50,
    width: widthPercentageToDP(100),
    backgroundColor: COLOR.BACKGROUND,
    justifyContent: 'center',
    alignItems: 'center',
  },
  monthText: {
    fontFamily: FONTS.FAMILY_SEMIBOLD,
    fontSize: 16,
    alignSelf: 'center',
    textAlign: 'center',
    textAlignVertical: 'center',
  },
  monthView: {
    height: 50,
    alignItems: 'center',
    alignSelf: 'center',
    justifyContent: 'center',
    width: wp(16.6),
  },
  buttonView: {
    height: 55,
    justifyContent: 'center',
    paddingHorizontal: 20,
  },
  optionText: {
    fontFamily: FONTS.FAMILY_BOLD,
    fontSize: 14,
  },
});
