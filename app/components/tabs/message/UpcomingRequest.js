import React from 'react';
import {Alert, RefreshControl} from 'react-native';
import {View, Image, StyleSheet, Text, SafeAreaView} from 'react-native';
import {FlatList, TouchableOpacity} from 'react-native';
import {TouchableWithoutFeedback} from 'react-native-gesture-handler';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import {connect} from 'react-redux';
import PackageAction from '../../../redux/action/PackageAction';
import RequestAction from '../../../redux/action/RequestAction';
import COLOR from '../../styles/Color';
import FONTS from '../../styles/Fonts';
import IMAGES from '../../styles/Images';
import Styles from '../../styles/Styles';
import DraftPackageView from './DraftPackageView';
import ExpiredOfferView from './ExpiredOfferView';
import NewTripView from './NewTripView';
import {NoRequestsView} from './NoRequestsView';
import OfferView from './OfferView';
import RejectedOfferView from './RejectedOfferView';
import RejectOfferModel from './RejectOfferModel';
import RequestedPackageView from './RequestedPackageView';
import RequestedTripView from './RequestedTripView';

class UpcomingRequest extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      upcomingTripData: [],
      pastTripData: [],
      cancelledTripData: [],
      packages: this.props.packages,
      requestedPackages: [],
      pastPackages: [],
      cancelledPackages: [],
      rejectModal: false,
      selectedTrip: null,
      refreshing: false,
      title: this.props.title,
    };

    this.getTripState = this.getTripState.bind(this);
    this.getPackageState = this.getPackageState.bind(this);
    this.getRequestedView = this.getRequestedView.bind(this);
  }

  _onRefresh = () => {
    this.setState({
      refreshing: true,
    });
    if (this.state.packages) {
      switch (this.props.status) {
        case 0:
          this.props.getBookedPackages();
          break;
        case 5:
          this.props.getCancelledPackage();
          break;
        case 6:
          this.props.getPastPackage();
          break;
      }
    } else {
      switch (this.props.status) {
        case 0:
          this.props.getUpcomingTrips();
          break;
        case 5:
          this.props.getCancelledTrips();
          break;
        case 6:
          this.props.getPastTrips();
          break;
      }
    }
  };

  componentDidMount() {
    this._onRefresh();
    if (this.props.upcomingTrips.length) {
      this.setState({
        upcomingTripData: this.props.upcomingTrips,
      });
    }
  }

  componentWillReceiveProps(props) {
    if (
      this.props.title &&
      this.props.title != this.state.title &&
      this.props.title == 'Packages'
    ) {
      this.setState({packages: true, title: this.props.title}, () => {
        this._onRefresh();
      });
    } else if (
      this.props.title &&
      this.props.title != this.state.title &&
      this.props.title == 'Requests'
    ) {
      this.setState({packages: false, title: this.props.title}, () => {
        this._onRefresh();
      });
    }

    if (this.props.upcomingTrips != props.upcomingTrips) {
      this.setState({
        upcomingTripData: props.upcomingTrips,
        refreshing: false,
      });
    }

    if (this.props.cancelledTrips != props.cancelledTrips) {
      this.setState({
        cancelledTripData: props.cancelledTrips,
        refreshing: false,
      });
    }

    if (this.props.pastTrips != props.pastTrips) {
      this.setState({
        pastTripData: props.pastTrips,
        refreshing: false,
      });
    }

    if (props.bookedPackages != this.props.bookedPackages) {
      this.setState({
        requestedPackages: props.bookedPackages,
        refreshing: false,
      });
    }

    if (props.cancelledPackages != this.props.cancelledPackages) {
      this.setState({
        cancelledPackages: props.cancelledPackages,
        refreshing: false,
      });
    }

    if (props.pastPackages != this.props.pastPackages) {
      this.setState({
        pastPackages: props.pastPackages,
        refreshing: false,
      });
    }
  }

  optionView = (props) => {
    const id = props.item.mode.id;
    const getData = (id) => {
      switch (id) {
        case 1:
          return props.item.schedule_flight;
        case 3:
          return props.item.schedule_stay;
        case 4:
          return props.item.schedule_train;
        case 5:
          return props.item.schedule_cruise;
        default:
          return null;
      }
    };

    handleOnClick = (id, item) => {
      switch (id) {
        case 1:
          return null;
        case 3:
          return this.props.navigation.navigate('StayScreen', {
            data: item,
            index: 0,
            screens: ['StayScreen'],
            id: props.tripId,
            depart: props.depart,
            return: props.return,
          });
        case 4:
          return this.props.navigation.navigate('TrainScreen', {
            data: item,
            index: 0,
            screens: ['TrainScreen'],
            id: props.tripId,
            depart: props.depart,
            return: props.return,
          });
        case 5:
          return this.props.navigation.navigate('CruiseScreen', {
            data: item,
            index: 0,
            screens: ['BookFlight'],
            id: props.id,
            depart: props.depart,
            return: props.return,
          });
        default:
          return null;
      }
    };

    return (
      <View
        style={{
          backgroundColor: COLOR.WHITE,
          borderRadius: 15,
          paddingVertical: 5,
        }}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}>
          <View style={[style.imageView]}>
            <Image
              style={[
                {tintColor: COLOR.GREEN, height: 40, width: 40, margin: 10},
              ]}
              source={{uri: props.item.mode.image}}
            />
          </View>
          <View
            style={{
              flex: 1,
              justifyContent: 'flex-start',
              alignItems: 'flex-start',
            }}>
            <Text
              style={
                ([Styles.subheading_label],
                {width: null, textAlign: 'left', fontFamily: FONTS.FAMILY_BOLD})
              }>
              {props.item.mode.mode}
            </Text>
          </View>
          <View
            style={{
              height: 40,
              width: 40,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <TouchableOpacity style={{height: 40, width: 40}}>
              <Image
                style={{
                  height: 24,
                  width: 24,
                  top: 0,
                  right: 5,
                  tintColor: COLOR.RED,
                  position: 'absolute',
                }}
                source={IMAGES.cross_icon}
              />
            </TouchableOpacity>
          </View>
        </View>
        {id == 1 ? (
          <FlatList
          bounces={false}
            showsVerticalScrollIndicator={false}
            data={props.item.schedule_flight}
            renderItem={({item}) => {
              let flight_id = item.id;
              return (
                <FlatList
                bounces={false}
                  showsVerticalScrollIndicator={false}
                  data={item.schedule_modes}
                  renderItem={({item}) => (
                    <TouchableWithoutFeedback
                      onPress={() =>
                        this.props.navigation.navigate('BookFlight', {
                          flight_id: flight_id,
                          data: item,
                          index: 0,
                          screens: ['BookFlight'],
                          id: props.id,
                          depart: props.depart,
                          return: props.return,
                        })
                      }>
                      <View
                        style={{
                          marginVertical: 5,
                          marginHorizontal: 10,
                          alignSelf: 'center',
                        }}>
                        <View
                          style={{
                            width: '100%',
                            flexDirection: 'row',
                            borderWidth: 1,
                            borderColor: COLOR.GRAY,
                            alignSelf: 'flex-start',
                            paddingHorizontal: 15,
                            paddingVertical: 5,
                            borderRadius: 20,
                          }}>
                          <Text
                            style={
                              ([Styles.small_label],
                              {width: '40%', textAlign: 'center'})
                            }>
                            {item.from}
                          </Text>
                          <Image
                            style={{
                              height: 20,
                              width: 20,
                              marginHorizontal: 10,
                              alignSelf: 'center',
                            }}
                            source={{uri: props.item.mode.image}}
                          />
                          <Text
                            style={
                              ([Styles.small_label],
                              {width: '40%', textAlign: 'center'})
                            }>
                            {item.to}
                          </Text>
                        </View>
                        <View
                          style={{
                            flexDirection: 'row',
                            alignSelf: 'center',
                            borderRadius: 20,
                            marginVertical: 10,
                          }}>
                          <Image
                            style={{
                              height: 20,
                              width: 20,
                              marginRight: 5,
                            }}
                            source={IMAGES.calendar_small}
                            resizeMode={'contain'}
                          />
                          <Text
                            style={
                              ([Styles.body_label],
                              {width: null, textAlign: 'left'})
                            }>
                            {item.depart +
                              (item.return ? '  -  ' + item.return : '')}
                          </Text>
                        </View>
                      </View>
                    </TouchableWithoutFeedback>
                  )}
                  numColumns={1}
                  ListEmptyComponent={() => null}
                  keyExtractor={(item, index) => index.toString()}
                />
              );
            }}
            numColumns={1}
            ListEmptyComponent={() => null}
            keyExtractor={(item, index) => index.toString()}
          />
        ) : (
          <FlatList
            showsVerticalScrollIndicator={false}
            data={getData(id)}
            renderItem={({item}) => (
              <TouchableWithoutFeedback onPress={() => handleOnClick(id, item)}>
                <View
                  style={{
                    marginVertical: 5,
                    marginHorizontal: 10,
                    alignSelf: 'center',
                  }}>
                  {id == 3 ? null : (
                    <View
                      style={{
                        flexDirection: 'row',
                        borderWidth: 1,
                        borderColor: COLOR.GRAY,
                        alignSelf: 'center',
                        paddingHorizontal: 15,
                        paddingVertical: 5,
                        borderRadius: 20,
                      }}>
                      <Text
                        style={
                          ([Styles.small_label],
                          {width: '40%', textAlign: 'center'})
                        }>
                        {item.from}
                      </Text>
                      <Image
                        style={{
                          height: 20,
                          width: 20,
                          marginHorizontal: 10,
                        }}
                        source={{uri: props.item.mode.image}}
                      />
                      <Text
                        style={
                          ([Styles.small_label],
                          {width: '40%', textAlign: 'center'})
                        }>
                        {item.to}
                      </Text>
                    </View>
                  )}
                  <View
                    style={{
                      flexDirection: 'row',
                      alignSelf: 'center',
                      borderRadius: 20,
                      marginVertical: 10,
                    }}>
                    <Image
                      style={{
                        height: 20,
                        width: 20,
                        marginRight: 5,
                      }}
                      source={IMAGES.calendar_small}
                      resizeMode={'contain'}
                    />
                    <Text
                      style={
                        ([Styles.body_label], {width: null, textAlign: 'left'})
                      }>
                      {id == 3
                        ? item.check_in + '  -  ' + item.check_out
                        : id == 4
                        ? item.depart + '  -  ' + item.return
                        : id == 5
                        ? item.depart
                        : null}
                    </Text>
                  </View>
                  {id == 3 ? (
                    <View
                      style={{
                        flexDirection: 'row',
                        alignSelf: 'center',
                        borderRadius: 20,
                        marginVertical: 10,
                      }}>
                      <Image
                        style={{
                          height: 20,
                          width: 20,
                          marginRight: 5,
                        }}
                        source={IMAGES.pinpoint}
                        resizeMode={'contain'}
                      />
                      <Text
                        style={
                          ([Styles.body_label],
                          {width: null, textAlign: 'left'})
                        }>
                        {item.city}
                      </Text>
                    </View>
                  ) : null}
                </View>
              </TouchableWithoutFeedback>
            )}
            numColumns={1}
            ListEmptyComponent={() => null}
            keyExtractor={(item, index) => index.toString()}
          />
        )}
      </View>
    );
  };

  getTripState = () => {
    switch (this.props.status) {
      case 0:
        return this.state.upcomingTripData;
      case 5:
        return this.state.cancelledTripData;
      case 6:
        return this.state.pastTripData;
    }
  };

  getPackageState = () => {
    switch (this.props.status) {
      case 0:
        return this.state.requestedPackages;
      case 5:
        return this.state.cancelledPackages;
      case 6:
        return this.state.pastPackages;
    }
  };

  getRequestedView = (item) => {
    if (item.status) {
    if (item.is_offered == 1) {
        return (
          <OfferView
            isPackage={false}
            isPastTrip={item.status == 6 ? true : false}
            onReject={() =>
              this.setState({
                selectedTrip: item,
                rejectModal: true,
              })
            }
            item={item}
          />
        );
      } else if (item.is_offered == 3) {
        if (item.active_offers[0].expired == 1) {
          return <ExpiredOfferView isPastTrip={item.status == 6 ? true : false} isPackage={false} item={item} />;
        } else {
          return <RejectedOfferView isPastTrip={item.status == 6 ? true : false} isPackage={false} item={item} />;
        }
      } else {
        return <RequestedTripView isPastTrip={item.status == 6 ? true : false} item={item} />;
      }
    } else {
      return <NewTripView data={item} />;
    }
  };

  getPackageView = (item) => {
    if (item.status) {
      if (item.is_offered == 1) {
        return (
          <OfferView
            isPackage={true}
            onReject={() =>
              this.setState({
                selectedTrip: item,
                rejectModal: true,
              })
            }
            item={item}
          />
        );
      } else if (item.is_offered == 3) {
        if (item.active_offers[0].expired == 1) {
          return <ExpiredOfferView isPackage={true} item={item} />;
        } else {
          return <RejectedOfferView isPackage={true} item={item} />;
        }
      } else {
        return <RequestedPackageView item={item} />;
      }
    } else {
      return <DraftPackageView data={item} />;
    }
  };

  render() {
    return (
      <SafeAreaView
        style={[Styles.container, {backgroundColor: COLOR.BACKGROUND}]}>
        <FlatList
          showsVerticalScrollIndicator={false}
          // refreshControl={
          //   <RefreshControl
          //     colors={['#9Bd35A', '#689F38']}
          //     refreshing={this.state.refreshing}
          //     onRefresh={this._onRefresh.bind(this)}
          //   />
          // }
          data={
            this.state.packages ? this.getPackageState() : this.getTripState()
          }
          renderItem={({item, index}) => {
            const tripId = item.id;
            return this.state.packages
              ? this.getPackageView(item)
              : this.getRequestedView(item);
          }}
          numColumns={1}
          ListEmptyComponent={
            () => (
              //   // this.props.status == 0 && this.props.packages == false ? (
              <NoRequestsView
                props={this.props}
                navigation={this.props.navigation}
              />
            )
            //   // ) : (
            //   //   <View
            //   //     style={{
            //   //       marginTop: 100,
            //   //       alignSelf: 'center',
            //   //       justifyContent: 'center',
            //   //       flexDirection: 'row',
            //   //     }}>
            //   //     <Text>No Trip available</Text>
            //   //   </View>
            //   // )
          }
          // keyExtractor={(item, index) => index.toString()}
          refreshing={this.state.refreshing}
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={() => {
                this._onRefresh();
              }}
            />
          }
        />
        {this.state.selectedTrip ? (
          <RejectOfferModel
            selectedTrip={this.state.selectedTrip}
            modalVisible={this.state.rejectModal}
            isPackage={this.state.packages}
            onClose={() =>
              this.setState({
                rejectModal: false,
                selectedTrip: null,
              })
            }
          />
        ) : null}
      </SafeAreaView>
    );
  }
}

const style = StyleSheet.create({
  topBar: {
    height: 50,
    shadowColor: COLOR.VOILET,
    shadowOpacity: 0.2,
    backgroundColor: COLOR.WHITE,
    flexDirection: 'row',
    shadowOffset: {width: 2, height: 2},
    justifyContent: 'space-between',
    width: wp(95),
    alignSelf: 'center',
    marginVertical: 20,
    elevation: 3,
    alignItems: 'center',
  },
  statusText: {
    flex: 1,
    textAlign: 'center',
    fontSize: 10,
    marginHorizontal: 2,
  },
});

const mapStateToProps = (state, ownProps) => {
  return {
    upcomingTrips: state.requestReducer.upcomingTrips,
    cancelledTrips: state.requestReducer.cancelledTrips,
    pastTrips: state.requestReducer.pastTrips,
    requestTripData: state.requestReducer.requestTrip,
    bookedPackages: state.pacakgeReducer.bookedPackages,
    cancelledPackages: state.pacakgeReducer.cancelledPackages,
    pastPackages: state.pacakgeReducer.pastPackages,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getUpcomingTrips: (val) => {
      dispatch(RequestAction.getUpcomingTrips(val));
    },
    getCancelledTrips: (val) => {
      dispatch(RequestAction.getCancelledTrips(val));
    },
    getPastTrips: (val) => {
      dispatch(RequestAction.getPastTrips(val));
    },
    getBookedPackages: (val) => {
      dispatch(PackageAction.getBookedPackage(val));
    },
    getCancelledPackage: (val) => {
      dispatch(PackageAction.getCancelledPackage(val));
    },
    getPastPackage: (val) => {
      dispatch(PackageAction.getPastPackage(val));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(UpcomingRequest);
