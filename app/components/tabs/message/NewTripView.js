import TripServiceView from './TripServiceView';
import React, {useState} from 'react';
import {View, Image, Text, Alert} from 'react-native';
import {FlatList, TouchableOpacity} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import COLOR from '../../styles/Color';
import FONTS from '../../styles/Fonts';
import IMAGES from '../../styles/Images';
import Styles from '../../styles/Styles';
import {SemicircleDashView} from '../home/trip/NoTripView';
import {useNavigation} from '@react-navigation/native';
import {StyleSheet} from 'react-native';
import BriefTripServiceView from './BriefTripServiceView';
import SimpleToast from 'react-native-simple-toast';
import AddTravelModeNotice from '../profile/AddTravelModeNotice';

export default NewTripView = ({data}) => {
  const navigation = useNavigation();
  const [departDate, setDepart] = useState(
    data.trip_date_type.flexibility.depart,
  );
  const [returnDate, setReturn] = useState(
    data.trip_date_type.flexibility.return,
  );

const [showEmptyModal,setShowEmptyModal] = useState(false);
const [dayCount,setDayCount] = useState(0);

const goToView =(type)=>{
  setShowEmptyModal(false);
  if(type == 1){
  navigation.navigate('StayScreen', {
    data: data.trip_date_type.flexibility,
    index: 0,
    screens: ['StayScreen'],
    id: data.id,
    depart: data.trip_date_type.flexibility.depart,
    return: data.trip_date_type.flexibility.return,
  });
} else if(type == 2){
  navigation.navigate('CruiseScreen', {
    data: data.trip_date_type.flexibility,
    index: 0,
    screens: ['BookFlight'],
    id: data.id,
    depart:  data.trip_date_type.flexibility.depart,
    return:  data.trip_date_type.flexibility.return,
  });
}
}

  const [showTransports, setTransportVisible] = useState(true);
  return (
    <View>
      <View
        style={{
          width: widthPercentageToDP(90),
          alignSelf: 'center',
          marginTop: 20,
          marginBottom:10,
          borderRadius: 20,
          shadowColor: COLOR.VOILET,
          elevation: 3,
          shadowOpacity: 0.2,
          shadowOffset: {width: 2, height: 2},
          backgroundColor: COLOR.WHITE,
        }}>
        <View
          style={{
            flex: 1,
            alignItems: 'center',
            paddingHorizontal: 10,
          }}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              width: wp(80),
            }}>
            <Text
              style={[
                Styles.subheading_label,
                {
                  width: null,
                  alignSelf: 'flex-start',
                  marginVertical: 10,
                  fontFamily: FONTS.FAMILY_BOLD,
                },
              ]}>
              {data.trip_name}
            </Text>
          </View>

          {data.trip_date_type.flexibility.depart ? (
            <View style={styles.dateView}>
              <Text
                style={[
                  Styles.small_label,
                  {
                    width: null,
                    fontFamily: FONTS.FAMILY_SEMIBOLD,
                    textAlign: 'left',
                    color: COLOR.BLACK,
                  },
                ]}>
                {data.trip_date_type.flexibility.depart}
              </Text>
              <Image
                source={IMAGES.dot_back}
                style={{height: 25, width: 25}}
                resizeMode={'cover'}
              />

              <Text
                style={[
                  Styles.small_label,
                  {
                    width: null,
                    fontFamily: FONTS.FAMILY_SEMIBOLD,
                    textAlign: 'left',
                    color: COLOR.BLACK,
                  },
                ]}>
                {data.trip_date_type.flexibility.return}
              </Text>
            </View>
          ) : (
            <View style={styles.dateView}>
              <Text
                style={[
                  Styles.small_label,
                  {
                    width: null,
                    fontFamily: FONTS.FAMILY_SEMIBOLD,
                    textAlign: 'left',
                    color: COLOR.BLACK,
                  },
                ]}>
                {'Non Specific Dates'}
              </Text>
            </View>
          )}

          <SemicircleDashView />
          <View
            style={{
              alignSelf: 'center',
              width: widthPercentageToDP(80),
              justifyContent: 'center',
              alignItems: 'center',
              marginBottom: 10,
            }}>
            <TouchableOpacity
              onPress={() => {
                navigation.navigate('PlanTripSplash', {
                  id: data.id,
                  departFlexibility:
                    data.trip_date_type.flexibility.depart_flexibility_id,
                  returnFlexibility:
                    data.trip_date_type.flexibility.return_flexibility_id,
                  depart: departDate,
                  return: returnDate,
                  travelling_modes : data.travelling_modes
                });
              }}
              style={{
                alignItems: 'center',
                justifyContent: 'center',
                height: 50,
                borderRadius: 10,
                marginTop: 0,
                borderColor: COLOR.LIGHT_TEXT,
                borderWidth: 1,
                width: widthPercentageToDP(80),
                backgroundColor: COLOR.WHITE,
              }}>
              <Text style={[Styles.button_font, {color: COLOR.BLACK}]}>
                Details
              </Text>
            </TouchableOpacity>

            <View style={{marginTop: 10}}>
              <TouchableOpacity
                onPress={() => {
                  if(data.travelling_modes.length == 0){
                    console.log(data);

                    let date1 = new Date(data.trip_date_type.flexibility.depart);
                    let date2 = new Date(data.trip_date_type.flexibility.return);
                    let Difference_In_Time = date1.getTime() - date2.getTime();
                    console.log(Difference_In_Time);
                    
                    let Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);
                  

                  if(`${Difference_In_Days}`.includes('-')){
                    Difference_In_Days *= -1
                  }

                  setDayCount(Difference_In_Days)

                  if(Difference_In_Days > 0){
                    setShowEmptyModal(true)
                  } else {
                    SimpleToast.show('Please add some details about your trip before requesting')
                  }
                  }else {
                  navigation.navigate('PaymentScreen', {
                    data: data,
                    is_date_change: 0,
                  });
                }
                }}
                style={{
                  alignItems: 'center',
                  justifyContent: 'center',
                  height: 50,
                  borderRadius: 10,
                  width: widthPercentageToDP(80),
                  backgroundColor: COLOR.GREEN,
                }}>
                <Text style={[Styles.button_font, {color: COLOR.WHITE}]}>
                  Continue my request
                </Text>
              </TouchableOpacity>
            </View>
          </View>
          <View
            style={{
              backgroundColor: COLOR.GREEN_OPACITY,
              width: wp(90),
              borderRadius: 10,
            }}>
            {data.travelling_modes.length > 0 && showTransports ? (
              <View>
                <FlatList
                  data={data.travelling_modes}
                  renderItem={({item}) => {
                    return (
                      <View
                        style={{
                          margin: 10,
                          backgroundColor: COLOR.WHITE,
                          borderRadius: 10,
                        }}>
                        <TripServiceView
                          tripId={data.id}
                          item={item}
                          depart={departDate}
                          return={returnDate}
                        />
                      </View>
                    );
                  }}
                />
                </View>
            ) :   <View
            style={{
              backgroundColor: COLOR.WHITE,
              borderRadius:30,
              marginTop:10,
              width:widthPercentageToDP(85),
              alignSelf:'center'
            }}>
            <FlatList
              data={data.travelling_modes}
              renderItem={({item}) => {
                return (
                  // <View
                  //   style={{
                  //     backgroundColor: COLOR.WHITE,
                  //   }}>
                    <BriefTripServiceView
                      tripId={data.id}
                      item={item}
                      depart={departDate}
                      return={returnDate}
                    />
                  //  </View>
                );
              }}
            />
            </View>}
            {data.travelling_modes.length > 0 &&  <TouchableOpacity
                onPress={()=>{
                  setTransportVisible(!showTransports)
                }}
                >
                  <View
                    style={{
                      height: 40,
                      width: 40,
                      alignSelf: 'center',
                      borderRadius: 20,
                      backgroundColor: COLOR.WHITE,
                      shadowOpacity: 0.2,
                      justifyContent: 'center',
                      alignItems: 'center',
                      shadowColor: COLOR.VOILET,
                      elevation: 3,
                      marginBottom: -15,
                    }}>
                    <Image
                      source={ showTransports ? IMAGES.up_arrow : IMAGES.down_arrow}
                      style={{
                        height: 15,
                        width: 15,
                        alignSelf: 'center',
                      }}
                      resizeMode={'cover'}
                    />
                  </View>
                </TouchableOpacity>
}
              {showEmptyModal ? <AddTravelModeNotice dayCount={dayCount} hideModal={()=>setShowEmptyModal(false)}   goToView={(type)=>goToView(type)}/> : null}
          </View>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  dateView: {
    flexDirection: 'row',
    height: 45,
    justifyContent: 'space-between',
    width: wp(80),
    alignItems: 'center',
    marginBottom: 5,
    borderRadius: 10,
    borderWidth: 2,
    borderColor: COLOR.GRAY,
    paddingHorizontal: 10,
  },
});


