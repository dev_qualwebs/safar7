import React from 'react';
import {View, Image, Text} from 'react-native';
import {TouchableOpacity} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import COLOR from '../../styles/Color';
import FONTS from '../../styles/Fonts';
import IMAGES from '../../styles/Images';
import Styles from '../../styles/Styles';
import {SemicircleDashView} from '../home/trip/NoTripView';
import {useNavigation} from '@react-navigation/native';
import {StyleSheet} from 'react-native';
import API from '../../../api/Api';
import SimpleToast from 'react-native-simple-toast';
import WaitingView from '../../commonView/WaitingView';
import CompleteView from '../../commonView/CompleteView';

const DraftPackageView = ({data}) => {
  const navigation = useNavigation();
  const api = new API();
  const getSinglePackage = (id) => {
    api
      .getSinglePackage(id)
      .then((json) => {
        if (json.status == 200) {
          navigation.navigate('BookedPackage', {
            data: json.data.response,
          });
        } else if (json.status == 400) {
          setTimeout(() => {
            SimpleToast.show(json.data.message);
          }, 0);
        } else {
          setTimeout(() => {
            SimpleToast.show(json.data.message);
          }, 0);
        }
      })
      .catch((error) => {
        setTimeout(() => {
          console.log('json.data.message', error);
          //   Toast.show(String(error.response.data.message));
        }, 0);
        console.log('login error:-', error.response.data.message);
     
      });
  };

  return (
    <View>
      <View
        style={{
          width: widthPercentageToDP(90),
          alignSelf: 'center',
          marginTop: 20,
          borderRadius: 20,
          shadowColor: COLOR.VOILET,
          elevation: 3,
          shadowOpacity: 0.2,
          shadowOffset: {width: 2, height: 2},
          backgroundColor: COLOR.WHITE,
        }}>
        <View
          style={{
            flex: 1,
            alignItems: 'center',
            paddingHorizontal: 10,
          }}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              width: wp(80),
            }}>
            <Text
              style={[
                Styles.subheading_label,
                {
                  width: null,
                  alignSelf: 'flex-start',
                  marginVertical: 10,
                  fontFamily: FONTS.FAMILY_BOLD,
                },
              ]}>
              {data.package.package_name}
            </Text>
          </View>

          {data.is_date_change == 1 ? (
            <View style={styles.dateView}>
              <Text
                style={[
                  Styles.small_label,
                  {
                    width: null,
                    fontFamily: FONTS.FAMILY_SEMIBOLD,
                    textAlign: 'left',
                    color: COLOR.BLACK,
                  },
                ]}>
                {data.start_date}
              </Text>
              <Image
                source={IMAGES.dot_back}
                style={{height: 25, width: 25}}
                resizeMode={'cover'}
              />

              <Text
                style={[
                  Styles.small_label,
                  {
                    width: null,
                    fontFamily: FONTS.FAMILY_SEMIBOLD,
                    textAlign: 'left',
                    color: COLOR.BLACK,
                  },
                ]}>
                {data.end_date}
              </Text>
            </View>
          ) : (
            <View style={styles.dateView}>
              <Text
                style={[
                  Styles.small_label,
                  {
                    width: null,
                    fontFamily: FONTS.FAMILY_SEMIBOLD,
                    textAlign: 'left',
                    color: COLOR.BLACK,
                  },
                ]}>
                {data.package.period_of_travel}
              </Text>
            </View>
          )}

          <SemicircleDashView />
          <View
            style={{
              marginHorizontal: 20,
              flex: 1,
              alignSelf: 'center',
              flexDirection: 'row',
            }}>
            <CompleteView />
            <View style={styles.hr} />
            <WaitingView />
            <View style={styles.hr} />
            <WaitingView />
            <View style={styles.hr} />
            <WaitingView />
            <View style={styles.hr} />
            <WaitingView />
          </View>
          <View
            style={{
              flex: 1,
              marginVertical: 10,
              alignSelf: 'center',
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <Text style={styles.statusText}>Waiting for Acceptance</Text>
            <Text style={styles.statusText}>In Progress</Text>
            <Text style={styles.statusText}>Reservations</Text>
            <Text style={styles.statusText}>Trip Plan</Text>

            <Text style={styles.statusText}>Review</Text>
          </View>

          <View
            style={{
              alignSelf: 'center',
              width: widthPercentageToDP(80),
              justifyContent: 'center',
              alignItems: 'center',
              marginBottom: 10,
            }}>
            <TouchableOpacity
            onPress={()=>{
              // navigation.navigate('BookedPackage',{data:data})
              getSinglePackage(data.package.id);
              console.log('package data',data)
            }}
              style={{
                alignItems: 'center',
                justifyContent: 'center',
                height: 50,
                borderRadius: 10,
                marginTop: 0,
                borderColor: COLOR.LIGHT_TEXT,
                borderWidth: 1,
                width: widthPercentageToDP(80),
                backgroundColor: COLOR.WHITE,
              }}>
              <Text style={[Styles.button_font, {color: COLOR.BLACK}]}>
                Details
              </Text>
            </TouchableOpacity>

            {/* <View style={{marginTop: 10}}>
              <TouchableOpacity
                onPress={() => {
                  navigation.navigate('PaymentScreen', {
                    data: data.package,
                    requestId: data.id,
                    is_date_change: data.is_date_change,
                    depart: data.package.depart,
                    return: data.package.return,
                    is_package: 1,
                  });
                }}
                style={{
                  alignItems: 'center',
                  justifyContent: 'center',
                  height: 50,
                  borderRadius: 10,
                  width: widthPercentageToDP(80),
                  backgroundColor: COLOR.GREEN,
                }}>
                <Text style={[Styles.button_font, {color: COLOR.WHITE}]}>
                  Continue my request
                </Text>
              </TouchableOpacity>
            </View> */}
          </View>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  dateView: {
    flexDirection: 'row',
    height: 45,
    justifyContent: 'space-between',
    width: wp(80),
    alignItems: 'center',
    marginBottom: 5,
    borderRadius: 10,
    borderWidth: 2,
    borderColor: COLOR.GRAY,
    paddingHorizontal: 10,
  },
  hr: {
    height: 3,
    backgroundColor: COLOR.GRAY,
    flex: 1,
    alignSelf: 'center',
  },
  statusText: {
    flex: 1,
    textAlign: 'center',
    fontSize: 10,
    marginHorizontal: 2,
  },
});

export default DraftPackageView;
