import WaitingView from '../../commonView/WaitingView';
import React from 'react';
import {View, Image, Text} from 'react-native';
import {TouchableOpacity, StyleSheet} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import COLOR from '../../styles/Color';
import FONTS from '../../styles/Fonts';
import IMAGES from '../../styles/Images';
import Styles from '../../styles/Styles';
import {SemicircleDashView} from '../home/trip/NoTripView';
import CompleteView from '../../commonView/CompleteView';
import { useNavigation } from '@react-navigation/native';

export default RequestedTripView = (props) => {
  const navigation = useNavigation();
  return (
    <View>
      <View style={styles.container}>
        <View
          style={{
            flex: 1,
            alignItems: 'center',
            paddingHorizontal: 10,
          }}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              width: wp(80),
            }}>
            <Text
              style={[
                Styles.subheading_label,
                {
                  width: null,
                  alignSelf: 'flex-start',
                  marginVertical: 10,
                  fontFamily: FONTS.FAMILY_BOLD,
                },
              ]}>
              {props.item.trip_name}
            </Text>
          </View>
          <View
            style={{
              flexDirection: 'row',
              height: 45,
              justifyContent: 'space-between',
              width: wp(80),
              alignItems: 'center',
              marginBottom: 5,
              borderRadius: 10,
              borderWidth: 2,
              borderColor: COLOR.GRAY,
              paddingHorizontal: 10,
            }}>
            {props.item.trip_date_type.flexibility.depart ? (
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-evenly',
                  width: '100%',
                }}>
                <Text
                  style={[
                    Styles.small_label,
                    {
                      width: null,
                      fontFamily: FONTS.FAMILY_SEMIBOLD,
                      textAlign: 'left',
                      color: COLOR.BLACK,
                    },
                  ]}>
                  {props.item.trip_date_type.flexibility.depart}
                </Text>
                <Image
                  source={IMAGES.dot_back}
                  style={{height: 25, width: 25}}
                  resizeMode={'cover'}
                />

                <Text
                  style={[
                    Styles.small_label,
                    {
                      width: null,
                      fontFamily: FONTS.FAMILY_SEMIBOLD,
                      textAlign: 'left',
                      color: COLOR.BLACK,
                    },
                  ]}>
                  {props.item.trip_date_type.flexibility.return}
                </Text>
              </View>
            ) : (
              <Text
                style={[
                  Styles.small_label,
                  {
                    width: null,
                    fontFamily: FONTS.FAMILY_SEMIBOLD,
                    textAlign: 'left',
                    color: COLOR.BLACK,
                  },
                ]}>
                Non-Specific Dates
              </Text>
            )}
          </View>
          <SemicircleDashView />
          { props.item.offer_payment != 1 ? 
          <><View
            style={{
              marginHorizontal: 20,
              flex: 1,
              alignSelf: 'center',
              flexDirection: 'row',
            }}>
            {props.item.status.id >= 1 ? <CompleteView /> : <WaitingView />}
            <View style={styles.hr} />
            {props.item.status.id >= 2 ? <CompleteView /> : <WaitingView />}
            <View style={styles.hr} />
            {props.item.status.id >= 3 ? <CompleteView /> : <WaitingView />}
            <View style={styles.hr} />
            {props.item.status.id >= 4 ? <CompleteView /> : <WaitingView />}
            <View style={styles.hr} />
            {props.item.status.id >= 7 ? <CompleteView /> : <WaitingView />}
          </View>
          <View style={styles.statusView}>
            <Text style={styles.statusText}>Waiting for Acceptance</Text>
            <Text style={styles.statusText}>In Progress</Text>
            <Text style={styles.statusText}>Reservations</Text>
            <Text style={styles.statusText}>Trip Plan</Text>

            <Text style={styles.statusText}>Review</Text>
          </View></> : null}
          {props.item.status.id >= 7 && props.item.offer_payment != 1 ? (
            <Text style={[Styles.small_label,{color:COLOR.RED,textAlign:'center',marginVertical:10,}]}>You Will Receive the Offers Soon!</Text>
          ) : null}

          <TouchableOpacity style={styles.detailButton}
          onPress={()=>{
            if(props.item.offer_payment == 1){
              navigation.navigate('PaymentSecond', {
                id: props.item.id,
              });
            }else {
            navigation.navigate('TripStatusView')}
          }}
          >
            <Text style={[Styles.button_font, {color: COLOR.BLACK}]}>
              Details
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  topBar: {
    height: 50,
    shadowColor: COLOR.VOILET,
    shadowOpacity: 0.2,
    backgroundColor: COLOR.WHITE,
    flexDirection: 'row',
    shadowOffset: {width: 2, height: 2},
    justifyContent: 'space-between',
    width: wp(95),
    alignSelf: 'center',
    marginVertical: 20,
    elevation: 3,
    alignItems: 'center',
  },
  statusText: {
    flex: 1,
    textAlign: 'center',
    fontSize: 10,
    marginHorizontal: 2,
  },
  detailButton: {
    alignItems: 'center',
    justifyContent: 'center',
    height: 50,
    borderRadius: 10,
    marginTop: 0,
    marginBottom: 10,
    borderColor: COLOR.LIGHT_TEXT,
    borderWidth: 1,
    width: widthPercentageToDP(80),
    backgroundColor: COLOR.WHITE,
  },
  statusView: {
    flex: 1,
    marginVertical: 10,
    alignSelf: 'center',
    flexDirection: 'row',
    alignItems: 'center',
  },
  hr: {
    height: 3,
    backgroundColor: COLOR.GRAY,
    flex: 1,
    alignSelf: 'center',
  },
  container: {
    width: widthPercentageToDP(90),
    alignSelf: 'center',
    marginTop: 20,
    borderRadius: 20,
    shadowColor: COLOR.VOILET,
    elevation: 3,
    shadowOpacity: 0.2,
    shadowOffset: {width: 2, height: 2},
    backgroundColor: COLOR.WHITE,
  },
});
