import React from 'react';
import {
  Modal,
  Text,
  View,
  StyleSheet,
  Image,
  Button,
  Alert,
} from 'react-native';
import {TextInput, TouchableOpacity} from 'react-native';
import {
  heightPercentageToDP,
  widthPercentageToDP,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import COLOR from '../../../components/styles/Color';
import FONTS from '../../../components/styles/Fonts';
import {KeyboardAvoidingView} from 'react-native';
import IMAGES from '../../../components/styles/Images';
import Styles from '../../../components/styles/Styles';
import NavigationBar from '../../commonView/NavigationBar';
import SelectDate from '../calendar/SelectDate';
import Toast from 'react-native-simple-toast';
import {
  getEndDateTimestamp,
  getStartDateTimestamp,
} from '../../commonView/Helpers';
import SimpleToast from 'react-native-simple-toast';
import {DoneButtonKeyboard} from '../../commonView/CommonView';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview';

export default class AddNewStay extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      openCalendar: false,
      selectedIndex: null,
      // city: '',
      checkInDate: null,
      checkOutDate: null,
      check_in_flexibility: 1,
      check_out_flexibility: 1,
      number_of_days: 0,
      number_of_rooms: 1,
      selector: null,
      openPlaces: false,
      stayId: null,
      city: null,
      minDate: getStartDateTimestamp(this.props.props.route.params.depart),
      maxDate: getEndDateTimestamp(this.props.props.route.params.return),
      tripDepart: this.props.props.route.params.depart,
      tripReturn: this.props.props.route.params.return,
      fromFlexibility: 0,
      toFlexibility: 0,
      selectedRoom: this.props.selectedRoom,
      package_request_id: this.props.props.route.params.package_request_id,
    };
    this.openCalendar = this.openCalendar.bind(this);
    this.handleNavigation = this.handleNavigation.bind(this);
  }

  componentDidMount() {
    if (this.props.props.route && this.props.props.route.params.data) {
      let data = this.props.props.route.params.data;
      console.log('data.stay_preference.rooms', data.stay_preference.rooms);
      let room = data.stay_preference.rooms.map((val, index) => {
        let object = {
          type_id: val.room_type_id,
          bed_preference_id: val.bed_preference_id,
          comment: val.comment,
          name:
            val.room_type_id == 1
              ? 'Superior ' + (index + 1)
              : 'Premium ' + (index + 1),
        };
        return object;
      });

      this.setState({
        stayId: data.id,
        city: data.city,
        number_of_days: `${data.number_of_days}`,
        checkInDate: data.check_in,
        checkOutDate: data.check_out,
        check_in_flexibility: data.check_in_flexibility,
        check_out_flexibility: data.check_out_flexibility,
        selectedRoom: room[0],
      });
    }
  }

  componentDidUpdate(prevProps) {
    if (prevProps.selectedRoom !== this.props.selectedRoom) {
      this.setState({
        selectedRoom: this.props.selectedRoom,
      });
    }
  }

  updateMasterState = (attrName, value) => {
    this.setState({[attrName]: value});
  };

  handleNavigation = () => {
    if (this.state.city == null) {
      Toast.show('Enter city');
    } else if (this.state.checkInDate == null && this.state.tripDepart) {
      Toast.show('Select Check in Date');
    } else if (this.state.checkOutDate == null && this.state.tripDepart) {
      Toast.show('Select Check out Date');
    } else if (this.state.number_of_days == 0) {
      Toast.show('Enter number of days');
    } else if (this.state.selectedRoom != null) {
      const rooms = [];
      rooms.push(this.state.selectedRoom);
      this.props.props.navigation.replace('StayOption', {
        params: this.state,
        rooms: rooms,
        id: this.props.id,
        package_request_id: this.state.package_request_id,
      });
      this.props.updateMasterState('modalVisible', false);
    } else {
      Toast.show('Please select room');
    }
  };

  openCalendar = (minDate, selector) => {
    if (minDate) {
      this.setState({
        minDate: minDate,
        selector: selector,
        openCalendar: true,
      });
    } else {
      SimpleToast.show('First Select depart date');
    }

    if (selector == 0) {
      this.setState({
        checkOutDate: null,
      });
    }
  };

  render() {
    return (
      <View style={Styles.container}>
        {this.state.openCalendar ? (
          <SelectDate
            allowRangeSelection={false}
            selector={this.state.selector}
            updateMasterState={this.updateMasterState}
            props={this.props.props}
            minDate={this.state.minDate}
            maxDate={this.state.maxDate}
          />
        ) : this.state.openPlaces ? null : (
          <Modal
            animationType={'slide'}
            transparent={true}
            style={{backgroundColor: COLOR.BLACK}}
            visible={this.props.modalVisible}
            onRequestClose={() => {}}>
              <KeyboardAvoidingView
                behavior={'padding'}
                style={styles.modal}>
            <View style={styles.modalBody}>
              
                <View>
                  <NavigationBar
                    prop={this.props.props ? this.props.props : this.props}
                    navHeight={40}
                    name={this.state.stayId ? 'Edit Stay' : 'Add New Stay'}
                  />
                  <View
                    style={{alignSelf: 'center', width: wp(90), marginTop: 10}}>
                    <View
                      style={{
                        flexDirection: 'row',
                        height: 50,
                        alignItems: 'center',
                        marginBottom: 0,
                      }}>
                      <Image
                        style={{height: 30, width: 30, marginRight: 10}}
                        source={IMAGES.pinpoint}
                        resizeMode={'contain'}
                      />
                      <View>
                        <Text style={Styles.small_label}>City Name</Text>
                        <TextInput
                          inputAccessoryViewID={'uniqueID'}
                          onPress={() => {
                            this.props.props.navigation.navigate(
                              'GooglePlaces',
                              {
                                updateMasterState: this.updateMasterState,
                                attrName: 'city',
                              },
                            );
                            this.setState({openPlaces: true});
                          }}
                          onFocus={() => {
                            this.props.props.navigation.navigate(
                              'GooglePlaces',
                              {
                                updateMasterState: this.updateMasterState,
                                attrName: 'city',
                              },
                            );
                            this.setState({openPlaces: true});
                          }}
                          value={this.state.city}
                          onChangeText={(text) => this.setState({city: text})}
                          style={Styles.body_label}
                          placeholder={'Select Place'}
                          placeholderTextColor={COLOR.LIGHT_TEXT_COLOR}
                        />
                        <DoneButtonKeyboard />
                      </View>
                    </View>
                    {this.state.tripDepart ? (
                      <View>
                        <View
                          style={[
                            Styles.line_view,
                            {width: wp(90), marginVertical: 10},
                          ]}
                        />
                        <View
                          style={{
                            flexDirection: 'row',
                            alignItems: 'center',
                            marginBottom: 0,
                          }}>
                          <Image
                            style={{height: 30, width: 30, marginRight: 10}}
                            source={IMAGES.departure_date}
                            resizeMode={'contain'}
                          />
                          <View>
                            <Text style={Styles.small_label}>
                              Check-In Date
                            </Text>
                            <TouchableOpacity
                              onPress={() => {
                                this.openCalendar(
                                  getStartDateTimestamp(this.state.tripDepart),
                                  0,
                                );
                              }}>
                              <Text style={Styles.body_label}>
                                {this.state.checkInDate
                                  ? this.state.checkInDate
                                  : 'Select Date'}
                              </Text>
                            </TouchableOpacity>
                          </View>
                        </View>
                        <View
                          style={[
                            Styles.line_view,
                            {width: wp(90), marginVertical: 10},
                          ]}
                        />
                        <View
                          style={{
                            flexDirection: 'row',
                            alignItems: 'center',
                            marginBottom: 0,
                          }}>
                          <Image
                            style={{height: 30, width: 30, marginRight: 10}}
                            source={IMAGES.return_date}
                            resizeMode={'contain'}
                          />
                          <View>
                            <Text style={Styles.small_label}>
                              Check-Out Date
                            </Text>
                            <TouchableOpacity
                              onPress={() => {
                                this.openCalendar(
                                  getStartDateTimestamp(this.state.checkInDate),
                                  1,
                                );
                              }}>
                              <Text style={Styles.body_label}>
                                {this.state.checkOutDate
                                  ? this.state.checkOutDate
                                  : 'Select Date'}
                              </Text>
                            </TouchableOpacity>
                          </View>
                        </View>
                      </View>
                    ) : null}
                    <View
                      style={[
                        Styles.line_view,
                        {width: wp(90), marginVertical: 10},
                      ]}
                    />
                    <View
                      style={{
                        flexDirection: 'row',
                        height: 50,
                        alignItems: 'center',
                        marginBottom: 0,
                      }}>
                      <Image
                        style={{height: 30, width: 30, marginRight: 10}}
                        source={IMAGES.calendar_small}
                        resizeMode={'contain'}
                      />
                      <View>
                        <Text style={Styles.small_label}>Number of Days</Text>
                        <TextInput
                          inputAccessoryViewID={'uniqueID'}
                          value={this.state.number_of_days}
                          onChangeText={(text) =>
                            this.setState({number_of_days: text})
                          }
                          style={Styles.body_label}
                          placeholder={'Select Days'}
                          keyboardType={'number-pad'}
                          placeholderTextColor={COLOR.LIGHT_TEXT_COLOR}
                        />
                      </View>
                    </View>
                    <View
                      style={[
                        Styles.line_view,
                        {width: wp(90), marginVertical: 10},
                      ]}
                    />
                    <View
                      style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        marginBottom: 0,
                      }}>
                      <Image
                        style={{height: 30, width: 30, marginRight: 10}}
                        source={IMAGES.no_of_rooms}
                        resizeMode={'contain'}
                      />
                      <View>
                        <Text style={Styles.small_label}>Rooms</Text>
                        <TouchableOpacity
                          onPress={() => {
                            this.props.updateMasterState('modalVisible', false);
                            this.props.updateMasterState('roomsModal', true);
                          }}>
                          <Text style={Styles.body_label}>
                            {this.state.selectedRoom
                              ? this.state.selectedRoom.name
                              : 'Select Room'}
                          </Text>
                        </TouchableOpacity>
                      </View>
                    </View>
                  </View>
                </View>
                <View
                  style={{
                    width: wp(90),
                    alignSelf:'center',
                    marginTop: 20,
                    justifyContent: 'space-between',
                    flexDirection: 'row',
                    alignItems: 'center',
                  }}>
                  <TouchableOpacity
                    style={{
                      width: wp(42.5),
                      alignSelf: 'center',
                      height: 50,
                      alignItems: 'center',
                      justifyContent: 'center',
                      backgroundColor: COLOR.WHITE,
                      borderRadius: 10,
                      marginBottom: 20,
                      borderColor: COLOR.LIGHT_TEXT,
                      borderWidth: 1,
                    }}
                    onPress={() => {
                      this.setState({modalVisible: false}, (val) => {
                        this.props.props
                          ? this.props.props.navigation.goBack()
                          : this.props.navigation.goBack();
                      });
                    }}>
                    <Text
                      style={[
                        Styles.button_font,
                        {
                          color: COLOR.BLACK,
                          width: null,
                          textAlign: 'center',
                        },
                      ]}>
                      Cancel
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={{
                      width: wp(42.5),
                      alignSelf: 'center',
                      height: 50,
                      alignItems: 'center',
                      justifyContent: 'center',
                      backgroundColor: COLOR.GREEN,
                      borderRadius: 10,
                      marginBottom: 20,
                      borderColor: COLOR.LIGHT_TEXT,
                      borderWidth: 1,
                    }}
                    onPress={() => this.handleNavigation()}>
                    <Text
                      style={[
                        Styles.button_font,
                        {
                          color: COLOR.WHITE,
                          width: null,
                          textAlign: 'center',
                        },
                      ]}>
                      Continue
                    </Text>
                  </TouchableOpacity>
                </View>
              
             
            </View>
            </KeyboardAvoidingView>
          </Modal>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  modal: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',

    backgroundColor: 'rgba(0, 0, 0, 0.7)',
  },
  modalBody: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    backgroundColor: COLOR.WHITE,
    borderWidth: 1,
    height: heightPercentageToDP(75),
    justifyContent: 'space-between',
    width: wp(100),
    marginTop: 50,
    borderColor: COLOR.BORDER,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
  image: {
    height: 120,
    width: 120,
    alignSelf: 'center',
    marginVertical: 20,
  },
  headingLabel: {
    fontFamily: FONTS.FAMILY_BOLD,
    fontSize: FONTS.MEDIUM,
    color: COLOR.BLACK,
  },
  singleLineView: {
    marginVertical: 20,
    borderBottomColor: COLOR.GRAY,
    width: wp(100),
    alignSelf: 'center',
    borderBottomWidth: 1,
  },
  borderView: {
    borderRadius: 10,
    borderWidth: 3,
    width: wp(90),
    borderColor: COLOR.GRAY,
    alignSelf: 'center',
  },
});
