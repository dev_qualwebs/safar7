'use strict';

import React from 'react';
import {View, Image, StyleSheet, Text, SafeAreaView, Alert} from 'react-native';
import {
  TouchableOpacity,
  TouchableWithoutFeedback,
  TextInput,
} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import Slider from 'react-native-slider';
import COLOR from '../../../components/styles/Color';
import FONTS from '../../../components/styles/Fonts';
import IMAGES from '../../../components/styles/Images';
import Styles from '../../../components/styles/Styles';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview';
import {AirbnbRating, Rating} from 'react-native-ratings';
import {connect} from 'react-redux';
import StayAction from '../../../redux/action/StayAction';
import SimpleToast from 'react-native-simple-toast';
import { DoneButtonKeyboard } from '../../commonView/CommonView';

class StayOption extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedIndex: 0,
      defaultItem: 2,
      hotel_name: '',
      propertyType: [],
      serviceAndFacilities: [],
      sliderValue: 1,
      property_types: [],
      reservation_policy: [],
      services: [],
      id: this.props.route.params.id,
      startRating: 1,
      guestRating: 1,
      stayId: null,
      comment: '',
      package_request_id: this.props.route.params.package_request_id,
    };
    this.params = null;
    this.rooms = null;
    this.props.getStayPolicy();
    this.props.getStayProperty();
    this.props.getStayServices();
  }

  updateMasterState = (attrName, value) => {
    this.setState({[attrName]: value});
  };

  componentDidMount() {
    this.params = this.props.route.params.params;

    this.rooms = this.props.route.params.rooms;
    if (this.params.stayId) {
      this.setState({
        stayId: this.params.stayId,
      });
      this.props.getSingleStay(this.params.stayId);
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.singleStay != prevProps.singleStay && this.props.singleStay) {
      let stayPref = this.props.singleStay.stay_preference;
      this.setState({
        propertyType: stayPref.property_types.map((v) => v.id),
        reservation_policy: stayPref.reservation_policy.map((v) => v.id),
        services: stayPref.services.map((v) => v.id),
        startRating: stayPref.hotel_star_rating,
        guestRating: stayPref.hotel_user_rating,
        sliderValue: stayPref.stay_budget_id,
        comment: this.props.singleStay.comment,
        hotel_name: this.props.singleStay.hotel_name,
      });
    }
  }

  setPropertyType = (val) => {
    this.setState({
      propertyType: val,
    });
    console.log('Selected Type', val);
  };

  setServiceAndFacilities = (val) => {
    this.setState({
      serviceAndFacilities: val,
    });
    console.log('facilities', val);
  };

  componentWillUnmount() {
    this.props.clearSingleStay();
  }

  callSchedule = () => {
    let data = JSON.stringify({
      tripId: this.state.id,
      city: this.params.city,
      check_in: this.params.checkInDate,
      check_out: this.params.checkOutDate,
      check_in_flexibility: this.params.check_in_flexibility,
      check_out_flexibility: this.params.check_out_flexibility,
      number_of_days: this.params.number_of_days,
      number_of_rooms: this.params.number_of_rooms,
      rooms: this.rooms,
      hotel_name: this.state.hotel_name,
      hotel_star_rating: this.state.startRating,
      hotel_user_rating: this.state.guestRating,
      stay_budget_id: this.state.sliderValue,
      property_types: this.state.propertyType,
      reservation_policy: this.state.reservation_policy,
      services: this.state.services,
      comment: this.state.comment,
      package_request_id: this.state.package_request_id,
    });
    if (this.state.stayId) {
      this.props.updateStay(data, this.state.stayId);
    } else {
      this.props.scheduleStay(data, this.state.id);
    }
  };

  handleValidation = () => {
    if (this.state.propertyType.length == 0) {
      SimpleToast.show('Select Property Type');
    } else if (this.state.reservation_policy.length == 0) {
      SimpleToast.show('Select reservation policy');
    } else if (this.state.services.length == 0) {
      SimpleToast.show('Select Services');
    } else if (this.state.hotel_name == '') {
      SimpleToast.show('Enter Hotel Name');
    } else {
      this.callSchedule();
      this.props.navigation.goBack();
    }
  };

  selectionView = (prop) => {
    return (
      <TouchableWithoutFeedback style={{height: 130}} onPress={prop.onPress}>
        <View
          style={{
            flex: 1,
            borderRadius: 10,
            borderWidth: 1,
            height: 130,
            width: wp(28),
            margin: 5,
            borderColor: prop.hasData ? '#05778F' : COLOR.GRAY,
            alignSelf: 'center',
            alignItems: 'center',
            justifyContent: 'center',
            padding: 10,
            backgroundColor : prop.hasData ? COLOR.GREEN_OPACITY :  COLOR.WHITE
          }}>
          <View
            style={{
              height: 60,
              width: 60,
              marginVertical: 10,
              backgroundColor: prop.hasData ? COLOR.WHITE : prop.bg,
              borderRadius: 5,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Image
              resizeMode={'contain'}
              source={prop.image}
              style={{
                height: 30,
                width: 30,
              }}
            />
          </View>
          <Text
            style={{
              marginBottom: 10,
              textAlign: 'center',
              fontSize: 14,
              fontFamily: FONTS.FAMILY_SEMIBOLD,
            }}>
            {prop.name}
          </Text>
        </View>
      </TouchableWithoutFeedback>
    );
  };

  render() {
    return (
      <SafeAreaView style={[Styles.container,{backgroundColor:COLOR.WHITE}]}>
        <KeyboardAwareScrollView
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}>
          <View>
            <View style={style.topBar}>
              <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                <Image
                  style={{height: 15, width: 15}}
                  source={IMAGES.back_arrow}
                />
              </TouchableOpacity>
              <Text
                style={[
                  Styles.subheading_label,
                  {width: null, fontSize: 20, fontFamily: FONTS.FAMILY_BOLD},
                ]}>
                Stay Options
              </Text>
              <TouchableOpacity>
                <Image
                  style={{height: 20, width: 20}}
                  source={''}
                  // IMAGES.filter_image
                />
              </TouchableOpacity>
            </View>
            <View
              style={[
                Styles.button_content,
                {borderRadius: 0, width: wp(100), height: 40},
              ]}>
              <Text style={Styles.button_font}>{this.params && this.params.city}</Text>
            </View>

            <View
              style={{
                flexDirection: 'row',
                width: wp(90),
                alignSelf: 'center',
                marginTop: 10,
                justifyContent: 'space-between',
              }}>
              <this.selectionView
                name={'Property\nType'}
                image={IMAGES.building}
                bg={'#CF212B20'}
                onPress={() =>
                  this.props.navigation.navigate('PropertyType', {
                    updateMasterState: this.updateMasterState,
                    data: this.props.stayProperty,
                    selected: this.state.propertyType,
                  })
                }
                hasData={this.state.propertyType.length > 0 ? true : false}
              />
              <this.selectionView
                name={'Reservation\nPoilcy'}
                image={IMAGES.shield}
                bg={'#05778F20'}
                onPress={() =>
                  this.props.navigation.navigate('StayPolicy', {
                    updateMasterState: this.updateMasterState,
                    data: this.props.stayPolicy,
                    selected: this.state.reservation_policy,
                    name : 'Reservation Poilcy'
                  })
                }
                hasData={this.state.reservation_policy.length > 0 ? true : false}
              />
              <this.selectionView
                name={'Services and\nFacilities'}
                image={IMAGES.knife}
                bg={'#02599520'}
                onPress={() =>
                  this.props.navigation.navigate('ServicesAndFacilities', {
                    updateMasterState: this.updateMasterState,
                    data: this.props.stayServices,
                    selected: this.state.services,
                  })
                }
                hasData={this.state.services.length > 0 ? true : false}
              />
            </View>
          </View>
          <View style={Styles.line_view} />
          <Text
            style={[
              Styles.subheading_label,
              {marginTop: 20, width: wp(90), fontSize: FONTS.REGULAR},
            ]}>
            Minimum Star Rating
          </Text>

          <Rating
            imageSize={30}
            ratingBackgroundColor={COLOR.GRAY}
            startingValue={this.state.startRating}
            onFinishRating={(rating) =>
              this.setState({
                startRating: rating,
              })
            }
            style={{paddingVertical: 10, width: wp(90)}}
          />
{/* 
<AirbnbRating
showRating={false}
 size={30}
 ratingBackgroundColor={COLOR.GRAY}
 startingValue={this.state.startRating}
 onFinishRating={(rating) =>
   this.setState({
     startRating: rating,
   })
 }
 style={{paddingVertical: 10, width: wp(90)}}
/> */}

          <View
            style={[Styles.line_view, {marginVertical: 20, width: wp(90)}]}
          />
          <Text style={[Styles.subheading_label, {fontSize: FONTS.REGULAR}]}>
            Minimum Guest Rating
          </Text>
          <View style={{width: wp(90), alignSelf: 'center'}}>
            <Slider
              step={1}
              minimumValue={1}
              thumbImage={IMAGES.pause}
              thumbStyle={{justifyContent:'center',alignItems:'center'}}
              maximumValue={5}
              value={this.state.guestRating}
              minimumTrackTintColor={COLOR.GREEN}
              thumbTintColor={COLOR.GREEN}
              maximumTrackTintColor={COLOR.GRAY}
              onValueChange={(value) => this.setState({guestRating: value})}
            />
            <View
              style={{flexDirection: 'row', justifyContent: 'space-between'}}>
              <Text>1</Text>
              <Text>2</Text>
              <Text>3</Text>
              <Text>4</Text>
              <Text>5</Text>
            </View>
          </View>
          <View
            style={[Styles.line_view, {marginVertical: 20, width: wp(90)}]}
          />
          <Text style={[Styles.subheading_label, {fontSize: FONTS.REGULAR}]}>
            Budget
          </Text>
          <View style={{width: wp(90), alignSelf: 'center'}}>
            <Slider
              step={1}
              minimumValue={1}
              thumbImage={IMAGES.pause}
              thumbStyle={{justifyContent:'center',alignItems:'center'}}
              maximumValue={5}
              value={this.state.sliderValue}
              minimumTrackTintColor={COLOR.GREEN}
              thumbTintColor={COLOR.GREEN}
              maximumTrackTintColor={COLOR.GRAY}
              onValueChange={(value) => this.setState({sliderValue: value})}
            />
            <View
              style={{flexDirection: 'row', justifyContent: 'space-between'}}>
              <Text style={style.budgetText}>100</Text>
              <Text style={style.budgetText}>200-400</Text>
              <Text style={style.budgetText}>400-600</Text>
              <Text style={style.budgetText}>600-800</Text>
              <Text style={style.budgetText}>800 & above</Text>
            </View>
          </View>
          <View
            style={[Styles.line_view, {marginVertical: 20, width: wp(90)}]}
          />

          <Text style={[Styles.subheading_label, {fontSize: FONTS.REGULAR}]}>
            Hotel Name
          </Text>
          <TextInput
           inputAccessoryViewID={'uniqueID'}
            placeholder={'Enter hotel name'}
            placeholderTextColor={COLOR.LIGHT_TEXT_COLOR}
            value={this.state.hotel_name}
            onChangeText={(text) => this.setState({hotel_name: text})}
            style={[
              Styles.body_label,
              {
                height: 50,
                alignSelf: 'center',
                alignItems: 'center',
                justifyContent: 'center',
                paddingHorizontal: 20,
                paddingVertical: 0,
                borderRadius: 10,
                borderWidth: 1,
                borderColor: COLOR.GRAY,
                marginTop: 10,
              },
            ]}
            multiline={false}
          />
          <DoneButtonKeyboard />
          <View
            style={[Styles.line_view, {marginVertical: 20, width: wp(90)}]}
          />
          <Text style={[Styles.subheading_label, {fontSize: FONTS.REGULAR}]}>
            Comments
          </Text>
          <TextInput
          inputAccessoryViewID={'uniqueID'}
            placeholder={'Write your comment'}
            placeholderTextColor={COLOR.LIGHT_TEXT_COLOR}
            style={[
              {
                height: 120,
                width: wp(90),
                alignSelf: 'center',
                paddingHorizontal: 20,
                paddingVertical: 20,
                borderRadius: 10,
                borderWidth: 1,
                paddingTop:15,
                borderColor: COLOR.GRAY,
                marginTop: 10,
              },
            ]}
            multiline={true}
            value={this.state.comment}
            onChangeText={(text) => this.setState({comment: text})}
          />
          <TouchableOpacity
            style={{
              width: wp(90),
              alignSelf: 'center',
              height: 50,
              alignItems: 'center',
              justifyContent: 'center',
              marginTop: 20,
              borderRadius: 10,
            }}
            onPress={() => {
              // this.setState({modalVisible: false}, (val) => {
              //   this.props.navigation.navigate('PropertyType');
              // });
this.setState({ selectedIndex: 0});
this.setState({ defaultItem: 2});
this.setState({ hotel_name: ''});
this.setState({propertyType: []});
this.setState({ serviceAndFacilities: []});
this.setState({ sliderValue: 1});
this.setState({property_types: []});
this.setState({reservation_policy: []});
this.setState({guestRating: 1});
this.setState({startRating: 0});
this.setState({services: []});
this.setState({stayId: null});
this.setState({comment: ''}); 
            }}>
            <Text
              style={[
                Styles.small_label,
                {
                  color: COLOR.LIGHT_TEXT,
                  width: null,
                  textAlign: 'right',
                },
              ]}>
              Clear all options
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              width: wp(90),
              alignSelf: 'center',
              height: 50,
              alignItems: 'center',
              justifyContent: 'center',
              backgroundColor: COLOR.GREEN,
              borderRadius: 10,
            }}
            onPress={() => {
              this.handleValidation();
            }}>
            <Text
              style={[
                Styles.button_font,
                {
                  color: COLOR.WHITE,
                  width: null,
                  textAlign: 'center',
                },
              ]}>
              Apply
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              width: wp(90),
              alignSelf: 'center',
              height: 50,
              alignItems: 'center',
              justifyContent: 'center',
              marginBottom: 0,
              borderRadius: 10,
            }}>
            <Text
              style={[
                Styles.small_label,
                {
                  color: COLOR.BLACK,
                  width: null,
                  textAlign: 'right',
                },
              ]}>
              Skip stay options
            </Text>
          </TouchableOpacity>
        </KeyboardAwareScrollView>
      </SafeAreaView>
    );
  }
}

const style = StyleSheet.create({
  topBar: {
    height: 50,
    backgroundColor: COLOR.WHITE,
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: wp(90),
    alignSelf: 'center',
    marginVertical: 0,
    alignItems: 'center',
  },
  budgetText: {
    fontSize: 10,
    alignSelf: 'center',
  },
});

mapDispatchToProps = (dispatch) => {
  return {
    scheduleStay: (data, id) => {
      dispatch(StayAction.scheduleStay(data, id));
    },
    getStayServices: (val) => {
      dispatch(StayAction.getStayServices(val));
    },
    getStayPolicy: (val) => {
      dispatch(StayAction.getStayPolicy(val));
    },
    getStayProperty: (val) => {
      dispatch(StayAction.getStayProperty(val));
    },
    getSingleStay: (val) => {
      dispatch(StayAction.getSingleStay(val));
    },
    updateStay: (data, id) => {
      dispatch(StayAction.updateStay(data, id));
    },
    clearSingleStay: () => {
      dispatch(StayAction.clearSingleStay());
    },
  };
};

const mapStateToProps = (state, ownProps) => {
  return {
    stayProperty: state.stayReducer.stayProperty,
    stayPolicy: state.stayReducer.stayPolicy,
    stayServices: state.stayReducer.stayServices,
    singleStay: state.stayReducer.singleStay,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(StayOption);
