import {View, Image, Text, Alert} from 'react-native';
import React from 'react';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import COLOR from '../../styles/Color';
import Styles from '../../styles/Styles';
import {
  FlatList,
  TouchableOpacity,
  TouchableWithoutFeedback,
} from 'react-native';
import IMAGES from '../../styles/Images';
import FONTS from '../../styles/Fonts';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import StayAction from '../../../redux/action/StayAction';
import {connect} from 'react-redux';
import TripDateView from '../../commonView/TripDateView';
import SelectDate from '../calendar/SelectDate';
import SimpleToast from 'react-native-simple-toast';

class StayScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      stayData: [],
      isPackage: this.props.route.params.isPackage
        ? this.props.route.params.isPackage
        : false,
      depart: this.props.route.params.depart,
      return: this.props.route.params.return,
      departFlexibility: 0,
      returnFlexibility: 0,
      package_request_id: this.props.route.params.package_request_id,
    };
    this.props.getScheduledStay(this.props.route.params.id);
  }

  componentDidMount() {
    if (this.props.scheduledStay) {
      this.setState({
        stayData: this.props.scheduledStay,
      });
    }

    if (this.props.route.params.depart) {
      let data = this.props.route.params;
      this.setState({
        tripDepart: data.depart,
        tripReturn: data.return,
        departFlexibility: data.departFlexibility,
        returnFlexibility: data.returnFlexibility,
      });
    }
  }

  componentDidUpdate(prevProps) {
    if (this.props.scheduledStay != prevProps.scheduledStay) {
      this.setState({
        stayData: this.props.scheduledStay,
      });
    }
  }

  render() {
    const screens = this.props.route.params.screens;
    const index = this.props.route.params.index;
    const id = this.props.route.params.id;
    return (
      <View style={{flex: 1, backgroundColor: COLOR.BACKGROUND}}>
        <View
          style={{
            height: 220,
            backgroundColor: COLOR.GREEN,
            borderBottomRightRadius: 15,
            borderBottomLeftRadius: 15,
          }}>
          <View
            style={[
              Styles.mainView,
              {
                height: 45,
                marginTop: getStatusBarHeight(true),
                alignSelf: 'center',
                alignItems: 'center',
                flexDirection: 'row',
              },
            ]}>
            <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
              <Image
                style={{height: 15, width: 15, tintColor: COLOR.WHITE}}
                source={IMAGES.back_arrow}
              />
            </TouchableOpacity>

            <View style={{flex: 1}}>
              <Text
                style={[
                  Styles.heading_label,
                  {
                    textAlign: 'center',
                    color: COLOR.WHITE,
                    width: null,
                    marginVertical: 0,
                    fontFamily: FONTS.FAMILY_BOLD,
                  },
                ]}>
                Stay
              </Text>
            </View>
          </View>
          <TripDateView
            departDate={this.state.tripDepart}
            returnDate={this.state.tripReturn}
            departFlexibility={this.state.departFlexibility}
            returnFlexibility={this.state.returnFlexibility}
          />
        </View>
        <View
          style={{
            marginTop: -50,
            width: wp(90),
            alignSelf: 'center',
            borderRadius: 10,
            shadowColor: COLOR.VOILET,
            shadowOffset: {width: 2, height: 2},
            shadowOpacity: 0.2,
            backgroundColor: COLOR.WHITE,
            marginBottom: 10,
            elevation: 3,
            paddingVertical: 20,
          }}>
          <FlatList
            showsVerticalScrollIndicator={false}
            data={this.state.stayData}
            renderItem={({item, index}) => (
              <TouchableWithoutFeedback
                onPress={() => {
                  this.props.navigation.navigate('Rooms', {
                    data: item,
                    id: id,
                    depart: this.state.depart,
                    return: this.state.return,
                  });
                }}>
                <View
                  style={{
                    alignSelf: 'center',
                    paddingHorizontal: 20,
                    backgroundColor: COLOR.WHITE,
                    alignItems: 'center',
                  }}>
                  <View
                    style={{
                      height: 50,
                      width: wp(85),
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      alignSelf: 'center',
                      alignItems: 'center',
                    }}>
                    <Text
                      style={[
                        Styles.body_label,
                        {
                          width: null,
                          fontFamily: FONTS.FAMILY_SEMIBOLD,
                          color: COLOR.BLACK,
                        },
                      ]}>
                      {item.hotel_name}
                    </Text>
                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                      <TouchableOpacity
                        onPress={() => {
                          this.props.navigation.navigate('Rooms', {
                            data: item,
                            id: id,
                            depart: this.state.depart,
                            return: this.state.return,
                          });
                        }}
                        style={{
                          height: 40,
                          aspectRatio: 1,
                          alignItems: 'center',
                          justifyContent: 'center',
                        }}>
                        <Image
                          style={{
                            height: 25,
                            width: 25,
                          }}
                          source={IMAGES.edit}
                        />
                      </TouchableOpacity>
                      <TouchableOpacity
                        onPress={() => {
                          this.props.deleteScheduledStay(item.id, id);
                          let data = this.state.stayData;
                          data.splice(index, 1);
                          this.setState({
                            stayData: data,
                          });
                        }}
                        style={{
                          height: 40,
                          aspectRatio: 1,
                          alignItems: 'center',
                          justifyContent: 'center',
                        }}>
                        <Image
                          style={{
                            height: 25,
                            width: 25,
                          }}
                          source={IMAGES.cross_icon}
                        />
                      </TouchableOpacity>
                    </View>
                  </View>
                  <View
                    style={[
                      Styles.line_view,
                      {width: wp(85), marginVertical: 10},
                    ]}
                  />
                </View>
              </TouchableWithoutFeedback>
            )}
            numColumns={1}
            ListEmptyComponent={() =>
              !this.state.stayData.length ? (
                <Text
                  style={{
                    textAlign: 'center',
                    color: COLOR.VOILET,
                    marginVertical: 40,
                  }}>
                  You did not add any Stay
                </Text>
              ) : null
            }
            keyExtractor={(item, index) => index.toString()}
          />

          <TouchableOpacity
            style={{
              width: wp(85),
              alignSelf: 'center',
              height: 50,
              alignItems: 'center',
              justifyContent: 'center',
              backgroundColor: COLOR.WHITE,
              borderRadius: 10,
              borderColor: COLOR.LIGHT_TEXT,
              borderWidth: 1,
              marginTop: 40,
            }}
            onPress={() => {
              this.setState({modalVisible: false}, (val) => {
                this.props.navigation.navigate('Rooms', {
                  id: id,
                  depart: this.state.depart,
                  return: this.state.return,
                  package_request_id: this.state.package_request_id,
                });
              });
            }}>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <Image
                style={{
                  width: 25,
                  height: 25,
                  borderRadius: 10,

                  marginRight: 5,
                }}
                source={IMAGES.add_plus}
              />

              <Text
                style={[
                  Styles.button_font,
                  {
                    color: COLOR.BLACK,
                    width: null,
                    textAlign: 'center',
                  },
                ]}>
                Add New Stay
              </Text>
            </View>
          </TouchableOpacity>
        </View>
        <TouchableOpacity
          style={{
            width: wp(90),
            alignSelf: 'center',
            height: 50,
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: COLOR.GREEN,
            borderRadius: 10,
            bottom: 20,
            position: 'absolute',
          }}
          onPress={() => {
            if (screens[index + 1]) {
              this.props.navigation.replace(screens[index + 1], {
                screens: screens,
                index: index + 1,
                id: id,
                depart: this.state.depart,
                return: this.state.return,
              });
            } else if (this.state.isPackage) {
              this.props.navigation.goBack();
            } else {
              if (this.state.stayData.length == 0) {
                SimpleToast.show('Add stay to continue');
              } else {
                this.props.navigation.navigate('HomeStack');
              }
            }
          }}>
          <Text
            style={[
              Styles.button_font,
              {
                color: COLOR.WHITE,
                width: null,
                textAlign: 'center',
              },
            ]}>
            Next
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    scheduledStay: state.stayReducer.scheduledStay,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getScheduledStay: (val) => {
      dispatch(StayAction.getScheduledStay(val));
    },
    deleteScheduledStay: (id, tripId) => {
      dispatch(StayAction.deleteScheduledStay(id, tripId));
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(StayScreen);
