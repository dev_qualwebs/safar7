import React from 'react';
import {
  Modal,
  Text,
  View,
  StyleSheet,
  Image,
  Button,
  KeyboardAvoidingView,
} from 'react-native';
import {TextInput, TouchableOpacity} from 'react-native';
import {
  widthPercentageToDP,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import COLOR from '../../../components/styles/Color';
import FONTS from '../../../components/styles/Fonts';
import IMAGES from '../../../components/styles/Images';
import Styles from '../../../components/styles/Styles';
import NavigationBar from '../../commonView/NavigationBar';
import {Dropdown, handleDropdownValue} from '../../commonView/Dropdown';
import {connect} from 'react-redux';
import StayAction from '../../../redux/action/StayAction';
import Auth from '../../../auth';
import DropDownPicker from 'react-native-dropdown-picker';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import { DoneButtonKeyboard } from '../../commonView/CommonView';

const auth = new Auth();
class AddRoom extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      roomType: [],
      bedPreference: [],
      selectedRoomType: null,
      selectedBedPreference: null,
      type_id: null,
      bed_preference_id: null,
      comment: '',
      roomDropdown: false,
      bedPreferenceDropdown: false,
      keyboardVisible: false,
    };

    this.props.getStayBedPreference();
    this.props.getStayRoomType();

    this.handleSelectedDropdownVal = this.handleSelectedDropdownVal.bind(this);
  }

  componentDidMount() {
    if (this.props.stayRoomTypeData.length) {
      this.setState({
        roomType: handleDropdownValue(this.props.stayRoomTypeData, 'roomType'),
      });
    }

    if (this.props.stayBedPreferenceData.length) {
      this.setState({
        bedPreference: handleDropdownValue(
          this.props.stayBedPreferenceData,
          'preference',
        ),
      });
    }
  }

  componentWillReceiveProps(props) {
    if (props.stayBedPreferenceData != this.props.stayBedPreferenceData) {
      if (props.stayBedPreferenceData.length) {
        this.setState(
          {
            bedPreference: handleDropdownValue(
              props.stayBedPreferenceData,
              'preference',
            ),
          },
          () => {
            this.setState({
              selectedBedPreference: this.state.bedPreference[0].value,
              bed_preference_id: this.state.bedPreference[0].id,
            });
          },
        );
      }
    }

    if (props.stayRoomTypeData != this.props.stayRoomTypeData) {
      if (props.stayRoomTypeData.length) {
        this.setState(
          {
            roomType: handleDropdownValue(props.stayRoomTypeData, 'roomType'),
          },
          () => {
            this.setState({
              selectedRoomType: this.state.roomType[0].value,
              type_id: this.state.roomType[0].id,
            });
          },
        );
      }
    }
  }

  handleSelectedDropdownVal = (val, param) => {
    console.log(val, param);
  };

  async handleAddRoom() {
    var rooms = [];
    const value = JSON.parse(await auth.getValue('Rooms'));
    if (value != null) {
      rooms = value;
    }

    let roomTypeIndex = this.state.roomType.findIndex(
      (value) => value.id == this.state.type_id,
    );
    let bedIndex = this.state.bedPreference.findIndex(
      (value) => value.id == this.state.bed_preference_id,
    );
    const room = {
      id: rooms.length,
      name: `${this.state.roomType[roomTypeIndex].label} ${rooms.length + 1}`,
      type_id: this.state.type_id,
      bed_preference_id: this.state.bed_preference_id,
      room_type: this.state.roomType[roomTypeIndex].label,
      size: this.state.bedPreference[bedIndex].label,
      comment: this.state.comment,
    };
    rooms.push(room);
    console.log(rooms);
    auth.setValue('Rooms', rooms);
    this.props.updateMasterState('roomData', rooms);
  }

  render() {
    const {selectedIndex} = this.state;
    return (
      <View style={[Styles.container]}>
       
          <Modal
            animationType={'slide'}
            transparent={true}
            style={{backgroundColor: COLOR.BLACK}}
            visible={this.props.modalVisible}
            onRequestClose={() => {
              console.log('Modal has been closed.');
            }}>
            <View style={[styles.modal,{marginBottom:this.state.keyboardVisible ? 300 : 0}]}>
             
              {/* <KeyboardAwareScrollView
          bounces={false}
          scrollToOverflowEnabled={true}
           style={{flex: 1,flexGrow:1}}
          keyboardShouldPersistTaps={'handled'}
          enableResetScrollToCoords={false}> */}
             <View style={styles.modalBody}>
                <NavigationBar
                  prop={this.props.props}
                  navHeight={40}
                  name={'Add Room'}
                />

                <View
                  style={{
                    width: wp(90),
                    marginVertical: 20,
                    alignSelf: 'center',
                    height: 300,
                  }}>
                  <Text
                    style={[
                      Styles.body_label,
                      {
                        paddingTop: 10,
                        marginBottom: 0,
                        color: COLOR.BLACK,
                        backgroundColor: COLOR.WHITE,
                        fontFamily: FONTS.FAMILY_BOLD,
                      },
                    ]}>
                    {'Room Type'}
                  </Text>
                  <DropDownPicker
                    open={this.state.roomDropdown}
                    setOpen={() =>
                      this.setState({roomDropdown: !this.state.roomDropdown})
                    }
                    items={this.state.roomType}
                    setValue={(callback) =>
                      this.setState((state) => ({
                        type_id: callback(state.value),
                      }))
                    }
                    textStyle={{
                      fontFamily: FONTS.FAMILY_REGULAR,
                    }}
                    labelStyle={{
                      fontFamily: FONTS.FAMILY_REGULAR,
                    }}
                    labelStyle={{
                      fontSize: FONTS.MEDIUM,
                      fontFamily: FONTS.FAMILY_REGULAR,
                      color: COLOR.BLACK,
                      marginLeft: -5,
                    }}
                    dropDownMaxHeight={300}
                    style={{
                      backgroundColor: '#ffffff',
                      shadowOpacity: 0,
                      borderWidth: 0,
                      height: 50,
                      alignSelf: 'center',
                      borderTopLeftRadius: 0,
                      borderTopRightRadius: 0,
                      borderBottomLeftRadius: 0,
                      borderBottomRightRadius: 0,
                    }}
                    dropDownStyle={{
                      backgroundColor: '#fff',
                    }}
                    containerStyle={{
                      borderRadius: 0,
                    }}
                    value={this.state.type_id}
                    zIndex={3995}
                    itemKey="key"
                    searchable={false}
                  />
                  <Text
                    style={[
                      Styles.body_label,
                      {
                        paddingTop: 10,
                        marginBottom: 0,
                        color: COLOR.BLACK,
                        backgroundColor: COLOR.WHITE,
                        fontFamily: FONTS.FAMILY_BOLD,
                      },
                    ]}>
                    {'Bed Preference'}
                  </Text>
                  <DropDownPicker
                    open={this.state.bedPreferenceDropdown}
                    setOpen={() =>
                      this.setState({
                        bedPreferenceDropdown:
                          !this.state.bedPreferenceDropdown,
                      })
                    }
                    items={this.state.bedPreference}
                    setValue={(callback) =>
                      this.setState((state) => ({
                        bed_preference_id: callback(state.value),
                      }))
                    }
                    textStyle={{
                      fontFamily: FONTS.FAMILY_REGULAR,
                    }}
                    labelStyle={{
                      fontFamily: FONTS.FAMILY_REGULAR,
                    }}
                    labelStyle={{
                      fontSize: FONTS.MEDIUM,
                      fontFamily: FONTS.FAMILY_REGULAR,
                      color: COLOR.BLACK,
                      marginLeft: -5,
                    }}
                    dropDownMaxHeight={300}
                    style={{
                      backgroundColor: '#ffffff',
                      shadowOpacity: 0,
                      borderWidth: 0,
                      height: 50,
                      alignSelf: 'center',
                      borderTopLeftRadius: 0,
                      borderTopRightRadius: 0,
                      borderBottomLeftRadius: 0,
                      borderBottomRightRadius: 0,
                    }}
                    dropDownStyle={{
                      backgroundColor: '#fff',
                    }}
                    containerStyle={{
                      borderRadius: 0,
                    }}
                    value={this.state.bed_preference_id}
                    zIndex={3990}
                    itemKey="key"
                    searchable={false}
                  />

                  <View
                    style={[
                      Styles.line_view,
                      {width: wp(90), marginVertical: 10},
                    ]}
                  />
                  <View
                    style={{
                      borderWidth: 1,
                      borderColor: COLOR.GRAY,
                      borderRadius: 10,
                      height: 80,
                      marginVertical: 20,
                      marginBottom: this.state.keyboardVisible ? 300 : 0,
                    }}>
                    <TextInput
                    inputAccessoryViewID={'uniqueID'}
                      multiline={true}
                      placeholderTextColor={COLOR.LIGHT_TEXT_COLOR}
                      placeholder={'Comments'}
                      onFocus={() => this.setState({keyboardVisible: true})}
                      onBlur={() => this.setState({keyboardVisible: false})}
                      value={this.state.comment}
                      onChangeText={(text) => this.setState({comment: text})}
                      style={[{margin: 10}]}
                    />
                    <DoneButtonKeyboard />
                  </View>
                </View>
                <View style={{width: wp(90)}}>
                  <TouchableOpacity
                    style={{
                      width: wp(90),
                      alignSelf: 'center',
                      height: 50,
                      alignItems: 'center',
                      justifyContent: 'center',
                      backgroundColor: COLOR.WHITE,
                      borderRadius: 10,
                      marginBottom: 20,
                      borderColor: COLOR.LIGHT_TEXT,
                      borderWidth: 1,
                    }}
                    onPress={() => {
                      this.setState({modalVisible: false}, (val) => {
                        this.handleAddRoom();
                        this.props.updateMasterState('addRoomModal', false);
                        this.props.updateMasterState('roomsModal', true);
                      });
                    }}>
                    <Text
                      style={[
                        Styles.button_font,
                        {
                          color: COLOR.BLACK,
                          width: null,
                          textAlign: 'center',
                        },
                      ]}>
                      Add Room
                    </Text>
                  </TouchableOpacity>
                </View>
                </View>
                {/* </KeyboardAwareScrollView> */}
              
            </View>
          </Modal>
        
      </View>
    );
  }
}

const styles = StyleSheet.create({
  modal: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
  },
  modalBody: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    backgroundColor: COLOR.WHITE,
    borderWidth: 1,
    width: wp(100),
    marginTop: 50,
    borderColor: COLOR.BORDER,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
  image: {
    height: 120,
    width: 120,
    alignSelf: 'center',
    marginVertical: 20,
  },
  headingLabel: {
    fontFamily: FONTS.FAMILY_BOLD,
    fontSize: FONTS.MEDIUM,
    color: COLOR.BLACK,
  },
  singleLineView: {
    marginVertical: 20,
    borderBottomColor: COLOR.GRAY,
    width: wp(100),
    alignSelf: 'center',
    borderBottomWidth: 1,
  },
  borderView: {
    borderRadius: 10,
    borderWidth: 3,
    width: wp(90),
    borderColor: COLOR.GRAY,
    alignSelf: 'center',
  },
});

const mapStateToProps = (state, ownProps) => {
  return {
    stayRoomTypeData: state.stayReducer.stayRoomTypeData,
    stayBedPreferenceData: state.stayReducer.stayBedPreferenceData,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getStayRoomType: (val) => {
      dispatch(StayAction.getStayRoomType(val));
    },
    getStayBedPreference: (val) => {
      dispatch(StayAction.getStayBedPreference(val));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(AddRoom);
