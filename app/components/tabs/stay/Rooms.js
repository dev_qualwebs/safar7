import React from 'react';
import {Modal, Text, View, StyleSheet, Image, Button} from 'react-native';
import {
  FlatList,
  TouchableOpacity,
  TouchableWithoutFeedback,
} from 'react-native';
import {widthPercentageToDP, widthPercentageToDP as wp} from 'react-native-responsive-screen';
import COLOR from '../../../components/styles/Color';
import FONTS from '../../../components/styles/Fonts';
import IMAGES from '../../../components/styles/Images';
import Styles from '../../../components/styles/Styles';
import NavigationBar from '../../commonView/NavigationBar';
import Auth from '../../../auth';
import AddRoom from './AddRoom';
import AddNewStay from './AddNewStay';
import SimpleToast from 'react-native-simple-toast';

let auth = new Auth();

export default class Rooms extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: true,
      addRoomModal: false,
      roomsModal: false,
      selectedIndex: null,
      roomData: [],
      selectedRoom: null,
      id: this.props.route.params && this.props.route.params.id,
      data: null,
    };
  }

  updateMasterState = (attrName, value) => {
    this.setState({[attrName]: value});
  };

  async componentDidMount() {
    const data = await auth.getValue('Rooms');
    const value = JSON.parse(data);
    this.setState(
      {
        roomData: value ? value : [],
      },
      () => {
        console.log('State data', this.state.roomData);
      },
    );

    if (this.props.route && this.props.route.params.data) {
      let data = this.props.route.params.data;
      this.setState({
        city: data.city,
        number_of_days: data.number_of_days,
        checkInDate: data.check_in,
        checkOutDate: data.check_out,
        check_in_flexibility: data.check_in_flexibility,
        check_out_flexibility: data.check_out_flexibility,
      });
    }
  }

  handleClearRooms() {
    var rooms = [];
    auth.setValue('Rooms', rooms);
    this.setState({roomData: rooms});
  }

  render() {
    const selectedRoom = this.state.roomData[this.state.selectedIndex];
    return (
      <View>
        <AddNewStay
          id={this.state.id}
          props={this.props}
          selectedRoom={this.state.selectedRoom}
          updateMasterState={this.updateMasterState}
          modalVisible={this.state.modalVisible}
        />
        <this.RoomsConst
          roomData={this.state.roomData}
          props={this.props}
          modalVisible={this.state.roomsModal}
          updateMasterState={this.updateMasterState}
          handleScreen={this.handleScreen}
          selectedIndex={this.state.selectedIndex}
        />
        <AddRoom
          data={this.state.data}
          props={this.props}
          updateMasterState={this.updateMasterState}
          updateRoomData={this.updateRoomData}
          modalVisible={this.state.addRoomModal}
        />
      </View>
    );
  }

  RoomsConst = (props) => {
    const selectedIndex = this.state.selectedIndex;

    return (
      <View style={Styles.container}>
        <Modal
          animationType={'slide'}
          transparent={true}
          style={{backgroundColor: COLOR.BLACK}}
          visible={props.modalVisible}
          onRequestClose={() => {
            console.log('Modal has been closed.');
          }}>
          <View style={styles.modal}>
            <View style={styles.modalBody}>
              <NavigationBar prop={props.props} navHeight={40} name={'Rooms'} />
              <View
                style={{
                  flex: 1,
                  width: wp(90),
                  alignSelf: 'center',
                  paddingVertical: 10,
                  marginVertical: 20,
                  height: 400,
                }}>
                <View>
                  <FlatList
                    numColumns={2}
                    showsHorizontalScrollIndicator={false}
                    showsVerticalScrollIndicator={false}
                    data={this.state.roomData}
                    renderItem={({item, index}) => {
                      return (
                        <TouchableWithoutFeedback
                          onPress={() => {
                            this.setState({selectedIndex: item.id});
                            console.log(selectedIndex);
                          }}>
                          <View
                            style={{
                              borderRadius: 10,
                              borderWidth: 1,
                              margin: 5,
                              borderColor:
                                selectedIndex == item.id
                                  ? COLOR.GREEN
                                  : COLOR.GRAY,
                              backgroundColor:
                                selectedIndex == item.id
                                  ? '#05778F10'
                                  : COLOR.WHITE,
                              alignSelf: 'center',
                              alignItems: 'center',
                              justifyContent: 'center',
                              // flex: 1,
                              width : widthPercentageToDP(90)/2 - 10,
                            }}>
                            <Image
                              source={IMAGES.no_of_rooms}
                              style={{
                                height: 50,
                                width: 50,
                                marginVertical: 10,
                                tintColor:
                                  selectedIndex == item.id
                                    ? COLOR.GREEN
                                    : COLOR.VOILET,
                              }}
                            />
                            <Text
                              style={{
                                marginBottom: 10,
                                textAlign: 'center',
                                fontFamily: FONTS.FAMILY_BOLD,
                                fontSize: FONTS.REGULAR,
                              }}>
                              {item.name}
                            </Text>
                            <Text
                              style={{
                                marginBottom: 10,
                                textAlign: 'center',
                                color: COLOR.VOILET,
                              }}>
                              {item.room_type}
                            </Text>
                            <Text
                              style={{
                                marginBottom: 10,
                                textAlign: 'center',
                                color: COLOR.VOILET,
                              }}>
                              {item.size}
                            </Text>
                          </View>
                        </TouchableWithoutFeedback>
                      );
                    }}
                    ListEmptyComponent={() =>
                      !this.state.roomData.length ? (
                        <Text
                          style={{
                            textAlign: 'center',
                            color: COLOR.VOILET,
                            marginVertical: 40,
                          }}>
                          You did not add any Stay
                        </Text>
                      ) : null
                    }
                    keyExtractor={(item, index) => index.toString()}
                  />
                  <View
                    style={[
                      Styles.line_view,
                      {width: wp(80), alignSelf: 'center', marginVertical: 10},
                    ]}
                  />
                </View>
              </View>
              <View style={{width: wp(90), marginVertically: 10}}>
                <TouchableOpacity
                  style={{
                    width: wp(90),
                    alignSelf: 'center',
                    height: 50,
                    alignItems: 'center',
                    justifyContent: 'center',
                    backgroundColor: COLOR.WHITE,
                    borderRadius: 10,
                    marginBottom: 20,
                    borderColor: COLOR.LIGHT_TEXT,
                    borderWidth: 1,
                    marginVertical: 20,
                  }}
                  onPress={() => {
                    this.setState({
                      roomsModal: false,
                      addRoomModal: true,
                    });
                  }}>
                  <View style={{flexDirection: 'row', alignItems: 'center'}}>
                    <Image
                      style={{
                        width: 25,
                        height: 25,
                        borderRadius: 10,
                        marginRight: 5,
                      }}
                      source={IMAGES.add_plus}
                    />
                    <Text
                      style={[
                        Styles.button_font,
                        {
                          color: COLOR.BLACK,
                          width: null,
                          textAlign: 'center',
                        },
                      ]}>
                      Add More Room
                    </Text>
                  </View>
                </TouchableOpacity>
                <View
                  style={{
                    marginBottom: 20,
                    height: 50,
                    flexDirection: 'row',
                    width: wp(90),
                    alignSelf: 'center',
                  }}>
                  <TouchableOpacity
                    onPress={() => {
                      this.handleClearRooms();
                    }}
                    style={{
                      flex: 1,
                      backgroundColor: COLOR.WHITE,
                      marginRight: 10,
                      borderRadius: 5,
                      borderWidth: 1,
                      borderColor: COLOR.GRAY,
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}>
                    <Text
                      style={[
                        Styles.button_font,
                        {
                          color: COLOR.BLACK,
                          width: null,
                          textAlign: 'center',
                        },
                      ]}>
                      Clear All
                    </Text>
                  </TouchableOpacity>

                  <TouchableOpacity
                    onPress={() => {
                      if (this.state.selectedIndex != null) {
                        const room =
                          this.state.roomData[this.state.selectedIndex];
                        this.setState({
                          modalVisible: true,
                          roomsModal: false,
                          selectedRoom: room,
                        });
                      } else {
                        SimpleToast.show('Please Select or add rooms first.');
                      }
                    }}
                    style={{
                      flex: 1,
                      backgroundColor: COLOR.GREEN,
                      borderRadius: 5,
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}>
                    <Text
                      style={[
                        Styles.button_font,
                        {
                          color: COLOR.WHITE,
                          width: null,
                          textAlign: 'center',
                        },
                      ]}>
                      Apply
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </View>
        </Modal>
      </View>
    );
  };
}

const styles = StyleSheet.create({
  modal: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
  },
  modalBody: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    backgroundColor: COLOR.WHITE,
    borderWidth: 1,
    width: wp(100),
    marginTop: 50,
    borderColor: COLOR.BORDER,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    height: 500,
  },
  image: {
    height: 120,
    width: 120,
    alignSelf: 'center',
    marginVertical: 20,
  },
  headingLabel: {
    fontFamily: FONTS.FAMILY_BOLD,
    fontSize: FONTS.MEDIUM,
    color: COLOR.BLACK,
  },
  singleLineView: {
    marginVertical: 20,
    borderBottomColor: COLOR.GRAY,
    width: wp(100),
    alignSelf: 'center',
    borderBottomWidth: 1,
  },
  borderView: {
    borderRadius: 10,
    borderWidth: 3,
    width: wp(90),
    borderColor: COLOR.GRAY,
    alignSelf: 'center',
  },
});
