import React from 'react';
import {Modal, Text, View, StyleSheet, Image, Button} from 'react-native';
import {
    TextInput,
    TouchableOpacity,
} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import COLOR from '../../../components/styles/Color';
import FONTS from '../../../components/styles/Fonts';
import IMAGES from '../../../components/styles/Images';
import Styles from '../../../components/styles/Styles';
import NavigationBar from '../../commonView/NavigationBar';
import {Dropdown, handleDropdownValue} from "../../commonView/Dropdown";
import {connect} from "react-redux";
import StayAction from "../../../redux/action/StayAction";


class EditStay extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            roomType: [],
            bedPreference: [],
            selectedRoomType: null,
            selectedBedPreference: null,
        };

        this.props.getStayBedPreference();
        this.props.getStayRoomType();

        this.handleSelectedDropdownVal = this.handleSelectedDropdownVal.bind(this);
    }

    componentDidMount() {
        if (this.props.stayRoomTypeData.length) {
            this.setState({
                roomType: handleDropdownValue(this.props.stayRoomTypeData, 'roomType')
            })
        }

        if (this.props.stayBedPreferenceData.length) {
            this.setState({
                bedPreference: handleDropdownValue(this.props.stayBedPreferenceData, 'preference')
            })
        }
    }

    componentWillReceiveProps(props) {
        if (props.stayBedPreferenceData != this.props.stayBedPreferenceData) {
            if (props.stayBedPreferenceData.length) {
                this.setState({
                    bedPreference: handleDropdownValue(props.stayBedPreferenceData, 'preference')
                })
            }
        }

        if (props.stayRoomTypeData != this.props.stayRoomTypeData) {
            if (props.stayRoomTypeData.length) {
                this.setState({
                    roomType: handleDropdownValue(props.stayRoomTypeData, 'roomType')
                })
            }
        }
    }

    handleSelectedDropdownVal = (val, param) => {
        console.log(val, param)
    }

    handleAddRoom = () => {

    }

    render() {
        const {selectedIndex} = this.state;
        return (
            <View style={Styles.container}>
                <Modal
                    animationType={'slide'}
                    transparent={true}
                    style={{backgroundColor: COLOR.BLACK}}
                    visible={this.state.modalVisible}
                    onRequestClose={() => {
                        console.log('Modal has been closed.');
                    }}>
                    <View style={styles.modal}>
                        <View style={styles.modalBody}>
                            <NavigationBar
                                prop={this.props}
                                navHeight={40}
                                name={'Add New Stay'}
                            />
                            <View style={{alignSelf: 'center', width: wp(90), marginTop: 10}}>
                                <View
                                    style={{
                                        flexDirection: 'row',
                                        height: 50,
                                        alignItems: 'center',
                                        marginBottom: 0,
                                    }}>
                                    <Image
                                        style={{height: 30, width: 30, marginRight: 10}}
                                        source={IMAGES.pinpoint}
                                        resizeMode={'contain'}
                                    />
                                    <View>
                                        <Text style={Styles.small_label}>City Name</Text>
                                        <Text style={Styles.body_label}>Denpasar</Text>
                                    </View>
                                </View>
                                <View
                                    style={[
                                        Styles.line_view,
                                        {width: wp(90), marginVertical: 10},
                                    ]}
                                />
                                <View
                                    style={{
                                        flexDirection: 'row',
                                        alignItems: 'center',
                                        marginBottom: 0,
                                    }}>
                                    <Image
                                        style={{height: 30, width: 30, marginRight: 10}}
                                        source={IMAGES.departure_date}
                                        resizeMode={'contain'}
                                    />
                                    <View>
                                        <Text style={Styles.small_label}>Check-In Date</Text>
                                        <Text style={Styles.body_label}>7 Jan 2021</Text>
                                    </View>
                                </View>
                                <View
                                    style={[
                                        Styles.line_view,
                                        {width: wp(90), marginVertical: 10},
                                    ]}
                                />
                                <View
                                    style={{
                                        flexDirection: 'row',
                                        alignItems: 'center',
                                        marginBottom: 0,
                                    }}>
                                    <Image
                                        style={{height: 30, width: 30, marginRight: 10}}
                                        source={IMAGES.return_date}
                                        resizeMode={'contain'}
                                    />
                                    <View>
                                        <Text style={Styles.small_label}>Check-Out Date</Text>
                                        <Text style={Styles.body_label}>7 Jan 2021</Text>
                                    </View>
                                </View>
                                <View
                                    style={[
                                        Styles.line_view,
                                        {width: wp(90), marginVertical: 10},
                                    ]}
                                />
                                <View
                                    style={{
                                        flexDirection: 'row',
                                        alignItems: 'center',
                                        marginBottom: 0,
                                    }}>
                                    <Image
                                        style={{height: 30, width: 30, marginRight: 10}}
                                        source={IMAGES.no_of_rooms}
                                        resizeMode={'contain'}
                                    />
                                    <View>
                                        <Text style={Styles.small_label}>Rooms</Text>
                                        <Text style={Styles.body_label}>1 Room</Text>
                                    </View>
                                </View>
                            </View>
                            <View style={{width: wp(90), marginTop: 20, flexDirection:'row', alignItems:'center'}}>
                                <TouchableOpacity
                                    style={{
                                        width: wp(90),
                                        alignSelf: 'center',
                                        height: 50,
                                        alignItems: 'center',
                                        justifyContent: 'center',
                                        backgroundColor: COLOR.WHITE,
                                        borderRadius: 10,
                                        marginBottom: 20,
                                        borderColor: COLOR.LIGHT_TEXT,
                                        borderWidth: 1,
                                    }}
                                    onPress={() => {
                                        this.setState({modalVisible: false}, (val) => {
                                            this.props.navigation.navigate('StayOption');
                                        });
                                    }}>
                                    <Text
                                        style={[
                                            Styles.button_font,
                                            {
                                                color: COLOR.BLACK,
                                                width: null,
                                                textAlign: 'center',
                                            },
                                        ]}>
                                        Cancel
                                    </Text>
                                </TouchableOpacity>

                            </View>
                        </View>
                    </View>
                </Modal>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    modal: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'flex-end',
        backgroundColor: 'rgba(0, 0, 0, 0.7)',


    },
    modalBody: {
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        backgroundColor: COLOR.WHITE,
        borderWidth: 1,
        width: wp(100),
        marginTop: 50,
        borderColor: COLOR.BORDER,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,

    },
    image: {
        height: 120,
        width: 120,
        alignSelf: 'center',
        marginVertical: 20,
    },
    headingLabel: {
        fontFamily: FONTS.FAMILY_BOLD,
        fontSize: FONTS.MEDIUM,
        color: COLOR.BLACK,
    },
    singleLineView: {
        marginVertical: 20,
        borderBottomColor: COLOR.GRAY,
        width: wp(100),
        alignSelf: 'center',
        borderBottomWidth: 1,
    },
    borderView: {
        borderRadius: 10,
        borderWidth: 3,
        width: wp(90),
        borderColor: COLOR.GRAY,
        alignSelf: 'center',
    },
});

const mapStateToProps = (state, ownProps) => {
    return {
        stayRoomTypeData: state.stayReducer.stayRoomTypeData,
        stayBedPreferenceData: state.stayReducer.stayBedPreferenceData,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        getStayRoomType: (val) => {
            dispatch(StayAction.getStayRoomType(val));
        },
        getStayBedPreference: (val) => {
            dispatch(StayAction.getStayBedPreference(val));
        },
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(EditStay);












