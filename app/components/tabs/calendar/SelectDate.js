import React from 'react';
import {Modal, Text, View, StyleSheet} from 'react-native';
import {TouchableOpacity} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import COLOR from '../../../components/styles/Color';
import FONTS from '../../../components/styles/Fonts';
import Styles from '../../../components/styles/Styles';
import NavigationBar from '../../commonView/NavigationBar';
import {FloatingTitleTextInputField} from '../../../helper/FloatingTitleTextInputField';
import CalendarPicker from 'react-native-calendar-picker';
import Moment from 'moment';
import moment from 'moment';
import FlexibilityView from './FlexibilityView';
import {getEndDateTimestamp} from '../../commonView/Helpers';

export default class SelectDate extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      packageDetail: null,
      modalVisible: true,
      selectedStartDate: new Date(),
      selectedEndDate: new Date(),
      previousTitle: '',
      nextTitle: '',
      allowRangeSelection: true,
      fromFlexibility: 0,
      toFlexibility: 0,
      selector: null,
      minDate: new Date(0),
      maxDate: new Date(5147962581000),
    };
    this.onDateChange = this.onDateChange.bind(this);
  }

  componentDidMount() {
    this.initialiseCalendar();
    if (this.props.allowRangeSelection == false) {
      this.setState({
        allowRangeSelection: this.props.allowRangeSelection,
      });
    }

    if (this.props.selector != null) {
      this.setState({
        selector: this.props.selector,
      });
    }

    if (this.props.minDate != null) {
      this.setState(
        {
          minDate: this.props.minDate,
          selectedStartDate : this.props.minDate
        },
        () => console.log('min --', this.state.minDate),
      );
    }

    if (this.props.maxDate != null || this.props.minDate != '') {
      this.setState({
        maxDate: this.props.maxDate,
        selectedEndDate : this.props.maxDate
      });
    }
  }

  componentDidUpdate(prevProps) {
    if (this.props.selector != prevProps.selector) {
      this.setState({
        selector: this.props.selector,
      });
    }
  }

  initialiseCalendar(date) {
    let day = date ? Moment(date).toDate() : this.state.selectedStartDate;
    let previousMonth = day.setMonth(day.getMonth() - 1);
    let nextMonth = day.setMonth(day.getMonth() + 2);

    this.setState({
      // selectedStartDate: date,
      previousTitle: '< ' + Moment(previousMonth).format('MMM YYYY'),
      nextTitle: Moment(nextMonth).format('MMM YYYY') + ' >',
    });
  }

  setFlexibility(val, id) {
    if (val == 1) {
      this.setState({
        fromFlexibility: id,
      });
    } else if (val == 2) {
      this.setState({
        toFlexibility: id,
      });
    }
    console.log(val + ' ' + id);
  }

  onDateChange(date, type) {
    if (type === 'END_DATE') {
      this.setState({
        selectedEndDate: date,
      });
    } else {
      this.setState({
        selectedStartDate: date,
        selectedEndDate: null,
      });
    }
  }

  modalBack = () => {
    this.setState({modalVisible: false}, (val) => {
      this.props.updateMasterState('openCalendar', false);
    });
  };

  render() {
    const {selectedStartDate, previousTitle, nextTitle} = this.state;
    const startDate = selectedStartDate ? selectedStartDate.toString() : '';

    return (
      <View style={Styles.container}>
        <Modal
          animationType={'slide'}
          transparent={true}
          style={{backgroundColor: COLOR.BLACK}}
          visible={this.state.modalVisible}
          onRequestClose={() => {
            console.log('Modal has been closed.');
          }}>
          <View style={styles.modal}>
            <View style={styles.modalBody}>
              <NavigationBar
                handleBack={this.props.modal ? this.modalBack : null}
                prop={this.props.props}
                navHeight={40}
                name={'Select Dates'}
              />
              <View style={{flex: 1}}>
                <CalendarPicker
                  allowRangeSelection={this.state.allowRangeSelection}
                  startFromMonday={true}
                  restrictMonthNavigation={true}
                  disabledDates={
                    this.props.minDate
                      ? (date) => {
                          let startDate = this.state.minDate;
                          let endDate = this.props.maxDate
                            ? this.state.maxDate
                            : 5147962581000;
                          if (
                            date.isSameOrAfter(startDate) &&
                            date.isSameOrBefore(endDate)
                          ) {
                            return false;
                          } else {
                            return true;
                          }
                        }
                      : null
                  }
                  selectedDayColor={COLOR.GREEN}
                  todayBackgroundColor={COLOR.GRAY}
                  showDayStragglers={true}
                  allowBackwardRangeSelect={false}
                  initialDate={this.state.selectedStartDate}
                  selectedDayTextColor={COLOR.WHITE}
                  nextTitle={nextTitle}
                  minDate={this.state.minDate}
                  maxDate={this.state.maxDate}
                  maxRangeDuration={10000}
                  previousTitle={previousTitle}
                  horizontal={true}
                  enableSwipe={true}
                  scaleFactor={375}
                  onDateChange={this.onDateChange}
                  onMonthChange={(date) => this.initialiseCalendar(date)}
                  previousTitleStyle={{color: COLOR.VOILET}}
                  nextTitleStyle={{color: COLOR.VOILET}}
                  monthYearHeaderWrapperStyle={{
                    height: 40,
                    backgroundColor: 'transparent',
                  }}
                  headerWrapperStyle={{backgroundColor: COLOR.GREEN_OPACITY}}
                />

                <FlexibilityView
                  startDate={this.state.selectedStartDate}
                  endDate={
                    this.state.selector == 1
                      ? this.state.selectedStartDate
                      : this.state.selectedEndDate
                  }
                  rangeSelection={this.props.allowRangeSelection}
                  setFlexibility={this.setFlexibility.bind(this)}
                  selector={this.state.selector}
                />
              </View>
              <View
                style={{
                  marginVertical: 20,
                  height: 50,
                  flexDirection: 'row',
                  width: wp(90),
                  alignSelf: 'center',
                }}>
                <TouchableOpacity
                  onPress={() => {
                    this.setState({
                      selectedStartDate: new Date(),
                      selectedEndDate: new Date(),
                    });
                  }}
                  style={{flex: 1, marginRight: 10}}>
                  <View
                    style={{
                      flex: 1,
                      alignItems: 'center',
                      justifyContent: 'center',
                      borderRadius: 10,
                      height: 50,
                      borderColor: COLOR.GRAY,
                      borderWidth: 1,
                      backgroundColor: COLOR.WHITE,
                    }}>
                    <Text style={[Styles.button_font, {color: COLOR.BLACK}]}>
                      Clear All
                    </Text>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => {
                    this.setState({modalVisible: false}, (val) => {
                      this.props.updateMasterState('openCalendar', false);
                      if (this.props.attrName != null) {
                        this.props.updateMasterState(
                          this.props.attrName,
                          moment(this.state.selectedStartDate).format(
                            'YYYY-MM-DD',
                          ),
                        );
                        if (this.props.attrFlexibility) {
                          this.props.updateMasterState(
                            this.props.attrFlexibility,
                            this.state.fromFlexibility,
                          );
                        }
                      } else {
                        if (this.state.selector == 0) {
                          this.props.updateMasterState(
                            'checkInDate',
                            moment(this.state.selectedStartDate).format(
                              'YYYY-MM-DD',
                            ),
                          );
                          this.props.updateMasterState(
                            'fromFlexibility',
                            this.state.fromFlexibility,
                          );
                        } else if (this.state.selector == 1) {
                          this.props.updateMasterState(
                            'checkOutDate',
                            moment(this.state.selectedStartDate).format(
                              'YYYY-MM-DD',
                            ),
                          );
                          this.props.updateMasterState(
                            'toFlexibility',
                            this.state.toFlexibility,
                          );
                        } else {
                          this.props.updateMasterState(
                            'checkInDate',
                            moment(this.state.selectedStartDate).format(
                              'YYYY-MM-DD',
                            ),
                          );
                          this.props.updateMasterState(
                            'checkOutDate',
                            moment(this.state.selectedEndDate).format(
                              'YYYY-MM-DD',
                            ),
                          );
                          this.props.updateMasterState(
                            'toFlexibility',
                            this.state.toFlexibility,
                          );
                          this.props.updateMasterState(
                            'fromFlexibility',
                            this.state.fromFlexibility,
                          );
                        }
                      }
                    });
                  }}
                  style={{flex: 1}}>
                  <View
                    style={{
                      flex: 1,
                      alignItems: 'center',
                      justifyContent: 'center',
                      borderRadius: 10,
                      height: 50,
                      backgroundColor: COLOR.GREEN,
                    }}>
                    <Text style={[Styles.button_font, {color: COLOR.WHITE}]}>
                      Apply
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  modal: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
  },
  modalBody: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    backgroundColor: COLOR.WHITE,
    borderWidth: 1,
    width: wp(100),
    borderColor: COLOR.BORDER,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    flex: 1,
    marginTop: 50,
  },
  image: {
    height: 120,
    width: 120,
    alignSelf: 'center',
    marginVertical: 20,
  },
  headingLabel: {
    fontFamily: FONTS.FAMILY_BOLD,
    fontSize: FONTS.MEDIUM,
    color: COLOR.BLACK,
  },
  singleLineView: {
    marginVertical: 20,
    borderBottomColor: COLOR.GRAY,
    width: wp(100),
    alignSelf: 'center',
    borderBottomWidth: 1,
  },
  borderView: {
    borderRadius: 10,
    borderWidth: 1,
    width: wp(90),
    borderColor: COLOR.GRAY,
    alignSelf: 'center',
  },
});
