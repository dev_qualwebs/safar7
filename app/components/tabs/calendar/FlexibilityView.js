import React, {useState} from 'react';
import {Image} from 'react-native';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import {widthPercentageToDP} from 'react-native-responsive-screen';
import COLOR from '../../styles/Color';
import IMAGES from '../../styles/Images';
import moment from 'moment';

//handled the flexibility view here
const FlexibilityView = ({
  setFlexibility,
  rangeSelection,
  selector,
  startDate,
  endDate,
}) => {
  const [selected, setSelected] = useState(0);
  const [selectedFrom, setSelectedFrom] = useState(0);
  const [selectedTo, setSelectedTo] = useState(0);

  function getBackgroundColor(id) {
    if (selected == 1 && id == selectedFrom) {
      return COLOR.GREEN;
    } else if (selected == 2 && id == selectedTo) {
      return COLOR.GREEN;
    } else {
      return COLOR.GRAY;
    }
  }

  function getTextColor(id) {
    if (selected == 1 && id == selectedFrom) {
      return COLOR.WHITE;
    } else if (selected == 2 && id == selectedTo) {
      return COLOR.WHITE;
    } else {
      return COLOR.BLACK;
    }
  }
  function getSelectedString(id) {
    switch (id) {
      case 0:
        return 'Exact Date';
      case 1:
        return '± 1 day';
      case 2:
        return '± 2 day';
      case 3:
        return '± 3 day';
    }
  }

  function setSelection(id) {
    if (selected == 1) {
      setSelectedFrom(id);
      setFlexibility(selected, id);
    } else if (selected == 2) {
      setSelectedTo(id);
      setFlexibility(selected, id);
    }
  }

  if (selector != null || rangeSelection) {
    return (
      <View style={[styles.container, {height: selected == 0 ? 80 : 180,marginBottom:50}]}>
        <View style={styles.rowView}>
          {selector == 0 || rangeSelection ? (
            <TouchableOpacity
              onPress={() => (selected == 1 ? setSelected(0) : setSelected(1))}
              style={styles.selectView}>
              <View style={styles.optionView}>
                <Text style={styles.titleText}>Depart</Text>
                <Text style={styles.flexibilityText}>
                  {startDate
                    ? moment(startDate).format('YYYY-MM-DD')
                    : 'Select Date'}
                </Text>
                <Text style={styles.selectedText}>
                  {getSelectedString(selectedFrom)}
                </Text>
              </View>
              <Image
                style={styles.arrowImage}
                source={selected == 1 ? IMAGES.down_arrow : IMAGES.right_arrow}
              />
            </TouchableOpacity>
          ) : null}
          <View style={styles.vr} />
          {selector == 1 || rangeSelection ? (
            <TouchableOpacity
              onPress={() => (selected == 2 ? setSelected(0) : setSelected(2))}
              style={styles.selectView}>
              <View style={styles.optionView}>
                <Text style={styles.titleText}>Return</Text>
                <Text style={styles.flexibilityText}>
                  {endDate
                    ? moment(endDate).format('YYYY-MM-DD')
                    : 'Select Date'}
                </Text>
                <Text style={styles.selectedText}>
                  {getSelectedString(selectedTo)}
                </Text>
              </View>
              <Image
                style={styles.arrowImage}
                source={selected == 2 ? IMAGES.down_arrow : IMAGES.right_arrow}
              />
            </TouchableOpacity>
          ) : null}
        </View>
        <View style={{flex: 1}}>
          {selected == 0 ? null : (
            <View style={{marginTop: 20}}>
              <View style={styles.bottomRow}>
                <TouchableOpacity
                  onPress={() => setSelection(0)}
                  style={[
                    styles.button,
                    {backgroundColor: getBackgroundColor(0)},
                  ]}>
                  <Text style={[styles.buttonText, {color: getTextColor(0)}]}>
                    Exact Day
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => setSelection(1)}
                  style={[
                    styles.button,
                    {backgroundColor: getBackgroundColor(1)},
                  ]}>
                  <Text style={[styles.buttonText, {color: getTextColor(1)}]}>
                    ± 1 day
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => setSelection(2)}
                  style={[
                    styles.button,
                    {backgroundColor: getBackgroundColor(2)},
                  ]}>
                  <Text style={[styles.buttonText, {color: getTextColor(2)}]}>
                    ± 2 day
                  </Text>
                </TouchableOpacity>
              </View>
              <View style={styles.bottomRow}>
                <TouchableOpacity
                  onPress={() => setSelection(3)}
                  style={[
                    styles.button,
                    {backgroundColor: getBackgroundColor(3)},
                  ]}>
                  <Text style={[styles.buttonText, {color: getTextColor(3)}]}>
                    ± 3 day
                  </Text>
                </TouchableOpacity>
                <View style={[styles.button]} />
                <View style={[styles.button]} />
              </View>
            </View>
          )}
        </View>
      </View>
    );
  } else {
    return null;
  }
};

const styles = StyleSheet.create({
  container: {
    width: widthPercentageToDP(90),
    borderRadius: 10,
    borderWidth: 1,
    borderColor: COLOR.LIGHT_TEXT_COLOR,
    alignSelf: 'center',
    marginVertical: 10,
    height: 150,
  },
  titleText: {
    marginHorizontal: 10,
    fontSize: 12,
    color: COLOR.LIGHT_TEXT_COLOR,
    marginTop: 15,
    marginBottom: 10,
  },
  selectedText: {
    marginVertical: 5,
    marginHorizontal: 10,
    fontSize: 15,
    fontWeight: '400',
    color: COLOR.TEXT_COLOR,
  },
  rowView: {flexDirection: 'row', height: 60},
  selectView: {flex: 1, justifyContent: 'space-between', flexDirection: 'row'},
  vr: {width: 1, height: 80, backgroundColor: COLOR.GRAY},
  arrowImage: {
    width: 15,
    height: 15,
    alignSelf: 'flex-end',
    marginRight: 10,
    marginBottom: 10,
  },
  optionView: {
    flex: 1,
    justifyContent: 'space-evenly',
  },
  bottomRow: {
    height: 40,
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    alignItems: 'center',
    alignSelf: 'center',
    marginTop: 5,
  },
  button: {
    flex: 1,
    height: 40,
    marginHorizontal: 5,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 5,
  },
  flexibilityText: {
    marginHorizontal: 10,
    fontSize: 17,
    fontWeight: '500',
    color: COLOR.BLACK,
    marginVertical: 5,
  },
});

export default FlexibilityView;
