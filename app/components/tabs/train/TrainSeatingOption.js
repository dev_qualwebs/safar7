import React from 'react';
import {Modal, Text, View, StyleSheet, Image} from 'react-native';
import {
  ScrollView,
  TextInput,
  TouchableOpacity,
  FlatList,
  TouchableWithoutFeedback,
} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import COLOR from '../../styles/Color';
import FONTS from '../../styles/Fonts';
import IMAGES from '../../styles/Images';
import Styles from '../../styles/Styles';
import NavigationBar from '../../commonView/NavigationBar';
import {D_TRAIN_SEATING, D_TRAIN_SEATING_TYPE} from '../../../helper/Constants';
import Switch from '../../commonView/Switch';

export default class TrainSeatingOption extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: true,
      selectedPosition: null,
      selectedType: null,
      seating_together: 0,
    };
  }

  componentDidMount() {
    if (this.props.route.params.selected) {
      let selected = this.props.route.params.selected;
      console.log();
      this.setState({
        selectedType: selected.seating_positions[0],
        selectedPosition: selected.seating_types[0],
        seating_together: selected.seating_together,
      });
    }
  }

  SelectionView = (props) => {
    return (
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          marginTop: 10,
        }}>
        <Text style={[Styles.body_label, {width: 130}]}>{props.name}</Text>
        <Text style={[Styles.body_label, {width: null}]}>{props.time}</Text>
        <TouchableOpacity
          style={{
            height: 25,
            width: 25,
            borderRadius: 5,
            backgroundColor: props.bg,
            borderColor: COLOR.GRAY,
            justifyContent: 'center',
            alignItems: 'center',
            borderWidth: 1,
          }}>
          <Image
            resizeMode={'contain'}
            style={{height: 20, width: 20, alignSelf: 'center'}}
            source={props.image}
          />
        </TouchableOpacity>
      </View>
    );
  };

  onCheckedListener = () => {
    this.setState(
      {
        seating_together: this.state.seating_together == 1 ? 0 : 1,
      },
      () => console.log(this.state.seating_together),
    );
  };

  render() {
    return (
      <View style={Styles.container}>
        <Modal
          animationType={'slide'}
          transparent={true}
          style={{backgroundColor: COLOR.BLACK}}
          visible={this.state.modalVisible}
          onRequestClose={() => {
            console.log('Modal has been closed.');
          }}>
          <View style={styles.modal}>
            <View style={styles.modalBody}>
              <NavigationBar
                prop={this.props}
                navHeight={40}
                right_image={IMAGES.filter_image}
                name={'Seating Preference'}
              />
              <View style={{flex: 1}}>
                <View
                  style={{
                    width: wp(90),
                    alignSelf: 'center',
                    marginTop: 20,
                  }}>
                  <Text
                    style={[
                      Styles.body_label,
                      {fontFamily: FONTS.FAMILY_BOLD},
                    ]}>
                    Seating Position
                  </Text>
                  <Text style={Styles.small_label}>
                    *Choosing seating position may cost extra-fees
                  </Text>
                </View>
                <View
                  style={{
                    width: wp(90),
                    alignSelf: 'center',
                    paddingVertical: 10,
                  }}>
                  <FlatList
                    style={{flex: 0}}
                    data={D_TRAIN_SEATING}
                    renderItem={({item}) => (
                      <TouchableWithoutFeedback>
                        <View
                          style={{
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            marginTop: 10,
                          }}>
                          <Text style={[Styles.body_label, {width: 170}]}>
                            {item.position}
                          </Text>

                          <TouchableOpacity
                            onPress={() => {
                              this.setState({selectedPosition: item.id});
                            }}
                            style={{
                              height: 30,
                              width: 30,
                              borderRadius: 5,
                              backgroundColor:
                                this.state.selectedPosition == item.id
                                  ? COLOR.GREEN
                                  : COLOR.WHITE,
                              borderColor: COLOR.GRAY,
                              justifyContent: 'center',
                              alignItems: 'center',
                              borderWidth: 1,
                            }}>
                            <Image
                              resizeMode={'contain'}
                              style={{
                                height: 20,
                                width: 20,
                                alignSelf: 'center',
                              }}
                              source={IMAGES.tick_image}
                            />
                          </TouchableOpacity>
                        </View>
                      </TouchableWithoutFeedback>
                    )}
                    numColumns={'1'}
                    removeClippedSubviews={false}
                    showsVerticalScrollIndicator={false}
                    showsHorizontalScrollIndicator={false}
                    keyExtractor={(item, index) => index.toString()}
                  />
                </View>
                <View style={[Styles.line_view, {marginVertical: 20}]} />
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                    alignSelf: 'center',

                    width: wp(90),
                  }}>
                  <View
                    style={{
                      flexDirection: 'row',
                    }}>
                    <Text
                      style={[
                        Styles.body_label,
                        {width: null, fontFamily: FONTS.FAMILY_BOLD},
                      ]}>
                      Seat Type
                    </Text>
                    <Text
                      style={[
                        Styles.body_label,
                        {
                          width: null,
                          marginRight: 5,
                          alignItems: 'flex-start',
                          color: COLOR.LIGHT_TEXT_COLOR,
                        },
                      ]}>
                      (upon availablity)
                    </Text>
                  </View>
                </View>
                <View
                  style={{
                    width: wp(90),
                    alignSelf: 'center',
                  }}>
                  <FlatList
                    style={{flex: 0}}
                    data={D_TRAIN_SEATING_TYPE}
                    renderItem={({item}) => (
                      <TouchableWithoutFeedback>
                        <View
                          style={{
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            marginTop: 10,
                          }}>
                          <Text style={[Styles.body_label, {width: 170}]}>
                            {item.type}
                          </Text>

                          <TouchableOpacity
                            onPress={() => {
                              this.setState({selectedType: item.id});
                            }}
                            style={{
                              height: 30,
                              width: 30,
                              borderRadius: 5,
                              backgroundColor:
                                this.state.selectedType == item.id
                                  ? COLOR.GREEN
                                  : COLOR.WHITE,
                              borderColor: COLOR.GRAY,
                              justifyContent: 'center',
                              alignItems: 'center',
                              borderWidth: 1,
                            }}>
                            <Image
                              resizeMode={'contain'}
                              style={{
                                height: 20,
                                width: 20,
                                alignSelf: 'center',
                              }}
                              source={IMAGES.tick_image}
                            />
                          </TouchableOpacity>
                        </View>
                      </TouchableWithoutFeedback>
                    )}
                    numColumns={'1'}
                    removeClippedSubviews={false}
                    showsVerticalScrollIndicator={false}
                    showsHorizontalScrollIndicator={false}
                    keyExtractor={(item, index) => index.toString()}
                  />
                </View>
                <View style={[Styles.line_view, {marginVertical: 20}]} />
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                    alignSelf: 'center',
                    marginVertical: 10,
                    width: wp(90),
                  }}>
                  <View
                    style={{
                      flexDirection: 'row',
                    }}>
                    <Text
                      style={[
                        Styles.body_label,
                        {width: null, fontFamily: FONTS.FAMILY_BOLD},
                      ]}>
                      Seat Together
                    </Text>
                    <Text
                      style={[
                        Styles.body_label,
                        {
                          width: null,
                          marginRight: 5,
                          alignItems: 'flex-start',
                          color: COLOR.LIGHT_TEXT_COLOR,
                        },
                      ]}>
                      (upon availablity)
                    </Text>
                  </View>
                  <Switch
                    isChecked={this.state.seating_together == 1 ? true : false}
                    onCheckedListener={this.onCheckedListener.bind(this)}
                  />
                </View>
                <View style={[Styles.line_view, {marginVertical: 20}]} />
                <TouchableOpacity
                  style={{
                    width: wp(90),
                    alignSelf: 'center',
                    height: 50,
                    alignItems: 'center',
                    justifyContent: 'center',
                    backgroundColor: COLOR.GREEN,
                    borderRadius: 10,
                    bottom: 20,
                    position: 'absolute',
                  }}
                  onPress={() => {
                    this.setState({modalVisible: false}, (val) => {
                      this.props.route.params.updateMasterState(
                        'seating_preference',
                        {
                          seating_together: this.state.seating_together,
                          seating_types: [this.state.selectedType],
                          seating_positions: [this.state.selectedPosition],
                        },
                      );
                      this.props.navigation.goBack();
                    });
                  }}>
                  <Text
                    style={[
                      Styles.button_font,
                      {
                        color: COLOR.WHITE,
                        width: null,
                        textAlign: 'center',
                      },
                    ]}>
                    Apply
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  modal: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
  },
  modalBody: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    backgroundColor: COLOR.WHITE,
    borderWidth: 1,
    width: wp(100),
    marginTop: 50,
    borderColor: COLOR.BORDER,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    flex: 1,
  },
  image: {
    height: 120,
    width: 120,
    alignSelf: 'center',
    marginVertical: 20,
  },
  headingLabel: {
    fontFamily: FONTS.FAMILY_BOLD,
    fontSize: FONTS.MEDIUM,
    color: COLOR.BLACK,
  },
  singleLineView: {
    marginVertical: 20,
    borderBottomColor: COLOR.GRAY,
    width: wp(100),
    alignSelf: 'center',
    borderBottomWidth: 1,
  },
  borderView: {
    borderRadius: 10,
    borderWidth: 3,
    width: wp(90),
    borderColor: COLOR.GRAY,
    alignSelf: 'center',
  },
});
