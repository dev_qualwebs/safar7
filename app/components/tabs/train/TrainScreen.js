import {View, Image, Text} from 'react-native';
import React from 'react';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import COLOR from '../../styles/Color';
import Styles from '../../styles/Styles';
import {
  FlatList,
  TouchableOpacity,
  TouchableWithoutFeedback,
} from 'react-native';
import IMAGES from '../../styles/Images';
import FONTS from '../../styles/Fonts';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import {connect} from 'react-redux';
import TrainAction from '../../../redux/action/TrainAction';
import TripDateView from '../../commonView/TripDateView';
import {StyleSheet} from 'react-native';
import SimpleToast from 'react-native-simple-toast';

class TrainScreen extends React.Component {
  constructor(props) {
    super(props);
    let route = this.props.route.params;
    this.state = {
      trainData: [],
      isPackage: route.isPackage ? route.isPackage : false,
      depart: route.depart,
      return: route.return,
      departFlexibility: 0,
      returnFlexibility: 0,
      package_request_id: route.package_request_id,
    };
    this.props.getScheduledTrains(route.id);
  }

  componentDidMount() {
    if (this.props.scheduledTrains) {
      this.setState({
        trainData: this.props.scheduledTrains,
      });
    }

    if (this.props.route.params.depart) {
      let data = this.props.route.params;
      this.setState({
        tripDepart: data.depart,
        tripReturn: data.return,
        departFlexibility: data.departFlexibility,
        returnFlexibility: data.returnFlexibility,
      });
    }
  }

  componentDidUpdate(prevProps) {
    if (this.props.scheduledTrains != prevProps.scheduledTrains) {
      this.setState({
        trainData: this.props.scheduledTrains,
      });
    }
  }

  render() {
    const screens = this.props.route.params.screens;
    const index = this.props.route.params.index;
    const id = this.props.route.params.id;
    return (
      <View style={styles.container}>
        <View
          style={{
            height: 220,
            backgroundColor: COLOR.GREEN,
            borderBottomRightRadius: 15,
            borderBottomLeftRadius: 15,
          }}>
          <View
            style={[
              Styles.mainView,
              {
                height: 45,
                marginTop: getStatusBarHeight(true),
                justifyContent: 'center',
                alignSelf: 'center',
                alignItems: 'center',
                flexDirection: 'row',
              },
            ]}>
            <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
              <Image
                style={{height: 15, width: 15, tintColor: COLOR.WHITE}}
                source={IMAGES.back_arrow}
              />
            </TouchableOpacity>
            <TouchableOpacity>
              <View>
                <Text
                  style={[
                    Styles.heading_label,
                    {
                      textAlign: 'center',
                      color: COLOR.WHITE,
                      marginVertical: 0,
                      fontFamily: FONTS.FAMILY_BOLD,
                    },
                  ]}>
                  Train
                </Text>
                <Image
                  style={{
                    width: 15,
                    height: 15,
                    right: 0,
                    position: 'absolute',
                  }}
                />
              </View>
            </TouchableOpacity>
          </View>
          <TripDateView
            departDate={this.state.tripDepart}
            returnDate={this.state.tripReturn}
            departFlexibility={this.state.departFlexibility}
            returnFlexibility={this.state.returnFlexibility}
          />
        </View>
        <View style={styles.cardView}>
          <View>
            <FlatList
              showsVerticalScrollIndicator={false}
              data={this.state.trainData}
              renderItem={({item, index}) => (
                <TouchableWithoutFeedback>
                  <View
                    style={{
                      alignSelf: 'center',
                      marginTop: 10,
                      backgroundColor: COLOR.WHITE,
                      width: wp(85),
                    }}>
                    <View
                      style={{
                        height: 50,
                        flex: 1,
                        flexDirection: 'row',
                        alignSelf: 'center',
                        alignItems: 'center',
                      }}>
                      <Text style={[Styles.body_label, {flex: 1}]}>
                        {item.from + ' ' + item.to}
                      </Text>
                      <TouchableOpacity
                        onPress={() =>
                          this.props.navigation.navigate('AddTrain', {
                            data: item,
                            id: id,
                            depart: this.state.depart,
                            return: this.state.return,
                          })
                        }
                        style={styles.edit}>
                        <Image
                          style={{
                            height: 20,
                            width: 20,
                          }}
                          source={IMAGES.edit}
                        />
                      </TouchableOpacity>
                      <TouchableOpacity
                        onPress={() => {
                          this.props.deleteScheduledTrain(item.id, id);
                          let data = this.state.trainData;
                          data.splice(index, 1);
                          this.setState({
                            trainData: data,
                          });
                        }}
                        style={{
                          aspectRatio: 1,
                          height: 20,
                          alignItems: 'center',
                          justifyContent: 'center',
                          marginHorizontal: 10,
                        }}>
                        <Image
                          style={{
                            height: 20,
                            width: 20,
                          }}
                          source={IMAGES.cross_icon}
                        />
                      </TouchableOpacity>
                    </View>
                  </View>
                </TouchableWithoutFeedback>
              )}
              numColumns={1}
              ListEmptyComponent={() =>
                !this.state.trainData.length ? (
                  <Text
                    style={{
                      textAlign: 'center',
                      color: COLOR.VOILET,
                      marginVertical: 40,
                    }}>
                    You did not add any Train
                  </Text>
                ) : null
              }
              keyExtractor={(item, index) => index.toString()}
            />
            <View
              style={[Styles.line_view, {width: wp(80), marginVertical: 20}]}
            />
            <TouchableOpacity
              style={styles.addButton}
              onPress={() => {
                this.setState({modalVisible: false}, (val) => {
                  this.props.navigation.navigate('AddTrain', {
                    id: id,
                    depart: this.state.depart,
                    return: this.state.return,
                    package_request_id: this.state.package_request_id,
                  });
                });
              }}>
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <Image
                  style={{
                    width: 25,
                    height: 25,
                    borderRadius: 10,
                    marginRight: 5,
                  }}
                  source={IMAGES.add_plus}
                />
                <Text
                  style={[
                    Styles.button_font,
                    {
                      color: COLOR.BLACK,
                      width: null,
                      textAlign: 'center',
                    },
                  ]}>
                  Add New Train
                </Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
        <TouchableOpacity
          style={styles.button}
          onPress={() => {
            if (screens[index + 1]) {
              this.props.navigation.replace(screens[index + 1], {
                screens: screens,
                index: index + 1,
                id: id,
                depart: this.state.depart,
                return: this.state.return,
              });
            } else if (this.state.isPackage) {
              this.props.navigation.goBack();
            } else {
              if (this.state.trainData.length == 0) {
                SimpleToast.show('Add train to continue');
              } else {
                this.props.navigation.navigate('HomeStack');
              }
            }
          }}>
          <Text
            style={[
              Styles.button_font,
              {
                color: COLOR.WHITE,
                width: null,
                textAlign: 'center',
              },
            ]}>
            Next
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}

let styles = StyleSheet.create({
  container: {flex: 1, backgroundColor: COLOR.BACKGROUND},
  cardView: {
    marginTop: -50,
    width: wp(90),
    alignSelf: 'center',
    borderRadius: 10,
    shadowColor: COLOR.VOILET,
    shadowOffset: {width: 2, height: 2},
    shadowOpacity: 0.2,
    backgroundColor: COLOR.WHITE,
    elevation: 3,
  },
  button: {
    width: wp(90),
    alignSelf: 'center',
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: COLOR.GREEN,
    borderRadius: 10,
    marginBottom: 20,
    bottom: 20,
    position: 'absolute',
  },

  addButton: {
    width: wp(85),
    alignSelf: 'center',
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: COLOR.WHITE,
    borderRadius: 10,
    borderColor: COLOR.BORDER,
    borderWidth: 1,
    marginVertical: 20,
  },
  edit: {
    aspectRatio: 1,
    height: 20,
    alignItems: 'center',
    justifyContent: 'center',
    marginHorizontal: 10,
  },
});
const mapStateToProps = (state, ownProps) => {
  return {
    scheduledTrains: state.trainReducer.scheduledTrains,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getScheduledTrains: (val) => {
      dispatch(TrainAction.getScheduledTrains(val));
    },
    deleteScheduledTrain: (id, tripId) => {
      dispatch(TrainAction.deleteScheduledTrain(id, tripId));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(TrainScreen);
