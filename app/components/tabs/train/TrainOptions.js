'use strict';

import React from 'react';
import {View, Image, StyleSheet, Text, SafeAreaView} from 'react-native';
import {TouchableOpacity, TextInput} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import COLOR from '../../../components/styles/Color';
import FONTS from '../../../components/styles/Fonts';
import IMAGES from '../../../components/styles/Images';
import Styles from '../../../components/styles/Styles';
import {connect} from 'react-redux';
import TrainAction from '../../../redux/action/TrainAction';
import SimpleToast from 'react-native-simple-toast';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import { DoneButtonKeyboard } from '../../commonView/CommonView';

class TrainOptions extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedIndex: 0,
      trainClassesData: [],
      trainPoliciesData: [],
      classes: [],
      reservation_policies: [],
      seating_preference: null,
      extra: null,
      comment: '',
      id: this.props.route.params.id,
      trainId: null,
      package_request_id: this.props.route.params.package_request_id,
    };
    this.props.getTrainClasses();
    this.props.getTrainPolicies();
  }

  callScheduleTrain = () => {
    let params = this.props.route.params.params;
    let data = JSON.stringify({
      tripId: this.state.id,
      from: params.from,
      to: params.to,
      depart: params.checkInDate,
      depart_flexibility: `${params.fromFlexibility}`,
      round_trip: params.round_trip ? 1 : 0,
      return: params.checkOutDate,
      return_flexibility: `${params.toFlexibility}`,
      classes: this.state.classes,
      reservation_policies: this.state.reservation_policies,
      seating_preference: this.state.seating_preference,
      extra: this.state.extra,
      comment: this.state.comment,
      package_request_id: this.state.package_request_id,
    });
    console.log(data);

    if (this.state.trainId) {
      this.props.updateTrain(data, this.state.trainId);
    } else {
      this.props.scheduleTrain(data, this.state.id);
    }
  };

  handleValidation = () => {
    if (this.state.classes.length == 0) {
      SimpleToast.show('Select Train classes');
    } else if (this.state.reservation_policies.length == 0) {
      SimpleToast.show('Select Reservation policy');
    } else if (this.state.seating_preference == null) {
      SimpleToast.show('Select Seating Preference');
    } else if (this.state.extra == null) {
      SimpleToast.show('Select Extra');
    } else {
      this.callScheduleTrain();
      this.props.navigation.goBack();
    }
  };

  updateMasterState = (attrName, value) => {
    this.setState({[attrName]: value});
  };

  componentDidMount() {
    console.log(this.props.route.params);
    let params = this.props.route.params.params;

    if (params.trainId) {
      this.setState({
        trainId: params.trainId,
      });
      this.props.getSingleTrain(params.trainId);
    }
    if (this.props.trainClasses.length) {
      this.setState({
        trainClassesData: this.props.trainClasses,
      });
    }
    if (this.props.trainPolicies.length) {
      this.setState({
        trainPoliciesData: this.props.trainPolicies,
      });
    }
  }

  componentWillReceiveProps(props) {
    if (props.trainClasses != this.props.trainClasses) {
      this.setState({
        trainClassesData: props.trainClasses,
      });
    }
    if (props.trainPolicies != this.props.trainPolicies) {
      this.setState({
        trainPoliciesData: props.trainPolicies,
      });
    }

    if (props.singleTrainData != this.props.singleTrainData) {
      let pref = props.singleTrainData.train_preference;
      let seatingPref = pref.seating_preference;
      this.setState({
        classes: pref.preferred_class.map((v) => v.id),
        reservation_policies: pref.reservation_policy.map((v) => v.id),
        seating_preference: {
          seating_together: seatingPref.seating_together,
          seating_types: seatingPref.seating_type_preference.map((v) => v.id),
          seating_positions: seatingPref.seating_position_preference.map(
            (v) => v.id,
          ),
        },
        extra: {
          hand_luggage: pref.extra.hand_luggage,
          check_in_luggage: pref.extra.check_in_luggage,
        },
        comment: props.singleTrainData.comment,
      });
    }
  }

  componentWillUnmount() {
    this.props.clearSingleTrain();
  }

  selectionView = (prop) => {
    return (
      <TouchableOpacity style={{height: 130}} onPress={prop.onPress}>
        <View style={[style.container,{
          backgroundColor : prop.hasData ? COLOR.GREEN_OPACITY : COLOR.WHITE,
          borderColor : prop.hasData ? '#05778F' : COLOR.GRAY
        }]}>
          <View
            style={{
              height: 60,
              width: 60,
              marginVertical: 10,
              backgroundColor: prop.hasData ? COLOR.WHITE : prop.bg,
              borderRadius: 5,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Image
              resizeMode={'contain'}
              source={prop.image}
              style={{
                height: 30,
                width: 30,
                tintColor: prop.tint,
              }}
            />
          </View>
          <Text style={{marginBottom: 10, textAlign: 'center', fontSize: 14}}>
            {prop.name}
          </Text>
        </View>
      </TouchableOpacity>
    );
  };

  render() {
    return (
      <SafeAreaView style={[Styles.container]}>
        <KeyboardAwareScrollView style={{flexGrow: 1, flex: 1}}>
          <View>
            <View style={style.topBar}>
              <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                <Image
                  style={{height: 15, width: 15}}
                  source={IMAGES.back_arrow}
                />
              </TouchableOpacity>
              <Text
                style={[
                  Styles.subheading_label,
                  {width: null, fontSize: 20, fontFamily: FONTS.FAMILY_BOLD},
                ]}>
                Train Options
              </Text>
              <TouchableOpacity>
                <Image
                  style={{height: 20, width: 20}}
                  source={IMAGES.filter_image}
                />
              </TouchableOpacity>
            </View>
            <View
              style={{
                flexDirection: 'row',
                width: wp(90),
                alignSelf: 'center',
                marginTop: 20,
                justifyContent: 'space-between',
              }}>
              <this.selectionView
                name={'Train Class'}
                image={IMAGES.train}
                bg={'#CF212B20'}
                tint={'#CF2120'}
                onPress={() => {
                  this.props.navigation.navigate('TrainClass', {
                    data: this.state.trainClassesData,
                    updateMasterState: this.updateMasterState,
                    selected: this.state.classes,
                  });
                }}
                hasData={this.state.classes && this.state.classes.length > 0 ? true : false}
              />
              <this.selectionView
                name={'Reservation\nPoilcy'}
                image={IMAGES.shield}
                bg={'#05778F20'}
                tint={'#05778F'}
                hasData={this.state.reservation_policies && this.state.reservation_policies.length > 0 ? true : false}
                onPress={() => {
                  this.setState({modalVisible: false}, (val) => {
                    this.props.navigation.navigate('TrainPolicies', {
                      data: this.state.trainPoliciesData,
                      updateMasterState: this.updateMasterState,
                      selected: this.state.reservation_policies,
                      name : 'Reservation Poilcy'
                    });
                  });
                }}
              />
              <this.selectionView
                name={'Seating\nPreference'}
                image={IMAGES.empty_seat}
                bg={'#02599520'}
                tint={'#025995'}
                hasData={this.state.seating_preference ? true : false}
                onPress={() => {
                  this.props.navigation.navigate('TrainSeatingOption', {
                    updateMasterState: this.updateMasterState,
                    selected: this.state.seating_preference,
                  });
                }}
              />
            </View>
            <View
              style={{
                flexDirection: 'row',
                width: wp(30),
                alignSelf: 'flex-start',
                marginBottom: 20,
                marginLeft: 20,
                justifyContent: 'space-between',
              }}>
              <this.selectionView
                name={'Extra'}
                image={IMAGES.extra_circle}
                bg={'#CF212B20'}
                tint={'#CF212B'}
                hasData={this.state.extra ? true : false}
                onPress={() => {
                  this.props.navigation.navigate('TrainExtra', {
                    updateMasterState: this.updateMasterState,
                    selected: this.state.extra,
                  });
                }}
              />
            </View>
          </View>

          <View
            style={[Styles.line_view, {marginVertical: 20, width: wp(90)}]}
          />

          <Text style={[Styles.subheading_label]}>Comments</Text>
          <TextInput
          inputAccessoryViewID={'uniqueID'}
            placeholder={'Write your comment'}
            placeholderTextColor={COLOR.LIGHT_TEXT_COLOR}
            value={this.state.comment}
            onChangeText={(text) => this.setState({comment: text})}
            style={[
              {
                height: 120,
                alignSelf: 'center',
                paddingHorizontal: 20,
                paddingVertical: 20,
                paddingTop: 10,
                width: wp(90),
                borderRadius: 10,
                borderWidth: 1,
                borderColor: COLOR.GRAY,
                marginTop: 10,
              },
            ]}
            multiline={true}
          />
          <DoneButtonKeyboard />
          <View
            style={{
              marginVertical: 20,
              alignSelf: 'center',
            }}>
            <TouchableOpacity
              style={{
                width: wp(90),
                alignSelf: 'center',
                height: 50,
                alignItems: 'center',
                justifyContent: 'center',
                backgroundColor: COLOR.GREEN,
                borderRadius: 10,
                marginTop: 20,
              }}
              onPress={() => {
                this.handleValidation();
              }}>
              <Text
                style={[
                  Styles.button_font,
                  {
                    color: COLOR.WHITE,
                    width: null,
                    textAlign: 'center',
                  },
                ]}>
                Apply
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={{
                width: wp(90),
                alignSelf: 'center',
                height: 50,
                alignItems: 'center',
                justifyContent: 'center',
                borderRadius: 10,
              }}>
              <Text
                style={[
                  Styles.small_label,
                  {
                    color: COLOR.BLACK,
                    width: null,
                    textAlign: 'right',
                  },
                ]}>
                Skip train options
              </Text>
            </TouchableOpacity>
          </View>
        </KeyboardAwareScrollView>
      </SafeAreaView>
    );
  }
}

const style = StyleSheet.create({
  topBar: {
    height: 50,
    backgroundColor: COLOR.WHITE,
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: wp(90),
    alignSelf: 'center',
    marginVertical: 0,
    alignItems: 'center',
  },
  container: {
    flex: 1,
    borderRadius: 10,
    borderWidth: 1,
    height: 130,
    width: wp(28),
    margin: 5,
    borderColor: COLOR.GRAY,
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 10,
  },
});

const mapDispatchToProps = (dispatch) => {
  return {
    getTrainClasses: (val) => {
      dispatch(TrainAction.getTrainClasses(val));
    },
    getTrainPolicies: (val) => {
      dispatch(TrainAction.getTrainPolicies(val));
    },
    scheduleTrain: (data, id) => {
      dispatch(TrainAction.scheduleTrain(data, id));
    },
    getSingleTrain: (id) => {
      dispatch(TrainAction.getSingleTrain(id));
    },
    updateTrain: (data, id) => {
      dispatch(TrainAction.updateTrain(data, id));
    },
    clearSingleTrain: () => {
      dispatch(TrainAction.clearSingleTrain());
    },
  };
};

const mapStateToProps = (state, ownProps) => {
  return {
    trainClasses: state.trainReducer.trainClasses,
    trainPolicies: state.trainReducer.trainPolicies,
    singleTrainData: state.trainReducer.singleTrainData,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(TrainOptions);
