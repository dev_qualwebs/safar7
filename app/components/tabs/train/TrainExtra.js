import React from 'react';
import {Modal, Text, View, StyleSheet, Image} from 'react-native';
import {TouchableOpacity} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import COLOR from '../../../components/styles/Color';
import FONTS from '../../../components/styles/Fonts';
import IMAGES from '../../../components/styles/Images';
import Styles from '../../../components/styles/Styles';
import NavigationBar from '../../commonView/NavigationBar';

export default class TrainExtra extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: true,
      handBaggage: 0,
      checkInBaggage: 0,
    };
  }

  updateMasterState = (attrName, value) => {
    this.setState({[attrName]: value});
  };

  componentDidMount() {
    console.log(this.props.route.params);
    if (this.props.route.params.selected) {
      let selected = this.props.route.params.selected;
      this.setState({
        handBaggage: selected.hand_luggage,
        checkInBaggage: selected.check_in_luggage,
      });
    }
  }

  handlePlusMinus = (id, val) => {
    if (id == 1) {
      if (val == 1 && this.state.handBaggage > 0) {
        this.setState({
          handBaggage: this.state.handBaggage - 1,
        });
      } else if (val == 2 && this.state.handBaggage < 7) {
        this.setState({
          handBaggage: this.state.handBaggage + 1,
        });
      }
    } else if (id == 2) {
      if (val == 1 && this.state.checkInBaggage > 0) {
        this.setState({
          checkInBaggage: this.state.checkInBaggage - 1,
        });
      } else if (val == 2 && this.state.checkInBaggage < 23) {
        this.setState({
          checkInBaggage: this.state.checkInBaggage + 1,
        });
      }
    }
  };

  IncrementView = (props) => (
    <View
      style={{
        flexDirection: 'row',
        width: wp(90),

        justifyContent: 'space-between',
      }}>
      <View
        style={{flex: 1, alignSelf: 'center', justifyContent: 'flex-start'}}>
        <Text
          style={[
            Styles.body_label,
            {width: null, color: COLOR.BLACK, alignSelf: 'flex-start'},
          ]}>
          {props.name}
        </Text>
      </View>
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'flex-end',
        }}>
        <TouchableOpacity
          onPress={() => this.handlePlusMinus(props.id, 1)}
          style={{
            height: 25,
            width: 25,
            borderRadius: 15,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Image
            style={{height: 30, width: 30}}
            resizeMode={'contain'}
            source={IMAGES.minus_image}
          />
        </TouchableOpacity>
        <Text
          style={[
            Styles.button_font,
            {width: 40, color: COLOR.BLACK, textAlign: 'center'},
          ]}>
          {props.value}
        </Text>
        <TouchableOpacity
          onPress={() => this.handlePlusMinus(props.id, 2)}
          style={{
            height: 30,
            width: 30,
            borderWidth: 1,
            borderRadius: 15,
            borderColor: COLOR.GREEN,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Image style={{height: 20, width: 20}} source={IMAGES.plus_image} />
        </TouchableOpacity>
      </View>
    </View>
  );

  SelectionView = (props) => {
    return (
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          marginTop: 10,
        }}>
        <Text style={[Styles.body_label, {width: 80}]}>{props.name}</Text>
        <Text style={[Styles.body_label, {width: null}]}>{props.time}</Text>
        <TouchableOpacity
          style={{
            height: 30,
            width: 30,
            borderRadius: 5,
            backgroundColor: props.bg,
            borderColor: COLOR.GRAY,
            justifyContent: 'center',
            alignItems: 'center',
            borderWidth: 1,
          }}>
          <Image
            resizeMode={'contain'}
            style={{height: 20, width: 20, alignSelf: 'center'}}
            source={props.image}
          />
        </TouchableOpacity>
      </View>
    );
  };

  render() {
    return (
      <View style={Styles.container}>
        <Modal
          animationType={'slide'}
          transparent={true}
          style={{backgroundColor: COLOR.BLACK}}
          visible={this.state.modalVisible}
          onRequestClose={() => {
            console.log('Modal has been closed.');
          }}>
          <View style={styles.modal}>
            <View style={styles.modalBody}>
              <NavigationBar
                prop={this.props}
                navHeight={40}
                right_image={IMAGES.filter_image}
                name={'Extra'}
              />
              <View
                style={{
                  flex: 1,
                  width: wp(90),
                  alignSelf: 'center',
                  paddingVertical: 10,
                }}>
                <Text
                  style={[
                    Styles.subheading_label,
                    {marginVertical: 20, fontSize: FONTS.REGULAR},
                  ]}>
                  Luggage
                </Text>
                <View
                  style={{
                    width: wp(90),
                    alignSelf: 'center',
                    marginBottom: 20,
                  }}>
                  <this.IncrementView
                    id={1}
                    value={this.state.handBaggage}
                    name={'Hand-Luggage (7 Kg)'}
                  />
                  <View style={[Styles.line_view, {marginVertical: 20}]} />
                  <this.IncrementView
                    id={2}
                    value={this.state.checkInBaggage}
                    name={'Check-in Luggage (20-23 Kg)'}
                  />
                </View>
              </View>
              <TouchableOpacity
                style={{
                  width: wp(90),
                  alignSelf: 'center',
                  height: 50,
                  alignItems: 'center',
                  justifyContent: 'center',
                  backgroundColor: COLOR.GREEN,
                  borderRadius: 10,
                  marginBottom: 20,
                }}
                onPress={() => {
                  this.setState({modalVisible: false}, (val) => {
                    this.props.route.params.updateMasterState('extra', {
                      hand_luggage: this.state.handBaggage,
                      check_in_luggage: this.state.checkInBaggage,
                    });
                    this.props.navigation.goBack();
                  });
                }}>
                <Text
                  style={[
                    Styles.button_font,
                    {
                      color: COLOR.WHITE,
                      width: null,
                      textAlign: 'center',
                    },
                  ]}>
                  Apply
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  modal: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
  },
  modalBody: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    backgroundColor: COLOR.WHITE,
    borderWidth: 1,
    width: wp(100),
    marginTop: 50,
    borderColor: COLOR.BORDER,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    flex: 1,
  },
  image: {
    height: 120,
    width: 120,
    alignSelf: 'center',
    marginVertical: 20,
  },
  headingLabel: {
    fontFamily: FONTS.FAMILY_BOLD,
    fontSize: FONTS.MEDIUM,
    color: COLOR.BLACK,
  },
  singleLineView: {
    marginVertical: 20,
    borderBottomColor: COLOR.GRAY,
    width: wp(100),
    alignSelf: 'center',
    borderBottomWidth: 1,
  },
  borderView: {
    borderRadius: 10,
    borderWidth: 3,
    width: wp(90),
    borderColor: COLOR.GRAY,
    alignSelf: 'center',
  },
});
