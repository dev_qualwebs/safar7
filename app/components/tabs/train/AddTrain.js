import React from 'react';
import {Modal, Text, View, StyleSheet, Image, Button} from 'react-native';
import {SafeAreaView} from 'react-native';
import {TextInput, TouchableOpacity} from 'react-native';
import {
  widthPercentageToDP,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import COLOR from '../../../components/styles/Color';
import FONTS from '../../../components/styles/Fonts';
import IMAGES from '../../../components/styles/Images';
import Styles from '../../../components/styles/Styles';
import GooglePlacesTextInput from '../../commonView/GooglePlacesTextInput';
import {
  getEndDateTimestamp,
  getStartDateTimestamp,
} from '../../commonView/Helpers';
import Switch from '../../commonView/Switch';
import TripDateView from '../../commonView/TripDateView';
import SelectDate from '../calendar/SelectDate';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import SimpleToast from 'react-native-simple-toast';
import { DoneButtonKeyboard } from '../../commonView/CommonView';


export default class AddTrain extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: true,
      selectedIndex: null,
      from: '',
      to: '',
      checkInDate: '',
      round_trip: 0,
      checkOutDate: '',
      openCalendar: false,
      selector: 0,
      id: this.props.route.params.id,
      fromFlexibility: 0,
      toFlexibility: 0,
      round_trip: false,
      minDate: getStartDateTimestamp(this.props.route.params.depart),
      maxDate: getEndDateTimestamp(this.props.route.params.return),
      tripDepart: this.props.route.params.depart,
      tripReturn: this.props.route.params.return,
      trainId: null,
      package_request_id: this.props.route.params.package_request_id,
    };
  }

  componentDidMount() {
    console.log(this.props.route.params.depart);
    if (this.props.route.params.data) {
      let params = this.props.route.params;

      this.setState({
        from: params.data.from,
        to: params.data.to,
        checkInDate: params.data.depart,
        checkOutDate: params.data.return,
        trainId: params.data.id,
        round_trip: params.data.round_trip == 1 ? true : false,
      });
    }
  }

  updateMasterState = (attrName, value) => {
    this.setState({[attrName]: value});
  };

  render() {
    const {selectedIndex} = this.state;
    return (
      <KeyboardAwareScrollView
        keyboardShouldPersistTaps={'always'}
        style={Styles.container}>
        {this.state.openCalendar ? (
          <SelectDate
            allowRangeSelection={false}
            selector={this.state.selector}
            updateMasterState={this.updateMasterState}
            props={this.props.props}
            modal={true}
            minDate={this.state.minDate}
            maxDate={this.state.maxDate}
          />
        ) : (
          <View style={{flex: 1}}>
            <View
              style={{
                height: 220,
                backgroundColor: COLOR.GREEN,
                borderBottomRightRadius: 15,
                borderBottomLeftRadius: 15,
              }}>
              <View
                style={[
                  Styles.mainView,
                  {
                    height: 45,

                    marginTop: getStatusBarHeight(true),
                    justifyContent: 'center',
                    alignSelf: 'center',
                    alignItems: 'center',
                    flexDirection: 'row',
                  },
                ]}>
                <TouchableOpacity
                  style={{
                    padding: 10,
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}
                  onPress={() => this.props.navigation.goBack()}>
                  <Image
                    style={{height: 15, width: 15, tintColor: COLOR.WHITE}}
                    source={IMAGES.back_arrow}
                  />
                </TouchableOpacity>

                <View>
                  <Text
                    style={[
                      Styles.heading_label,
                      {
                        textAlign: 'center',
                        color: COLOR.WHITE,
                        marginVertical: 0,
                        fontFamily: FONTS.FAMILY_BOLD,
                      },
                    ]}>
                    Train
                  </Text>
                  <Image
                    style={{
                      width: 15,
                      height: 15,
                      right: 0,
                      position: 'absolute',
                    }}
                  />
                </View>
              </View>
              <TripDateView
                departDate={this.state.tripDepart}
                returnDate={this.state.tripReturn}
                departFlexibility={this.state.departFlexibility}
                returnFlexibility={this.state.returnFlexibility}
              />
            </View>

            <View style={styles.parentView}>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  height: 60,
                  zIndex: 1100,
                }}>
                <Image
                  style={{height: 30, width: 30, marginRight: 10}}
                  source={IMAGES.pinpoint}
                  resizeMode={'contain'}
                />
                <View>
                  <Text style={Styles.small_label}>From</Text>
                  <GooglePlacesTextInput
                    value={this.state.from}
                    onValueChange={(text) =>
                      this.setState({
                        from: text,
                      })
                    }
                  />
                </View>
              </View>
              <View style={styles.hr} />
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  height: 60,
                  zIndex: 1000,
                }}>
                <Image
                  style={{height: 30, width: 30, marginRight: 10}}
                  source={IMAGES.pinpoint}
                  resizeMode={'contain'}
                />
                <View>
                  <Text style={Styles.small_label}>To</Text>
                  <GooglePlacesTextInput
                    value={this.state.to}
                    onValueChange={(text) =>
                      this.setState({
                        to: text,
                      })
                    }
                  />
                </View>
              </View>
              {this.state.tripDepart ? (
                <View>
                  <View style={styles.hr} />
                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                    }}>
                    <Image
                      style={{height: 30, width: 30, marginRight: 10}}
                      source={IMAGES.tick_calendar}
                      resizeMode={'contain'}
                    />
                    <View>
                      <Text
                        style={[
                          Styles.small_label,
                          {width: null, alignSelf: 'flex-start'},
                        ]}>
                        Departure Date
                      </Text>

                      <TouchableOpacity
                        onPress={() =>
                          this.setState({openCalendar: true, selector: 0})
                        }>
                        <Text
                          style={[
                            Styles.body_label,
                            {width: widthPercentageToDP(55)},
                          ]}>
                          {this.state.checkInDate
                            ? this.state.checkInDate
                            : 'Select Date'}
                        </Text>
                      </TouchableOpacity>
                    </View>
                    <View>
                      <Text style={[Styles.small_label, {width: null}]}>
                        Round Trip
                      </Text>
                      <Switch
                        isChecked={this.state.round_trip}
                        onCheckedListener={() =>
                          this.setState({
                            round_trip: !this.state.round_trip,
                          })
                        }
                      />
                    </View>
                  </View>
                  {this.state.round_trip ? (
                    <View>
                      <View style={styles.hr} />
                      <View
                        style={{
                          flexDirection: 'row',
                          alignItems: 'center',
                        }}>
                        <Image
                          style={{height: 30, width: 30, marginRight: 10}}
                          source={IMAGES.tick_calendar}
                          resizeMode={'contain'}
                        />
                        <View>
                          <Text style={Styles.small_label}>Return Date</Text>
                          <TouchableOpacity
                            onPress={() =>
                              this.setState({openCalendar: true, selector: 1})
                            }>
                            <Text style={Styles.body_label}>
                              {this.state.checkOutDate
                                ? this.state.checkOutDate
                                : 'Select Date'}
                            </Text>
                          </TouchableOpacity>
                        </View>
                      </View>
                    </View>
                  ) : null}
                </View>
              ) : null}
              <View style={styles.hr} />
              <Text
                style={{
                  fontSize: 16,
                  fontFamily: FONTS.FAMILY_SEMIBOLD,
                  marginVertical: 10,
                }}>
                Comment
              </Text>
              <View
                style={{
                  borderWidth: 1,
                  borderColor: COLOR.GRAY,
                  borderRadius: 10,
                  height: 120,
                  marginBottom: 20,
                }}>
                <TextInput
                inputAccessoryViewID={'uniqueID'}
                  placeholderTextColor={COLOR.LIGHT_TEXT_COLOR}
                  multiline={true}
                  placeholder={'Comments'}
                  style={[{margin: 10, flex: 1}]}
                />
                <DoneButtonKeyboard />
              </View>
            </View>

            <TouchableOpacity
              style={styles.button}
              onPress={() => {
                this.setState({modalVisible: false}, (val) => {
                  // from: '',
                  // to: '',
                  // checkInDate: '',
                  // round_trip: 1,
                  // checkOutDate: '',
                  console.log(this.state.round_trip)
                  if(this.state.from == ''){
                    SimpleToast.show('Enter boarding location');
                    return;
                  } else if (this.state.to == ''){
                    SimpleToast.show('Enter drop off location');
                    return;
                  }
                  //  else if (this.state.checkInDate == ''){
                  //   SimpleToast.show('Select departure date');
                  //   return;
                  // } else if (this.state.checkOutDate == '' && this.state.round_trip){
                  //   SimpleToast.show('Select return date');
                  //   return;
                  // }



                  this.props.navigation.replace('TrainOptions', {
                    params: this.state,
                    id: this.state.id,
                    package_request_id: this.state.package_request_id,
                  });
                });
              }}>
              <Text
                style={[
                  Styles.button_font,
                  {
                    color: COLOR.WHITE,
                    width: null,
                    textAlign: 'center',
                  },
                ]}>
                Next
              </Text>
            </TouchableOpacity>
          </View>
        )}
      </KeyboardAwareScrollView>
    );
  }
}

const styles = StyleSheet.create({
  modal: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
  },
  modalBody: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    backgroundColor: COLOR.WHITE,
    borderWidth: 1,
    width: wp(100),
    marginTop: 50,
    borderColor: COLOR.BORDER,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    flex: 1,
  },
  image: {
    height: 120,
    width: 120,
    alignSelf: 'center',
    marginVertical: 20,
  },
  headingLabel: {
    fontFamily: FONTS.FAMILY_BOLD,
    fontSize: FONTS.MEDIUM,
    color: COLOR.BLACK,
  },
  singleLineView: {
    marginVertical: 20,
    borderBottomColor: COLOR.GRAY,
    width: wp(100),
    alignSelf: 'center',
    borderBottomWidth: 1,
  },
  borderView: {
    borderRadius: 10,
    borderWidth: 3,
    width: wp(90),
    borderColor: COLOR.GRAY,
    alignSelf: 'center',
  },
  parentView: {
    marginTop: -50,
    width: wp(90),
    alignSelf: 'center',
    borderRadius: 10,
    zIndex: 1000,
    shadowColor: COLOR.VOILET,
    shadowOffset: {width: 2, height: 2},
    shadowOpacity: 0.2,
    backgroundColor: COLOR.WHITE,
    elevation: 3,
    padding: 15,
  },
  button: {
    width: wp(90),
    alignSelf: 'center',
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: COLOR.GREEN,
    borderRadius: 10,
    marginBottom: 20,
  },
  hr: {
    width: wp(80),
    height: 1,
    backgroundColor: COLOR.GRAY,
    marginVertical: 10,
    alignSelf: 'center',
  },
});
