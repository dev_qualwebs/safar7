import React from 'react';
import { Modal, Text, View, StyleSheet, Image, Alert } from 'react-native';
import { FlatList, TextInput, TouchableOpacity } from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import COLOR from '../../../components/styles/Color';
import FONTS from '../../../components/styles/Fonts';
import IMAGES from '../../../components/styles/Images';
import Styles from '../../../components/styles/Styles';
import NavigationBar from '../../commonView/NavigationBar';

export default class Destinations extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: true,
      address: [
        {
          id: 1,
          city_name: 'Denpaser, Bali',
          note: 'Note: South city with good view',
          days: '(5 days)',
        },
        {
          id: 2,
          city_name: 'Badung, Bali',
          note: null,
          days: '(3 days)',
        },
        {
          id: 3,
          city_name: 'Gili Trawangan, Lombok',
          note: null,
          days: '(5 days)',
        },
        {
          id: 4,
          city_name: 'Sengiggigi, Lombok',
          note: null,
          days: '(5 days)',
        },
      ],
      selectedIndex: null,
    };
  }

  selectTransport = (item) => {
    // this.setState({selectedIndex: item.id});
  };

  handleNavigation = () => {
    this.props.navigation.navigate('AddNewCity');
  };

  render() {
    const { selectedIndex, modalVisible } = this.state;
    return (
      <View style={Styles.container}>
        <Modal
          animationType={'slide'}
          transparent={true}
          style={{ backgroundColor: COLOR.BLACK }}
          visible={modalVisible}
          onRequestClose={() => {
            console.log('Modal has been closed.');
          }}>
          <View style={styles.modal}>
            <View style={styles.modalBody}>
              <NavigationBar
                prop={this.props}
                navHeight={40}
                right_image={IMAGES.filter_image}
                name={'Destinations'}
              />
              <View style={{ marginVertical: 10, flex: 1 }}>
                <FlatList
                  style={{
                    backgroundColor: COLOR.WHITE,
                    flex: 1,
                    flexGrow: 0,
                    minHeight: 250,
                  }}
                  data={this.state.address}
                  renderItem={({ item }) => (
                    <TouchableOpacity
                      onPress={() => this.selectTransport(item)}>
                      <View
                        style={{
                          borderRadius: 10,

                          width: wp(90),
                          margin: 5,

                          alignSelf: 'center',
                          alignItems: 'center',
                          justifyContent: 'space-between',
                          flexDirection: 'row',
                          padding: 10,
                        }}>
                        <View
                          style={{
                            flex: 1,
                            alignSelf: 'center',
                            alignItems: 'center',
                            justifyContent: 'center',
                          }}>
                          <Text
                            style={[
                              Styles.body_label,
                              {
                                width: null,
                                textAlign: 'left',
                                alignSelf: 'flex-start',
                              },
                            ]}>
                            {item.city_name}
                          </Text>
                          {item.note != null && (
                            <Text
                              style={[
                                Styles.small_label,
                                {
                                  textAlign: 'left',
                                  alignSelf: 'flex-start',
                                  width: null,
                                },
                              ]}>
                              {item.note}
                            </Text>
                          )}
                        </View>
                        <Text
                          style={{
                            textAlign: 'center',
                            width: 80,
                            alignItems: 'center',
                            justifyContent: 'center',
                          }}>
                          {item.days}
                        </Text>
                        <Image
                          style={{ width: 20, height: 20 }}
                          source={IMAGES.cross_icon}
                        />
                      </View>
                      <View
                        style={[
                          Styles.line_view,
                          { width: wp(90), alignSelf: 'center' },
                        ]}
                      />
                    </TouchableOpacity>
                  )}
                  horizontal={false}
                  numColumns={1}
                  showsVerticalScrollIndicator={false}
                  showsHorizontalScrollIndicator={false}
                  keyExtractor={(item, index) => index.toString()}
                />
                <Text
                  style={[
                    Styles.small_label,
                    { marginVertical: 10, color: COLOR.RED },
                  ]}>
                  This service will not include reservations for a flight or a
                  stay.
                </Text>
                <TouchableOpacity
                  style={{
                    width: wp(90),
                    alignSelf: 'center',
                    height: 50,
                    alignItems: 'center',
                    justifyContent: 'center',
                    backgroundColor: COLOR.WHITE,
                    borderRadius: 10,
                    marginBottom: 20,
                    borderColor: COLOR.LIGHT_TEXT,
                    borderWidth: 1,
                    marginTop: 20,
                  }}
                  onPress={() => {
                    this.setState({ modalVisible: false }, (val) => {
                      this.props.navigation.navigate('AddNewCity');
                    });
                  }}>
                  <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <View
                      style={{
                        justifyContent: 'center',
                        alignItems: 'center',
                        width: 25,
                        height: 25,
                        borderRadius: 15,
                        backgroundColor: COLOR.GREEN,
                        marginRight: 5,
                      }}>
                      <Image
                        style={{
                          width: 12.5,
                          height: 12.5,
                          borderRadius: 10,
                          tintColor: COLOR.WHITE,
                        }}
                        source={IMAGES.plus_image}
                      />
                    </View>
                    <Text
                      style={[
                        Styles.button_font,
                        {
                          color: COLOR.BLACK,
                          width: null,
                          textAlign: 'center',
                        },
                      ]}>
                      Add New City
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>

              <TouchableOpacity
                onPress={() => {
                  this.setState({ modalVisible: false }, (val) => {
                    this.props.navigation.navigate('AddNewCity');
                  });
                }}
                style={{
                  alignItems: 'center',
                  justifyContent: 'center',
                  height: 50,
                  borderRadius: 10,
                  borderColor: COLOR.GRAY,
                  borderWidth: 1,
                  width: wp(90),
                  marginBottom: 20,
                  backgroundColor: COLOR.GREEN,
                }}>
                <Text style={[Styles.button_font, { color: COLOR.WHITE }]}>
                  Done
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  modal: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
  },
  modalBody: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    backgroundColor: COLOR.WHITE,
    borderWidth: 1,
    width: wp(100),
    borderColor: COLOR.BORDER,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    marginTop: 50,
    flex: 1,
  },
  image: {
    height: 120,
    width: 120,
    alignSelf: 'center',
    marginVertical: 20,
  },
  headingLabel: {
    fontFamily: FONTS.FAMILY_BOLD,
    fontSize: FONTS.MEDIUM,
    color: COLOR.BLACK,
  },
  singleLineView: {
    marginVertical: 20,
    borderBottomColor: COLOR.GRAY,
    width: wp(100),
    alignSelf: 'center',
    borderBottomWidth: 1,
  },
  offer_view: {
    marginVertical: 0,
    width: wp(100),
    marginBottom: 20,
    height: 100,
    marginHorizontal: 10,
  },
  flatlist_view: {
    width: wp(90),
    marginVertical: 10,
    alignSelf: 'center',
  },
});
