import React from 'react';
import {Modal, Text, View, StyleSheet, Image} from 'react-native';
import {
  FlatList,
  TouchableOpacity,
  TouchableWithoutFeedback,
} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import COLOR from '../../../components/styles/Color';
import FONTS from '../../../components/styles/Fonts';
import IMAGES from '../../../components/styles/Images';
import Styles from '../../../components/styles/Styles';
import NavigationBar from '../../commonView/NavigationBar';
import {D_ATTRACTION_SHOWS, U_IMAGE_BASE} from '../../../helper/Constants';
import TripPlanAction from '../../../redux/action/TripPlanAction';
import {connect} from 'react-redux';
import {SelectionHoc} from '../hoc/SelectionHoc';
class AttractionShow extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: true,
      data: [],
      selectedIds: [],
    };
    this.props.getAttractionShow();
  }

  componentDidMount() {
    if (this.props.attractionShow) {
      this.setState({
        data: this.props.attractionShow,
      });
    }

    if (this.props.selectedData) {
      this.setState({
        selectedIds: this.props.selectedData,
      });
    }
  }

  componentDidUpdate(prevProps) {
    if (this.props.attractionShow !== prevProps.attractionShow) {
      this.setState({
        data: this.props.attractionShow,
      });
    }

    if (this.props.selectedData !== prevProps.selectedData) {
      this.setState({
        selectedIds: this.props.selectedData,
      });
    }
  }

  updateMasterState = (attrName, value) => {
    this.setState({[attrName]: value});
  };

  render() {
    return (
      <View style={Styles.container}>
        <Modal
          animationType={'slide'}
          transparent={true}
          style={{backgroundColor: COLOR.BLACK}}
          visible={this.state.modalVisible}
          onRequestClose={() => {
            console.log('Modal has been closed.');
          }}>
          <View style={styles.modal}>
            <View style={styles.modalBody}>
              <NavigationBar
                prop={this.props}
                navHeight={40}
                right_image={IMAGES.filter_image}
                name={'Attraction & Shows'}
              />
              <View
                style={{
                  flex: 1,
                  width: wp(90),

                  alignSelf: 'center',
                  paddingVertical: 10,
                }}>
                <FlatList
                  data={this.state.data}
                  renderItem={({item}) => (
                    <TouchableWithoutFeedback
                      onPress={() => this.props.addItem(item)}>
                      <View
                        style={{
                          flex: 1,
                          alignItems: 'center',
                          justifyContent: 'space-between',
                          margin: 5,
                        }}>
                        <View style={{width: wp(28)}}>
                          <Image
                            resizeMode={'cover'}
                            style={{
                              height: 80,
                              width: 80,
                              borderWidth: 1,
                              borderColor:
                                this.state.selectedIds.indexOf(item.id) !== -1
                                  ? COLOR.GREEN
                                  : 'transparent',
                              alignSelf: 'center',
                              borderRadius: 40,
                            }}
                            source={{uri: item.image}}
                          />
                        </View>
                        <Text
                          style={[
                            Styles.small_label,
                            {width: null, textAlign: 'center'},
                          ]}>
                          {item.name}
                        </Text>
                      </View>
                    </TouchableWithoutFeedback>
                  )}
                  numColumns={'3'}
                  removeClippedSubviews={false}
                  showsVerticalScrollIndicator={false}
                  showsHorizontalScrollIndicator={false}
                  keyExtractor={(item, index) => index.toString()}
                />
              </View>
              <TouchableOpacity
                style={{
                  width: wp(90),
                  alignSelf: 'center',
                  height: 50,
                  alignItems: 'center',
                  justifyContent: 'center',
                  backgroundColor: COLOR.GREEN,
                  borderRadius: 10,
                  marginBottom: 20,
                }}
                onPress={() => {
                  this.setState({modalVisible: false}, (val) => {
                    this.props.route.params.updateMasterState(
                      'attraction_and_shows',
                      this.state.selectedIds,
                    );
                    this.props.navigation.goBack();
                  });
                }}>
                <Text
                  style={[
                    Styles.button_font,
                    {
                      color: COLOR.WHITE,
                      width: null,
                      textAlign: 'center',
                    },
                  ]}>
                  Apply
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  modal: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
  },
  modalBody: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    backgroundColor: COLOR.WHITE,
    borderWidth: 1,
    width: wp(100),
    marginTop: 50,
    borderColor: COLOR.BORDER,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    flex: 1,
  },
  image: {
    height: 120,
    width: 120,
    alignSelf: 'center',
    marginVertical: 20,
  },
  headingLabel: {
    fontFamily: FONTS.FAMILY_BOLD,
    fontSize: FONTS.MEDIUM,
    color: COLOR.BLACK,
  },
  singleLineView: {
    marginVertical: 20,
    borderBottomColor: COLOR.GRAY,
    width: wp(100),
    alignSelf: 'center',
    borderBottomWidth: 1,
  },
  borderView: {
    borderRadius: 10,
    borderWidth: 3,
    width: wp(90),
    borderColor: COLOR.GRAY,
    alignSelf: 'center',
  },
});

const mapStateToProps = (state, ownProps) => {
  return {
    attractionShow: state.tripPlanReducer.attractionShow,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getAttractionShow: () => {
      dispatch(TripPlanAction.getAttractionShow());
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SelectionHoc(AttractionShow));
