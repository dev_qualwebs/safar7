import React from 'react';
import {Modal, Text, View, StyleSheet, Image} from 'react-native';
import {
  FlatList,
  TextInput,
  TouchableOpacity,
  TouchableWithoutFeedback,
} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import COLOR from '../../../components/styles/Color';
import FONTS from '../../../components/styles/Fonts';
import IMAGES from '../../../components/styles/Images';
import Styles from '../../../components/styles/Styles';
import NavigationBar from '../../commonView/NavigationBar';
import TripPlanAction from '../../../redux/action/TripPlanAction';
import {connect} from 'react-redux';
import {SelectionHoc} from '../hoc/SelectionHoc';
import SimpleToast from 'react-native-simple-toast';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview';
import { DoneButtonKeyboard } from '../../commonView/CommonView';
class ExperienceScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: true,
      selectedIds: [],
      data: [],
      comment: [],
    };
    this.params = this.props.route.params;
    this.getDataById(this.params.id);
  }

  componentDidMount() {
    var data;
    switch (this.params.id) {
      case 2:
        if (this.props.foodExperience) {
          data = [...this.props.foodExperience];
        }
        break;
      case 3:
        if (this.props.sports) {
          data = [...this.props.sports];
        }
        break;
      case 4:
        if (this.props.workshops) {
          data = [...this.props.workshops];
        }
        break;
      case 5:
        if (this.props.airActivities) {
          data = [...this.props.airActivities];
        }
        break;
      case 6:
        if (this.props.seaActivities) {
          data = [...this.props.seaActivities];
        }
        break;
      case 7:
        if (this.props.outdoorExperience) {
          data = [...this.props.outdoorExperience];
        }
        break;
      case 8:
        if (this.props.indoorExperience) {
          data = [...this.props.indoorExperience];
        }
        break;
    }

    this.setState(
      {
        data: data,
      },
      console.log(this.state.data),
    );
  }

  componentDidUpdate(prevProps) {
    if (this.props !== prevProps) {
      var data;
      switch (this.params.id) {
        case 2:
          if (prevProps.foodExperience !== this.props.foodExperience) {
            data = [...this.props.foodExperience];
          }
          break;
        case 3:
          if (prevProps.sports !== this.props.sports) {
            data = [...this.props.sports];
          }
          break;
        case 4:
          if (prevProps.workshops !== this.props.workshops) {
            data = [...this.props.workshops];
          }
          break;
        case 5:
          if (prevProps.airActivities !== this.props.airActivities) {
            data = [...this.props.airActivities];
          }
          break;
        case 6:
          if (prevProps.seaActivities !== this.props.seaActivities) {
            data = [...this.props.seaActivities];
          }
          break;
        case 7:
          if (prevProps.outdoorExperience !== this.props.outdoorExperience) {
            data = [...this.props.outdoorExperience];
          }
          break;
        case 8:
          if (prevProps.indoorExperience !== this.props.indoorExperience) {
            data = [...this.props.indoorExperience];
          }
          break;
      }
      if (data) {
        this.setState(
          {
            data: data,
          },
          () => console.log(this.state.data),
        );
      }
    }

    if (this.props.selectedData !== prevProps.selectedData) {
      this.setState({
        selectedIds: this.props.selectedData,
      });
    }
  }

  getTitleById = (id) => {
    switch (id) {
      case 2:
        return 'Food Experience';
      case 3:
        return 'Sports';
      case 4:
        return 'Classes & Workshops';
      case 5:
        return 'Air Activities';
      case 6:
        return 'Sea Activities';
      case 7:
        return 'Outdoor Experiences';
      case 8:
        return 'Indoor Activities';
    }
  };

  getDataById = (id) => {
    switch (id) {
      case 2:
        this.props.getFoodExperiences();
        break;
      case 3:
        this.props.getSports();
        break;
      case 4:
        this.props.getWorkshops();
        break;
      case 5:
        this.props.getAirActivities();
        break;
      case 6:
        this.props.getSeaActivities();
        break;
      case 7:
        this.props.getOutdoorActivities();
        break;
      case 8:
        this.props.getIndoorActivities();
        break;
    }
  };

  SelectionView = (props) => {
    return (
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          marginTop: 10,
        }}>
        <Text style={[Styles.body_label, {width: 80}]}>{props.name}</Text>
        <Text style={[Styles.body_label, {width: null}]}>{props.time}</Text>
        <TouchableOpacity
          style={{
            height: 30,
            width: 30,
            borderRadius: 5,
            backgroundColor: props.bg,
            borderColor: COLOR.GRAY,
            justifyContent: 'center',
            alignItems: 'center',
            borderWidth: 1,
          }}>
          <Image
            resizeMode={'contain'}
            style={{height: 20, width: 20, alignSelf: 'center'}}
            source={props.image}
          />
        </TouchableOpacity>
      </View>
    );
  };

  render() {
    return (
      <View style={Styles.container}>
        <Modal
          animationType={'slide'}
          transparent={true}
          style={{backgroundColor: COLOR.BLACK}}
          visible={this.state.modalVisible}
          onRequestClose={() => {
            console.log('Modal has been closed.');
          }}>
          <View style={styles.modal}>
            <KeyboardAwareScrollView>
              <View style={styles.modalBody}>
                <NavigationBar
                  prop={this.props}
                  navHeight={40}
                  handleBack={() => {
                    this.setState(
                      {
                        modalVisible: false,
                      },
                      this.props.navigation.goBack(),
                    );
                  }}
                  right_image={IMAGES.filter_image}
                  name={this.getTitleById(this.params.id)}
                />

                <View
                  style={{
                    flex: 1,
                    width: wp(90),
                    alignSelf: 'center',
                    paddingVertical: 0,
                  }}>
                  <FlatList
                    style={{flex: 0}}
                    data={this.state.data}
                    renderItem={({item}) => (
                      <TouchableWithoutFeedback>
                        <View
                          style={{
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            marginVertical: 10,
                            alignItems: 'center',
                          }}>
                          <Text
                            style={[
                              Styles.body_label,
                              {width: 170, textAlign: 'left', flex: 1},
                            ]}>
                            {item.name}
                          </Text>

                          <TouchableOpacity
                            onPress={() => {
                              this.props.addItem(item);
                            }}
                            style={{
                              height: 30,
                              width: 30,
                              borderRadius: 5,
                              backgroundColor:
                                this.state.selectedIds.indexOf(item.id) == -1
                                  ? COLOR.WHITE
                                  : COLOR.GREEN,
                              borderColor: COLOR.GRAY,
                              justifyContent: 'center',
                              alignItems: 'center',
                              borderWidth: 1,
                            }}>
                            <Image
                              resizeMode={'contain'}
                              style={{
                                height: 20,
                                width: 20,
                                alignSelf: 'center',
                              }}
                              source={IMAGES.tick_image}
                            />
                          </TouchableOpacity>
                        </View>
                      </TouchableWithoutFeedback>
                    )}
                    numColumns={'1'}
                    removeClippedSubviews={false}
                    showsVerticalScrollIndicator={false}
                    showsHorizontalScrollIndicator={false}
                    keyExtractor={(item, index) => index.toString()}
                  />
                </View>
                <View
                  style={{
                    width: wp(90),
                    alignSelf: 'center',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                    flexDirection: 'row',
                    marginVertical: 10,
                  }}>
                  <View
                    style={{
                      flex: 1,
                      alignSelf: 'center',
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}>
                    <Text
                      style={[
                        Styles.subheading_label,
                        {
                          width: null,
                          textAlign: 'left',
                          alignSelf: 'flex-start',
                          marginBottom: 10,
                        },
                      ]}>
                      Comments
                    </Text>
                    <TextInput
                    inputAccessoryViewID={'uniqueID'}
                      multiline={true}
                      placeholderTextColor={COLOR.LIGHT_TEXT_COLOR}
                      style={[
                        {
                          alignSelf: 'flex-start',
                          height: 100,
                          borderRadius: 10,
                          borderColor: COLOR.BORDER,
                          borderWidth: 1,
                          width: wp(90),
                          paddingHorizontal: 20,
                          paddingVertical: 20,
                        },
                      ]}
                      placeholder={'Describe your destination'}
                    />
                    <DoneButtonKeyboard />
                  </View>
                </View>
                <TouchableOpacity
                  style={{
                    width: wp(90),
                    alignSelf: 'center',
                    height: 50,
                    alignItems: 'center',
                    justifyContent: 'center',
                    backgroundColor: COLOR.GREEN,
                    borderRadius: 10,
                    marginVertical: 20,
                  }}
                  onPress={() => {
                    if (this.state.selectedIds.length > 0) {
                      this.setState({modalVisible: false}, (val) => {
                        this.params.setSelection({
                          id: this.params.id,
                          types: this.state.selectedIds,
                        });
                        this.props.navigation.goBack();
                      });
                    } else {
                      SimpleToast.show('Select Atleast One');
                    }
                  }}>
                  <Text
                    style={[
                      Styles.button_font,
                      {
                        color: COLOR.WHITE,
                        width: null,
                        textAlign: 'center',
                      },
                    ]}>
                    Apply
                  </Text>
                </TouchableOpacity>
              </View>
            </KeyboardAwareScrollView>
          </View>
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  modal: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
  },
  modalBody: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    backgroundColor: COLOR.WHITE,
    borderWidth: 1,
    width: wp(100),
    marginTop: 50,
    borderColor: COLOR.BORDER,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    flex: 1,
  },
  image: {
    height: 120,
    width: 120,
    alignSelf: 'center',
    marginVertical: 20,
  },
  headingLabel: {
    fontFamily: FONTS.FAMILY_BOLD,
    fontSize: FONTS.MEDIUM,
    color: COLOR.BLACK,
  },
  singleLineView: {
    marginVertical: 20,
    borderBottomColor: COLOR.GRAY,
    width: wp(100),
    alignSelf: 'center',
    borderBottomWidth: 1,
  },
  borderView: {
    borderRadius: 10,
    borderWidth: 3,
    width: wp(90),
    borderColor: COLOR.GRAY,
    alignSelf: 'center',
  },
});

const mapDispatchToProps = (dispatch) => {
  return {
    getAirActivities: () => {
      dispatch(TripPlanAction.getAirActivities());
    },
    getSeaActivities: () => {
      dispatch(TripPlanAction.getSeaActivities());
    },
    getWorkshops: () => {
      dispatch(TripPlanAction.getWorkshops());
    },
    getFoodExperiences: () => {
      dispatch(TripPlanAction.getFoodExperiences());
    },
    getSports: () => {
      dispatch(TripPlanAction.getSports());
    },
    getOutdoorActivities: () => {
      dispatch(TripPlanAction.getOutdoorActivities());
    },
    getIndoorActivities: () => {
      dispatch(TripPlanAction.getIndoorActivities());
    },
  };
};

const mapStateToProps = (state, ownProps) => {
  return {
    airActivities: state.tripPlanReducer.airActivities,
    seaActivities: state.tripPlanReducer.seaActivities,
    workshops: state.tripPlanReducer.workshops,
    foodExperience: state.tripPlanReducer.foodExperience,
    sports: state.tripPlanReducer.sports,
    outdoorExperience: state.tripPlanReducer.outdoorExperience,
    indoorExperience: state.tripPlanReducer.indoorExperience,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SelectionHoc(ExperienceScreen));
