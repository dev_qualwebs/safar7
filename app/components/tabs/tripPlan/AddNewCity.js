import React from 'react';
import {Modal, Text, View, StyleSheet, Image, Alert} from 'react-native';
import {TextInput, TouchableOpacity} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import COLOR from '../../../components/styles/Color';
import FONTS from '../../../components/styles/Fonts';
import IMAGES from '../../../components/styles/Images';
import Styles from '../../../components/styles/Styles';
import NavigationBar from '../../commonView/NavigationBar';
import GooglePlacesTextInput from '../../commonView/GooglePlacesTextInput';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import SelectDate from '../calendar/SelectDate';
import moment from 'moment';
import Switch from '../../commonView/Switch';
import SimpleToast from 'react-native-simple-toast';
import API from '../../../api/Api';
import ActivityIndicator from '../../commonView/ActivityIndicator';
import {DoneButtonKeyboard} from '../../commonView/CommonView';

const api = new API();
export default class AddNewCity extends React.Component {
  constructor(props) {
    super(props);
    let data = this.props.route.params;
    this.state = {
      modalVisible: true,
      selectedIndex: null,
      city: '',
      days: 0,
      openCalendar: false,
      minDate: data.depart ? data.depart : Date(),
      maxDate: data.return ? data.return : new Date(5147962581000),
      checkInDate: null,
      checkOutDate: null,
      fromFlexibility: null,
      toFlexibility: null,
      exploreCity: false,
      localTransportation: null,
      english: false,
      arabic: false,
      comment: '',
      tripPlanId: data.data ? data.data.id : null,
      loading: data.data ? true : false,
      data: null,
    };
    if (data.data) {
      this.getSingleLocalTripPlan(data.data.id);
    }
  }

  componentDidMount() {}

  getSingleLocalTripPlan = async (id) => {
    let that = this;
    api
      .getSingleLocalTrip(id)
      .then((json) => {
        console.log(json.data.response);
        let data = json.data.response;
        that.setState({
          city: data.city ? data.city : '',
          exploreCity: data.explore_new_city ? 1 : 0,
          checkInDate: data.start_date ? data.start_date : '',
          checkOutDate: data.end_date ? data.end_date : '',
          fromFlexibility:
            data.start_date_flexibility_id != null
              ? data.start_date_flexibility_id
              : '',
          toFlexibility:
            data.end_date_flexibility_id != null
              ? data.end_date_flexibility_id
              : '',
          days: data.days ? data.days : '',
          localTransportation: data.local_trip_preference
            ? data.local_trip_preference.local_transportation[0]
              ? data.local_trip_preference.local_transportation[0].id
              : null
            : null,
          english: data.driver_language ? data.driver_language.english : false,
          arabic: data.driver_language ? data.driver_language.arabic : false,
          comment: data.comment ? data.comment : '',
          data: data,
          loading: false,
        });
      })
      .catch((error) => {
        that.setState({
          loading: false,
        });
        console.log(error);
      });
  };

  selectTransport = (item) => {
    this.setState({selectedIndex: item.id});
  };

  CommonView = (props) => (
    <View
      style={{
        flexDirection: 'row',
        width: 120,
        justifyContent: 'space-between',
      }}>
      <View
        style={{
          flex: 1,
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'flex-end',
        }}>
        <TouchableOpacity
          onPress={() => {
            if (this.state.days != 0) {
              this.setState({
                days: this.state.days - 1,
              });
            }
          }}
          style={{
            height: 30,
            width: 30,
            borderRadius: 15,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Image
            style={{height: 30, width: 30}}
            resizeMode={'cover'}
            source={IMAGES.minus_image}
          />
        </TouchableOpacity>
        <Text
          style={[
            Styles.button_font,
            {width: 40, color: COLOR.BLACK, textAlign: 'center'},
          ]}>
          {this.state.days}
        </Text>
        <TouchableOpacity
          onPress={() =>
            this.setState({
              days: this.state.days + 1,
            })
          }
          style={{
            height: 30,
            width: 30,
            borderWidth: 1,
            borderRadius: 15,
            borderColor: COLOR.GREEN,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Image style={{height: 20, width: 20}} source={IMAGES.plus_image} />
        </TouchableOpacity>
      </View>
    </View>
  );

  SelectionView = (props) => {
    return (
      <TouchableOpacity
        onPress={() => {
          this.setState({
            localTransportation: props.id,
          });
        }}
        style={styles.selection_view}>
        <Image
          style={{
            height: 30,
            width: 30,
            marginTop: 10,
            tintColor:
              this.state.localTransportation == props.id
                ? COLOR.GREEN
                : COLOR.BLACK,
          }}
          source={props.image}
          resizeMode={'contain'}
        />
        <Text
          style={[
            Styles.body_label,
            {
              width: null,
              textAlign: 'center',
              marginVertical: 10,
              fontFamily: FONTS.FAMILY_SEMIBOLD,
            },
          ]}>
          {props.name}
        </Text>
      </TouchableOpacity>
    );
  };

  updateMasterState = (attrName, value) => {
    this.setState({[attrName]: value}, () => {
      if (this.state.checkOutDate && this.state.checkInDate) {
        let days = moment(this.state.checkOutDate).diff(
          this.state.checkInDate,
          'day',
        );
        this.setState({
          days: days + 1,
        });
      }
    });
  };

  handleNavigation = () => {
    if (this.state.exploreCity == false && this.state.city == '') {
      SimpleToast.show('Enter City');
    } else if (this.state.days == 0 && this.state.checkInDate == null) {
      SimpleToast.show('Select date or days for destination');
    } else if (this.state.localTransportation == null) {
      SimpleToast.show('Select Local Transportation');
    } else {
      let data = {
        trip_id: this.props.route.params.id ? this.props.route.params.id : null,
        tripPlanId: this.state.tripPlanId,
        city: this.state.city,
        explore_new_city: this.state.exploreCity ? 1 : 0,
        start_date: this.state.checkInDate,
        end_date: this.state.checkOutDate,
        start_date_flexibility_id: this.state.fromFlexibility,
        end_date_flexibility_id: this.state.toFlexibility,
        days: this.state.days,
        local_transportation: [this.state.localTransportation],
        driver_language: {
          english: this.state.english,
          arabic: this.state.arabic,
        },
        comment: this.state.comment,
        data: this.state.data,
      };
      console.log('kkk', this.state.localTransportation);
      this.setState({modalVisible: false}, (val) => {
        this.props.navigation.replace('TripPlanOption', data);
      });
    }
  };

  render() {
    const {modalVisible} = this.state;
    return (
      <View style={Styles.container}>
        {this.state.openCalendar ? (
          <SelectDate
            allowRangeSelection={true}
            updateMasterState={this.updateMasterState.bind(this)}
            props={this.props}
            modal={true}
            minDate={this.state.minDate}
            maxDate={this.state.maxDate}
          />
        ) : this.state.loading ? (
          <ActivityIndicator loading={true} />
        ) : (
          <Modal
            animationType={'slide'}
            transparent={true}
            style={{backgroundColor: COLOR.BLACK}}
            visible={modalVisible}
            onRequestClose={() => {
              console.log('Modal has been closed.');
            }}>
            <View style={styles.modal}>
              <View style={styles.modalBody}>
                <KeyboardAwareScrollView
                bounces={false}
                  style={Styles.container}
                  keyboardShouldPersistTaps={'always'}
                  showsHorizontalScrollIndicator={false}
                  showsVerticalScrollIndicator={false}>
                  <NavigationBar
                    prop={this.props}
                    navHeight={40}
                    right_image={IMAGES.filter_image}
                    name={'Add New City'}
                  />
                  <View
                    style={{
                      width: wp(90),
                      alignSelf: 'center',
                      alignItems: 'center',
                      justifyContent: 'space-between',
                      flexDirection: 'row',
                      marginTop: 20,
                      zIndex: 900,
                    }}>
                    <Image
                      style={{width: 25, height: 25, marginRight: 15}}
                      source={IMAGES.pinpoint}
                    />
                    <View
                      style={{
                        flex: 1,
                        alignSelf: 'center',
                        zIndex: 1000,
                      }}>
                      <Text
                        style={[
                          Styles.body_label,
                          {
                            width: null,
                            textAlign: 'left',
                            fontFamily: FONTS.FAMILY_SEMIBOLD,
                            alignSelf: 'flex-start',
                          },
                        ]}>
                        City Name
                      </Text>

                      <GooglePlacesTextInput
                        value={this.state.city}
                        onValueChange={(text) =>
                          this.setState({
                            city: text,
                          })
                        }
                      />
                    </View>
                  </View>
                  <View
                    style={{
                      flexDirection: 'row',
                      width: wp(90),
                      alignSelf: 'center',
                      marginTop:5
                    }}>
                    <TouchableOpacity
                      onPress={() =>
                        this.setState({
                          exploreCity: !this.state.exploreCity,
                        })
                      }
                      style={{
                        height: 25,
                        width: 25,
                        borderRadius: 5,
                        marginRight: 5,
                        backgroundColor: this.state.exploreCity
                          ? COLOR.GREEN
                          : COLOR.WHITE,
                        borderColor: COLOR.GRAY,
                        justifyContent: 'center',
                        alignItems: 'center',
                        borderWidth: 1,
                      }}>
                      <Image
                        resizeMode={'contain'}
                        style={{height: 15, width: 15, alignSelf: 'center'}}
                        source={IMAGES.tick_image}
                      />
                    </TouchableOpacity>
                    <Text
                      style={[
                        Styles.small_label,
                        {fontFamily: FONTS.FAMILY_SEMIBOLD,marginLeft:10},
                      ]}>
                      Explore a new city
                    </Text>
                  </View>
                  <View
                    style={[
                      Styles.line_view,
                      {width: wp(90), alignSelf: 'center', marginTop: 20},
                    ]}
                  />
                  <View
                    style={{
                      width: wp(90),
                      alignSelf: 'center',
                      alignItems: 'center',
                      justifyContent: 'space-between',
                      flexDirection: 'row',
                      marginVertical: 20,
                    }}>
                    <Image
                      style={{width: 25, height: 25, marginRight: 15}}
                      source={IMAGES.notebook}
                    />

                    <Text
                      style={[
                        Styles.body_label,
                        {
                          width: null,
                          textAlign: 'left',
                          alignSelf: 'center',
                          alignItems: 'center',
                          flex: 1,
                          fontFamily: FONTS.FAMILY_BOLD,
                        },
                      ]}>
                      Dates
                    </Text>
                    <TouchableOpacity
                      onPress={() =>
                        this.setState({
                          openCalendar: true,
                        })
                      }>
                      <Text
                        style={[
                          Styles.body_label,
                          {
                            width: null,
                            textAlign: 'right',
                            alignSelf: 'center',
                            alignItems: 'center',
                            fontFamily: FONTS.FAMILY_BOLD,
                          },
                        ]}>
                        {this.state.checkInDate
                          ? moment(this.state.checkInDate).format('DD MMM') +
                            ' - ' +
                            moment(this.state.checkOutDate).format('DD MMM YY')
                          : 'Select Dates'}
                      </Text>
                    </TouchableOpacity>
                  </View>
                  <View style={{flexDirection: 'row', alignItems: 'center',alignSelf:'center'}}>
                    <View
                      style={[
                        Styles.line_view,
                        {width: wp(43), alignSelf: 'center', marginVertical: 0},
                      ]}
                    />
<Text style={{marginRight:5,marginLeft:5}}>or</Text>
                    <View
                      style={[
                        Styles.line_view,
                        {width: wp(43), alignSelf: 'center', marginVertical: 0},
                      ]}
                    />
                  </View>
                  <View
                    style={{
                      width: wp(90),
                      alignSelf: 'center',
                      alignItems: 'center',
                      justifyContent: 'space-between',
                      flexDirection: 'row',
                      marginVertical: 20,
                    }}>
                    <Image
                      style={{width: 25, height: 25, marginRight: 15}}
                      source={IMAGES.notebook}
                    />

                    <Text
                      style={[
                        Styles.body_label,
                        {
                          width: null,
                          textAlign: 'left',
                          alignSelf: 'center',
                          alignItems: 'center',
                          flex: 1,
                          fontFamily: FONTS.FAMILY_BOLD,
                        },
                      ]}>
                      Days
                    </Text>
                    <this.CommonView />
                  </View>
                  <View
                    style={[
                      Styles.line_view,
                      {width: wp(90), alignSelf: 'center', marginVertical: 0},
                    ]}
                  />
                  <View style={styles.shadow_view}>
                    <Text style={[Styles.subheading_label, {marginTop: 20}]}>
                      Local Transportation
                    </Text>
                    <Text style={Styles.small_label}>
                      (From cheapest to most expensive)
                    </Text>
                    <View
                      style={{
                        width: wp(90),
                        marginVertical: 20,
                        flexDirection: 'row',
                        alignSelf: 'center',
                        justifyContent: 'space-between',
                      }}>
                      <this.SelectionView
                        id={1}
                        name={'Tram\nMetro'}
                        image={IMAGES.train}
                      />
                      <this.SelectionView
                        id={2}
                        name={'Bus'}
                        image={IMAGES.bus_front}
                      />
                      <this.SelectionView
                        id={3}
                        name={'Taxi'}
                        image={IMAGES.taxi_image}
                      />
                      <this.SelectionView
                        id={4}
                        name={'Car with\ndriver'}
                        image={IMAGES.car_image}
                      />
                    </View>
                    {this.state.localTransportation == 4 ? (
                      <View>
                        <Text style={[Styles.subheading_label, {}]}>
                          Spoken language of the driver
                        </Text>
                        <View
                          style={{
                            width: wp(90),
                            marginVertical: 10,
                            flexDirection: 'row',
                            alignSelf: 'center',
                            justifyContent: 'space-between',
                          }}>
                          <Text style={[Styles.body_label, {width: null}]}>
                            English
                          </Text>
                          <Switch
                            isChecked={this.state.english}
                            onCheckedListener={() =>
                              this.setState({
                                english: !this.state.english,
                              })
                            }
                          />
                        </View>
                        <View
                          style={{
                            width: wp(90),
                            marginVertical: 10,
                            flexDirection: 'row',
                            alignSelf: 'center',
                            justifyContent: 'space-between',
                          }}>
                          <Text style={[Styles.body_label, {width: null}]}>
                            Arabic{' '}
                          </Text>
                          <Switch
                            isChecked={this.state.arabic}
                            onCheckedListener={() =>
                              this.setState({
                                arabic: !this.state.arabic,
                              })
                            }
                          />
                        </View>
                      </View>
                    ) : null}
                  </View>
                  <View
                    style={{
                      width: wp(90),
                      alignSelf: 'center',
                      alignItems: 'center',
                      justifyContent: 'space-between',
                      flexDirection: 'row',
                      marginTop: 20,
                    }}>
                    <View
                      style={{
                        flex: 1,
                        alignSelf: 'center',
                        alignItems: 'center',
                        justifyContent: 'center',
                      }}>
                      <Text
                        style={[
                          Styles.subheading_label,
                          {
                            width: null,
                            textAlign: 'left',
                            alignSelf: 'flex-start',
                            marginBottom: 10,
                          },
                        ]}>
                        Comments
                      </Text>
                      <TextInput
                        inputAccessoryViewID={'uniqueID'}
                        multiline={true}
                        value={this.state.comment}
                        onChangeText={(text) =>
                          this.setState({
                            comment: text,
                          })
                        }
                        style={[
                          {
                            alignSelf: 'flex-start',
                            height: 90,
                            borderRadius: 10,
                            borderColor: COLOR.BORDER,
                            borderWidth: 1,
                            width: wp(90),
                            paddingHorizontal: 10,
                            paddingVertical: 20,
                          },
                        ]}
                        placeholder={'Describe your destination'}
                        placeholderTextColor={COLOR.LIGHT_TEXT_COLOR}
                      />
                      <DoneButtonKeyboard />
                    </View>
                  </View>
                  <TouchableOpacity
                    style={{
                      alignItems: 'center',
                      justifyContent: 'center',
                      height: 50,
                      borderRadius: 10,
                      borderColor: COLOR.LIGHT_TEXT,
                      borderWidth: 1,
                      width: wp(90),
                      marginVertical: 20,
                      backgroundColor: COLOR.WHITE,
                      alignSelf: 'center',
                    }}
                    onPress={() => {
                      this.handleNavigation();
                    }}>
                    <Text style={[Styles.button_font, {color: COLOR.BLACK}]}>
                      Add City
                    </Text>
                  </TouchableOpacity>
                </KeyboardAwareScrollView>
              </View>
            </View>
          </Modal>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  modal: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
  },
  modalBody: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    backgroundColor: COLOR.WHITE,
    borderWidth: 1,
    width: wp(100),
    borderColor: COLOR.BORDER,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    flex: 1,
    marginTop: 50,
  },
  image: {
    height: 120,
    width: 120,
    alignSelf: 'center',
    marginVertical: 20,
  },
  headingLabel: {
    fontFamily: FONTS.FAMILY_BOLD,
    fontSize: FONTS.MEDIUM,
    color: COLOR.BLACK,
  },
  singleLineView: {
    marginVertical: 20,
    borderBottomColor: COLOR.GRAY,
    width: wp(100),
    alignSelf: 'center',
    borderBottomWidth: 1,
  },
  offer_view: {
    marginVertical: 0,
    width: wp(100),
    marginBottom: 20,
    height: 100,
    marginHorizontal: 10,
  },
  flatlist_view: {
    width: wp(90),
    marginVertical: 10,
    alignSelf: 'center',
  },
  shadow_view: {
    width: wp(90),
    alignSelf: 'center',
    borderRadius: 10,
    shadowColor: COLOR.VOILET,
    // shadowOffset: {width: 2, height: 2},
    // shadowOpacity: 0.2,
    backgroundColor: COLOR.WHITE,
    marginVertical: 0,
    elevation: 3,
  },
  selection_view: {
    height: 100,
    alignSelf: 'center',
    justifyContent: 'center',
    borderRadius: 10,
    borderColor: COLOR.GRAY,
    borderWidth: 1,
    backgroundColor: COLOR.WHITE,
    alignItems: 'center',
    flex: 1,
    marginHorizontal: 5,
  },
  select_city_view: {
    paddingVertical: 20,
    marginTop: -30,
    width: wp(90),
    alignSelf: 'center',
    justifyContent: 'center',
    borderRadius: 10,
    shadowColor: COLOR.VOILET,
    shadowOffset: {width: 2, height: 2},
    shadowOpacity: 0.2,
    backgroundColor: COLOR.WHITE,
    elevation: 3,
  },
});
