import React from 'react';
import {View, Image, StyleSheet, Text, SafeAreaView} from 'react-native';
import {
  TouchableOpacity,
  TouchableWithoutFeedback,
  TextInput,
} from 'react-native';
import {
  heightPercentageToDP,
  heightPercentageToDP as hp,
  widthPercentageToDP,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import COLOR from '../../../components/styles/Color';
import FONTS from '../../../components/styles/Fonts';
import IMAGES from '../../../components/styles/Images';
import Styles from '../../../components/styles/Styles';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview';
import Slider from 'react-native-slider';
import API from '../../../api/Api';
import SimpleToast from 'react-native-simple-toast';
import { DoneButtonKeyboard } from '../../commonView/CommonView';

const api = new API();
export default class TripPlanOption extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedIndex: 0,
      sliderValue: 1,
      attraction_and_shows: [],
      cuisine_types: [],
      trip_start_time: 0,
      activities_and_experience: [],
      tripPlanId: this.props.route.params.tripPlanId,
    };
    console.log(this.props.route.params);
  }
  componentDidMount() {
    if (this.props.route.params.data) {
      let data = this.props.route.params.data;
      let prefs = data.local_trip_preference
        ? data.local_trip_preference
        : null;
      this.setState({
        sliderValue: prefs ? prefs.budget_id : 0,
        attraction_and_shows: prefs
          ? prefs.attraction_and_shows.map((value) => value.id)
          : [],
        cuisine_types: prefs ? prefs.cuisine_type.map((value) => value.id) : [],
        trip_start_time: data.trip_start_time
          ? parseInt(data.trip_start_time.slice(0, 2))
          : 0,
        activities_and_experience: prefs
          ? this.getActivities(prefs.activity_experiences)
          : [],
      });
    }
  }

  getActivities = (data) => {
    let activities = data.map((value) => {
      return {
        id: value.activity_and_experience_id,
        types: value[
          Object.keys(value).find((v) => v != 'activity_and_experience_id')
        ]
          ? value[
              Object.keys(value).find((v) => v != 'activity_and_experience_id')
            ].map((v) => v.id)
          : [],
      };
    });
    return activities;
  };

  selectTransport = (name) => {
    let object = {
      updateMasterState: this.updateMasterState.bind(this),
    };
    switch (name) {
      case 'Attractions & Shows':
        object.selected = this.state.attraction_and_shows;
        this.props.navigation.navigate('AttractionShow', object);
        break;
      case 'Activities &\nExperience':
        object.selected = this.state.activities_and_experience;

        this.props.navigation.navigate('ActivityExperience', object);
        break;
      case 'Cuisine\nTypes':
        object.selected = this.state.cuisine_types;

        this.props.navigation.navigate('CuisineType', object);
        break;
    }
  };

  selectionView = (prop) => {
    return (
      <TouchableWithoutFeedback
        style={{height: 130}}
        onPress={() => this.selectTransport(prop.name)}>
        <View
          style={{
            flex: 1,
            borderRadius: 10,
            borderWidth: 1,
            height: 130,
            width: wp(28),
            margin: 5,
            borderColor: prop.hasData ? '#05778F' : COLOR.GRAY,
            alignSelf: 'center',
            alignItems: 'center',
            justifyContent: 'center',
            padding: 10,
            backgroundColor : prop.hasData ? COLOR.GREEN_OPACITY : COLOR.WHITE
          }}>
          <View
            style={{
              height: 60,
              width: 60,
              marginVertical: 10,
              backgroundColor: prop.hasData ? COLOR.WHITE : prop.bg,
              borderRadius: 10,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Image
              resizeMode={'contain'}
              source={prop.image}
              style={{
                height: 30,
                width: 30,
              }}
            />
          </View>
          <Text style={{marginBottom: 10, textAlign: 'center'}}>
            {prop.name}
          </Text>
        </View>
      </TouchableWithoutFeedback>
    );
  };

  addTripPlan = () => {
    let data = JSON.stringify({
      ...this.props.route.params,
      attraction_and_shows: this.state.attraction_and_shows,
      cuisine_types: this.state.cuisine_types,
      trip_start_time:
        `${this.state.trip_start_time}`.length == 1
          ? `0${this.state.trip_start_time}:00:00`
          : `${this.state.trip_start_time}:00:00`,
      activities_and_experience: this.state.activities_and_experience,
      budget_id: this.state.sliderValue,
      comment: '',
    });
    api
      .scheduleLocalTrip(this.props.route.params.trip_id, data)
      .then((json) => {
        console.log(json.data.response);
        SimpleToast.show('Added Successfully');
        this.setState({modalVisible: false}, (val) => {
          this.props.navigation.goBack();
        });
      })
      .catch((error) => {
        console.log(error.response.data);
      });
  };

  updateTripPlan = () => {
    let params = this.props.route.params;
    params.data = null;
    let data = JSON.stringify({
      ...params,
      attraction_and_shows: this.state.attraction_and_shows,
      cuisine_types: this.state.cuisine_types,
      trip_start_time:
        `${this.state.trip_start_time}`.length == 1
          ? `0${this.state.trip_start_time}:00:00`
          : `${this.state.trip_start_time}:00:00`,
      activities_and_experience: this.state.activities_and_experience,
      budget_id: this.state.sliderValue,
    });

    api
      .updateLocalTrip(this.state.tripPlanId, data)
      .then((json) => {
        console.log(json.data.response);
        SimpleToast.show('Added Successfully');
        this.setState({modalVisible: false}, (val) => {
          this.props.navigation.goBack();
        });
      })
      .catch((error) => {
        console.log(error.response.data);
      });
  };

  CommonView = (props) => (
    <View
      style={{
        flexDirection: 'row',
        width: 200,
        justifyContent: 'space-between',
      }}>
      <View
        style={{
          flex: 1,
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'flex-end',
        }}>
        <TouchableOpacity
          onPress={() => {
            if (this.state.trip_start_time == 0) {
              this.setState({
                trip_start_time: 24,
              });
            } else {
              this.setState({
                trip_start_time: this.state.trip_start_time - 1,
              });
            }
          }}
          style={{
            height: 30,
            width: 30,
            borderRadius: 15,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Image
            style={{height: 30, width: 30}}
            resizeMode={'contain'}
            source={IMAGES.minus_image}
          />
        </TouchableOpacity>
        <Text
          style={[
            Styles.button_font,
            {width: 80, color: COLOR.BLACK, textAlign: 'center'},
          ]}>
          {`${this.state.trip_start_time}`.length == 1
            ? `0${this.state.trip_start_time}:00`
            : `${this.state.trip_start_time}:00`}
        </Text>
        <TouchableOpacity
          onPress={() => {
            if (this.state.trip_start_time == 24) {
              this.setState({
                trip_start_time: 0,
              });
            } else {
              this.setState({
                trip_start_time: this.state.trip_start_time + 1,
              });
            }
          }}
          style={{
            height: 30,
            width: 30,
            borderWidth: 1,
            borderRadius: 15,
            borderColor: COLOR.GREEN,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Image style={{height: 20, width: 20}} source={IMAGES.plus_image} />
        </TouchableOpacity>
      </View>
    </View>
  );

  updateMasterState = (attrName, value) => {
    this.setState({[attrName]: value});
  };

  handleValidation = () => {
    if (this.state.attraction_and_shows.length == 0) {
      SimpleToast.show('Select Attraction and shows');
    } else if (this.state.cuisine_types.length == 0) {
      SimpleToast.show('Select Cuisine Types ');
    } else if (this.state.activities_and_experience.length == 0) {
      SimpleToast.show('Select Activities and Experience');
    } else {
      this.state.tripPlanId ? this.updateTripPlan() : this.addTripPlan();
    }
  };

  render() {
    return (
      <SafeAreaView style={[Styles.container]}>
        <KeyboardAwareScrollView
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}>
          <View>
            <View style={style.topBar}>
              <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                <Image
                  style={{height: 15, width: 15}}
                  source={IMAGES.back_arrow}
                />
              </TouchableOpacity>
              <Text
                style={[
                  Styles.subheading_label,
                  {width: null, fontFamily: FONTS.FAMILY_BOLD},
                ]}>
                Trip Plan Options
              </Text>
              <TouchableOpacity>
                <Image
                  style={{height: 20, width: 20}}
                  source={IMAGES.filter_image}
                />
              </TouchableOpacity>
            </View>
            <View
              style={{
                flexDirection: 'row',
                width: wp(90),
                alignSelf: 'center',
                marginTop: 20,
                justifyContent: 'space-between',
              }}>

              <this.selectionView
              hasData={this.state.attraction_and_shows.length > 0 ? true : false}
                name={'Attractions & Shows'}
                image={IMAGES.running_man}
                bg={'#05778F20'}
              />
              <this.selectionView
              hasData={this.state.activities_and_experience.length > 0 ? true : false}
                name={'Activities &\nExperience'}
                image={IMAGES.table_tennis}
                bg={'#02599520'}
              />
              <this.selectionView
              hasData={this.state.cuisine_types.length > 0 ? true : false}
                name={'Cuisine\nTypes'}
                image={IMAGES.knife}
                bg={'#CF212B20'}
              />
            </View>
          </View>

          <View
            style={[Styles.line_view, {marginVertical: 20, width: wp(90)}]}
          />
          <View
            style={{
              flexDirection: 'row',
              width: wp(90),
              alignSelf: 'center',
              justifyContent: 'space-between',
            }}>
            <Text
              style={[
                Styles.subheading_label,
                {width: null, alignSelf: 'center'},
              ]}>
              Budget
            </Text>
            <Text
              style={[Styles.small_label, {width: null, alignSelf: 'center'}]}>
              SAR per night
            </Text>
          </View>
          <View style={{width: wp(90), alignSelf: 'center'}}>
            <Slider
              step={1}
              minimumValue={1}
              maximumValue={5}
              value={this.state.sliderValue}
              minimumTrackTintColor={COLOR.GREEN}
              thumbTintColor={COLOR.GREEN}
              maximumTrackTintColor={COLOR.GRAY}
              onValueChange={(value) => this.setState({sliderValue: value})}
            />
            <View
              style={{flexDirection: 'row', justifyContent: 'space-between'}}>
              <Text>200</Text>
              <Text>200-400</Text>
              <Text>400-600</Text>
              <Text>600-800</Text>
              <Text>800</Text>
            </View>
          </View>
          <View
            style={[Styles.line_view, {marginVertical: 20, width: wp(90)}]}
          />
          <View
            style={{
              width: wp(90),
              alignSelf: 'center',
              alignItems: 'center',
              justifyContent: 'space-between',
              flexDirection: 'row',
              marginVertical: 20,
            }}>
            <Text
              style={[
                Styles.subheading_label,
                {
                  width: null,
                  textAlign: 'left',
                  alignSelf: 'center',
                  alignItems: 'center',
                  flex: 1,
                },
              ]}>
              Trip Start Time
            </Text>
            <this.CommonView />
          </View>
          <View
            style={[Styles.line_view, {marginVertical: 20, width: wp(90)}]}
          />

          <Text style={[Styles.subheading_label]}>Comments</Text>
          <TextInput
          inputAccessoryViewID={'uniqueID'}
            placeholder={'Write your comment'}
            placeholderTextColor={COLOR.LIGHT_TEXT_COLOR}
            style={[
              {
                height: 120,
                alignSelf: 'center',
                paddingHorizontal: 20,
                paddingVertical: 20,
                width: wp(90),
                borderRadius: 10,
                borderWidth: 1,
                borderColor: COLOR.GRAY,
                marginTop: 10,
              },
            ]}
            multiline={true}
          />
          <DoneButtonKeyboard />
          <View style={{marginVertical: 20, alignSelf: 'center'}}>
            <TouchableOpacity
              style={{
                width: wp(90),
                alignSelf: 'center',
                height: 50,
                alignItems: 'center',
                justifyContent: 'center',
                marginTop: 20,
                borderRadius: 10,
              }}
              onPress={() => {
                this.setState({modalVisible: false}, (val) => {
                  this.addTripPlan();
                  this.props.navigation.goBack();
                });
              }}>
              <Text
                style={[
                  Styles.small_label,
                  {
                    color: COLOR.LIGHT_TEXT_COLOR,
                    width: null,
                    textAlign: 'right',
                  },
                ]}>
                Clear all options
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={{
                width: wp(90),
                alignSelf: 'center',
                height: 50,
                alignItems: 'center',
                justifyContent: 'center',
                backgroundColor: COLOR.GREEN,
                borderRadius: 10,
                marginTop: 0,
              }}
              onPress={() => {
                this.handleValidation();
              }}>
              <Text
                style={[
                  Styles.button_font,
                  {
                    color: COLOR.WHITE,
                    width: null,
                    textAlign: 'center',
                  },
                ]}>
                Apply
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={{
                width: wp(90),
                alignSelf: 'center',
                height: 50,
                alignItems: 'center',
                justifyContent: 'center',

                borderRadius: 10,
              }}>
              <Text
                style={[
                  Styles.small_label,
                  {
                    color: COLOR.BLACK,
                    width: null,
                    textAlign: 'right',
                  },
                ]}>
                Skip trip plan options
              </Text>
            </TouchableOpacity>
          </View>
        </KeyboardAwareScrollView>
      </SafeAreaView>
    );
  }
}

const style = StyleSheet.create({
  topBar: {
    height: 50,
    backgroundColor: COLOR.WHITE,
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: wp(90),
    alignSelf: 'center',
    marginVertical: 0,
    alignItems: 'center',
  },
});
