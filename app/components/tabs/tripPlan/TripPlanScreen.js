import {View, Image, Text, StyleSheet} from 'react-native';
import React from 'react';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import COLOR from '../../styles/Color';
import Styles from '../../styles/Styles';
import {TouchableOpacity, FlatList} from 'react-native';
import IMAGES from '../../styles/Images';
import FONTS from '../../styles/Fonts';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import TripDateView from '../../commonView/TripDateView';
import API from '../../../api/Api';
import SimpleToast from 'react-native-simple-toast';
import RequestAction from '../../../redux/action/RequestAction';
import { connect } from 'react-redux';

const api = new API();
 class TripPlanScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      destinations: [],
      tripId: null,
      tripDepart: null,
      tripReturn: null,
      departFlexibility: null,
      returnFlexibility: null,
      stayLocalTrips: false,
      isEdit : false
    };
  }

  componentDidMount() {
    if (this.props.route.params) {
      let data = this.props.route.params;
      this.unsubscribe = this.props.navigation.addListener('focus', () => {
        this.getLocalTrips(data.id);
      });

      if (this.props.route.params && this.props.route.params.isEdit) {
        this.setState({isEdit: true});
      }

      this.setState({
        tripDepart: data.depart ? data.depart : null,
        tripReturn: data.return ? data.return : null,
        tripId: data.id,
        departFlexibility: data.departFlexibility
          ? data.departFlexibility
          : null,
        returnFlexibility: data.returnFlexibility
          ? data.returnFlexibility
          : null,
      });
    }
  }

  componentWillUnmount() {
    this.unsubscribe();
  }

  getLocalTrips = async (val) => {
    api
      .getLocalTrips(val)
      .then((json) => {
        console.log(json.data.response);
        if (
          json.data.response.some((value) => value.is_stay_trip == 1) ==
            false &&
          this.state.stayLocalTrips
        ) {
          SimpleToast.show("You don't have any stay scheduled");
        }
        this.setState({
          destinations: json.data.response,
          stayLocalTrips: json.data.response.some(
            (value) => value.is_stay_trip == 1,
          ),
        });
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  getStayLocalTrip = async () => {
    let data = JSON.stringify({
      checked: this.state.stayLocalTrips,
    });
    let that = this;
    api
      .getStayLocalTrip(this.state.tripId, data)
      .then((json) => {
        console.log(json.data.response);
        that.getLocalTrips(that.state.tripId);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  removeLocalTrip = (async = (id) => {
    api
      .removeLocalTrip(id)
      .then((json) => {
        console.log(json.data.response);
        this.getLocalTrips(this.state.tripId);
      })
      .catch((error) => {
        console.log(error.response.data);
      });
  });

  SelectionView = (props) => {
    const screens = this.props.route.params.screens;
    const index = this.props.route.params.index;
    return (
      <View style={style.selection_view}>
        <Image
          style={{height: 30, width: 30, marginTop: 10}}
          source={props.image}
          resizeMode={'contain'}
        />
        <Text
          style={[
            Styles.small_label,
            {width: null, textAlign: 'center', marginVertical: 10, height: 40},
          ]}>
          {props.name}
        </Text>
      </View>
    );
  };

  render() {
    const screens = this.props.route.params.screens;
    const index = this.props.route.params.index;
    const id = this.props.route.params.id;

    return (
      <View style={Styles.container}>
        <View
          style={{
            height: hp(25),
            backgroundColor: COLOR.GREEN,
            borderBottomRightRadius: 15,
            borderBottomLeftRadius: 15,
          }}>
          <View
            style={[
              Styles.mainView,
              {
                height: 45,
                marginTop: getStatusBarHeight(true),
                justifyContent: 'center',
                alignSelf: 'center',
                alignItems: 'center',
                flexDirection: 'row',
              },
            ]}>
            <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
              <Image
                style={{height: 15, width: 15, tintColor: COLOR.WHITE}}
                source={IMAGES.back_arrow}
              />
            </TouchableOpacity>
            <View>
              <Text
                style={[
                  Styles.heading_label,
                  {
                    textAlign: 'center',
                    color: COLOR.WHITE,
                    marginVertical: 0,
                    fontFamily: FONTS.FAMILY_BOLD,
                  },
                ]}>
                Trip Plan
              </Text>
              <Image
                // source={IMAGES.notification_bell}
                style={{
                  width: 20,
                  height: 20,
                  right: 0,
                  position: 'absolute',
                  tintColor: COLOR.WHITE,
                }}
              />
            </View>
          </View>
          <TripDateView
            departDate={this.state.tripDepart}
            returnDate={this.state.tripReturn}
            departFlexibility={this.state.departFlexibility}
            returnFlexibility={this.state.returnFlexibility}
          />
        </View>
        <View style={style.select_city_view}>
          <View
            style={{
              flexDirection: 'row',
              alignSelf: 'center',
              alignItems: 'flex-start',
              width: wp(85),
            }}>
            <TouchableOpacity
              style={{
                width: 30,
                height: 30,
                justifyContent: 'center',
                alignItems: 'center',
                marginRight: 15,
                borderRadius: 5,
                borderWidth: 1,
                borderColor: COLOR.GRAY,
                backgroundColor: this.state.stayLocalTrips
                  ? COLOR.GREEN
                  : COLOR.WHITE,
              }}
              onPress={() =>
                this.setState(
                  {
                    stayLocalTrips: !this.state.stayLocalTrips,
                  },
                  () => this.getStayLocalTrip(),
                )
              }>
              <Image
                style={{
                  height: 20,
                  width: 20,
                }}
                source={IMAGES.tick_image}
                resizeMode={'contain'}
              />
            </TouchableOpacity>
            <View style={{flex:1}}>
              <Text style={[Styles.small_label,{width:null}]}>
                Make a Trip plan for the cities included in the Stay
                reservations.
              </Text>
            </View>
          </View>
          <View
            style={[Styles.line_view, {width: wp(85), marginVertical: 20}]}
          />
          <FlatList
            showsVerticalScrollIndicator={false}
            data={this.state.destinations}
            style={{flex: 1}}
            renderItem={({item, index}) => (
              <View>
                <View
                  style={{
                    alignSelf: 'center',
                    paddingHorizontal: 20,
                    backgroundColor: COLOR.WHITE,
                    alignItems: 'center',
                  }}>
                  <View
                    style={{
                      height: 50,
                      width: wp(85),
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      alignSelf: 'center',
                      alignItems: 'center',
                    }}>
                    <TouchableOpacity
                      onPress={() =>
                        this.props.navigation.navigate('AddNewCity', {
                          ...this.props.route.params,
                          data: item,
                        })
                      }>
                      <Text
                        style={[
                          Styles.body_label,
                          {
                            width: null,
                            fontFamily: FONTS.FAMILY_SEMIBOLD,
                            color: COLOR.BLACK,
                          },
                        ]}>
                        {item.city}
                      </Text>
                    </TouchableOpacity>

                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                      <TouchableOpacity
                        onPress={() => this.removeLocalTrip(item.id)}
                        style={{
                          height: 40,
                          aspectRatio: 1,
                          alignItems: 'center',
                          justifyContent: 'center',
                        }}>
                        <Image
                          style={{
                            height: 25,
                            width: 25,
                          }}
                          source={IMAGES.cross_icon}
                        />
                      </TouchableOpacity>
                    </View>
                  </View>
                  <View
                    style={[
                      Styles.line_view,
                      {width: wp(85), marginVertical: 10},
                    ]}
                  />
                </View>
              </View>
            )}
            numColumns={1}
            ListEmptyComponent={() => (
              <Text
                style={{
                  textAlign: 'center',
                  color: COLOR.VOILET,
                  marginVertical: 40,
                }}>
                You did not add any Destination
              </Text>
            )}
            keyExtractor={(item, index) => index.toString()}
          />
          <TouchableOpacity
            onPress={() =>
              this.props.navigation.navigate(
                'AddNewCity',
                this.props.route.params,
              )
            }
            style={{
              width: wp(85),
              alignSelf: 'center',
              height: 50,
              alignItems: 'center',
              justifyContent: 'center',
              backgroundColor: COLOR.WHITE,
              borderRadius: 10,
              borderColor: COLOR.LIGHT_TEXT,
              borderWidth: 1,
              marginTop: 40,
            }}>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <Image
                style={{
                  width: 25,
                  height: 25,
                  borderRadius: 10,

                  marginRight: 5,
                }}
                source={IMAGES.add_plus}
              />

              <Text
                style={[
                  Styles.button_font,
                  {
                    color: COLOR.BLACK,
                    width: null,
                    textAlign: 'center',
                  },
                ]}>
                Add New Destination
              </Text>
            </View>
          </TouchableOpacity>
          <Text style={[Styles.small_label,{textAlign:'center',color:'#CC3E3E',alignSelf:'center',marginTop:20}]}>This service will not include reservations for a flight or a stay. </Text>
        </View>

        

        <TouchableOpacity
          style={{
            width: wp(85),
            alignSelf: 'center',
            height: 50,
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: COLOR.GREEN,
            borderRadius: 10,
            bottom: 20,
            position: 'absolute',
            marginTop: 20,
          }}
          onPress={() => {
            this.props.getUpcomingTrips();
            if(!this.state.isEdit){
            if (screens[index + 1]) {
              this.props.navigation.replace(screens[index + 1], {
                screens: screens,
                index: index + 1,
                id: id,
                depart: this.state.depart,
                return: this.state.return,
              });
            } else if (this.state.isPackage) {
              this.props.navigation.goBack();
            } else {
              if (this.state.destinations.length == 0) {
                SimpleToast.show('Add any destination to continue.');
              } else {
                this.props.navigation.navigate('HomeStack');
              }
            }} else {
              this.props.navigation.goBack()
            }
          }}>
          <Text
            style={[
              Styles.button_font,
              {
                color: COLOR.WHITE,
                width: null,
                textAlign: 'center',
              },
            ]}>
            Next
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const style = StyleSheet.create({
  shadow_view: {
    width: wp(90),
    alignSelf: 'center',
    borderRadius: 10,
    shadowColor: COLOR.VOILET,
    shadowOffset: {width: 2, height: 2},
    shadowOpacity: 0.2,
    backgroundColor: COLOR.WHITE,
    marginVertical: 20,
    elevation: 3,
  },
  select_city_view: {
    paddingVertical: 20,
    marginTop: -30,
    marginBottom: 100,
    width: wp(90),
    flex: 1,
    alignSelf: 'center',
    justifyContent: 'center',
    borderRadius: 10,
    shadowColor: COLOR.VOILET,
    shadowOffset: {width: 2, height: 2},
    shadowOpacity: 0.2,
    backgroundColor: COLOR.WHITE,
    elevation: 3,
  },
  selection_view: {
    height: 100,
    alignSelf: 'center',
    justifyContent: 'center',
    borderRadius: 10,
    borderColor: COLOR.GRAY,
    borderWidth: 1,
    backgroundColor: COLOR.WHITE,
    alignItems: 'center',
    flex: 1,
    marginHorizontal: 5,
  },
});


const mapStateToProps = (state, ownProps) => {
  return {

  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getUpcomingTrips: (val) => {
      dispatch(RequestAction.getUpcomingTrips(val));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(TripPlanScreen);
