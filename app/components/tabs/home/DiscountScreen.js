import React from 'react';
import {Modal, Text, View, StyleSheet, Image, Button, ScrollView} from 'react-native';
import {
  FlatList,
  TouchableOpacity,
} from 'react-native';
import {
  heightPercentageToDP,
  widthPercentageToDP,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import COLOR from '../../../components/styles/Color';
import FONTS from '../../../components/styles/Fonts';
import IMAGES from '../../../components/styles/Images';
import Styles from '../../../components/styles/Styles';
import NavigationBar from '../../commonView/NavigationBar';
import {D_TRAIN_CLASS} from '../../../helper/Constants';
import HTMLView from 'react-native-htmlview';
import RenderHtml from 'react-native-render-html';



export default class DiscountScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: true,
      offerData:'',
    };
  }

  componentDidMount(){
    if(this.props.route.params && this.props.route.params.data){
      this.setState({offerData:this.props.route.params.data})
    }
  }

  updateMasterState = (attrName, value) => {
    this.setState({[attrName]: value});
  };

  render() {
    const {offerData} = this.state;
    return (
      <View style={Styles.container}>
        <Modal
          animationType={'slide'}
          transparent={true}
          style={{backgroundColor: COLOR.BLACK}}
          visible={this.state.modalVisible}
          onRequestClose={() => {
            console.log('Modal has been closed.');
          }}>
          <View style={styles.modal}>
            <View style={styles.modalBody}>
              <NavigationBar
                prop={this.props}
                navHeight={45}
                name={`${offerData.percentage}% Discount`}
              />
              <ScrollView showsVerticalScrollIndicator={false} bounces={false}>
              <Image
                style={{width: wp(100), height: 120}}
                source={offerData.image ? {uri:offerData.image} : IMAGES.big_offer_view}
              />
              <View
                style={{
                  flex: 1,
                  width: wp(90),
                  alignSelf: 'center',
                  paddingVertical: 20,
                }}>
                <Text
                  style={[{alignSelf: 'flex-start', marginBottom: 10,fontFamily:FONTS.FAMILY_BOLD,fontSize:16},
                  ]}>
                 About the offer
                </Text>
                <Text
                  style={[Styles.body_label, {fontSize: 14, color: '#465A64'}]}>
                  {offerData.description ? offerData.description : ''}
                </Text>
                <Text
                  style={[Styles.body_label, {fontSize: 14,marginVertical:5, color: '#465A64'}]}>
                  {offerData.offer_start_date ? offerData.offer_start_date : ''} - {offerData.offer_end_date ? offerData.offer_end_date : ''}
                </Text>
                <View style={{width:widthPercentageToDP(100),alignSelf:'center',height:1,backgroundColor:'#EBEBEB',marginVertical:10}}/>
                <Text
                  style={[{alignSelf: 'flex-start', marginBottom: 5,fontFamily:FONTS.FAMILY_BOLD,fontSize:16},
                  ]}>
                Terms & Conditions
                </Text>
                 <RenderHtml
      contentWidth={widthPercentageToDP(90)}
      source={{html:offerData.terms_and_condition ? offerData.terms_and_condition : ''}}
    />
                <View style={{width:widthPercentageToDP(100),alignSelf:'center',height:1,backgroundColor:'#EBEBEB',marginVertical:10}}/>
               
                <View
                  style={{flexDirection: 'row', width: null, marginTop: 20}}>
                  <Text style={[Styles.small_label, {width: null}]}>
                    *Please review{' '}
                  </Text>
                  <TouchableOpacity
                  onPress={()=>{
                    this.setState({modalVisible:false},()=>{
                      this.props.navigation.replace('PolicyPage',{screenTitle:'Terms and Condition'})
                    })
                  
                  }}
                  >
                    <Text
                      style={[
                        Styles.small_label,
                        {width: null, color: COLOR.GREEN,marginBottom:-3,},
                      ]}>
                      Terms & Conditions
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>
              </ScrollView>
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  modal: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
  },
  modalBody: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    backgroundColor: COLOR.WHITE,
    borderWidth: 1,
    width: wp(100),
    marginTop: 50,
    
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    flex: 1,
  },
  image: {
    height: 120,
    width: 120,
    alignSelf: 'center',
    marginVertical: 20,
  },
  headingLabel: {
    fontFamily: FONTS.FAMILY_BOLD,
    fontSize: FONTS.MEDIUM,
    color: COLOR.BLACK,
  },
  singleLineView: {
    marginVertical: 20,
    borderBottomColor: COLOR.GRAY,
    width: wp(100),
    alignSelf: 'center',
    borderBottomWidth: 1,
  },
  borderView: {
    borderRadius: 10,
    borderWidth: 3,
    width: wp(90),
    borderColor: COLOR.GRAY,
    alignSelf: 'center',
  },
});
