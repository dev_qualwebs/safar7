import React from 'react';
import {Modal, Text, View, StyleSheet, Image, Alert} from 'react-native';
import {ScrollView, TextInput, TouchableOpacity} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import API from '../../../../api/Api';
import COLOR from '../../../../components/styles/Color';
import FONTS from '../../../../components/styles/Fonts';
import IMAGES from '../../../../components/styles/Images';
import Styles from '../../../../components/styles/Styles';
import NavigationBar from '../../../commonView/NavigationBar';
import HelpDetails from './HelpDetails';
import { connect } from 'react-redux';
import ProfileAction from '../../../../redux/action/ProfileAction';

const api = new API();

class EmergencyMainView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: true,
      showDetails: false,
      selectedImage: '',
      selectedName: '',
      paraData: '',
    };
  }

  componentDidMount(){
  if(this.props.userDetail == null){
    this.props.getUserDetail();
  }
  }

  updateMasterState = (attrName, value) => {
    this.setState({[attrName]: value});
  };

  optionView = (props) => (
    <TouchableOpacity
      onPress={() => {

      }}>
      <View
        style={{
          marginVertical: 5,
          height: 50,
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
          paddingHorizontal: 10,
        }}>
        <Image
          style={{height: 25, width: 25,marginLeft:5}}
          source={props.image}
          resizeMode={'contain'}
        />
        <Text
          style={[
            Styles.body_label,
            {width: null, textAlign: 'left', flex: 1, marginLeft: 10},
          ]}>
          {props.name}
        </Text>
        <Image style={{height: 12, width: 12}} source={IMAGES.right_arrow} />
      </View>
    </TouchableOpacity>
  );

  SingleLineView = () => (
    <View
      style={{
        borderBottomColor: COLOR.GRAY,
        marginVertical: 5,
        borderBottomWidth: 2,
      }}
    />
  );

  render() {
    return (
      <View style={Styles.container}>
        {this.state.showDetails ? (
          <HelpDetails
            props={this.props}
            image={this.state.selectedImage}
            name={this.state.selectedName}
            countryId={this.state.selectedCountryId}
            paraData={this.state.paraData}
            updateMasterState={this.updateMasterState}
          />
        ) : (
          <Modal
            animationType={'slide'}
            transparent={true}
            style={{backgroundColor: COLOR.BLACK}}
            visible={this.state.modalVisible}
            onRequestClose={() => {
              console.log('Modal has been closed.');
            }}>
            <View style={styles.modal}>
              <View style={styles.modalBody}>
                <NavigationBar
                  prop={this.props}
                  navHeight={40}
                  name={'Emergency'}
                />

                <View style={{flex: 1}}>
                  <View style={[styles.borderView, {marginVertical: 10}]}>
                    <this.optionView
                      name={'Police'}
                      image={IMAGES.police}
                      type={1}
                    />
                    <this.SingleLineView />
                    <this.optionView
                      name={'Ambulance'}
                      image={IMAGES.ambulance}
                      type={2}
                    />
                    <this.SingleLineView />
                    <this.optionView
                      name={'Civil defense'}
                      image={IMAGES.defence}
                      type={3}
                    />
                    <this.SingleLineView />
                    <this.optionView
                      name={'Traffic police'}
                      image={IMAGES.traffic}
                    />
                  </View>
                </View>
               
              </View>
            </View>
          </Modal>
        )}
      </View>
    );
  }
}

mapStateToProps = (state, ownProps) => {
  return {
    userDetail: state.profileReducer.userDetail,
    specialNeeds: state.profileReducer.specialNeeds,
  };
};

mapDispatchToProps = (dispatch) => {
  return {
    getUserDetail: (val) => {
      dispatch(ProfileAction.getUserInfo(val));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(EmergencyMainView);


const styles = StyleSheet.create({
  modal: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
  },
  modalBody: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    backgroundColor: COLOR.WHITE,
    borderWidth: 1,
    width: wp(100),
    marginTop: 50,
    borderColor: COLOR.BORDER,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    flex: 1,
  },
  image: {
    height: 120,
    width: 120,
    alignSelf: 'center',
    marginVertical: 20,
  },
  headingLabel: {
    fontFamily: FONTS.FAMILY_BOLD,
    fontSize: FONTS.MEDIUM,
    color: COLOR.BLACK,
  },
  singleLineView: {
    marginVertical: 20,
    borderBottomColor: COLOR.GRAY,
    width: wp(100),
    alignSelf: 'center',
    borderBottomWidth: 1,
  },
  borderView: {
    borderRadius: 10,
    borderWidth: 2,
    width: wp(90),
    borderColor: COLOR.GRAY,
    alignSelf: 'center',
  },
});
