import React from 'react';
import {Modal, Text, View, StyleSheet, Image, Alert} from 'react-native';
import {ScrollView, TextInput, TouchableOpacity} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import COLOR from '../../../../components/styles/Color';
import FONTS from '../../../../components/styles/Fonts';
import IMAGES from '../../../../components/styles/Images';
import Styles from '../../../../components/styles/Styles';
import NavigationBar from '../../../commonView/NavigationBar';
import {FloatingTitleTextInputField} from '../../../../helper/FloatingTitleTextInputField';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview';
import GooglePlacesTextInput from '../../../commonView/GooglePlacesTextInput';
import SimpleToast from 'react-native-simple-toast';
import ActionSheet from '../../../commonView/ActionSheet';
import ImagePicker from 'react-native-image-crop-picker';
import MapView from 'react-native-maps';
import {ModalDatePicker} from '../../../commonView/CommonView';

export default class AddPlace extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      address: '',
      time: '',
      duration: '',
      note: '',
      phoneNumber: '',
      website: '',
      modalVisible: true,
      id: '',
      photoModelVisible: false,
    };
  }

  updateMasterState = (attrName, value) => {
    this.setState({[attrName]: value});
  };

  SingleLineView = () => (
    <View
      style={{
        borderBottomColor: COLOR.GRAY,
        marginBottom: 10,
        borderBottomWidth: 1,
      }}
    />
  );

  componentDidMount() {
    if (this.props.route.params && this.props.route.params.id) {
      this.setState({id: this.props.route.params.id});
    }
  }

  handleSheet = (val) => {
    if (val == 1) {
      this.chooseImage(1);
    } else if (val == 2) {
      this.chooseImage(2);
    } else if (val == 3) {
      this.setState({
        photoModelVisible: false,
        modalVisible: true,
      });
    }
  };

  chooseImage = (val) => {
    let options = {
      includeBase64: true,
      title: 'Select Image',
      width: 1630,
      height: 750,
      compressImageMaxWidth: 1630,
      compressImageMaxHeight: 750,
      multiple: true,
    };

    if (val == 1) {
      ImagePicker.openCamera({
        title: 'Select Image',
        width: 1024,
        height: 472,
        compressImageMaxWidth: 1024,
        compressImageMaxHeight: 472,
        includeBase64: true,
      }).then((response) => {
        console.log('image local reposnse', response);
        this.setState({
          photoModelVisible: false,
          modalVisible: true,
        });
        // this.callUploadImage(['data:image/jpeg;base64,' + response.data], val);
      });
    } else if (val == 2) {
      ImagePicker.openPicker(options).then((response) => {
        console.log('image local reposnse', response);
        this.setState({
          photoModelVisible: false,
          modalVisible: true,
        });
        // for (let i = 0; i < response.length; i++) {
        //   this.callUploadImage(response[i], val);
        // }
        let base64Response = response.map((val) => {
          return 'data:image/jpeg;base64,' + val.data;
        });
        // this.callUploadImage(base64Response, val);
      });
    }
  };

  render() {
    return (
      <View style={Styles.container}>
        <Modal
          animationType={'slide'}
          transparent={true}
          style={{backgroundColor: COLOR.BLACK}}
          visible={this.state.modalVisible}
          onRequestClose={() => {
            console.log('Modal has been closed.');
          }}>
          <View style={styles.modal}>
            <View style={styles.modalBody}>
              <NavigationBar
                prop={this.props}
                navHeight={40}
                name={'Add a New Place'}
              />
              <View style={{flex: 1}}>
                <KeyboardAwareScrollView bounces={false} showsVerticalScrollIndicator={false}>
                  <View style={[styles.borderView, {marginVertical: 20}]}>
                    {/* <FloatingTitleTextInputField
                      attrName="name"
                      title="Name"
                      value={this.state.name}
                      updateMasterState={this.updateMasterState}
        /> */}
                    <View style={{padding: 10, zIndex: 1000005}}>
                      <GooglePlacesTextInput
                        value={this.state.name}
                        onValueChange={(text) => this.setState({name: text})}
                      />
                    </View>
                    <View
                      style={{
                        borderBottomColor: COLOR.GRAY,
                        borderBottomWidth: 1,
                      }}
                    />
                    {/* <FloatingTitleTextInputField
                      attrName="address"
                      title="Address"
                      value={this.state.address}
                      updateMasterState={this.updateMasterState}
                    /> */}
                    <MapView
                      style={{
                        height: 60,
                        borderBottomLeftRadius: 10,
                        borderBottomRightRadius: 10,
                      }}
                    />
                  </View>

                  <View style={[styles.borderView, {marginBottom: 20}]}>
                    <ModalDatePicker
                      placeholder={'End Time'}
                      mode={'time'}
                      isVisible={true}
                      onChange={(val) => {
                        this.setState({time: val});
                      }}
                      value={this.state.time == '' ? null : this.state.time}
                    />
                    <this.SingleLineView />
                    <FloatingTitleTextInputField
                      attrName="duration"
                      title="Duration/Spending Time"
                      value={this.state.duration}
                      updateMasterState={this.updateMasterState}
                    />

                    <this.SingleLineView />

                    <View
                      style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'space-between',
                      }}>
                      <View style={{flex: 1}}>
                        <FloatingTitleTextInputField
                          attrName="note"
                          title="Note"
                          value={this.state.note}
                          updateMasterState={this.updateMasterState}
                        />
                      </View>
                      <Text
                        style={[
                          Styles.small_label,
                          {width: 70, color: COLOR.VOILET},
                        ]}>
                        Optional
                      </Text>
                    </View>
                    <this.SingleLineView />
                    <View
                      style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'space-between',
                      }}>
                      <View style={{flex: 1}}>
                        <FloatingTitleTextInputField
                          attrName="phoneNumber"
                          title="Phone Number"
                          value={this.state.phoneNumber}
                          updateMasterState={this.updateMasterState}
                        />
                      </View>
                      <Text
                        style={[
                          Styles.small_label,
                          {width: 70, color: COLOR.VOILET},
                        ]}>
                        Optional
                      </Text>
                    </View>

                    <this.SingleLineView />
                    <View
                      style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'space-between',
                      }}>
                      <View style={{flex: 1}}>
                        <FloatingTitleTextInputField
                          attrName="website"
                          title="Website"
                          value={this.state.website}
                          updateMasterState={this.updateMasterState}
                        />
                      </View>
                      <Text
                        style={[
                          Styles.small_label,
                          {width: 70, color: COLOR.VOILET},
                        ]}>
                        Optional
                      </Text>
                    </View>
                  </View>
                </KeyboardAwareScrollView>
                <View
                  style={{
                    marginVertical: 20,
                  }}>
                  <TouchableOpacity
                    onPress={() => {
                      this.setState({
                        photoModelVisible: true,
                        modalVisible: false,
                      });
                    }}
                    style={{
                      alignItems: 'center',
                      justifyContent: 'center',
                      height: 50,
                      borderRadius: 10,
                      borderColor: COLOR.GRAY,
                      borderWidth: 1,
                      width: wp(90),
                      backgroundColor: COLOR.WHITE,
                    }}>
                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        paddingLeft: 20,
                        paddingRight: 10,
                        alignItems: 'center',
                      }}>
                      <Image
                        source={IMAGES.second_camera}
                        resizeMode={'contain'}
                        style={{
                          width: 20,
                          height: 20,

                          marginRight: 10,
                        }}
                      />
                      <Text
                        style={[
                          Styles.button_font,
                          {color: COLOR.VOILET, flex: 1},
                        ]}>
                        Add Picture of the attraction
                      </Text>
                      <Image
                        source={IMAGES.right_arrow}
                        resizeMode={'contain'}
                        style={{
                          width: 15,
                          height: 15,
                          tintColor: COLOR.BLACK,
                          marginRight: 10,
                        }}
                      />
                    </View>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={{
                      alignItems: 'center',
                      justifyContent: 'center',
                      height: 50,
                      borderRadius: 10,
                      borderColor: COLOR.GRAY,
                      borderWidth: 1,
                      width: wp(90),
                      backgroundColor: COLOR.WHITE,
                      marginVertical: 10,
                    }}
                    onPress={() => {
                      this.setState({
                        photoModelVisible: true,
                        modalVisible: false,
                      });
                    }}>
                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        paddingLeft: 20,
                        paddingRight: 10,
                        alignItems: 'center',
                      }}>
                      <Image
                        resizeMode={'contain'}
                        style={{
                          width: 20,
                          height: 20,
                          tintColor: COLOR.BLACK,
                          marginRight: 10,
                        }}
                      />
                      <Text
                        style={[
                          Styles.button_font,
                          {color: COLOR.VOILET, flex: 1},
                        ]}>
                        Add a reservation ticket
                      </Text>
                      <Image
                        source={IMAGES.right_arrow}
                        resizeMode={'contain'}
                        style={{
                          width: 15,
                          height: 15,
                          tintColor: COLOR.BLACK,
                          marginRight: 10,
                        }}
                      />
                    </View>
                  </TouchableOpacity>

                  <TouchableOpacity
                    onPress={() => {
                      // if (this.state.name == '') {
                      //   SimpleToast.show('Enter name');
                      // } else
                      if (this.state.address == '') {
                        SimpleToast.show('Enter name');
                      } else if (this.state.time == '') {
                        SimpleToast.show('Enter time');
                      } else if (this.state.duration == '') {
                        SimpleToast.show('Enter duration');
                      } else {
                        this.setState({modalVisible: false}, (val) => {
                          this.props.navigation.replace('BookTicket', {
                            updateMasterState: this.updateMasterState,
                            tripId: this.state.id,
                          });
                        });
                      }
                    }}
                    style={{
                      alignItems: 'center',
                      justifyContent: 'center',
                      height: 50,
                      borderRadius: 10,
                      borderColor: COLOR.GRAY,
                      borderWidth: 1,
                      width: wp(90),
                      backgroundColor: COLOR.GREEN,
                    }}>
                    <Text style={[Styles.button_font, {color: COLOR.WHITE}]}>
                      Add
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </View>
        </Modal>
        <ActionSheet
          modalVisible={this.state.photoModelVisible}
          handleSheet={this.handleSheet}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  modal: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
  },
  modalBody: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    backgroundColor: COLOR.WHITE,
    borderWidth: 1,
    width: wp(100),
    borderColor: COLOR.BORDER,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    flex: 1,
    marginTop: 50,
  },
  image: {
    height: 120,
    width: 120,
    alignSelf: 'center',
    marginVertical: 20,
  },
  headingLabel: {
    fontFamily: FONTS.FAMILY_BOLD,
    fontSize: FONTS.MEDIUM,
    color: COLOR.BLACK,
  },
  singleLineView: {
    marginVertical: 20,
    borderBottomColor: COLOR.GRAY,
    width: wp(100),
    alignSelf: 'center',
    borderBottomWidth: 1,
  },
  borderView: {
    borderRadius: 10,
    borderWidth: 1,
    width: wp(90),
    borderColor: COLOR.GRAY,
    alignSelf: 'center',
  },
});
