import React from 'react';
import {Modal, Text, View, StyleSheet, Image, Alert} from 'react-native';
import {TextInput, TouchableOpacity} from 'react-native';
import {
  heightPercentageToDP,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import {connect} from 'react-redux';
import COLOR from '../../../styles/Color';
import FONTS from '../../../styles/Fonts';
import Styles from '../../../styles/Styles';
import TripAction from '../../../../redux/action/TripAction';
import NavigationBar from '../../../commonView/NavigationBar';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview';
import Toast from 'react-native-simple-toast';

class PolicyPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: true,
      title:this.props.route.params.screenTitle
    };
  }

  render() {
    return (
      <View style={Styles.container}>
        <Modal
          animationType={'slide'}
          transparent={true}
          style={{backgroundColor: COLOR.BLACK}}
          visible={this.state.modalVisible}
          onRequestClose={() => {
            console.log('Modal has been closed.');
          }}>
          <KeyboardAwareScrollView style={{flex: 1}}>
            <View style={styles.modal}>
              <View style={styles.modalBody}>
                <NavigationBar
                  prop={this.props}
                  navHeight={40}
                  name={this.state.title}
                />
                <View
                  style={{
                    width: wp(90),
                    alignSelf: 'center',
                    marginTop: 20,
                    flex:1
                  }}>
                  <Text style={{color:COLOR.BLACK}}>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sem
                    sapien, elit sapien iaculis. Sollicitudin tortor maecenas
                    morbi pretium laoreet massa pharetra cursus. Porta at sit
                    lectus eget tristique. Venenatis augue massa mattis cursus
                    arcu vitae. Hendrerit blandit sapien libero, dui velit
                    pellentesque sem. Purus mauris imperdiet convallis
                    condimentum commodo velit, dui. Consectetur consequat sem
                    ultrices tortor mauris pulvinar. Semper eu vestibulum purus
                    massa ullamcorper diam. Sapien id amet cursus vel. Amet
                    pulvinar ut fermentum facilisis odio erat dolor id netus.
                    Duis pulvinar nisi, blandit suspendisse libero commodo
                    pharetra. Arcu, quis metus nullam adipiscing volutpat.
                    Habitant lorem consequat aliquam ac dolor. Lorem nisi, nisl
                    vestibulum adipiscing velit, tincidunt nisl imperdiet
                    eleifend. Vel ut sed diam ac. Feugiat nibh ipsum enim mauris
                    facilisis. Faucibus accumsan ut ac turpis ut diam pretium
                    dui, amet. Habitant ipsum lorem faucibus ut blandit sodales
                    arcu lorem sed. Suspendisse. Nunc quam mauris id hac donec
                    eget erat. Diam duis feugiat lacus, aliquet orci iaculis
                    nisi, sodales. Massa sed id maecenas nunc. Sed ut
                    suspendisse.
                  </Text>
                </View>
              </View>
            </View>
          </KeyboardAwareScrollView>
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  modal: {
    flex: 1,
    height: heightPercentageToDP(100),
    alignItems: 'center',
    justifyContent: 'flex-end',
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
  },
  modalBody: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    backgroundColor: COLOR.WHITE,
    borderWidth: 1,
    width: wp(100),
    borderColor: COLOR.BORDER,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    marginTop:50,
    flex:1
  },
  image: {
    height: 120,
    width: 120,
    alignSelf: 'center',
    marginVertical: 20,
  },
  headingLabel: {
    fontFamily: FONTS.FAMILY_BOLD,
    fontSize: FONTS.MEDIUM,
    color: COLOR.BLACK,
  },
  singleLineView: {
    marginVertical: 20,
    borderBottomColor: COLOR.GRAY,
    width: wp(100),
    alignSelf: 'center',
    borderBottomWidth: 1,
  },
});

mapDispatchToProps = (dispatch) => {
  return {
    requestTrip: (data) => {
      dispatch(TripAction.requestTrip(data));
    },
  };
};

export default connect(null, mapDispatchToProps)(PolicyPage);
