import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Image } from 'react-native';
import { SafeAreaView } from 'react-native';

import { FlatList, ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scrollview';
import { DAY } from '../../../../helper/Constants';
import COLOR from '../../../styles/Color';
import FONTS from '../../../styles/Fonts';
import Styles from '../../../styles/Styles';

export default class extends React.Component {
    render() {
        return (

            <SafeAreaView style={styles.container}>
                <ScrollView style={styles.container}>
                    <View style={styles.toolbar}>
                        <Text style={{ fontFamily: FONTS.FAMILY_REGULAR, alignSelf: 'center', marginHorizontal: 10 }}>Monday, 06 Feb 2021</Text>
                        <View style={{ justifyContent: 'center', flexDirection: 'row' }}>
                            <Image
                                source={require('../../../styles/assets/weather.png')}
                                style={{
                                    width: 20,
                                    height: 20,
                                    alignSelf: 'center'
                                }}
                                resizeMode={'contain'}
                            />
                            <Text style={{ fontFamily: FONTS.FAMILY_REGULAR, alignSelf: 'center', marginHorizontal: 10 }}>26 - 38°</Text>
                            <View
                                style={{
                                    width: 40,
                                    height: 40,
                                    backgroundColor: COLOR.BORDER,
                                    justifyContent: 'center'
                                }}
                            >
                                <Image
                                    source={require('../../../styles/assets/weather.png')}
                                    style={{
                                        width: 20,
                                        height: 20,
                                        alignSelf: 'center'
                                    }}
                                    resizeMode={'contain'}
                                />
                            </View>
                        </View>
                    </View>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginBottom: 15 }}>
                        <FlatList
                            data={DAY}
                            horizontal
                            style={{ marginHorizontal: 5 }}
                            renderItem={({ item }) => (
                                <TouchableOpacity
                                    style={styles.itemStyle}
                                >
                                    <Text style={styles.commomText}>{item.day}</Text>
                                    <Text style={styles.commomText}>{item.date}</Text>
                                </TouchableOpacity>
                            )} />
                        <View style={{
                            width: 30,
                            height: 30,
                            alignSelf: 'center',
                            backgroundColor: COLOR.GREEN,
                            borderTopLeftRadius: 15,
                            borderBottomLeftRadius: 15,
                            justifyContent: 'center'
                        }}>
                            <Image
                                source={require('../../../styles/assets/plus.png')}
                                style={{
                                    width: 15,
                                    height: 15,
                                    alignSelf: 'center'
                                }}
                                resizeMode={'contain'}
                            />
                        </View>
                    </View>
                    <View style={{ backgroundColor: '#EBEFF2', paddingBottom: 20 }}>
                        <Text style={styles.date}>Thursday, 29 Mar 2021</Text>
                        <View style={{ flexDirection: 'row' }}>
                            <View style={{ width: 50, marginHorizontal: 20, justifyContent: 'center' }}>
                                <Text style={styles.timeText}>09:00</Text>
                                <Text style={styles.gmt}>GMT+3</Text>
                                <View style={styles.smallDot} />
                                <View style={styles.line} />

                            </View>
                            <View style={styles.card}>
                                <View style={styles.row}>
                                    <Image
                                        source={require('../../../styles/assets/plane2.png')}
                                        style={{
                                            width: 40,
                                            height: 40,
                                            alignSelf: 'center'
                                        }}
                                        resizeMode={'contain'}
                                    />
                                    <Image
                                        source={require('../../../styles/assets/menuHorizontal.png')}
                                        style={{
                                            width: 20,
                                            height: 5,
                                            marginHorizontal: 5,
                                            alignSelf: 'center'
                                        }}
                                        resizeMode={'contain'}
                                    />
                                </View>
                                <View style={styles.row}>
                                    <View>
                                        <Text style={styles.textLarge} >DMM</Text>
                                        <Text style={styles.gmt}>02:55 pm</Text>
                                    </View>
                                    <View>
                                        <Text style={styles.textLarge} >DPS</Text>
                                        <Text style={styles.gmt}>12:55 pm</Text>
                                    </View>
                                </View>
                                <View style={[styles.row, { marginVertical: 10 }]}>
                                    <View
                                        style={styles.dot}
                                    />
                                    <Image
                                        source={require('../../../styles/assets/plane_horizontal.png')}
                                        style={{
                                            width: 20,
                                            height: 20,
                                            marginHorizontal: 5,
                                            alignSelf: 'center'
                                        }}
                                        resizeMode={'contain'}
                                    />
                                    <View
                                        style={styles.dot}
                                    />
                                </View>
                                <View style={Styles.line_view} />
                                <TouchableOpacity style={{ marginHorizontal: 10, marginTop: 10, alignSelf: 'flex-end' }}>
                                    <Text style={{ alignSelf: 'center', color: '#05778F' }}>Details {'>'} </Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={{
                            width: 50,
                            height: 50,
                            borderRadius: 25,
                            backgroundColor: '#465A64',
                            justifyContent: 'center',
                            marginHorizontal: 20,
                        }}>
                            <Image
                                source={require('../../../styles/assets/car.png')}
                                style={{
                                    width: 20,
                                    height: 20,
                                    alignSelf: 'center',
                                    tintColor: COLOR.WHITE,
                                }}
                                resizeMode={'contain'}
                            />
                        </View>
                        <View style={{ flexDirection: 'row' }}>
                            <View style={{ width: 50, marginHorizontal: 20, justifyContent: 'center' }}>
                                <Text style={styles.timeText}>09:00</Text>
                                <Text style={styles.gmt}>GMT+3</Text>
                                <View style={styles.smallDot} />
                                <View style={styles.line} />

                            </View>
                            <View style={styles.card}>
                                <View style={styles.row}>
                                    <Text style={styles.timeText}>Gemitir Garden</Text>
                                    <Image
                                        source={require('../../../styles/assets/menuHorizontal.png')}
                                        style={{
                                            width: 20,
                                            height: 5,
                                            marginHorizontal: 5,
                                            alignSelf: 'center'
                                        }}
                                        resizeMode={'contain'}
                                    />
                                </View>
                                <View style={[styles.row, { justifyContent: 'flex-start', marginTop: 15, marginBottom: 5 }]}>
                                    <Image
                                        source={require('../../../styles/assets/pin.png')}
                                        style={{
                                            width: 20,
                                            height: 20,
                                            marginHorizontal: 5,

                                        }}
                                        resizeMode={'contain'}
                                    />
                                    <Text style={styles.gmt}>Marigold, Badung Regency, Bali 80353, Indonesia</Text>
                                </View>
                                <View style={[styles.row, { justifyContent: 'flex-start', marginVertical: 5 }]}>
                                    <Image
                                        source={require('../../../styles/assets/time.png')}
                                        style={{
                                            width: 20,
                                            height: 20,
                                            marginHorizontal: 5,

                                        }}
                                        resizeMode={'contain'}
                                    />
                                    <Text style={styles.gmt}>30 min (10:15 GMT+7)</Text>
                                </View>
                                <View style={[styles.row, { marginVertical: 10, backgroundColor: COLOR.BORDER, height: 30, borderRadius: 15, paddingHorizontal: 15 }]}>
                                    <Image
                                        source={require('../../../styles/assets/call.png')}
                                        style={{
                                            width: 20,
                                            height: 20,
                                            marginHorizontal: 5,
                                            alignSelf: 'center'
                                        }}
                                        resizeMode={'contain'}
                                    />
                                    <Image
                                        source={require('../../../styles/assets/web.png')}
                                        style={{
                                            width: 25,
                                            height: 25,
                                            marginHorizontal: 5,
                                            alignSelf: 'center'
                                        }}
                                        resizeMode={'contain'}
                                    />
                                    <Image
                                        source={require('../../../styles/assets/info.png')}
                                        style={{
                                            width: 20,
                                            height: 20,
                                            marginHorizontal: 5,
                                            alignSelf: 'center'
                                        }}
                                        resizeMode={'contain'}
                                    />
                                    <Image
                                        source={require('../../../styles/assets/ticket.png')}
                                        style={{
                                            width: 20,
                                            height: 20,
                                            marginHorizontal: 5,
                                            alignSelf: 'center'
                                        }}
                                        resizeMode={'contain'}
                                    />
                                </View>
                            </View>
                        </View>
                        <View style={{
                            width: 50,
                            height: 50,
                            borderRadius: 25,
                            backgroundColor: '#465A64',
                            justifyContent: 'center',
                            marginHorizontal: 20,
                        }}>
                            <Image
                                source={require('../../../styles/assets/walk.png')}
                                style={{
                                    width: 20,
                                    height: 20,
                                    alignSelf: 'center',
                                    tintColor: COLOR.WHITE,
                                }}
                                resizeMode={'contain'}
                            />
                        </View>
                        <View style={{ flexDirection: 'row' }}>
                            <View style={{ width: 50, marginHorizontal: 20, justifyContent: 'center' }}>
                                <Text style={styles.timeText}>09:00</Text>
                                <Text style={styles.gmt}>GMT+3</Text>
                                <View style={styles.smallDot} />
                                <View style={styles.line} />

                            </View>
                            <View style={styles.card}>
                                <View style={styles.row}>
                                    <Text style={styles.timeText}>Gemitir Garden</Text>
                                    <Image
                                        source={require('../../../styles/assets/menuHorizontal.png')}
                                        style={{
                                            width: 20,
                                            height: 5,
                                            marginHorizontal: 5,
                                            alignSelf: 'center'
                                        }}
                                        resizeMode={'contain'}
                                    />
                                </View>
                                <View style={[styles.row, { justifyContent: 'flex-start', marginTop: 15, marginBottom: 5 }]}>
                                    <Image
                                        source={require('../../../styles/assets/pin.png')}
                                        style={{
                                            width: 20,
                                            height: 20,
                                            marginHorizontal: 5,

                                        }}
                                        resizeMode={'contain'}
                                    />
                                    <Text style={styles.gmt}>Marigold, Badung Regency, Bali 80353, Indonesia</Text>
                                </View>
                                <View style={[styles.row, { justifyContent: 'flex-start', marginVertical: 5 }]}>
                                    <Image
                                        source={require('../../../styles/assets/time.png')}
                                        style={{
                                            width: 20,
                                            height: 20,
                                            marginHorizontal: 5,

                                        }}
                                        resizeMode={'contain'}
                                    />
                                    <Text style={styles.gmt}>30 min (10:15 GMT+7)</Text>
                                </View>
                                <View style={[styles.row, { marginVertical: 10, backgroundColor: COLOR.BORDER, height: 30, borderRadius: 15, paddingHorizontal: 15 }]}>
                                    <Image
                                        source={require('../../../styles/assets/call.png')}
                                        style={{
                                            width: 20,
                                            height: 20,
                                            marginHorizontal: 5,
                                            alignSelf: 'center'
                                        }}
                                        resizeMode={'contain'}
                                    />
                                    <Image
                                        source={require('../../../styles/assets/web.png')}
                                        style={{
                                            width: 25,
                                            height: 25,
                                            marginHorizontal: 5,
                                            alignSelf: 'center'
                                        }}
                                        resizeMode={'contain'}
                                    />
                                    <Image
                                        source={require('../../../styles/assets/info.png')}
                                        style={{
                                            width: 20,
                                            height: 20,
                                            marginHorizontal: 5,
                                            alignSelf: 'center'
                                        }}
                                        resizeMode={'contain'}
                                    />
                                    <Image
                                        source={require('../../../styles/assets/ticket.png')}
                                        style={{
                                            width: 20,
                                            height: 20,
                                            marginHorizontal: 5,
                                            alignSelf: 'center'
                                        }}
                                        resizeMode={'contain'}
                                    />
                                </View>
                            </View>
                        </View>

                    </View>
                </ScrollView>
            </SafeAreaView >

        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    toolbar: {
        marginHorizontal: 10,
        justifyContent: 'space-between',
        flexDirection: 'row',
        borderWidth: 0.5,
        borderRadius: 5,
        height: 40,
        borderColor: COLOR.BORDER,
        marginVertical: 15,

    },
    itemStyle: {
        marginHorizontal: 3,
        borderColor: COLOR.BORDER,
        borderWidth: 1,
        width: 50,
        height: 60,
        borderRadius: 5,
        justifyContent: 'space-evenly'
    },
    commomText: {
        fontFamily: FONTS.FAMILY_REGULAR,
        alignSelf: 'center',
    },
    date: {
        fontSize: FONTS.FAMILY_BOLD,
        fontSize: 18,
        marginHorizontal: 20,
        marginVertical: 20,
    },
    textLarge: {
        fontSize: FONTS.FAMILY_BOLD,
        fontSize: 20,
        marginVertical: 5,
    },
    timeText: {
        alignSelf: 'center',
        fontFamily: FONTS.FAMILY_BOLD,
        fontSize: 16,
    },
    gmt: {
        alignSelf: 'center',
        fontFamily: FONTS.FAMILY_REGULAR,
        fontSize: 12,
        color: COLOR.TEXT_COLOR
    },
    line: {
        width: 1,
        flex: 1,
        backgroundColor: COLOR.BLACK,
        alignSelf: 'center'
    },
    card: {
        flex: 1,
        marginHorizontal: 10,
        padding: 10,
        backgroundColor: COLOR.WHITE,
        borderRadius: 10,
    },
    row: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    dot: {
        width: 10,
        height: 10,
        borderRadius: 5,
        backgroundColor: COLOR.BORDER,
        borderWidth: 1,
        borderColor: COLOR.BLACK,
        alignSelf: 'center'
    },
    smallDot: {
        width: 10,
        height: 10,
        borderRadius: 5,
        alignSelf: 'center',
        backgroundColor: COLOR.BLACK
    }

});