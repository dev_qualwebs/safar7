import React, { useEffect, useState } from 'react';
import {Modal, Text, View, StyleSheet, Image, Alert} from 'react-native';
import {TextInput, TouchableOpacity} from 'react-native';
import {
  heightPercentageToDP,
  widthPercentageToDP,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import {connect} from 'react-redux';
import COLOR from '../../../styles/Color';
import FONTS from '../../../styles/Fonts';
import Styles from '../../../styles/Styles';
import IMAGES from '../../../styles/Images';
import TripAction from '../../../../redux/action/TripAction';
import NavigationBar from '../../../commonView/NavigationBar';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview';
import PDFView from 'react-native-view-pdf';

const resources = {
    file: Platform.OS === 'ios' ? 'downloadedDocument.pdf' : '/sdcard/Download/downloadedDocument.pdf',
    url: 'http://www.africau.edu/images/default/sample.pdf',
    base64: 'JVBERi0xLjMKJcfs...',
  };
  
  const resourceType = 'url';

export const TicketPdfView = props => {
const [modalVisible,setModalVisible] = useState(false);
    useEffect(() => {
        setModalVisible(props.modalVisible)
      }, [props.modalVisible]);
    return (
      <View style={Styles.container}>
        <Modal
          animationType={'slide'}
          transparent={true}
          style={{backgroundColor: COLOR.BLACK}}
          visible={modalVisible}
          onRequestClose={() => {
            console.log('Modal has been closed.');
          }}>
            <View style={styles.modal}>
              <View style={styles.modalBody}>
              <View style={{position:'absolute',top:10,right:10}}>
            <TouchableOpacity
            onPress={props.setVisibility}
            >
                <Image style={{height:30,width:30,tintColor:COLOR.BLACK}} source={IMAGES.close} />
            </TouchableOpacity>
            </View>
                <View style={{height: heightPercentageToDP(80),backgroundColor:COLOR.WHITE}}>
                  <PDFView
                    fadeInDuration={250.0}
                    style={{flex: 1, backgroundColor: COLOR.BLACK,height:heightPercentageToDP(80),width:widthPercentageToDP(90)}}
                    resource={resources[resourceType]}
                    resourceType={resourceType}
                    onLoad={() =>
                      console.log(`PDF rendered from ${resourceType}`)
                    }
                    onError={(error) => console.log('Cannot render PDF', error)}
                  />
                </View>
              </View>
            </View>
        </Modal>
      </View>
    );
//   }
}

const styles = StyleSheet.create({
  modal: {
    flex: 1,
    height: heightPercentageToDP(100),
    alignItems: 'center',
    justifyContent: 'flex-end',
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
  },
  modalBody: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    backgroundColor: COLOR.WHITE,
    borderWidth: 1,
    width: wp(100),
    borderColor: COLOR.BORDER,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    marginTop: 50,
    flex: 1,
  },
  image: {
    height: 120,
    width: 120,
    alignSelf: 'center',
    marginVertical: 20,
  },
  headingLabel: {
    fontFamily: FONTS.FAMILY_BOLD,
    fontSize: FONTS.MEDIUM,
    color: COLOR.BLACK,
  },
  singleLineView: {
    marginVertical: 20,
    borderBottomColor: COLOR.GRAY,
    width: wp(100),
    alignSelf: 'center',
    borderBottomWidth: 1,
  },
});

mapDispatchToProps = (dispatch) => {
  return {
    requestTrip: (data) => {
      dispatch(TripAction.requestTrip(data));
    },
  };
};

export default connect(null, mapDispatchToProps)(TicketPdfView);
