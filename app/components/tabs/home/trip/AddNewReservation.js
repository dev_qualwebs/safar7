import React from 'react';
import {
  Modal,
  Text,
  View,
  StyleSheet,
  Image,
  Alert,
  TouchableOpacity,
  TouchableWithoutFeedback,
  FlatList,
  TextInput,
} from 'react-native';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import COLOR from '../../../../components/styles/Color';
import FONTS from '../../../../components/styles/Fonts';
import IMAGES from '../../../../components/styles/Images';
import Styles from '../../../../components/styles/Styles';
import NavigationBar from '../../../commonView/NavigationBar';

export default class AddNewReservation extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: true,
      selectTransport: [
        {
          id: 1,
          name: 'Flight',
          image: IMAGES.plane,
        },
        {
          id: 2,
          name: 'Stay',
          image: IMAGES.bed,
        },
        {
          id: 3,
          name: 'Car',
          image: IMAGES.car_image,
        },
        {
          id: 4,
          name: 'Cruise',
          image: IMAGES.cruise,
        },
        {
          id: 5,
          name: 'Train',
          image: IMAGES.train,
        },
        {
          id: 6,
          name: 'Transfer',
          image: IMAGES.train,
        },
        {
          id: 7,
          name: 'Others',
          image: IMAGES.token,
        },
      ],
      selectedIndex: null,
    };
    this.selectTransport = this.selectTransport.bind(this);
    this.handleNavigation = this.handleNavigation.bind(this);
    this.tripId = this.props.route.params.tripId;
  }

  selectTransport = (item) => {
    console.log('worling');
    this.setState({selectedIndex: item.id});
  };

  handleNavigation = () => {
    if (this.state.selectedIndex) {
      this.setState({modalVisible: false}, (val) => {
        switch (this.state.selectedIndex) {
          case 1:
            this.props.navigation.replace('BookTicket', {
              title: 'Flight',
              id: 1,
              tripId: this.tripId,
            });
            break;
          case 2:
            this.props.navigation.replace('BookTicket', {
              title: 'Stay',
              id: 2,
              tripId: this.tripId,
            });
            break;
          case 3:
            this.props.navigation.replace('BookTicket', {
              title: 'Car',
              id: 3,
              tripId: this.tripId,
            });
            break;
          case 4:
            this.props.navigation.replace('BookTicket', {
              title: 'Cruise',
              id: 4,
              tripId: this.tripId,
            });
            break;
          case 5:
            this.props.navigation.replace('BookTicket', {
              title: 'Train',
              id: 5,
              tripId: this.tripId,
            });
            break;
          case 6:
            this.props.navigation.replace('BookTicket', {
              title: 'Transfer',
              id: 6,
              tripId: this.tripId,
            });
            break;
          case 7:
            this.props.navigation.replace('BookTicket', {
              title: 'Other',
              id: 7,
              tripId: this.tripId,
            });
            break;

          default:
            break;
        }
      });
    } else {
      Alert.alert('Select an Option');
    }
  };

  render() {
    const {selectedIndex, modalVisible} = this.state;
    return (
      <View style={Styles.container}>
        <Modal
          animationType={'slide'}
          transparent={true}
          style={{backgroundColor: COLOR.BLACK}}
          visible={modalVisible}
          onRequestClose={() => {
            console.log('Modal has been closed.');
          }}>
          <View style={styles.modal}>
            <View style={styles.modalBody}>
              <NavigationBar
                prop={this.props}
                navHeight={40}
                name={'Add a New Reservation'}
              />
              <FlatList
                style={{backgroundColor: COLOR.WHITE}}
                data={this.state.selectTransport}
                renderItem={({item}) => (
                  <TouchableOpacity
                    onPress={() => {
                      console.log('called');
                      this.selectTransport(item);
                    }}>
                    <View
                      style={{
                        flex: 1,
                        borderRadius: 10,
                        borderWidth: 1,
                        height: 120,
                        margin: 5,
                        borderColor:
                          selectedIndex == item.id ? COLOR.GREEN : COLOR.GRAY,
                        backgroundColor:
                          selectedIndex == item.id ? '#05778F10' : COLOR.WHITE,
                        alignSelf: 'center',
                        alignItems: 'center',
                        justifyContent: 'center',
                        width: wp(43.5),
                      }}>
                      <Image
                        source={item.image}
                        style={{
                          height: 50,
                          width: 50,
                          marginVertical: 10,
                          tintColor:
                            selectedIndex == item.id
                              ? COLOR.GREEN
                              : COLOR.VOILET,
                        }}
                      />
                      <Text style={{marginBottom: 10, textAlign: 'center'}}>
                        {item.name}
                      </Text>
                    </View>
                  </TouchableOpacity>
                )}
                horizontal={false}
                numColumns={2}
                showsVerticalScrollIndicator={false}
                showsHorizontalScrollIndicator={false}
                keyExtractor={(item, index) => index.toString()}
              />
              <TouchableOpacity
                onPress={() => {
                  this.handleNavigation();
                }}
                style={{
                  alignItems: 'center',
                  justifyContent: 'center',
                  height: 50,
                  borderRadius: 10,

                  width: wp(90),
                  marginVertical: 20,
                  backgroundColor: COLOR.GREEN,
                }}>
                <Text style={[Styles.button_font, {color: COLOR.WHITE}]}>
                  Next
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  modal: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
  },
  modalBody: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    backgroundColor: COLOR.WHITE,
    borderWidth: 1,
    width: wp(100),
    borderColor: COLOR.BORDER,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    flex: 1,
    marginTop: 50,
  },
  image: {
    height: 120,
    width: 120,
    alignSelf: 'center',
    marginVertical: 20,
  },
  headingLabel: {
    fontFamily: FONTS.FAMILY_BOLD,
    fontSize: FONTS.MEDIUM,
    color: COLOR.BLACK,
  },
  singleLineView: {
    marginVertical: 20,
    borderBottomColor: COLOR.GRAY,
    width: wp(100),
    alignSelf: 'center',
    borderBottomWidth: 1,
  },
  offer_view: {
    marginVertical: 0,
    width: wp(100),
    marginBottom: 20,
    height: 100,
    marginHorizontal: 10,
  },
  flatlist_view: {
    width: wp(90),
    marginVertical: 10,
    alignSelf: 'center',
  },
});
