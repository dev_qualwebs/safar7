import React from 'react';
import {Modal, Text, View, StyleSheet, Image} from 'react-native';
import {ScrollView, TextInput, TouchableOpacity} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import COLOR from '../../../../components/styles/Color';
import FONTS from '../../../../components/styles/Fonts';
import IMAGES from '../../../../components/styles/Images';
import Styles from '../../../../components/styles/Styles';
import NavigationBar from '../../../commonView/NavigationBar';

export default class Checklist extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: true,
    };
  }

  optionView = (props) => (
    <View
      style={{
        marginVertical: 5,
        height: 50,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: 10,
      }}>
      <Image
        style={{height: 30, width: 30}}
        source={props.image}
        resizeMode={'contain'}
      />
      <Text
        style={[
          Styles.body_label,
          {width: null, textAlign: 'left', flex: 1, marginLeft: 10},
        ]}>
        {props.name}
      </Text>
      <Image style={{height: 12, width: 12}} source={IMAGES.right_arrow} />
    </View>
  );

  SingleLineView = () => (
    <View
      style={{
        borderBottomColor: COLOR.GRAY,
        marginVertical: 5,
        borderBottomWidth: 2,
      }}
    />
  );

  render() {
    return (
      <View style={Styles.container}>
        <Modal
          animationType={'slide'}
          transparent={true}
          style={{backgroundColor: COLOR.BLACK}}
          visible={this.state.modalVisible}
          onRequestClose={() => {
            console.log('Modal has been closed.');
          }}>
          <View style={styles.modal}>
            <View style={styles.modalBody}>
              <NavigationBar
                prop={this.props}
                navHeight={40}
                name={'Checklist'}
              />
              <View style={{flex: 1, backgroundColor: COLOR.BACKGROUND}}>
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    alignSelf: 'flex-end',
                    height: 40,
                    right: 10,
                    position: 'absolute',
                  }}>
                  <View style={{flex: 1}} />
                  <Image
                    style={{height: 25, width: 25, marginRight: 5}}
                    source={IMAGES.add_plus}
                    resizeMode={'contain'}
                  />
                  <Text
                    style={[
                      Styles.body_label,
                      {fontFamily: FONTS.FAMILY_BOLD},
                    ]}>
                    Add Group
                  </Text>
                </View>
              </View>
              <View style={{marginVertical: 20}}>
                <TouchableOpacity
                  onPress={() => {
                    this.setState({modalVisible: false}, (val) => {
                      this.props.navigation.navigate('HomeStack');
                    });
                  }}
                  style={{
                    alignItems: 'center',
                    justifyContent: 'center',
                    height: 50,
                    borderRadius: 10,

                    width: wp(90),
                    backgroundColor: COLOR.GREEN,
                  }}>
                  <Text style={[Styles.button_font, {color: COLOR.WHITE}]}>
                    Save
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  modal: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
  },
  modalBody: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    backgroundColor: COLOR.WHITE,
    borderWidth: 1,
    width: wp(100),
    marginTop: 50,
    borderColor: COLOR.BORDER,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    flex: 1,
  },
  image: {
    height: 120,
    width: 120,
    alignSelf: 'center',
    marginVertical: 20,
  },
  headingLabel: {
    fontFamily: FONTS.FAMILY_BOLD,
    fontSize: FONTS.MEDIUM,
    color: COLOR.BLACK,
  },
  singleLineView: {
    marginVertical: 20,
    borderBottomColor: COLOR.GRAY,
    width: wp(100),
    alignSelf: 'center',
    borderBottomWidth: 1,
  },
  borderView: {
    borderRadius: 10,
    borderWidth: 2,
    width: wp(90),
    borderColor: COLOR.GRAY,
    alignSelf: 'center',
  },
});
