import React from 'react';
import { View, Image, StyleSheet, Text } from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import DatePicker from '../../../commonView/DatePicker';
import COLOR from '../../../styles/Color';
import IMAGES from '../../../styles/Images';
import Styles from '../../../styles/Styles';

export default class SingleTrip extends React.Component {
  constructor(props) {
    super(props);
  }

  selected = (date) => { };

  render() {
    return (
      <View style={Styles.container}>
        <View style={style.topBar}>
          <Text style={[Styles.body_label, { marginLeft: 10, width: null }]}>
            Monday 06, Feb 2021
          </Text>
          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <Image style={{ width: 25, height: 25 }} source={IMAGES.back_arrow} />
            <Text style={[Styles.body_label, { marginLeft: 10, width: null }]}>
              28-38C
            </Text>
            <Image style={{ width: 25, height: 25 }} source={IMAGES.back_arrow} />
          </View>
        </View>
        <View
          style={{
            height: 80,
            width: wp(100),
            marginLeft: 10,
            flexDirection: 'row',
          }}>
          <DatePicker
            pressedColor={COLOR.WHITE}
            depressedColor={COLOR.VOILET}
          />
          <View
            style={{
              width: wp(20),
              height: 40,
              backgroundColor: COLOR.GREEN,
              alignSelf: 'center',
              justifyContent: 'center',
              borderRadius: 20,
              marginRight: -30,
              marginLeft: 10,
            }}>
            <Image
              style={{
                left: 10,
                position: 'absolute',
                height: 20,
                width: 20,
                tintColor: COLOR.WHITE,
              }}
              source={IMAGES.plus_image}
            />
          </View>
        </View>
      </View>
    );
  }
}

const style = StyleSheet.create({
  topBar: {
    height: 50,
    shadowColor: COLOR.VOILET,
    shadowOpacity: 0.2,
    backgroundColor: COLOR.WHITE,
    flexDirection: 'row',
    elevation: 3,
    shadowOffset: { width: 2, height: 2 },
    justifyContent: 'space-between',
    width: wp(95),
    alignSelf: 'center',
    marginVertical: 20,

    alignItems: 'center',
  },
});
