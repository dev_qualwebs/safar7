import React from 'react';
import { Modal, Text, View, StyleSheet, Image, Button } from 'react-native';
import {

  TouchableOpacity,

} from 'react-native';
import {
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import COLOR from '../../../../components/styles/Color';
import FONTS from '../../../../components/styles/Fonts';
import IMAGES from '../../../../components/styles/Images';
import Styles from '../../../../components/styles/Styles';
import NavigationBar from '../../../commonView/NavigationBar';
import { SafeAreaView } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';

export default class TripAddon extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: true,
    };
  }

  commonView = (props) => {
    return (
      <View
        style={{
          flexDirection: 'row',
          width: wp(90),
          alignSelf: 'center',
          justifyContent: 'space-between',
          alignItems: 'center',
          backgroundColor: COLOR.WHITE,
          marginBottom: 20,
          padding: 15,
          shadowColor: COLOR.VOILET,
          shadowOffset: { width: 0.2, height: 0.2 },
          shadowOpacity: 0.2,
          elevation: 3,
          borderWidth: 0.5,
          borderColor: COLOR.GRAY,
          borderRadius: 10,
        }}>
        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
          <Image
            style={{
              width: 25,
              height: 25,
              marginRight: 10,
              tintColor: '#465A64',
            }}
            source={props.image}
            resizeMode={'contain'}
          />
          <Text
            style={[Styles.body_label, { width: null, fontSize: FONTS.MEDIUM }]}>
            {props.name}
          </Text>
        </View>
        <TouchableOpacity>
          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <Text
              style={[
                Styles.body_label,
                {
                  width: null,
                  color: COLOR.GREEN,
                  fontFamily: FONTS.FAMILY_SEMIBOLD,
                  marginRight: 5,
                },
              ]}>
              Details
            </Text>
            <Image
              style={{
                width: 10,
                height: 10,

                tintColor: COLOR.GREEN,
              }}
              source={IMAGES.right_arrow}
              resizeMode={'contain'}
            />
          </View>
        </TouchableOpacity>
      </View>
    );
  };

  render() {
    return (
      <View style={Styles.container}>
        <Modal
          animationType={'slide'}
          transparent={true}
          style={{ backgroundColor: COLOR.BLACK }}
          visible={this.state.modalVisible}
          onRequestClose={() => {
            console.log('Modal has been closed.');
          }}>
          <View style={styles.modal}>
            <View style={styles.modalBody}>
              <NavigationBar prop={this.props} navHeight={40} name={'Addons'} />
              <SafeAreaView style={[Styles.container]}>
                <ScrollView style={[Styles.container, { paddingVertical: 10 }]}>
                  <this.commonView
                    image={IMAGES.shield}
                    name={'Travel Insurance'}
                  />
                  <this.commonView
                    image={IMAGES.world_globe}
                    name={'Travel Visa'}
                  />
                  <this.commonView
                    image={IMAGES.assistance}
                    name={'Travel Assistance'}
                  />
                  <this.commonView
                    image={IMAGES.sim_card}
                    name={'Data Roaming Simcard'}
                  />
                  <this.commonView
                    image={IMAGES.sim_card}
                    name={'Router for Data Roaming'}
                  />
                  <this.commonView
                    image={IMAGES.first_aid}
                    name={'Medical Kit Bag'}
                  />
                  <this.commonView
                    image={IMAGES.travel_digital_bag}
                    name={'Travel Digital Bag'}
                  />
                  <this.commonView
                    image={IMAGES.paper_plane}
                    name={'GPS Device Rental'}
                  />
                  <this.commonView
                    image={IMAGES.taxi_image}
                    name={'Airport Transportation'}
                  />
                  <this.commonView
                    image={IMAGES.driving_license}
                    name={'International Driving License'}
                  />
                </ScrollView>
              </SafeAreaView>
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  modal: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
  },
  modalBody: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    backgroundColor: COLOR.WHITE,
    borderWidth: 1,
    width: wp(100),
    marginTop: 50,
    borderColor: COLOR.BORDER,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    flex: 1,
  },
});
