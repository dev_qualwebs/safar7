import React from 'react';
import { Modal, Text, View, StyleSheet, Image } from 'react-native';
import { ScrollView, TextInput, TouchableOpacity } from 'react-native';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import COLOR from '../../../../components/styles/Color';
import FONTS from '../../../../components/styles/Fonts';
import IMAGES from '../../../../components/styles/Images';
import Styles from '../../../../components/styles/Styles';
import NavigationBar from '../../../commonView/NavigationBar';
import { FloatingTitleTextInputField } from '../../../../helper/FloatingTitleTextInputField';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scrollview';

export default class AddGuest extends React.Component {
  constructor(props) {
    super(props);
    this.state = ({
      modalVisible: true,
      adult: 0,
      child: 0,
      infant: 0,

    });
  }

  onIncrement = (state, value) => {
    const newValue = value + 1;
    this.setState({ [state]: newValue });
  }

  onDecrement = (state, value) => {
    if (value != 0) {
      const newValue = value - 1;
      this.setState({ [state]: newValue });
    }

  }

  handleApply = () => {
    let guests = [];
    let guestString = '';

    if (this.state.adult > 0) {
      let object = {
        age_preference_id: 6,
        number_of_guest: this.state.adult
      };
      guestString = guestString + this.state.adult + ' Adult ,';
      guests.push(object);
    };

    if (this.state.child > 0) {
      let object = {
        age_preference_id: 7,
        number_of_guest: this.state.child
      };
      guestString = guestString + this.state.child + ' Child ,';
      guests.push(object);
    }

    if (this.state.infant > 0) {
      let object = {
        age_preference_id: 8,
        number_of_guest: this.state.infant
      };
      guestString = guestString + this.state.infant + ' Infant ';
      guests.push(object);
    }

    console.log('guestString',guestString)
    this.props.updateMasterState('guestModal', false);
    this.props.updateMasterState('modalVisible', true);
    this.props.updateMasterState('guests', guests);
    this.props.updateMasterState('guestText', guestString);
  }

  CommonView = (props) => (


    <View
      style={{
        flexDirection: 'row',
        width: wp(90),
        justifyContent: 'space-between',
        marginTop: 20,
      }}>
      <View
        style={{ flex: 1, alignSelf: 'center', justifyContent: 'flex-start' }}>
        <Text
          style={[
            Styles.button_font,
            { width: null, color: COLOR.BLACK, alignSelf: 'flex-start' },
          ]}>
          {props.category}
        </Text>
        <Text
          style={[
            Styles.small_label,
            { width: null, color: COLOR.BLACK, alignSelf: 'flex-start' },
          ]}>
          {props.ageGroup}
        </Text>
      </View>
      <View
        style={{
          flex: 1,
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'flex-end',
        }}>
        <TouchableOpacity
          onPress={() => this.onDecrement(props.state, props.value)}
          style={{
            height: 25,
            width: 25,
            borderRadius: 15,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Image
            style={{ height: 30, width: 30 }}
            resizeMode={'contain'}
            source={IMAGES.minus_image}
          />
        </TouchableOpacity>
        <Text
          style={[
            Styles.button_font,
            { width: 40, color: COLOR.BLACK, textAlign: 'center' },
          ]}>
          {props.value}
        </Text>
        <TouchableOpacity
          onPress={() => this.onIncrement(props.state, props.value)}
          style={{
            height: 30,
            width: 30,
            borderWidth: 1,
            borderRadius: 15,
            borderColor: COLOR.GREEN,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Image style={{ height: 20, width: 20 }} source={IMAGES.plus_image} />
        </TouchableOpacity>
      </View>
    </View>
  );

  SingleLineView = () => (
    <View
      style={{
        borderBottomColor: COLOR.GRAY,
        marginTop: 20,
        borderBottomWidth: 1,
      }}
    />
  );

  render() {
    return (
      <View style={Styles.container}>
        <Modal
          animationType={'slide'}
          transparent={true}
          style={{ backgroundColor: COLOR.BLACK }}
          visible={this.props.modalVisible}
          onRequestClose={() => {
            console.log('Modal has been closed.');
          }}>
          <View style={styles.modal}>
            <View style={styles.modalBody}>
              <NavigationBar
                height={40}
                prop={this.props}
                name={'Guest'}
                handleBack={()=> {
                  this.props.updateMasterState('guestModal', false);
                  this.props.updateMasterState('modalVisible', true);
                }}
              />
              <this.CommonView category={'Adult'} ageGroup={'Age 12+ '} value={this.state.adult} state={'adult'} />
              <this.SingleLineView />
              <this.CommonView category={'Child'} ageGroup={'Child 2-11'} value={this.state.child} state={'child'} />
              <this.SingleLineView />
              <this.CommonView category={'Infant'} ageGroup={'Below age 2'} value={this.state.infant} state={'infant'} />
              <this.SingleLineView />
              <View
                style={{
                  marginVertical: 20,
                  height: 50,
                  flexDirection: 'row',
                  width: wp(90),
                  alignSelf: 'center',
                }}>
                <TouchableOpacity
                  onPress={() => {
                    this.setState({ modalVisible: false }, (val) => {
                      this.props.navigation.navigate('PaymentScreen');
                    });
                  }}
                  style={{ flex: 1, marginRight: 10 }}>
                  <View
                    style={{
                      flex: 1,
                      alignItems: 'center',
                      justifyContent: 'center',
                      borderRadius: 10,
                      height: 50,
                      borderColor: COLOR.GRAY,
                      borderWidth: 1,
                      backgroundColor: COLOR.WHITE,
                    }}>
                    <Text style={[Styles.button_font, { color: COLOR.BLACK }]}>
                      Clear All
                    </Text>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={this.handleApply}
                  style={{ flex: 1 }}>
                  <View
                    style={{
                      flex: 1,
                      alignItems: 'center',
                      justifyContent: 'center',
                      borderRadius: 10,
                      height: 50,
                      backgroundColor: COLOR.GREEN,
                    }}>
                    <Text style={[Styles.button_font, { color: COLOR.WHITE }]}>
                      Apply
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  modal: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
  },
  modalBody: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    backgroundColor: COLOR.WHITE,
    borderWidth: 1,
    width: wp(100),
    borderColor: COLOR.BORDER,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    marginTop: 50,
  },
  image: {
    height: 120,
    width: 120,
    alignSelf: 'center',
    marginVertical: 20,
  },
  headingLabel: {
    fontFamily: FONTS.FAMILY_BOLD,
    fontSize: FONTS.MEDIUM,
    color: COLOR.BLACK,
  },
  singleLineView: {
    marginVertical: 20,
    borderBottomColor: COLOR.GRAY,
    width: wp(100),
    alignSelf: 'center',
    borderBottomWidth: 1,
  },
  borderView: {
    borderRadius: 10,
    borderWidth: 3,
    width: wp(90),
    borderColor: COLOR.GRAY,
    alignSelf: 'center',
  },
});
