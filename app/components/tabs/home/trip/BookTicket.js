import React from 'react';
import {Modal, Text, View, StyleSheet, Image} from 'react-native';
import {ScrollView, TextInput, TouchableOpacity} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import COLOR from '../../../../components/styles/Color';
import FONTS from '../../../../components/styles/Fonts';
import IMAGES from '../../../../components/styles/Images';
import Styles from '../../../../components/styles/Styles';
import NavigationBar from '../../../commonView/NavigationBar';
import {FloatingTitleTextInputField} from '../../../../helper/FloatingTitleTextInputField';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview';
import AddGuest from './AddGuest';
import {connect} from 'react-redux';
import TripAction from '../../../../redux/action/TripAction';
import SelectDate from '../../calendar/SelectDate';
import API from '../../../../api/Api';
import ImagePicker from 'react-native-image-crop-picker';
import ActionSheet from '../../../commonView/ActionSheet';
import ActivityIndicator from '../../../commonView/ActivityIndicator';
import SimpleToast from 'react-native-simple-toast';
import GooglePlacesTextInput from '../../../commonView/GooglePlacesTextInput';

class BookTicket extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      companyName: '',
      from: '',
      to: '',
      guests: '',
      guestText: '',
      modalVisible: true,
      fromDate: '',
      toDate: '',
      reservationNo: '',
      gateTerminal: '',
      flightNo: '',
      guestModal: false,
      attrName: '',
      openCalendar: false,
      actionModalVisible: false,
      image: null,
      loading: false,
    };
    if (this.props.route.params) {
      this.title = this.props.route.params.title;
      this.id = this.props.route.params.id;
      this.tripId = this.props.route.params.tripId;
    }
  }

  updateMasterState = (attrName, value) => {
    this.setState({[attrName]: value});
  };

  SingleLineView = () => (
    <View
      style={{
        borderBottomColor: COLOR.GRAY,
        marginBottom: 10,
        borderBottomWidth: 1,
      }}
    />
  );

  handleSchedule = () => {
    if (this.state.companyName == '') {
      SimpleToast.show('Enter company name');
    } else if (this.state.fromDate == '') {
      SimpleToast.show('Enter start date');
    } else if (this.state.toDate == '') {
      SimpleToast.show('Enter end date');
    } else if (this.state.guests == '') {
      SimpleToast.show('Enter number of guests');
    } else if (this.state.from == '' && this.title != 'Stay') {
      SimpleToast.show('Enter start location');
    } else if (this.state.to == '' && this.title != 'Stay') {
      SimpleToast.show('Enter end location');
    } else if (this.state.reservationNo == '') {
      SimpleToast.show('Enter reservation number');
    } else if (this.state.gateTerminal == '') {
      SimpleToast.show('Enter gate terminal');
    } else if (this.state.flightNo == '') {
      SimpleToast.show('Enter flight number');
    } else {
      let data = JSON.stringify({
        travelling_mode_id: this.id,
        company_name: this.state.companyName,
        from: this.state.from,
        to: this.state.to,
        depart_date: this.state.fromDate,
        depart_time: '12:00',
        reach_out_date: this.state.toDate,
        reach_out_time: '10:00',
        guest: this.state.guests,
        reservation_number: this.state.reservationNo,
        preference1: 'test1',
        preference2: 'test2',
        tickets: [this.state.image],
      });
      this.props.scheduleManualTrip(data, this.tripId);
    }
  };

  chooseImage = (val) => {
    let options = {
      includeBase64: true,
      title: 'Select Image',
      width: 1630,
      height: 750,
      compressImageMaxWidth: 1630,
      compressImageMaxHeight: 750,
      multiple: true,
    };

    if (val == 1) {
      ImagePicker.openCamera({
        title: 'Select Image',
        width: 1024,
        height: 472,
        compressImageMaxWidth: 1024,
        compressImageMaxHeight: 472,
        includeBase64: true,
      }).then((response) => {
        console.log('image local reposnse', response);
        this.setState({
          actionModalVisible: false,
          modalVisible: true,
        });
        this.callUploadImage(['data:image/jpeg;base64,' + response.data], val);
      });
    } else if (val == 2) {
      ImagePicker.openPicker(options).then((response) => {
        console.log('image local reposnse', response);
        this.setState({
          actionModalVisible: false,
          modalVisible: true,
        });
        // for (let i = 0; i < response.length; i++) {
        //   this.callUploadImage(response[i], val);
        // }
        let base64Response = response.map((val) => {
          return 'data:image/jpeg;base64,' + val.data;
        });
        this.callUploadImage(base64Response, val);
      });
    }
  };

  callUploadImage(res, val) {
    let data = {
      images: res,
      path: 'file',
    };

    this.setState({
      loading: true,
    });

    let that = this;
    const api = new API();
    api
      .uploadImage(data)
      .then((json) => {
        console.log('upload responce:-', json.data);

        if (json.status == 200) {
          console.log('Succes');
          this.setState({
            image: json.data.response[0],
            loading: false,
          });
        } else if (json.status == 400) {
          setTimeout(() => {
            Toast.show(json.data.message);
          }, 0);
        } else {
          setTimeout(() => {
            Toast.show(json.data.message);
          }, 0);
        }
      })
      .catch((error) => {
        this.setState({
          loading: false,
        });
        setTimeout(() => {
          console.log('json.data.message', error);
          Toast.show(String(error));
        }, 0);
        console.log('error:-', error);
      });
  }

  handleSheet = (val) => {
    if (val == 1) {
      this.chooseImage(1);
    } else if (val == 2) {
      this.chooseImage(2);
    } else if (val == 3) {
      this.setState({
        actionModalVisible: false,
        modalVisible: true,
      });
    }
  };

  render() {
    return (
      <View>
        <View style={Styles.container}>
          {this.state.openCalendar ? (
            <SelectDate
              modal={true}
              allowRangeSelection={false}
              minDate={
                this.state.fromDate == '' ? new Date() : this.state.fromDate
              }
              maxDate={new Date(5147962581000)}
              attrName={this.state.attrName}
              updateMasterState={this.updateMasterState}
              props={this.props}
            />
          ) : (
            <Modal
              animationType={'slide'}
              transparent={true}
              style={{backgroundColor: COLOR.BLACK}}
              visible={this.state.modalVisible}
              onRequestClose={() => {
                console.log('Modal has been closed.');
              }}>
              <View style={styles.modal}>
                <View style={styles.modalBody}>
                  <NavigationBar
                    prop={this.props}
                    left_image={IMAGES.back_arrow}
                    navHeight={40}
                    name={this.title}
                    handleBack={() => {
                      // this.props.route.params.updateMasterState('modalVisible',true);
                      this.props.navigation.goBack();
                    }}
                  />
                  <View style={{flex: 1}}>
                    {/* <KeyboardAwareScrollView
                      showsVerticalScrollIndicator={false}> */}
                    <ScrollView
                      bounces={false}
                      showsVerticalScrollIndicator={false}>
                      <View
                        style={[
                          styles.borderView,
                          {marginVertical: 20, paddingTop: 10},
                        ]}>
                        <FloatingTitleTextInputField
                          attrName="companyName"
                          title="Company Name"
                          value={this.state.companyName}
                          updateMasterState={this.updateMasterState}
                        />
                      </View>
                      <View style={[styles.borderView, {paddingTop: 10}]}>
                        <View style={{flexDirection: 'row'}}>
                          <View style={{flex: 1}}>
                            <View
                              style={{
                                flexDirection: 'row',
                                alignItems: 'center',
                                justifyContent: 'space-between',
                              }}>
                              <View style={{flex: 1}}>
                                <FloatingTitleTextInputField
                                  attrName="fromDate"
                                  title="From"
                                  handleFocus={() => {
                                    this.setState({
                                      attrName: 'fromDate',
                                      openCalendar: true,
                                    });
                                  }}
                                  value={this.state.fromDate}
                                  updateMasterState={this.updateMasterState}
                                />
                              </View>
                              <Image
                                style={{height: 12, width: 12, marginRight: 10}}
                                source={IMAGES.right_arrow}
                                resizeMode={'contain'}
                              />
                            </View>
                          </View>
                          <View
                            style={{width: 1, backgroundColor: COLOR.GRAY}}
                          />
                          <View style={{flex: 1}}>
                            <View
                              style={{
                                flexDirection: 'row',
                                alignItems: 'center',
                                justifyContent: 'space-between',
                              }}>
                              <View style={{flex: 1}}>
                                <FloatingTitleTextInputField
                                  attrName="toDate"
                                  title="Until"
                                  handleFocus={() => {
                                    this.setState({
                                      attrName: 'toDate',
                                      openCalendar: true,
                                    });
                                  }}
                                  value={this.state.toDate}
                                  updateMasterState={this.updateMasterState}
                                />
                              </View>
                              <Image
                                style={{height: 12, width: 12, marginRight: 10}}
                                source={IMAGES.right_arrow}
                                resizeMode={'contain'}
                              />
                            </View>
                          </View>
                        </View>
                        <this.SingleLineView />
                        <View
                          style={{
                            flexDirection: 'row',
                            alignItems: 'center',
                            justifyContent: 'space-between',
                          }}>
                          <View style={{flex: 1}}>
                            <FloatingTitleTextInputField
                              keyboardType={'number-pad'}
                              attrName="guests"
                              title="Guest"
                              value={this.state.guestText}
                              updateMasterState={this.updateMasterState}
                            />
                          </View>
                          <TouchableOpacity
                            onPress={() => {
                              this.setState({
                                guestModal: true,
                                modalVisible: false,
                              });
                            }}>
                            <Image
                              style={{height: 12, width: 12, marginRight: 10}}
                              source={IMAGES.right_arrow}
                              resizeMode={'contain'}
                            />
                          </TouchableOpacity>
                        </View>
                      </View>

                      {this.title == 'Stay' || this.title == 'Other' ? null : (
                        <View
                          style={[
                            styles.borderView,
                            {marginTop: 20, paddingTop: 10},
                          ]}>
                          <View
                            style={{
                              flexDirection: 'row',
                              alignItems: 'center',
                              justifyContent: 'space-between',
                            }}>
                            <View style={{flex: 1}}>
                              <FloatingTitleTextInputField
                                handleFocus={() => {
                                  this.props.navigation.navigate(
                                    'GooglePlaces',
                                    {
                                      updateMasterState: this.updateMasterState,
                                      attrName: 'from',
                                    },
                                  );
                                  this.setState({modalVisible: false});
                                }}
                                attrName="from"
                                title="From"
                                value={this.state.from}
                                updateMasterState={this.updateMasterState}
                              />
                            </View>
                            <Image
                              style={{height: 25, width: 25, marginRight: 10}}
                              source={IMAGES.pinpoint}
                              resizeMode={'contain'}
                            />
                          </View>
                          <this.SingleLineView />

                          <View
                            style={{
                              flexDirection: 'row',
                              alignItems: 'center',
                              justifyContent: 'space-between',
                            }}>
                            <View style={{flex: 1}}>
                              <FloatingTitleTextInputField
                                handleFocus={() => {
                                  this.props.navigation.navigate(
                                    'GooglePlaces',
                                    {
                                      updateMasterState: this.updateMasterState,
                                      attrName: 'to',
                                    },
                                  );
                                  this.setState({modalVisible: false});
                                }}
                                attrName="to"
                                title="To"
                                value={this.state.to}
                                updateMasterState={this.updateMasterState}
                              />
                            </View>
                            <Image
                              style={{height: 25, width: 25, marginRight: 10}}
                              source={IMAGES.pinpoint}
                              resizeMode={'contain'}
                            />
                          </View>
                        </View>
                      )}

                      <View
                        style={[
                          styles.borderView,
                          {
                            marginTop: 20,
                            marginBottom: 20,
                            paddingTop: 10,
                            zIndex: 10004,
                          },
                        ]}>
                        {this.title == 'Stay' || this.title == 'Other' ? (
                          <>
                            <View
                              style={{
                                padding: 10,
                                paddingTop: 0,
                                zIndex: 10004,
                              }}>
                              <GooglePlacesTextInput
                                value={this.state.name}
                                onValueChange={(text) =>
                                  this.setState({name: text})
                                }
                              />
                            </View>
                            <this.SingleLineView />
                          </>
                        ) : null}

                        <FloatingTitleTextInputField
                          keyboardType={'number-pad'}
                          attrName="reservationNo"
                          title="Reservation Number"
                          value={this.state.reservationNo}
                          updateMasterState={this.updateMasterState}
                        />
                        {this.title == 'Train' ? null : (
                          <>
                            <this.SingleLineView />
                            <View style={{flexDirection: 'row'}}>
                              <View style={{flex: 1, justifyContent: 'center'}}>
                                <FloatingTitleTextInputField
                                  attrName="gateTerminal"
                                  title={
                                    this.title == 'Cruise'
                                      ? 'Deck'
                                      : this.title == 'Car'
                                      ? 'Car Modal'
                                      : this.title == 'Stay'
                                      ? 'Rooms'
                                      : 'Gate/Terminal'
                                  }
                                  value={this.state.gateTerminal}
                                  updateMasterState={this.updateMasterState}
                                />
                              </View>
                              <View
                                style={{width: 1, backgroundColor: COLOR.GRAY}}
                              />
                              <View style={{flex: 1}}>
                                <FloatingTitleTextInputField
                                  attrName="flightNo"
                                  keyboardType={'number-pad'}
                                  title={
                                    this.title == 'Cruise'
                                      ? 'Cabin'
                                      : this.title == 'Car'
                                      ? 'Car Type'
                                      : this.title == 'Stay'
                                      ? 'Room Type'
                                      : 'Flight Number'
                                  }
                                  value={this.state.flightNo}
                                  updateMasterState={this.updateMasterState}
                                />
                              </View>
                            </View>
                          </>
                        )}
                      </View>

                      <TouchableOpacity
                        onPress={() =>
                          this.setState({
                            actionModalVisible: true,
                            modalVisible: false,
                          })
                        }
                        style={{
                          alignItems: 'center',
                          justifyContent: 'center',
                          height: 50,
                          borderRadius: 10,
                          borderColor: COLOR.GRAY,
                          borderWidth: 1,
                          width: wp(90),
                          backgroundColor: COLOR.WHITE,
                          marginVertical: 10,
                        }}>
                        <View
                          style={{
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            paddingLeft: 20,
                            paddingRight: 10,

                            alignItems: 'center',
                          }}>
                          <Image
                            source={IMAGES.chain}
                            resizeMode={'contain'}
                            style={{
                              width: 20,
                              height: 20,
                              tintColor: COLOR.BLACK,
                              marginRight: 10,
                            }}
                          />
                          <Text
                            style={[
                              Styles.button_font,
                              {color: COLOR.BLACK, flex: 1},
                            ]}>
                            Attach a file
                          </Text>
                          <Image
                            source={IMAGES.right_arrow}
                            resizeMode={'contain'}
                            style={{
                              width: 15,
                              height: 15,
                              tintColor: COLOR.BLACK,
                              marginRight: 10,
                            }}
                          />
                        </View>
                      </TouchableOpacity>
                    </ScrollView>
                    <View style={{marginVertical: 20}}>
                      <TouchableOpacity
                        onPress={this.handleSchedule}
                        style={{
                          alignItems: 'center',
                          justifyContent: 'center',
                          height: 50,
                          borderRadius: 10,

                          width: wp(90),
                          backgroundColor: COLOR.GREEN,
                        }}>
                        <Text
                          style={[Styles.button_font, {color: COLOR.WHITE}]}>
                          Add Reservation
                        </Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                </View>
              </View>
              <ActivityIndicator loading={this.state.loading} />
            </Modal>
          )}
        </View>
        <ActionSheet
          modalVisible={this.state.actionModalVisible}
          handleSheet={this.handleSheet}
        />

        <AddGuest
          updateMasterState={this.updateMasterState}
          modalVisible={this.state.guestModal}
          navigation={this.props.navigation}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  modal: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
  },
  modalBody: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    backgroundColor: COLOR.WHITE,
    borderWidth: 1,
    width: wp(100),
    marginTop: 50,
    borderColor: COLOR.BORDER,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    flex: 1,
  },
  image: {
    height: 120,
    width: 120,
    alignSelf: 'center',
    marginVertical: 20,
  },
  headingLabel: {
    fontFamily: FONTS.FAMILY_BOLD,
    fontSize: FONTS.MEDIUM,
    color: COLOR.BLACK,
  },
  singleLineView: {
    marginVertical: 20,
    borderBottomColor: COLOR.GRAY,
    width: wp(100),
    alignSelf: 'center',
    borderBottomWidth: 1,
  },
  borderView: {
    borderRadius: 10,
    borderWidth: 1,
    width: wp(90),
    borderColor: COLOR.GRAY,
    alignSelf: 'center',
  },
});

const mapDispatchToProps = (dispatch) => {
  return {
    scheduleManualTrip: (data, val) => {
      dispatch(TripAction.scheduleManualTrip(data, val));
    },
  };
};

export default connect(null, mapDispatchToProps)(BookTicket);
