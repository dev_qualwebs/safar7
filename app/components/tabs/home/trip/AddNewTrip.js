import React from 'react';
import {Modal, Text, View, StyleSheet, Image, Alert, KeyboardAvoidingView} from 'react-native';
import {TextInput, TouchableOpacity} from 'react-native';
import {
  heightPercentageToDP,
  widthPercentageToDP,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import {connect} from 'react-redux';
import COLOR from '../../../../components/styles/Color';
import FONTS from '../../../../components/styles/Fonts';
import IMAGES from '../../../../components/styles/Images';
import Styles from '../../../../components/styles/Styles';
import TripAction from '../../../../redux/action/TripAction';
import NavigationBar from '../../../commonView/NavigationBar';
import SelectDate from '../../calendar/SelectDate';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview';
import Toast from 'react-native-simple-toast';
import API from '../../../../api/Api';
import {DoneButtonKeyboard} from '../../../commonView/CommonView';
import {GooglePlacesAutocomplete} from 'react-native-google-places-autocomplete';

const api = new API();

class AddNewTrip extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      countryCode: '',
      modalVisible: true,
      trip_name: null,
      budget: null,
      checkInDate: null,
      checkOutDate: null,
      openCalendar: false,
      isEdit: false,
      tripId: null,
      placesName: '',
      placesId:'',
    };

    this.searchRef = React.createRef();
  }

  componentDidMount() {
    if (this.props.route.params && this.props.route.params.data) {
      this.setState({
        trip_name: this.props.route.params.data.trip_name,
        budget: this.props.route.params.data.budget,
        checkInDate: this.props.route.params.data.checkInDate,
        checkOutDate: this.props.route.params.data.checkOutDate,
        isEdit: true,
        tripId: this.props.route.params.data.id,
        placesId : this.props.route.params.data.location_id,
        placesName : this.props.route.params.data.location_name,
      },()=>{
        console.log('this.props.route.params.data',this.props.route.params.data)
      });
    }
  }

  updateMasterState = (attrName, value) => {
    this.setState({[attrName]: value});
  };

  requestTrip = () => {
    if (!this.state.trip_name) {
      Toast.show('Enter Trip name');
    } else if (!this.state.budget) {
      Toast.show('Enter Budget');
    } else if (!this.state.checkInDate) {
      Toast.show('Select date');
    } else if (!this.state.budget) {
      Toast.show('Select date');
    }  else if (this.state.placesName == '') {
      Toast.show('Please select a location');
    }else {
      const data = {
        trip_type: 2,
        trip_name: this.state.trip_name,
        budget: this.state.budget,
        currency_id: 141,
        selected_date_type_id: 1,
        depart: this.state.checkInDate,
        return: this.state.checkOutDate,
        depart_flexibility_id: 1,
        return_flexibility_id: 1,
        within_month_preference: 'October 2022',
        location_id : this.state.placesId,
        location_name : this.state.placesName
      };
      this.props.requestTrip(data);
      this.props.navigation.goBack();
    }
  };

  updateTrip = () => {
    if (!this.state.trip_name) {
      Toast.show('Enter Trip name');
    } else if (!this.state.budget) {
      Toast.show('Enter Budget');
    } else if (!this.state.checkInDate) {
      Toast.show('Select date');
    } else if (!this.state.budget) {
      Toast.show('Select date');
    } else if (this.state.placesId == '') {
      Toast.show('Please select a location');
    } else {
      const data = {
        trip_type: 2,
        trip_name: this.state.trip_name,
        budget: this.state.budget,
        currency_id: 141,
        selected_date_type_id: 1,
        depart: this.state.checkInDate,
        return: this.state.checkOutDate,
        depart_flexibility_id: 1,
        return_flexibility_id: 1,
        within_month_preference: 'October 2022',
        location_id : this.state.placesId,
        location_name : this.state.placesName
      };

      api
        .updateRequestedTrip(`${this.state.tripId}`, data)
        .then((json) => {
          if (json.status == 200) {
            Toast.show(json.data.message);
            this.props.navigation.goBack();
          } else {
            Toast.show(json.data.message);
            console.log(json.data.message);
          }
        })
        .catch(function (error) {
          Toast.show(error.response.data.message);
        });
    }
  };

  render() {
    return (
      <View style={Styles.container}>
        {this.state.openCalendar ? (
          <SelectDate
            minDate={Date()}
            maxDate={new Date(5147962581000)}
            updateMasterState={this.updateMasterState}
            props={this.props}
          />
        ) : (
          <Modal
            animationType={'slide'}
            transparent={true}
            style={{backgroundColor: COLOR.BLACK}}
            visible={this.state.modalVisible}
            onRequestClose={() => {
              console.log('Modal has been closed.');
            }}>
               <KeyboardAvoidingView
                behavior={'padding'}
                style={styles.modal}>
            {/* <View style={{flex: 1}}> */}
              {/* <View style={styles.modal}> */}
                <View style={styles.modalBody}>
                  <NavigationBar
                    prop={this.props}
                    navHeight={40}
                    name={'Add New Trip'}
                  />
                  <View style={{flexDirection: 'row', alignItems: 'center'}}>
                    <View>
                      <Text
                        style={[
                          styles.body_label,
                          {
                            width: wp(90),
                            alignSelf: 'center',
                            marginTop: 20,
                            marginBottom: 10,
                            fontFamily: FONTS.FAMILY_BOLD,
                          },
                        ]}>
                        Trip Name
                      </Text>
                      <TextInput
                        inputAccessoryViewID={'uniqueID'}
                        placeholder={'Enter name of the city or airport'}
                        placeholderTextColor={COLOR.LIGHT_TEXT_COLOR}
                        style={Styles.body_label}
                        value={this.state.trip_name}
                        onChangeText={(text) =>
                          this.setState({trip_name: text})
                        }
                      />
                      <DoneButtonKeyboard />
                    </View>
                    <Image
                      style={{width: 15, height: 15}}
                      source={IMAGES.right_arrow}
                      resizeMode={'contain'}
                    />
                  </View>
                  <View style={styles.singleLineView} />
                  <View style={{flexDirection: 'row', alignItems: 'center'}}>
                    <View>
                      <Text
                        style={[
                          styles.body_label,
                          {
                            width: wp(90),
                            alignSelf: 'center',
                            marginBottom: 10,
                            fontFamily: FONTS.FAMILY_BOLD,
                          },
                        ]}>
                        Budget
                      </Text>
                      <TextInput
                        inputAccessoryViewID={'uniqueID'}
                        placeholder={'Enter your budget'}
                        placeholderTextColor={COLOR.LIGHT_TEXT_COLOR}
                        style={Styles.body_label}
                        value={this.state.budget}
                        onChangeText={(text) => this.setState({budget: text})}
                      />
                    </View>
                    <Image
                      style={{width: 15, height: 15}}
                      source={IMAGES.right_arrow}
                      resizeMode={'contain'}
                    />
                  </View>
                  <View style={styles.singleLineView} />
                  <View style={{flexDirection: 'row', alignItems: 'center'}}>
                    <View>
                      <Text
                        style={[
                          styles.body_label,
                          {
                            width: wp(90),
                            alignSelf: 'center',
                            fontFamily: FONTS.FAMILY_BOLD,
                            marginBottom: 10,
                            fontSize: 14,
                          },
                        ]}>
                        Trip Dates
                      </Text>
                      <TouchableOpacity
                        onPress={() => this.setState({openCalendar: true})}>
                        <Text
                          style={[
                            Styles.body_label,
                            {color: COLOR.LIGHT_TEXT_COLOR},
                          ]}>
                          {this.state.checkInDate
                            ? this.state.checkOutDate
                              ? this.state.checkInDate +
                                ' - ' +
                                this.state.checkOutDate
                              : 'Enter start and end dates for the trip'
                            : 'Enter start and end dates for the trip'}
                        </Text>
                      </TouchableOpacity>
                    </View>
                    <Image
                      style={{width: 15, height: 15}}
                      source={IMAGES.right_arrow}
                      resizeMode={'contain'}
                    />
                  </View>
                  <View style={styles.singleLineView} />
                  <Text
                    style={[
                      styles.body_label,
                      {
                        width: wp(90),
                        alignSelf: 'center',
                        fontFamily: FONTS.FAMILY_BOLD,
                        marginBottom: 10,
                        fontSize: 14,
                      },
                    ]}>
                    Location
                  </Text>
                  <TextInput
                    inputAccessoryViewID={'uniqueID'}
                    onPress={() => {
                      this.props.navigation.navigate('GooglePlacesImages', {
                        updateMasterState: this.updateMasterState,
                        attrName: 'placesId',
                        attr2Name: 'placesName',
                      });
                      this.setState({modalVisible: false});
                    }}
                    placeholder={'Add a location'}
                    placeholderTextColor={COLOR.LIGHT_TEXT_COLOR}
                    value={this.state.placesName}
                    onChangeText={(text) => this.setState({placesName: text})}
                    style={Styles.body_label}
                  />
                  <View style={styles.singleLineView} />

                  <TouchableOpacity
                    onPress={() => {
                      if (this.state.isEdit) {
                        this.updateTrip();
                      } else {
                        this.requestTrip();
                      }
                      this.props.getManualTrips(1);
                      console.log('this.state.places', this.state.placesId);
                    }}
                    style={{
                      alignItems: 'center',
                      justifyContent: 'center',
                      height: 50,
                      borderRadius: 10,
                      borderColor: COLOR.LIGHT_TEXT,
                      borderWidth: 1,
                      width: wp(70),
                      backgroundColor: COLOR.WHITE,
                      marginVertical: 20,
                    }}>
                    <Text style={[Styles.button_font, {color: COLOR.BLACK}]}>
                      {this.state.isEdit ? 'Edit' : 'Add'}
                    </Text>
                  </TouchableOpacity>
                </View>
              {/* </View> */}
            {/* </View> */}
            </KeyboardAvoidingView>
          </Modal>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  modal: {
    flex: 1,
    height: heightPercentageToDP(100),
    alignItems: 'center',
    justifyContent: 'flex-end',
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
  },
  modalBody: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    backgroundColor: COLOR.WHITE,
    borderWidth: 1,
    width: wp(100),
    borderColor: COLOR.BORDER,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
  image: {
    height: 120,
    width: 120,
    alignSelf: 'center',
    marginVertical: 20,
  },
  headingLabel: {
    fontFamily: FONTS.FAMILY_BOLD,
    fontSize: FONTS.MEDIUM,
    color: COLOR.BLACK,
  },
  singleLineView: {
    marginVertical: 20,
    borderBottomColor: COLOR.GRAY,
    width: wp(100),
    alignSelf: 'center',
    borderBottomWidth: 1,
  },
});

mapDispatchToProps = (dispatch) => {
  return {
    requestTrip: (data) => {
      dispatch(TripAction.requestTrip(data));
    },
    getManualTrips: (val) => {
      dispatch(TripAction.getManualTrips(val));
    },
  };
};

export default connect(null, mapDispatchToProps)(AddNewTrip);
