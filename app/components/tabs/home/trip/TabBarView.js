import * as React from 'react';
import {View, StyleSheet, Dimensions, Alert, Text} from 'react-native';
import {widthPercentageToDP} from 'react-native-responsive-screen';
import {TabView, SceneMap, TabBar} from 'react-native-tab-view';
import {
  T_SCHEDULE_TRIP_TAB,
  T_REQUEST_TRIP_TAB,
  T_SCHEDULE_FLIGHT_TAB,
  T_SCHEDULE_CAR_TAB,
  T_PROFILE_TAB,
  T_PACKAGE_DETAIL_TAB,
  T_TRAVELLER_TAB,
  T_REQUEST_OR_PACKAGE_TAB
} from '../../../../helper/Constants';
import COLOR from '../../../styles/Color';
import FONTS from '../../../styles/Fonts';
import MulticityFlight from '../../bookFlight/MulticityFlight';
import OneWayFlight from '../../bookFlight/OneWayFlight';
import DifferentDropoff from '../../car/DifferentDropoff';
import SameDropoff from '../../car/SameDropoff';
import PastRequest from '../../message/PastRequest';
import UpcomingRequest from '../../message/UpcomingRequest';
import PackageHightlights from '../../package/PackageHighlights';
import PackagePlans from '../../package/PackagePlans';
import PackageServices from '../../package/PackagesServices';
import PassportDetail from '../../profile/PassportDetail';
import PersonalDetail from '../../profile/PersonalDetail';
import TravellerPassport from '../../profile/TravellerPassport';
import TravellerProfileDetail from '../../profile/TravellerProfileDetail';
import PastTrips from './PastTrips';
import UpcomingTrips from './UpcomingTrips';

const initialLayout = {width: Dimensions.get('window').width};

export default TopTabBar = (props) => {
  const [index, setIndex] = React.useState(0);

  setCurrentTab = (tabName, val) => {
    switch (tabName) {
      case T_SCHEDULE_TRIP_TAB:
        if (val == 1) {
          return 'Upcoming Trips';
        } else if (val == 2) {
          return 'Past Trips';
        }
        break;
      case T_REQUEST_TRIP_TAB:
        if (val == 1) { 
          return 'Upcoming';
        } else if (val == 2) {
          return 'Past';
        } else if (val == 3) {
          return 'Cancelled';
        }
        break;
      case T_PACKAGE_DETAIL_TAB:
        if (val == 1) {
          return 'Details';
        } else if (val == 2) {
          return 'Include/Exclude';
        } else if (val == 3) {
          return 'Trip Plans';
        }
        break;
      case T_SCHEDULE_FLIGHT_TAB:
        if (val == 1) {
          return 'One Way';
        } else if (val == 2) {
          return 'Round-Trip';
        } else if (val == 3) {
          return 'Multi-City';
        }
        break;
      case T_SCHEDULE_CAR_TAB:
        if (val == 1) {
          return 'Same Drop-Off';
        } else if (val == 2) {
          return 'Different Drop-Off';
        }
        break;
      case T_PROFILE_TAB:
        if (val == 1) {
          return 'Personal Details';
        } else if (val == 2) {
          return 'Passport Details';
        }
        break;
        case T_REQUEST_OR_PACKAGE_TAB:
          if (val == 1) {
            return 'Requests';
          } else if (val == 2) {
            return 'Packages';
          }
          break;
      case T_TRAVELLER_TAB:
        if (val == 1) {
          return 'Personal Details';
        } else if (val == 2) {
          return 'Passport Details';
        }
        break;
      default:
        break;
    }
  };

  const [routes] =
    props.currentTab == T_SCHEDULE_FLIGHT_TAB ||
    props.currentTab == T_REQUEST_TRIP_TAB ||
    props.currentTab == T_PACKAGE_DETAIL_TAB
      ? React.useState([
          {
            key: 'first',
            title: setCurrentTab(props.currentTab, 1),
          },
          {
            key: 'second',
            title: setCurrentTab(props.currentTab, 2),
          },
          {
            key: 'third',
            title: setCurrentTab(props.currentTab, 3),
          },
        ])
      : React.useState([
          {
            key: 'first',
            title: setCurrentTab(props.currentTab, 1),
          },
          {
            key: 'second',
            title: setCurrentTab(props.currentTab, 2),
          },
        ]);

  const renderScene = ({route, jumpTo}) => {
    if (props.currentTab == T_SCHEDULE_TRIP_TAB) {
      switch (route.key) {
        case 'first':
          return (
            <UpcomingTrips
              navigation={props.props.navigation}
              packageTab={props.packageTab}
            />
          );
        case 'second':
          return (
            <PastTrips
              navigation={props.props.navigation}
              packageTab={props.packageTab}
            />
          );
        default:
          return null;
      }
    } else if (props.currentTab == T_REQUEST_TRIP_TAB) {
      switch (route.key) {
        case 'first':
          return (
            <UpcomingRequest
              status={0}
              navigation={props.props.navigation}
              packages={props.packages}
              title={props.title}
            />
          );
        case 'second':
          return (
            <UpcomingRequest
              status={6}
              navigation={props.props.navigation}
              packages={props.packages}
              title={props.title}
            />
          );
        case 'third':
          return (
            <UpcomingRequest
              status={5}
              navigation={props.props.navigation}
              packages={props.packages}
              title={props.title}
            />
          );
        default:
          return null;
      }
    } else if (props.currentTab == T_PACKAGE_DETAIL_TAB) {
      switch (route.key) {
        case 'first':
          return (
            <PackageHightlights
              data={props.props}
              navigation={props.props.navigation}
            />
          );
        case 'second':
          return (
            <PackageServices
              data={props.props}
              navigation={props.props.navigation}
            />
          );
        case 'third':
          return (
            <PackagePlans
              data={props.props}
              navigation={props.props.navigation}
            />
          );
        default:
          return null;
      }
    } else if (props.currentTab == T_REQUEST_OR_PACKAGE_TAB) {
      switch (route.key) {
        case 'first':
          return (
            <TopTabBar
          currentTab={T_REQUEST_TRIP_TAB}
          props={props.props}
          packages={false}
          // title={'Requests'}
        />
          );
        case 'second':
          return (
            <TopTabBar
            currentTab={T_REQUEST_TRIP_TAB}
            props={props.props}
            packages={true}
            // title={'Packages'}
          />
          );
        default:
          return null;
      }
    }  else if (props.currentTab == T_SCHEDULE_FLIGHT_TAB) {
      switch (route.key) {
        case 'first':
          return (
            <OneWayFlight
              depart={props.depart}
              return={props.return}
              props={props.props}
              data={props.data}
              navigation={props.props.navigation}
              flightId={props.flightId}
              package_request_id={props.package_request_id}
              screen={1}
            />
          );
        case 'second':
          return (
            <OneWayFlight
              depart={props.depart}
              return={props.return}
              props={props.props}
              data={props.data}
              navigation={props.props.navigation}
              flightId={props.flightId}
              package_request_id={props.package_request_id}
              screen={2}
            />
          );
        case 'third':
          return (
            <MulticityFlight
              depart={props.depart}
              return={props.return}
              props={props.props}
              data={props.data}
              navigation={props.props.navigation}
              flightId={props.flightId}
              package_request_id={props.package_request_id}
              screen={3}
            />
          );
        default:
          return null;
      }
    } else if (props.currentTab == T_SCHEDULE_CAR_TAB) {
      switch (route.key) {
        case 'first':
          return (
            <SameDropoff
              props={props.props}
              navigation={props.props.navigation}
            />
          );
        case 'second':
          return (
            <DifferentDropoff
              props={props.props}
              navigation={props.props.navigation}
            />
          );
        default:
          return null;
      }
    } else if (props.currentTab == T_PROFILE_TAB) {
      switch (route.key) {
        case 'first':
          return (
            <PersonalDetail
              nextBtn={props.nextBtn}
              jumpTo={jumpTo}
              navigation={props.props.navigation}
              props={props.props}
              setData={props.setData}
              singleTravellerData={props.singleTravellerData}
            />
          );
        case 'second':
          return (
            <PassportDetail
              nextBtn={props.nextBtn}
              jumpTo={jumpTo}
              navigation={props.props.navigation}
              props={props.props}
              data={props.data}
              singleTravellerData={props.singleTravellerData}
            />
          );
        default:
          return null;
      }
    } else if (props.currentTab == T_TRAVELLER_TAB) {
      switch (route.key) {
        case 'first':
          return (
            <TravellerProfileDetail
              jumpTo={jumpTo}
              navigation={props.props.navigation}
              props={props.props}
              id={props.id}
              addTraveller={props.addTraveller}
              singleTravellerData={props.singleTravellerData}
            />
          );
        case 'second':
          return (
            <TravellerPassport
              jumpTo={jumpTo}
              navigation={props.props.navigation}
              props={props.props}
              id={props.id}
              singleTravellerData={props.singleTravellerData}
            />
          );
        default:
          return null;
      }
    }
  };

  const renderTabBar1 = (props) => (
    <TabBar
      {...props}
      indicatorStyle={{backgroundColor: COLOR.GREEN}}
      style={{backgroundColor: COLOR.WHITE, borderRadius: 10}}
      activeColor={COLOR.BLACK}
      labelStyle={{
        textTransform: 'capitalize',
        fontFamily: FONTS.FAMILY_SEMIBOLD,
        fontSize: 14,
      }}
      inactiveColor={'#8a9a92'}
    />
  );
  const renderTabBar2 = (props) => (
    <TabBar
      {...props}
      tabStyle={{backgroundColor: COLOR.WHITE}}
      indicatorStyle={{backgroundColor: '#00000000'}}
      style={{backgroundColor: COLOR.WHITE, borderRadius: 10}}
      activeColor={COLOR.WHITE}
      labelStyle={{textTransform: 'capitalize'}}
      inactiveColor={COLOR.BLACK}
      renderLabel={({route, focused, color}) => (
        <View
          style={{
            backgroundColor: focused ? COLOR.GREEN : COLOR.WHITE,
            height: 35,
            justifyContent: 'center',
            alignItems: 'center',
            paddingHorizontal: 5,
            borderRadius: 10,
            marginBottom: -8,
            fontFamily: FONTS.FAMILY_SEMIBOLD,
            fontSize: 14,
          }}>
          <Text style={{color: focused ? 'white' : 'black', margin: 10}}>
            {route.title}
          </Text>
        </View>
      )}
    />
  );
  const renderTabBar3 = (prop) => (
    <TabBar
      {...prop}
      style={{
        backgroundColor: '#C8DCE9',
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        overflow: 'hidden',
      }}
      labelStyle={{textTransform: 'capitalize'}}
      renderLabel={({route, focused, color}) => (
        <View
          style={{
            backgroundColor: focused ? COLOR.WHITE : 'transparent',
            width:
              props.currentTab == T_SCHEDULE_FLIGHT_TAB
                ? widthPercentageToDP(90) / 3
                : widthPercentageToDP(90) / 2,
            margin: -20,
            justifyContent: 'center',
            alignItems: 'center',
            flex: 1,
            borderTopLeftRadius: 22,
            borderTopRightRadius: 22,
            borderColor: 'transparent',
            overflow: 'hidden',
          }}>
          <Text
            style={[
              {
                color: focused ? COLOR.GREEN : COLOR.BLACK,
                textTransform: 'capitalize',
                fontFamily: FONTS.FAMILY_SEMIBOLD,
                fontSize: 14,
              },
            ]}>
            {route.title}
          </Text>
        </View>
      )}
    />
  );

  const renderTabBar4 = (prop) => (
    <TabBar
      {...prop}
      style={{
        backgroundColor: COLOR.GREEN,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        overflow: 'hidden',
      }}
      indicatorStyle={{backgroundColor: '#00000000'}}
      labelStyle={{textTransform: 'capitalize'}}
      renderLabel={({route, focused, color}) => (
        <View
          style={{
            backgroundColor: focused ? COLOR.WHITE : 'transparent',
            width: widthPercentageToDP(100) / 3,
            margin: -20,
            justifyContent: 'center',
            alignItems: 'center',
            flex: 1,
            overflow: 'hidden',
          }}>
          <Text
            style={[
              {
                color: focused ? COLOR.GREEN : COLOR.WHITE,
                textTransform: 'capitalize',
                fontFamily: FONTS.FAMILY_SEMIBOLD,
                fontSize: 14,
              },
            ]}>
            {route.title}
          </Text>
        </View>
      )}
    />
  );


  const renderTabBar5 = (prop) => (
    <TabBar
      {...prop}
      style={{
        backgroundColor: COLOR.GREEN,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        overflow: 'hidden',
      }}
      indicatorStyle={{backgroundColor: '#00000000'}}
      labelStyle={{textTransform: 'capitalize'}}
      renderLabel={({route, focused, color}) => (
        <View
          style={{
            backgroundColor: focused ? COLOR.WHITE : COLOR.GREEN ,
            width: widthPercentageToDP(100) / 2,
            margin: -20,
            justifyContent: 'center',
            alignItems: 'center',
            flex: 1,
            overflow: 'hidden',
          }}>
          <Text
            style={[
              {
                color: focused ? COLOR.GREEN : COLOR.WHITE ,
                textTransform: 'capitalize',
                fontFamily: FONTS.FAMILY_SEMIBOLD,
                fontSize: 14,
              },
            ]}>
            {route.title}
          </Text>
        </View>
      )}
    />
  );

  return (
    <TabView
      {...props}
      navigationState={{index, routes}}
      renderScene={renderScene}
      renderTabBar={
        props.currentTab == T_SCHEDULE_TRIP_TAB ||
        props.currentTab == T_PROFILE_TAB ||
        props.currentTab == T_TRAVELLER_TAB 
          ? renderTabBar1
          : props.currentTab == T_REQUEST_TRIP_TAB
          ? renderTabBar2
          : props.currentTab == T_PACKAGE_DETAIL_TAB
          ? renderTabBar4 : props.currentTab == T_REQUEST_OR_PACKAGE_TAB ? renderTabBar5  : renderTabBar3
      }
      onIndexChange={setIndex}
      initialLayout={initialLayout}
    />
  );
};
