import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {ImageBackground} from 'react-native';
import {Image} from 'react-native';
import {SafeAreaView} from 'react-native';
import ActionButton from 'react-native-action-button';

import {
  FlatList,
  ScrollView,
  TouchableOpacity,
} from 'react-native-gesture-handler';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview';
import {widthPercentageToDP} from 'react-native-responsive-screen';
import {DAY} from '../../../../helper/Constants';
import {DateView} from '../../../commonView/CommonView';
import COLOR from '../../../styles/Color';
import FONTS from '../../../styles/Fonts';
import IMAGES from '../../../styles/Images';
import Styles from '../../../styles/Styles';

export default class extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      trip: this.props.route.params.item,
      tripId: this.props.route.params.item.id,
      isActionButtonOpen:false,
    };
  }

  dateView(props) {
    return (
      <DateView
        day={props.item.day}
        date={props.item.date}
        selected={props.item.isSelected}
      />
    );
  }

  componentWillMount() {
    // console.log(this.props.route.params.item);
  }

  render() {
    const trip = this.state.trip;
    return (
      <SafeAreaView style={styles.container}>
        <View>
          <TouchableOpacity
            style={{
              height: 30,
              width: 45,
              marginLeft: 15,
              marginTop: 0,
              marginBottom: 0,
            }}
            onPress={() => {
              this.props.navigation.goBack();
            }}>
            <Image
              source={require('../../../styles/assets/left_arrow.png')}
              resizeMode={'contain'}
              style={{height: 15, width: 15}}
            />
          </TouchableOpacity>
        </View>
        <ScrollView style={styles.container}>
          <View style={styles.toolbar}>
            <Text
              style={{
                color: COLOR.WHITE,
                fontFamily: FONTS.FAMILY_REGULAR,
                alignSelf: 'center',
                marginHorizontal: 10,
              }}>
              {trip.trip_date_type.flexibility.depart}
            </Text>
            <View style={{justifyContent: 'center', flexDirection: 'row'}}>
              <Image
                source={require('../../../styles/assets/weather.png')}
                style={{
                  width: 20,
                  height: 20,
                  alignSelf: 'center',
                  tintColor: COLOR.WHITE,
                }}
                resizeMode={'contain'}
              />
              <Text
                style={{
                  fontFamily: FONTS.FAMILY_REGULAR,
                  alignSelf: 'center',
                  marginHorizontal: 10,
                  color: COLOR.WHITE,
                }}>
                26 - 38°
              </Text>
              {/* <View
                                style={{
                                    width: 40,
                                    height: 40,
                                    backgroundColor: COLOR.BORDER,
                                    justifyContent: 'center'
                                }}
                            >
                                <Image
                                    source={require('../../../styles/assets/zig-zag.png')}
                                    style={{
                                        width: 20,
                                        height: 20,
                                        alignSelf: 'center',
                                        backgroundColor: 'transparent'
                                    }}
                                    resizeMode={'contain'}
                                />
                            </View> */}
            </View>
          </View>
          <View style={[styles.toolbar, {backgroundColor: COLOR.WHITE}]}>
            <Text
              style={{
                fontFamily: FONTS.FAMILY_REGULAR,
                alignSelf: 'center',
                marginHorizontal: 10,
              }}>
              {trip.trip_name}
            </Text>
            <View style={{justifyContent: 'center', flexDirection: 'row'}}>
              <Text
                style={{
                  fontFamily: FONTS.FAMILY_REGULAR,
                  alignSelf: 'center',
                  marginHorizontal: 10,
                }}>
                TripID: {this.state.tripId}
              </Text>
            </View>
          </View>

          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
              width: widthPercentageToDP(80),
              alignSelf: 'center',
              marginTop: 10,
              marginBottom: 10,
            }}>
            <Image
              source={IMAGES.shield}
              style={{height: 20, width: 20}}
              resizeMode={'contain'}
            />
            <Image
              source={IMAGES.zig_zag}
              style={{height: 20, width: 20}}
              resizeMode={'contain'}
            />
            <Image
              source={IMAGES.info}
              style={{height: 20, width: 20}}
              resizeMode={'contain'}
            />
          </View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginBottom: 15,
              backgroundColor: COLOR.WHITE,
              paddingTop: 10,
              paddingBottom: 10,
            }}>
            <FlatList
              data={DAY}
              horizontal
              style={{marginHorizontal: 5}}
              renderItem={(item) =>
                // <TouchableOpacity
                //     style={styles.itemStyle}
                // >
                //     <Text style={styles.commomText}>{item.day}</Text>
                //     <Text style={styles.commomText}>{item.date}</Text>
                // </TouchableOpacity>
                this.dateView(item)
              }
            />
            <View
              style={{
                width: 30,
                height: 30,
                alignSelf: 'center',
                backgroundColor: COLOR.GREEN,
                borderTopLeftRadius: 15,
                borderBottomLeftRadius: 15,
                justifyContent: 'center',
              }}>
              <Image
                source={require('../../../styles/assets/plus.png')}
                style={{
                  width: 15,
                  height: 15,
                  alignSelf: 'center',
                }}
                resizeMode={'contain'}
              />
            </View>
          </View>
          <View style={{backgroundColor: '#EBEFF2', paddingVertical: 20}}>
            <Text
              style={[
                styles.timeText,
                {
                  alignSelf: 'flex-start',
                  marginLeft: 10,
                  marginBottom: 10,
                  fontSize: 18,
                },
              ]}>
              Thursday, 29 Mar 2021
            </Text>
            <this.flightView />
            <this.travelView travel={0} />
            <this.travelView travel={1} />
            <this.travelView travel={0} />
            <this.flightView />
          </View>
        </ScrollView>
        <ActionButton
          bgColor="#00000080"
          position={'right'}
          offsetX={25}
          backdrop={false}
          offsetY={25}
          spacing={10}
          size={44}
          
          buttonColor={COLOR.GREEN}
          >
          <ActionButton.Item
            position={'right'}
            size={44}
            buttonColor={COLOR.WHITE}
            textContainerStyle={{backgroundColor: '#00000000', borderWidth: 0}}
            textStyle={{color: COLOR.WHITE, fontSize: 16}}
            title="Add a Place"
            onPress={() => {
              this.props.navigation.navigate('AddPlace',{id:this.state.tripId})
              }}>
            <Image
              source={IMAGES.trip_plan}
              style={{height: 25, width: 25}}
              resizeMode={'contain'}
            />
          </ActionButton.Item>
          <ActionButton.Item
            position={'right'}
            size={44}
            buttonColor={COLOR.WHITE}
            textContainerStyle={{backgroundColor: '#00000000', borderWidth: 0}}
            textStyle={{color: COLOR.WHITE, fontSize: 16}}
            title="Add Reservation"
            onPress={() =>
              this.props.navigation.navigate('AddNewReservation', {
                tripId: this.state.tripId,
              })
            }>
            <Image
              source={IMAGES.ticket_reservation}
              style={{height: 30, width: 30}}
              resizeMode={'contain'}
            />
          </ActionButton.Item>
        </ActionButton>
      </SafeAreaView>
    );
  }

  flightView = (props) => {
    return (
      <View style={{flexDirection: 'row'}}>
        <View
          style={{width: 50, marginHorizontal: 20, justifyContent: 'center'}}>
          <Text style={styles.timeText}>09:00</Text>
          <Text style={styles.gmt}>GMT+3</Text>
          <View style={styles.smallDot} />
          <View style={styles.line} />
        </View>
        <View style={{marginTop: 10, flex: 1}}>
          <View style={styles.card}>
            <View style={styles.row}>
              <Image
                source={require('../../../styles/assets/plane2.png')}
                style={{
                  width: 40,
                  height: 40,
                  alignSelf: 'center',
                }}
                resizeMode={'contain'}
              />
              <Image
                source={require('../../../styles/assets/menuHorizontal.png')}
                style={{
                  width: 20,
                  height: 5,
                  marginHorizontal: 5,
                  alignSelf: 'center',
                }}
                resizeMode={'contain'}
              />
            </View>
            <View style={styles.row}>
              <View>
                <Text style={styles.textLarge}>DMM</Text>
                <Text style={styles.gmt}>02:55 pm</Text>
              </View>
              <View>
                <Text style={styles.textLarge}>DPS</Text>
                <Text style={styles.gmt}>12:55 pm</Text>
              </View>
            </View>
            {/* //styles.row, */}
            <View
              style={[
                {
                  marginTop: 10,
                  alignItems: 'center',
                  flex: 1,
                  flexDirection: 'row',
                  justifyContent: 'space-around',
                },
              ]}>
              <View style={styles.dot} />
              <Image
                resizeMode="contain"
                style={{flex: 1}}
                source={require('../../../styles/assets/horizontal_dotted_line.png')}
              />
              <Image
                source={require('../../../styles/assets/plane_horizontal.png')}
                style={{
                  width: 20,
                  height: 20,
                  marginHorizontal: 5,
                  alignSelf: 'center',
                }}
                resizeMode={'contain'}
              />
              <Image
                resizeMode="contain"
                style={{flex: 1}}
                source={require('../../../styles/assets/horizontal_dotted_line.png')}
              />
              <View style={styles.dot} />
            </View>
            <Text style={styles.gmt}>12 hr 30 min</Text>
            <View style={[Styles.line_view, {marginTop: 5}]} />
            <TouchableOpacity
              style={{
                marginHorizontal: 10,
                marginTop: 10,
                alignSelf: 'flex-end',
              }}>
              <Text style={{alignSelf: 'center', color: '#05778F'}}>
                Details {'>'}{' '}
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  };

  travelView = (props) => {
    return (
      <View>
        <View
          style={{width: 50, marginHorizontal: 20, justifyContent: 'center'}}>
          <View style={[styles.line, {height: 20}]} />
          <View
            style={{
              height: 30,
              flex: 1,
              flexDirection: 'row',
              alignItems: 'center',
              marginLeft: 10,
              width: widthPercentageToDP(90),
            }}>
            <View style={styles.iconBackground}>
              <Image
                source={
                  props.travel == 0
                    ? require('../../../styles/assets/car.png')
                    : require('../../../styles/assets/walk.png')
                }
                style={{
                  width: 20,
                  height: 20,
                  alignSelf: 'center',
                  tintColor: COLOR.WHITE,
                }}
              />
            </View>
            <ImageBackground
              style={{
                width: 65,
                height: 25,
                justifyContent: 'center',
              }}
              source={require('../../../styles/assets/timebackground.png')}>
              <Text style={[styles.gmt, {alignSelf: 'center'}]}>30 mins</Text>
            </ImageBackground>
            <Image
              resizeMode="contain"
              style={{flex: 1}}
              source={require('../../../styles/assets/horizontal_dotted_line.png')}
            />
          </View>
          {/*  */}
          {/* <ImageBackground style={{
                        width: 65,
                        height: 25,
                        justifyContent: 'center',
                        position: 'absolute',
                        left: 40,
                        top: 20
                    }}
                        source={require('../../../styles/assets/timebackground.png')}
                    >
                       
                    </ImageBackground> */}
          <View style={[styles.line, {height: 10}]} />
        </View>
        <View style={{flexDirection: 'row'}}>
          <View
            style={{width: 50, marginHorizontal: 20, justifyContent: 'center'}}>
            <Text style={styles.timeText}>09:00</Text>
            <Text style={styles.gmt}>GMT+3</Text>
            <View style={styles.smallDot} />
            <View style={styles.line} />
          </View>
          <View style={{marginTop: 10, flex: 1}}>
            <View style={styles.card}>
              <View style={styles.row}>
                <Text style={{fontWeight: '700', fontSize: 16}}>
                  Gemitir Garden
                </Text>
                <Image
                  source={require('../../../styles/assets/menuHorizontal.png')}
                  style={{
                    width: 20,
                    height: 5,
                    marginHorizontal: 5,
                    alignSelf: 'center',
                  }}
                  resizeMode={'contain'}
                />
              </View>
              <View style={{flexDirection: 'row', marginTop: 10}}>
                <Image
                  source={require('../../../styles/assets/pinpoint.png')}
                  style={{
                    width: 20,
                    height: 20,
                    marginRight: 10,
                  }}
                  resizeMode={'contain'}
                />
                <Text style={styles.gmt}>
                  Marigold, Badung Regency, Bali 80353, Indonesia
                </Text>
              </View>
              <View style={{flexDirection: 'row', marginTop: 10}}>
                <Image
                  source={require('../../../styles/assets/time.png')}
                  style={{
                    width: 20,
                    height: 20,
                    marginRight: 10,
                  }}
                  resizeMode={'contain'}
                />
                <Text style={styles.gmt}>30 min (10:15 GMT+7)</Text>
              </View>
              <View
                style={{
                  width: '100%',
                  backgroundColor: '#EBEFF2',
                  height: 30,
                  borderRadius: 15,
                  marginTop: 10,
                  flexDirection: 'row',
                  justifyContent: 'space-evenly',
                  alignItems: 'center',
                }}>
                <TouchableOpacity>
                  <Image
                    source={require('../../../styles/assets/call.png')}
                    style={{width: 20, height: 20, alignSelf: 'center'}}
                    resizeMode={'contain'}
                  />
                </TouchableOpacity>
                <TouchableOpacity>
                  <Image
                    source={require('../../../styles/assets/open_website.png')}
                    style={{width: 20, height: 20, alignSelf: 'center'}}
                    resizeMode={'contain'}
                  />
                </TouchableOpacity>

                <TouchableOpacity>
                  <Image
                    source={require('../../../styles/assets/help.png')}
                    style={{width: 20, height: 20, alignSelf: 'center'}}
                    resizeMode={'contain'}
                  />
                </TouchableOpacity>

                <TouchableOpacity>
                  <Image
                    source={require('../../../styles/assets/ticket.png')}
                    style={{width: 20, height: 20, alignSelf: 'center'}}
                    resizeMode={'contain'}
                  />
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>
      </View>
    );
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#EBEFF2',
  },
  toolbar: {
    marginHorizontal: 10,
    justifyContent: 'space-between',
    flexDirection: 'row',
    borderWidth: 0.5,
    borderRadius: 6,
    height: 30,
    borderColor: COLOR.BORDER,
    marginBottom: 5,
    backgroundColor: COLOR.GREEN,
  },
  itemStyle: {
    marginHorizontal: 3,
    borderColor: COLOR.BORDER,
    borderWidth: 1,
    width: 50,
    height: 60,
    borderRadius: 5,
    justifyContent: 'space-evenly',
  },
  commomText: {
    fontFamily: FONTS.FAMILY_REGULAR,
    alignSelf: 'center',
  },
  date: {
    fontSize: 18,
    marginHorizontal: 20,
    marginVertical: 20,
  },
  textLarge: {
    fontSize: 20,
    marginVertical: 5,
  },
  timeText: {
    alignSelf: 'center',
    fontFamily: FONTS.FAMILY_BOLD,
    fontSize: 16,
  },
  gmt: {
    alignSelf: 'center',
    fontFamily: FONTS.FAMILY_REGULAR,
    fontSize: 12,
    color: COLOR.TEXT_COLOR,
  },
  line: {
    width: 1,
    flex: 1,
    backgroundColor: COLOR.BLACK,
    alignSelf: 'center',
  },
  card: {
    marginTop: 5,
    flex: 1,
    marginHorizontal: 10,
    padding: 10,
    backgroundColor: COLOR.WHITE,
    borderRadius: 10,
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  dot: {
    width: 10,
    height: 10,
    borderRadius: 5,
    backgroundColor: COLOR.BORDER,
    borderWidth: 1,
    borderColor: COLOR.BLACK,
    alignSelf: 'center',
  },
  smallDot: {
    width: 10,
    height: 10,
    borderRadius: 5,
    alignSelf: 'center',
    backgroundColor: COLOR.BLACK,
  },
  iconBackground: {
    alignSelf: 'center',
    width: 30,
    height: 30,
    justifyContent: 'center',
    backgroundColor: '#465A64',
    borderRadius: 15,
  },
});
