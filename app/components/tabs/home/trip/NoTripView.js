import { ImageBackground, View, Image, Text } from 'react-native';
import React from 'react';

import IMAGES from '../../../styles/Images';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import COLOR from '../../../styles/Color';
import Styles from '../../../styles/Styles';
import FONTS from '../../../styles/Fonts';
import { TouchableOpacity } from 'react-native';
import { navigate } from '../../../routes/RootNavigation';

export const NoTripsView = (props) => (
  <View
    style={{
      width: wp(90),
      alignSelf: 'center',
      marginTop: 20,
      backgroundColor: COLOR.WHITE,
      elevation: 4,
      shadowColor: COLOR.VOILET,
      shadowOpacity: 0.2,
      shadowOffset: { width: 0.2, height: 0.2 },
      borderRadius: 20,
      alignItems: 'center',
    }}>
    <Image
      source={IMAGES.no_trip_bag}
      resizeMode={'contain'}
      style={{ marginVertical: 50, height: 110, overflow: 'hidden' }}
    />
    <Text
      style={[
        Styles.button_font,
        {
          width: null,
          alignSelf: 'center',
          marginBottom: 20,
          textAlign: 'center',
          fontSize: FONTS.MEDIUM,
          fontFamily: FONTS.FAMILY_BOLD,
          color: COLOR.BLACK,
        },
      ]}>
      You have no {props.title}
    </Text>
    <View
      style={{
        flexDirection: 'row',
        height: 25,
        justifyContent: 'space-between',
        width: wp(95),
        alignSelf: 'center',
      }}>
      <View
        style={{
          height: 25,
          width: 25,
          borderRadius: 12.5,
          backgroundColor: COLOR.BACKGROUND,
        }}
      />
      <View
        style={{
          height: 25,
          width: 25,
          borderRadius: 12.5,
          backgroundColor: COLOR.BACKGROUND,
        }}
      />
    </View>
    <View>
  {props.title == 'upcoming trips' ? <TouchableOpacity
        onPress={() =>{
          //  props.navigation.navigate('TripTimeline')
          props.navigation.navigate('AddNewTrip')
          }
      }
        style={[
          Styles.button_content,
          {
            backgroundColor: COLOR.WHITE,
            borderRadius: 10,
            marginTop: 0,
            marginBottom: 20,
            borderWidth: 1,
            borderColor: COLOR.LIGHT_TEXT,
            width: wp(80),
          },
        ]}>
        <Text style={[Styles.button_font, { color: COLOR.BLACK }]}>
          Add Your Trip Manually
        </Text>
      </TouchableOpacity> : null}
      <View
        style={{
          paddingHorizontal: 10,
          paddingVertical: 5,
          backgroundColor: COLOR.GRAY,
          borderRadius: 5,
          flexDirection: 'row',
          alignSelf: 'center',
          alignItems: 'center',
          justifyContent: 'center',
          marginBottom: 20,
        }}>
        <Image
          style={{
            height: 15,
            width: 15,
            marginRight: 10,
          }}
          resizeMode={'contain'}
          source={IMAGES.info}
        />
        <View>
          <Text
            style={[
              Styles.small_label,
              { width: null, alignSelf: 'flex-start' },
            ]}>
            For more information, visit{' '}
            <TouchableOpacity
            style={{marginBottom:-3}}
            onPress={()=>{
              props.navigation.navigate('PolicyPage',{screenTitle:'Terms and Condition'})
        //  navigate('PolicyPage',{screenTitle:'Terms and Condition'})
            }}
            >
            <Text
              style={[
                Styles.small_label,
                { width: null, color: COLOR.GREEN, alignSelf: 'flex-start' },
              ]}>
              Terms and Conditions
            </Text>
            </TouchableOpacity>
          </Text>
        </View>
      </View>
    </View>
  </View>
);

export const RoundButton = (props) => {
  return (
    <View
      style={{
        height: 50,
        width: 50,
        borderRadius: 25,
        shadowColor: COLOR.VOILET,
        shadowOpacity: 0.2,
        elevation: 3,
        shadowOffset: { width: 2, height: 2 },
        backgroundColor: props.color,
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      <Image source={props.image} style={{ height: 20, width: 20 }} />
    </View>
  );
};

export const SemicircleDashView = () => {
  return (
    <View
      style={{
        flexDirection: 'row',
        height: 25,
        justifyContent: 'space-between',
        width: wp(95),
        marginVertical: 0,
      }}>
      <View
        style={{
          height: 25,
          width: 25,
          borderRadius: 12.5,
          backgroundColor: COLOR.BACKGROUND,
        }}
      />
      <View
        style={{
          height: 25,
          width: 25,
          borderRadius: 12.5,
          backgroundColor: COLOR.BACKGROUND,
        }}
      />
    </View>
  );
};
