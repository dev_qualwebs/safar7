import React from 'react';
import {
  View,
  Image,
  StyleSheet,
  Text,
  SafeAreaView,
  ImageBackground,
} from 'react-native';
import {FlatList, TouchableOpacity} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import COLOR from '../../../styles/Color';
import FONTS from '../../../styles/Fonts';
import IMAGES from '../../../styles/Images';
import Styles from '../../../styles/Styles';
import ActionButton from 'react-native-action-button';
import {connect} from 'react-redux';
import TripAction from '../../../../redux/action/TripAction';
import PackageAction from '../../../../redux/action/PackageAction';
import {NoTripsView} from './NoTripView';
import { GOOGLE_IMAGE, GOOGLE_PLACES_API } from '../../../commonView/CommonView';

class PastTrips extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      tripData: [],
    };
  }

  componentDidMount() {
    this.props.getManualTrips(2);
  }

  componentWillReceiveProps(props) {
    if (props.pastTrips != this.props.pastTrips) {
      console.log('props.upcomingTrips', props.pastTrips);
      this.setState({
        tripData: props.pastTrips,
      });
    }
  }

  render() {
    return (
      <SafeAreaView
        style={[Styles.container, {backgroundColor: COLOR.BACKGROUND}]}>
        <View
          style={[
            Styles.container,
            {backgroundColor: COLOR.BACKGROUND, marginBottom: 10},
          ]}>
          <FlatList
            showsVerticalScrollIndicator={false}
            data={this.state.tripData}
            renderItem={({item}) => (
              <TouchableOpacity
                onPress={() =>
                  // this.state.packageTab
                  //   ? this.getSinglePackage(item.package.id)
                  //   : this.props.navigation.navigate('SingleTripScreen', {
                  //       item: item,
                  //     })

                  this.state.packageTab
                    ? this.getSinglePackage(item.package.id)
                    : // this.props.navigation.navigate('SingleTripScreen', {item: item})
                      this.props.navigation.navigate('AddNewTrip', {
                        data: {
                          trip_name: item.trip_name,
                          budget: item.budget,
                          checkInDate: item.trip_date_type.flexibility.depart,
                          checkOutDate: item.trip_date_type.flexibility.return,
                          id: item.id,
                          location_id : item.location_id,
                          location_name: item.location_name
                        },
                      })
                }>
                <View
                  style={{
                    width: wp(90),
                    alignSelf: 'center',
                    marginTop: 20,
                    borderRadius: 20,
                    shadowColor: COLOR.VOILET,
                    elevation: 3,
                    shadowOpacity: 0.2,
                    shadowOffset: {width: 2, height: 2},
                    backgroundColor: COLOR.WHITE,
                  }}>
                  <View style={{flex: 1, alignItems: 'center'}}>
                    <ImageBackground
                      // source={
                      //   this.state.packageTab
                      //     ? {uri: item.package.package_image}
                      //     : IMAGES.maldive_square
                      // }

                      source={
                        this.state.packageTab
                          ? {uri: item.package.package_image}
                          : item.location_id
                          ? {
                              uri:
                                GOOGLE_IMAGE +
                                item.location_id +
                                '&key=' +
                                GOOGLE_PLACES_API,
                            }
                          : IMAGES.maldive_square
                      }
                      imageStyle={{
                        borderTopLeftRadius: 20,
                        borderTopRightRadius: 20,
                      }}
                      style={{
                        marginTop: 0,
                        height: 120,
                        width: wp(90),
                        borderTopLeftRadius: 20,
                        borderTopRightRadius: 20,
                      }}>
                      {/* <View style={{position: 'absolute', right: 15, top: 20}}>
                        <TouchableOpacity>
                          <Image
                            source={require('../../../styles/assets/menu_horizontal.png')}
                            resizeMode={'contain'}
                            style={{width: 30, tintColor: COLOR.WHITE}}
                          />
                        </TouchableOpacity>
                      </View> */}
                    </ImageBackground>

                    <View
                      style={{
                        flexDirection: 'row',
                        width: wp(80),
                        justifyContent: 'space-between',
                      }}>
                      <Text
                        style={[
                          Styles.subheading_label,
                          {
                            width: null,
                            alignSelf: 'flex-start',
                            fontFamily: FONTS.FAMILY_BOLD,
                            marginTop: 10,
                          },
                        ]}>
                        {this.state.packageTab
                          ? item.package.package_name
                          : item.trip_name}
                      </Text>
                    </View>
                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        width: wp(80),
                      }}>
                      <Text
                        style={[
                          Styles.small_label,
                          {
                            width: null,
                            textAlign: 'left',
                            color: COLOR.VOILET,
                          },
                        ]}>
                        Trip No: {this.state.packageTab ? item.id : item.id}
                      </Text>

                      <Text
                        style={[
                          Styles.body_label,
                          {
                            width: null,
                            textAlign: 'right',
                            color: COLOR.BLACK,
                            fontFamily: FONTS.FAMILY_SEMIBOLD,
                          },
                        ]}>
                        {this.state.packageTab
                          ? item.package.price
                          : item.budget}{' '}
                        {this.state.packageTab
                          ? item.package.currency.iso4217_code
                          : item.currency.iso4217_code}
                      </Text>
                    </View>
                    <View
                      style={{
                        flexDirection: 'row',
                        height: 20,
                        justifyContent: 'space-between',
                        width: wp(95),
                        marginVertical: 0,
                      }}>
                      <View
                        style={{
                          height: 20,
                          width: 20,
                          borderRadius: 10,
                          backgroundColor: COLOR.BACKGROUND,
                        }}
                      />
                      <View
                        style={{
                          height: 20,
                          width: 20,
                          borderRadius: 10,
                          backgroundColor: COLOR.BACKGROUND,
                        }}
                      />
                    </View>
                    <View
                      style={{
                        flexDirection: 'row',
                        height: 50,
                        justifyContent: 'space-between',
                        width: wp(80),
                        alignItems: 'center',
                        marginBottom: 0,
                        borderRadius: 10,
                        borderWidth: 1,
                        borderColor: COLOR.GRAY,
                        paddingHorizontal: 10,
                      }}>
                      <Text
                        style={[
                          Styles.body_label,
                          {
                            width: null,
                            fontFamily: FONTS.FAMILY_SEMIBOLD,
                            textAlign: 'left',
                            color: COLOR.BLACK,
                          },
                        ]}>
                        {this.state.packageTab
                          ? '2020-06-01'
                          : item.trip_date_type.flexibility.depart}
                      </Text>
                      <Image
                        source={IMAGES.dot_back}
                        style={{height: 20, width: 20}}
                        resizeMode={'cover'}
                      />

                      <Text
                        style={[
                          Styles.body_label,
                          {
                            width: null,
                            fontFamily: FONTS.FAMILY_SEMIBOLD,
                            textAlign: 'right',
                            color: COLOR.BLACK,
                          },
                        ]}>
                        {this.state.packageTab
                          ? '2020-06-20'
                          : item.trip_date_type.flexibility.return}
                      </Text>
                    </View>
                    <TouchableOpacity
                      // onPress={() => this.state.packageTab ? this.getSinglePackage(item.package.id) : null}
                      onPress={() => {
                        this.state.packageTab
                          ? this.getSinglePackage(item.package.id)
                          : this.props.navigation.navigate('SingleTripScreen', {
                              item: item,
                            });
                      }}>
                      <View
                        style={{
                          flexDirection: 'row',
                          alignItems: 'center',
                          marginVertical: 10,
                        }}>
                        <Text
                          style={[
                            Styles.small_label,
                            {
                              color: COLOR.GREEN,
                              width: wp(75),
                              textAlign: 'right',
                              marginRight: 2,
                            },
                          ]}>
                          Details
                        </Text>
                        <Image
                          style={{
                            height: 10,
                            width: 10,
                            tintColor: COLOR.GREEN,
                          }}
                          resizeMode={'contain'}
                          source={IMAGES.right_arrow}
                        />
                      </View>
                    </TouchableOpacity>
                  </View>
                </View>
              </TouchableOpacity>
            )}
            numColumns={1}
            // ListEmptyComponent={() =>
            //   !this.state.tripData.length ? (
            //       <Text
            //           style={{
            //               textAlign: 'center',
            //               color: COLOR.LIGHT_TEXT,
            //               marginTop: 100,
            //           }}>
            //           No Past Trips Found
            //       </Text>
            //   ) : null
            // }
            // keyExtractor={(item, index) => index.toString()}
            keyExtractor={(item, index) => index.toString()}
            ListEmptyComponent={() => {
              return (
                <NoTripsView
                  navigation={this.props.navigation}
                  title={'past trips'}
                />
              );
            }}
          />
        </View>
      </SafeAreaView>
    );
  }
}

const style = StyleSheet.create({
  topBar: {
    height: 50,
    shadowColor: COLOR.VOILET,
    shadowOpacity: 0.2,
    elevation: 3,
    backgroundColor: COLOR.WHITE,
    flexDirection: 'row',
    shadowOffset: {width: 2, height: 2},
    justifyContent: 'space-between',
    width: wp(95),
    alignSelf: 'center',
    marginVertical: 20,

    alignItems: 'center',
  },
});

const mapStateToProps = (state, ownProps) => {
  return {
    pastTrips: state.tripReducer.pastTrips,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getManualTrips: (val) => {
      dispatch(TripAction.getManualTrips(val));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(PastTrips);
