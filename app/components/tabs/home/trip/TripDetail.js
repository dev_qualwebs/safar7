import React, {Component} from 'react';
import {SafeAreaView, Alert} from 'react-native';
import NavigationBar from '../../../commonView/NavigationBar';
import Styles from '../../../styles/Styles';
import {
  T_SCHEDULE_TRIP_TAB,
} from '../../../../helper/Constants';
import TopTabBar from './TabBarView';
import IMAGES from '../../../styles/Images';

export default class TripDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      index: 0,
      modalVisible: false,
    };

    this.hideModal = this.hideModal.bind(this);
  }

  hideModal = () => {
    // Alert.alert('working');
    // this.setState({modalVisible: !this.state.modalVisible});
  };

  render() {
    return (
      <SafeAreaView style={Styles.container}>
        <NavigationBar
          prop={this.props}
          right_image={IMAGES.notification_bell}
          isMainHeading={true}
          navHeight={45}
          name={this.props.route.params.packageTab ? 'Booked Packages':'Trips'}
        />
        <TopTabBar currentTab={T_SCHEDULE_TRIP_TAB} props={this.props} packageTab={this.props.route.params.packageTab} />
      </SafeAreaView>
    );
  }
}
