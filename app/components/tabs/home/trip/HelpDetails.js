import React from 'react';
import {Modal, Text, View, StyleSheet, Image, Alert} from 'react-native';
import {ScrollView, TextInput, TouchableOpacity} from 'react-native';
import {
  widthPercentageToDP,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import COLOR from '../../../../components/styles/Color';
import FONTS from '../../../../components/styles/Fonts';
import IMAGES from '../../../../components/styles/Images';
import Styles from '../../../../components/styles/Styles';
import HTMLView from 'react-native-htmlview';

export default class HelpDetails extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: true,
      name: props.name,
      image: props.image,
      id: props.countryId,
      data: props.paraData,
    };
  }

  componentDidMount(){
   
  }

//   source = `<p><a href="http://jsdf.co">&hearts; nice job!</a></p>`;

  modalBack = () => {
    this.setState({modalVisible: false}, (val) => {
      this.props.updateMasterState('showDetails', false);
    });
  };

  SingleLineView = () => (
    <View
      style={{
        borderBottomColor: COLOR.GRAY,
        marginVertical: 5,
        borderBottomWidth: 2,
        width: widthPercentageToDP(100),
        alignSelf: 'center',
      }}
    />
  );

  render() {
    return (
      <View style={Styles.container}>
        <Modal
          animationType={'slide'}
          transparent={true}
          style={{backgroundColor: COLOR.BLACK}}
          visible={this.state.modalVisible}
          onRequestClose={() => {
            console.log('Modal has been closed.');
          }}>
          <View style={styles.modal}>
            <View style={styles.modalBody}>
              <View
                style={{
                  height: 40,
                  width: widthPercentageToDP(100),
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                }}>
                <TouchableOpacity
                  style={{
                    height: 50,
                    width: 45,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}
                  onPress={() => {
                    {
                      this.modalBack();
                    }
                  }}>
                  <Image
                    style={{
                      height: 15,
                      width: 15,
                    }}
                    source={IMAGES.back_arrow}
                  />
                </TouchableOpacity>

                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                  <Image
                    style={{height: 30, width: 30}}
                    source={this.state.image}
                    resizeMode={'contain'}
                  />
                  <Text
                    style={[
                      {
                        marginLeft: 5,
                        fontFamily: FONTS.FAMILY_BOLD,
                        fontSize: FONTS.REGULAR,
                        alignSelf: 'center',
                        color: COLOR.TEXT_COLOR,
                      },
                    ]}>
                    {this.state.name}
                  </Text>
                </View>

                <TouchableOpacity
                  style={{
                    height: 50,
                    width: 45,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Image
                    style={{
                      height: 25,
                      width: 25,
                    }}
                    source={''}
                  />
                </TouchableOpacity>
              </View>
              <this.SingleLineView />
              <View style={{flex: 1,width:widthPercentageToDP(90),marginTop:10,marginBottom:20}}>
                  <HTMLView value={this.state.data} stylesheet={styles} />
              </View>
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  modal: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
  },
  modalBody: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    backgroundColor: COLOR.WHITE,
    borderWidth: 1,
    width: wp(100),
    marginTop: 50,
    borderColor: COLOR.BORDER,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    flex: 1,
  },
  image: {
    height: 120,
    width: 120,
    alignSelf: 'center',
    marginVertical: 20,
  },
  headingLabel: {
    fontFamily: FONTS.FAMILY_BOLD,
    fontSize: FONTS.MEDIUM,
    color: COLOR.BLACK,
  },
  singleLineView: {
    marginVertical: 20,
    borderBottomColor: COLOR.GRAY,
    width: wp(100),
    alignSelf: 'center',
    borderBottomWidth: 1,
  },
  borderView: {
    borderRadius: 10,
    borderWidth: 2,
    width: wp(90),
    borderColor: COLOR.GRAY,
    alignSelf: 'center',
  },
});
