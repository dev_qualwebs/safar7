import React from 'react';
import {
  Image,
  Text,
  TouchableOpacity,
  View,
  SafeAreaView,
  TextInput,
  KeyboardAvoidingView,
  FlatList,
  StyleSheet,
  InputAccessoryView,
  Keyboard,
  ImageBackground,
  Modal,
} from 'react-native';
import COLOR from '../../styles/Color';
import Styles from '../../styles/Styles';
import IMAGES from '../../styles/Images';
// import database from '@react-native-firebase/database';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';
import {connect} from 'react-redux';
import API from '../../../api/Api';
import FONTS from '../../styles/Fonts';
import ActionSheet from '../../commonView/ActionSheet';
import NavigationBar from '../../commonView/NavigationBar';
import ImagePicker from 'react-native-image-crop-picker';

// const reference = database().ref('/users/123');
const api = new API();

class AdminChatScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      openOption: false,
      messages: [
        {sender_id: 1, has_document: true},
        {sender_id: 2, has_document: true},
        {sender_id: 1, message: 'hello world'},
        {sender_id: 2, message: 'nice world'},
        {sender_id: 1, has_image: true},
        {sender_id: 2, has_image: true},
      ],
      userDetail: null,
      chatID: '',
      keyboardVisible: false,
      inputText: '',
      actionModalVisible: false,
    };

    this.inputRef = React.createRef();
    // this.getMessages = this.getMessages.bind(this);
  }

  componentDidMount() {
    // this.inputRef.focus();
    let value = {
      id: 1,
    };
    this.setState({userDetail: value});
    // if (this.props.route.params && this.props.route.params.userDetail) {
    //   this.setState({userDetail: this.props.route.params.userDetail}, () => {
    //     console.log(this.props.route.params.userDetail,'chat id')
    //   });

    //   if(this.props.route.params && this.props.route.params.chatID){
    //     this.setState({chatID:this.props.route.params.chatID},()=>{
    //       this.getMessages();
    //     })
    //   }
    // }
  }

  //  sendImages(image) {
  //     api
  //         .uploadImageFile({file: "data:image/jpeg;base64," + image})
  //         .then(res => {
  //             if (res.data.data && res.data.data.base64Upload) {
  //                 // this.uploadImage(res.data.data.base64Upload.location)
  //                 this.sendMessage(res.data.data.base64Upload.location);
  //             } else {
  //                 showAlertMessage(res.data.errors[0].message, 2);
  //             }
  //         })
  //         .catch(errors => {
  //             showAlertMessage(errors, 2);
  //             console.log('error is', errors);
  //         });
  //   }

  //   chooseImage = val => {
  //     let options = {
  //         title: Locale('Select Image'),
  //         compressImageMaxWidth: 1500,
  //         compressImageMaxHeight: 700,
  //         multiple: false,
  //         cropping: true,
  //         freeStyleCropEnabled: true,
  //         includeBase64:true,
  //         compressImageQuality: 0.75,
  //     };

  //     if (val == 1) {
  //         ImagePicker.openCamera({
  //           includeBase64:true,
  //             title: Locale('Select Image'),
  //             compressImageMaxWidth: 1500,
  //             compressImageMaxHeight: 700,
  //             cropping: true,
  //             freeStyleCropEnabled: true,
  //             compressImageQuality: 0.75,
  //         })
  //             .then(response => {
  //                 if (response.didCancel) {
  //                     console.log('User cancelled  Camera');
  //                 } else if (response.error) {
  //                     console.log('ImagePicker Error: ', response.error);
  //                 } else if (response.customButton) {
  //                     console.log('User tapped custom button: ', response.customButton);
  //                     showAlertMessage(response.customButton, 2);
  //                 } else {
  //                     this.setState({
  //                         isLoading: false,
  //                         actionModalVisible: false,
  //                     });
  //                     this.sendImages(response.data)
  //                 }
  //             })
  //             .catch(error => {
  //                 if (error.message) {
  //                     showAlertMessage(error.message, 2);
  //                 }
  //             });
  //     } else if (val == 2) {
  //         ImagePicker.openPicker(options)
  //             .then(response => {
  //                 console.log('image local reposnse', response);
  //                 if (response.didCancel) {
  //                     console.log('User cancelled image picker');
  //                 } else if (response.error) {
  //                     console.log('ImagePicker Error: ', response.error);
  //                 } else if (response.customButton) {
  //                     console.log('User tapped custom button: ', response.customButton);
  //                     alert(response.customButton);
  //                 } else {
  //                     this.setState({
  //                         isLoading: false,
  //                         actionModalVisible: false,
  //                     });
  //                     this.sendImages(response.data)
  //                 }
  //             })
  //             .catch(error => {
  //                 if (error.message) {
  //                     showAlertMessage(error.message);
  //                 }
  //             });
  //     }
  // };

  //Handling camera/gallery selection
  handleSheet = (val) => {
    if (val == 1) {
      this.chooseImage(1);
    } else if (val == 2) {
      this.chooseImage(2);
    } else if (val == 3) {
      this.setState({
        actionModalVisible: false,
      });
    }
  };

  chooseImage = (val) => {
    let options = {
      includeBase64: true,
      title: 'Select Image',
      width: 1630,
      height: 750,
      compressImageMaxWidth: 1630,
      compressImageMaxHeight: 750,
      multiple: true,
    };

    if (val == 1) {
      ImagePicker.openCamera({
        title: 'Select Image',
        width: 1024,
        height: 472,
        compressImageMaxWidth: 1024,
        compressImageMaxHeight: 472,
        includeBase64: true,
      }).then((response) => {});
    } else if (val == 2) {
      ImagePicker.openPicker(options).then((response) => {
        this.setState({actionModalVisible: false});
      });
    }
  };

  //   getMessages() {
  //     let data = {getThreadMessagesId: this.state.chatID};
  //     api
  //       .getThreadMessages(data)
  //       .then(res => {
  //         if (res.data.data && res.data.data.getThreadMessages) {
  //           this.addChatObserver(res.data.data.getThreadMessages.chat_thread_id);
  //           if (res.data.data.getThreadMessages.messages.length > 0) {
  //             this.setState(
  //               {messages: res.data.data.getThreadMessages.messages.reverse()},
  //               () => {
  //               },
  //             );
  //           }
  //         }
  //       })
  //       .catch(error => {});
  //   }
  NoActivityView() {
    return (
      <View
        style={[
          {
            borderRadius: 10,
            width: widthPercentageToDP(90),
            alignSelf: 'center',
            alignItems: 'center',
            justifyContent: 'center',
            padding: 15,
            marginTop: 80,
          },
        ]}>
        <Image
          style={{
            height: 72,
            width: 72,
            marginVertical: 20,
            alignSelf: 'center',
          }}
          resizeMode={'contain'}
          source={IMAGES.black_message}
        />
        <View style={{padding: 20, alignItems: 'center'}}>
          <Text
            style={{
              fontSize: 14,
              fontFamily: FONTS.FAMILY_REGULAR,
              textAlign: 'center',
            }}>
            Safar support representative will be happy to help you with anything
            you need
          </Text>
        </View>
      </View>
    );
  }

  addChatObserver = async (threadId) => {
    database()
      .ref(`chat_line/${threadId}`)
      .on('value', (snapshot) => {
        let that = this;
        let data = [];
        snapshot.forEach((childSnapshot, i) => {
          data.push(childSnapshot.val().message);
        });
        that.setState({
          messages: data.reverse(),
        });
      });
  };

  sendMessage = (image) => {
    let data = {
      data: {
        chat_thread_id: this.state.chatID,
        message: image == '' ? this.state.inputText : image,
        documents: image == '' ? [] : [image],
      },
    };
    api
      .sendMessage(data)
      .then((res) => {
        this.setState({inputText: ''});
        if (this.state.messages.length == 0) {
          this.getMessages();
        }
      })
      .catch((error) => {
        showAlertMessage('Error sending message', 2);
      });
  };

  LeftView = (props) => (
    <View style={{flexDirection: 'row', marginLeft: 10}}>
      <View
        style={{
          height: 40,
          width: 40,
          alignSelf: 'flex-end',
          marginRight: 5,
          backgroundColor: '#C8DCE9',
          borderRadius: 20,
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        <Image source={IMAGES.toyFace} style={{height: 35, width: 31}} />
      </View>
      <View
        style={{
          width: 0,
          backgroundColor: '#FFFFFF',
          borderTopColor: 'transparent',
          borderBottomColor: '#C8DCE9',
          borderLeftColor: 'transparent',
          borderRightColor: '#C8DCE9',
          borderTopWidth: 10,
          borderBottomWidth: 10,
          borderLeftWidth: 10,
          borderRightWidth: 15,
          marginLeft: 40,
          marginTop: props.item.has_image
            ? 218
            : props.item.has_document
            ? 79
            : 48,
          position: 'absolute',
        }}
      />
      <View
        style={[
          style.shadowView,
          {
            margin: 0,
            padding: 5,
            borderBottomLeftRadius: 0,
            width: null,
            alignSelf: 'flex-start',
            marginLeft: 10,
            backgroundColor: '#C8DCE9',
          },
        ]}>
        {props.item.has_image ? (
          <ImageBackground
            source={IMAGES.bali}
            style={{
              height: 200,
              width: 200,
              borderRadius: 4,
              marginBottom: 3,
              alignItems: 'flex-end',
              justifyContent: 'flex-end',
            }}>
            <View
              style={{
                backgroundColor: COLOR.BLACK,
                paddingHorizontal: 5,
                borderRadius: 5,
                height: 15,
                marginRight: 3,
                marginBottom: 3,
              }}>
              <Text
                style={[
                  {
                    textAlign: 'right',
                    fontFamily: FONTS.FAMILY_REGULAR,
                    fontSize: 10,
                    color: COLOR.WHITE,
                  },
                ]}>
                4:12
              </Text>
            </View>
          </ImageBackground>
        ) : props.item.has_document ? (
          <>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                marginTop: 5,
                marginLeft: 5,
              }}>
              <View
                style={{
                  alignItems: 'center',
                  justifyContent: 'center',
                  height: 42,
                  width: 42,
                  backgroundColor: COLOR.WHITE,
                  borderRadius: 4,
                }}>
                <Image
                  resizeMode={'contain'}
                  source={IMAGES.documentPicker}
                  style={{height: 24, width: 24, tintColor: '#465A64'}}
                />
              </View>
              <View style={{marginLeft: 20, marginRight: 30, marginBottom: 5}}>
                <Text style={{fontSize: 14, fontFamily: FONTS.FAMILY_BOLD}}>
                  Catalogue.pdf
                </Text>
                <Text>PDF • 2,8 MB</Text>
              </View>
            </View>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                alignSelf: 'flex-end',
              }}>
              <Text
                style={[
                  {
                    textAlign: 'right',
                    fontFamily: FONTS.FAMILY_REGULAR,
                    fontSize: FONTS.EXTRA_SMALL,
                  },
                ]}>
                4:13
              </Text>
            </View>
          </>
        ) : (
          <>
            <Text style={[Styles.medium_label, {textAlign: 'left'}]}>
              {props.item.message}
            </Text>
            <Text
              style={[
                {
                  textAlign: 'right',
                  fontFamily: FONTS.FAMILY_REGULAR,
                  fontSize: FONTS.EXTRA_SMALL,
                },
              ]}>
              4:12
            </Text>
          </>
        )}
      </View>
    </View>
  );

  RightView = (props) => (
    <>
      <View
        style={[
          style.shadow,
          style.shadowView,
          {
            margin: 0,
            padding: 5,
            borderBottomRightRadius: props.item.has_image ? 10 : 0,
            width: null,
            alignSelf: 'flex-end',
            marginRight: 28,
            backgroundColor: '#FFFFFF',
            borderColor: '#C4CDD2',
            borderWidth: 1,
          },
        ]}>
        {props.item.has_image ? (
          <ImageBackground
            source={IMAGES.maldive_square}
            style={{
              height: 200,
              width: 200,
              borderRadius: 4,
              marginBottom: 3,
              alignItems: 'flex-end',
              justifyContent: 'flex-end',
            }}>
            <View
              style={{
                backgroundColor: COLOR.BLACK,
                paddingHorizontal: 5,
                borderRadius: 5,
                height: 15,
                marginRight: 3,
                marginBottom: 3,
              }}>
              <Text
                style={[
                  {
                    textAlign: 'right',
                    fontFamily: FONTS.FAMILY_REGULAR,
                    fontSize: 10,
                    color: COLOR.WHITE,
                  },
                ]}>
                4:12
              </Text>
            </View>
          </ImageBackground>
        ) : props.item.has_document ? (
          <>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                marginTop: 5,
              }}>
              <View
                style={{
                  alignItems: 'center',
                  justifyContent: 'center',
                  height: 42,
                  width: 42,
                  backgroundColor: COLOR.WHITE,
                  borderRadius: 4,
                }}>
                <Image
                  resizeMode={'contain'}
                  source={IMAGES.documentPicker}
                  style={{height: 24, width: 24, tintColor: '#465A64'}}
                />
              </View>
              <View style={{marginLeft: 20, marginRight: 30, marginBottom: 5}}>
                <Text style={{fontSize: 14, fontFamily: FONTS.FAMILY_BOLD}}>
                  Catalogue.pdf
                </Text>
                <Text>PDF • 2,8 MB</Text>
              </View>
            </View>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                alignSelf: 'flex-end',
              }}>
              <Image
                source={IMAGES.seenChat}
                style={{height: 18, width: 20, tintColor: COLOR.GREEN}}
              />
              <Text
                style={[
                  {
                    textAlign: 'right',
                    fontFamily: FONTS.FAMILY_REGULAR,
                    fontSize: FONTS.EXTRA_SMALL,
                    marginLeft: 3,
                  },
                ]}>
                4:13
              </Text>
            </View>
          </>
        ) : (
          <>
            <Text style={[Styles.medium_label, {textAlign: 'right'}]}>
              {props.item.message}
            </Text>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                alignSelf: 'flex-end',
              }}>
              <Image
                source={IMAGES.seenChat}
                style={{height: 18, width: 20, tintColor: COLOR.GREEN}}
              />
              <Text
                style={[
                  {
                    textAlign: 'right',
                    fontFamily: FONTS.FAMILY_REGULAR,
                    fontSize: FONTS.EXTRA_SMALL,
                    marginLeft: 3,
                  },
                ]}>
                4:13
              </Text>
            </View>
          </>
        )}
      </View>
     {props.item.has_image ? null : <View
        style={{
          height: 16,
          width: 20,
          borderBottomColor: '#C4CDD2',
          borderBottomWidth: 1,
          backgroundColor:'#FFFFFF',
          position: 'absolute',
          right: 10,
        }}>
        <View
          style={{
            transform: [{rotate: '41deg'}],
            width: 26,
            height: 10,
            borderBottomColor: '#C4CDD2',
            borderBottomWidth: 1,
          }}
        />
      </View>}
    </>
  );

  render() {
    const {userDetail, messages, keyboardVisible, inputText} = this.state;
    return (
      <SafeAreaView style={{flex: 1}}>
        <KeyboardAvoidingView
          extraScrollHeight={100}
          behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
          style={{flex: 1}}>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
            }}>
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.goBack();
              }}>
              <Image
                source={IMAGES.back_arrow}
                style={{height: 15, width: 15, marginLeft: 20}}
              />
            </TouchableOpacity>

            <View>
              <Text
                style={[
                  {
                    textAlign: 'center',
                    marginTop: 5,
                    fontSize: 20,
                    fontFamily: FONTS.FAMILY_BOLD,
                  },
                ]}>
                Safar Support
              </Text>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  marginTop: 5,
                  alignSelf: 'center',
                }}>
                <Text
                  style={[
                    {
                      textAlign: 'center',

                      fontFamily: FONTS.FAMILY_REGULAR,
                      fontSize: FONTS.SMALL,
                    },
                  ]}>
                  Online
                </Text>
                <View
                  style={{
                    height: 8,
                    width: 8,
                    backgroundColor: COLOR.LIGHT_GREEN,
                    borderRadius: 4,
                    marginLeft: 5,
                  }}
                />
              </View>
            </View>
            <View
              style={{
                height: 40,
                width: 40,
                marginRight: 20,
                backgroundColor: '#C8DCE9',
                borderRadius: 20,
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <Image source={IMAGES.toyFace} style={{height: 35, width: 31}} />
            </View>
          </View>
          <View
            style={{
              backgroundColor: COLOR.LIGHT_TEXT_COLOR,
              height: 1,
              marginTop: 13,
            }}
          />

          <FlatList
            inverted={messages.length > 0 ? true : false}
            style={{marginBottom: 0, marginTop: 0, paddingVertical: 10}}
            bounceVertical={false}
            bounces={false}
            data={messages}
            renderItem={({item, index}) => (
              <>
                {userDetail &&
                  userDetail &&
                  userDetail.id == item.sender_id && (
                    <this.LeftView item={item} />
                  )}
                {userDetail &&
                  userDetail &&
                  userDetail.id != item.sender_id && (
                    <this.RightView item={item} />
                  )}
              </>
            )}
            showsVerticalScrollIndicator={false}
            showsHorizontalScrollIndicator={false}
            keyExtractor={(item, index) => index.toString()}
            ListEmptyComponent={this.NoActivityView()}
          />
          <View
            style={{
              backgroundColor: '#EBEFF2',
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <TouchableOpacity
              onPress={() => {
                this.setState({actionModalVisible: true});
              }}>
              <Image
                source={IMAGES.plus_image}
                style={{
                  height: 24,
                  width: 24,
                  marginRight: 15,
                  marginBottom: keyboardVisible ? 15 : 5,
                  marginLeft: 15,
                }}
                resizeMode={'contain'}
              />
            </TouchableOpacity>

            <View
              style={[
                {
                  marginTop: 15,
                  backgroundColor: COLOR.WHITE,
                  borderRadius: 20,
                  alignSelf: 'center',
                  paddingHorizontal: 15,
                  paddingVertical: 5,
                  flex: 1,
                  height: 40,
                  backgroundColor: COLOR.WHITE,
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'center',
                  marginBottom: keyboardVisible ? 30 : 10,
                },
              ]}>
              <View style={{flex: 1, marginRight: 10, height: 30}}>
                <TextInput
                  ref={(ref) => (this.inputRef = ref)}
                  placeholder={'Type your message'}
                  style={Styles.medium_label}
                  placeholderTextColor={'#848484'}
                  value={this.state.inputText}
                  onChangeText={(text) => this.setState({inputText: text})}
                  multiline={true}
                  inputAccessoryViewID={'uniqueID'}
                  underlineColorAndroid="transparent"
                  onFocus={() => this.setState({keyboardVisible: true})}
                  onBlur={() => this.setState({keyboardVisible: false})}
                />
                <InputAccessoryView nativeID={'uniqueID'}>
                  <View
                    style={{
                      alignItems: 'flex-end',
                      backgroundColor: COLOR.WHITE,
                    }}>
                    <TouchableOpacity
                      onPress={() => {
                        Keyboard.dismiss();
                      }}>
                      <View
                        style={{
                          height: 30,
                          width: 60,
                          alignItems: 'center',
                          justifyContent: 'center',
                        }}>
                        <Text
                          style={[
                            Styles.medium_label,
                            {color: '#0E86D4', fontWeight: 'bold'},
                          ]}>
                          Done
                        </Text>
                      </View>
                    </TouchableOpacity>
                  </View>
                </InputAccessoryView>
              </View>
            </View>
            <TouchableOpacity onPress={() => this.handleSheet(1)}>
              <Image
                source={IMAGES.cameraChat}
                style={{
                  height: 24,
                  width: 24,
                  tintColor: COLOR.GREEN,
                  marginBottom: keyboardVisible ? 15 : 5,
                  marginLeft: 15,
                }}
                resizeMode={'contain'}
              />
            </TouchableOpacity>
            <TouchableOpacity>
              <Image
                source={IMAGES.chatMic}
                style={{
                  height: 24,
                  width: 24,
                  marginRight: 15,
                  tintColor: COLOR.GREEN,
                  marginBottom: keyboardVisible ? 15 : 5,
                  marginLeft: 15,
                }}
                resizeMode={'contain'}
              />
            </TouchableOpacity>
          </View>
        </KeyboardAvoidingView>
        <Modal
          animationType={'slide'}
          transparent={true}
          style={{backgroundColor: COLOR.BLACK}}
          visible={this.state.actionModalVisible}
          onRequestClose={() => {
            console.log('Modal has been closed.');
          }}>
          <View style={style.modal}>
            <View style={style.modalBody}>
              <NavigationBar
                handleBack={() => {
                  this.setState({actionModalVisible: false});
                }}
                prop={this.props}
                navHeight={40}
                name={this.state.title}
              />
              <View
                style={{
                  width: widthPercentageToDP(90),
                  alignSelf: 'center',
                  marginTop: 20,
                  flex: 1,
                }}>
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'space-around',
                  }}>
                  <TouchableOpacity
                    onPress={() => {
                      this.handleSheet(2);
                    }}>
                    <View
                      style={{
                        height: 70,
                        width: 70,
                        alignItems: 'center',
                        justifyContent: 'center',
                        borderColor: COLOR.LIGHT_TEXT_COLOR,
                        borderWidth: 1,
                        borderRadius: 4,
                      }}>
                      <Image
                        resizeMode={'contain'}
                        source={IMAGES.galleryPicker}
                        style={{height: 24, width: 24}}
                      />
                    </View>
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={() => {
                      this.handleSheet(2);
                    }}>
                    <View
                      style={{
                        height: 70,
                        width: 70,
                        alignItems: 'center',
                        justifyContent: 'center',
                        borderColor: COLOR.LIGHT_TEXT_COLOR,
                        borderWidth: 1,
                        borderRadius: 4,
                      }}>
                      <Image
                        resizeMode={'contain'}
                        source={IMAGES.videoPicker}
                        style={{height: 24, width: 24, tintColor: COLOR.GREEN}}
                      />
                    </View>
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={() => {
                      this.handleSheet(2);
                    }}>
                    <View
                      style={{
                        height: 70,
                        width: 70,
                        alignItems: 'center',
                        justifyContent: 'center',
                        borderColor: COLOR.LIGHT_TEXT_COLOR,
                        borderWidth: 1,
                        borderRadius: 4,
                      }}>
                      <Image
                        resizeMode={'contain'}
                        source={IMAGES.documentPicker}
                        style={{height: 24, width: 24}}
                      />
                    </View>
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={() => {
                      // this.handleSheet(2);
                    }}>
                    <View
                      style={{
                        height: 70,
                        width: 70,
                        alignItems: 'center',
                        justifyContent: 'center',
                        borderColor: COLOR.LIGHT_TEXT_COLOR,
                        borderWidth: 1,
                        borderRadius: 4,
                      }}>
                      <Image
                        resizeMode={'contain'}
                        source={IMAGES.locationPicker}
                        style={{height: 24, width: 24}}
                      />
                    </View>
                  </TouchableOpacity>
                </View>

                <TouchableOpacity
                  style={{
                    width: widthPercentageToDP(90),
                    borderRadius: 5,
                    borderColor: COLOR.VOILET,
                    borderWidth: 1,
                    height: 50,
                    marginTop: 25,
                  }}
                  onPress={() => {
                    this.handleSheet(3);
                  }}>
                  <View
                    style={{
                      flex: 1,
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}>
                    <Text>Cancel</Text>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </Modal>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    // userInfo: state.authReducer.userInfo,
  };
};

export default connect(mapStateToProps, null)(AdminChatScreen);

const style = StyleSheet.create({
  shadow: {
    shadowRadius: 5,
    shadowColor: '#000',
    shadowOpacity: 0.09,
  },
  shadowView: {
    marginTop: 15,
    backgroundColor: COLOR.WHITE,
    borderRadius: 10,
    width: widthPercentageToDP(90),
    alignSelf: 'center',
    paddingHorizontal: 15,
    paddingVertical: 10,
  },
  modal: {
    flex: 1,
    height: heightPercentageToDP(35),
    alignItems: 'center',
    justifyContent: 'flex-end',
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
  },
  modalBody: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    backgroundColor: COLOR.WHITE,
    borderWidth: 1,
    width: widthPercentageToDP(100),
    borderColor: COLOR.BORDER,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    marginTop: heightPercentageToDP(100) - 260,
    flex: 1,
  },
  image: {
    height: 120,
    width: 120,
    alignSelf: 'center',
    marginVertical: 20,
  },
  headingLabel: {
    fontFamily: FONTS.FAMILY_BOLD,
    fontSize: FONTS.MEDIUM,
    color: COLOR.BLACK,
  },
  singleLineView: {
    marginVertical: 20,
    borderBottomColor: COLOR.GRAY,
    width: widthPercentageToDP(100),
    alignSelf: 'center',
    borderBottomWidth: 1,
  },
});
