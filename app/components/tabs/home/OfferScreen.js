import React from 'react';
import {Modal, Text, View, StyleSheet, Image, Button} from 'react-native';
import {
  FlatList,
  TouchableOpacity,
  TouchableWithoutFeedback,
} from 'react-native';
import {
  heightPercentageToDP,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import COLOR from '../../../components/styles/Color';
import FONTS from '../../../components/styles/Fonts';
import IMAGES from '../../../components/styles/Images';
import Styles from '../../../components/styles/Styles';
import NavigationBar from '../../commonView/NavigationBar';
import {D_TRAIN_CLASS} from '../../../helper/Constants';

export default class OfferScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: true,
    };
  }

  updateMasterState = (attrName, value) => {
    this.setState({[attrName]: value});
  };

  render() {
    return (
      <View style={Styles.container}>
        <Modal
          animationType={'slide'}
          transparent={true}
          style={{backgroundColor: COLOR.BLACK}}
          visible={this.state.modalVisible}
          onRequestClose={() => {
            console.log('Modal has been closed.');
          }}>
          <View style={styles.modal}>
            <View style={styles.modalBody}>
              <NavigationBar prop={this.props} navHeight={40} name={'Offer'} />
              <Image
                style={{width: wp(100), height: heightPercentageToDP(20)}}
                source={IMAGES.offer_image}
              />
              <View
                style={{
                  flex: 1,
                  width: wp(90),
                  alignSelf: 'center',
                  paddingVertical: 10,
                }}>
                <Text
                  style={[
                    Styles.button_font,
                    {
                      color: COLOR.BLACK,
                      alignSelf: 'flex-start',
                      fontFamily: FONTS.FAMILY_BOLD,
                      fontSize: 20,
                    },
                  ]}>
                  90% Discount
                </Text>
                <Text style={Styles.small_label}>26-31 Oct</Text>
                <View style={[Styles.line_view, {marginVertical: 20}]} />
                <Text
                  style={[
                    Styles.body_label,
                    {
                      color: COLOR.BLACK,
                      alignSelf: 'flex-start',
                      fontFamily: FONTS.FAMILY_BOLD,
                      marginBottom: 10,
                    },
                  ]}>
                  About the offer
                </Text>
                <Text style={Styles.small_label}>
                  Take the advantage of this offer by requesting a trip plan
                  without any fees!
                </Text>
                <View style={[Styles.line_view, {marginVertical: 20}]} />
               <TouchableOpacity
               onPress={()=>{
                this.props.navigation.navigate('PolicyPage',{screenTitle:'Terms and Condition'})
               }}
               >
                <Text
                  style={[
                    Styles.body_label,
                    {
                      color: COLOR.BLACK,
                      alignSelf: 'flex-start',
                      fontFamily: FONTS.FAMILY_BOLD,
                      marginBottom: 10,
                      marginBottom:-3,
                    },
                  ]}>
                  Terms & Conditions
                </Text>
                </TouchableOpacity>
                <View>
                  <View style={{flexDirection: 'row', marginBottom: 5}}>
                    <View style={styles.circle_view} />
                    <Text
                      style={[
                        Styles.small_label,
                        {width: null, alignSelf: 'flex-start'},
                      ]}>
                      Faucibus turpis at ac pretium, ut. Varius pretium, justo,
                      aenean sed nisl in maecenas.
                    </Text>
                  </View>
                  <View style={{flexDirection: 'row', marginBottom: 5}}>
                    <View style={styles.circle_view} />
                    <Text
                      style={[
                        Styles.small_label,
                        {width: null, alignSelf: 'flex-start'},
                      ]}>
                      Aliquet dictumst in non massa. Ultrices nisi, congue
                      semper amet diam posuere diam.
                    </Text>
                  </View>
                  <View style={{flexDirection: 'row', marginBottom: 5}}>
                    <View style={styles.circle_view} />
                    <Text
                      style={[
                        Styles.small_label,
                        {width: null, alignSelf: 'flex-start'},
                      ]}>
                      Consectetur amet placerat id at semper. Mi dignissim
                      pharetra, metus massa suspendisse ut arcu.
                    </Text>
                  </View>
                  <View style={{flexDirection: 'row', marginBottom: 5}}>
                    <View style={styles.circle_view} />
                    <Text
                      style={[
                        Styles.small_label,
                        {width: null, alignSelf: 'flex-start'},
                      ]}>
                      Viverra molestie non amet, dolor nunc vestibulum. Morbi
                      nisi, rhoncus ornare tempor, praesent sit pretium lectus.
                    </Text>
                  </View>
                  <View style={{flexDirection: 'row', marginBottom: 5}}>
                    <View style={styles.circle_view} />
                    <Text
                      style={[
                        Styles.small_label,
                        {width: null, alignSelf: 'flex-start'},
                      ]}>
                      Velit vivamus morbi sit fames nec adipiscing. Elit, diam
                      molestie nulla ornare.
                    </Text>
                  </View>
                </View>
              </View>
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  modal: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
  },
  modalBody: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    backgroundColor: COLOR.WHITE,
    borderWidth: 1,
    width: wp(100),
    marginTop: 50,
    borderColor: COLOR.BORDER,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    flex: 1,
  },
  image: {
    height: 120,
    width: 120,
    alignSelf: 'center',
    marginVertical: 20,
  },
  headingLabel: {
    fontFamily: FONTS.FAMILY_BOLD,
    fontSize: FONTS.MEDIUM,
    color: COLOR.BLACK,
  },
  singleLineView: {
    marginVertical: 20,
    borderBottomColor: COLOR.GRAY,
    width: wp(100),
    alignSelf: 'center',
    borderBottomWidth: 1,
  },
  borderView: {
    borderRadius: 10,
    borderWidth: 3,
    width: wp(90),
    borderColor: COLOR.GRAY,
    alignSelf: 'center',
  },
  circle_view: {
    width: 10,
    height: 10,
    borderRadius: 5,
    backgroundColor: COLOR.GREEN,
    marginRight: 10,
    marginTop: 5,
  },
});
