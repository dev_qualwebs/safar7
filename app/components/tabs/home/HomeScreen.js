import React from 'react';
import {
  View,
  Text,
  SafeAreaView,
  Image,
  StyleSheet,
  Alert,
  TouchableWithoutFeedback,
} from 'react-native';
import {FlatList, ScrollView, TouchableOpacity} from 'react-native';
import IMAGES from '../../styles/Images';
import Styles from '../../styles/Styles';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import COLOR from '../../styles/Color';
import NavigationBar from '../../commonView/NavigationBar';
import FONTS from '../../styles/Fonts';
import {connect} from 'react-redux';
import PackageAction from '../../../redux/action/PackageAction';
import FastImage from 'react-native-fast-image';
import AlertModal from '../../../models/AlertModal';
import {Platform} from 'react-native';
import {
  check,
  request,
  PERMISSIONS,
  RESULTS,
  openSettings,
  checkNotifications,
} from 'react-native-permissions';
import NotificationModal from '../../../models/notification/NotificationModal';

class HomeScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      tripData: [],
      offersData: [],
      showAlertModal: false,
      showNotificationModal: false,
      showMoreButton: true,
    };

    this.scrollRef = React.createRef();
  }

  componentDidMount() {
    this.props.getAllPackages();
    this.props.getAllOffers();
    if (this.props.allPackageData) {
      this.setState({
        tripData: this.props.allPackageData,
      });
    }

    if (this.props.allOffersData) {
      this.setState({
        offersData: this.props.allOffersData,
      });
    }

    this.checkPermissions();
  }

  updateMasterState = (attrName, value) => {
    this.setState({[attrName]: value});
  };

  checkPermissions() {
    check(PERMISSIONS.IOS.LOCATION_WHEN_IN_USE)
      .then((result) => {
        switch (result) {
          case RESULTS.UNAVAILABLE:
            console.log(
              'This feature is not available (on this device / in this context)',
            );
            break;
          case RESULTS.DENIED:
            // this.setState({showAlertModal: true});
            //console.log('The permission has not been requested / is denied but requestable');
            break;
          case RESULTS.LIMITED:
            console.log('The permission is limited: some actions are possible');
            break;
          case RESULTS.GRANTED:
            console.log('The permission is granted');
            break;
          case RESULTS.BLOCKED:
            console.log('The permission is denied and not requestable anymore');
            break;
        }
      })
      .catch((error) => {
        // …
      });

    check(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION)
      .then((result) => {
        switch (result) {
          case RESULTS.UNAVAILABLE:
            console.log(
              'This feature is not available (on this device / in this context)',
            );
            break;
          case RESULTS.DENIED:
            // this.setState({showAlertModal: true});
            console.log(
              'The permission has not been requested / is denied but requestable',
            );
            break;
          case RESULTS.LIMITED:
            console.log('The permission is limited: some actions are possible');
            break;
          case RESULTS.GRANTED:
            console.log('The permission is granted');
            break;
          case RESULTS.BLOCKED:
            console.log('The permission is denied and not requestable anymore');
            break;
        }
      })
      .catch((error) => {
        // …
      });
  }

  checkNotificationPermission = () => {
    checkNotifications().then(({status, settings}) => {
      switch (status) {
        case RESULTS.UNAVAILABLE:
          console.log(
            'This feature is not available (on this device / in this context)',
          );
          break;
        case RESULTS.DENIED:
          this.setState({showNotificationModal: true});
          //console.log('The permission has not been requested / is denied but requestable');
          break;
        case RESULTS.LIMITED:
          console.log('The permission is limited: some actions are possible');
          break;
        case RESULTS.GRANTED:
          console.log('The permission is granted');
          break;
        case RESULTS.BLOCKED:
          console.log('The permission is denied and not requestable anymore');
          break;
      }
    });
  };

  componentWillReceiveProps(props) {
    if (props.allPackageData != this.props.allPackageData) {
      this.setState({
        tripData: props.allPackageData,
      });
    }

    if (props.allOffersData != this.props.allOffersData) {
      this.setState({
        offersData: props.allOffersData,
      });
    }
  }

  commonView = (props) => {
    return (
      <TouchableOpacity
        onPress={() => {
          this.props.navigation.navigate('SinglePackageDetail', {
            data: props.item,
          });
        }}
        style={classStyle.flatlist_image_view}>
        <FastImage
          source={{
            uri: props.item.package_image,
            priority: FastImage.priority.normal,
          }}
          resizeMode={FastImage.resizeMode.cover}
          style={[
            [Styles.backgroundImage, {marginRight: 10, borderRadius: 15}],
          ]}>
          <View
            style={{
              marginTop: 0,
              flexDirection: 'row',
              justifyContent: 'space-between',
            }}>
            <View
              style={{
                width: 90,
                height: 25,
                backgroundColor: COLOR.RED,
                borderTopLeftRadius: 15,
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <Text
                style={[Styles.small_label, {color: COLOR.WHITE, width: null}]}>
                Only {props.item.availability} Left
              </Text>
            </View>
            <View
              style={{
                width: 90,
                height: 50,
                backgroundColor: COLOR.GREEN,
                borderTopRightRadius: 15,
                borderBottomLeftRadius: 15,
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <Text
                style={[
                  Styles.small_label,
                  {color: COLOR.WHITE, width: null, textAlign: 'center'},
                ]}>
                <Text
                  style={[
                    Styles.body_label,
                    {
                      fontFamily: FONTS.FAMILY_BOLD,
                      color: COLOR.WHITE,
                      fontSize: 22,
                    },
                  ]}>
                  {parseInt(props.item.price)}
                </Text>{' '}
                SAR{'\n'}/1 person
              </Text>
            </View>
          </View>
          <View
            style={{
              marginHorizontal: 0,
              bottom: 0,
              position: 'absolute',
            }}>
            <Text
              style={[
                {
                  color: COLOR.WHITE,
                  fontFamily: FONTS.FAMILY_EXTRA_BOLD,
                  fontSize: 30,
                  width: null,
                  marginHorizontal: 10,
                },
              ]}>
              {props.item.package_name}
            </Text>
            <View style={{flexDirection: 'row', margin: 10}}>
              <Text
                style={[
                  Styles.body_label,
                  {
                    borderRadius: 12.5,
                    height: 25,
                    borderWidth: 1,
                    width: null,
                    borderColor: COLOR.WHITE,
                    color: COLOR.WHITE,
                    paddingHorizontal: 10,
                    alignItems: 'center',
                  },
                ]}>
                {props.item.duration} days
              </Text>
              <Text
                style={[
                  Styles.body_label,
                  {
                    height: 25,
                    width: null,
                    color: COLOR.WHITE,
                    alignItems: 'center',
                    marginLeft: 10,
                  },
                ]}>
                {props.item.period_of_travel}
              </Text>
            </View>
            <ScrollView style={{flex: 1}}>
              <View
                style={{
                  alignSelf: 'center',
                  alignItems: 'center',
                  // backgroundColor: '#ffffff90',
                  margin: 0,
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  paddingVertical: 5,
                }}>
                {props.item.package_exclude_include &&
                  props.item.package_exclude_include.package_services &&
                  props.item.package_exclude_include.package_services.service.package_travelling_modes.map(
                    (item) => {
                      return (
                        <View
                          style={{
                            marginHorizontal: 10,
                            alignItems: 'center',
                            justifyContent: 'center',
                            marginVertical: 0,
                            flex: 1,
                          }}>
                          <FastImage
                            style={{
                              height: 35,
                              width: 35,
                              marginBottom: 10,
                              alignSelf: 'center',
                              overflow: 'hidden',
                            }}
                            source={{
                              uri: item.mode.image,
                              priority: FastImage.priority.normal,
                            }}
                            resizeMode={FastImage.resizeMode.contain}
                          />
                          <Text
                            style={{
                              textAlign: 'center',
                              color: COLOR.WHITE,
                              fontSize: 12,
                              marginBottom: 0,
                            }}>
                            {item.mode.mode}
                          </Text>
                        </View>
                      );
                    },
                  )}
              </View>
            </ScrollView>
          </View>
        </FastImage>
      </TouchableOpacity>
    );
  };

  render() {
    let {tripData} = this.state;
    return (
      <View style={Styles.container}>
        <SafeAreaView style={Styles.container}>
          <NavigationBar
            prop={this.props}
            isMainHeading={true}
            right_image={IMAGES.notification_bell}
            navHeight={45}
            name={'Explore'}
          />
          <ScrollView
            bounces={false}
            showsVerticalScrollIndicator={false}
            contentContainerStyle={{flexGrow: 1}}
            ref={this.scrollRef}
            onScroll={(event) => {
              console.log(event.nativeEvent.contentOffset.y);
              if (event.nativeEvent.contentOffset.y < 15) {
                this.setState({showMoreButton: true});
              } else {
                this.setState({showMoreButton: false});
              }
            }}
            scrollEventThrottle={16}>
            <View>
              <View style={Styles.mainView}>
                <Text
                  style={[
                    Styles.small_label,
                    {
                      marginVertical: 20,
                      color: '#273238',
                      fontFamily: FONTS.FAMILY_BOLD,
                    },
                  ]}>
                  SAFAR7 PACKAGES
                </Text>
              </View>
              <FlatList
              bounces={false}
                style={classStyle.flatlist_view}
                data={tripData}
                renderItem={({item, index}) =>
                  index == tripData.length - 1 ? (
                    <TouchableOpacity
                      style={{justifyContent: 'center', alignItems: 'center'}}
                      onPress={() => {
                        this.props.navigation.navigate('AllPackages');
                      }}>
                      <View
                        style={{
                          marginRight: 20,
                          alignItems: 'center',
                          justifyContent: 'center',
                          width: 120,
                        }}>
                        <Image
                          style={{height: 120, width: 120, marginBottom: -45}}
                          resizeMode={'contain'}
                          source={IMAGES.menu_circle}
                        />
                        <Text
                          style={[
                            Styles.small_label,
                            {color: COLOR.GREEN, width: null},
                          ]}>
                          Show All Packages
                        </Text>
                      </View>
                    </TouchableOpacity>
                  ) : (
                    <this.commonView item={item} />
                  )
                }
                horizontal={true}
                showsVerticalScrollIndicator={false}
                showsHorizontalScrollIndicator={false}
                keyExtractor={(item, index) => index.toString()}
              />

              <View
                style={[Styles.mainView, {marginTop: 20, marginBottom: 20}]}>
                <Text
                  style={[
                    Styles.small_label,
                    {color: '#273238', fontFamily: FONTS.FAMILY_BOLD},
                  ]}>
                  OFFERS
                </Text>
              </View>

              <FlatList
                style={[
                  classStyle.flatlist_view,
                  {height: null, width: wp(100), alignSelf: 'center'},
                ]}
                data={this.state.offersData}
                renderItem={({item}) => (
                  <TouchableOpacity
                    onPress={() =>
                      this.props.navigation.navigate('DiscountScreen', {
                        data: item,
                      })
                    }
                    style={classStyle.offer_view}>
                    <View
                      style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                      }}>
                      <Image
                        source={
                          item.image
                            ? {uri: item.image}
                            : IMAGES.offer_image_green
                        }
                        resizeMode={'contain'}
                        style={{
                          width: 120,
                          marginRight: 15,
                          height: 120,
                          borderRadius: 10,
                        }}
                      />
                      <View style={{flex: 1}}>
                        <Text
                          style={[
                            Styles.body_label,
                            {
                              fontFamily: FONTS.FAMILY_BOLD,
                              width: null,
                              alignSelf: 'flex-start',
                            },
                          ]}>
                          {item.percentage ? item.percentage : ''}% discount
                        </Text>
                        <Text
                          style={[
                            Styles.small_label,
                            {color: COLOR.VOILET, width: null,alignSelf: 'flex-start'}
                          ]}>
                          {item.description ? item.description : ''}
                        </Text>
                      </View>
                    </View>
                  </TouchableOpacity>
                )}
                numColumns={1}
                showsVerticalScrollIndicator={false}
                showsHorizontalScrollIndicator={false}
                keyExtractor={(item, index) => index.toString()}
              />
            </View>
          </ScrollView>
          {tripData.length > 0 && this.state.showMoreButton && (
            <TouchableWithoutFeedback
              onPress={async () => {
                this.scrollRef.current.scrollToEnd({animated: true});
              }}>
              <Image
                style={{
                  width: 70,
                  height: 50,
                  bottom: 0,
                  margin: 10,
                  alignSelf: 'center',
                  position: 'absolute',
                }}
                source={IMAGES.more_swipe}
                resizeMode={'contain'}
              />
            </TouchableWithoutFeedback>
          )}
        </SafeAreaView>
        {this.state.showAlertModal ? (
          <AlertModal
            updateMasterState={this.updateMasterState}
            checkNotificationPermission={this.checkNotificationPermission}
          />
        ) : null}
        {this.state.showNotificationModal ? (
          <NotificationModal updateMasterState={this.updateMasterState} />
        ) : null}
      </View>
    );
  }
}

const classStyle = StyleSheet.create({
  flatlist_view: {
    marginVertical: 0,
    width: wp(100),

    marginHorizontal: 15,
  },
  flatlist_image_view: {
    height: 420,
    // hp(50)
    width: wp(80),
    marginRight: 10,
  },
  offer_view: {
    marginVertical: 0,
    width: wp(90),
    marginBottom: 35,
    height: 100,
    alignSelf: 'center',
  },
});

const mapStateToProps = (state, ownProps) => {
  return {
    allPackageData: state.pacakgeReducer.allPackageData,
    allOffersData: state.pacakgeReducer.allOffersData,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getAllPackages: (val) => {
      dispatch(PackageAction.getAllPackages(val));
    },
    getAllOffers: (val) => {
      dispatch(PackageAction.getAllOffers(val));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);
