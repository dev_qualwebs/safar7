import {useNavigation} from '@react-navigation/native';
import React from 'react';
import {StyleSheet, Image, View, Text, TouchableOpacity} from 'react-native';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';
import COLOR from '../../styles/Color';
import FONTS from '../../styles/Fonts';

export const PaymentCompleteScreen = () => {
  const navigation = useNavigation();
  return (
    <View style={styles.container}>
      <Image
        source={require('../../styles/assets/in_pool.png')}
        style={{
          width: widthPercentageToDP(100),
          height: heightPercentageToDP(50),
        }}
        resizeMode={'contain'}
      />
      <Text style={styles.title}>Your Trip Plan is Almost Ready!</Text>
      <Text style={styles.message}>
        Our Team will plan you the best trip plan of the selected options. We
        will get back to you in 24 - 48 hours for final trip plan.
      </Text>
      <TouchableOpacity
        onPress={() => navigation.navigate('HomeStack')}
        style={styles.button}>
        <Text style={styles.textColor}>Great!</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: COLOR.WHITE,
    flex: 1,
  },
  title: {
    fontSize: 28,
    fontFamily: FONTS.FAMILY_BOLD,
    alignSelf: 'center',
    marginTop: 30,
    marginHorizontal: 50,
    textAlign: 'center',
  },
  message: {
    fontFamily: FONTS.FAMILY_REGULAR,
    fontSize: 15,
    marginTop: 15,
    alignSelf: 'center',
    textAlign: 'center',
    marginHorizontal: '10%',
    color: 'rgba(70, 90, 100, 1)',
  },
  button: {
    width: widthPercentageToDP(85),
    height: 50,
    borderRadius: 8,
    backgroundColor: COLOR.GREEN,
    position: 'absolute',
    bottom: 30,
    justifyContent: 'center',
    alignSelf: 'center',
    alignItems: 'center',
  },
  textColor: {
    color: COLOR.WHITE,
    fontFamily: FONTS.FAMILY_REGULAR,
  },
});
