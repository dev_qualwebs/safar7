import React from 'react';
import {View, Text, ImageBackground, Image} from 'react-native';
import {TouchableOpacity} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import COLOR from '../../styles/Color';
import IMAGES from '../../styles/Images';
import Styles from '../../styles/Styles';

export default class TripReady extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View style={[Styles.container]}>
        <Image
          resizeMode={'cover'}
          source={IMAGES.success_image}
          style={{height: hp(50), width: wp(100), borderRadius: 20}}
        />
        <Text
          style={[
            Styles.heading_label,
            {color: COLOR.BLACK, textAlign: 'center', marginVertical: 20},
          ]}>
          Your Trip Plan is Almost Ready!
        </Text>
        <Text
          style={[
            Styles.small_label,
            {color: COLOR.BLACK, textAlign: 'center', width: wp(80)},
          ]}>
          Our Team will plan you the best trip plan of the selected options. We
          will get back to you in 24 - 48 hours for final trip plan.
        </Text>
        <TouchableOpacity
          style={{
            width: wp(90),
            alignSelf: 'center',
            height: 50,
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: COLOR.GREEN,
            borderRadius: 10,

            bottom: 20,
            position: 'absolute',
          }}
          onPress={() => {
            this.setState({modalVisible: false}, (val) => {
              this.props.navigation.navigate('HomeStack');
            });
          }}>
          <Text
            style={[
              Styles.button_font,
              {
                color: COLOR.WHITE,
                width: null,
                textAlign: 'center',
              },
            ]}>
            Great
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}
