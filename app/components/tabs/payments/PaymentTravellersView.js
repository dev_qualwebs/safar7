import React from 'react';
import {
  StyleSheet,
  Text,
  FlatList,
  TouchableOpacity,
  View,
  TextInput,
} from 'react-native';
import {
  widthPercentageToDP,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import {heightPercentageToDP} from 'react-native-responsive-screen';
import Styles from '../../styles/Styles';
import COLOR from '../../styles/Color';
import FONTS from '../../styles/Fonts';
import {Image} from 'react-native';
import IMAGES from '../../styles/Images';
import CountryPicker from 'react-native-country-picker-modal';
import { DoneButtonKeyboard } from '../../commonView/CommonView';

export default class PaymentTravellersView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      pickerVisible: false,
    };
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.protectView}>
          <Image
            style={styles.protectImage}
            resizeMode={'contain'}
            source={IMAGES.shield}
          />
          <Text style={styles.protectText}>
            WE PROTECT YOUR PERSONAL INFORMATION
          </Text>
        </View>
        <Text style={styles.title}>TRAVELLERS</Text>
        <FlatList
        bounces={false}
          numColumns={1}
          style={{height: heightPercentageToDP(45)}}
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}
          data={this.props.travellers}
          renderItem={({item}) => {
            return (
              <TouchableOpacity>
                <View style={styles.travellersView}>
                  <Text style={[Styles.body_label, {width: null, flex: 1}]}>
                    {item.prefix + ' ' + item.first_name}
                  </Text>
                </View>
                <View style={Styles.line_view} />
              </TouchableOpacity>
            );
          }}
          keyExtractor={(item, index) => index.toString()}
        />

       {this.props.offerPayment == 1 ? null : <View
          style={{
            width: widthPercentageToDP(90),
            alignSelf: 'center',
            borderRadius: 10,
            borderWidth: 1,
            borderColor: COLOR.BORDER,
            padding: 10,
          }}>
          <Text style={styles.title}>
            Emergency Contact
            <Text style={styles.optionText}> (Optional)</Text>
          </Text>
          <Text style={styles.commonText}>
            Complete these details if you would like to connect an emergency
            contact to your trip
          </Text>
          <TextInput
          inputAccessoryViewID={'uniqueID'}
            placeholderTextColor={COLOR.LIGHT_TEXT_COLOR}
            placeholder={'Name '}
            style={{marginTop: 15, marginBottom: 5}}
          />
          <DoneButtonKeyboard/>
          <View
            style={[Styles.line_view, {marginVertical: 10, width: '100%'}]}
          />
          <TextInput
          inputAccessoryViewID={'uniqueID'}
            placeholderTextColor={COLOR.LIGHT_TEXT_COLOR}
            placeholder={'Relationship'}
            style={{marginVertical: 5}}
          />
          <View
            style={[Styles.line_view, {marginVertical: 10, width: '100%'}]}
          />
          <View style={{flexDirection: 'row'}}>
            <View style={{width: wp(20), marginRight: 10}}>
              <CountryPicker
              withAlphaFilter={true}
                withCallingCode={true}
                withFlag={true}
                visible={this.state.pickerVisible}
                onSelect={(country) => {
                  this.setState({
                    country_id: country.callingCode,
                    pickerVisible: false,
                  });
                }}
                onClose={()=>{
                  this.setState({pickerVisible:false})
                }}
                renderFlagButton={(country) => {
                  return (
                    <TouchableOpacity
                      onPress={() => this.setState({pickerVisible: true})}
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        width: null,
                        alignItems: 'center',
                        height: 27,
                      }}>
                      <Text
                        style={{
                          fontFamily: FONTS.FAMILY_REGULAR,
                          fontSize: 14,
                        }}>
                        {'Code'}
                      </Text>
                      <Image
                        style={{
                          height: 10,
                          width: 10,
                          tintColor: COLOR.VOILET,
                          marginHorizontal: 5,
                        }}
                        source={IMAGES.down_arrow}
                      />
                    </TouchableOpacity>
                  );
                }}
              />

              <View
                style={[Styles.line_view, {marginVertical: 10, width: wp(20)}]}
              />
            </View>
            <View style={{flex: 1}}>
              <TextInput
              inputAccessoryViewID={'uniqueID'}
                placeholderTextColor={COLOR.LIGHT_TEXT_COLOR}
                placeholder={'Phone Number'}
                style={{marginVertical: 5}}
              />
              <View
                style={[Styles.line_view, {marginVertical: 10, width: wp(65)}]}
              />
            </View>
          </View>
        </View>}
   {this.props.offerPayment == 1 ? null : 
   <TouchableOpacity
          onPress={() => this.props.changeScreen(3)}
          style={styles.continueButton}>
          <Text style={[Styles.button_font, {color: COLOR.WHITE}]}>
            Continue
          </Text>
        </TouchableOpacity> 
        }
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: wp(90),
    alignSelf: 'center',
    flex: 1,
  },
  title: {
    fontSize: 17,
    marginVertical: 15,
    fontFamily: FONTS.FAMILY_SEMIBOLD,
  },
  travellersView: {
    flexDirection: 'row',
    alignSelf: 'center',
    width: wp(90),
    alignItems: 'center',
    marginVertical: 10,
  },
  continueButton: {
    alignItems: 'center',
    justifyContent: 'center',
    height: 50,
    borderRadius: 10,
    width: wp(90),
    marginTop: 10,
    marginBottom: 20,
    backgroundColor: COLOR.GREEN,
    alignSelf: 'center',
  },
  protectView: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 10,
    marginTop: 20,
  },
  protectImage: {
    width: 20,
    height: 20,
    alignSelf: 'center',
    marginRight: 10,
  },
  protectText: {
    fontFamily: FONTS.FAMILY_REGULAR,
    color: COLOR.LIGHT_GREEN,
  },
  optionText: {
    fontSize: 12,
    fontFamily: FONTS.FAMILY_REGULAR,
    color: COLOR.LIGHT_TEXT,
  },
  commonText: {
    fontSize: 14,
    fontFamily: FONTS.FAMILY_REGULAR,
  },
});
