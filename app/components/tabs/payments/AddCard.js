import React from 'react';
import { Modal, Text, View, StyleSheet, Image } from 'react-native';
import {

  TouchableOpacity,

} from 'react-native';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import COLOR from '../../../components/styles/Color';
import FONTS from '../../../components/styles/Fonts';
import IMAGES from '../../../components/styles/Images';
import Styles from '../../../components/styles/Styles';
import NavigationBar from '../../commonView/NavigationBar';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scrollview';
import { AS_FCM_TOKEN, D_PROPERTY_TYPES } from '../../../helper/Constants';
import { Hoshi } from 'react-native-textinput-effects';
import { connect } from 'react-redux';
import PaymentAction from '../../../redux/action/PaymentAction';
import API from '../../../api/Api';
import Auth from '../../../auth';
import Toast from 'react-native-simple-toast';

let api = new API();
let auth = new Auth();

class AddCard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: true,
      cardDetail: {
        card_number: '',
        card_holder: '',
        cvv: '',
        expiry_date: '',
      },
    };

    this.updateMasterState = this.updateMasterState.bind(this);
    this.handleAddCard = this.handleAddCard.bind(this);
  }

  updateMasterState = (attrName, value) => {
    if (attrName == 'card_number') {
      if (this.state.cardDetail.card_number.length > 18) {
        return;
      }
    } else if (attrName == 'cvv') {
      if (this.state.cardDetail.cvv.length > 2) {
        return;
      }
    } else if (attrName == 'expiry_date') {
      if (this.state.cardDetail.expiry_date.length > 4) {
        return;
      } else {
        if (
          this.state.cardDetail.expiry_date.indexOf('.') >= 0 ||
          this.state.cardDetail.expiry_date.length.length > 5
        ) {
          return;
        }
        if (this.state.cardDetail.expiry_date.length === 1) {
          value += '/';
        }
      }
    }
    this.setState({
      cardDetail: {
        ...this.state.cardDetail,
        [attrName]: value,
      },
    });
  };

  handleAddCard = async () => {
    let that = this;
    const { cardDetail } = that.state;
    const token = await auth.getValue(AS_FCM_TOKEN);
    let card = cardDetail;
    card.card_number = card.card_number.replace(/ /g, '');
    let data = JSON.stringify(card);
    that.setState({ isLoading: true });
    api
      .addPaymentCard(data)
      .then((json) => {
        console.log('card response:-', json);
        that.setState({ isLoading: false });
        if (json.status == 200) {
          setTimeout(() => {
            Toast.show('Successfully added card');
          }, 0);
          this.props.getAddedCards(1);
          // this.props.navigation.navigate('PaymentScreen');
          this.props.navigation.goBack();
        } else if (json.status == 400) {
          setTimeout(() => {
            Toast.show(json.data.message);
          }, 0);
        } else {
          setTimeout(() => {
            Toast.show(json.data.message);
          }, 0);
        }
      })
      .catch((error) => {
        setTimeout(() => {
          Toast.show(String(error.response.data.message));
        }, 0);
        console.log('login error:-', error.response.data.message);
        that.setState({ isLoading: false });
      });
  };

  render() {
    return (
      <View style={Styles.container}>
        <Modal
          animationType={'slide'}
          transparent={true}
          style={{ backgroundColor: COLOR.BLACK }}
          visible={this.state.modalVisible}
          onRequestClose={() => {
            console.log('Modal has been closed.');
          }}>
          <View style={styles.modal}>
            <View style={styles.modalBody}>
              <KeyboardAwareScrollView>
                <NavigationBar
                  prop={this.props}
                  navHeight={40}
                  name={'Add Card'}
                  // handleBack={()=>{
                  //   this.setState({modalVisible:false},()=>{
                  //     this.props.route.params.setModal
                  //     this.props.navigation.goBack();
                    
                  //   })
                //  }
               // }
                />
                <View
                  style={{
                    width: wp(90),
                    alignSelf: 'center',
                    paddingVertical: 20,
                  }}>
                  <Text style={Styles.small_label}>ACCEPTED CARD</Text>
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      marginVertical: 20,
                    }}>
                    <View
                      style={{
                        flexDirection: 'row',
                      }}>
                      <Image
                        style={{ width: 45, height: 30 }}
                        resizeMode={'contain'}
                        source={IMAGES.mastercard}
                      />
                      <Image
                        style={{ width: 45, height: 30, marginHorizontal: 10 }}
                        resizeMode={'contain'}
                        source={IMAGES.visa}
                      />
                      <Image
                        style={{ width: 45, height: 30 }}
                        resizeMode={'contain'}
                        source={IMAGES.new_card_mada}
                      />
                    </View>
                    <Image
                      style={{ width: 25, height: 25 }}
                      resizeMode={'contain'}
                      source={IMAGES.tick_lock}
                    />
                  </View>
                  <TouchableOpacity
                    style={{
                      width: wp(90),
                      alignSelf: 'center',
                      height: 50,
                      alignItems: 'center',
                      justifyContent: 'center',
                      backgroundColor: COLOR.WHITE,
                      borderRadius: 10,
                      borderWidth: 1,
                      borderColor: COLOR.LIGHT_TEXT,
                      marginBottom: 0,
                    }}
                    >
                    <View style={{ flexDirection: 'row' }}>
                      <Image
                        style={styles.buttonImage}
                        resizeMode={'contain'}
                        source={IMAGES.red_camera}
                      />
                      <Text
                        style={[
                          Styles.button_font,
                          {
                            color: COLOR.BLACK,
                            width: null,
                            textAlign: 'center',
                          },
                        ]}>
                        Scan Your Card
                      </Text>
                    </View>
                  </TouchableOpacity>
                </View>

                <View
                  style={{
                    alignSelf: 'center',
                    width: wp(90),
                    marginBottom: 20,
                  }}>
                  <Hoshi
                    label={'Card Number'}
                    borderColor={'transparent'}
                    borderRadius={1}
                    height={45}
                    returnKeyType={'done'}
                    keyboardType={'number-pad'}
                    style={{ marginBottom: 15 }}
                    onChangeText={(val) =>
                      this.updateMasterState('card_number', val)
                    }
                    value={this.state.cardDetail.card_number
                      .replace(/\s?/g, '')
                      .replace(/(\d{4})/g, '$1 ')
                      .trim()}
                    inputStyle={Styles.text_input_text}
                    labelStyle={Styles.text_input_heading}
                  />
                  <Hoshi
                    label={'Cardholders Name'}
                    borderColor={'transparent'}
                    borderRadius={1}
                    height={45}
                    returnKeyType={'done'}
                    keyboardType={'default'}
                    onChangeText={(val) =>
                      this.updateMasterState('card_holder', val)
                    }
                    style={{ marginBottom: 15 }}
                    inputStyle={Styles.text_input_text}
                    value={this.state.cardDetail.card_holder}
                    labelStyle={Styles.text_input_heading}
                  />
                  <View style={{ flexDirection: 'row' }}>
                    <Hoshi
                      label={'Expiry Date'}
                      borderColor={'transparent'}
                      borderRadius={1}
                      height={45}
                      returnKeyType={'done'}
                      onChangeText={(val) =>
                        this.updateMasterState('expiry_date', val)
                      }
                      keyboardType={'number-pad'}
                      value={this.state.cardDetail.expiry_date}
                      style={{ marginBottom: 15, flex: 1, marginRight: 10 }}
                      inputStyle={Styles.text_input_text}
                      labelStyle={Styles.text_input_heading}
                    />
                    <Hoshi
                      label={'CVV'}
                      borderColor={'transparent'}
                      borderRadius={1}
                      height={45}
                      returnKeyType={'done'}
                      onChangeText={(val) => this.updateMasterState('cvv', val)}
                      value={this.state.cardDetail.cvv}
                      keyboardType={'number-pad'}
                      style={{ marginBottom: 15, flex: 1 }}
                      inputStyle={Styles.text_input_text}
                      labelStyle={Styles.text_input_heading}
                    />
                  </View>
                </View>

                <TouchableOpacity
                  style={{
                    width: wp(90),
                    alignSelf: 'center',
                    height: 50,
                    alignItems: 'center',
                    justifyContent: 'center',
                    backgroundColor: COLOR.GREEN,
                    borderRadius: 10,
                    marginBottom: 10,
                  }}
                  onPress={this.handleAddCard}>
                  <View style={{ flexDirection: 'row' }}>
                    <Image
                      style={[styles.buttonImage, { tintColor: COLOR.WHITE }]}
                      resizeMode={'contain'}
                      source={IMAGES.tick_lock}
                    />
                    <Text
                      style={[
                        Styles.button_font,
                        {
                          color: COLOR.WHITE,
                          width: null,
                          textAlign: 'center',
                        },
                      ]}>
                      Use Payment Method
                    </Text>
                  </View>
                </TouchableOpacity>
              </KeyboardAwareScrollView>
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  modal: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
  },
  modalBody: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    backgroundColor: COLOR.WHITE,
    borderWidth: 1,
    width: wp(100),
    marginTop: 50,
    borderColor: COLOR.BORDER,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
  image: {
    height: 120,
    width: 120,
    alignSelf: 'center',
    marginVertical: 20,
  },
  headingLabel: {
    fontFamily: FONTS.FAMILY_BOLD,
    fontSize: FONTS.MEDIUM,
    color: COLOR.BLACK,
  },
  singleLineView: {
    marginVertical: 20,
    borderBottomColor: COLOR.GRAY,
    width: wp(100),
    alignSelf: 'center',
    borderBottomWidth: 1,
  },
  borderView: {
    borderRadius: 10,
    borderWidth: 3,
    width: wp(90),
    borderColor: COLOR.GRAY,
    alignSelf: 'center',
  },
  buttonImage: { width: 20, height: 20, tintColor: COLOR.BLACK, marginRight: 5 },
});

mapDispatchToProps = (dispatch) => {
  return {
    getAddedCards: (val) => {
      dispatch(PaymentAction.getAddedCards(val));
    },
  };
};

export default connect(null, mapDispatchToProps)(AddCard);
