import React from 'react';
import {Modal, Text, View, StyleSheet, Image} from 'react-native';
import {
  FlatList,
  TouchableOpacity,
} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import COLOR from '../../../components/styles/Color';
import FONTS from '../../../components/styles/Fonts';
import IMAGES from '../../../components/styles/Images';
import Styles from '../../../components/styles/Styles';
import NavigationBar from '../../commonView/NavigationBar';
import { connect } from 'react-redux';
import PaymentAction from '../../../redux/action/PaymentAction';


 class ChoosePaymentMethod extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: true,
      paymentMethod: [],
      cardData:[],
    };

    this.props.getPaymentMethod()
  }
   
   componentDidMount() {
     if (this.props.paymentMethodData) {
        this.setState({
          paymentMethod: this.props.paymentMethodData,
        });
     }

     if(this.props.route.params && this.props.route.params.cards){
      this.setState({cardData:this.props.route.params.cards})
     }
   }
   
   componentWillReceiveProps(props) {
     if (props.paymentMethodData != this.props.paymentMethodData) {
       this.setState({
         paymentMethod: props.paymentMethodData,
       });
     }
   }

  render() {
    return (
      <View style={Styles.container}>
        <Modal
          animationType={'slide'}
          transparent={true}
          style={{backgroundColor: COLOR.BLACK}}
          visible={this.state.modalVisible}
          onRequestClose={() => {
            console.log('Modal has been closed.');
          }}>
          <View style={styles.modal}>
            <View style={styles.modalBody}>
              <NavigationBar
                prop={this.props}
                navHeight={40}
                name={'Choose Payment Method'}
              />
              <View
                style={{
                  width: wp(90),
                  alignSelf: 'center',
                  paddingVertical: 10,
                }}>
                <FlatList
                  style={{flex: 0}}
                  data={this.state.paymentMethod}
                  renderItem={({item}) => (
                    <TouchableOpacity onPress={() => {
                      if(item.name == 'Credit/Debit Cards'){
                      this.setState({modalVisible: false}, (val) => {
                      // if(this.state.cardData.length != 0){
                        // this.props.navigation.navigate('AddCard');
                        this.props.navigation.replace('AddCard');
                      // }else {
                      //   this.props.navigation.goBack();
                      // }
                      });
                    }
                    }}>
                      <View
                        style={{
                          flexDirection: 'row',
                          justifyContent: 'space-between',
                          marginTop: 0,
                          alignItems: 'center',
                        }}>
                        <View
                          style={{
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                          }}>
                          <Image
                            style={{height: 30, width: 30, marginRight: 10}}
                            source={item.image}
                            resizeMode={'contain'}
                          />
                          <Text style={[Styles.body_label, {width: null}]}>
                            {item.name}
                          </Text>
                        </View>
                        <Image
                          resizeMode={'contain'}
                          style={{height: 15, width: 15, alignSelf: 'center'}}
                          source={IMAGES.right_arrow}
                        />
                      </View>
                      <View style={[Styles.line_view, {marginVertical: 20}]} />
                    </TouchableOpacity>
                  )}
                  numColumns={'1'}
                  removeClippedSubviews={false}
                  showsVerticalScrollIndicator={false}
                  showsHorizontalScrollIndicator={false}
                  keyExtractor={(item, index) => index.toString()}
                />
              </View>
              <TouchableOpacity
                style={{
                  width: wp(90),
                  alignSelf: 'center',
                  height: 50,
                  alignItems: 'center',
                  justifyContent: 'center',
                  backgroundColor: COLOR.WHITE,
                  borderRadius: 10,
                  borderWidth: 1,
                  borderColor: COLOR.GRAY,
                  marginBottom: 20,
                }}
                onPress={() => {
                  this.props.navigation.goBack(null)
                }}>
                <Text
                  style={[
                    Styles.button_font,
                    {
                      color: COLOR.BLACK,
                      width: null,
                      textAlign: 'center',
                    },
                  ]}>
                  Cancel
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  modal: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
  },
  modalBody: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    backgroundColor: COLOR.WHITE,
    borderWidth: 1,
    width: wp(100),
    marginTop: 50,
    borderColor: COLOR.BORDER,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
  image: {
    height: 120,
    width: 120,
    alignSelf: 'center',
    marginVertical: 20,
  },
  headingLabel: {
    fontFamily: FONTS.FAMILY_BOLD,
    fontSize: FONTS.MEDIUM,
    color: COLOR.BLACK,
  },
  singleLineView: {
    marginVertical: 20,
    borderBottomColor: COLOR.GRAY,
    width: wp(100),
    alignSelf: 'center',
    borderBottomWidth: 1,
  },
  borderView: {
    borderRadius: 10,
    borderWidth: 3,
    width: wp(90),
    borderColor: COLOR.GRAY,
    alignSelf: 'center',
  },
});


const mapStateToProps = (state, ownProps) => {
  return {
    paymentMethodData: state.paymentReducer.paymentMethod,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getPaymentMethod: (val) => {
      dispatch(PaymentAction.getPaymentMethods(val));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ChoosePaymentMethod); 