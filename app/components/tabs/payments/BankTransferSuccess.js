import React from 'react';
import {View, Text, ImageBackground, Image} from 'react-native';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import COLOR from '../../styles/Color';
import IMAGES from '../../styles/Images';
import Styles from '../../styles/Styles';

export default class BankTransferSuccess extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View style={[Styles.container]}>
        <ImageBackground
          source={IMAGES.green_gradient_view}
          resizeMode={'cover'}
          style={[
            Styles.container,
            {justifyContent: 'center', alignItems: 'center'},
          ]}>
          <Image
            resizeMode={'contain'}
            source={IMAGES.tick_circle}
            style={{height: 100, width: 100, borderRadius: 50}}
          />
          <Text
            style={[
              Styles.subheading_label,
              {color: COLOR.WHITE, textAlign: 'center', marginVertical: 20},
            ]}>
            Success
          </Text>
          <Text
            style={[
              Styles.small_label,
              {color: COLOR.WHITE, textAlign: 'center',width:wp(80)},
            ]}>
            Thank you for uploading the receipt. Verification of your trip will
            confirmed after receiving the transfer within 24 hours
          </Text>
        </ImageBackground>
      </View>
    );
  }
}
