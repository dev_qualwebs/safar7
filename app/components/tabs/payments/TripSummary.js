import React from 'react';
import {Image, View, Text, FlatList} from 'react-native';
import {TouchableOpacity} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import COLOR from '../../styles/Color';
import Styles from '../../styles/Styles';
import TripServiceView from '../message/TripServiceView';
import PaymentOneServiceView from '../paymentOne/PaymentOneServiceView';

const TripSummary = (props) => {
  let {onContinue, summary, id, departDate, returnDate, package_request_id,isPackage} =
    props;
  return (
    <View style={{flex: 1, marginTop: 25}}>
      <FlatList
      bounces={false}
      showsVerticalScrollIndicator={false}
        data={summary}
        renderItem={({item}) => (
          <View style={{marginHorizontal: 15, marginVertical: 7.5}}>
            <PaymentOneServiceView
              package_request_id={package_request_id}
              tripId={id}
              depart={departDate}
              return={returnDate}
              item={item}
              tripSummary={true}
              isPackage={isPackage}
            />
          </View>
        )}
      />
      <TouchableOpacity
        onPress={() => onContinue()}
        style={{
          alignItems: 'center',
          justifyContent: 'center',
          height: 50,
          borderRadius: 10,
          width: wp(90),
          marginVertical: 10,
          backgroundColor: COLOR.GREEN,
          alignSelf: 'center',
        }}>
        <Text style={[Styles.button_font, {color: COLOR.WHITE}]}>Continue</Text>
      </TouchableOpacity>
    </View>
  );
};

export default TripSummary;
