import React from 'react';
import { Modal, Text, View, StyleSheet, Image } from 'react-native';
import {
  TouchableOpacity,
} from 'react-native';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import COLOR from '../../../components/styles/Color';
import FONTS from '../../../components/styles/Fonts';
import IMAGES from '../../../components/styles/Images';
import Styles from '../../../components/styles/Styles';
import NavigationBar from '../../commonView/NavigationBar';
import { Hoshi } from 'react-native-textinput-effects';

export default class BankTransfer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: true,
    };
  }

  updateMasterState = (attrName, value) => {
    this.setState({ [attrName]: value });
  };

  render() {
    return (
      <View style={Styles.container}>
        <Modal
          animationType={'slide'}
          transparent={true}
          style={{ backgroundColor: COLOR.BLACK }}
          visible={this.state.modalVisible}
          onRequestClose={() => {
            console.log('Modal has been closed.');
          }}>
          <View style={styles.modal}>
            <View style={styles.modalBody}>
              <NavigationBar
                prop={this.props}
                navHeight={40}
                name={'Bank Transfer'}
              />
              <View
                style={{
                  width: wp(90),
                  alignSelf: 'center',
                  paddingVertical: 20,
                }}>
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    marginBottom: 20,
                  }}>
                  <Image
                    style={{ width: 25, height: 25, marginRight: 10 }}
                    resizeMode={'contain'}
                    source={IMAGES.banking}
                  />
                  <Text style={[Styles.body_label]}>Bank Details</Text>
                </View>
                <Hoshi
                  label={'Account Name'}
                  placeholderTextColor={COLOR.BLACK}
                  borderColor={COLOR.BORDER}
                  borderRadius={1}
                  height={45}
                  returnKeyType={'done'}
                  keyboardType={'default'}
                  style={{ marginBottom: 15 }}
                  inputStyle={Styles.text_input_text}
                  labelStyle={Styles.text_input_heading}
                />
                <Hoshi
                  label={'Account Number'}
                  placeholderTextColor={COLOR.BLACK}
                  borderColor={COLOR.BORDER}
                  borderRadius={1}
                  height={45}
                  returnKeyType={'done'}
                  keyboardType={'number-pad'}
                  style={{ marginBottom: 15 }}
                  inputStyle={Styles.text_input_text}
                  labelStyle={Styles.text_input_heading}
                />
                <Hoshi
                  label={'IBAN'}
                  borderColor={COLOR.BORDER}
                  placeholderTextColor={COLOR.BLACK}
                  borderRadius={1}
                  height={45}
                  returnKeyType={'done'}
                  keyboardType={'number-pad'}
                  inputStyle={Styles.text_input_text}
                  labelStyle={Styles.text_input_heading}
                />
                <View
                  style={{
                    borderRadius: 10,
                    borderColor: COLOR.GRAY,
                    borderWidth: 1,
                    paddingVertical: 20,
                    marginVertical: 20,
                    paddingHorizontal: 10,
                  }}>
                  <Text
                    style={[
                      Styles.small_label,
                      {
                        fontFamily: FONTS.FAMILY_BOLD,
                        width: null,
                        alignSelf: 'flex-start',
                        color: COLOR.BLACK,
                        marginBottom: 10,
                      },
                    ]}>
                    Instructions
                  </Text>

                  <Text
                    style={[
                      Styles.small_label,
                      {
                        width: null,
                        alignSelf: 'flex-start',
                      },
                    ]}>
                    1. Pay the trip due by either at the bank or through online
                    banking.
                  </Text>
                  <Text
                    style={[
                      Styles.small_label,
                      {
                        width: null,
                        alignSelf: 'flex-start',
                      },
                    ]}>
                    2. Please upload a clear and readable photo of your receipt
                    for faster processing.
                  </Text>
                  <Text
                    style={[
                      Styles.small_label,
                      {
                        width: null,
                        alignSelf: 'flex-start',
                      },
                    ]}>
                    3. Your trip plan will shown in your app account after
                    transfer verified within 24 hours.
                  </Text>
                </View>
              </View>

              <TouchableOpacity
                style={{
                  width: wp(90),
                  alignSelf: 'center',
                  height: 50,
                  alignItems: 'center',
                  justifyContent: 'center',
                  backgroundColor: COLOR.GREEN,
                  borderRadius: 10,

                  marginBottom: 10,
                }}
                onPress={() => {
                  this.setState({ modalVisible: false }, (val) => {
                    this.props.navigation.navigate('TripReady');
                  });
                }}>
                <Text
                  style={[
                    Styles.button_font,
                    {
                      color: COLOR.WHITE,
                      width: null,
                      textAlign: 'center',
                    },
                  ]}>
                  Upload Receipt
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  modal: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
  },
  modalBody: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    backgroundColor: COLOR.WHITE,
    borderWidth: 1,
    width: wp(100),
    marginTop: 50,
    borderColor: COLOR.BORDER,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
  image: {
    height: 120,
    width: 120,
    alignSelf: 'center',
    marginVertical: 20,
  },
  headingLabel: {
    fontFamily: FONTS.FAMILY_BOLD,
    fontSize: FONTS.MEDIUM,
    color: COLOR.BLACK,
  },
  singleLineView: {
    marginVertical: 20,
    borderBottomColor: COLOR.GRAY,
    width: wp(100),
    alignSelf: 'center',
    borderBottomWidth: 1,
  },
  borderView: {
    borderRadius: 10,
    borderWidth: 3,
    width: wp(90),
    borderColor: COLOR.GRAY,
    alignSelf: 'center',
  },
});
