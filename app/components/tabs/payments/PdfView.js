import {useRoute} from '@react-navigation/native';
import React from 'react';
import {View} from 'react-native';
import PDFView from 'react-native-view-pdf';

export default function PdfView(props) {
  const route = useRoute();
  return (
    <View style={{flex: 1}}>
      {/* Some Controls to change PDF resource */}
      <PDFView
        fadeInDuration={250.0}
        style={{flex: 1}}
        resource={route.params.url}
        resourceType={'url'}
        onLoad={() => console.log(`PDF rendered from`)}
        onError={(error) => console.log('Cannot render PDF', error)}
      />
    </View>
  );
}
