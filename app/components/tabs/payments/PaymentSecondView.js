import React from 'react';
import {Image, View, Text, Alert} from 'react-native';
import {TouchableOpacity} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import COLOR from '../../styles/Color';
import IMAGES from '../../styles/Images';
import Styles from '../../styles/Styles';
import FONTS from '../../styles/Fonts';
import {FlatList, TextInput} from 'react-native-gesture-handler';
import CardBottomSheet from './CardBottomSheet';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview';
import {StyleSheet} from 'react-native';
import PaymentAction from '../../../redux/action/PaymentAction';
import {connect} from 'react-redux';
import ActivityIndicator from '../../commonView/ActivityIndicator';
import SimpleToast from 'react-native-simple-toast';
import API from '../../../api/Api';
import RequestAction from '../../../redux/action/RequestAction';
import {DoneButtonKeyboard} from '../../commonView/CommonView';
import ProfileAction from '../../../redux/action/ProfileAction';
import PackageAction from '../../../redux/action/PackageAction';

const api = new API();

class PaymentSecondView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      tdChecked: false,
      detailChecked: false,
      changeCardModel: false,
      cardData: [],
      cvvNumber: '',
      tripP2: null,
      packageP2: null,
      loading: false,
      offer: this.props.offer,
      isPackage: this.props.isPackage,
      offerData: this.props.activeOffer,
      addonOffers: this.props.activeOffer.addon_offers,
      isCreditPayEnabled: false,
      userDetail: '',
      offerPayment: 0,
    };

    this.props.getAddedCards();
  }

  componentDidMount() {
    if (this.props.cardData) {
      this.setState({
        cardData: this.props.cardData,
      });

      if (this.props.activeOffer) {
        this.setState({
          isCreditPayEnabled:
            this.props.activeOffer.points_applied == 0 ? false : true,
        });
      }
    }

    if (this.props.userDetail) {
      this.setState({userDetail: this.props.userDetail},()=>{
        console.log('userdetail',this.state.userDetail);
      });
    }
  }

  componentDidUpdate(prevProps) {
    if (this.props.singleTripOffer !== prevProps.singleTripOffer && this.props.singleTripOffer) {
      this.setState(
        {
          offerData: this.props.singleTripOffer.active_offers[0],
          isCreditPayEnabled:
            this.props.singleTripOffer.active_offers[0].points_applied == 0
              ? false
              : true,
        },
        () => {
          this.setState({addonOffers: this.state.offerData.addon_offers});
        },
      );
    }

    if (this.props.singlePackageOffer !== prevProps.singlePackageOffer && this.props.singlePackageOffer) {
      this.setState({
        offer: this.props.singlePackageOffer,
        isCreditPayEnabled: this.props.singlePackageOffer.active_offers[0].points_applied == 0
          ? false
          : true,
    },
    () => {
      this.setState({addonOffers: this.state.offerData.addon_offers});
    },);
    }

    if (prevProps.cardData != this.props.cardData) {
      this.setState({
        cardData: this.props.cardData,
      });
    }

    if (prevProps.tripP2Data !== this.props.tripP2Data) {
      this.setState({
        loading: false,
      });
      this.props.getUserDetail();
      this.props.navigation.navigate('SuccessScreen');
    }

    if (prevProps.packageP2Data !== this.props.packageP2Data) {
      this.props.getUserDetail();
      this.props.navigation.navigate('SuccessScreen');
      this.setState({
        loading: false,
      });
    }

    if (prevProps.userDetail != this.props.userDetail) {
      this.setState({
        userDetail: this.props.userDetail,
      });
    }
  }

  /**
   * enables and disables payment with credit which were earned from sharing the app or other methods
   */
  setPayByCredit = () => {
    api
      .setCreditPay(this.state.offerData && this.state.offerData.id)
      .then((json) => {
        if (json.status == 200) {
          if(this.props.isPackage){
            this.props.getSinglePackageOffer(this.props.id);
          } else {
          this.props.getSingleTripOffer(this.props.packageId);
        }
        } else {
          Toast.show(json.data.message);
          console.log(json.data.message);
        }
      })
      .catch(function (error) {
        Toast.show(error.response.data.message);
      });
  };

  updateMasterState = (attr, value) => {
    this.setState({
      [attr]: value,
    });
  };

  handleBooking = () => {
    let that = this;
    if (this.state.cardData.length == 0) {
      SimpleToast.show('Please add card to Book Trip');
    } else if (this.state.tdChecked == false) {
      SimpleToast.show('Please Agree T&C');
    } else if (this.state.detailChecked == false) {
      SimpleToast.show(
        'Please confirm and agree to, that all travellers details are correct.',
      );
    } else if (this.state.cvvNumber == '') {
      SimpleToast.show('Enter CVV');
    } else {
      let cardLength = this.state.cardData.length;
      var data;
      if (this.state.isPackage) {
        data = JSON.stringify({
          package_request_id: this.state.offer.id,
          payment_method_id: 1,
          card_id: this.state.cardData[cardLength - 1].id,
          cvv: this.state.cvvNumber,
        });
      } else {
        data = JSON.stringify({
          trip_id: this.state.offer.id,
          payment_method_id: 1,
          card_id: this.state.cardData[cardLength - 1].id,
          cvv: this.state.cvvNumber,
        });
      }

      // that.setState({loading: true});

      this.state.isPackage
        ? this.props.packageP2(data)
        : this.props.tripP2(data);
    }
  };

  addRemoveAddons = (status, id, requestedId) => {
    let data = {
      requested_trip: requestedId,
      addon_id: id,
      status: status,
    };
    api
      .addRemoveAddons(data)
      .then((json) => {
        this.props.getSingleTripOffer(this.props.packageId);
      })
      .catch(function (error) {
        console.log(error.response.data.message);
      });
  };

  Addons = () => (
    <View
      style={{
        borderRadius: 10,
        backgroundColor: COLOR.WHITE,
        borderWidth: 0,
        shadowColor: COLOR.VOILET,
        shadowOffset: {width: 0.2, height: 0.2},
        shadowOpacity: 0.2,
        width: wp(90),
        alignSelf: 'center',
        paddingHorizontal: 10,
        paddingVertical: 20,
        elevation: 3,
        marginTop: 20,
      }}>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          marginBottom: 10,
        }}>
        <Text
          style={[
            Styles.small_label,
            {
              width: null,
              color: COLOR.BLACK,
              fontFamily: FONTS.FAMILY_BOLD,
            },
          ]}>
          ADDONS
        </Text>
      </View>
      <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
        <View style={{flexDirection: 'row'}}>
          <Text
            style={[
              Styles.small_label,
              {
                width: null,
                alignSelf: 'flex-start',
                marginBottom: 10,
                fontSize: 14,
                color: COLOR.BLACK,
              },
            ]}>
            Data roaming simcard
          </Text>
        </View>
        <View style={{flexDirection: 'row'}}>
          <Text
            style={[
              Styles.small_label,
              {
                width: null,
                alignSelf: 'flex-start',
                marginBottom: 10,
                fontSize: 14,
                color: COLOR.BLACK,
                marginRight: 10,
              },
            ]}>
            450 SAR
          </Text>
          <Image
            style={{height: 12, width: 12, marginTop: 2}}
            resizeMode={'contain'}
            source={IMAGES.chain}
          />
        </View>
      </View>
      <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
        <View style={{flexDirection: 'row'}}>
          <Text
            style={[
              Styles.small_label,
              {
                width: null,
                alignSelf: 'flex-start',
                marginBottom: 10,
                fontSize: 14,
                color: COLOR.BLACK,
              },
            ]}>
            Travel Visa
          </Text>
        </View>
        {/* <View style={{flexDirection: 'row'}}>
                <Text
                  style={[
                    Styles.small_label,
                    {
                      width: null,
                      alignSelf: 'flex-start',
                      marginBottom: 10,
                      fontSize: 14,
                      color: COLOR.BLACK,
                      marginRight: 10,
                    },
                  ]}>
                  450 SAR
                </Text>
                <Image
                  style={{height: 12, width: 12, marginTop: 2}}
                  resizeMode={'contain'}
                  source={IMAGES.chain}
                />
              </View> */}
      </View>

      <Text
        style={[
          Styles.small_label,
          {
            marginTop: 20,
            width: null,
            alignSelf: 'flex-start',
            color: COLOR.LIGHT_GREEN,
          },
        ]}>
        All documents will be deliver to your email after the payment
      </Text>
    </View>
  );

  render() {
    let {tdChecked, detailChecked, offerData, addonOffers} = this.state;

    return (
      <KeyboardAwareScrollView>
        <View style={{backgroundColor: COLOR.WHITE}}>
          {addonOffers && addonOffers.length > 0 ? (
            <View
              style={{
                borderRadius: 10,
                backgroundColor: COLOR.WHITE,
                borderWidth: 0,
                shadowColor: COLOR.VOILET,
                shadowOffset: {width: 0.2, height: 0.2},
                shadowOpacity: 0.2,
                width: wp(90),
                alignSelf: 'center',
                paddingHorizontal: 10,
                paddingVertical: 20,
                elevation: 3,
                marginTop: 20,
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  marginBottom: 10,
                }}>
                <Text
                  style={[
                    Styles.small_label,
                    {
                      width: null,
                      color: COLOR.BLACK,
                      fontFamily: FONTS.FAMILY_BOLD,
                    },
                  ]}>
                  ADDONS
                </Text>
              </View>

              <FlatList
                data={addonOffers}
                renderItem={({item, index}) => (
                  <>
                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                      }}>
                      <View style={{flexDirection: 'row'}}>
                        <TouchableOpacity
                          onPress={() => {
                            this.addRemoveAddons(
                              item.status == 1 ? 0 : 1,
                              item.addon_id,
                              item.admin_trip_offer_id,
                            );
                          }}
                          style={{
                            height: 20,
                            width: 20,
                            borderRadius: 5,
                            backgroundColor: COLOR.GREEN,
                            borderColor: COLOR.GRAY,
                            justifyContent: 'center',
                            alignItems: 'center',
                            borderWidth: 1,
                            marginRight: 5,
                          }}>
                          {item.status == 0 ? null : (
                            <Image
                              resizeMode={'contain'}
                              style={{
                                height: 12,
                                width: 12,
                                alignSelf: 'center',
                              }}
                              source={IMAGES.tick_image}
                            />
                          )}
                        </TouchableOpacity>
                        <Text
                          style={[
                            Styles.small_label,
                            {
                              width: null,
                              alignSelf: 'flex-start',
                              marginBottom: 10,
                              fontSize: 14,
                              color: COLOR.BLACK,
                            },
                          ]}>
                          {item.addon_name ? item.addon_name : ''}
                        </Text>
                      </View>
                      <View style={{flexDirection: 'row'}}>
                        <Text
                          style={[
                            Styles.small_label,
                            {
                              width: null,
                              alignSelf: 'flex-start',
                              marginBottom: 10,
                              fontSize: 14,
                              color: COLOR.BLACK,
                              marginRight: 10,
                            },
                          ]}>
                          {item.total_amount ? item.total_amount : ''} SAR
                        </Text>
                        <Image
                          style={{height: 12, width: 12, marginTop: 2}}
                          resizeMode={'contain'}
                          source={IMAGES.chain}
                        />
                      </View>
                    </View>
                  </>
                )}
              />

              <Text
                style={[
                  Styles.small_label,
                  {
                    marginTop: 20,
                    width: null,
                    alignSelf: 'flex-start',
                    color: COLOR.LIGHT_GREEN,
                  },
                ]}>
                All documents will be deliver to your email after the payment
              </Text>
            </View>
          ) : null}
          <View style={styles.paymentHeading}>
            <Text
              style={[
                Styles.subheading_label,
                {
                  width: null,
                  fontFamily: FONTS.FAMILY_EXTRA_BOLD,
                  fontSize: 22,
                },
              ]}>
              Payment
            </Text>
          </View>

          <View style={styles.paymentRowView}>
            <Text
              style={[
                Styles.subheading_label,
                {width: null, fontFamily: FONTS.FAMILY_EXTRA_BOLD},
              ]}>
              Total
              <Text style={[Styles.small_label, {width: null}]}>
                {' '}
                (VATS included)
              </Text>
            </Text>

            <Text
              style={[
                Styles.subheading_label,
                {
                  width: null,
                  color: COLOR.BLACK,
                  fontFamily: FONTS.FAMILY_EXTRA_BOLD,
                },
              ]}>
              {/* {offerData.total_amount + ' SAR'} */}
              {parseFloat(offerData.total_amount) -
                parseFloat(offerData.points_discount) +
                ' SAR'}
            </Text>
          </View>
          <TouchableOpacity
            onPress={() => {
              this.props.navigation.navigate('PolicyPage', {
                screenTitle: 'Terms & Conditions',
              });
            }}>
            <Text style={[Styles.extra_small_label]}>
              *Upon the
              <Text style={[Styles.extra_small_label, {color: COLOR.GREEN}]}>
                Terms of Conditions{' '}
              </Text>
              , the fee of the service will be conducted from the customer upon
              type and number of requests.
            </Text>
          </TouchableOpacity>

          <View
            style={{
              flexDirection: 'row',
              width: wp(90),
              alignSelf: 'center',
              marginTop: 15,
            }}>
            <TouchableOpacity
              onPress={() => {
                // this.setState(
                //   {isCreditPayEnabled: !this.state.isCreditPayEnabled},
                //   () => {
                    this.setPayByCredit();
                  // },);
              }}>
              <Image
                style={{height: 30, width: 40, marginRight: 10}}
                source={
                  this.state.isCreditPayEnabled
                    ? IMAGES.radio_button_on
                    : IMAGES.radio_button_off
                }
                resizeMode={'contain'}
              />
            </TouchableOpacity>
            <Text style={[Styles.body_label, {width: null}]}>
              Use My Credits
            </Text>
            <Text
              style={[
                Styles.body_label,
                {
                  width: null,
                  flex: 1,
                  textAlign: 'right',
                  color: COLOR.LIGHT_GREEN,
                },
              ]}>
              {this.state.userDetail && this.state.userDetail.credit
                ? this.state.userDetail.credit
                : '0'}
            </Text>
          </View>

          <View
            style={{
              borderWidth: 1,
              borderColor: COLOR.GRAY,
              borderRadius: 10,
              height: 50,
              flexDirection: 'row',
              width: wp(90),
              alignSelf: 'center',
              paddingHorizontal: 20,
              justifyContent: 'space-between',
              alignItems: 'center',
              marginVertical: 30,
            }}>
            <Text
              style={[
                Styles.body_label,
                {width: null, color: COLOR.LIGHT_TEXT_COLOR},
              ]}>
              Add Coupon Code
            </Text>
            <Text
              style={[
                Styles.button_font,
                {
                  width: null,
                  color: COLOR.GREEN,
                  fontFamily: FONTS.FAMILY_SEMIBOLD,
                },
              ]}>
              Apply
            </Text>
          </View>
          <View style={{width: wp(90), alignSelf: 'center'}}>
            <View
              style={{
                flexDirection: 'row',
                width: wp(80),
              }}>
              <TouchableOpacity
                onPress={() => {
                  this.setState({tdChecked: !this.state.tdChecked});
                }}>
                <Image
                  style={{
                    height: 20,
                    width: 20,
                    borderRadius: 5,
                    borderWidth: 1,
                    borderColor: COLOR.VOILET,
                    marginRight: 15,
                    backgroundColor: tdChecked ? COLOR.GREEN : COLOR.WHITE,
                  }}
                  source={tdChecked ? IMAGES.tick_image : null}
                  resizeMode={'contain'}
                />
              </TouchableOpacity>
              <Text style={{color: COLOR.VOILET, fontSize: 12}}>
                I have read, understand and accept the{' '}
                <TouchableOpacity
                  onPress={() => {
                    this.props.navigation.navigate('PolicyPage', {
                      screenTitle: 'Privacy Policy',
                    });
                  }}>
                  <Text
                    style={[
                      Styles.small_label,
                      {width: null, color: COLOR.GREEN, marginBottom: -5},
                    ]}>
                    Privacy Policy{' '}
                  </Text>
                </TouchableOpacity>
                and{' '}
                <TouchableOpacity
                  onPress={() => {
                    this.props.navigation.navigate('PolicyPage', {
                      screenTitle: 'Terms & Conditions',
                    });
                  }}>
                  <Text
                    style={[
                      Styles.small_label,
                      {width: null, color: COLOR.GREEN, marginBottom: -5},
                    ]}>
                    Terms of Conditions{' '}
                  </Text>
                </TouchableOpacity>
              </Text>
            </View>

            <View
              style={{
                flexDirection: 'row',
                width: wp(80),
                marginTop: 10,
              }}>
              <TouchableOpacity
                onPress={() => {
                  this.setState({detailChecked: !detailChecked});
                }}>
                <Image
                  style={{
                    height: 20,
                    width: 20,
                    borderRadius: 5,
                    borderWidth: 1,
                    borderColor: COLOR.VOILET,
                    marginRight: 15,
                    backgroundColor: detailChecked ? COLOR.GREEN : COLOR.WHITE,
                  }}
                  resizeMode={'contain'}
                  source={detailChecked ? IMAGES.tick_image : null}
                />
              </TouchableOpacity>
              <Text
                style={{color: COLOR.VOILET, fontSize: 12, marginRight: 10}}>
                I Confirm that all travellers details are correct, match
                passport information and that at least one adult is 18 years old
                or above.
              </Text>
            </View>
          </View>
          {this.state.cardData.length > 0 && (
            <View
              style={{marginVertical: 20, width: wp(90), alignSelf: 'center'}}>
              <View
                style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                <Text
                  style={[
                    Styles.small_label,
                    {color: COLOR.VOILET, width: null},
                  ]}>
                  CARD NUMBER
                </Text>
                <View
                  style={{
                    width: 100,
                    alignItems: 'center',
                    flexDirection: 'row',
                  }}>
                  <Text
                    style={[
                      Styles.small_label,
                      {
                        color: COLOR.VOILET,
                        textAlign: 'left',
                        width: null,
                        marginRight: 10,
                      },
                    ]}>
                    CVV
                  </Text>
                  <Image
                    style={{height: 15, width: 15}}
                    source={IMAGES.red_alert}
                  />
                </View>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'space-between',
                  marginTop: 10,
                }}>
                <View
                  style={{
                    alignItems: 'center',
                    flexDirection: 'row',
                  }}>
                  <Image
                    style={{height: 25, width: 45, marginRight: 10}}
                    source={IMAGES.visa}
                    resizeMode={'contain'}
                  />
                  <Text
                    style={[
                      Styles.body_label,
                      {
                        color: COLOR.BLACK,
                        textAlign: 'left',
                        width: null,
                        marginRight: 0,
                      },
                    ]}>
                    {
                      this.state.cardData[this.state.cardData.length - 1]
                        .card_number
                    }
                  </Text>
                </View>
                <TextInput
                  // inputAccessoryViewID={'uniqueID'}
                  onChangeText={(text) => this.setState({cvvNumber: text})}
                  defaultValue={this.state.cvvNumber}
                  style={{
                    padding: 10,
                    width: 100,
                    borderWidth: 1,
                    borderColor: COLOR.VOILET,
                  }}
                  keyboardType={'number-pad'}
                />
                <DoneButtonKeyboard />
              </View>
              <TouchableOpacity
                onPress={() => this.setState({changeCardModel: true})}>
                <Text
                  style={[
                    Styles.small_label,
                    {color: COLOR.BLUE, width: null},
                  ]}>
                  CHANGE CARD
                </Text>
              </TouchableOpacity>
            </View>
          )}
          <View
            style={{
              flexDirection: 'row',
              width: wp(75),
              alignSelf: 'center',
              marginVertical: 15,
            }}>
            <Image
              style={{height: 20, width: 20, marginRight: 15, marginLeft: 5}}
              source={IMAGES.red_alert}
              resizeMode={'contain'}
            />

            <Text
              style={[
                Styles.small_label,
                {width: null, color: COLOR.RED, marginRight: 10},
              ]}>
              Traveller details cannot be edited once submit this page
            </Text>
          </View>
        </View>

        <TouchableOpacity
          onPress={() => this.handleBooking()}
          style={styles.payButton}>
          <Text style={[Styles.button_font, {color: COLOR.WHITE}]}>
            Pay by Credit Card
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            this.setState({modalVisible: false}, (val) => {
              this.props.navigation.navigate('ChoosePaymentMethod');
            });
          }}
          style={[
            {
              marginTop: 0,
              marginBottom: 20,
              height: 50,
              width: wp(90),
              alignSelf: 'center',
              borderColor: 'transparent',
            },
          ]}>
          <Text style={[Styles.body_label, {color: COLOR.BLACK, width: null}]}>
            Change Payment Method
          </Text>
        </TouchableOpacity>
        <CardBottomSheet
          updateMasterState={this.updateMasterState}
          modelVisibility={this.state.changeCardModel}
          cards={this.state.cardData}
        />
        <ActivityIndicator loading={this.state.loading} />
      </KeyboardAwareScrollView>
    );
  }
}

const styles = StyleSheet.create({
  paymentHeading: {
    flexDirection: 'row',
    width: wp(90),
    alignSelf: 'center',
    marginTop: 15,
    justifyContent: 'space-between',
  },
  paymentRowView: {
    flexDirection: 'row',
    width: wp(90),
    alignSelf: 'center',
    marginVertical: 10,
    justifyContent: 'space-between',
  },
  payButton: {
    marginTop: 20,
    marginBottom: 10,
    height: 50,
    backgroundColor: COLOR.GREEN,
    width: wp(90),
    alignSelf: 'center',
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

const mapStateToProps = (state, ownProps) => {
  return {
    cardData: state.paymentReducer.paymentCards,
    tripP2Data: state.paymentReducer.tripP2,
    packageP2Data: state.paymentReducer.packageP2,
    singleTripOffer: state.requestReducer.singleTripOffer,
    userDetail: state.profileReducer.userDetail,
    singlePackageOffer: state.pacakgeReducer.singlePackageOffer,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getAddedCards: (val) => {
      dispatch(PaymentAction.getAddedCards(val));
    },
    tripP2: (data) => {
      dispatch(PaymentAction.tripP2(data));
    },
    packageP2: (data) => {
      dispatch(PaymentAction.packageP2(data));
    },
    getSingleTripOffer: (val) => {
      dispatch(RequestAction.getSingleTripOffer(val));
    },
    getUserDetail: (val) => {
      dispatch(ProfileAction.getUserInfo(val));
    },
    getSinglePackageOffer: (val) => {
      dispatch(PackageAction.getSinglePackageOffer(val));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(PaymentSecondView);
