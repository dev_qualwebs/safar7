import React from 'react';
import {Modal, Text, View, StyleSheet, Image} from 'react-native';
import {TouchableOpacity} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';
import {
  heightPercentageToDP,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import COLOR from '../../../components/styles/Color';
import FONTS from '../../../components/styles/Fonts';
import IMAGES from '../../../components/styles/Images';
import Styles from '../../../components/styles/Styles';

export default function CardBottomSheet({
  cards,
  updateMasterState,
  modelVisibility,
}) {
  const handleSelect = (value, index) => {
    let data = cards;
    data.splice(index, 1);
    data.push(value);
    updateMasterState('cardData', data);
    updateMasterState('changeCardModel', false);
  };

  return (
    <View style={Styles.container}>
      <Modal
        animationType={'slide'}
        transparent={true}
        style={{backgroundColor: COLOR.BLACK}}
        visible={modelVisibility}
        onRequestClose={() => {
          console.log('Modal has been closed.');
        }}>
        <View style={styles.modal}>
          <View style={styles.modalBody}>
            <TouchableOpacity
              onPress={() => updateMasterState('changeCardModel', false)}
              style={styles.closeButton}>
              <Image
                source={require('../../styles/assets/close.png')}
                resizeMode={'contain'}
                style={{
                  width: 20,
                  height: 20,
                }}
              />
            </TouchableOpacity>
            <Text
              style={{alignSelf: 'center', marginVertical: 15, fontSize: 18}}>
              Select Card
            </Text>
            <ScrollView style={{paddingBottom: 15}}>
              {cards.map((value, index) => (
                <TouchableOpacity onPress={() => handleSelect(value, index)}>
                  <View
                    style={{
                      marginVertical: 10,
                      width: wp(90),
                      alignSelf: 'center',
                    }}>
                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                      }}>
                      <Text
                        style={[
                          Styles.small_label,
                          {color: COLOR.VOILET, width: null},
                        ]}>
                        CARD NUMBER
                      </Text>
                    </View>
                    <View
                      style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'space-between',
                        marginTop: 10,
                      }}>
                      <View
                        style={{
                          alignItems: 'center',
                          flexDirection: 'row',
                        }}>
                        <Image
                          style={{height: 25, width: 45, marginRight: 10}}
                          source={IMAGES.visa}
                          resizeMode={'contain'}
                        />
                        <Text
                          style={[
                            Styles.body_label,
                            {
                              color: COLOR.BLACK,
                              textAlign: 'left',
                              width: null,
                              marginRight: 0,
                            },
                          ]}>
                          {value.card_number}
                        </Text>
                      </View>
                    </View>
                  </View>
                </TouchableOpacity>
              ))}
            </ScrollView>
          </View>
        </View>
      </Modal>
    </View>
  );
}

const styles = StyleSheet.create({
  modal: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
  },
  modalBody: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    backgroundColor: COLOR.WHITE,
    borderWidth: 1,
    width: wp(100),
    height: heightPercentageToDP(75),
    marginTop: 50,
    borderColor: COLOR.BORDER,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
  image: {
    height: 120,
    width: 120,
    alignSelf: 'center',
    marginVertical: 20,
  },
  headingLabel: {
    fontFamily: FONTS.FAMILY_BOLD,
    fontSize: FONTS.MEDIUM,
    color: COLOR.BLACK,
  },
  singleLineView: {
    marginVertical: 20,
    borderBottomColor: COLOR.GRAY,
    width: wp(100),
    alignSelf: 'center',
    borderBottomWidth: 1,
  },
  borderView: {
    borderRadius: 10,
    borderWidth: 3,
    width: wp(90),
    borderColor: COLOR.GRAY,
    alignSelf: 'center',
  },
  buttonImage: {width: 20, height: 20, tintColor: COLOR.BLACK, marginRight: 5},
  closeButton: {
    width: 30,
    height: 30,
    position: 'absolute',
    right: 10,
    top: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
