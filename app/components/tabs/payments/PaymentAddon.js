import React from 'react';
import {FlatList, ScrollView} from 'react-native';
import {View, Image, Text, SafeAreaView, StyleSheet} from 'react-native';
import {TextInput, TouchableOpacity} from 'react-native';
import {
  widthPercentageToDP,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import NavigationBar from '../../commonView/NavigationBar';
import COLOR from '../../styles/Color';
import IMAGES from '../../styles/Images';
import Styles from '../../styles/Styles';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import FONTS from '../../styles/Fonts';
import API from '../../../api/Api';
import SimpleToast from 'react-native-simple-toast';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview';
import {DoneButtonKeyboard} from '../../commonView/CommonView';
import {connect} from 'react-redux';
import RequestAction from '../../../redux/action/RequestAction';

const TickView = (props) => {
  return (
    <View
      style={{
        flexDirection: 'row',
        width: wp(80),
        alignItems: 'center',
        marginBottom: 5,
      }}>
      <Image
        style={{width: 12, height: 12, marginRight: 10}}
        source={IMAGES.green_tick}
      />
      <Text style={[Styles.small_label, {flex: 1, width: null}]}>
        {props.name}
      </Text>
    </View>
  );
};

const api = new API();
class PaymentAddon extends React.Component {
  constructor(props) {
    super(props);
    let params = this.props.route.params;
    this.state = {
      addons: params.addons ? params.addons : null,
      type: params.type,
      val: params.val,
      travellingInsurance: false,
      travelVisa: false,
      selectedAddons: [],
      roaming: 0,
      router: 0,
      showTravelInsuarance: false,
      showTravelVisa: false,
      showTravelAssistance: false,
      showDataRoaming: false,
      showrouter: false,
      showMedical: false,
      showDigitalBag: false,
      showRental: false,
      showAirportTransport: false,
      showingLicence: false,
    };
  }

  componentDidMount() {
    this.getTripAddons();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.propsAddons != this.props.propsAddons) {
      this.setState({selectedAddons: this.props.propsAddons});
    }
  }

  addAddon = async (qty, id) => {
    let payload = JSON.stringify({
      trip_addon_id: id,
      quantity: qty,
    });

    api
      .addAddons(this.state.type, this.state.val, payload)
      .then((json) => {
        console.log(json.data.response);
        this.getTripAddons();
      })
      .catch((error) => {
        SimpleToast.show(error.response.data.message);
      });
  };

  removeAddons = async (id) => {
    let index = this.state.selectedAddons.findIndex((value) => value.id == id);
    if (index != -1) {
      api
        .removeAddons(this.state.type, this.state.val, id)
        .then((json) => {
          console.log(json.data.response);
          this.getTripAddons();
        })
        .catch((error) => {
          console.log(error);
        });
    }
  };

  getTripAddons = async () => {
    let that = this;
    api
      .getTripAddons(this.state.type, this.state.val)
      .then((json) => {
        that.setState({
          selectedAddons: json.data.response,
        });

        if (json.data.response.some((v) => v.id == 5)) {
          that.setState({
            roaming: json.data.response.find((v) => v.id == 5).pivot.quantity,
          });
        }
        if (json.data.response.some((v) => v.id == 6)) {
          that.setState({
            router: json.data.response.find((v) => v.id == 6).pivot.quantity,
          });
        }
      })
      .catch((error) => {
        console.log(error.response.data);
      });
  };

  TravelInsaurance = (props) => {
    return (
      <View
        style={[
          this.state.showTravelInsuarance ? style.main_view : style.hiddenView,
        ]}>
        <TouchableOpacity
          style={{}}
          onPress={() => {
            this.setState({
              showTravelInsuarance: !this.state.showTravelInsuarance,
            });
          }}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}>
            <Text
              style={[
                Styles.small_label,
                {
                  width: null,
                  fontFamily: FONTS.FAMILY_BOLD,
                  color: COLOR.BLACK,
                },
              ]}>
              TRAVEL INSURANCE
            </Text>
            <Image
              style={{height: 12, width: 12}}
              source={
                this.state.showTravelInsuarance
                  ? IMAGES.up_arrow
                  : IMAGES.down_arrow
              }
              resizeMode={'contain'}
            />
          </View>
        </TouchableOpacity>

        {this.state.showTravelInsuarance ? (
          <View
            style={[
              style.back_view,
              {
                backgroundColor: props.isSelected ? COLOR.WHITE : '#ECFCFF',
                borderColor: props.isSelected ? COLOR.GRAY : COLOR.GREEN,
              },
            ]}>
            <View style={{flexDirection: 'row'}}>
              <Image
                style={{
                  height: 30,
                  width: 39,
                  marginRight: 10,
                  tintColor: COLOR.GREEN,
                }}
                source={IMAGES.shield}
                resizeMode={'contain'}
              />
              <Text
                style={[
                  Styles.body_label,
                  {width: null, flex: 1, fontFamily: FONTS.FAMILY_BOLD},
                ]}>
                Travel Insurance
              </Text>
              <TouchableOpacity
                onPress={() =>
                  props.isSelected
                    ? SimpleToast.show('Apply For this Addon')
                    : this.removeAddons(1)
                }
                style={{
                  height: 30,
                  width: 30,
                  borderRadius: 5,
                  backgroundColor: props.isSelected ? COLOR.WHITE : COLOR.GREEN,
                  borderColor: COLOR.GRAY,
                  justifyContent: 'center',
                  alignItems: 'center',
                  borderWidth: 1,
                }}>
                <Image
                  resizeMode={'contain'}
                  style={{height: 20, width: 20, alignSelf: 'center'}}
                  source={IMAGES.tick_image}
                />
              </TouchableOpacity>
            </View>
            <View
              style={[Styles.line_view, {width: wp(85), marginVertical: 20}]}
            />
            <Text
              style={[
                Styles.small_label,
                {
                  width: null,
                  marginBottom: 10,
                  alignSelf: 'flex-start',
                  fontSize: 14,
                },
              ]}>
              Add Travel Insurance for the whole trip, including your flight,
              hotel, and more{' '}
              <Text style={{fontSize: 12, color: COLOR.LIGHT_TEXT}}>
                (Exclusions in the details)
              </Text>
            </Text>
            <TickView name={'Trip Cancellation & Curtailment'} />
            <TickView name={'Lost or Stollen Baggage'} />
            <TickView name={'Baggage Delay'} />
            <TickView name={'Medical Expenses'} />
            <TickView name={'Personal accidents & Hijack'} />
            <TickView name={'Personal property'} />
            <TickView name={'Repatriation'} />

            <TouchableOpacity
              onPress={() =>
                this.props.navigation.navigate('TravellersSelection', {
                  name: 'TravellerInsurance',
                  type: this.state.type,
                  val: this.state.val,
                })
              }
              style={[
                style.button,
                {backgroundColor: props.isSelected ? COLOR.GREEN : COLOR.WHITE},
              ]}>
              <Text
                style={[
                  Styles.button_font,
                  {
                    color: props.isSelected ? COLOR.WHITE : COLOR.BLACK,
                    fontFamily: FONTS.FAMILY_SEMIBOLD,
                  },
                ]}>
                {props.isSelected ? 'Apply' : 'Applied'}
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate('PolicyPage', {
                  screenTitle: 'Travel Insurance',
                });
              }}
              style={style.button}>
              <Text
                style={[
                  Styles.button_font,
                  {color: COLOR.BLACK, fontFamily: FONTS.FAMILY_SEMIBOLD},
                ]}>
                See More Details
              </Text>
            </TouchableOpacity>
          </View>
        ) : null}
      </View>
    );
  };

  TravelVisa = (props) => {
    return (
      <View
        style={[
          this.state.showTravelVisa ? style.main_view : style.hiddenView,
        ]}>
        <TouchableOpacity
          onPress={() => {
            this.setState({showTravelVisa: !this.state.showTravelVisa});
          }}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
            }}>
            <Text
              style={[
                Styles.small_label,
                {
                  width: null,
                  fontFamily: FONTS.FAMILY_BOLD,
                  color: COLOR.BLACK,
                },
              ]}>
              TRAVEL VISA
            </Text>
            <Image
              style={{height: 12, width: 12}}
              source={
                this.state.showTravelVisa ? IMAGES.up_arrow : IMAGES.down_arrow
              }
              resizeMode={'contain'}
            />
          </View>
        </TouchableOpacity>
        {this.state.showTravelVisa ? (
          <View
            style={[
              style.back_view,
              {
                backgroundColor: props.isSelected ? COLOR.WHITE : '#ECFCFF',
                borderColor: props.isSelected ? COLOR.GRAY : COLOR.GREEN,
              },
            ]}>
            <View style={{flexDirection: 'row'}}>
              <Image
                style={{height: 30, width: 39, marginRight: 10}}
                source={IMAGES.world_globe}
                resizeMode={'contain'}
              />
              <Text
                style={[
                  Styles.body_label,
                  {width: null, flex: 1, fontFamily: FONTS.FAMILY_BOLD},
                ]}>
                Travel Visa
              </Text>
              <TouchableOpacity
                onPress={() => {
                  props.isSelected
                    ? SimpleToast.show('Apply For this Addon')
                    : this.removeAddons(2);
                }}
                style={{
                  height: 30,
                  width: 30,
                  borderRadius: 5,
                  backgroundColor: props.isSelected ? COLOR.WHITE : COLOR.GREEN,
                  borderColor: COLOR.GRAY,
                  justifyContent: 'center',
                  alignItems: 'center',
                  borderWidth: 1,
                }}>
                <Image
                  resizeMode={'contain'}
                  style={{height: 20, width: 20, alignSelf: 'center'}}
                  source={IMAGES.tick_image}
                />
              </TouchableOpacity>
            </View>
            <View
              style={[Styles.line_view, {width: wp(85), marginVertical: 20}]}
            />
            <Text
              style={[
                Styles.small_label,
                {width: null, marginBottom: 10, alignSelf: 'flex-start'},
              ]}>
              Assisting to get a Travel Visa from different worldwide embassies
            </Text>
            <TickView name={'Filling all applications'} />
            <TickView name={'Schedule appointment with the Embassy'} />
            <TickView name={'Assisting to provide all requirements needed'} />

            <TouchableOpacity
              onPress={() =>
                this.props.navigation.navigate('TravellersSelection', {
                  name: 'TravelVisa',
                  type: this.state.type,
                  val: this.state.val,
                })
              }
              style={[
                style.button,
                {backgroundColor: props.isSelected ? COLOR.GREEN : COLOR.WHITE},
              ]}>
              <Text
                style={[
                  Styles.button_font,
                  {
                    color: props.isSelected ? COLOR.WHITE : COLOR.BLACK,
                    fontFamily: FONTS.FAMILY_SEMIBOLD,
                  },
                ]}>
                {props.isSelected ? 'Apply' : 'Applied'}
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate('PolicyPage', {
                  screenTitle: 'Travel Visa',
                });
              }}
              style={style.button}>
              <Text
                style={[
                  Styles.button_font,
                  {color: COLOR.BLACK, fontFamily: FONTS.FAMILY_SEMIBOLD},
                ]}>
                See More Details
              </Text>
            </TouchableOpacity>
          </View>
        ) : null}
      </View>
    );
  };

  TravelAssistance = (props) => {
    return (
      <View
        style={[
          this.state.showTravelAssistance ? style.main_view : style.hiddenView,
        ]}>
        <TouchableOpacity
          onPress={() => {
            this.setState({
              showTravelAssistance: !this.state.showTravelAssistance,
            });
          }}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}>
            <Text
              style={[
                Styles.small_label,
                {
                  width: null,
                  fontFamily: FONTS.FAMILY_BOLD,
                  color: COLOR.BLACK,
                },
              ]}>
              TRAVEL ASSISTANCE
            </Text>
            <Image
              style={{height: 12, width: 12}}
              source={
                this.state.showTravelAssistance
                  ? IMAGES.up_arrow
                  : IMAGES.down_arrow
              }
              resizeMode={'contain'}
            />
          </View>
        </TouchableOpacity>
        {this.state.showTravelAssistance ? (
          <View
            style={[
              style.back_view,
              {
                backgroundColor: props.isSelected ? COLOR.WHITE : '#ECFCFF',
                borderColor: props.isSelected ? COLOR.GRAY : COLOR.GREEN,
              },
            ]}>
            <View style={{flexDirection: 'row'}}>
              <Image
                style={{height: 30, width: 39, marginRight: 10}}
                source={IMAGES.mike}
                resizeMode={'contain'}
              />
              <Text
                style={[
                  Styles.body_label,
                  {width: null, flex: 1, fontFamily: FONTS.FAMILY_BOLD},
                ]}>
                Travel Assistance
              </Text>
              <TouchableOpacity
                onPress={() => {
                  props.isSelected ? this.addAddon(1, 3) : this.removeAddons(3);
                }}
                style={{
                  height: 30,
                  width: 30,
                  borderRadius: 5,
                  backgroundColor: props.isSelected ? COLOR.WHITE : COLOR.GREEN,
                  borderColor: COLOR.GRAY,
                  justifyContent: 'center',
                  alignItems: 'center',
                  borderWidth: 1,
                }}>
                <Image
                  resizeMode={'contain'}
                  style={{height: 20, width: 20, alignSelf: 'center'}}
                  source={IMAGES.tick_image}
                />
              </TouchableOpacity>
            </View>
            <View
              style={[Styles.line_view, {width: wp(85), marginVertical: 20}]}
            />
            <Text
              style={[
                Styles.small_label,
                {width: null, marginBottom: 10, alignSelf: 'flex-start'},
              ]}>
              You can get 24/7 Online assistance through
            </Text>
            <TickView name={'100% Trip Cancellation'} />
            <TickView name={'Lost or Stolen Baggage'} />
            <TickView name={'Trip Inerruption'} />

            <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate('PolicyPage', {
                  screenTitle: 'Travel Assistance',
                });
              }}
              style={style.button}>
              <Text
                style={[
                  Styles.button_font,
                  {color: COLOR.BLACK, fontFamily: FONTS.FAMILY_SEMIBOLD},
                ]}>
                See More Details
              </Text>
            </TouchableOpacity>
          </View>
        ) : null}
      </View>
    );
  };

  DataRoaming = (props) => {
    return (
      <View
        style={[
          this.state.showDataRoaming ? style.main_view : style.hiddenView,
        ]}>
        <TouchableOpacity
          onPress={() => {
            this.setState({showDataRoaming: !this.state.showDataRoaming});
          }}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}>
            <Text
              style={[
                Styles.small_label,
                {
                  width: null,
                  fontFamily: FONTS.FAMILY_BOLD,
                  color: COLOR.BLACK,
                },
              ]}>
              DATA ROAMING
            </Text>
            <Image
              style={{height: 12, width: 12}}
              source={
                this.state.showDataRoaming ? IMAGES.up_arrow : IMAGES.down_arrow
              }
              resizeMode={'contain'}
            />
          </View>
        </TouchableOpacity>
        {this.state.showDataRoaming ? (
          <View
            style={[
              style.back_view,
              {
                backgroundColor: props.isSelected ? COLOR.WHITE : '#ECFCFF',
                borderColor: props.isSelected ? COLOR.GRAY : COLOR.GREEN,
              },
            ]}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
              }}>
              <Image
                style={{height: 30, width: 39, marginRight: 10}}
                source={IMAGES.sim_card}
                resizeMode={'contain'}
              />
              <Text
                style={[
                  Styles.body_label,
                  {width: null, flex: 1, fontFamily: FONTS.FAMILY_BOLD},
                ]}>
                Data Roaming
              </Text>
              <View
                style={{
                  flex: 1,
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'flex-end',
                }}>
                <TouchableOpacity
                  onPress={() => {
                    if (this.state.roaming == 1) {
                      this.removeAddons(5);
                    }
                    if (this.state.roaming != 0) {
                      this.setState({
                        roaming: this.state.roaming - 1,
                      });
                    }
                  }}
                  style={{
                    height: 25,
                    width: 25,
                    borderRadius: 15,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Image
                    style={{height: 30, width: 30}}
                    resizeMode={'contain'}
                    source={IMAGES.minus_image}
                  />
                </TouchableOpacity>
                <Text
                  style={[
                    Styles.button_font,
                    {width: 40, color: COLOR.BLACK, textAlign: 'center'},
                  ]}>
                  {this.state.roaming}
                </Text>
                <TouchableOpacity
                  onPress={() => {
                    this.setState(
                      {
                        roaming: this.state.roaming + 1,
                      },
                      () => {
                        this.addAddon(this.state.roaming, 5);
                      },
                    );
                  }}
                  style={{
                    height: 30,
                    width: 30,
                    borderWidth: 1,
                    borderRadius: 15,
                    borderColor: COLOR.GREEN,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Image
                    style={{height: 20, width: 20}}
                    source={IMAGES.plus_image}
                  />
                </TouchableOpacity>
              </View>
            </View>
            <View
              style={[Styles.line_view, {width: wp(85), marginVertical: 20}]}
            />
            <Text
              style={[
                Styles.small_label,
                {width: null, marginBottom: 10, alignSelf: 'flex-start'},
              ]}>
              4G-5G Data Roaming SIM Card (Unlimited Data) From the local
              country before travel{'\n\n'}Best Price and Network Guarantee for
              you Trip Plan
            </Text>
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate('PolicyPage', {
                  screenTitle: 'Data Roaming',
                });
              }}
              style={style.button}>
              <Text
                style={[
                  Styles.button_font,
                  {color: COLOR.BLACK, fontFamily: FONTS.FAMILY_SEMIBOLD},
                ]}>
                See More Details
              </Text>
            </TouchableOpacity>
          </View>
        ) : null}
      </View>
    );
  };

  MedicalKitBag = (props) => {
    return (
      <View
        style={[this.state.showMedical ? style.main_view : style.hiddenView]}>
        <TouchableOpacity
          onPress={() => {
            this.setState({showMedical: !this.state.showMedical});
          }}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}>
            <Text
              style={[
                Styles.small_label,
                {
                  width: null,
                  fontFamily: FONTS.FAMILY_BOLD,
                  color: COLOR.BLACK,
                },
              ]}>
              MEDICAL KIT BAG
            </Text>
            <Image
              style={{height: 12, width: 12}}
              source={
                this.state.showMedical ? IMAGES.up_arrow : IMAGES.down_arrow
              }
              resizeMode={'contain'}
            />
          </View>
        </TouchableOpacity>
        {this.state.showMedical ? (
          <View
            style={[
              style.back_view,
              {
                backgroundColor: props.isSelected ? COLOR.WHITE : '#ECFCFF',
                borderColor: props.isSelected ? COLOR.GRAY : COLOR.GREEN,
              },
            ]}>
            <View style={{flexDirection: 'row'}}>
              <Image
                style={{height: 30, width: 39, marginRight: 10}}
                source={IMAGES.first_aid}
                resizeMode={'contain'}
              />
              <Text
                style={[
                  Styles.body_label,
                  {width: null, flex: 1, fontFamily: FONTS.FAMILY_BOLD},
                ]}>
                Medical Kit Bag
              </Text>
              <TouchableOpacity
                style={{
                  height: 25,
                  width: 25,
                  borderWidth: 1,
                  borderRadius: 5,
                  backgroundColor: props.isSelected ? COLOR.WHITE : COLOR.GREEN,
                  borderColor: COLOR.GRAY,
                }}
                onPress={() => {
                  props.isSelected ? this.addAddon(1, 9) : this.removeAddons(9);
                }}>
                <Image
                  resizeMode={'contain'}
                  style={{height: 20, width: 20, alignSelf: 'center'}}
                  source={IMAGES.tick_image}
                />
              </TouchableOpacity>
            </View>
            <View
              style={[Styles.line_view, {width: wp(85), marginVertical: 20}]}
            />
            <Text
              style={[
                Styles.small_label,
                {width: null, marginBottom: 10, alignSelf: 'flex-start'},
              ]}>
              Travel Medical Kit bag contains on all the necessary medication
              and products for traveller.
            </Text>
            <TickView
              name={'Important medications (Analgesic, Antihistamine)'}
            />
            <TickView
              name={'Dressing (Bandage, Scissors, gloves, Sterile gauze)'}
            />
            <TickView name={'More Details'} />

            <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate('PolicyPage', {
                  screenTitle: 'Medical Kit Bag',
                });
              }}
              style={style.button}>
              <Text style={[Styles.button_font, {color: COLOR.BLACK}]}>
                See More Details
              </Text>
            </TouchableOpacity>
          </View>
        ) : null}
      </View>
    );
  };

  Router = (props) => {
    return (
      <View
        style={[this.state.showrouter ? style.main_view : style.hiddenView]}>
        <TouchableOpacity
          onPress={() => {
            this.setState({showrouter: !this.state.showrouter});
          }}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}>
            <Text
              style={[
                Styles.small_label,
                {
                  width: null,
                  fontFamily: FONTS.FAMILY_BOLD,
                  color: COLOR.BLACK,
                },
              ]}>
              ROUTER FOR DATA ROAMING
            </Text>
            <Image
              style={{height: 12, width: 12}}
              source={
                this.state.showrouter ? IMAGES.up_arrow : IMAGES.down_arrow
              }
              resizeMode={'contain'}
            />
          </View>
        </TouchableOpacity>
        {this.state.showrouter ? (
          <View
            style={[
              style.back_view,
              {
                backgroundColor: props.isSelected ? COLOR.WHITE : '#ECFCFF',
                borderColor: props.isSelected ? COLOR.GRAY : COLOR.GREEN,
              },
            ]}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
              }}>
              <Image
                style={{height: 30, width: 39, marginRight: 10}}
                source={IMAGES.mike}
                resizeMode={'contain'}
              />
              <Text
                style={[
                  Styles.body_label,
                  {width: null, flex: 1, fontFamily: FONTS.FAMILY_BOLD},
                ]}>
                Router for Data Roaming
              </Text>
              <View
                style={{
                  flex: 1,
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'flex-end',
                }}>
                <TouchableOpacity
                  onPress={() => {
                    if (this.state.router == 1) {
                      this.removeAddons(6);
                    }
                    if (this.state.router != 0) {
                      this.setState({
                        router: this.state.router - 1,
                      });
                    }
                  }}
                  style={{
                    height: 25,
                    width: 25,
                    borderRadius: 15,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Image
                    style={{height: 30, width: 30}}
                    resizeMode={'contain'}
                    source={IMAGES.minus_image}
                  />
                </TouchableOpacity>
                <Text
                  style={[
                    Styles.button_font,
                    {width: 40, color: COLOR.BLACK, textAlign: 'center'},
                  ]}>
                  {this.state.router}
                </Text>
                <TouchableOpacity
                  onPress={() => {
                    this.setState(
                      {
                        router: this.state.router + 1,
                      },
                      () => {
                        this.addAddon(this.state.router, 6);
                      },
                    );
                  }}
                  style={{
                    height: 30,
                    width: 30,
                    borderWidth: 1,
                    borderRadius: 15,
                    borderColor: COLOR.GREEN,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Image
                    style={{height: 20, width: 20}}
                    source={IMAGES.plus_image}
                  />
                </TouchableOpacity>
              </View>
            </View>
            <View
              style={[Styles.line_view, {width: wp(85), marginVertical: 20}]}
            />
            <Text
              style={[
                Styles.small_label,
                {width: null, marginBottom: 10, alignSelf: 'flex-start'},
              ]}>
              Rental Service of Portable Wi-Fi Router (4G-5G) to take during
              your trip.{'\n\n'} * Price including insurance fees
            </Text>
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate('PolicyPage', {
                  screenTitle: 'Router for Data Roaming',
                });
              }}
              style={style.button}>
              <Text
                style={[
                  Styles.button_font,
                  {color: COLOR.BLACK, fontFamily: FONTS.FAMILY_SEMIBOLD},
                ]}>
                See More Details
              </Text>
            </TouchableOpacity>
          </View>
        ) : null}
      </View>
    );
  };

  TravelBag = (props) => {
    return (
      <View
        style={[
          this.state.showDigitalBag ? style.main_view : style.hiddenView,
        ]}>
        <TouchableOpacity
          onPress={() => {
            this.setState({showDigitalBag: !this.state.showDigitalBag});
          }}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}>
            <Text
              style={[
                Styles.small_label,
                {
                  width: null,
                  fontFamily: FONTS.FAMILY_BOLD,
                  color: COLOR.BLACK,
                },
              ]}>
              TRAVEL BAG
            </Text>
            <Image
              style={{height: 12, width: 12}}
              source={
                this.state.showDigitalBag ? IMAGES.up_arrow : IMAGES.down_arrow
              }
              resizeMode={'contain'}
            />
          </View>
        </TouchableOpacity>
        {this.state.showDigitalBag ? (
          <View
            style={[
              style.back_view,
              {
                backgroundColor: props.isSelected ? COLOR.WHITE : '#ECFCFF',
                borderColor: props.isSelected ? COLOR.GRAY : COLOR.GREEN,
              },
            ]}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
              }}>
              <Image
                style={{height: 30, width: 39, marginRight: 10}}
                source={IMAGES.mike}
                resizeMode={'contain'}
              />
              <Text
                style={[
                  Styles.body_label,
                  {width: null, flex: 1, fontFamily: FONTS.FAMILY_BOLD},
                ]}>
                Travel Digital Bag
              </Text>
              <TouchableOpacity
                style={{
                  height: 25,
                  width: 25,
                  borderWidth: 1,
                  borderRadius: 5,
                  backgroundColor: props.isSelected ? COLOR.WHITE : COLOR.GREEN,
                  borderColor: COLOR.GRAY,
                }}
                onPress={() => {
                  props.isSelected ? this.addAddon(1, 8) : this.removeAddons(8);
                }}>
                <Image
                  resizeMode={'contain'}
                  style={{height: 20, width: 20, alignSelf: 'center'}}
                  source={IMAGES.tick_image}
                />
              </TouchableOpacity>
            </View>
            <View
              style={[Styles.line_view, {width: wp(85), marginVertical: 20}]}
            />
            <Text
              style={[
                Styles.small_label,
                {width: null, marginBottom: 10, alignSelf: 'flex-start'},
              ]}>
              Providing a travel digital bag for an additional fee contains on
              all the necessary electroni accessories.
            </Text>
            <TickView name={'Universal travel adapter'} />
            <TickView name={'Portable Charger'} />
            <TickView name={'Smartphones USB Cables (Iphone and Android)  '} />
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate('PolicyPage', {
                  screenTitle: 'Travel Bag',
                });
              }}
              style={style.button}>
              <Text style={[Styles.button_font, {color: COLOR.BLACK}]}>
                See More Details
              </Text>
            </TouchableOpacity>
          </View>
        ) : null}
      </View>
    );
  };

  GPSDeviceRental = (props) => {
    return (
      <View
        style={[this.state.showRental ? style.main_view : style.hiddenView]}>
        <TouchableOpacity
          onPress={() => {
            this.setState({showRental: !this.state.showRental});
          }}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}>
            <Text
              style={[
                Styles.small_label,
                {
                  width: null,
                  fontFamily: FONTS.FAMILY_BOLD,
                  color: COLOR.BLACK,
                },
              ]}>
              GPS DEVICE RENTAL
            </Text>
            <Image
              style={{height: 12, width: 12}}
              source={
                this.state.showRental ? IMAGES.up_arrow : IMAGES.down_arrow
              }
              resizeMode={'contain'}
            />
          </View>
        </TouchableOpacity>
        {this.state.showRental ? (
          <View
            style={[
              style.back_view,
              {
                backgroundColor: props.isSelected ? COLOR.WHITE : '#ECFCFF',
                borderColor: props.isSelected ? COLOR.GRAY : COLOR.GREEN,
              },
            ]}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
              }}>
              <Image
                style={{height: 30, width: 39, marginRight: 10}}
                source={IMAGES.paper_plane}
                resizeMode={'contain'}
              />
              <Text
                style={[
                  Styles.body_label,
                  {width: null, flex: 1, fontFamily: FONTS.FAMILY_BOLD},
                ]}>
                GPS Device Rental
              </Text>
              <TouchableOpacity
                onPress={() => {
                  props.isSelected ? this.addAddon(1, 7) : this.removeAddons(7);
                }}
                style={{
                  height: 25,
                  width: 25,
                  borderWidth: 1,
                  borderRadius: 5,
                  backgroundColor: props.isSelected ? COLOR.WHITE : COLOR.GREEN,
                  borderColor: COLOR.GRAY,
                  justifyContent: 'center',
                }}>
                <Image
                  style={{
                    height: 20,
                    width: 20,
                    alignSelf: 'center',
                  }}
                  source={IMAGES.tick_image}
                  resizeMode={'contain'}
                />
              </TouchableOpacity>
            </View>
            <View
              style={[Styles.line_view, {width: wp(85), marginVertical: 20}]}
            />
            <Text
              style={[
                Styles.small_label,
                {width: null, marginBottom: 10, alignSelf: 'flex-start'},
              ]}>
              Providing a portable GPS Navigation for an additional fee to keep
              you on track and make your driving to a new destination feel easy.
            </Text>

            <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate('PolicyPage', {
                  screenTitle: 'GPS Device Rental',
                });
              }}
              style={style.button}>
              <Text
                style={[
                  Styles.button_font,
                  {color: COLOR.BLACK, fontFamily: FONTS.FAMILY_SEMIBOLD},
                ]}>
                See More Details
              </Text>
            </TouchableOpacity>
          </View>
        ) : null}
      </View>
    );
  };

  AirportTransportation = (props) => {
    return (
      <View
        style={[
          this.state.showAirportTransport ? style.main_view : style.hiddenView,
        ]}>
        <TouchableOpacity
          onPress={() => {
            this.setState({
              showAirportTransport: !this.state.showAirportTransport,
            });
          }}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}>
            <Text
              style={[
                Styles.small_label,
                {
                  width: null,
                  fontFamily: FONTS.FAMILY_BOLD,
                  color: COLOR.BLACK,
                },
              ]}>
              AIR TRANSPORTATION
            </Text>
            <Image
              style={{height: 12, width: 12}}
              source={
                this.state.showAirportTransport
                  ? IMAGES.up_arrow
                  : IMAGES.down_arrow
              }
              resizeMode={'contain'}
            />
          </View>
        </TouchableOpacity>
        {this.state.showAirportTransport ? (
          <View
            style={[
              style.back_view,
              {
                backgroundColor: props.isSelected ? COLOR.WHITE : '#ECFCFF',
                borderColor: props.isSelected ? COLOR.GRAY : COLOR.GREEN,
              },
            ]}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
              }}>
              <Image
                style={{height: 30, width: 39, marginRight: 10}}
                source={IMAGES.green_car}
                resizeMode={'contain'}
              />
              <Text
                style={[
                  Styles.body_label,
                  {width: null, flex: 1, fontFamily: FONTS.FAMILY_BOLD},
                ]}>
                Airport Transportation
              </Text>
              <TouchableOpacity
                onPress={() => {
                  props.isSelected
                    ? SimpleToast.show('Apply for this addon')
                    : this.removeAddons(4);
                }}
                style={{
                  height: 25,
                  width: 25,
                  borderWidth: 1,
                  borderRadius: 5,
                  backgroundColor: props.isSelected ? COLOR.WHITE : COLOR.GREEN,
                  borderColor: COLOR.GRAY,
                  justifyContent: 'center',
                }}>
                <Image
                  style={{
                    height: 20,
                    width: 20,
                    alignSelf: 'center',
                  }}
                  source={IMAGES.tick_image}
                  resizeMode={'contain'}
                />
              </TouchableOpacity>
            </View>
            <View
              style={[Styles.line_view, {width: wp(85), marginVertical: 20}]}
            />
            <Text
              style={[
                Styles.small_label,
                {width: null, marginBottom: 10, alignSelf: 'flex-start'},
              ]}>
              Assisting to get a suitable private transportation from or to the
              airport with reasonable price.
            </Text>

            <TouchableOpacity
              onPress={() =>
                this.props.navigation.navigate('TransportationScreen', {
                  type: this.state.type,
                  val: this.state.val,
                })
              }
              style={[
                style.button,
                {backgroundColor: props.isSelected ? COLOR.GREEN : COLOR.WHITE},
              ]}>
              <Text
                style={[
                  Styles.button_font,
                  {
                    color: props.isSelected ? COLOR.WHITE : COLOR.BLACK,
                    fontFamily: FONTS.FAMILY_SEMIBOLD,
                  },
                ]}>
                {props.isSelected ? 'Apply' : 'Applied'}
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate('PolicyPage', {
                  screenTitle: 'Air Transportation',
                });
              }}
              style={style.button}>
              <Text
                style={[
                  Styles.button_font,
                  {color: COLOR.BLACK, fontFamily: FONTS.FAMILY_SEMIBOLD},
                ]}>
                See More Details
              </Text>
            </TouchableOpacity>
          </View>
        ) : null}
      </View>
    );
  };

  DrivingLicense = (props) => {
    return (
      <View
        style={[
          this.state.showingLicence ? style.main_view : style.hiddenView,
        ]}>
        <TouchableOpacity
          onPress={() => {
            this.setState({showingLicence: !this.state.showingLicence});
          }}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}>
            <Text
              style={[
                Styles.small_label,
                {
                  width: null,
                  fontFamily: FONTS.FAMILY_BOLD,
                  color: COLOR.BLACK,
                },
              ]}>
              INTERNATIONAL DRIVING LICENSE
            </Text>
            <Image
              style={{height: 12, width: 12}}
              source={
                this.state.showingLicence ? IMAGES.up_arrow : IMAGES.down_arrow
              }
              resizeMode={'contain'}
            />
          </View>
        </TouchableOpacity>
        {this.state.showingLicence ? (
          <View
            style={[
              style.back_view,
              {
                backgroundColor: props.isSelected ? COLOR.WHITE : '#ECFCFF',
                borderColor: props.isSelected ? COLOR.GRAY : COLOR.GREEN,
              },
            ]}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
              }}>
              <Image
                style={{height: 30, width: 39, marginRight: 10}}
                source={IMAGES.green_car}
                resizeMode={'contain'}
              />
              <Text
                style={[
                  Styles.body_label,
                  {width: null, flex: 1, fontFamily: FONTS.FAMILY_BOLD},
                ]}>
                International Driving License
              </Text>
              <TouchableOpacity
                onPress={() => {
                  props.isSelected
                    ? SimpleToast.show('Apply for this addon')
                    : this.removeAddons(10);
                }}
                style={{
                  height: 25,
                  width: 25,
                  borderWidth: 1,
                  borderRadius: 5,
                  backgroundColor: props.isSelected ? COLOR.WHITE : COLOR.GREEN,
                  borderColor: COLOR.GRAY,
                  justifyContent: 'center',
                }}>
                <Image
                  style={{
                    height: 25,
                    width: 25,
                    borderWidth: 1,
                    borderRadius: 5,
                    backgroundColor: props.isSelected
                      ? COLOR.WHITE
                      : COLOR.GREEN,
                    borderColor: COLOR.GRAY,
                  }}
                  source={IMAGES.tick_image}
                  resizeMode={'contain'}
                />
              </TouchableOpacity>
            </View>
            <View
              style={[Styles.line_view, {width: wp(85), marginVertical: 20}]}
            />
            <Text
              style={[
                Styles.small_label,
                {width: null, marginBottom: 10, alignSelf: 'flex-start'},
              ]}>
              Assisting to get an international driving license to drive or rent
              car in several foreign countries valid for one year.
            </Text>
            <TickView name={'Accepted in more than 150 Countries Worldwide.'} />
            <TickView name={'Extra Details'} />
            <TouchableOpacity
              onPress={() =>
                this.props.navigation.navigate('TravellersSelection', {
                  name: 'InternationalDrivingLicense',
                  type: this.state.type,
                  val: this.state.val,
                })
              }
              style={[
                style.button,
                {backgroundColor: props.isSelected ? COLOR.GREEN : COLOR.WHITE},
              ]}>
              <Text
                style={[
                  Styles.button_font,
                  {
                    color: props.isSelected ? COLOR.WHITE : COLOR.BLACK,
                    fontFamily: FONTS.FAMILY_SEMIBOLD,
                  },
                ]}>
                {props.isSelected ? 'Apply' : 'Applied'}
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate('PolicyPage', {
                  screenTitle: 'International Driving License',
                });
              }}
              style={style.button}>
              <Text
                style={[
                  Styles.button_font,
                  {color: COLOR.BLACK, fontFamily: FONTS.FAMILY_SEMIBOLD},
                ]}>
                See More Details
              </Text>
            </TouchableOpacity>
          </View>
        ) : null}
      </View>
    );
  };

  render() {
    return (
      <SafeAreaView style={[Styles.container]}>
        <KeyboardAwareScrollView
          bounces={false}
          style={{flex: 1}}
          keyboardShouldPersistTaps={'handled'}
          enableResetScrollToCoords={false}>
          <NavigationBar
            handleBack={() => {
              this.props.getAddons(this.state.type, this.state.val);
              this.props.navigation.goBack();
            }}
            prop={this.props}
            height={40}
            name={'Addons'}
          />
          <Text
            style={[
              Styles.small_label,
              {
                color: COLOR.LIGHT_GREEN,
                marginVertical: 20,
                alignSelf: 'center',
                width: wp(85),
              },
            ]}>
            Addons will Cover the whole duration of your trip, and it will
            Customized for your geographic trip.
          </Text>
          <FlatList
            data={this.state.addons}
            style={{flex: 1}}
            renderItem={({item}) => {
              const id = item.addon.id;
              switch (id) {
                case 1:
                  return (
                    <this.TravelInsaurance
                      isSelected={
                        this.state.selectedAddons.findIndex(
                          (val) => val.id == 1,
                        ) == -1
                          ? true
                          : false
                      }
                    />
                  );
                case 2:
                  return (
                    <this.TravelVisa
                      isSelected={
                        this.state.selectedAddons.findIndex(
                          (val) => val.id == 2,
                        ) == -1
                          ? true
                          : false
                      }
                    />
                  );
                case 3:
                  return (
                    <this.TravelAssistance
                      isSelected={
                        this.state.selectedAddons.findIndex(
                          (val) => val.id == 3,
                        ) == -1
                          ? true
                          : false
                      }
                    />
                  );
                case 4:
                  return (
                    <this.AirportTransportation
                      isSelected={
                        this.state.selectedAddons.findIndex(
                          (val) => val.id == 4,
                        ) == -1
                          ? true
                          : false
                      }
                    />
                  );
                case 5:
                  return (
                    <this.DataRoaming
                      isSelected={
                        this.state.selectedAddons.findIndex(
                          (val) => val.id == 5,
                        ) == -1
                          ? true
                          : false
                      }
                    />
                  );
                case 6:
                  return (
                    <this.Router
                      isSelected={
                        this.state.selectedAddons.findIndex(
                          (val) => val.id == 6,
                        ) == -1
                          ? true
                          : false
                      }
                    />
                  );
                case 7:
                  return (
                    <this.GPSDeviceRental
                      isSelected={
                        this.state.selectedAddons.findIndex(
                          (val) => val.id == 7,
                        ) == -1
                          ? true
                          : false
                      }
                    />
                  );
                case 8:
                  return (
                    <this.TravelBag
                      isSelected={
                        this.state.selectedAddons.findIndex(
                          (val) => val.id == 8,
                        ) == -1
                          ? true
                          : false
                      }
                    />
                  );
                case 9:
                  return (
                    <this.MedicalKitBag
                      isSelected={
                        this.state.selectedAddons.findIndex(
                          (val) => val.id == 9,
                        ) == -1
                          ? true
                          : false
                      }
                    />
                  );
                case 10:
                  return (
                    <this.DrivingLicense
                      isSelected={
                        this.state.selectedAddons.findIndex(
                          (val) => val.id == 10,
                        ) == -1
                          ? true
                          : false
                      }
                    />
                  );
                default:
                  <View />;
              }
            }}
          />
          {this.state.addons == null ? (
            <>
              <this.TravelInsaurance
                isSelected={
                  this.state.selectedAddons.findIndex((val) => val.id == 1) ==
                  -1
                    ? true
                    : false
                }
              />
              <this.TravelVisa
                isSelected={
                  this.state.selectedAddons.findIndex((val) => val.id == 2) ==
                  -1
                    ? true
                    : false
                }
              />
              <this.TravelAssistance
                isSelected={
                  this.state.selectedAddons.findIndex((val) => val.id == 3) ==
                  -1
                    ? true
                    : false
                }
              />
              <this.DataRoaming
                isSelected={
                  this.state.selectedAddons.findIndex((val) => val.id == 5) ==
                  -1
                    ? true
                    : false
                }
              />
              <this.Router
                isSelected={
                  this.state.selectedAddons.findIndex((val) => val.id == 6) ==
                  -1
                    ? true
                    : false
                }
              />
              <this.MedicalKitBag
                isSelected={
                  this.state.selectedAddons.findIndex((val) => val.id == 9) ==
                  -1
                    ? true
                    : false
                }
              />
              <this.TravelBag
                isSelected={
                  this.state.selectedAddons.findIndex((val) => val.id == 8) ==
                  -1
                    ? true
                    : false
                }
              />
              <this.GPSDeviceRental
                isSelected={
                  this.state.selectedAddons.findIndex((val) => val.id == 7) ==
                  -1
                    ? true
                    : false
                }
              />
              <this.AirportTransportation
                isSelected={
                  this.state.selectedAddons.findIndex((val) => val.id == 4) ==
                  -1
                    ? true
                    : false
                }
              />
              <this.DrivingLicense
                isSelected={
                  this.state.selectedAddons.findIndex((val) => val.id == 10) ==
                  -1
                    ? true
                    : false
                }
              />
            </>
          ) : null}
          <Text
            style={[
              Styles.subheading_label,
              {marginTop: 20, marginBottom: 10},
            ]}>
            Comments
          </Text>
          <View
            style={[
              style.back_view,
              {
                height: 120,
                width: wp(90),
                alignSelf: 'center',
                marginBottom: 20,
              },
            ]}>
            <TextInput
              inputAccessoryViewID={'uniqueID'}
              placeholderTextColor={COLOR.LIGHT_TEXT_COLOR}
              placeholder={'Write your comments'}
            />
            <DoneButtonKeyboard />
          </View>

          <TouchableOpacity
            onPress={() => {
              this.setState({modalVisible: false}, (val) => {
                this.props.getAddons(this.state.type, this.state.val);
                this.props.navigation.navigate('PaymentScreen');
              });
            }}
            style={[
              style.button,
              {
                marginTop: 20,
                height: 50,
                borderColor: COLOR.GRAY,
                borderWidth: 1,
                width: widthPercentageToDP(90),
                alignSelf: 'center',
                marginBottom: 10,
              },
            ]}>
            <Text style={[Styles.button_font, {color: COLOR.BLACK}]}>
              Skip to Payments
            </Text>
          </TouchableOpacity>
        </KeyboardAwareScrollView>
      </SafeAreaView>
    );
  }
}

const style = StyleSheet.create({
  main_view: {
    width: wp(90),
    alignSelf: 'center',
    marginVertical: 20,
  },
  back_view: {
    backgroundColor: COLOR.WHITE,
    borderRadius: 10,
    shadowColor: COLOR.WHITE,
    shadowOffset: {width: 0.2, height: 0.2},
    shadowOpacity: 0.2,
    paddingHorizontal: 10,
    paddingVertical: 20,
    borderWidth: 1,
    borderColor: COLOR.GRAY,
    elevation: 3,
    marginTop: 10,
  },
  hiddenView: {
    backgroundColor: COLOR.WHITE,
    alignSelf: 'center',
    width: wp(95),
    borderRadius: 10,
    shadowColor: COLOR.WHITE,
    shadowOffset: {width: 0.2, height: 0.2},
    shadowOpacity: 0.2,
    paddingHorizontal: 10,
    paddingVertical: 10,
    marginVertical: 5,
    borderWidth: 1,
    borderColor: COLOR.GRAY,
    elevation: 3,
  },
  button: {
    height: 40,
    justifyContent: 'center',
    marginTop: 20,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: COLOR.GRAY,
    backgroundColor: COLOR.WHITE,
  },
});

const mapStateToProps = (state, ownProps) => {
  return {
    propsAddons: state.requestReducer.tripAddons,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getAddons: (type, val) => {
      dispatch(RequestAction.getTripAddons(type, val));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(PaymentAddon);
