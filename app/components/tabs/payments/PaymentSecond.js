import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  TouchableOpacity,
  FlatList,
  Alert,
} from 'react-native';
import COLOR from '../../styles/Color';
import FONTS from '../../styles/Fonts';
import Styles from '../../styles/Styles';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP,
} from 'react-native-responsive-screen';
import IMAGES from '../../styles/Images';
import {Platform} from 'react-native';
import PaymentSummary from './PaymentSummary';
import PaymentTravellersView from './PaymentTravellersView';
import PaymentSecondView from './PaymentSecondView';
import {connect} from 'react-redux';
import RequestAction from '../../../redux/action/RequestAction';
import TripDateView from '../../commonView/TripDateView';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview';
import PaymentServiceView from './PaymentServiceView';

class PaymentSecond extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      screen: 1,
      offer: '',
      offerPayment: 0,
      travellingModes:[]
    };

    this.changeScreen = this.changeScreen.bind(this);
    this.getScreenFromState = this.getScreenFromState.bind(this);
    this.getTitleFromState = this.getTitleFromState.bind(this);
    // this.props.getSingleTripOffer(this.props.route.params.id);
  }

  componentDidUpdate(prevProps) {
    if (this.props.singleTripOffer != prevProps.singleTripOffer && this.props.singleTripOffer) {
     
      console.log('this.props.singleTripOffer.active_offer_travelling_modes',this.props.singleTripOffer.active_offer_travelling_modes);
      // this.setState({});
    }
  }

  componentDidMount(){
    this.props.getSingleTripOffer(this.props.route.params.id);
  }

  changeScreen = (screen) => {
    this.setState({
      screen: screen,
    });
  };

  getScreenFromState = (state) => {
    switch (state) {
      case 1:
        return (
          <PaymentSummary
            travellingModes={this.props.singleTripOffer && this.props.singleTripOffer.active_offer_travelling_modes}
            changeScreen={this.changeScreen}
            offerPayment={this.props.singleTripOffer && this.props.singleTripOffer.offer_payment}
          />
        );
      case 2:
        return (
          <PaymentTravellersView
            travellers={this.props.singleTripOffer && this.props.singleTripOffer.travellers}
            changeScreen={this.changeScreen}
            offerPayment={this.props.singleTripOffer && this.props.singleTripOffer.offer_payment}
          />
        );
      case 3:
        return (
          <PaymentSecondView
            navigation={this.props.navigation}
            isPackage={false}
            offer={this.props.singleTripOffer && this.props.singleTripOffer}
            activeOffer={this.props.singleTripOffer && this.props.singleTripOffer.active_offers[0]}
            packageId={this.props.singleTripOffer && this.props.singleTripOffer.id}
          />
        );
    }
  };
  getTitleFromState(state) {
    switch (state) {
      case 1:
        return 'Trip Summary';
      case 2:
        return 'Travellers';
      case 3:
        return 'Payment';
    }
  }

  render() {
    return (
      // <View style={Styles.container}>
      <View style={styles.container}>
        {/* <KeyboardAwareScrollView
          bounces={false}
          showsVerticalScrollIndicator={false}
          style={{flex: 1, backgroundColor: COLOR.BACKGROUND}}> */}
          <View style={styles.statusBar}>
            <Text style={styles.title}>
              {this.state.screen == 2
                ? 'Travellers'
                : this.state.screen == 3
                ? 'Payment'
                : 'Trip Summary'}
            </Text>
            <View style={styles.statusView}>
              <View style={{width: 80}}>
                <View
                  style={{
                    width: 20,
                    height: 20,
                    backgroundColor: COLOR.WHITE,
                    borderRadius: 10,
                    justifyContent: 'center',
                    alignSelf: 'center',
                  }}>
                  <Image
                    source={require('../../styles/assets/checked.png')}
                    style={{width: 10, height: 10, alignSelf: 'center'}}
                    resizeMode={'contain'}
                  />
                </View>
                <Text
                  style={{
                    fontSize: 12,
                    color: COLOR.WHITE,
                    alignSelf: 'center',
                    marginTop: 3,
                  }}>
                  Trip Summary
                </Text>
              </View>
              <View style={{width: 80}}>
                <View
                  style={{
                    width: 20,
                    height: 20,
                    borderWidth: this.state.screen > 1 ? 0 : 2,
                    backgroundColor: this.state.screen > 1 ? COLOR.WHITE : null,
                    borderColor: '#82BBC7',
                    borderRadius: 10,
                    justifyContent: 'center',
                    alignSelf: 'center',
                  }}>
                  {this.state.screen > 1 ? (
                    <Image
                      source={require('../../styles/assets/checked.png')}
                      style={{width: 10, height: 10, alignSelf: 'center'}}
                      resizeMode={'contain'}
                    />
                  ) : (
                    <Text
                      style={{
                        alignSelf: 'center',
                        fontSize: 12,
                        color: COLOR.WHITE,
                      }}>
                      2
                    </Text>
                  )}
                </View>
                <Text
                  style={{
                    fontSize: 12,
                    color: COLOR.WHITE,
                    alignSelf: 'center',
                    marginTop: 3,
                  }}>
                  Travellers
                </Text>
              </View>
              {this.props.singleTripOffer && this.props.singleTripOffer.offer_payment == 1 ? null : (
                <View style={{width: 80}}>
                  <View
                    style={{
                      width: 20,
                      height: 20,
                      borderWidth: this.state.screen == 3 ? 0 : 2,
                      backgroundColor:
                        this.state.screen == 3 ? COLOR.WHITE : null,
                      borderColor: '#82BBC7',
                      borderRadius: 10,
                      justifyContent: 'center',
                      alignSelf: 'center',
                    }}>
                    {this.state.screen == 3 ? (
                      <Image
                        source={require('../../styles/assets/checked.png')}
                        style={{width: 10, height: 10, alignSelf: 'center'}}
                        resizeMode={'contain'}
                      />
                    ) : (
                      <Text
                        style={{
                          alignSelf: 'center',
                          fontSize: 12,
                          color: COLOR.WHITE,
                        }}>
                        3
                      </Text>
                    )}
                  </View>
                  <Text
                    style={{
                      fontSize: 12,
                      color: COLOR.WHITE,
                      alignSelf: 'center',
                      marginTop: 3,
                    }}>
                    Payment
                  </Text>
                </View>
              )}
              <View
                style={{
                  position: 'absolute',
                  top: 10,
                  left: 50,
                  width: this.props.singleTripOffer && this.props.singleTripOffer.offer_payment == 1 ? 220 : 100,
                  height: 3,
                  backgroundColor:
                    this.state.screen > 1 ? COLOR.WHITE : '#82BBC7',
                }}
              />
              <View
                style={{
                  position: 'absolute',
                  top: 10,
                  right: 50,
                  width: this.props.singleTripOffer && this.props.singleTripOffer.offer_payment == 1 ? 0 : 100,
                  height: 3,
                  backgroundColor:
                    this.state.screen == 3 ? COLOR.WHITE : '#82BBC7',
                }}
              />
            </View>
            {this.state.screen == 1 && this.props.singleTripOffer && this.props.singleTripOffer ? (
              <TripDateView
                marginTop={1}
                departDate={this.props.singleTripOffer && this.props.singleTripOffer.trip_date_type.flexibility.depart}
                returnDate={this.props.singleTripOffer && this.props.singleTripOffer.trip_date_type.flexibility.return}
              />
            ) : null}
            <TouchableOpacity
              onPress={() => {
                if (this.state.screen == 3) {
                  this.setState({screen: 2});
                } else if (this.state.screen == 2) {
                  this.setState({screen: 1});
                } else {
                  this.props.navigation.goBack();
                }
              }}
              style={styles.backButton}>
              <Image style={styles.backImage} source={IMAGES.back_arrow} />
            </TouchableOpacity>
          </View>

          {this.props.singleTripOffer && this.props.singleTripOffer ? this.getScreenFromState(this.state.screen) : null}

          {/* {this.state.screen == 1 && this.state.offer ? (
            <View style={[styles.container, {paddingTop: 15}]}>
              <FlatList
                data={this.state.travellingModes}
                renderItem={({item}) => (
                  <View style={{marginHorizontal: 15, marginVertical: 7.5}}>
                    <PaymentServiceView item={item} />
                  </View>
                )}
                keyExtractor={(val, index) => index}
              />
              <TouchableOpacity
                onPress={() => this.changeScreen(2)}
                style={styles.continueButton}>
                <Text style={[Styles.button_font, {color: COLOR.WHITE}]}>
                {this.state.offerPayment == 1 ? 'Next' : 'Continue'}
                </Text>
              </TouchableOpacity>
            </View>
          ) : this.state.screen == 2 ? (
            <PaymentTravellersView
              travellers={this.state.offer && this.state.offer.travellers}
              changeScreen={this.changeScreen}
              offerPayment={this.state.offerPayment}
            />
          ) : this.state.screen == 3 ? (
            <PaymentSecondView
              navigation={this.props.navigation}
              isPackage={false}
              offer={this.state.offer}
              activeOffer={
                this.state.offer && this.state.offer.active_offers[0]
              }
              packageId={this.state.offer && this.state.offer.id}
            />
          ) : null} */}
        {/* </KeyboardAwareScrollView> */}
      </View>

      // </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // flexGrow: 1,
    backgroundColor: COLOR.BACKGROUND,
  },
  backButton: {
    alignSelf: 'center',
    height: 15,
    width: 15,
    top: Platform.OS === 'ios' ? 50 : 30,
    left: 20,
    position: 'absolute',
  },
  backImage: {
    tintColor: COLOR.WHITE,
    alignSelf: 'center',
    height: 15,
    width: 15,
  },
  statusBar: {
    height: hp(20),
    backgroundColor: COLOR.GREEN,
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
    paddingTop: Platform.OS === 'ios' ? 40 : 20,
  },
  title: {
    textAlign: 'center',
    color: COLOR.WHITE,
    marginVertical: 0,
    fontSize: FONTS.EXTRA_LARGE,
    fontFamily: FONTS.FAMILY_BOLD,
  },
  statusView: {
    width: 320,
    flexDirection: 'row',
    alignSelf: 'center',
    justifyContent: 'space-between',
    marginVertical: 10,
  },
  continueButton: {
    alignItems: 'center',
    justifyContent: 'center',
    height: 50,
    borderRadius: 10,
    width: widthPercentageToDP(90),
    marginTop: 10,
    marginBottom: 20,
    backgroundColor: COLOR.GREEN,
    alignSelf: 'center',
  },
});

const mapStateToProps = (state, ownProps) => {
  return {
    singleTripOffer: state.requestReducer.singleTripOffer,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getSingleTripOffer: (val) => {
      dispatch(RequestAction.getSingleTripOffer(val));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(PaymentSecond);

// import React from 'react';
// import {
//   StyleSheet,
//   View,
//   Text,
//   Image,
//   TouchableOpacity,
// } from 'react-native';
// import COLOR from '../../styles/Color';
// import FONTS from '../../styles/Fonts';
// import Styles from '../../styles/Styles';
// import {
//   heightPercentageToDP as hp,
//   widthPercentageToDP,
// } from 'react-native-responsive-screen';
// import IMAGES from '../../styles/Images';
// import {Platform} from 'react-native';
// import PaymentSummary from './PaymentSummary';
// import PaymentTravellersView from './PaymentTravellersView';
// import PaymentSecondView from './PaymentSecondView';
// import {connect} from 'react-redux';
// import RequestAction from '../../../redux/action/RequestAction';
// import TripDateView from '../../commonView/TripDateView';
// import PackagePaymentSummary from '../package/PackagePaymentSummary';

// class PaymentSecond extends React.Component {
//   constructor(props) {
//     super(props);
//     this.state = {
//       screen: 1,
//       offer: null,
//       offerPayment:0
//     };

//     this.changeScreen = this.changeScreen.bind(this);
//     this.getScreenFromState = this.getScreenFromState.bind(this);
//     this.getTitleFromState = this.getTitleFromState.bind(this);
//     this.props.getSingleTripOffer(this.props.route.params.id);
//   }

//   componentDidUpdate(prevProps) {
//     if (this.props.singleTripOffer != prevProps.singleTripOffer && this.props.singleTripOffer) {
//       this.setState({
//         offer: this.props.singleTripOffer,
//         offerPayment: this.props.singleTripOffer.offer_payment,
//       },()=>{
//       });
//     }
//   }

//   changeScreen = (screen) => {
//     this.setState({
//       screen: screen,
//     });
//   };

//   getScreenFromState(state) {
//     switch (state) {
//       case 1:
//         return (
//           <PackagePaymentSummary
//             travellingModes={
//               this.state.offer.active_offer_travelling_modes
//             }
//             changeScreen={this.changeScreen}
//             offerPayment={this.state.offerPayment}
//           />
//         );
//       case 2:
//         return (
//           <PaymentTravellersView
//             travellers={this.state.offer.travellers}
//             changeScreen={this.changeScreen}
//             offerPayment={this.state.offerPayment}
//           />
//         );
//       case 3:
//         return (
//           <PaymentSecondView
//             isPackage={false}
//             id={this.props.route.params.id}
//             navigation={this.props.navigation}
//             offer={this.state.offer}
//             activeOffer={this.state.offer.active_offers[0]}
//             packageId={this.state.offer.id}
//           />
//         );
//     }
//   }
//   getTitleFromState(state) {
//     switch (state) {
//       case 1:
//         return 'Trip Summary';
//       case 2:
//         return 'Travellers';
//       case 3:
//         return 'Payment';
//     }
//   }

//   render() {
//     return (
//       <View style={Styles.container}>
//         <View style={styles.container}>
//           <View style={styles.statusBar}>
//             <Text style={styles.title}>
//               {this.getTitleFromState(this.state.screen)}
//             </Text>
//             <View style={styles.statusView}>
//               <View style={{width: 80}}>
//                 <View
//                   style={{
//                     width: 20,
//                     height: 20,
//                     backgroundColor: COLOR.WHITE,
//                     borderRadius: 10,
//                     justifyContent: 'center',
//                     alignSelf: 'center',
//                   }}>
//                   <Image
//                     source={require('../../styles/assets/checked.png')}
//                     style={{width: 10, height: 10, alignSelf: 'center'}}
//                     resizeMode={'contain'}
//                   />
//                 </View>
//                 <Text
//                   style={{
//                     fontSize: 12,
//                     color: COLOR.WHITE,
//                     alignSelf: 'center',
//                     marginTop: 3,
//                   }}>
//                   Trip Summary
//                 </Text>
//               </View>
//               <View style={{width: 80}}>
//                 <View
//                   style={{
//                     width: 20,
//                     height: 20,
//                     borderWidth: this.state.screen > 1 ? 0 : 2,
//                     backgroundColor: this.state.screen > 1 ? COLOR.WHITE : null,
//                     borderColor: '#82BBC7',
//                     borderRadius: 10,
//                     justifyContent: 'center',
//                     alignSelf: 'center',
//                   }}>
//                   {this.state.screen > 1 ? (
//                     <Image
//                       source={require('../../styles/assets/checked.png')}
//                       style={{width: 10, height: 10, alignSelf: 'center'}}
//                       resizeMode={'contain'}
//                     />
//                   ) : (
//                     <Text
//                       style={{
//                         alignSelf: 'center',
//                         fontSize: 12,
//                         color: COLOR.WHITE,
//                       }}>
//                       2
//                     </Text>
//                   )}
//                 </View>
//                 <Text
//                   style={{
//                     fontSize: 12,
//                     color: COLOR.WHITE,
//                     alignSelf: 'center',
//                     marginTop: 3,
//                   }}>
//                   Travellers
//                 </Text>
//               </View>
//               <View style={{width: 80}}>
//                 <View
//                   style={{
//                     width: 20,
//                     height: 20,
//                     borderWidth: this.state.screen == 3 ? 0 : 2,
//                     backgroundColor:
//                       this.state.screen == 3 ? COLOR.WHITE : null,
//                     borderColor: '#82BBC7',
//                     borderRadius: 10,
//                     justifyContent: 'center',
//                     alignSelf: 'center',
//                   }}>
//                   {this.state.screen == 3 ? (
//                     <Image
//                       source={require('../../styles/assets/checked.png')}
//                       style={{width: 10, height: 10, alignSelf: 'center'}}
//                       resizeMode={'contain'}
//                     />
//                   ) : (
//                     <Text
//                       style={{
//                         alignSelf: 'center',
//                         fontSize: 12,
//                         color: COLOR.WHITE,
//                       }}>
//                       3
//                     </Text>
//                   )}
//                 </View>
//                 <Text
//                   style={{
//                     fontSize: 12,
//                     color: COLOR.WHITE,
//                     alignSelf: 'center',
//                     marginTop: 3,
//                   }}>
//                   Payment
//                 </Text>
//               </View>
//               <View
//                 style={{
//                   position: 'absolute',
//                   top: 10,
//                   left: 50,
//                   width: 100,
//                   height: 3,
//                   backgroundColor:
//                     this.state.screen > 1 ? COLOR.WHITE : '#82BBC7',
//                 }}
//               />
//               <View
//                 style={{
//                   position: 'absolute',
//                   top: 10,
//                   right: 50,
//                   width: 100,
//                   height: 3,
//                   backgroundColor:
//                     this.state.screen == 3 ? COLOR.WHITE : '#82BBC7',
//                 }}
//               />
//             </View>
//             {this.state.screen == 1 && this.state.offer ? (
//               <TripDateView
//                 marginTop={1}
//                 departDate={this.state.offer.start_date}
//                 returnDate={this.state.offer.end_date}
//               />
//             ) : null}
//             <TouchableOpacity
//               onPress={() => this.props.navigation.goBack()}
//               style={styles.backButton}>
//               <Image style={styles.backImage} source={IMAGES.back_arrow} />
//             </TouchableOpacity>
//           </View>

//           {this.state.offer ? this.getScreenFromState(this.state.screen) : null}
//         </View>
//       </View>
//     );
//   }
// }

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     flexGrow: 1,
//     backgroundColor: COLOR.BACKGROUND,
//   },
//   backButton: {
//     alignSelf: 'center',
//     height: 15,
//     width: 15,
//     top: Platform.OS === 'ios' ? 50 : 30,
//     left: 20,
//     position: 'absolute',
//   },
//   backImage: {
//     tintColor: COLOR.WHITE,
//     alignSelf: 'center',
//     height: 15,
//     width: 15,
//   },
//   statusBar: {
//     height: hp(20),
//     backgroundColor: COLOR.GREEN,
//     borderBottomLeftRadius: 10,
//     borderBottomRightRadius: 10,
//     paddingTop: Platform.OS === 'ios' ? 40 : 20,
//   },
//   title: {
//     textAlign: 'center',
//     color: COLOR.WHITE,
//     marginVertical: 0,
//     fontSize: FONTS.EXTRA_LARGE,
//     fontFamily: FONTS.FAMILY_BOLD,
//   },
//   statusView: {
//     width: 320,
//     flexDirection: 'row',
//     alignSelf: 'center',
//     justifyContent: 'space-between',
//     marginVertical: 10,
//   },
// });

// const mapStateToProps = (state, ownProps) => {
//   return {
//     singleTripOffer: state.requestReducer.singleTripOffer,
//   };
// };

// const mapDispatchToProps = (dispatch) => {
//   return {
//     getSingleTripOffer: (val) => {
//       dispatch(RequestAction.getSingleTripOffer(val));
//     },
//   };
// };

// export default connect(mapStateToProps, mapDispatchToProps)(PaymentSecond);