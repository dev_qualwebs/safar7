import React from 'react';
import {Image, View, Text, StyleSheet, FlatList} from 'react-native';
import {TouchableOpacity} from 'react-native';
import {
  heightPercentageToDP,
  heightPercentageToDP as hp,
  widthPercentageToDP,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import COLOR from '../../styles/Color';
import IMAGES from '../../styles/Images';
import Styles from '../../styles/Styles';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import FONTS from '../../styles/Fonts';
import {ScrollView, TextInput} from 'react-native-gesture-handler';
import {connect} from 'react-redux';
import PaymentAction from '../../../redux/action/PaymentAction';
import Toast from 'react-native-simple-toast';
import API from '../../../api/Api';
import TripAction from '../../../redux/action/TripAction';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview';
import ProfileAction from '../../../redux/action/ProfileAction';
import PackageAction from '../../../redux/action/PackageAction';
import ActionButton from 'react-native-action-button';
import SimpleToast from 'react-native-simple-toast';
import ActivityIndicator from '../../commonView/ActivityIndicator';
import CardBottomSheet from './CardBottomSheet';
import TripSummary from './TripSummary';
import TripDateView from '../../commonView/TripDateView';
import RequestAction from '../../../redux/action/RequestAction';
import {Alert} from 'react-native';
import PaymentOneTravellerView from '../paymentOne/PaymentOneTravellerView';
import TravellerNumberModal from '../message/travellerNumberModal';
import {DoneButtonKeyboard} from '../../commonView/CommonView';

let api = new API();

class PaymentScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      tripDetail: null,
      tdChecked: false,
      detailChecked: false,
      cardData: [],
      cvvNumber: '',
      selectedAddons: [],
      travellersData: [],
      selectedTravellers: [],
      screen: 1,
      packageSummary: [],
      packageAddons: [],
      administrativeFees: null,
      currency: '',
      isLoading: false,
      depart: null,
      return: null,
      changeCardModel: false,
      departFlexibility: 0,
      returnFlexibility: 0,
      createPackageId: null,
      packageRequestSummary: null,
      personCount: null,
      isFlightAdded: false,
      isCruiseAdded: false,
      userDetail: this.props.userDetail ? this.props.userDetail : null,
      showTravellerModal: false,
      summary: null,
      showTravellerModal: false,
    };
    this.package = this.props.route.params.is_package;
    this.id = this.package ? null : this.props.route.params.data.id;
    this.updateTextfield = this.updateTextfield.bind(this);
    this.handleBooking = this.handleBooking.bind(this);
    this.props.getTravellers();
    this.handleTravelerSelection = this.handleTravelerSelection.bind(this);
    this.getTripAddons = this.getTripAddons.bind(this);
  }

  componentDidMount() {
    let data = this.props.route.params.data;
    let params = this.props.route.params;
    if (this.package == 1) {
      this.setState({
        tripDetail: data,
        packageAddons: data.package_exclude_include.package_addons,
        depart: params.start_date,
        return: params.end_date,
        isLoading: true,
      });
      if (params.requestId) {
        this.getSinglePackageRequest(params.requestId);
        this.props.getAdministrativeFees(1);
        this.setState({
          createPackageId: params.requestId,
        });
      } else {
        let createData = JSON.stringify({
          package_id: data.id,
          is_date_change: params.is_date_change,
          start_date: params.start_date,
          end_date: params.end_date,
          number_of_persons: data.personCount,
        });

        this.createPackageRequest(createData);
        this.props.getAdministrativeFees(1);
      }
    } else {
      this.setState({
        tripDetail: data,
        packageAddons: null,
        depart: data.trip_date_type.flexibility.depart,
        return: data.trip_date_type.flexibility.return,
        departFlexibility:
          data.trip_date_type.flexibility.depart_flexibility_id,
        returnFlexibility:
          data.trip_date_type.flexibility.return_flexibility_id,
      });
      this.props.getAdministrativeFees(2);
      this.props.getRequestedSummary(data.id);
    }

    if (this.props.cardData) {
      this.setState({
        cardData: this.props.cardData,
      });
    }

    this.props.getUserDetail();
    this.props.getAddedCards();
    this.getTripAddons();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.requestPackage != this.props.requestPackage) {
      this.props.navigation.navigate(3);
      SimpleToast.show('Package request sent successfully');
    }

    if (prevProps.userDetail != this.props.userDetail) {
      this.setState({userDetail: this.props.userDetail});
    }

    if (prevProps.cardData != this.props.cardData) {
      this.setState({
        cardData: this.props.cardData,
      });
    }

    if (prevProps.propsAddons != this.props.propsAddons) {
      this.setState({
        selectedAddons: this.props.propsAddons,
      });
    }

    if (prevProps.travellersData !== this.props.travellersData) {
      this.setState(
        {
          travellersData: [...this.props.travellersData],
        },
        () => {
          let index = this.state.travellersData.findIndex(
            (value) => value.is_default_user == 1,
          );

          if (index != -1 && this.state.selectedTravellers.length == 0) {
            if (this.state.travellersData[index].traveller_passport != null) {
              let array = [...this.state.selectedTravellers];
              array.push(this.state.travellersData[index]);
              this.setState({
                selectedTravellers: array,
              });
            }
          }
        },
      );
    }
    if (this.props.packageSummary != prevProps.packageSummary) {
      if (this.props.packageSummary.length > 0) {
        let isFlightAdded = this.props.packageSummary[0].travelling_modes.some(
          (value) => value.mode.id == 1,
        );
        let isCruiseAdded = this.props.packageSummary[0].travelling_modes.some(
          (value) => value.mode.id == 5,
        );

        this.setState({
          packageSummary: this.props.packageSummary[0].travelling_modes,
          isFlightAdded: isFlightAdded,
          isCruiseAdded: isCruiseAdded,
        });
      }
    }

    if (prevProps.administrativeFees != this.props.administrativeFees) {
      let fee = this.props.administrativeFees;
      this.setState({
        administrativeFees:
          this.package == 1 ? fee.package_request_fee : fee.trip_request_fee,
        currency:
          this.package == 1
            ? fee.pack_currency.iso4217_code
            : fee.trip_currency.iso4217_code,
      });
    }
  }

  setModal() {
    this.setState({modalVisible: true});
  }

  getTripAddons = async () => {
    let that = this;
    api
      .getTripAddons(this.package == 1 ? 1 : 0, this.id)
      .then((json) => {
        that.setState({
          selectedAddons: json.data.response,
        });
      })
      .catch((error) => {
        console.log(error.response.data);
      });
  };

  createPackageRequest = (data) => {
    let that = this;

    api
      .createPackageRequest(data)
      .then((json) => {
        that.setState({
          createPackageId: json.data.response.id,
        });
        that.getSinglePackageRequest(json.data.response.id);
      })
      .catch(function (error) {
        console.log(error.response.data.message);
      });
  };

  updateMasterState = (attrName, value) => {
    this.setState({[attrName]: value});
  };

  updateTextfield = (val) => {
    if (val && val.length < 4) {
      this.setState({
        cvvNumber: val,
      });
    }
  };

  addTraveller = (val) => {
    let data = this.state.selectedTravellers;
    if (val.traveller_passport == null) {
      Alert.alert(
        'Add Passport',
        'Please Add User Passport to proceed futher',
        [
          {
            text: 'Cancel',
            onPress: () => {},
            style: 'cancel',
          },
          {
            text: 'OK',
            onPress: () => {
              if (val.is_default_user == 1) {
                this.props.navigation.navigate('ProfileDetail');
              } else {
                this.props.navigation.navigate('TravellersDetail', {data: val});
              }
            },
          },
        ],
      );
    } else {
      if (this.package && this.state.personCount) {
        if (data.length < this.state.personCount) {
          data.push(val);
          this.setState({
            selectedTravellers: data,
          });
        } else {
          this.setState({showTravellerModal: true});
        }
      } else {
        data.push(val);
        this.setState({
          selectedTravellers: data,
        });
      }
    }
  };

  removeTraveller = (val) => {
    console.log('Remove', this.state.selectedTravellers);
    let data = [...this.state.selectedTravellers];
    let index = data
      .map(function (val) {
        return val.id;
      })
      .indexOf(val.id);
    data.splice(index, 1);
    this.setState({
      selectedTravellers: data,
    });
  };

  getTravellersId = () => {
    if (this.state.selectedTravellers) {
      let ids = this.state.selectedTravellers.map(function (value) {
        return value.id;
      });
      return ids;
    } else {
      return null;
    }
  };

  handleBooking = () => {
    let that = this;
    if (this.state.cardData.length == 0) {
      Toast.show('Please add card to Book Trip');
    } else if (this.state.tdChecked == false) {
      Toast.show('Please Agree T&C');
    } else if (this.state.detailChecked == false) {
      Toast.show(
        'Please confirm and agree to, that all travellers details are correct.',
      );
    } else if (this.state.cvvNumber == '') {
      Toast.show('Enter CVV');
    } else {
      let cardLength = this.state.cardData.length;
      var data;

      if (this.package == 1) {
        data = {
          package_request_id: this.state.createPackageId,
          traveller_id: this.getTravellersId(),
          payment_method_id: 1,
          payment_card_id: this.state.cardData[cardLength - 1].id,
          cvv: this.state.cvvNumber,
        };
      } else {
        data = {
          trip_id: this.state.tripDetail.id,
          service_amount: this.state.administrativeFees,
          payment_method_id: 1,
          travellers: this.getTravellersId(),
          card_id: this.state.cardData[cardLength - 1].id,
          cvv: this.state.cvvNumber,
        };
      }

      let request =
        this.package == 1 ? api.requestPackage(data) : api.servicePayment(data);
      request
        .then((json) => {
          if (json.status == 200) {
            this.props.navigation.reset({
              index: 0,
              routes: [{name: 'HomeStack', params: {screen: 'Message'}}],
            });
            this.props.getBookedPackages();
            SimpleToast.show('Successfully booked Trip');
            this.setState({modalVisible: false});
            this.props.getUpcomingTrips();
          }
        })
        .catch((error) => {
          Toast.show(`${error.response.data.message}`);
        });
    }
  };

  handleTravelerSelection = () => {
    if (this.state.tripDetail.personCount) {
      if (
        this.state.selectedTravellers.length !=
        this.state.tripDetail.personCount
      ) {
        this.setState({showTravellerModal: true});
      } else {
        this.props.navigation.navigate('PaymentAddon', {
          addons: this.state.packageAddons,
          val: this.id,
          type: this.package == 1 ? 1 : 0,
        });
        this.setState({
          screen: 3,
        });
      }
    } else if (this.state.selectedTravellers.length > 0) {
      if (this.state.packageAddons) {
        this.props.navigation.navigate('PaymentAddon', {
          addons: this.state.packageAddons,
          val: this.id,
          type: this.package == 1 ? 1 : 0,
        });
      } else {
        this.props.navigation.navigate('PaymentAddon', {
          addons: null,
          val: this.id,
          type: this.package == 1 ? 1 : 0,
        });
      }

      this.setState({
        screen: 3,
      });
    } else {
      SimpleToast.show('Select Atleast one traveller');
    }
  };

  onTripContinue = () => {
    this.setState({
      screen: 2,
    });
  };

  getSinglePackageRequest = async (id) => {
    let that = this;

    api
      .getSinglePackageRequest(id)
      .then((json) => {
        console.log('single package request', json.data);
        that.id = json.data.package.trip.id;
        that.setState({
          packageRequestSummary:
            json.data.package.trip.user_package_travelling_modes,
          isLoading: false,
          personCount: json.data.number_of_persons,
          summary: json.data.package,
        });
      })
      .catch(function (error) {
        console.log(error.response.data.message);
      });
  };

  getSinglePackage = (id) => {
    let that = this;
    api
      .getSinglePackage(id)
      .then((json) => {
        if (json.status == 200) {
          console.log('single response:-', json);
          this.props.navigation.navigate('BookedPackage', {
            data: json.data.response,
          });
        } else if (json.status == 400) {
          setTimeout(() => {
            Toast.show(json.data.message);
          }, 0);
        } else {
          setTimeout(() => {
            Toast.show(json.data.message);
          }, 0);
        }
      })
      .catch((error) => {
        setTimeout(() => {
          console.log('json.data.message', error);
          //   Toast.show(String(error.response.data.message));
        }, 0);
        console.log('login error:-', error.response.data.message);
      });
  };

  render() {
    return (
      <View style={Styles.container}>
        <KeyboardAwareScrollView
          bounces={false}
          showsVerticalScrollIndicator={false}
          style={{flex: 1, backgroundColor: COLOR.BACKGROUND}}>
          <View
            style={{
              height: hp(20),
              backgroundColor: COLOR.GREEN,
              borderBottomLeftRadius: 10,
              borderBottomRightRadius: 10,
            }}>
            <Text
              style={[
                Styles.heading_label,
                {
                  textAlign: 'center',
                  color: COLOR.WHITE,
                  marginTop: getStatusBarHeight(true),
                  marginVertical: 0,
                  fontFamily: FONTS.FAMILY_BOLD,
                },
              ]}>
              {this.state.screen == 1
                ? 'Trip Summary'
                : this.state.screen == 2
                ? 'Travellers'
                : 'Payment'}
            </Text>
            <View style={style.statusView}>
              <View>
                <View
                  style={{
                    width: 20,
                    height: 20,
                    backgroundColor: COLOR.WHITE,
                    borderRadius: 10,
                    justifyContent: 'center',
                    alignSelf: 'center',
                  }}>
                  <Image
                    source={require('../../styles/assets/checked.png')}
                    style={{width: 10, height: 10, alignSelf: 'center'}}
                    resizeMode={'contain'}
                  />
                </View>
                <Text
                  style={{
                    fontSize: 12,
                    color: COLOR.WHITE,
                    alignSelf: 'center',
                    marginTop: 3,
                  }}>
                  Trip Summary
                </Text>
              </View>
              <View>
                <View
                  style={{
                    width: 20,
                    height: 20,
                    borderWidth: this.state.screen > 1 ? 0 : 2,
                    backgroundColor: this.state.screen > 1 ? COLOR.WHITE : null,
                    borderColor: '#82BBC7',
                    borderRadius: 10,
                    justifyContent: 'center',
                    alignSelf: 'center',
                  }}>
                  {this.state.screen > 1 ? (
                    <Image
                      source={require('../../styles/assets/checked.png')}
                      style={{width: 10, height: 10, alignSelf: 'center'}}
                      resizeMode={'contain'}
                    />
                  ) : (
                    <Text
                      style={{
                        alignSelf: 'center',
                        fontSize: 12,
                        color: COLOR.WHITE,
                      }}>
                      2
                    </Text>
                  )}
                </View>
                <Text
                  style={{
                    fontSize: 12,
                    color: COLOR.WHITE,
                    alignSelf: 'center',
                    marginTop: 3,
                  }}>
                  Travellers
                </Text>
              </View>
              <View>
                <View
                  style={{
                    width: 20,
                    height: 20,
                    borderWidth: this.state.screen == 3 ? 0 : 2,
                    backgroundColor:
                      this.state.screen == 3 ? COLOR.WHITE : null,
                    borderColor: '#82BBC7',
                    borderRadius: 10,
                    justifyContent: 'center',
                    alignSelf: 'center',
                  }}>
                  {this.state.screen == 3 ? (
                    <Image
                      source={require('../../styles/assets/checked.png')}
                      style={{width: 10, height: 10, alignSelf: 'center'}}
                      resizeMode={'contain'}
                    />
                  ) : (
                    <Text
                      style={{
                        alignSelf: 'center',
                        fontSize: 12,
                        color: COLOR.WHITE,
                      }}>
                      3
                    </Text>
                  )}
                </View>
                <Text
                  style={{
                    fontSize: 12,
                    color: COLOR.WHITE,
                    alignSelf: 'center',
                    marginTop: 3,
                  }}>
                  Payment
                </Text>
              </View>
              <View
                style={{
                  position: 'absolute',
                  top: 10,
                  left: 50,
                  width: '38%',
                  height: 3,
                  backgroundColor:
                    this.state.screen > 1 ? COLOR.WHITE : '#82BBC7',
                }}
              />
              <View
                style={{
                  position: 'absolute',
                  top: 10,
                  right: 32,
                  width: '34%',
                  height: 3,
                  backgroundColor:
                    this.state.screen == 3 ? COLOR.WHITE : '#82BBC7',
                }}
              />
            </View>
            {this.state.screen == 1 ? (
              <TripDateView
                marginTop={1}
                departDate={this.state.depart}
                returnDate={this.state.return}
                departFlexibility={this.state.departFlexibility}
                returnFlexibility={this.state.returnFlexibility}
              />
            ) : null}
            <TouchableOpacity
              onPress={() => {
                if (this.state.screen == 3) {
                  this.setState({screen: 2});
                } else if (this.state.screen == 2) {
                  this.setState({screen: 1});
                } else {
                  this.props.navigation.goBack();
                }
              }}
              style={{
                alignSelf: 'center',
                height: 15,
                width: 15,
                top: getStatusBarHeight(true) + 15,
                left: 20,
                position: 'absolute',
              }}>
              <Image
                style={{
                  tintColor: COLOR.WHITE,
                  alignSelf: 'center',
                  height: 15,
                  width: 15,
                }}
                source={IMAGES.back_arrow}
              />
            </TouchableOpacity>
          </View>

          {this.state.screen == 1 ? (
            <TripSummary
              package_request_id={this.state.createPackageId}
              id={this.id}
              departDate={this.state.depart}
              returnDate={this.state.return}
              departFlexibility={this.state.departFlexibility}
              returnFlexibility={this.state.returnFlexibility}
              summary={
                this.package == 1
                  ? this.state.packageRequestSummary
                  : this.state.packageSummary
              }
              isPackage={this.package}
              onContinue={this.onTripContinue.bind(this)}
            />
          ) : this.state.screen == 2 ? (
            <PaymentOneTravellerView
              userDetail={this.state.userDetail}
              travellersData={this.state.travellersData}
              removeTraveller={this.removeTraveller}
              addTraveller={this.addTraveller}
              packageSummary={this.state.summary}
              deleteTraveller={(id) => this.props.deleteTraveller(id)}
              selectedTravellers={this.state.selectedTravellers}
              handleTravelerSelection={this.handleTravelerSelection}
            />
          ) : (
            <this.PaymentView />
          )}
        </KeyboardAwareScrollView>
        {this.state.screen == 1 && this.package != 1 ? (
          <ActionButton
            bgColor="#00000080"
            position={'right'}
            offsetX={25}
            offsetY={25}
            spacing={10}
            size={44}
            buttonColor={COLOR.GREEN}
            onPress={() =>
              this.props.navigation.navigate('NewReservationPackage', {
                tripId: this.id,
                depart: this.state.depart,
                return: this.state.return,
                departFlexibility: this.state.departFlexibility,
                returnFlexibility: this.state.returnFlexibility,
                isPackage: this.package == 1 ? true : false,
                package_request_id: this.state.createPackageId,
                isFlightAdded: this.state.isFlightAdded,
                isCruiseAdded: this.state.isCruiseAdded,
              })
            }
          />
        ) : null}
        <ActivityIndicator loading={this.state.isLoading} />
        {this.state.showTravellerModal ? (
          <TravellerNumberModal
            handleVisible={this.state.showTravellerModal}
            hideModal={() => this.setState({showTravellerModal: false})}
            modal={true}
          />
        ) : null}
      </View>
    );
  }

  TravellerView = (props) => {
    return (
      <View style={{width: wp(90), alignSelf: 'center', flex: 1}}>
        <Text
          style={{
            fontSize: 18,
            marginVertical: 15,
          }}>
          Travellers
        </Text>
        <View style={{flexDirection: 'row', marginVertical: 10}}>
          <Image
            source={IMAGES.shield}
            style={{
              width: 25,
              height: 25,
              marginRight: 10,
            }}
            resizeMode={'contain'}
          />
          <Text style={{color: COLOR.LIGHT_GREEN}}>
            WE PROTECT YOUR PERSONAL INFORMATION
          </Text>
        </View>
        <FlatList
          bounces={false}
          numColumns={1}
          style={{height: heightPercentageToDP(45)}}
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}
          data={this.state.travellersData}
          renderItem={({item}) => {
            const selected = this.state.selectedTravellers.findIndex(
              (value) => value.id == item.id,
            );

            return (
              <View>
                <View
                  style={{
                    flexDirection: 'row',
                    alignSelf: 'center',
                    width: wp(90),
                    alignItems: 'center',
                    marginVertical: 10,
                  }}>
                  <TouchableOpacity
                    onPress={() =>
                      selected != -1
                        ? this.removeTraveller(item)
                        : this.addTraveller(item)
                    }
                    style={{
                      height: 30,
                      width: 30,
                      marginHorizontal: 5,
                      borderRadius: 5,
                      backgroundColor:
                        selected != -1 ? COLOR.GREEN : COLOR.WHITE,
                      borderColor: COLOR.GRAY,
                      justifyContent: 'center',
                      alignItems: 'center',
                      borderWidth: 1,
                    }}>
                    <Image
                      resizeMode={'contain'}
                      style={{height: 20, width: 20, alignSelf: 'center'}}
                      source={IMAGES.tick_image}
                    />
                  </TouchableOpacity>
                  <Text style={[Styles.body_label, {width: null, flex: 1}]}>
                    {item.prefix + ' ' + item.first_name}
                  </Text>
                  {item.is_default_user == 1 ? null : (
                    <>
                      <TouchableOpacity
                        onPress={() =>
                          this.props.navigation.navigate('TravellersDetail', {
                            data: item,
                          })
                        }
                        style={{
                          height: 40,
                          width: 40,
                          justifyContent: 'center',
                          alignItems: 'center',
                        }}>
                        <Image
                          source={IMAGES.edit}
                          style={{height: 20, width: 20}}
                          resizeMode={'contain'}
                        />
                      </TouchableOpacity>
                      {/* <TouchableOpacity
                        onPress={() => {
                          this.props.deleteTraveller(item.id);
                          this.removeTraveller(item);
                        }}
                        style={{
                          height: 40,
                          width: 40,
                          justifyContent: 'center',
                          alignItems: 'center',
                        }}>
                        <Image
                          source={IMAGES.cross_icon}
                          style={{height: 20, width: 20}}
                          resizeMode={'contain'}
                        />
                      </TouchableOpacity> */}
                    </>
                  )}
                </View>
                <View style={Styles.line_view} />
              </View>
            );
          }}
          keyExtractor={(item, index) => index.toString()}
        />
        <TouchableOpacity
          onPress={() => {
            this.props.navigation.navigate('TravellersDetail');
          }}
          style={{
            alignItems: 'center',
            justifyContent: 'center',
            height: 50,
            borderRadius: 10,
            borderColor: COLOR.GRAY,
            borderWidth: 1,
            width: wp(90),
            marginVertical: 10,
            backgroundColor: COLOR.WHITE,
            alignSelf: 'center',
          }}>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <View
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                width: 25,
                height: 25,
                borderRadius: 15,
                backgroundColor: COLOR.GREEN,
                marginRight: 10,
              }}>
              <Image
                style={{
                  width: 15,
                  height: 15,
                  borderRadius: 10,
                  tintColor: COLOR.WHITE,
                }}
                source={IMAGES.plus_image}
              />
            </View>

            <Text style={[Styles.button_font, {color: COLOR.BLACK}]}>
              Add New Traveller
            </Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={this.handleTravelerSelection}
          style={{
            alignItems: 'center',
            justifyContent: 'center',
            height: 50,
            borderRadius: 10,
            width: wp(90),
            marginVertical: 10,
            backgroundColor: COLOR.GREEN,
            alignSelf: 'center',
          }}>
          <Text style={[Styles.button_font, {color: COLOR.WHITE}]}>
            Continue
          </Text>
        </TouchableOpacity>
        {this.state.showTravellerModal ? (
          <TravellerNumberModal
            handleVisible={this.state.showTravellerModal}
            hideModal={() => this.setState({showTravellerModal: false})}
            modal={true}
          />
        ) : null}
      </View>
    );
  };

  PaymentView = (props) => {
    let {tripDetail, tdChecked, detailChecked} = this.state;
    return (
      <View>
        {this.state.selectedAddons.length > 0 ? (
          <View
            style={{
              borderRadius: 10,
              backgroundColor: COLOR.WHITE,
              borderWidth: 0,
              shadowColor: COLOR.VOILET,
              shadowOffset: {width: 0.2, height: 0.2},
              shadowOpacity: 0.2,
              width: wp(90),
              alignSelf: 'center',
              paddingHorizontal: 10,
              paddingVertical: 20,
              elevation: 3,
              marginTop: 20,
            }}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginBottom: 10,
              }}>
              <Text
                style={[
                  Styles.small_label,
                  {
                    width: null,
                    color: COLOR.BLACK,
                    fontFamily: FONTS.FAMILY_BOLD,
                  },
                ]}>
                ADDONS
              </Text>
              <TouchableOpacity
                onPress={() => {
                  this.props.navigation.navigate('PaymentAddon', {
                    addons: this.state.packageAddons,
                    val: this.id,
                    type: this.package == 1 ? 1 : 0,
                  });
                }}>
                <Image
                  style={{height: 25, width: 25}}
                  resizeMode={'contain'}
                  source={IMAGES.edit}
                />
              </TouchableOpacity>
            </View>
            {/* <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <View style={{flexDirection: 'row'}}>
              <Text
                style={[
                  Styles.small_label,
                  {
                    width: null,
                    alignSelf: 'flex-start',
                    marginBottom: 10,
                    fontSize: 14,
                    color: COLOR.BLACK,
                  },
                ]}>
                Data roaming simcard
              </Text>
            </View>
            <View style={{flexDirection: 'row'}}>
                <Text
                  style={[
                    Styles.small_label,
                    {
                      width: null,
                      alignSelf: 'flex-start',
                      marginBottom: 10,
                      fontSize: 14,
                      color: COLOR.BLACK,
                      marginRight: 10,
                    },
                  ]}>
                  450 SAR
                </Text>
                <Image
                  style={{height: 12, width: 12, marginTop: 2}}
                  resizeMode={'contain'}
                  source={IMAGES.chain}
                />
              </View> 
          </View> */}

            {this.state.selectedAddons &&
              this.state.selectedAddons.map((val) => {
                return (
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                    }}>
                    <View style={{flexDirection: 'row'}}>
                      <Text
                        style={[
                          Styles.small_label,
                          {
                            width: null,
                            alignSelf: 'flex-start',
                            marginBottom: 10,
                            fontSize: 14,
                            color: COLOR.BLACK,
                          },
                        ]}>
                        {val.name}
                      </Text>
                    </View>
                  </View>
                );
              })}

            <Text
              style={[
                Styles.small_label,
                {
                  marginTop: 20,
                  width: null,
                  alignSelf: 'flex-start',
                  color: COLOR.LIGHT_GREEN,
                },
              ]}>
              All documents will be deliver to your email after the payment
            </Text>
          </View>
        ) : null}
        <View
          style={{
            flexDirection: 'row',
            width: wp(90),
            alignSelf: 'center',
            marginTop: 15,
            justifyContent: 'space-between',
          }}>
          <Text
            style={[
              Styles.subheading_label,
              {
                width: null,
                fontFamily: FONTS.FAMILY_EXTRA_BOLD,
                fontSize: 22,
              },
            ]}>
            Payment
          </Text>
          {this.package == 1 ? (
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <TouchableOpacity
                onPress={() =>
                  // this.props.navigation.navigate('BookedPackage', {
                  //   data: this.state.tripDetail,
                  // })
                  this.getSinglePackage(this.state.tripDetail.id)
                }>
                <Text
                  style={[
                    Styles.small_label,
                    {width: null, color: COLOR.GREEN, fontSize: 14},
                  ]}>
                  View Trip Summary
                </Text>
              </TouchableOpacity>
              <Image
                style={{height: 10, width: 10, tintColor: COLOR.GREEN}}
                source={IMAGES.right_arrow}
              />
            </View>
          ) : null}
        </View>
        <View
          style={{
            flexDirection: 'row',
            width: wp(90),
            alignSelf: 'center',
            marginTop: 20,
            justifyContent: 'space-between',
          }}>
          <Text style={[Styles.body_label, {width: null, fontSize: 14}]}>
            Administratinve Fee
          </Text>
          <Text
            style={[
              Styles.body_label,
              {width: null, color: COLOR.BLACK, fontSize: 14},
            ]}>
            {this.state.administrativeFees ? this.state.administrativeFees : 0}{' '}
            {this.state.currency ? this.state.currency : 0}
          </Text>
        </View>

        {/* <View
          style={{
            flexDirection: 'row',
            width: wp(90),
            alignSelf: 'center',
            marginVertical: 15,
          }}>
          <Image
            style={{height: 30, width: 40, marginRight: 10}}
            source={IMAGES.radio_button_off}
            resizeMode={'contain'}
          />
          <Text style={[Styles.body_label, {width: null}]}>Use My Credits</Text>
          <Text
            style={[
              Styles.body_label,
              {
                width: null,
                flex: 1,
                textAlign: 'right',
                color: COLOR.LIGHT_GREEN,
              },
            ]}>
             {tripDetail ? tripDetail.price : 0}{' '}
            {tripDetail ? tripDetail.currency.iso4217_code : 0}
          </Text>
        </View> */}
        {/* (1500 Credits) -1500 SAR */}
        <View
          style={[
            Styles.line_view,
            {
              width: wp(90),
            },
          ]}
        />
        <View
          style={{
            flexDirection: 'row',
            width: wp(90),
            alignSelf: 'center',
            marginVertical: 15,
            justifyContent: 'space-between',
          }}>
          <View
            style={{
              flexDirection: 'row',
            }}>
            <Text
              style={[
                Styles.subheading_label,
                {width: null, fontFamily: FONTS.FAMILY_EXTRA_BOLD},
              ]}>
              Total
            </Text>
            <Text style={[Styles.small_label, {width: null}]}>
              {' '}
              (VATS included)
            </Text>
          </View>

          <Text
            style={[
              Styles.subheading_label,
              {
                width: null,
                color: COLOR.BLACK,
                fontFamily: FONTS.FAMILY_EXTRA_BOLD,
              },
            ]}>
            {this.state.administrativeFees ? this.state.administrativeFees : 0}{' '}
            {this.state.currency ? this.state.currency : 0}
          </Text>
        </View>
        <TouchableOpacity
          onPress={() => {
            this.props.navigation.navigate('PolicyPage', {
              screenTitle: 'Terms and Condition',
            });
          }}>
          <Text style={[Styles.extra_small_label]}>
            *Upon the
            <Text style={[Styles.extra_small_label, {color: COLOR.GREEN}]}>
              Terms of Conditions{' '}
            </Text>
            , the fee of the service will be conducted from the customer upon
            type and number of requests.
          </Text>
        </TouchableOpacity>

        <View
          style={{
            borderWidth: 1,
            borderColor: COLOR.GRAY,
            borderRadius: 10,
            height: 50,
            flexDirection: 'row',
            width: wp(90),
            alignSelf: 'center',
            paddingHorizontal: 20,
            justifyContent: 'space-between',
            alignItems: 'center',
            marginVertical: 30,
          }}>
          <Text
            style={[
              Styles.body_label,
              {width: null, color: COLOR.LIGHT_TEXT_COLOR},
            ]}>
            Add Coupon Code
          </Text>
          <Text
            style={[
              Styles.button_font,
              {
                width: null,
                color: COLOR.GREEN,
                fontFamily: FONTS.FAMILY_SEMIBOLD,
              },
            ]}>
            Apply
          </Text>
        </View>
        <View style={{width: wp(90), alignSelf: 'center'}}>
          <View
            style={{
              flexDirection: 'row',
              width: wp(80),
            }}>
            <TouchableOpacity
              onPress={() => {
                this.setState({tdChecked: !this.state.tdChecked});
              }}>
              <Image
                style={{
                  height: 20,
                  width: 20,
                  borderRadius: 5,
                  borderWidth: 1,
                  borderColor: COLOR.VOILET,
                  marginRight: 15,
                  backgroundColor: tdChecked ? COLOR.GREEN : COLOR.WHITE,
                }}
                source={tdChecked ? IMAGES.tick_image : null}
                resizeMode={'contain'}
              />
            </TouchableOpacity>
            <Text style={{color: COLOR.VOILET, fontSize: 12}}>
              I have read, understand and accept the{' '}
              <TouchableOpacity
                onPress={() => {
                  this.props.navigation.navigate('PolicyPage', {
                    screenTitle: 'Privacy Policy',
                  });
                }}>
                <Text
                  style={[
                    Styles.small_label,
                    {width: null, color: COLOR.GREEN, marginBottom: -5},
                  ]}>
                  Privacy Policy
                </Text>
              </TouchableOpacity>
              and{' '}
              <TouchableOpacity
                onPress={() => {
                  this.props.navigation.navigate('PolicyPage', {
                    screenTitle: 'Terms & Conditions',
                  });
                }}>
                <Text
                  style={[
                    Styles.small_label,
                    {width: null, color: COLOR.GREEN, marginBottom: -5},
                  ]}>
                  Terms of Conditions{' '}
                </Text>
              </TouchableOpacity>
              .
            </Text>
          </View>

          <View
            style={{
              flexDirection: 'row',
              width: wp(80),
              marginTop: 10,
            }}>
            <TouchableOpacity
              onPress={() => {
                this.setState({detailChecked: !detailChecked});
              }}>
              <Image
                style={{
                  height: 20,
                  width: 20,
                  borderRadius: 5,
                  borderWidth: 1,
                  borderColor: COLOR.VOILET,
                  marginRight: 15,
                  backgroundColor: detailChecked ? COLOR.GREEN : COLOR.WHITE,
                }}
                resizeMode={'contain'}
                source={detailChecked ? IMAGES.tick_image : null}
              />
            </TouchableOpacity>
            <Text style={{color: COLOR.VOILET, fontSize: 12, marginRight: 10}}>
              I Confirm that all travellers details are correct, match passport
              information and that at least one adult is 18 years old or above.
            </Text>
          </View>
        </View>
        {this.state.cardData.length > 0 && (
          <View
            style={{marginVertical: 20, width: wp(90), alignSelf: 'center'}}>
            <View
              style={{flexDirection: 'row', justifyContent: 'space-between'}}>
              <Text
                style={[
                  Styles.small_label,
                  {color: COLOR.VOILET, width: null},
                ]}>
                CARD NUMBER
              </Text>
              <View
                style={{
                  width: 100,
                  alignItems: 'center',
                  flexDirection: 'row',
                }}>
                <Text
                  style={[
                    Styles.small_label,
                    {
                      color: COLOR.VOILET,
                      textAlign: 'left',
                      width: null,
                      marginRight: 10,
                    },
                  ]}>
                  CVV
                </Text>
                {/* <Image
                    style={{height: 15, width: 15}}
                    source={IMAGES.red_alert}
                  /> */}
              </View>
            </View>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
                marginTop: 10,
              }}>
              <View
                style={{
                  alignItems: 'center',
                  flexDirection: 'row',
                }}>
                <Image
                  style={{height: 25, width: 45, marginRight: 10}}
                  source={IMAGES.visa}
                  resizeMode={'contain'}
                />
                <Text
                  style={[
                    Styles.body_label,
                    {
                      color: COLOR.BLACK,
                      textAlign: 'left',
                      width: null,
                      marginRight: 0,
                    },
                  ]}>
                  {
                    this.state.cardData[this.state.cardData.length - 1]
                      .card_number
                  }
                </Text>
              </View>
              <TextInput
                // inputAccessoryViewID={'uniqueID'}
                onChangeText={this.updateTextfield}
                defaultValue={this.state.cvvNumber}
                style={{
                  padding: 10,
                  width: 100,
                  borderWidth: 1,
                  borderColor: COLOR.VOILET,
                }}
                keyboardType={'number-pad'}
              />
              <DoneButtonKeyboard />
            </View>
            <TouchableOpacity
              onPress={() => this.setState({changeCardModel: true})}>
              <Text
                style={[Styles.small_label, {color: COLOR.BLUE, width: null}]}>
                CHANGE CARD
              </Text>
            </TouchableOpacity>
          </View>
        )}
        <View
          style={{
            flexDirection: 'row',
            width: wp(90),
            alignSelf: 'center',
            marginVertical: 15,
          }}>
          <Text
            style={[
              Styles.small_label,
              {color: COLOR.RED, textAlign: 'center', alignSelf: 'center'},
            ]}>
            Your Card Information will not be saved.
          </Text>
        </View>

        <TouchableOpacity
          onPress={() => this.handleBooking()}
          style={[
            style.button,
            {
              marginTop: 20,
              marginBottom: 10,
              height: 50,
              backgroundColor: COLOR.GREEN,
              width: wp(90),
              alignSelf: 'center',
            },
          ]}>
          <Text style={[Styles.button_font, {color: COLOR.WHITE}]}>
            Pay by Credit Card
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            this.setState({modalVisible: false}, (val) => {
              this.props.navigation.navigate('ChoosePaymentMethod', {
                cards: this.state.cardData,
              });
            });
          }}
          style={[
            style.button,
            {
              marginTop: 0,
              marginBottom: 20,
              height: 50,
              width: wp(90),
              alignSelf: 'center',
              borderColor: 'transparent',
            },
          ]}>
          <Text style={[Styles.body_label, {color: COLOR.BLACK, width: null}]}>
            Change Payment Method
          </Text>
        </TouchableOpacity>
        <CardBottomSheet
          updateMasterState={this.updateMasterState}
          modelVisibility={this.state.changeCardModel}
          cards={this.state.cardData}
        />
      </View>
    );
  };
}

const style = StyleSheet.create({
  main_view: {width: wp(90), alignSelf: 'center', marginVertical: 20},
  back_view: {
    backgroundColor: COLOR.WHITE,
    borderRadius: 10,
    shadowColor: COLOR.WHITE,
    shadowOffset: {width: 0.2, height: 0.2},
    shadowOpacity: 0.2,
    paddingHorizontal: 10,
    paddingVertical: 20,
    borderWidth: 1,
    borderColor: COLOR.GRAY,
    elevation: 3,
  },
  button: {
    height: 40,
    justifyContent: 'center',
    marginTop: 20,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: COLOR.GRAY,
    backgroundColor: COLOR.WHITE,
  },
  statusView: {
    width: widthPercentageToDP(90),
    flexDirection: 'row',
    alignSelf: 'center',
    justifyContent: 'space-between',
    marginVertical: 10,
  },
});

const mapStateToProps = (state, ownProps) => {
  return {
    cardData: state.paymentReducer.paymentCards,
    travellersData: state.profileReducer.travellersData,
    packageSummary: state.pacakgeReducer.packageSummary,
    administrativeFees: state.pacakgeReducer.administrativeFees,
    userDetail: state.profileReducer.userDetail,
    bookedPackages: state.pacakgeReducer.bookedPackages,
    propsAddons: state.requestReducer.tripAddons,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getAddedCards: (val) => {
      dispatch(PaymentAction.getAddedCards(val));
    },

    getUpcomingTrips: (val) => {
      dispatch(RequestAction.getUpcomingTrips());
    },
    getTravellers: () => {
      dispatch(ProfileAction.getTravellers());
    },
    deleteTraveller: (val) => {
      dispatch(ProfileAction.deleteTraveller(val));
    },
    getAdministrativeFees: (val) => {
      dispatch(PackageAction.getAdministrativeFees(val));
    },
    getBookedPackages: (val) => {
      dispatch(PackageAction.getBookedPackage(val));
    },
    getRequestedSummary: (val) => {
      dispatch(PackageAction.getRequestedSummary(val));
    },
    getUserDetail: (val) => {
      dispatch(ProfileAction.getUserInfo(val));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(PaymentScreen);
