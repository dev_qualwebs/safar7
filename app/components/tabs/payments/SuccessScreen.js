import {useNavigation} from '@react-navigation/native';
import React from 'react';
import {StyleSheet, View, Image, Text, TouchableOpacity} from 'react-native';
import COLOR from '../../styles/Color';
import FONTS from '../../styles/Fonts';

export default function SuccessScreen(props) {
  const navigation = useNavigation();
  return (
    <View style={styles.container}>
      <View style={styles.opacityCircle}>
        <View style={styles.circularBackground}>
          <Image
            source={require('../../styles/assets/right_arrow.png')}
            style={{
              width: 40,
              height: 40,
              alignSelf: 'center',
            }}
            resizeMode={'contain'}
          />
        </View>
      </View>
      <Text style={styles.successText}>Success</Text>
      <Text style={styles.message}>
        Your payment will be processed. Verification of your trip will confirmed
        after the payment within 24 hours
      </Text>
      <TouchableOpacity
        onPress={() => navigation.navigate('PaymentCompleteScreen')}
        style={styles.closeButton}>
        <Image
          source={require('../../styles/assets/close.png')}
          style={{
            width: 25,
            height: 25,
            tintColor: COLOR.WHITE,
          }}
        />
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#61DC81',
    alignItems: 'center',
    justifyContent: 'center',
  },
  opacityCircle: {
    width: 130,
    height: 130,
    alignSelf: 'center',
    borderRadius: 65,
    backgroundColor: 'rgba(255, 255, 255, 0.4)',
    justifyContent: 'center',
  },
  circularBackground: {
    width: 100,
    height: 100,
    backgroundColor: COLOR.WHITE,
    borderRadius: 50,
    alignSelf: 'center',
    justifyContent: 'center',
  },
  successText: {
    marginTop: 25,
    fontFamily: FONTS.FAMILY_BOLD,
    fontSize: 28,
    color: COLOR.WHITE,
    alignSelf: 'center',
  },
  message: {
    fontFamily: FONTS.FAMILY_SEMIBOLD,
    color: COLOR.WHITE,
    marginHorizontal: 40,
    fontSize: 14,
    marginTop: 10,
    textAlign: 'center',
    alignSelf: 'center',
  },
  closeButton: {
    position: 'absolute',
    top: 50,
    left: 20,
    alignItems: 'center',
    justifyContent: 'center',
    width: 30,
    height: 30,
  },
});
