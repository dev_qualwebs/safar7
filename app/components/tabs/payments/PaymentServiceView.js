import {useNavigation} from '@react-navigation/core';
import React, {useEffect, useState} from 'react';
import {
  View,
  Image,
  Text,
  StyleSheet,
  Modal,
  SafeAreaView,
  Platform,
} from 'react-native';
import CameraRoll from '@react-native-community/cameraroll';
import {FlatList, TouchableOpacity} from 'react-native';
import {TouchableWithoutFeedback} from 'react-native-gesture-handler';
import {widthPercentageToDP} from 'react-native-responsive-screen';
import COLOR from '../../styles/Color';
import FONTS from '../../styles/Fonts';
import IMAGES from '../../styles/Images';
import Styles from '../../styles/Styles';
import RNFetchBlob from 'rn-fetch-blob';
import SimpleToast from 'react-native-simple-toast';
import Share from 'react-native-share';
import TicketPdfView from '../home/trip/TicketPdfView';

const android = RNFetchBlob.android;

const resources = {
  file:
    Platform.OS === 'ios'
      ? 'downloadedDocument.pdf'
      : '/sdcard/Download/downloadedDocument.pdf',
  url: 'https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf',
  base64: 'JVBERi0xLjMKJcfs...',
};

const resourceType = 'url';

const pdfBase64 =
  'data:application/pdf;base64,JVBERi0xLjMNCiXi48/TDQoNCjEgMCBvYmoNCjw8DQovVHlwZSAvQ2F0YWxvZw0KL091dGxpbmVzIDIgMCBSDQovUGFnZXMgMyAwIFINCj4+DQplbmRvYmoNCg0KMiAwIG9iag0KPDwNCi9UeXBlIC9PdXRsaW5lcw0KL0NvdW50IDANCj4+DQplbmRvYmoNCg0KMyAwIG9iag0KPDwNCi9UeXBlIC9QYWdlcw0KL0NvdW50IDINCi9LaWRzIFsgNCAwIFIgNiAwIFIgXSANCj4+DQplbmRvYmoNCg0KNCAwIG9iag0KPDwNCi9UeXBlIC9QYWdlDQovUGFyZW50IDMgMCBSDQovUmVzb3VyY2VzIDw8DQovRm9udCA8PA0KL0YxIDkgMCBSIA0KPj4NCi9Qcm9jU2V0IDggMCBSDQo+Pg0KL01lZGlhQm94IFswIDAgNjEyLjAwMDAgNzkyLjAwMDBdDQovQ29udGVudHMgNSAwIFINCj4+DQplbmRvYmoNCg0KNSAwIG9iag0KPDwgL0xlbmd0aCAxMDc0ID4+DQpzdHJlYW0NCjIgSg0KQlQNCjAgMCAwIHJnDQovRjEgMDAyNyBUZg0KNTcuMzc1MCA3MjIuMjgwMCBUZA0KKCBBIFNpbXBsZSBQREYgRmlsZSApIFRqDQpFVA0KQlQNCi9GMSAwMDEwIFRmDQo2OS4yNTAwIDY4OC42MDgwIFRkDQooIFRoaXMgaXMgYSBzbWFsbCBkZW1vbnN0cmF0aW9uIC5wZGYgZmlsZSAtICkgVGoNCkVUDQpCVA0KL0YxIDAwMTAgVGYNCjY5LjI1MDAgNjY0LjcwNDAgVGQNCigganVzdCBmb3IgdXNlIGluIHRoZSBWaXJ0dWFsIE1lY2hhbmljcyB0dXRvcmlhbHMuIE1vcmUgdGV4dC4gQW5kIG1vcmUgKSBUag0KRVQNCkJUDQovRjEgMDAxMCBUZg0KNjkuMjUwMCA2NTIuNzUyMCBUZA0KKCB0ZXh0LiBBbmQgbW9yZSB0ZXh0LiBBbmQgbW9yZSB0ZXh0LiBBbmQgbW9yZSB0ZXh0LiApIFRqDQpFVA0KQlQNCi9GMSAwMDEwIFRmDQo2OS4yNTAwIDYyOC44NDgwIFRkDQooIEFuZCBtb3JlIHRleHQuIEFuZCBtb3JlIHRleHQuIEFuZCBtb3JlIHRleHQuIEFuZCBtb3JlIHRleHQuIEFuZCBtb3JlICkgVGoNCkVUDQpCVA0KL0YxIDAwMTAgVGYNCjY5LjI1MDAgNjE2Ljg5NjAgVGQNCiggdGV4dC4gQW5kIG1vcmUgdGV4dC4gQm9yaW5nLCB6enp6ei4gQW5kIG1vcmUgdGV4dC4gQW5kIG1vcmUgdGV4dC4gQW5kICkgVGoNCkVUDQpCVA0KL0YxIDAwMTAgVGYNCjY5LjI1MDAgNjA0Ljk0NDAgVGQNCiggbW9yZSB0ZXh0LiBBbmQgbW9yZSB0ZXh0LiBBbmQgbW9yZSB0ZXh0LiBBbmQgbW9yZSB0ZXh0LiBBbmQgbW9yZSB0ZXh0LiApIFRqDQpFVA0KQlQNCi9GMSAwMDEwIFRmDQo2OS4yNTAwIDU5Mi45OTIwIFRkDQooIEFuZCBtb3JlIHRleHQuIEFuZCBtb3JlIHRleHQuICkgVGoNCkVUDQpCVA0KL0YxIDAwMTAgVGYNCjY5LjI1MDAgNTY5LjA4ODAgVGQNCiggQW5kIG1vcmUgdGV4dC4gQW5kIG1vcmUgdGV4dC4gQW5kIG1vcmUgdGV4dC4gQW5kIG1vcmUgdGV4dC4gQW5kIG1vcmUgKSBUag0KRVQNCkJUDQovRjEgMDAxMCBUZg0KNjkuMjUwMCA1NTcuMTM2MCBUZA0KKCB0ZXh0LiBBbmQgbW9yZSB0ZXh0LiBBbmQgbW9yZSB0ZXh0LiBFdmVuIG1vcmUuIENvbnRpbnVlZCBvbiBwYWdlIDIgLi4uKSBUag0KRVQNCmVuZHN0cmVhbQ0KZW5kb2JqDQoNCjYgMCBvYmoNCjw8DQovVHlwZSAvUGFnZQ0KL1BhcmVudCAzIDAgUg0KL1Jlc291cmNlcyA8PA0KL0ZvbnQgPDwNCi9GMSA5IDAgUiANCj4+DQovUHJvY1NldCA4IDAgUg0KPj4NCi9NZWRpYUJveCBbMCAwIDYxMi4wMDAwIDc5Mi4wMDAwXQ0KL0NvbnRlbnRzIDcgMCBSDQo+Pg0KZW5kb2JqDQoNCjcgMCBvYmoNCjw8IC9MZW5ndGggNjc2ID4+DQpzdHJlYW0NCjIgSg0KQlQNCjAgMCAwIHJnDQovRjEgMDAyNyBUZg0KNTcuMzc1MCA3MjIuMjgwMCBUZA0KKCBTaW1wbGUgUERGIEZpbGUgMiApIFRqDQpFVA0KQlQNCi9GMSAwMDEwIFRmDQo2OS4yNTAwIDY4OC42MDgwIFRkDQooIC4uLmNvbnRpbnVlZCBmcm9tIHBhZ2UgMS4gWWV0IG1vcmUgdGV4dC4gQW5kIG1vcmUgdGV4dC4gQW5kIG1vcmUgdGV4dC4gKSBUag0KRVQNCkJUDQovRjEgMDAxMCBUZg0KNjkuMjUwMCA2NzYuNjU2MCBUZA0KKCBBbmQgbW9yZSB0ZXh0LiBBbmQgbW9yZSB0ZXh0LiBBbmQgbW9yZSB0ZXh0LiBBbmQgbW9yZSB0ZXh0LiBBbmQgbW9yZSApIFRqDQpFVA0KQlQNCi9GMSAwMDEwIFRmDQo2OS4yNTAwIDY2NC43MDQwIFRkDQooIHRleHQuIE9oLCBob3cgYm9yaW5nIHR5cGluZyB0aGlzIHN0dWZmLiBCdXQgbm90IGFzIGJvcmluZyBhcyB3YXRjaGluZyApIFRqDQpFVA0KQlQNCi9GMSAwMDEwIFRmDQo2OS4yNTAwIDY1Mi43NTIwIFRkDQooIHBhaW50IGRyeS4gQW5kIG1vcmUgdGV4dC4gQW5kIG1vcmUgdGV4dC4gQW5kIG1vcmUgdGV4dC4gQW5kIG1vcmUgdGV4dC4gKSBUag0KRVQNCkJUDQovRjEgMDAxMCBUZg0KNjkuMjUwMCA2NDAuODAwMCBUZA0KKCBCb3JpbmcuICBNb3JlLCBhIGxpdHRsZSBtb3JlIHRleHQuIFRoZSBlbmQsIGFuZCBqdXN0IGFzIHdlbGwuICkgVGoNCkVUDQplbmRzdHJlYW0NCmVuZG9iag0KDQo4IDAgb2JqDQpbL1BERiAvVGV4dF0NCmVuZG9iag0KDQo5IDAgb2JqDQo8PA0KL1R5cGUgL0ZvbnQNCi9TdWJ0eXBlIC9UeXBlMQ0KL05hbWUgL0YxDQovQmFzZUZvbnQgL0hlbHZldGljYQ0KL0VuY29kaW5nIC9XaW5BbnNpRW5jb2RpbmcNCj4+DQplbmRvYmoNCg0KMTAgMCBvYmoNCjw8DQovQ3JlYXRvciAoUmF2ZSBcKGh0dHA6Ly93d3cubmV2cm9uYS5jb20vcmF2ZVwpKQ0KL1Byb2R1Y2VyIChOZXZyb25hIERlc2lnbnMpDQovQ3JlYXRpb25EYXRlIChEOjIwMDYwMzAxMDcyODI2KQ0KPj4NCmVuZG9iag0KDQp4cmVmDQowIDExDQowMDAwMDAwMDAwIDY1NTM1IGYNCjAwMDAwMDAwMTkgMDAwMDAgbg0KMDAwMDAwMDA5MyAwMDAwMCBuDQowMDAwMDAwMTQ3IDAwMDAwIG4NCjAwMDAwMDAyMjIgMDAwMDAgbg0KMDAwMDAwMDM5MCAwMDAwMCBuDQowMDAwMDAxNTIyIDAwMDAwIG4NCjAwMDAwMDE2OTAgMDAwMDAgbg0KMDAwMDAwMjQyMyAwMDAwMCBuDQowMDAwMDAyNDU2IDAwMDAwIG4NCjAwMDAwMDI1NzQgMDAwMDAgbg0KDQp0cmFpbGVyDQo8PA0KL1NpemUgMTENCi9Sb290IDEgMCBSDQovSW5mbyAxMCAwIFINCj4+DQoNCnN0YXJ0eHJlZg0KMjcxNA0KJSVFT0YNCg==';

const PaymentServiceView = (props) => {
  const id = props.item.mode.id;
  const navigation = useNavigation();
  const [actionSheetVisible, setActionSheet] = useState(false);

  const [pdfModal, pdfModalVisible] = useState(false);

  const getData = (id) => {
    switch (id) {
      case 1:
        return props.item.schedule_flight;
      case 2:
        return props.item.schedule_car;
      case 3:
        return props.item.schedule_stay;
      case 4:
        return props.item.schedule_train;
      case 5:
        return props.item.schedule_cruise;
      case 6:
        return props.item.schedule_transfer;
      default:
        return null;
    }
  };

  const getOffer = (id) => {
    switch (id) {
      case 1:
        return props.item.schedule_flight;
      case 2:
        return props.item.schedule_car[0].car_offers != []
          ? props.item.schedule_car[0].car_offers[0]
          : [];
      case 3:
        return props.item.schedule_stay[0].stay_offers != []
          ? props.item.schedule_stay[0].stay_offers[0]
          : [];
      case 4:
        return props.item.schedule_train[0].train_offers != []
          ? props.item.schedule_train[0].train_offers[0]
          : [];
      case 5:
        return props.item.schedule_cruise[0].cruise_offers != []
          ? props.item.schedule_cruise[0].cruise_offers[0]
          : [];
          case 6:
        return props.item.schedule_cruise[0].cruise_offers != []
          ? props.item.schedule_cruise[0].cruise_offers[0]
          : [];
      default:
        return null;
    }
  };

  const handleDownload = async (imgUrl) => {
    RNFetchBlob.config({
      fileCache: true,
      appendExt: 'png',
    })
      .fetch('GET', imgUrl)
      .then((res) => {
        CameraRoll.save(res.data, 'photo')
          .then((res) => {
            SimpleToast.show('Saved to gallery');
          })
          .catch((err) => console.log(err));
      })
      .catch((error) => console.log(error));
  };

  const getViewById = (id, item) => {
    switch (id) {
      case 2:
        return <CarView item={item} />;
      case 3:
        return <StayView item={item} />;
      case 4:
        return <TrainView item={item} />;
      case 5:
        return <CruiseView item={item} />;
      default:
        return null;
    }
  };

  const getDates = (id, item) => {
    if (
      item.check_in == null &&
      item.depart == null &&
      item.pickup_date == null
    ) {
      return null;
    } else {
      return id == 3
        ? item.check_in + '  -  ' + item.check_out
        : id == 4
        ? item.depart + '  -  ' + item.return
        : id == 5
        ? item.depart
        : id == 2
        ? item.pickup_date + ' - ' + item.drop_off_date
        : null;
    }
  };

  const HeaderView = ({name, offer}) => {
    return (
      <View style={styles.header}>
        <View style={styles.borderView}>
          <Image style={styles.image} source={{uri: props.item.mode.image}} />
        </View>
        <View style={styles.headingView}>
          <Text style={styles.headerMode}>{props.item.mode.mode}</Text>
          <Text>{name ? name : ''}</Text>
        </View>
        <View style={styles.priceView}>
          <Text
            style={{
              color: COLOR.GREEN,
              fontSize: 16,
              fontFamily: FONTS.FAMILY_SEMIBOLD,
            }}>
            {offer ? offer.total_amount + ' SAR' : ''}
          </Text>
          <Text
            style={{
              fontSize: 10,
            }}>
            Including TAX
          </Text>
        </View>
      </View>
    );
  };

  const StayView = ({item}) => (
    <>
      <View style={[styles.borderView, styles.stayView]}>
        <Image
          style={styles.pinPoint}
          source={IMAGES.pinpoint}
          resizeMode={'contain'}
        />
        <Text style={([Styles.body_label], {width: null, textAlign: 'left'})}>
          {item.city}
        </Text>
      </View>
      {getDates(id, item) ? (
        <View style={[styles.dateView, styles.stayView]}>
          <Image
            style={styles.dateIcon}
            source={IMAGES.calendar_small}
            resizeMode={'contain'}
          />
          <Text style={([Styles.body_label], {width: null, textAlign: 'left'})}>
            {getDates(id, item)}
          </Text>
        </View>
      ) : null}
    </>
  );

  const CruiseView = ({item}) => (
    <>
      <View style={styles.optionView}>
        <View style={[styles.borderView, {width: '30%', minHeight: 60}]}>
          <Text style={([Styles.small_label], {textAlign: 'center'})}>
            {item.from ? item.from : ''}
          </Text>
        </View>
        <View>
          <Image
            style={styles.modeImage}
            source={{uri: props.item.mode.image}}
          />
          {getDates(id, item) ? (
            <View style={[styles.dateView, {flexDirection: 'column'}]}>
              <Text
                style={
                  ([Styles.body_label],
                  {
                    width: null,
                    textAlign: 'center',
                    fontSize: 10,
                  })
                }>
                {getDates(id, item)}
              </Text>
            </View>
          ) : null}
        </View>
        <View style={[styles.borderView, {width: '30%', minHeight: 60}]}>
          <Text style={([Styles.small_label], {textAlign: 'center'})}>
            {item.to ? item.to : ''}
          </Text>
        </View>
      </View>
    </>
  );

  const TrainView = ({item}) => (
    <>
      <View style={styles.optionView}>
        <View style={[styles.borderView, {width: '30%', minHeight: 60}]}>
          <Text style={([Styles.small_label], {textAlign: 'center'})}>
            {item.from ? item.from : ''}
          </Text>
        </View>
        <Image style={styles.modeImage} source={{uri: props.item.mode.image}} />
        <View style={[styles.borderView, {width: '30%', minHeight: 60}]}>
          <Text style={([Styles.small_label], {textAlign: 'center'})}>
            {item.to ? item.to : ''}
          </Text>
        </View>
      </View>
      {getDates(id, item) ? (
        <View style={[styles.dateView, styles.stayView]}>
          <Image
            style={styles.dateIcon}
            source={IMAGES.calendar_small}
            resizeMode={'contain'}
          />
          <Text style={([Styles.body_label], {width: null, textAlign: 'left'})}>
            {getDates(id, item)}
          </Text>
        </View>
      ) : null}
    </>
  );

  const CarView = ({item}) => (
    <>
      <View>
        <View style={styles.carRowView}>
          <Text style={{alignSelf: 'center', marginHorizontal: 10}}>
            {'Pickup'}
          </Text>
          <View style={[styles.borderView, styles.carPickView]}>
            <Image
              style={styles.pinPoint}
              source={IMAGES.pinpoint}
              resizeMode={'contain'}
            />
            <Text style={[{textAlign: 'center', width: null}]}>
              {item.pickup_location ? item.pickup_location : ''}
            </Text>
          </View>
        </View>
        <View style={styles.carRowView}>
          <Text style={{alignSelf: 'center', marginHorizontal: 10}}>
            {'Drop-off'}
          </Text>

          <View style={[styles.borderView, styles.carPickView]}>
            <Image
              style={styles.pinPoint}
              source={IMAGES.pinpoint}
              resizeMode={'contain'}
            />
            <Text style={[{textAlign: 'center', width: null}]}>
              {item.drop_off_location ? item.drop_off_location : ''}
            </Text>
          </View>
        </View>
      </View>

      {getDates(id, item) ? (
        <View style={[styles.dateView, styles.stayView, {marginTop: 15}]}>
          <Image
            style={styles.dateIcon}
            source={IMAGES.calendar_small}
            resizeMode={'contain'}
          />
          <Text style={([Styles.body_label], {width: null, textAlign: 'left'})}>
            {getDates(id, item)}
          </Text>
        </View>
      ) : null}
    </>
  );

  return (
    <View style={{paddingTop: 20}}>
      {id == 1 ? (
        <FlatList
          bounces={false}
          showsVerticalScrollIndicator={false}
          data={props.item.schedule_flight}
          renderItem={({item}) => {
            let flightModeName = item.flight_mode.name;
            let flightOffer = item.flight_offers;
            return (
              <FlatList
                bounces={false}
                showsVerticalScrollIndicator={false}
                data={item.schedule_modes}
                renderItem={({item}) => (
                  <TouchableWithoutFeedback
                    onPress={() => {
                      setActionSheet(true);
                    }}
                    style={styles.serviceContainer}>
                    <View style={styles.valueBackground}>
                      <HeaderView
                        name={flightModeName}
                        offer={flightOffer && flightOffer[0]}
                      />
                      <View style={styles.optionView}>
                        <View
                          style={[
                            styles.borderView,
                            {width: '30%', minHeight: 60},
                          ]}>
                          <Text
                            style={
                              ([Styles.small_label], {textAlign: 'center'})
                            }>
                            {item.from}
                          </Text>
                        </View>
                        <View>
                          <Image
                            style={styles.modeImage}
                            source={require('../../styles/assets/plane_horizontal.png')}
                          />
                          {item.depart ? (
                            <View
                              style={[
                                styles.dateView,
                                {flexDirection: 'column'},
                              ]}>
                              <Text
                                style={
                                  ([Styles.body_label],
                                  {
                                    width: null,
                                    textAlign: 'center',
                                    fontSize: 10,
                                  })
                                }>
                                {item.depart}
                              </Text>
                              <Text
                                style={
                                  ([Styles.body_label],
                                  {
                                    width: null,
                                    textAlign: 'center',
                                    fontSize: 10,
                                  })
                                }>
                                {item.return ? ' - ' + item.return : ''}
                              </Text>
                            </View>
                          ) : null}
                        </View>
                        <View
                          style={[
                            styles.borderView,
                            {width: '30%', minHeight: 60},
                          ]}>
                          <Text
                            style={
                              ([Styles.small_label], {textAlign: 'center'})
                            }>
                            {item.to}
                          </Text>
                        </View>
                      </View>

                      <Image
                        source={require('../../styles/assets/rArrow.png')}
                        style={styles.arrowView}
                      />
                    </View>
                    <Text style={styles.ticketText}>T I C K E T</Text>
                  </TouchableWithoutFeedback>
                )}
                numColumns={1}
                ListEmptyComponent={() => null}
                keyExtractor={(item, index) => index.toString()}
              />
            );
          }}
          numColumns={1}
          ListEmptyComponent={() => null}
          keyExtractor={(item, index) => index.toString()}
        />
      ) : (
        <FlatList
          bounces={false}
          showsVerticalScrollIndicator={false}
          data={getData(id)}
          renderItem={({item}) => (
            <TouchableWithoutFeedback style={styles.serviceContainer}>
              <View style={styles.valueBackground}>
                <Image
                  source={require('../../styles/assets/rArrow.png')}
                  style={styles.arrowView}
                />
                <HeaderView offer={getOffer(id)} />

                {getViewById(id, item)}
              </View>
              <Text style={styles.ticketText}>T I C K E T</Text>
            </TouchableWithoutFeedback>
          )}
          numColumns={1}
          ListEmptyComponent={() => null}
          keyExtractor={(item, index) => index.toString()}
        />
      )}

      <Modal
        animationType={'slide'}
        transparent={true}
        visible={actionSheetVisible}
        onRequestClose={() => {
          console.log('Modal has been closed.');
        }}>
        <View style={styles.modal}>
          <SafeAreaView style={{flex: 1}}>
            <View style={styles.white_view}>
              <Text
                style={[
                  Styles.subheading_label,
                  {marginVertical: 10, textAlign: 'center'},
                ]}>
                Choose Option
              </Text>
              <View
                style={{
                  flex: 1,
                  width: '90%',
                }}>
                <TouchableOpacity
                  onPress={() => {
                    setActionSheet(false);
                    setTimeout(async () => {
                      const shareOptions = {
                        title: 'Share file',
                        failOnCancel: false,
                        urls: [pdfBase64],
                      };

                      try {
                        const ShareResponse = await Share.open(shareOptions);
                        setResult(JSON.stringify(ShareResponse, null, 2));
                      } catch (error) {
                        console.log('Error =>', error);
                        setResult('error: '.concat(getErrorString(error)));
                      }
                    }, 50);
                  }}
                  style={[
                    styles.button_content,
                    {
                      backgroundColor: COLOR.WHITE,
                      flexDirection: 'row',
                    },
                  ]}>
                  <Text
                    style={[
                      Styles.bold_body_label,
                      {
                        color: COLOR.BLACK,
                        fontWeight: 'normal',
                      },
                    ]}>
                    Download
                  </Text>
                </TouchableOpacity>

                <TouchableOpacity
                  onPress={async () => {
                    setActionSheet(false);
                    pdfModalVisible(true);
                  }}
                  style={[
                    styles.button_content,
                    {
                      backgroundColor: COLOR.WHITE,
                      flexDirection: 'row',
                    },
                  ]}>
                  <Text
                    style={[
                      Styles.bold_body_label,
                      {
                        color: COLOR.BLACK,
                        fontWeight: 'normal',
                      },
                    ]}>
                    View ticket
                  </Text>
                </TouchableOpacity>

                <TouchableOpacity
                  onPress={() => {
                    setActionSheet(false);
                  }}
                  style={[
                    styles.button_content,
                    {
                      backgroundColor: COLOR.WHITE,
                      flexDirection: 'row',
                    },
                  ]}>
                  <Text
                    style={[
                      Styles.bold_body_label,
                      {
                        color: COLOR.BLACK,
                        fontWeight: 'normal',
                      },
                    ]}>
                    Cancel
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </SafeAreaView>
        </View>
      </Modal>
      <TicketPdfView
        modalVisible={pdfModal}
        setVisibility={() => {
          pdfModalVisible(false);
        }}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: COLOR.WHITE,
    borderRadius: 15,
    paddingVertical: 5,
  },
  header: {
    flexDirection: 'row',
    width: '100%',
  },
  headingView: {
    marginTop: 10,
    marginLeft: 5,
  },
  priceView: {
    alignItems: 'center',
    position: 'absolute',
    right: 10,
    top: 10,
  },
  image: {tintColor: COLOR.GREEN, height: 40, width: 40},

  dateView: {
    alignSelf: 'center',
    flexDirection: 'row',
    paddingHorizontal: 5,
  },
  dateIcon: {
    height: 20,
    width: 20,
    marginRight: 5,
  },

  serviceContainer: {
    marginVertical: 5,
    minHeight: 120,
    borderRadius: 10,
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: COLOR.GREEN,
    alignSelf: 'center',
    width: widthPercentageToDP(90),
  },
  modeImage: {
    height: 20,
    width: 20,
    marginHorizontal: 10,
    alignSelf: 'center',
  },
  optionView: {
    flexDirection: 'row',
    alignSelf: 'center',
    justifyContent: 'space-between',
    width: '90%',
    borderRadius: 20,
    alignItems: 'center',
  },
  ticketText: {
    transform: [{rotate: '90deg'}],
    fontFamily: FONTS.FAMILY_REGULAR,
    fontSize: 18,
    position: 'absolute',
    width: 120,
    height: 40,
    right: -40,
    paddingTop: 5,
    justifyContent: 'center',
    textAlign: 'center',
    color: COLOR.WHITE,
    alignSelf: 'center',
  },
  valueBackground: {
    minHeight: 120,
    backgroundColor: COLOR.WHITE,
    paddingVertical: 5,
    borderRadius: 10,
    width: '90%',
    alignSelf: 'flex-start',
    justifyContent: 'center',
    alignItems: 'center',
  },
  borderView: {
    borderWidth: 1,
    borderColor: COLOR.BORDER,
    borderRadius: 5,
    alignItems: 'center',
    justifyContent: 'center',
    marginHorizontal: 10,
    padding: 5,
    marginVertical: 5,
  },
  arrowView: {
    width: 10,
    height: 10,
    position: 'absolute',
    top: '45%',
    alignSelf: 'center',
    justifyContent: 'center',
    bottom: '45%',
    right: 5,
  },
  headerMode: {
    fontFamily: FONTS.FAMILY_SEMIBOLD,
    fontSize: 18,
  },
  pinPoint: {
    height: 20,
    width: 20,
    marginRight: 5,
  },
  stayView: {
    flexDirection: 'row',
    marginTop: 0,
    marginBottom: 10,
    marginHorizontal: 30,
    width: '75%',
    alignSelf: 'flex-end',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  carPickView: {
    marginHorizontal: 0,
    alignSelf: 'center',
    marginVertical: 0,
    flexDirection: 'row',
    width: '70%',
  },
  carRowView: {
    flexDirection: 'row',
    marginVertical: 5,
    width: '90%',
    justifyContent: 'space-between',
  },
  modal: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
    backgroundColor: '#00000030',
  },
  modalBody: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    flex: 1,
  },
  appicon_image: {
    height: 90,
    width: 90,
    marginTop: 50,
    marginLeft: 20,
    overflow: 'hidden',
  },
  white_view: {
    width: widthPercentageToDP(90),
    alignSelf: 'center',
    alignItems: 'center',
    backgroundColor: COLOR.WHITE,
    bottom: 20,
    position: 'absolute',
    borderRadius: 20,
  },
  button_content: {
    height: 45,
    width: '100%',
    borderRadius: 22.5,
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
  },
  image_view: {height: 15, width: 15, marginRight: 10},
});

export default PaymentServiceView;
