import React from 'react';

export const SelectionHoc = (WrappedComponent) => {
  return class extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        selectedData: this.props.route.params.selected
          ? this.props.route.params.selected
          : [],
      };
    }

    addItem = (item) => {
      let index = this.state.selectedData.indexOf(item.id);
      if (index == -1) {
        this.setState({
          selectedData: [...this.state.selectedData, item.id],
        });
      } else {
        let array = [...this.state.selectedData];
        array.splice(index, 1);
        this.setState({
          selectedData: array,
        });
      }
    };

    render() {
      return (
        <WrappedComponent
          addItem={this.addItem.bind(this)}
          selectedData={this.state.selectedData}
          {...this.props}
        />
      );
    }
  };
};
