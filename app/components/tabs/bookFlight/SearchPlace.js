import React from 'react';
import { Modal, Text, View, StyleSheet, Image } from 'react-native';
import { TextInput, TouchableOpacity } from 'react-native';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import COLOR from '../../../components/styles/Color';
import FONTS from '../../../components/styles/Fonts';
import IMAGES from '../../../components/styles/Images';
import Styles from '../../../components/styles/Styles';
import { DoneButtonKeyboard } from '../../commonView/CommonView';
import NavigationBar from '../../commonView/NavigationBar';


export default class SearchPlace extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: true,
    };
  }

  updateMasterState = (attrName, value) => {
    this.setState({ [attrName]: value });
  };

  render() {
    return (
      <View style={Styles.container}>
        <Modal
          animationType={'slide'}
          transparent={true}
          style={{ backgroundColor: COLOR.BLACK }}
          visible={this.state.modalVisible}
          onRequestClose={() => {
            console.log('Modal has been closed.');
          }}>
          <View style={styles.modal}>
            <View style={styles.modalBody}>
              <NavigationBar
                prop={this.props}
                navHeight={40}
                right_image={IMAGES.filter_image}
                name={'Origin'}
              />
              <View style={{ flex: 1, marginBottom: 10 }}>
                <View
                  style={{
                    height: 50,
                    marginBottom: 20,
                    backgroundColor: '#F1F4F6',
                    width: wp(90),
                    alignSelf: 'center',
                    paddingHorizontal: 10,
                    alignItems: 'center',
                    borderRadius: 22,
                    flexDirection: 'row',
                  }}>
                  <Image
                    style={{ height: 20, width: 20, marginRight: 10 }}
                    source={IMAGES.back_arrow}
                  />
                  <TextInput
                  inputAccessoryViewID={'uniqueID'}
                    style={[Styles.body_label, { flex: 1 }]}
                    placeholder={'Enter address'}
                    placeholderTextColor={COLOR.LIGHT_TEXT}
                  />
<DoneButtonKeyboard />
                </View>
                <View
                  style={{
                    height: 30,
                    marginBottom: 0,
                    width: wp(90),
                    alignSelf: 'center',
                    paddingHorizontal: 10,
                    alignItems: 'center',
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                  }}>
                  <Text style={[Styles.body_label, { width: null }]}>
                    Recent Searched
                  </Text>
                  <Text style={[Styles.small_label, { width: null }]}>
                    Clear all
                  </Text>
                </View>
                <TouchableOpacity
                  style={{
                    width: wp(90),
                    alignSelf: 'center',
                    height: 50,
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}
                  onPress={() => this.props.navigation.navigate('AddNewTrip')}>
                  <Text
                    style={[
                      Styles.small_label,
                      {
                        color: COLOR.WHITE,
                        width: wp(70),
                        textAlign: 'right',
                        marginBottom: 20,
                        marginTop: 5,
                      },
                    ]}>
                    Apply
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  modal: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
  },
  modalBody: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    backgroundColor: COLOR.WHITE,
    borderWidth: 1,
    width: wp(100),
    marginTop: 50,
    borderColor: COLOR.BORDER,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    flex: 1,
  },
  image: {
    height: 120,
    width: 120,
    alignSelf: 'center',
    marginVertical: 20,
  },
  headingLabel: {
    fontFamily: FONTS.FAMILY_BOLD,
    fontSize: FONTS.MEDIUM,
    color: COLOR.BLACK,
  },
  singleLineView: {
    marginVertical: 20,
    borderBottomColor: COLOR.GRAY,
    width: wp(100),
    alignSelf: 'center',
    borderBottomWidth: 1,
  },
  borderView: {
    borderRadius: 10,
    borderWidth: 3,
    width: wp(90),
    borderColor: COLOR.GRAY,
    alignSelf: 'center',
  },
});
