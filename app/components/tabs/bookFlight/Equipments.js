import React from 'react';
import {Modal, Text, View, StyleSheet, Image} from 'react-native';
import {
  FlatList,
  ScrollView,
  TextInput,
  TouchableOpacity,
  TouchableWithoutFeedback,
} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import COLOR from '../../../components/styles/Color';
import FONTS from '../../../components/styles/Fonts';
import IMAGES from '../../../components/styles/Images';
import Styles from '../../../components/styles/Styles';
import NavigationBar from '../../commonView/NavigationBar';
import {D_EQUIPMENTS, D_RESERVATION_POLICY} from '../../../helper/Constants';
import FlightAction from '../../../redux/action/FlightAction';
import {connect} from 'react-redux';
import { DoneButtonKeyboard } from '../../commonView/CommonView';

class Equipments extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: true,
      equipmentData: [],
    };

    this.props.getFlightEquipmentData();
  }

  componentDidMount() {
    if (this.props.flightEquipmentData.length) {
      this.setState({
        equipmentData: this.props.flightEquipmentData,
      });
    }
  }

  componentWillReceiveProps(props) {
    if (props.flightEquipmentData != this.props.flightEquipmentData) {
      this.setState({
        equipmentData: props.flightEquipmentData,
      });
    }
  }

  updateMasterState = (attrName, value) => {
    this.setState({[attrName]: value});
  };

  SelectionView = (props) => {
    return (
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          marginTop: 10,
        }}>
        <Text style={[Styles.body_label, {width: 200}]}>{props.name}</Text>
        <Text style={[Styles.body_label, {width: null}]}>{props.time}</Text>
        <TouchableOpacity
          style={{
            height: 30,
            width: 30,
            borderRadius: 5,
            backgroundColor: props.bg,
            borderColor: COLOR.GRAY,
            justifyContent: 'center',
            alignItems: 'center',
            borderWidth: 1,
          }}>
          <Image
            resizeMode={'contain'}
            style={{height: 20, width: 20, alignSelf: 'center'}}
            source={props.image}
          />
        </TouchableOpacity>
      </View>
    );
  };

  render() {
    return (
      <View style={Styles.container}>
        <Modal
          animationType={'slide'}
          transparent={true}
          style={{backgroundColor: COLOR.BLACK}}
          visible={this.state.modalVisible}
          onRequestClose={() => {
            console.log('Modal has been closed.');
          }}>
          <View style={styles.modal}>
            <View style={styles.modalBody}>
              <NavigationBar
                prop={this.props}
                navHeight={40}
                right_image={IMAGES.filter_image}
                name={'Equipments'}
              />
              <View
                style={{
                  width: wp(90),
                  alignSelf: 'center',
                  paddingVertical: 10,
                  flex: 1,
                  marginVertical: 0,
                }}>
                <View>
                  <FlatList
                    data={this.state.equipmentData}
                    renderItem={({item}) => (
                      <TouchableWithoutFeedback>
                        <View
                          style={{
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            marginTop: 10,
                          }}>
                          <Text style={[Styles.body_label, {width: 250}]}>
                            {item.name}
                          </Text>

                          <TouchableOpacity
                            onPress={() => {
                              this.setState({selectedItem: item.name});
                            }}
                            style={{
                              height: 30,
                              width: 30,
                              borderRadius: 5,
                              backgroundColor:
                                this.state.selectedItem == item.name
                                  ? COLOR.GREEN
                                  : COLOR.WHITE,
                              borderColor: COLOR.GRAY,
                              justifyContent: 'center',
                              alignItems: 'center',
                              borderWidth: 1,
                            }}>
                            <Image
                              resizeMode={'contain'}
                              style={{
                                height: 20,
                                width: 20,
                                alignSelf: 'center',
                              }}
                              source={IMAGES.tick_image}
                            />
                          </TouchableOpacity>
                        </View>
                      </TouchableWithoutFeedback>
                    )}
                    numColumns={'1'}
                    removeClippedSubviews={false}
                    showsVerticalScrollIndicator={false}
                    showsHorizontalScrollIndicator={false}
                    keyExtractor={(item, index) => index.toString()}
                  />
                </View>
                <View
                  style={{
                    width: wp(90),
                    alignSelf: 'center',
                    height: 120,
                    borderWidth: 0.5,
                    borderColor: COLOR.GRAY,
                    borderRadius: 10,
                    marginVertical: 20,
                  }}>
                  <TextInput
                  inputAccessoryViewID={'uniqueID'}
                    style={[Styles.body_label, {padding: 15}]}
                    placeholder={
                      'Please specify if you plan to bring any equipments'
                    }
                    placeholderTextColor={COLOR.LIGHT_TEXT}
                    multiline={true}
                  />
                  <DoneButtonKeyboard />
                </View>
              </View>

              <TouchableOpacity
                style={{
                  width: wp(90),
                  alignSelf: 'center',
                  height: 50,
                  alignItems: 'center',
                  justifyContent: 'center',
                  backgroundColor: COLOR.GREEN,
                  borderRadius: 10,
                  marginBottom: 20,
                }}
                onPress={() => {
                  this.setState({modalVisible: false}, (val) => {
                    this.props.navigation.navigate('Airlines');
                  });
                }}>
                <Text
                  style={[
                    Styles.button_font,
                    {
                      color: COLOR.WHITE,
                      width: null,
                      textAlign: 'center',
                    },
                  ]}>
                  Apply
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  modal: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
  },
  modalBody: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    backgroundColor: COLOR.WHITE,
    borderWidth: 1,
    width: wp(100),
    marginTop: 50,
    borderColor: COLOR.BORDER,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    flex: 1,
  },
  image: {
    height: 120,
    width: 120,
    alignSelf: 'center',
    marginVertical: 20,
  },
  headingLabel: {
    fontFamily: FONTS.FAMILY_BOLD,
    fontSize: FONTS.MEDIUM,
    color: COLOR.BLACK,
  },
  singleLineView: {
    marginVertical: 20,
    borderBottomColor: COLOR.GRAY,
    width: wp(100),
    alignSelf: 'center',
    borderBottomWidth: 1,
  },
});

const mapStateToProps = (state, ownProps) => {
  return {
    flightEquipmentData: state.flightReducer.flightEquipmentData,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getFlightEquipmentData: (val) => {
      dispatch(FlightAction.getEquipmentData(val));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Equipments);
