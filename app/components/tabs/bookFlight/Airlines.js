import React from 'react';
import {Modal, Text, View, StyleSheet, Image} from 'react-native';
import {
  FlatList,
  TouchableOpacity,
  TouchableWithoutFeedback,
} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import COLOR from '../../../components/styles/Color';
import FONTS from '../../../components/styles/Fonts';
import IMAGES from '../../../components/styles/Images';
import Styles from '../../../components/styles/Styles';
import FastImage from 'react-native-fast-image';
import NavigationBar from '../../commonView/NavigationBar';
import {D_AEROPLANE_SUPPLIER_DATA} from '../../../helper/Constants';
import {connect} from 'react-redux';
import FlightAction from '../../../redux/action/FlightAction';

class Airlines extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: true,
      aeroplaneData: [],
      selectedIndex: [],
    };

    this.props.getAirlinesData();
  }

  componentDidMount() {
    if (this.props.airlineData.length) {
      this.setState({
        aeroplaneData: this.props.airlineData,
      });
    }

    if (this.props.route.params.selectedItem) {
      this.setState({
        selectedIndex: this.props.route.params.selectedItem,
      });
    }
  }

  componentWillReceiveProps(props) {
    if (props.airlineData != this.props.airlineData) {
      console.log('flight data is:-', props.airlineData);
      this.setState({
        aeroplaneData: props.airlineData,
      });
    }
  }

  updateMasterState = (attrName, value) => {
    this.setState({[attrName]: value});
  };

  addOrRemoveItem = (item) => {
    let array = [...this.state.selectedIndex];
    let index = array.findIndex((value) => value == item);
    if (index != -1) {
      array.splice(index, 1);
    } else {
      array.push(item);
    }
    this.setState({
      selectedIndex: array,
    });
  };

  render() {
    return (
      <View style={Styles.container}>
        <Modal
          animationType={'slide'}
          transparent={true}
          style={{backgroundColor: COLOR.BLACK}}
          visible={this.state.modalVisible}
          onRequestClose={() => {
            console.log('Modal has been closed.');
          }}>
          <View style={styles.modal}>
            <View style={styles.modalBody}>
              <NavigationBar
                prop={this.props}
                height={40}
                right_image={IMAGES.filter_image}
                name={'Airlines'}
              />
              <View
                style={{
                  flex: 1,
                  width: wp(90),

                  alignSelf: 'center',
                  paddingVertical: 10,
                }}>
                <FlatList
                  style={{flex: 0}}
                  data={this.state.aeroplaneData}
                  renderItem={({item}) => (
                    <TouchableWithoutFeedback
                      onPress={() => {
                        this.addOrRemoveItem(item.id);
                      }}>
                      <View
                        style={{
                          flex: 1,
                          alignItems: 'center',
                          justifyContent: 'space-between',
                          margin: 5,
                        }}>
                        <View style={{width: wp(28)}}>
                          <FastImage
                            resizeMode={FastImage.resizeMode.cover}
                            style={{
                              height: 80,
                              width: 80,
                              alignSelf: 'center',
                              borderRadius: 40,
                              borderWidth: 1,
                              borderColor: this.state.selectedIndex.some(
                                (value) => value == item.id,
                              )
                                ? COLOR.BLUE
                                : 'transparent',
                            }}
                            source={{
                              uri: item.image,
                              priority: FastImage.priority.normal,
                            }}
                          />
                        </View>
                        <Text
                          style={[
                            Styles.small_label,
                            {width: null, textAlign: 'center'},
                          ]}>
                          {item.name}
                        </Text>
                      </View>
                    </TouchableWithoutFeedback>
                  )}
                  numColumns={'3'}
                  removeClippedSubviews={false}
                  showsVerticalScrollIndicator={false}
                  showsHorizontalScrollIndicator={false}
                  keyExtractor={(item, index) => index.toString()}
                />
              </View>
              <TouchableOpacity
                style={{
                  width: wp(90),
                  alignSelf: 'center',
                  height: 50,
                  alignItems: 'center',
                  justifyContent: 'center',
                  backgroundColor: COLOR.GREEN,
                  borderRadius: 10,
                  marginBottom: 20,
                }}
                onPress={() => {
                  this.setState({modalVisible: false}, (val) => {
                    this.props.route.params.handleSelection(
                      this.state.selectedIndex,
                      false
                    );
                    this.props.navigation.goBack(null);
                  });
                }}>
                <Text
                  style={[
                    Styles.button_font,
                    {
                      color: COLOR.WHITE,
                      width: null,
                      textAlign: 'center',
                    },
                  ]}>
                  Apply
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  modal: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
  },
  modalBody: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    backgroundColor: COLOR.WHITE,
    borderWidth: 1,
    width: wp(100),
    marginTop: 50,
    borderColor: COLOR.BORDER,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    flex: 1,
  },
  image: {
    height: 120,
    width: 120,
    alignSelf: 'center',
    marginVertical: 20,
  },
  headingLabel: {
    fontFamily: FONTS.FAMILY_BOLD,
    fontSize: FONTS.MEDIUM,
    color: COLOR.BLACK,
  },
  singleLineView: {
    marginVertical: 20,
    borderBottomColor: COLOR.GRAY,
    width: wp(100),
    alignSelf: 'center',
    borderBottomWidth: 1,
  },
  borderView: {
    borderRadius: 10,
    borderWidth: 1,
    width: wp(90),
    borderColor: COLOR.GRAY,
    alignSelf: 'center',
  },
});

const mapStateToProps = (state, ownProps) => {
  return {
    airlineData: state.flightReducer.flightAirlineData,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getAirlinesData: (val) => {
      dispatch(FlightAction.getAirlinesData(val));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Airlines);
