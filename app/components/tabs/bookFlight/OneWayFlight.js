import React from 'react';
import {
  View,
  Image,
  StyleSheet,
  Text,
  SafeAreaView,
  TextInput,
  Alert,
} from 'react-native';
import {TouchableOpacity} from 'react-native';
import Switch from '../../commonView/Switch';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import COLOR from '../../../components/styles/Color';
import IMAGES from '../../../components/styles/Images';
import Styles from '../../../components/styles/Styles';
import FONTS from '../../styles/Fonts';
import SimpleToast from 'react-native-simple-toast';
import SelectDate from '../calendar/SelectDate';
import GooglePlacesTextInput from '../../commonView/GooglePlacesTextInput';

export default class OneWayFlight extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      tripData: this.props.data,
      flightId: this.props.flightId,
      screen: this.props.screen,
      package_request_id: this.props.props.route.params.package_request_id,
      checkInDate: this.props.depart,
      checkOutDate: null,
      from: '',
      to: '',
      fromNearBy: false,
      toNearBy: false,
      minDate: this.props.depart,
      maxDate: this.props.return,
      selector: 0,
      openCalendar: false,
      fromFlexibility: 0,
      toFlexibility: 0,
    };
  }

  componentDidMount() {
    if (this.state.flightId) {
      let data = [...this.props.data];
      console.log(data);
      this.setState({
        checkInDate: data[0].depart,
        checkOutDate: data[0].return ? data[0].return : null,
        from: data[0].from,
        to: data[0].to,
        fromFlexibility: data[0].depart_flexibility,
        toFlexibility: data[0].depart_flexibility,
        fromNearBy: data[0].from_nearby_airport == 1 ? true : false,
        toNearBy: data[0].to_nearby_airport == 1 ? true : false,
      });
    }
  }

  componentDidUpdate(prevProps) {}

  updateMasterState = (attrName, value) => {
    this.setState({[attrName]: value}, () => {});
  };

  handleNavigation() {
    if (this.state.from == '') {
      SimpleToast.show('Enter from location');
    } else if (this.state.to == '') {
      SimpleToast.show('Enter to location');
    } else if (this.state.minDate && this.state.checkInDate == null) {
      SimpleToast.show('Select Checkin date');
    } else if (
      this.state.minDate &&
      this.state.screen == 2 &&
      this.state.checkOutDate == null
    ) {
      SimpleToast.show('Select Checkout Date');
    } else {
      let params = {
        from: this.state.from,
        to: this.state.to,
        depart: this.state.checkInDate,
        depart_flexibility: this.state.fromFlexibility,
        from_nearby_airport: this.state.fromNearBy ? 1 : 0,
        to_nearby_airport: this.state.toNearBy ? 1 : 0,
      };
      if (this.state.screen == 2) {
        params.return = this.state.checkOutDate;
        params.return_flexibility = this.state.toFlexibility;
      }
      let paramsScreen = this.props.props.route.params;
      const screens = paramsScreen.screens;
      const index = paramsScreen.index;
      this.props.navigation.replace('FlightOption', {
        params: [params],
        mode: this.state.screen,
        id: this.props.props.route.params.id,
        flightId: this.state.flightId ? this.state.flightId : null,
        package_request_id: this.state.package_request_id,
        screens: screens,
        index: index,
        depart: paramsScreen.depart,
        return: paramsScreen.return,
      });
    }
  }

  render() {
    return (
      <SafeAreaView
        style={[Styles.container, {backgroundColor: COLOR.BACKGROUND}]}>
        <View style={styles.flexView}>
          <View style={[styles.flightBackground]}>
            <View style={[styles.fieldView, {zIndex: 990}]}>
              <View>
                <Image
                  source={IMAGES.depart}
                  style={styles.icon}
                  resizeMode={'contain'}
                />
              </View>
              <View style={[styles.flexView, {zIndex: 1000}]}>
                <Text style={styles.label}>From</Text>

                <GooglePlacesTextInput
                  value={this.state.from}
                  onValueChange={(text) => this.setState({from: text})}
                />
              </View>
              <View>
                <Text style={styles.label}>Add nearby airports</Text>
                <Switch
                  isChecked={this.state.fromNearBy}
                  onCheckedListener={() =>
                    this.setState({
                      fromNearBy: !this.state.fromNearBy,
                    })
                  }
                />
              </View>
            </View>
            <View style={styles.hr} />
            <View style={[styles.fieldView, {zIndex: 900}]}>
              <View>
                <Image
                  source={IMAGES.arrive}
                  style={styles.icon}
                  resizeMode={'contain'}
                />
              </View>
              <View style={styles.flexView}>
                <Text style={styles.label}>To</Text>
                <GooglePlacesTextInput
                  value={this.state.to}
                  onValueChange={(text) => this.setState({to: text})}
                />
              </View>
              <View style={styles.nearByView}>
                <Text style={styles.label}>Add nearby airports</Text>
                <Switch
                  isChecked={this.state.toNearBy}
                  onCheckedListener={() =>
                    this.setState({
                      toNearBy: !this.state.toNearBy,
                    })
                  }
                />
              </View>
            </View>

            {this.state.minDate != null ? (
              <>
                <View style={styles.hr} />
                <View style={styles.fieldView}>
                  <View>
                    <Image
                      source={IMAGES.return_date}
                      style={styles.icon}
                      resizeMode={'contain'}
                    />
                  </View>
                  <View style={styles.flexView}>
                    <Text style={styles.label}>Departure Date</Text>
                    <TouchableOpacity
                      onPress={() => {
                        this.setState({
                          selector: 0,
                          openCalendar: true,
                        });
                      }}>
                      <Text style={styles.textInput}>
                        {this.state.checkInDate
                          ? this.state.checkInDate
                          : 'Select Depart Date'}
                      </Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </>
            ) : null}

            {this.state.screen == 2 && this.state.minDate ? (
              <View>
                <View
                  style={[
                    Styles.line_view,
                    {alignSelf: 'center', width: wp(75)},
                  ]}
                />
                <View style={styles.fieldView}>
                  <View>
                    <Image
                      source={IMAGES.return_date}
                      style={styles.icon}
                      resizeMode={'contain'}
                    />
                  </View>
                  <View style={styles.flexView}>
                    <Text style={styles.label}>Return Date</Text>
                    <TouchableOpacity
                      onPress={() => {
                        this.setState({
                          selector: 1,
                          openCalendar: true,
                        });
                      }}>
                      <Text style={styles.textInput}>
                        {this.state.checkOutDate
                          ? this.state.checkOutDate
                          : 'Select Return Date'}
                      </Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            ) : null}
          </View>
        </View>

        <View>
          <TouchableOpacity
            style={styles.nextButton}
            onPress={() => this.handleNavigation()}>
            <Text
              style={[
                Styles.button_font,
                {
                  color: COLOR.WHITE,
                  width: null,
                  textAlign: 'center',
                },
              ]}>
              Next
            </Text>
          </TouchableOpacity>
        </View>
        {this.state.openCalendar ? (
          <SelectDate
            allowRangeSelection={false}
            selector={this.state.selector}
            updateMasterState={this.updateMasterState}
            props={this.props.props}
            modal={true}
            minDate={this.state.minDate}
            maxDate={this.state.maxDate}
          />
        ) : null}
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  topBar: {
    height: 50,
    shadowColor: COLOR.VOILET,
    shadowOpacity: 0.2,
    elevation: 3,
    backgroundColor: COLOR.WHITE,
    flexDirection: 'row',
    shadowOffset: {width: 2, height: 2},
    justifyContent: 'space-between',
    width: wp(95),
    alignSelf: 'center',
    marginVertical: 20,

    alignItems: 'center',
  },
  nextButton: {
    width: wp(90),
    alignSelf: 'center',
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: COLOR.GREEN,
    borderRadius: 10,
    marginVertical: 10,
  },
  plusButton: {
    width: 25,
    height: 25,
    borderRadius: 10,
    marginRight: 5,
  },
  addButton: {
    width: wp(90),
    alignSelf: 'center',
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: COLOR.WHITE,
    borderRadius: 10,
    borderColor: COLOR.LIGHT_TEXT,
    borderWidth: 1,
    marginTop: 40,
  },
  viewDetailsButton: {
    width: wp(90),
    alignSelf: 'center',
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: COLOR.GREEN,
    borderRadius: 10,
    marginVertical: 10,
  },
  flightBackground: {
    alignSelf: 'center',
    shadowColor: COLOR.VOILET,
    elevation: 3,
    shadowOpacity: 0.2,
    shadowOffset: {width: 2, height: 2},
    backgroundColor: COLOR.WHITE,
    width: wp(90),
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
  },
  fieldView: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
    margin: 15,
  },
  icon: {
    height: 25,
    width: 25,
    marginRight: 15,
  },
  textInput: {
    fontSize: FONTS.MEDIUM,
    fontFamily: FONTS.FAMILY_SEMIBOLD,
    paddingLeft: 0,
  },
  label: {
    fontSize: FONTS.SMALL,
    fontFamily: FONTS.FAMILY_REGULAR,
    color: COLOR.VOILET,
  },
  nearByView: {
    flex: 1,
    alignItems: 'flex-end',
  },
  flexView: {
    flex: 1,
  },
  hr: {
    alignSelf: 'center',
    width: wp(75),
    height: 1,
    backgroundColor: COLOR.GRAY,
  },
});
