import React from 'react';
import {ToastAndroid} from 'react-native';
import {Modal, Text, View, StyleSheet, Image, Button} from 'react-native';
import {TextInput, TouchableOpacity} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import COLOR from '../../../components/styles/Color';
import FONTS from '../../../components/styles/Fonts';
import IMAGES from '../../../components/styles/Images';
import Styles from '../../../components/styles/Styles';
import { DoneButtonKeyboard } from '../../commonView/CommonView';
import {
  getEndDateTimestamp,
  getStartDateTimestamp,
} from '../../commonView/Helpers';
import NavigationBar from '../../commonView/NavigationBar';
import Switch from '../../commonView/Switch';
import SelectDate from '../calendar/SelectDate';

export default class AddNewMultipleFlight extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: true,
      openCalendar: false,
      from: '',
      to: '',
      checkInDate: '',
      sFrom: '',
      sTo: '',
      sCheckInDate: '',
      attrName: null,
      flight_mode_id: this.props.route.params.mode,
      id: this.props.route.params.id,
      from_nearby_airport: false,
      to_nearby_airport: false,
      s_from_nearby_airport: false,
      s_to_nearby_airport: false,
      attrFlexibility: null,
      depart_flexibility: 0,
      s_depart_flexibility: 0,
      minDate: getStartDateTimestamp(this.props.route.params.depart),
      maxDate: getEndDateTimestamp(this.props.route.params.return),
      tripDepart: this.props.route.params.depart,
      tripReturn: this.props.route.params.return,
      package_request_id: this.props.route.params.package_request_id,
    };
  }

  updateMasterState = (attrName, value) => {
    this.setState({[attrName]: value});
    console.log(attrName + ' - ' + value);
  };

  onCheckedListener = (val) => {
    switch (val) {
      case 1:
        this.setState({
          from_nearby_airport: !this.state.from_nearby_airport,
        });
        break;
      case 2:
        this.setState({
          to_nearby_airport: !this.state.to_nearby_airport,
        });
        break;
      case 3:
        this.setState({
          s_from_nearby_airport: !this.state.s_from_nearby_airport,
        });
        break;
      case 4:
        this.setState({
          s_to_nearby_airport: !this.state.s_to_nearby_airport,
        });
        break;
    }
  };

  componentDidMount() {
    if (this.props.route.params.data) {
      let params = this.props.route.params;
      let data = params.data;

      this.setState({
        from: data[0].from,
        to: data[0].to,
        checkInDate: data[0].depart,
        from_nearby_airport: data[0].from_nearby_airport == 1 ? true : false,
        to_nearby_airport: data[0].to_nearby_airport == 1 ? true : false,
        depart_flexibility: data[0].depart_flexibility,
        sFrom: data[1].from,
        sTo: data[1].to,
        sCheckInDate: data[1].depart,
        s_from_nearby_airport: data[1].from_nearby_airport == 1 ? true : false,
        s_to_nearby_airport: data[1].to_nearby_airport == 1 ? true : false,
        s_depart_flexibility: data[1].depart_flexibility,
      });
    }
  }

  openCalendar = (attrName, attrFlexibility) => {
    this.setState({
      attrFlexibility: attrFlexibility,
      attrName: attrName,
      openCalendar: true,
    });
  };

  handleNavigation = () => {
    if (this.state.from == '') {
      SimpleToast.show('Enter from location');
    } else if (this.state.to == '') {
      SimpleToast.show('Enter to location');
    } else if (this.state.tripDepart && this.state.checkInDate == null) {
      SimpleToast.show('Select Checkin date');
    }
    if (this.state.sFrom == '') {
      SimpleToast.show('Enter from location');
    } else if (this.state.sTo == '') {
      SimpleToast.show('Enter to location');
    } else if (this.state.tripDepart && this.state.sCheckInDate == null) {
      SimpleToast.show('Select Checkin date');
    } else {
      this.setState({modalVisible: false});
      let params = {
        from: this.state.from,
        to: this.state.to,
        depart: this.state.checkInDate,
        depart_flexibility: this.state.depart_flexibility,
        from_nearby_airport: this.state.from_nearby_airport ? 1 : 0,
        to_nearby_airport: this.state.to_nearby_airport ? 1 : 0,
      };
      let params2 = {
        from: this.state.sFrom,
        to: this.state.sTo,
        depart: this.state.sCheckInDate,
        depart_flexibility: this.state.s_depart_flexibility,
        from_nearby_airport: this.state.s_from_nearby_airport ? 1 : 0,
        to_nearby_airport: this.state.s_to_nearby_airport ? 1 : 0,
      };

      this.props.navigation.replace('FlightOption', {
        params: [params, params2],
        mode: this.state.flight_mode_id,
        id: this.state.id,
        flightId: this.props.route.params.flight_id
          ? this.props.route.params.flight_id
          : null,
        package_request_id: this.state.package_request_id,
      });
    }
  };

  render() {
    return (
      <View style={Styles.container}>
        {this.state.openCalendar ? (
          <SelectDate
            allowRangeSelection={false}
            attrName={this.state.attrName}
            selector={0}
            attrFlexibility={this.state.attrFlexibility}
            updateMasterState={this.updateMasterState}
            props={this.props.props}
            modal={true}
            minDate={this.state.minDate}
            maxDate={this.state.maxDate}
          />
        ) : (
          <Modal
            animationType={'slide'}
            transparent={true}
            style={{backgroundColor: COLOR.BLACK}}
            visible={this.state.modalVisible}
            onRequestClose={() => {
              console.log('Modal has been closed.');
            }}>
            <View style={styles.modal}>
              <View style={styles.modalBody}>
                <NavigationBar
                  prop={this.props}
                  navHeight={40}
                  name={
                    this.props.route.params.flight_id
                      ? 'Edit Flight'
                      : 'Add New Flight'
                  }
                />
                <View
                  style={{alignSelf: 'center', width: wp(90), marginTop: 10}}>
                  <Text style={{marginVertical: 10}}>Flight 1</Text>
                  <View
                    style={{
                      flexDirection: 'row',
                      height: 50,
                      alignItems: 'center',
                      marginBottom: 0,
                    }}>
                    <Image
                      style={{height: 30, width: 30, marginRight: 10}}
                      source={IMAGES.pinpoint}
                      resizeMode={'contain'}
                    />
                    <View>
                      <Text style={Styles.small_label}>From</Text>
                      <TextInput
                      inputAccessoryViewID={'uniqueID'}
                        onPress={() => {
                          this.props.navigation.navigate('GooglePlaces', {
                            updateMasterState: this.updateMasterState,
                            attrName: 'from',
                          });
                          this.setState({modalVisible: false});
                        }}
                        placeholder={'City or Airport'}
                        placeholderTextColor={COLOR.LIGHT_TEXT_COLOR}
                        value={this.state.from}
                        onChangeText={(text) => this.setState({from: text})}
                        style={Styles.body_label}
                      />
                      <DoneButtonKeyboard />
                    </View>
                    <View style={styles.switchView}>
                      <Text style={styles.switchText}>Add nearby airports</Text>
                      <Switch
                        isChecked={this.state.from_nearby_airport}
                        onCheckedListener={() => this.onCheckedListener(1)}
                      />
                    </View>
                  </View>
                  <View
                    style={[
                      Styles.line_view,
                      {width: wp(90), marginVertical: 10},
                    ]}
                  />
                  <View
                    style={{
                      flexDirection: 'row',
                      height: 50,
                      alignItems: 'center',
                      marginBottom: 0,
                    }}>
                    <Image
                      style={{height: 30, width: 30, marginRight: 10}}
                      source={IMAGES.pinpoint}
                      resizeMode={'contain'}
                    />
                    <View>
                      <Text style={Styles.small_label}>To</Text>
                      <TextInput
                      inputAccessoryViewID={'uniqueID'}
                        onPress={() => {
                          this.props.navigation.navigate('GooglePlaces', {
                            updateMasterState: this.updateMasterState,
                            attrName: 'to',
                          });
                          this.setState({modalVisible: false});
                        }}
                        placeholder={'City or Airport'}
                        placeholderTextColor={COLOR.LIGHT_TEXT_COLOR}
                        value={this.state.to}
                        onChangeText={(text) => this.setState({to: text})}
                        style={Styles.body_label}
                      />
                    </View>
                    <View style={styles.switchView}>
                      <Text style={styles.switchText}>Add nearby airports</Text>
                      <Switch
                        isChecked={this.state.to_nearby_airport}
                        onCheckedListener={() => this.onCheckedListener(2)}
                      />
                    </View>
                  </View>
                  <View
                    style={[
                      Styles.line_view,
                      {width: wp(90), marginVertical: 10},
                    ]}
                  />
                  {this.state.tripDepart ? (
                    <View
                      style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        marginBottom: 0,
                      }}>
                      <Image
                        style={{height: 30, width: 30, marginRight: 10}}
                        source={IMAGES.departure_date}
                        resizeMode={'contain'}
                      />
                      <View>
                        <Text style={Styles.small_label}>Departure Date</Text>
                        <TouchableOpacity
                          onPress={() => {
                            this.openCalendar(
                              'checkInDate',
                              'depart_flexibility',
                            );
                          }}>
                          <Text style={Styles.body_label}>
                            {this.state.checkInDate
                              ? this.state.checkInDate
                              : 'Select Date'}
                          </Text>
                        </TouchableOpacity>
                      </View>
                    </View>
                  ) : null}
                </View>
                <View
                  style={{alignSelf: 'center', width: wp(90), marginTop: 10}}>
                  <Text style={{marginVertical: 10}}>Flight 2</Text>
                  <View
                    style={{
                      flexDirection: 'row',
                      height: 50,
                      alignItems: 'center',
                      marginBottom: 0,
                    }}>
                    <Image
                      style={{height: 30, width: 30, marginRight: 10}}
                      source={IMAGES.pinpoint}
                      resizeMode={'contain'}
                    />
                    <View>
                      <Text style={Styles.small_label}>From</Text>
                      <TextInput
                      inputAccessoryViewID={'uniqueID'}
                        onPress={() => {
                          this.props.navigation.navigate('GooglePlaces', {
                            updateMasterState: this.updateMasterState,
                            attrName: 'sFrom',
                          });
                          this.setState({modalVisible: false});
                        }}
                        placeholder={'City or Airport'}
                        placeholderTextColor={COLOR.LIGHT_TEXT_COLOR}
                        value={this.state.sFrom}
                        onChangeText={(text) => this.setState({sFrom: text})}
                        style={Styles.body_label}
                      />
                    </View>
                    <View style={styles.switchView}>
                      <Text style={styles.switchText}>Add nearby airports</Text>
                      <Switch
                        isChecked={this.state.s_from_nearby_airport}
                        onCheckedListener={() => this.onCheckedListener(3)}
                      />
                    </View>
                  </View>
                  <View
                    style={[
                      Styles.line_view,
                      {width: wp(90), marginVertical: 10},
                    ]}
                  />
                  <View
                    style={{
                      flexDirection: 'row',
                      height: 50,
                      alignItems: 'center',
                      marginBottom: 0,
                    }}>
                    <Image
                      style={{height: 30, width: 30, marginRight: 10}}
                      source={IMAGES.pinpoint}
                      resizeMode={'contain'}
                    />
                    <View>
                      <Text style={Styles.small_label}>To</Text>
                      <TextInput
                      inputAccessoryViewID={'uniqueID'}
                        onPress={() => {
                          this.props.navigation.navigate('GooglePlaces', {
                            updateMasterState: this.updateMasterState,
                            attrName: 'sTo',
                          });
                          this.setState({modalVisible: false});
                        }}
                        placeholder={'City or Airport'}
                        placeholderTextColor={COLOR.LIGHT_TEXT_COLOR}
                        value={this.state.sTo}
                        onChangeText={(text) => this.setState({sTo: text})}
                        style={Styles.body_label}
                      />
                    </View>
                    <View style={styles.switchView}>
                      <Text style={styles.switchText}>Add nearby airports</Text>
                      <Switch
                        isChecked={this.state.s_to_nearby_airport}
                        onCheckedListener={() => this.onCheckedListener(4)}
                      />
                    </View>
                  </View>
                  <View
                    style={[
                      Styles.line_view,
                      {width: wp(90), marginVertical: 10},
                    ]}
                  />
                  {this.state.tripDepart ? (
                    <View
                      style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        marginBottom: 0,
                      }}>
                      <Image
                        style={{height: 30, width: 30, marginRight: 10}}
                        source={IMAGES.departure_date}
                        resizeMode={'contain'}
                      />
                      <View>
                        <Text style={Styles.small_label}>Departure Date</Text>
                        <TouchableOpacity
                          onPress={() => {
                            this.openCalendar(
                              'sCheckInDate',
                              's_depart_flexibility',
                            );
                          }}>
                          <Text style={Styles.body_label}>
                            {this.state.sCheckInDate
                              ? this.state.sCheckInDate
                              : 'Select Date'}
                          </Text>
                        </TouchableOpacity>
                      </View>
                    </View>
                  ) : null}
                </View>
                <View
                  style={{
                    width: wp(90),
                    marginTop: 20,
                    justifyContent: 'space-between',
                    flexDirection: 'row',
                    alignItems: 'center',
                  }}>
                  <TouchableOpacity
                    style={{
                      width: wp(42.5),
                      alignSelf: 'center',
                      height: 50,
                      alignItems: 'center',
                      justifyContent: 'center',
                      backgroundColor: COLOR.WHITE,
                      borderRadius: 10,
                      marginBottom: 20,
                      borderColor: COLOR.LIGHT_TEXT,
                      borderWidth: 1,
                    }}
                    onPress={() => {
                      this.setState({modalVisible: false}, (val) => {
                        this.props.navigation.goBack();
                      });
                    }}>
                    <Text
                      style={[
                        Styles.button_font,
                        {
                          color: COLOR.BLACK,
                          width: null,
                          textAlign: 'center',
                        },
                      ]}>
                      Cancel
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={{
                      width: wp(42.5),
                      alignSelf: 'center',
                      height: 50,
                      alignItems: 'center',
                      justifyContent: 'center',
                      backgroundColor: COLOR.GREEN,
                      borderRadius: 10,
                      marginBottom: 20,
                      borderColor: COLOR.LIGHT_TEXT,
                      borderWidth: 1,
                    }}
                    onPress={() => this.handleNavigation()}>
                    <Text
                      style={[
                        Styles.button_font,
                        {
                          color: COLOR.WHITE,
                          width: null,
                          textAlign: 'center',
                        },
                      ]}>
                      Continue
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </Modal>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  modal: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
  },
  modalBody: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    backgroundColor: COLOR.WHITE,
    borderWidth: 1,
    width: wp(100),
    marginTop: 50,
    borderColor: COLOR.BORDER,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
  image: {
    height: 120,
    width: 120,
    alignSelf: 'center',
    marginVertical: 20,
  },
  headingLabel: {
    fontFamily: FONTS.FAMILY_BOLD,
    fontSize: FONTS.MEDIUM,
    color: COLOR.BLACK,
  },
  singleLineView: {
    marginVertical: 20,
    borderBottomColor: COLOR.GRAY,
    width: wp(100),
    alignSelf: 'center',
    borderBottomWidth: 1,
  },
  borderView: {
    borderRadius: 10,
    borderWidth: 3,
    width: wp(90),
    borderColor: COLOR.GRAY,
    alignSelf: 'center',
  },
  switchView: {
    position: 'absolute',
    justifyContent: 'flex-end',
    right: 0,
  },
  switchText: {
    fontFamily: FONTS.FAMILY_REGULAR,
    color: COLOR.LIGHT_TEXT_COLOR,
  },
});
