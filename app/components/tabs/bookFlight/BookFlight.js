import {View, Image, Text, SafeAreaView, StyleSheet} from 'react-native';
import React from 'react';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import COLOR from '../../styles/Color';
import Styles from '../../styles/Styles';
import {TouchableOpacity} from 'react-native';
import IMAGES from '../../styles/Images';
import TopTabBar from '../home/trip/TabBarView';
import {T_SCHEDULE_FLIGHT_TAB} from '../../../helper/Constants';
import FONTS from '../../styles/Fonts';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import {connect} from 'react-redux';
import FlightAction from '../../../redux/action/FlightAction';
import TripDateView from '../../commonView/TripDateView';
import OneWayFlight from './OneWayFlight';
import MulticityFlight from './MulticityFlight';

class BookFlight extends React.Component {
  constructor(props) {
    super(props);
    if (this.props.route.params.depart) {
      var data = this.props.route.params;
    }
    this.state = {
      flight_id: this.props.route.params.flight_id,
      data: this.props.route.params.data,
      depart: data ? data.depart : null,
      return: data ? data.return : null,
      departFlexibility: data ? data.departFlexibility : null,
      returnFlexibility: data ? data.returnFlexibility : null,
      package_request_id: data ? data.package_request_id : null,
    };
  }

  componentDidMount() {
    if (this.props.route.params.depart) {
      var data = this.props.route.params;
      this.setState({
        depart: data.depart,
        return: data.return,
        departFlexibility: data.departFlexibility,
        returnFlexibility: data.returnFlexibility,
        package_request_id: data.package_request_id,
      });
    }
    if (this.props.route.params.id) {
      this.props.getScheduledFlights(this.props.route.params.id);
    }
  }

  getViewByFlightById = () => {
    if (this.state.flight_id) {
      let params = this.props.route.params;
      if (params.mode == 1) {
        return (
          <OneWayFlight
            depart={this.state.depart}
            return={this.state.return}
            props={this.props}
            data={this.state.data}
            navigation={this.props.navigation}
            flightId={this.state.flight_id}
            package_request_id={this.state.package_request_id}
            screen={1}
          />
        );
      } else if (params.mode == 2) {
        return (
          <OneWayFlight
            depart={this.state.depart}
            return={this.state.return}
            props={this.props}
            data={this.state.data}
            navigation={this.props.navigation}
            flightId={this.state.flight_id}
            package_request_id={this.state.package_request_id}
            screen={2}
          />
        );
      } else {
        return (
          <MulticityFlight
            depart={this.state.depart}
            return={this.state.return}
            props={this.props}
            data={this.state.data}
            navigation={this.props.navigation}
            flightId={this.state.flight_id}
            package_request_id={this.state.package_request_id}
            screen={3}
          />
        );
      }
    } else {
      return (
        <TopTabBar
          currentTab={T_SCHEDULE_FLIGHT_TAB}
          props={this.props}
          data={this.state.data}
          flightId={this.state.flight_id}
          depart={this.state.depart}
          return={this.state.return}
          package_request_id={this.state.package_request_id}
        />
      );
    }
  };

  render() {
    return (
      <View style={{flex: 1, backgroundColor: COLOR.BACKGROUND}}>
        <View
          style={{
            height: 220,
            backgroundColor: COLOR.GREEN,
            borderBottomRightRadius: 15,
            borderBottomLeftRadius: 15,
          }}>
          <View
            style={[
              Styles.mainView,
              {
                height: 45,
                marginTop: getStatusBarHeight(true),
                justifyContent: 'space-between',
                alignSelf: 'center',
                alignItems: 'center',
                flexDirection: 'row',
                width: wp(90),
              },
            ]}>
            <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
              <Image
                style={{height: 15, width: 15, tintColor: COLOR.WHITE}}
                source={IMAGES.back_arrow}
              />
            </TouchableOpacity>
            <TouchableOpacity>
              <View>
                <Text
                  style={[
                    Styles.heading_label,
                    {
                      textAlign: 'center',
                      color: COLOR.WHITE,
                      fontFamily: FONTS.FAMILY_BOLD,
                      marginVertical: 0,
                      width: null,
                    },
                  ]}>
                  Flight
                </Text>
              </View>
            </TouchableOpacity>
            <Image
              source={IMAGES.notification_bell}
              style={{
                width: 25,
                height: 25,
                tintColor: COLOR.WHITE,
              }}
            />
          </View>
          <TripDateView
            departDate={this.state.depart}
            returnDate={this.state.return}
            departFlexibility={this.state.departFlexibility}
            returnFlexibility={this.state.returnFlexibility}
          />
        </View>
        <SafeAreaView style={styles.safeArea}>
          {this.getViewByFlightById()}
        </SafeAreaView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
    marginTop: -50,
    width: wp(90),
    alignSelf: 'center',
    borderRadius: 10,
  },
  smallLabel: {
    fontFamily: FONTS.FAMILY_REGULAR,
    fontSize: FONTS.SMALL,
    alignSelf: 'center',
    color: COLOR.VOILET,
    textAlign: 'center',
  },
});

mapStateToProps = (state, ownProps) => {
  return {
    scheduledFlights: state.flightReducer.scheduledFlights,
  };
};

mapDispatchToProps = (dispatch) => {
  return {
    getScheduledFlights: (val) => {
      dispatch(FlightAction.getScheduledFlights(val));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(BookFlight);
