import React from 'react';
import {View, Image, StyleSheet, Text, TextInput, Alert} from 'react-native';
import {TouchableOpacity, TouchableWithoutFeedback} from 'react-native';
import Switch from '../../commonView/Switch';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import COLOR from '../../../components/styles/Color';
import IMAGES from '../../../components/styles/Images';
import Styles from '../../../components/styles/Styles';
import FONTS from '../../styles/Fonts';
import SelectDate from '../calendar/SelectDate';
import {ScrollView} from 'react-native';
import GooglePlacesTextInput from '../../commonView/GooglePlacesTextInput';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';

export default class MulticityFlight extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      tripData: this.props.data,
      flightId: this.props.flightId,
      screen: this.props.screen,
      package_request_id: this.props.props.route.params.package_request_id,
      scheduledData: this.props.props.scheduledFlights,
      oneWayFlight: null,
      roundFlight: null,
      multiCity: null,
      oneFrom: '',
      onTo: '',
      sFrom: '',
      sTo: '',
      sDepart: '',
      sReturn: '',
      attrName: '',
      fromNearBy: false,
      toNearBy: false,
      sFromNearBy: false,
      sToNearBy: false,
      minDate: this.props.depart,
      maxDate: this.props.return,
      attrFlexibility: '',
      depart_flexibility: 0,
      s_depart_flexibility: 0,
      sCheckInDate: '',
      checkInDate: this.props.depart,
    };
  }

  componentDidMount() {

    if (this.state.flightId) {
      let data = [...this.props.data];
      this.setState({
        checkInDate: data[0].depart,
        oneFrom: data[0].from,
        onTo: data[0].to,
        depart_flexibility: data[0].depart_flexibility,
        fromNearBy: data[0].from_nearby_airport == 1 ? true : false,
        toNearBy: data[0].to_nearby_airport == 1 ? true : false,
        sCheckInDate: data[1].depart,
        sFrom: data[1].from,
        sTo: data[1].to,
        s_depart_flexibility: data[1].depart_flexibility,
        sFromNearBy: data[0].from_nearby_airport == 1 ? true : false,
        sToNearBy: data[0].to_nearby_airport == 1 ? true : false,
      });
    }
  }

  componentDidUpdate(prevProps) {}

  handleScreens = () => {
    let params = this.props.props.route.params;
    const screens = params.screens;
    const index = params.index;
    if (screens[index + 1]) {
      this.props.navigation.navigate(screens[index + 1], {
        screens: screens,
        index: index + 1,
        id: params.id,
        depart: params.depart,
        return: params.return,
      });
    } else if (this.state.isPackage) {
      this.props.navigation.goBack();
    } else {
      this.props.navigation.navigate('HomeStack');
    }
  };

  handleNavigation = () => {
    if (this.state.oneFrom == '') {
      SimpleToast.show('Enter from location');
    } else if (this.state.onTo == '') {
      SimpleToast.show('Enter to location');
    } else if (this.state.minDate && this.state.checkInDate == null) {
      SimpleToast.show('Select Checkin date');
    }
    if (this.state.sFrom == '') {
      SimpleToast.show('Enter from location');
    } else if (this.state.sTo == '') {
      SimpleToast.show('Enter to location');
    } else if (this.state.minDate && this.state.sCheckInDate == null) {
      SimpleToast.show('Select Checkin date');
    } else {
      this.setState({modalVisible: false});
      let params = {
        from: this.state.oneFrom,
        to: this.state.onTo,
        depart: this.state.checkInDate,
        depart_flexibility: this.state.depart_flexibility,
        from_nearby_airport: this.state.fromNearBy ? 1 : 0,
        to_nearby_airport: this.state.toNearBy ? 1 : 0,
      };
      let params2 = {
        from: this.state.sFrom,
        to: this.state.sTo,
        depart: this.state.sCheckInDate,
        depart_flexibility: this.state.s_depart_flexibility,
        from_nearby_airport: this.state.sFromNearBy ? 1 : 0,
        to_nearby_airport: this.state.sToNearBy ? 1 : 0,
      };
      let paramsScreen = this.props.props.route.params;
      const screens = paramsScreen.screens;
      const index = paramsScreen.index;
      this.props.navigation.replace('FlightOption', {
        params: [params, params2],
        mode: this.state.screen,
        id: this.props.props.route.params.id,
        flightId: this.state.flightId ? this.state.flightId : null,
        package_request_id: this.state.package_request_id,
        screens: screens,
        index: index,
        depart: paramsScreen.depart,
        return: paramsScreen.return,
      });
    }
  };

  updateMasterState = (attrName, value) => {
    this.setState({[attrName]: value}, () => {});
  };

  openCalendar = (attrName, attrFlexibility) => {
    this.setState({
      attrFlexibility: attrFlexibility,
      attrName: attrName,
      openCalendar: true,
    });
  };

  render() {
    const id = this.props.props.route.params.id;
    return (
      <KeyboardAwareScrollView
      bounces={false}

showsHorizontalScrollIndicator={false}
showsVerticalScrollIndicator={false}
keyboardShouldPersistTaps="always"
        extraScrollHeight={180}
        style={[Styles.container, {backgroundColor: COLOR.BACKGROUND}]}>
        <View style={{zIndex: 1500}}>
          <View style={{zIndex: 3000}}>
            <View style={[styles.flightBackground, {zIndex: 2005}]}>
              <Text style={styles.flightTitle}>FLIGHT 1</Text>
              <View style={[styles.fieldView, {zIndex: 2000}]}>
                <View>
                  <Image
                    source={IMAGES.depart}
                    style={styles.icon}
                    resizeMode={'contain'}
                  />
                </View>
                <View style={styles.flexView}>
                  <Text style={styles.label}>From</Text>
                  <GooglePlacesTextInput
                    value={this.state.oneFrom}
                    onValueChange={(text) => this.setState({oneFrom: text})}
                  />
                </View>
                <View style={styles.nearByView}>
                  <Text style={styles.label}>Add nearby airports</Text>
                  <Switch
                    isChecked={this.state.fromNearBy}
                    onCheckedListener={() =>
                      this.setState({
                        fromNearBy: !this.state.fromNearBy,
                      })
                    }
                  />
                </View>
              </View>
              <View style={styles.hr} />
              <View style={[styles.fieldView, {zIndex: 1900}]}>
                <View>
                  <Image
                    source={IMAGES.arrive}
                    style={styles.icon}
                    resizeMode={'contain'}
                  />
                </View>
                <View style={styles.flexView}>
                  <Text style={styles.label}>To</Text>

                  <GooglePlacesTextInput
                    value={this.state.onTo}
                    onValueChange={(text) => this.setState({onTo: text})}
                  />
                </View>
                <View style={styles.nearByView}>
                  <Text style={styles.label}>Add nearby airports</Text>
                  <Switch
                    isChecked={this.state.toNearBy}
                    onCheckedListener={() =>
                      this.setState({
                        toNearBy: !this.state.toNearBy,
                      })
                    }
                  />
                </View>
              </View>
              {this.state.minDate ? (
                <View>
                  <View style={styles.hr} />
                  <View style={styles.fieldView}>
                    <View>
                      <Image
                        source={IMAGES.return_date}
                        style={styles.icon}
                        resizeMode={'contain'}
                      />
                    </View>
                    <View style={styles.flexView}>
                      <Text style={styles.label}>Departure Date</Text>
                      <TouchableOpacity
                        onPress={() =>
                          this.openCalendar('checkInDate', 'depart_flexibility')
                        }>
                        <Text
                          style={[
                            styles.textInput,
                            {marginBottom: 15, marginTop: 5},
                          ]}>
                          {this.state.checkInDate
                            ? this.state.checkInDate
                            : 'Select Depart'}
                        </Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                </View>
              ) : null}
            </View>
          </View>
          <View style={{zIndex: 1500}}>
            <View
              style={[
                styles.flightBackground,
                {
                  marginVertical: 10,
                  borderRadius: 10,
                },
              ]}>
              <Text style={styles.flightTitle}>FLIGHT 2</Text>
              <View style={[styles.fieldView, {zIndex: 1500}]}>
                <View>
                  <Image
                    source={IMAGES.depart}
                    style={styles.icon}
                    resizeMode={'contain'}
                  />
                </View>
                <View style={styles.flexView}>
                  <Text style={styles.label}>From</Text>
                  <GooglePlacesTextInput
                    value={this.state.sFrom}
                    onValueChange={(text) => this.setState({sFrom: text})}
                  />
                </View>
                <View style={[styles.flexView, {alignItems: 'flex-end'}]}>
                  <Text style={styles.label}>Add nearby airports</Text>
                  <Switch
                    isChecked={this.state.sFromNearBy}
                    onCheckedListener={() =>
                      this.setState({
                        sFromNearBy: !this.state.sFromNearBy,
                      })
                    }
                  />
                </View>
              </View>
              <View style={styles.hr} />
              <View style={[styles.fieldView, {zIndex: 1200}]}>
                <View>
                  <Image
                    source={IMAGES.arrive}
                    style={styles.icon}
                    resizeMode={'contain'}
                  />
                </View>
                <View style={styles.flexView}>
                  <Text style={styles.label}>To</Text>
                  <GooglePlacesTextInput
                    value={this.state.sTo}
                    onValueChange={(text) => this.setState({sTo: text})}
                  />
                </View>
                <View style={[styles.flexView, {alignItems: 'flex-end'}]}>
                  <Text style={styles.label}>Add nearby airports</Text>
                  <Switch
                    isChecked={this.state.sToNearBy}
                    onCheckedListener={() =>
                      this.setState({
                        sToNearBy: !this.state.sToNearBy,
                      })
                    }
                  />
                </View>
              </View>
              {this.state.minDate ? (
                <View>
                  <View style={styles.hr} />
                  <View style={[styles.fieldView, {marginVertical: 10}]}>
                    <View>
                      <Image
                        source={IMAGES.return_date}
                        style={styles.icon}
                        resizeMode={'contain'}
                      />
                    </View>
                    <View style={styles.flexView}>
                      <Text style={styles.label}>Departure Date</Text>
                      <TouchableOpacity
                        onPress={() =>
                          this.openCalendar(
                            'sCheckInDate',
                            's_depart_flexibility',
                          )
                        }>
                        <Text
                          style={[
                            styles.textInput,
                            {marginBottom: 15, marginTop: 5},
                          ]}>
                          {this.state.sCheckInDate
                            ? this.state.sCheckInDate
                            : 'Select Depart Date'}
                        </Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                </View>
              ) : null}
            </View>
          </View>
        </View>

        <View>
          <TouchableOpacity
            style={styles.nextButton}
            onPress={() => this.handleNavigation()}>
            <Text
              style={[
                Styles.button_font,
                {
                  color: COLOR.WHITE,
                  width: null,
                  textAlign: 'center',
                },
              ]}>
              Next
            </Text>
          </TouchableOpacity>
        </View>
        {this.state.openCalendar ? (
          <SelectDate
            allowRangeSelection={false}
            attrName={this.state.attrName}
            selector={0}
            attrFlexibility={this.state.attrFlexibility}
            updateMasterState={this.updateMasterState}
            props={this.props.props}
            modal={true}
            minDate={this.state.minDate}
            maxDate={this.state.maxDate}
          />
        ) : null}
      </KeyboardAwareScrollView>
    );
  }
}

const styles = StyleSheet.create({
  topBar: {
    height: 50,
    shadowColor: COLOR.VOILET,
    shadowOpacity: 0.2,
    elevation: 3,
    backgroundColor: COLOR.WHITE,
    flexDirection: 'row',
    shadowOffset: {width: 2, height: 2},
    justifyContent: 'space-between',
    width: wp(95),
    alignSelf: 'center',
    marginVertical: 20,

    alignItems: 'center',
  },
  nextButton: {
    width: wp(90),
    alignSelf: 'center',
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: COLOR.GREEN,
    borderRadius: 10,
    marginVertical: 10,
  },
  plusButton: {
    width: 25,
    height: 25,
    borderRadius: 10,
    marginRight: 5,
  },
  addButton: {
    width: wp(90),
    alignSelf: 'center',
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: COLOR.WHITE,
    borderRadius: 10,
    borderColor: COLOR.LIGHT_TEXT,
    borderWidth: 1,
    marginTop: 40,
  },
  viewDetailsButton: {
    width: wp(90),
    alignSelf: 'center',
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: COLOR.GREEN,
    borderRadius: 10,
    marginVertical: 10,
  },
  flightBackground: {
    alignSelf: 'center',
    shadowColor: COLOR.VOILET,
    elevation: 3,
    shadowOpacity: 0.2,
    justifyContent: 'space-between',
    paddingBottom: 5,
    shadowOffset: {width: 2, height: 2},
    backgroundColor: COLOR.WHITE,
    width: wp(90),
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
  },
  fieldView: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: 15,
    marginTop: 10,
  },
  icon: {
    height: 25,
    width: 25,
    marginRight: 15,
  },
  textInput: {
    fontSize: FONTS.MEDIUM,
    fontFamily: FONTS.FAMILY_SEMIBOLD,
  },
  label: {
    fontSize: FONTS.SMALL,
    fontFamily: FONTS.FAMILY_REGULAR,
    color: COLOR.VOILET,
  },
  nearByView: {
    flex: 1,
    alignItems: 'flex-end',
    alignSelf: 'flex-start',
  },
  flexView: {
    flex: 1,
  },
  hr: {
    alignSelf: 'center',
    width: wp(75),
    height: 1,
    marginTop: 5,
    backgroundColor: COLOR.GRAY,
  },
  flightTitle: {
    fontSize: 14,
    alignSelf: 'flex-start',
    marginVertical: 5,
    marginHorizontal: 10,
    fontFamily: FONTS.FAMILY_BOLD,
  },
});
