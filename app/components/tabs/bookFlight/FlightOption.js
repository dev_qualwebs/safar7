import React from 'react';
import {
  TouchableOpacity,
  TouchableWithoutFeedback,
  TextInput,
  View,
  Image,
  StyleSheet,
  Text,
  SafeAreaView,
  Alert,
} from 'react-native';
import {widthPercentageToDP, widthPercentageToDP as wp} from 'react-native-responsive-screen';
import COLOR from '../../../components/styles/Color';
import FONTS from '../../../components/styles/Fonts';
import IMAGES from '../../../components/styles/Images';
import Styles from '../../../components/styles/Styles';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import FlightAction from '../../../redux/action/FlightAction';
import {connect} from 'react-redux';
import Switch from '../../commonView/Switch';
import {MultiSelectView} from '../../commonView/MultiSelectView';
import SimpleToast from 'react-native-simple-toast';
import { DoneButtonKeyboard } from '../../commonView/CommonView';

class FlightOption extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      flightParam: null,
      modalVisible: false,
      aeroplaneData: [],
      flightTimeData: [],
      equipmentData: [],
      selectionData: [],
      seatingData: [],
      policyData: [],
      classData: [],
      modeData: [],
      selectedItem: [],
      preferedClass: [],
      reservationPolicy: [],
      airlines: [],
      seatingPreference: [],
      equipment: [],
      departureTime: [],
      title: null,
      handBaggage: 0,
      checkInBaggage: 0,
      stopIndex: 0,
      petAllowed: false,
      comment: '',
      selectedPref: [],
      id: this.props.route.params.id,
      flightId: this.props.route.params.flightId,
      singleFlightData: null,
      seating_together: false,
      refreshData: true,
    };

    this.handleSelection = this.handleSelection.bind(this);
    this.handleModalVisibility = this.handleModalVisibility.bind(this);
    this.handlePreferenceSelection = this.handlePreferenceSelection.bind(this);
  }

  componentDidMount() {
    this.props.getAirlinesData();
    this.props.getFlightTimeData();
    this.props.getFlightEquipmentData();
    this.props.getFlightSeatingData();
    this.props.getFlightPolicyData();
    this.props.getFlightModes();
    this.props.getFlightClasses();

    console.log(this.props.route.params.flightId);
    if (this.props.route.params.flightId != null) {
      this.props.getSingleFlight(this.props.route.params.flightId);
    }
    if (this.props.route.params.params) {
      console.log('Passed data', this.props.route.params.params);
      this.setState({
        flightParam: this.props.route.params.params,
      });
    }
    if (this.props.airlineData.length) {
      this.setState({
        aeroplaneData: this.props.airlineData,
      });
    }

    if (this.props.flightTimeData.length) {
      this.setState({
        flightTimeData: this.props.flightTimeData,
      });
    }

    if (this.props.flightEquipmentData.length) {
      this.setState({
        equipmentData: this.props.flightEquipmentData,
      });
    }

    if (this.props.flightSeatingData.length) {
      this.setState({
        seatingData: this.props.flightSeatingData,
      });
    }

    if (this.props.flightPolicyData.length) {
      this.setState({
        policyData: this.props.flightPolicyData,
      });
    }

    if (this.props.flightModeData.length) {
      this.setState({
        modeData: this.props.flightModeData,
      });
    }

    if (this.props.flightClassData.length) {
      this.setState({
        classData: this.props.flightClassData,
      });
    }
  }

  setRefresh() {
    this.setState({refreshData: false});
  }

  componentDidUpdate(prevProps) {
    if (
      prevProps.singleFlightData !== this.props.singleFlightData &&
      this.props.route.params.flightId &&
      this.state.refreshData
    ) {
      let flightPrefference = this.props.singleFlightData.flight_preference;
      this.setState(
        {
          singleFlightData: this.props.singleFlightData,
          preferedClass: flightPrefference.preferred_class
            ? flightPrefference.preferred_class.map((value) => value.id)
            : [],
          reservationPolicy: flightPrefference.reservation_policy.map(
            (value) => value.id,
          ),
          airlines: flightPrefference.airlines.map((value) => value.id),
          seatingPreference: flightPrefference.seating_preference.map(
            (value) => value.id,
          ),
          equipment: flightPrefference.equipments.map((value) => value.id),
          departureTime: flightPrefference.departure_times.map(
            (value) => value.id,
          ),
          handBaggage: flightPrefference.baggage.hand_luggage,
          checkInBaggage: flightPrefference.baggage.check_in_luggage,
          stopIndex: flightPrefference.stop.id,
          petAllowed: flightPrefference.pat_allowance,
          seating_together:
            flightPrefference.seating_together == 1 ? true : false,
          comment: flightPrefference.comment,
        },
        () => {
          console.log(this.state.reservationPolicy);
        },
      );
    }

    if (
      prevProps.airlineData != this.props.airlineData &&
      this.state.refreshData
    ) {
      this.setState({
        aeroplaneData: this.props.airlineData,
      });
    }

    if (
      prevProps.flightTimeData != this.props.flightTimeData &&
      this.state.refreshData
    ) {
      this.setState({
        flightTimeData: this.props.flightTimeData,
      });
    }

    if (
      prevProps.flightEquipmentData != this.props.flightEquipmentData &&
      this.state.refreshData
    ) {
      this.setState({
        equipmentData: this.props.flightEquipmentData,
      });
    }

    if (
      prevProps.flightSeatingData != this.props.flightSeatingData &&
      this.state.refreshData
    ) {
      this.setState({
        seatingData: this.props.flightSeatingData,
      });
    }

    if (
      prevProps.flightPolicyData != this.props.flightPolicyData &&
      this.state.refreshData
    ) {
      this.setState({
        policyData: this.props.flightPolicyData,
      });
    }

    if (
      prevProps.flightModeData != this.props.flightModeData &&
      this.state.refreshData
    ) {
      this.setState({
        modeData: this.props.flightModeData,
      });
    }

    if (
      prevProps.flightClassData != this.props.flightClassData &&
      this.state.refreshData
    ) {
      this.setState({
        classData: this.props.flightClassData,
      });
    }
  }

  componentWillUnmount() {
    this.props.clearSingleFlight();
  }

  handleSelection = (val) => {
    this.setState({
      selectedItem: val,
    });
    switch (this.state.selectedPref) {
      case 1: {
        this.setState({preferedClass: val});
        console.log('prefered class', this.state.preferedClass);

        break;
      }
      case 2: {
        this.setState({reservationPolicy: val});
        console.log('reservation policy', this.state.reservationPolicy);
        break;
      }
      case 3: {
        this.setState({airlines: val});
        console.log('Airlines', this.state.airlines);
        break;
      }
      case 4: {
        this.setState({seatingPreference: val});
        console.log('seatingPreference', this.state.seatingPreference);
        break;
      }
      case 5: {
        this.setState({equipment: val});
        console.log('equipment', this.state.equipment);
        break;
      }
      case 6: {
        this.setState({departureTime: val});
        console.log('departureTime', this.state.departureTime);
        break;
      }
    }
  };

  handlePreferenceSelection = (pref) => {
    if (pref == 1) {
      this.setState({
        selectedItem: this.state.preferedClass,
        title: 'Prefered Class',
        modalVisible: true,
        selectionData: this.state.classData,
        selectedPref: 1,
      });
    } else if (pref == 2) {
      this.setState({
        selectedItem: this.state.reservationPolicy,
        title: 'Reservation Policy',
        modalVisible: true,
        selectionData: this.state.policyData,
        selectedPref: 2,
      });
    } else if (pref == 3) {
      this.setState({
        selectedPref: 3,
      });
      this.props.navigation.navigate('Airlines', {
        handleSelection: this.handleSelection,
        selectedItem: this.state.airlines,
      });
    } else if (pref == 4) {
      this.setState({
        selectedItem: this.state.seatingPreference,
        title: 'Seating Preferences',
        modalVisible: true,
        selectionData: this.state.seatingData,
        selectedPref: 4,
      });
    } else if (pref == 5) {
      this.setState({
        selectedItem: this.state.equipment,
        title: 'Equipment',
        modalVisible: true,
        selectionData: this.state.equipmentData,
        selectedPref: 5,
      });
    } else if (pref == 6) {
      console.log(this.state.flightTimeData);
      this.setState({
        selectedItem: this.state.departureTime,
        title: 'Departure time',
        modalVisible: true,
        selectionData: this.state.flightTimeData,
        selectedPref: 6,
      });
    }
  };

  handleModalVisibility = () => {
    this.setState({
      modalVisible: false,
      selectedItem: [],
    });
  };

  selectionView = (prop) => {
    return (
      <TouchableWithoutFeedback
        style={{height: 130}}
        onPress={() => this.handlePreferenceSelection(prop.id)}>
        <View
          style={{
            flex: 1,
            borderRadius: 10,
            borderWidth: 1,
            height: 130,
            width: wp(28),
            margin: 5,
            borderColor: prop.hasData ? '#05778F' : COLOR.GRAY,
            alignSelf: 'center',
            alignItems: 'center',
            justifyContent: 'center',
            padding: 10,
            backgroundColor: prop.hasData ? COLOR.GREEN_OPACITY : COLOR.WHITE
          }}>
          <View
            style={{
              height: 60,
              width: 60,
              marginVertical: 10,
              backgroundColor: prop.hasData ? COLOR.WHITE : prop.bg,
              borderRadius: 5,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Image
              resizeMode={'contain'}
              source={prop.image}
              style={{
                height: 30,
                width: 30,
              }}
            />
          </View>
          <Text style={{marginBottom: 10, textAlign: 'center'}}>
            {prop.name}
          </Text>
        </View>
      </TouchableWithoutFeedback>
    );
  };

  callScheduleFlight() {
    let data = JSON.stringify({
      tripId: this.state.id,
      flight_mode_id: this.props.route.params.mode,
      flight: this.state.flightParam,
      pat_allowance: this.state.petAllowed ? 1 : 0,
      hand_luggage: this.state.handBaggage,
      check_in_luggage: this.state.checkInBaggage,
      flight_stops_id: this.state.stopIndex,
      class: this.state.preferedClass,
      reservation_policy: this.state.reservationPolicy,
      airlines: this.state.airlines,
      seating_preference: this.state.seatingPreference,
      seating_together: this.state.seating_together ? 1 : 0,
      equipments: this.state.equipment,
      departure_times: this.state.departureTime,
      comment: this.state.comment,
      package_request_id: this.props.route.params.package_request_id,
    });
    if (this.state.flightId) {
      this.props.updateScheduledFlight(data, this.state.flightId);
    } else {
      this.props.scheduleFlight(data, this.state.id);
    }
  }

  handleNavigation = () => {
    let params = this.props.route.params;
    const screens = params.screens;
    const index = params.index;
    if (screens && screens[index + 1]) {
      this.props.navigation.replace(screens[index + 1], {
        screens: screens,
        index: index + 1,
        id: params.id,
        depart: params.depart,
        return: params.return,
      });
    } else {
      this.props.navigation.replace('HomeStack');
    }
  };

  validateData = () => {
    if (this.state.preferedClass.length == 0) {
      SimpleToast.show('Please Select Prefered class');
    } else if (this.state.reservationPolicy.length == 0) {
      SimpleToast.show('Please select reservation policy');
    } else if (this.state.airlines.length == 0) {
      SimpleToast.show('Please select Airlines');
    } else if (this.state.seatingPreference.length == 0) {
      SimpleToast.show('Please select seating preferences');
    } else if (this.state.equipment.length == 0) {
      SimpleToast.show('Please Select Equipment');
    } else if (this.state.departureTime.length == 0) {
      SimpleToast.show('Please Select Departure time');
    } else {
      this.handleNavigation();
      this.callScheduleFlight();
    }
  };

  handlePlusMinus = (id, val) => {
    if (id == 1) {
      if (val == 1 && this.state.handBaggage > 0) {
        this.setState({
          handBaggage: this.state.handBaggage - 1,
        });
      } else if (val == 2 && this.state.handBaggage < 7) {
        this.setState({
          handBaggage: this.state.handBaggage + 1,
        });
      }
    } else if (id == 2) {
      if (val == 1 && this.state.checkInBaggage > 0) {
        this.setState({
          checkInBaggage: this.state.checkInBaggage - 1,
        });
      } else if (val == 2 && this.state.checkInBaggage < 23) {
        this.setState({
          checkInBaggage: this.state.checkInBaggage + 1,
        });
      }
    }
  };

  IncrementView = (props) => (
    <View
      style={{
        flexDirection: 'row',
        width: wp(90),

        justifyContent: 'space-between',
      }}>
      <View
        style={{flex: 1, alignSelf: 'center', justifyContent: 'flex-start'}}>
        <Text
          style={[
            Styles.body_label,
            {width: null, color: COLOR.BLACK, alignSelf: 'flex-start'},
          ]}>
          {props.name}
        </Text>
      </View>
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'flex-end',
        }}>
        <TouchableOpacity
          onPress={() => this.handlePlusMinus(props.id, 1)}
          style={{
            height: 25,
            width: 25,
            borderRadius: 15,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Image
            style={{height: 30, width: 30}}
            resizeMode={'contain'}
            source={IMAGES.minus_image}
          />
        </TouchableOpacity>
        <Text
          style={[
            Styles.button_font,
            {width: 40, color: COLOR.BLACK, textAlign: 'center'},
          ]}>
          {props.value}
        </Text>
        <TouchableOpacity
          onPress={() => this.handlePlusMinus(props.id, 2)}
          style={{
            height: 30,
            width: 30,
            borderWidth: 1,
            borderRadius: 15,
            borderColor: COLOR.GREEN,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Image style={{height: 20, width: 20}} source={IMAGES.plus_image} />
        </TouchableOpacity>
      </View>
    </View>
  );

  transitView = (props) => {
    return (
      <TouchableOpacity
        style={{marginRight: 15}}
        onPress={() => {
          this.setState({
            stopIndex: props.id,
          });
        }}>
        <Text
          style={{
            paddingHorizontal: 20,
            paddingVertical: 5,
            flex: 1,
            backgroundColor:
              this.state.stopIndex == props.id
                ? COLOR.GREEN
                : COLOR.GREEN_OPACITY,
            color: this.state.stopIndex == props.id ? COLOR.WHITE : COLOR.GREEN,
            textAlign: 'center',
            fontFamily: FONTS.FAMILY_SEMIBOLD,
            borderRadius: 5,
            overflow: 'hidden',
          }}>
          {props.name}
        </Text>
      </TouchableOpacity>
    );
  };

  render() {
    return (
      <SafeAreaView style={Styles.container}>
        {/* <View style={[Styles.container]}> */}
        <KeyboardAwareScrollView
        showsHorizontalScrollIndicator={false}
        showsVerticalScrollIndicator={false}
        bounces={false}
          style={{flex: 1}}
          keyboardShouldPersistTaps={'handled'}
          enableResetScrollToCoords={false}
          scrollToOverflowEnabled={true}
          >
              <MultiSelectView
          title={this.state.title}
          showBottomNote={false}
          showImage={false}
          handleSelection={this.handleSelection}
          selectedItem={this.state.selectedItem}
          data={this.state.selectionData}
          props={this.props}
          setSeatTogether={(val) => this.setState({seating_together: val})}
          seatTogether={this.state.seating_together}
          handleModalVisibility={() => this.handleModalVisibility()}
          modalVisible={this.state.modalVisible}
        />
            <View>
              <View style={style.topBar}>
                <TouchableOpacity
                  onPress={() => this.props.navigation.goBack()}>
                  <Image
                    style={{height: 15, width: 15}}
                    source={IMAGES.back_arrow}
                  />
                </TouchableOpacity>
                <Text
                  style={[
                    {width: null, fontSize: 20, fontFamily: FONTS.FAMILY_BOLD},
                  ]}>
                  Flight Options
                </Text>
                <TouchableOpacity>
                  <Image
                    style={{height: 20, width: 20}}
                    source={IMAGES.filter_image}
                  />
                </TouchableOpacity>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  width: wp(90),
                  alignSelf: 'center',
                  marginTop: 20,
                  justifyContent: 'space-between',
                }}>
                <this.selectionView
                  id={1}
                  name={'Prefered\nClass'}
                  image={IMAGES.seat}
                  bg={'#CF212B20'}
                  hasData={this.state.preferedClass.length > 0 ? true : false}
                />
                <this.selectionView
                  id={2}
                  name={'Reservation\nPoilcy'}
                  image={IMAGES.shield}
                  bg={'#05778F20'}
                  hasData={this.state.reservationPolicy.length > 0 ? true : false}
                />
                <this.selectionView
                  id={3}
                  name={'Airlines'}
                  image={IMAGES.plane_line}
                  bg={'#02599520'}
                  hasData={this.state.airlines.length > 0 ? true : false}
                />
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  width: wp(90),
                  alignSelf: 'center',
                  marginBottom: 20,
                  justifyContent: 'space-between',
                }}>
                <this.selectionView
                  id={4}
                  name={'Seating\nPreferences'}
                  image={IMAGES.empty_seat}
                  bg={'#02599520'}
                  hasData={this.state.seatingPreference.length > 0 ? true : false}
                />
                <this.selectionView
                  id={5}
                  name={'Equipment'}
                  image={IMAGES.basket}
                  bg={'#CF212B20'}
                  hasData={this.state.equipment.length > 0 ? true : false}
                />
                <this.selectionView
                  id={6}
                  name={'Departure\nTime'}
                  image={IMAGES.plane_red}
                  bg={'#CF212B20'}
                  hasData={this.state.departureTime.length > 0 ? true : false}
                />
              </View>
            </View>
            <View style={Styles.line_view} />
            <Text
              style={[
                Styles.subheading_label,
                {marginBottom: 10,marginTop:10, fontSize: FONTS.REGULAR},
              ]}>
              Baggage
            </Text>
            <View style={{width:widthPercentageToDP(90),alignSelf:'center',height:1,marginBottom:10,backgroundColor:COLOR.GRAY}}/>
            <View
              style={{width: wp(90), alignSelf: 'center', marginBottom: 20}}>
              <this.IncrementView
                id={1}
                value={this.state.handBaggage}
                name={'Hand-baggage (7 Kg)'}
              />
              <View style={[Styles.line_view, {marginVertical: 20,width: wp(90),}]} />
              <this.IncrementView
                id={2}
                value={this.state.checkInBaggage}
                name={'Check-in baggage (20-23 Kg)'}
              />
            </View>
            <View style={Styles.line_view} />
            <Text
              style={[
                Styles.subheading_label,
                {marginVertical: 20, fontSize: FONTS.REGULAR},
              ]}>
              Stops
            </Text>
            <View
              style={{
                flexDirection: 'row',
                width: wp(90),
                alignSelf: 'center',
                justifyContent: 'center',
                alignItems: 'center',
                borderRadius: 10,
              }}>
              <this.transitView id={1} name={'Direct'} />
              <this.transitView id={2} name={'1 Transit'} />
              <this.transitView id={3} name={'2 Transits'} />
            </View>
            <View style={[Styles.line_view, {marginVertical: 20}]} />
            <View
              style={{
                flexDirection: 'row',
                width: wp(90),
                alignSelf: 'center',
                justifyContent: 'center',
                alignItems: 'center',
                borderRadius: 10,
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  flex: 1,
                  alignItems: 'center',
                }}>
                <Text
                  style={[
                    Styles.subheading_label,
                    {width: null, marginRight: 5, fontSize: FONTS.REGULAR},
                  ]}>
                  Pet Allowance
                </Text>
                <TouchableOpacity
                  onPress={() => {
                    this.props.navigation.navigate('PolicyPage', {
                      screenTitle: 'Pet Allowance',
                    });
                  }}>
                  <Image
                    style={{height: 15, width: 15}}
                    source={IMAGES.attention}
                  />
                </TouchableOpacity>
              </View>
              <Switch
                isChecked={this.state.petAllowed == 1 ? true : false}
                onCheckedListener={() =>
                  this.setState({petAllowed: !this.state.petAllowed})
                }
              />
            </View>
            <View style={[Styles.line_view, {marginVertical: 20}]} />
            <Text style={[Styles.subheading_label, {fontSize: FONTS.REGULAR}]}>
              Comments
            </Text>

            <TextInput
            inputAccessoryViewID={'uniqueID'}
              placeholder={'Write your comment'}
              placeholderTextColor={COLOR.LIGHT_TEXT}
              style={[
                {
                  height: 120,
                  padding: 20,
                  borderRadius: 10,
                  borderWidth: 1,
                  width: wp(90),
                  alignSelf: 'center',
                  borderColor: COLOR.GRAY,
                  marginTop: 10,
                },
              ]}
              multiline={true}
              value={this.state.comment}
              onChangeText={(text) => this.setState({comment: text})}
            />
<DoneButtonKeyboard />
            <TouchableOpacity
              style={{
                width: wp(90),
                alignSelf: 'center',
                height: 50,
                alignItems: 'center',
                justifyContent: 'center',
                marginTop: 20,
                borderRadius: 10,
              }}
              onPress={() => {
                // this.setState({modalVisible: false}, () => {
                //   this.props.navigation.navigate('DepartureTime');
                // });
                this.setState({refreshData: false});
                this.setState({title: null});
                this.setState({handBaggage: 0});
                this.setState({checkInBaggage: 0});
                this.setState({stopIndex: 0});
                this.setState({petAllowed: false});
                this.setState({comment: ''});
                this.setState({selectedPref: []});
                this.setState({singleFlightData: null});
                this.setState({seating_together: false});
                this.setState({departureTime: []});
                this.setState({equipment: []});
                this.setState({seatingPreference: []});
                this.setState({airlines: []});
                this.setState({reservationPolicy: []});
                this.setState({preferedClass: []});
                this.setState({selectedItem: []});
                this.setState({selectionData: []});
                this.setState({modalVisible: false});
              }}>
              <Text
                style={[
                  Styles.small_label,
                  {
                    color: COLOR.LIGHT_TEXT,
                    width: null,
                    textAlign: 'right',
                  },
                ]}>
                Clear all options
              </Text>
            </TouchableOpacity>
            {this.state.flightParam ? (
              <TouchableOpacity
                style={{
                  width: wp(90),
                  alignSelf: 'center',
                  height: 50,
                  alignItems: 'center',
                  justifyContent: 'center',
                  backgroundColor: COLOR.GREEN,
                  borderRadius: 10,
                  marginTop: 0,
                }}
                onPress={() => {
                  this.validateData();
                }}>
                <Text
                  style={[
                    Styles.button_font,
                    {
                      color: COLOR.WHITE,
                      width: null,
                      textAlign: 'center',
                    },
                  ]}>
                  Apply
                </Text>
              </TouchableOpacity>
            ) : null}
            <TouchableOpacity
              style={{
                width: wp(90),
                alignSelf: 'center',
                height: 50,
                alignItems: 'center',
                justifyContent: 'center',
                marginBottom: 0,
                borderRadius: 10,
              }}
              onPress={() => {
                this.setState({modalVisible: false}, (val) => {
                  
                });
              }}>
              <Text
                style={[
                  Styles.small_label,
                  {
                    color: COLOR.BLACK,
                    width: null,
                    textAlign: 'right',
                  },
                ]}>
                Skip flight options
              </Text>
            </TouchableOpacity>
          </KeyboardAwareScrollView>
        {/* </View> */}
      </SafeAreaView>
    );
  }
}

const style = StyleSheet.create({
  topBar: {
    height: 50,
    // backgroundColor: '#f4f4f4',
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: wp(88),
    alignSelf: 'center',
    marginVertical: 0,
    alignItems: 'center',
  },
});

const mapStateToProps = (state, ownProps) => {
  return {
    airlineData: state.flightReducer.flightAirlineData,
    flightTimeData: state.flightReducer.flightTimeData,
    flightEquipmentData: state.flightReducer.flightEquipmentData,
    flightSeatingData: state.flightReducer.flightSeatingData,
    flightPolicyData: state.flightReducer.flightPolicyData,
    flightClassData: state.flightReducer.flightClassData,
    flightModeData: state.flightReducer.flightModeData,
    singleFlightData: state.flightReducer.singleFlightData,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getSingleFlight: (id) => {
      dispatch(FlightAction.getSingleFlight(id));
    },
    getAirlinesData: (val) => {
      dispatch(FlightAction.getAirlinesData(val));
    },
    getFlightTimeData: (val) => {
      dispatch(FlightAction.getFlightTimes(val));
    },
    getFlightEquipmentData: (val) => {
      dispatch(FlightAction.getEquipmentData(val));
    },
    getFlightSeatingData: (val) => {
      dispatch(FlightAction.getFlightSeatings(val));
    },
    getFlightPolicyData: (val) => {
      dispatch(FlightAction.getFlightPolicies(val));
    },
    getFlightClasses: (val) => {
      dispatch(FlightAction.getFlightClasses(val));
    },
    getFlightModes: (val) => {
      dispatch(FlightAction.getFlightModes(val));
    },
    scheduleFlight: (data, id) => {
      dispatch(FlightAction.scheduleFlight(data, id));
    },
    updateScheduledFlight: (data, id) => {
      dispatch(FlightAction.updateScheduledFlight(data, id));
    },
    clearSingleFlight: () => {
      dispatch(FlightAction.clearSingleFlight());
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(FlightOption);
