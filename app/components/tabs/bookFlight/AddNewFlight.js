import React from 'react';
import {Alert, ToastAndroid} from 'react-native';
import {Modal, Text, View, StyleSheet, Image, Button} from 'react-native';
import {TextInput, TouchableOpacity} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import SimpleToast from 'react-native-simple-toast';
import COLOR from '../../../components/styles/Color';
import FONTS from '../../../components/styles/Fonts';
import IMAGES from '../../../components/styles/Images';
import Styles from '../../../components/styles/Styles';
import { DoneButtonKeyboard } from '../../commonView/CommonView';
import {
  getEndDateTimestamp,
  getStartDateTimestamp,
} from '../../commonView/Helpers';
import NavigationBar from '../../commonView/NavigationBar';
import Switch from '../../commonView/Switch';
import SelectDate from '../calendar/SelectDate';

export default class AddNewFlight extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: true,
      openCalendar: false,
      from: '',
      to: '',
      checkInDate: null,
      checkOutDate: null,
      attrName: null,
      selector: 0,
      flight_mode_id: this.props.route.params.mode,
      id: this.props.route.params.id,
      from_nearby_airport: false,
      to_nearby_airport: false,
      fromFlexibility: 0,
      toFlexibility: 0,
      minDate: getStartDateTimestamp(this.props.route.params.depart),
      maxDate: getEndDateTimestamp(this.props.route.params.return),
      tripDepart: this.props.route.params.depart,
      tripReturn: this.props.route.params.return,
      package_request_id: this.props.route.params.package_request_id,
    };
  }

  updateMasterState = (attrName, value) => {
    this.setState({[attrName]: value}, () => {
      console.log(attrName + '  ' + value);
    });
  };

  componentDidMount() {
    if (this.props.route.params.data) {
      let params = this.props.route.params;
      let data = params.data;
      this.setState({
        from: data[0].from,
        to: data[0].to,
        fromFlexibility: data[0].depart_flexibility,
        toFlexibility: data[0].return_flexibility,
        checkInDate: params.depart,
        checkOutDate: params.return ? params.return : '',
        from_nearby_airport: data[0].from_nearby_airport == 1 ? true : false,
        to_nearby_airport: data[0].to_nearby_airport == 1 ? true : false,
      });
    }
  }

  onCheckedListener = (val) => {
    if (val == 1) {
      this.setState({
        from_nearby_airport: !this.state.from_nearby_airport,
      });
    } else {
      this.setState({
        to_nearby_airport: !this.state.to_nearby_airport,
      });
    }
  };

  openCalendar = (minDate, selector) => {
    if (minDate || selector == 0) {
      this.setState({
        minDate: minDate,
        selector: selector,
        openCalendar: true,
      });
    } else {
      SimpleToast.show('First Select depart date');
    }

    if (selector == 0) {
      this.setState({
        checkOutDate: null,
      });
    }
  };

  handleNavigation() {
    if (this.state.from == '') {
      SimpleToast.show('Enter from location');
    } else if (this.state.to == '') {
      SimpleToast.show('Enter to location');
    } else if (this.state.tripDepart && this.state.checkInDate == null) {
      SimpleToast.show('Select Checkin date');
    } else if (
      this.state.tripDepart &&
      this.state.flight_mode_id == 2 &&
      this.state.checkOutDate == null
    ) {
      SimpleToast.show('Select Checkout Date');
    } else {
      this.setState({modalVisible: false});
      let params = {
        from: this.state.from,
        to: this.state.to,
        depart: this.state.checkInDate,
        depart_flexibility: this.state.fromFlexibility,
        from_nearby_airport: this.state.from_nearby_airport ? 1 : 0,
        to_nearby_airport: this.state.to_nearby_airport ? 1 : 0,
      };
      if (this.state.flight_mode_id == 2) {
        params.return = this.state.checkOutDate;
        params.return_flexibility = this.state.toFlexibility;
      }

      this.props.navigation.replace('FlightOption', {
        params: [params],
        mode: this.state.flight_mode_id,
        id: this.state.id,
        flightId: this.props.route.params.flight_id
          ? this.props.route.params.flight_id
          : null,
        package_request_id: this.state.package_request_id,
      });
    }
  }

  render() {
    return (
      <View style={Styles.container}>
        {this.state.openCalendar ? (
          <SelectDate
            allowRangeSelection={false}
            selector={this.state.selector}
            updateMasterState={this.updateMasterState}
            props={this.props.props}
            modal={true}
            minDate={this.state.minDate}
            maxDate={this.state.maxDate}
          />
        ) : (
          <Modal
            animationType={'slide'}
            transparent={true}
            style={{backgroundColor: COLOR.BLACK}}
            visible={this.state.modalVisible}
            onRequestClose={() => {
              console.log('Modal has been closed.');
            }}>
            <View style={styles.modal}>
              <View style={styles.modalBody}>
                <NavigationBar
                  prop={this.props}
                  navHeight={40}
                  name={
                    this.props.route.params.flight_id
                      ? 'Edit Flight'
                      : 'Add New Flight'
                  }
                />
                <View
                  style={{alignSelf: 'center', width: wp(90), marginTop: 10}}>
                  <View
                    style={{
                      flexDirection: 'row',
                      height: 50,
                      alignItems: 'center',
                      marginBottom: 0,
                    }}>
                    <Image
                      style={{height: 30, width: 30, marginRight: 10}}
                      source={IMAGES.pinpoint}
                      resizeMode={'contain'}
                    />
                    <View>
                      <Text style={Styles.small_label}>From</Text>
                      <TextInput
                      inputAccessoryViewID={'uniqueID'}
                        onPress={() => {
                          this.props.navigation.navigate('GooglePlaces', {
                            updateMasterState: this.updateMasterState,
                            attrName: 'from',
                          });
                          this.setState({modalVisible: false});
                        }}
                        placeholder={'City or Airport'}
                        placeholderTextColor={COLOR.LIGHT_TEXT_COLOR}
                        value={this.state.from}
                        onChangeText={(text) => this.setState({from: text})}
                        style={Styles.body_label}
                      />
                      <DoneButtonKeyboard />
                    </View>
                    <View style={styles.switchView}>
                      <Text style={styles.switchText}>Add nearby airports</Text>
                      <Switch
                        isChecked={this.state.from_nearby_airport}
                        onCheckedListener={() => this.onCheckedListener(1)}
                      />
                    </View>
                  </View>
                  <View
                    style={[
                      Styles.line_view,
                      {width: wp(90), marginVertical: 10},
                    ]}
                  />
                  <View
                    style={{
                      width: wp(90),
                      flexDirection: 'row',
                      height: 50,
                      alignItems: 'center',
                      marginBottom: 0,
                      alignSelf: 'center',
                      justifyContent: 'space-between',
                    }}>
                    <Image
                      style={{height: 30, width: 30, marginRight: 10}}
                      source={IMAGES.pinpoint}
                      resizeMode={'contain'}
                    />
                    <View>
                      <Text style={Styles.small_label}>To</Text>

                      <TextInput
                      inputAccessoryViewID={'uniqueID'}
                        onPress={() => {
                          this.props.navigation.navigate('GooglePlaces', {
                            updateMasterState: this.updateMasterState,
                            attrName: 'to',
                          });
                          this.setState({modalVisible: false});
                        }}
                        placeholder={'City or Airport'}
                        placeholderTextColor={COLOR.LIGHT_TEXT_COLOR}
                        value={this.state.to}
                        onChangeText={(text) => this.setState({to: text})}
                        style={{
                          fontFamily: FONTS.FAMILY_REGULAR,
                          fontSize: FONTS.REGULAR,
                          color: COLOR.TEXT_COLOR,
                          flex: 1,
                        }}
                      />
                    </View>
                    <View style={styles.switchView}>
                      <Text style={styles.switchText}>Add nearby airports</Text>

                      <Switch
                        isChecked={this.state.to_nearby_airport}
                        onCheckedListener={() => this.onCheckedListener(2)}
                      />
                    </View>
                  </View>
                  <View
                    style={[
                      Styles.line_view,
                      {width: wp(90), marginVertical: 10},
                    ]}
                  />
                  {this.state.tripDepart ? (
                    <View>
                      <View
                        style={{
                          flexDirection: 'row',
                          alignItems: 'center',
                          marginBottom: 0,
                        }}>
                        <Image
                          style={{height: 30, width: 30, marginRight: 10}}
                          source={IMAGES.departure_date}
                          resizeMode={'contain'}
                        />
                        <View>
                          <Text style={Styles.small_label}>Departure Date</Text>
                          <TouchableOpacity
                            onPress={() =>{
                              this.openCalendar(
                                getStartDateTimestamp(this.state.tripDepart),
                                0,
                              )
                            }
                            }>
                            <Text style={Styles.body_label}>
                              {this.state.checkInDate
                                ? this.state.checkInDate
                                : 'Select Dates'}
                            </Text>
                          </TouchableOpacity>
                        </View>
                      </View>
                      {this.state.flight_mode_id == 2 ? (
                        <View>
                          <View
                            style={[
                              Styles.line_view,
                              {width: wp(90), marginVertical: 10},
                            ]}
                          />
                          <View
                            style={{
                              flexDirection: 'row',
                              alignItems: 'center',
                              marginBottom: 0,
                            }}>
                            <Image
                              style={{height: 30, width: 30, marginRight: 10}}
                              source={IMAGES.return_date}
                              resizeMode={'contain'}
                            />
                            <View>
                              <Text style={Styles.small_label}>
                                Return Date
                              </Text>
                              <TouchableOpacity
                                onPress={() =>
                                  this.openCalendar(
                                    getStartDateTimestamp(
                                      this.state.checkInDate,
                                    ),
                                    1,
                                  )
                                }>
                                <Text style={Styles.body_label}>
                                  {this.state.checkOutDate
                                    ? this.state.checkOutDate
                                    : 'Select Date'}
                                </Text>
                              </TouchableOpacity>
                            </View>
                          </View>
                        </View>
                      ) : null}
                    </View>
                  ) : null}
                </View>
                <View
                  style={{
                    width: wp(90),
                    marginTop: 20,
                    justifyContent: 'space-between',
                    flexDirection: 'row',
                    alignItems: 'center',
                  }}>
                  <TouchableOpacity
                    style={{
                      width: wp(42.5),
                      alignSelf: 'center',
                      height: 50,
                      alignItems: 'center',
                      justifyContent: 'center',
                      backgroundColor: COLOR.WHITE,
                      borderRadius: 10,
                      marginBottom: 20,
                      borderColor: COLOR.LIGHT_TEXT,
                      borderWidth: 1,
                    }}
                    onPress={() => {
                      this.setState({modalVisible: false}, (val) => {
                        this.props.navigation.goBack();
                      });
                    }}>
                    <Text
                      style={[
                        Styles.button_font,
                        {
                          color: COLOR.BLACK,
                          width: null,
                          textAlign: 'center',
                        },
                      ]}>
                      Cancel
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={{
                      width: wp(42.5),
                      alignSelf: 'center',
                      height: 50,
                      alignItems: 'center',
                      justifyContent: 'center',
                      backgroundColor: COLOR.GREEN,
                      borderRadius: 10,
                      marginBottom: 20,
                      borderColor: COLOR.LIGHT_TEXT,
                      borderWidth: 1,
                    }}
                    onPress={() => this.handleNavigation()}>
                    <Text
                      style={[
                        Styles.button_font,
                        {
                          color: COLOR.WHITE,
                          width: null,
                          textAlign: 'center',
                        },
                      ]}>
                      Continue
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </Modal>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  modal: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
  },
  modalBody: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    backgroundColor: COLOR.WHITE,
    borderWidth: 1,
    width: wp(100),
    marginTop: 50,
    borderColor: COLOR.BORDER,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
  image: {
    height: 120,
    width: 120,
    alignSelf: 'center',
    marginVertical: 20,
  },
  headingLabel: {
    fontFamily: FONTS.FAMILY_BOLD,
    fontSize: FONTS.MEDIUM,
    color: COLOR.BLACK,
  },
  singleLineView: {
    marginVertical: 20,
    borderBottomColor: COLOR.GRAY,
    width: wp(100),
    alignSelf: 'center',
    borderBottomWidth: 1,
  },
  borderView: {
    borderRadius: 10,
    borderWidth: 3,
    width: wp(90),
    borderColor: COLOR.GRAY,
    alignSelf: 'center',
  },
  switchView: {
    position: 'absolute',
    justifyContent: 'flex-end',
    right: 0,
  },
  switchText: {
    fontFamily: FONTS.FAMILY_REGULAR,
    color: COLOR.LIGHT_TEXT_COLOR,
  },
});
