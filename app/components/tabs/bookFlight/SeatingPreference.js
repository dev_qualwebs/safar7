import React from 'react';
import { Modal, Text, View, StyleSheet, Image } from 'react-native';
import { TouchableOpacity } from 'react-native';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import COLOR from '../../styles/Color';
import FONTS from '../../styles/Fonts';
import IMAGES from '../../styles/Images';
import Styles from '../../styles/Styles';
import NavigationBar from '../../commonView/NavigationBar';


export default class SeatingPreferences extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: true,
    };
  }

  updateMasterState = (attrName, value) => {
    this.setState({ [attrName]: value });
  };

  SelectionView = (props) => {
    return (
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          marginTop: 10,
        }}>
        <Text style={[Styles.body_label, { width: 130 }]}>{props.name}</Text>
        <Text style={[Styles.body_label, { width: null }]}>{props.time}</Text>
        <TouchableOpacity
          style={{
            height: 25,
            width: 25,
            borderRadius: 5,
            backgroundColor: props.bg,
            borderColor: COLOR.GRAY,
            justifyContent: 'center',
            alignItems: 'center',
            borderWidth: 1,
          }}>
          <Image
            resizeMode={'contain'}
            style={{ height: 20, width: 20, alignSelf: 'center' }}
            source={props.image}
          />
        </TouchableOpacity>
      </View>
    );
  };

  render() {
    return (
      <View style={Styles.container}>
        <Modal
          animationType={'slide'}
          transparent={true}
          style={{ backgroundColor: COLOR.BLACK }}
          visible={this.state.modalVisible}
          onRequestClose={() => {
            console.log('Modal has been closed.');
          }}>
          <View style={styles.modal}>
            <View style={styles.modalBody}>
              <NavigationBar
                prop={this.props}
                navHeight={40}
                right_image={IMAGES.filter_image}
                name={'Seating Preference'}
              />
              <View style={{ flex: 1 }}>
                <View
                  style={{
                    width: wp(90),
                    alignSelf: 'center',
                    marginTop: 20,
                  }}>
                  <Text
                    style={[
                      Styles.body_label,
                      { fontFamily: FONTS.FAMILY_BOLD },
                    ]}>
                    Seating Position
                  </Text>
                  <Text style={Styles.small_label}>
                    *Choosing seating position may cost extra-fees
                  </Text>
                </View>
                <View
                  style={{
                    width: wp(90),
                    alignSelf: 'center',
                    paddingVertical: 10,
                  }}>
                  <this.SelectionView
                    bg={COLOR.GREEN}
                    name={'Any'}
                    time={''}
                    image={IMAGES.tick_image}
                  />
                  <this.SelectionView
                    bg={COLOR.GREEN}
                    name={'Aisle'}
                    time={''}
                    image={IMAGES.tick_image}
                  />
                  <this.SelectionView
                    bg={COLOR.GREEN}
                    name={'Window'}
                    time={''}
                    image={IMAGES.tick_image}
                  />
                </View>
                <View style={[Styles.line_view, { marginVertical: 20 }]} />
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                    alignSelf: 'center',
                    marginVertical: 10,
                    width: wp(90),
                  }}>
                  <View
                    style={{
                      flexDirection: 'row',
                    }}>
                    <Text
                      style={[
                        Styles.body_label,
                        { width: null, fontFamily: FONTS.FAMILY_BOLD },
                      ]}>
                      Seat Together
                    </Text>
                    <Text
                      style={[
                        Styles.body_label,
                        {
                          width: null,
                          marginRight: 5,
                          alignItems: 'flex-start',
                          color: COLOR.LIGHT_TEXT_COLOR,
                        },
                      ]}>
                      (upon availablity)
                    </Text>
                  </View>
                  <Image
                    style={{ height: 30, width: 55 }}
                    source={IMAGES.radio_button_on}
                    resizeMode={'contain'}
                  />
                </View>
                <View style={[Styles.line_view, { marginVertical: 20 }]} />
                <TouchableOpacity
                  style={{
                    width: wp(90),
                    alignSelf: 'center',
                    height: 50,
                    alignItems: 'center',
                    justifyContent: 'center',
                    backgroundColor: COLOR.GREEN,
                    borderRadius: 10,
                    bottom: 20,
                    position: 'absolute',
                  }}
                  onPress={() => {
                    this.setState({ modalVisible: false }, (val) => {
                      this.props.navigation.navigate('Equipments');
                    });
                  }}>
                  <Text
                    style={[
                      Styles.button_font,
                      {
                        color: COLOR.WHITE,
                        width: null,
                        textAlign: 'center',
                      },
                    ]}>
                    Apply
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  modal: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
  },
  modalBody: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    backgroundColor: COLOR.WHITE,
    borderWidth: 1,
    width: wp(100),
    marginTop: 50,
    borderColor: COLOR.BORDER,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    flex: 1,
  },
  image: {
    height: 120,
    width: 120,
    alignSelf: 'center',
    marginVertical: 20,
  },
  headingLabel: {
    fontFamily: FONTS.FAMILY_BOLD,
    fontSize: FONTS.MEDIUM,
    color: COLOR.BLACK,
  },
  singleLineView: {
    marginVertical: 20,
    borderBottomColor: COLOR.GRAY,
    width: wp(100),
    alignSelf: 'center',
    borderBottomWidth: 1,
  },
  borderView: {
    borderRadius: 10,
    borderWidth: 3,
    width: wp(90),
    borderColor: COLOR.GRAY,
    alignSelf: 'center',
  },
});
