import React from 'react';
import {Modal, Text, View, StyleSheet, Image} from 'react-native';
import {
  FlatList,
  TouchableOpacity,
  TouchableWithoutFeedback,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import COLOR from '../../styles/Color';
import FONTS from '../../styles/Fonts';
import IMAGES from '../../styles/Images';
import Styles from '../../styles/Styles';
import NavigationBar from '../../commonView/NavigationBar';
import SimpleToast from 'react-native-simple-toast';

export default class AddTransportation extends React.Component {
  constructor(props) {
    super(props);
    let params = this.props.route.params;
    this.state = {
      modalVisible: true,
      val: params.val,
      type: params.type,
      selectedIndex: null,
      transportationData: [
        {
          id: 1,
          name: 'From Airport\nTo Stay',
        },
        {
          id: 2,
          name: 'From Stay\nTo Airport',
        },
        {
          id: 3,
          name: 'From Airport\nTo Home',
        },
        {
          id: 4,
          name: 'From Home\nTo Airport',
        },
      ],
    };
  }

  updateMasterState = (attrName, value) => {
    this.setState({[attrName]: value});
  };

  render() {
    return (
      <View style={Styles.container}>
        <Modal
          animationType={'slide'}
          transparent={true}
          style={{backgroundColor: COLOR.BLACK}}
          visible={this.state.modalVisible}
          onRequestClose={() => {
            console.log('Modal has been closed.');
          }}>
          <View style={styles.modal}>
            <View style={styles.modalBody}>
              <NavigationBar
                prop={this.props}
                navHeight={40}
                left_image={IMAGES.back_arrow}
                name={'Transportation'}
              />
              <View
                style={{
                  flex: 1,
                  width: wp(90),
                  alignSelf: 'center',
                  paddingTop: 20,
                }}>
                <FlatList
                  showsVerticalScrollIndicator={false}
                  data={this.state.transportationData}
                  keyExtractor={(index) => index}
                  renderItem={({item, index}) => (
                    <TouchableOpacity
                      key={index}
                      onPress={() =>
                        this.setState({
                          selectedIndex: item.id,
                        })
                      }
                      style={{
                        marginRight: 15,
                        marginBottom: 30,
                        height: 120,
                        width: wp(43),
                        backgroundColor: COLOR.WHITE,
                        justifyContent: 'center',
                        alignItems: 'center',
                        alignSelf: 'center',
                        borderRadius: 10,
                        borderWidth: 1,
                        borderColor:
                          this.state.selectedIndex == item.id
                            ? COLOR.GREEN
                            : COLOR.GRAY,
                      }}>
                      <Text
                        style={[
                          Styles.subheading_label,
                          {textAlign: 'center'},
                        ]}>
                        {item.name}
                      </Text>
                    </TouchableOpacity>
                  )}
                  numColumns={2}
                  ListEmptyComponent={() =>
                    !this.state.transportationData.length ? (
                      <Text
                        style={{
                          textAlign: 'center',
                          color: COLOR.VOILET,
                          marginVertical: 40,
                        }}>
                        You did not add any Transportation
                      </Text>
                    ) : null
                  }
                  keyExtractor={(item, index) => index.toString()}
                />
                <View
                  style={[
                    Styles.line_view,
                    {width: wp(80), alignSelf: 'center', marginVertical: 10},
                  ]}
                />
              </View>

              <TouchableOpacity
                style={{
                  width: wp(90),
                  alignSelf: 'center',
                  height: 50,
                  alignItems: 'center',
                  justifyContent: 'center',
                  backgroundColor: COLOR.GREEN,
                  borderRadius: 10,
                  marginBottom: 20,
                }}
                onPress={() => {
                  if (this.state.selectedIndex) {
                    this.setState({modalVisible: false}, (val) => {
                      this.props.navigation.replace('TransportationAddress', {
                        data: this.state.transportationData[
                          this.state.selectedIndex - 1
                        ],
                        type: this.state.type,
                        val: this.state.val,
                      });
                    });
                  } else {
                    SimpleToast.show('Select One Option');
                  }
                }}>
                <Text
                  style={[
                    Styles.button_font,
                    {
                      color: COLOR.WHITE,
                      width: null,
                      textAlign: 'center',
                    },
                  ]}>
                  Continue
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  modal: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
  },
  modalBody: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    backgroundColor: COLOR.WHITE,
    borderWidth: 1,
    width: wp(100),
    marginTop: 50,
    borderColor: COLOR.BORDER,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    flex: 1,
  },
  image: {
    height: 120,
    width: 120,
    alignSelf: 'center',
    marginVertical: 20,
  },
  headingLabel: {
    fontFamily: FONTS.FAMILY_BOLD,
    fontSize: FONTS.MEDIUM,
    color: COLOR.BLACK,
  },
  singleLineView: {
    marginVertical: 20,
    borderBottomColor: COLOR.GRAY,
    width: wp(100),
    alignSelf: 'center',
    borderBottomWidth: 1,
  },
  borderView: {
    borderRadius: 10,
    borderWidth: 3,
    width: wp(90),
    borderColor: COLOR.GRAY,
    alignSelf: 'center',
  },
});
