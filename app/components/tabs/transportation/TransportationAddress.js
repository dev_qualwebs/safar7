import React from 'react';
import {Modal, Text, View, StyleSheet, Image} from 'react-native';
import {
  FlatList,
  TextInput,
  TouchableOpacity,
  TouchableWithoutFeedback,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
  widthPercentageToDP,
} from 'react-native-responsive-screen';
import COLOR from '../../styles/Color';
import FONTS from '../../styles/Fonts';
import IMAGES from '../../styles/Images';
import Styles from '../../styles/Styles';
import NavigationBar from '../../commonView/NavigationBar';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview';
import GooglePlacesTextInput from '../../commonView/GooglePlacesTextInput';
import SimpleToast from 'react-native-simple-toast';
import SelectDate from '../calendar/SelectDate';
import API from '../../../api/Api';
import ActivityIndicator from '../../commonView/ActivityIndicator';
import { DoneButtonKeyboard } from '../../commonView/CommonView';
import { connect } from 'react-redux';
import RequestAction from '../../../redux/action/RequestAction';

const api = new API();
 class TransportationAddress extends React.Component {
  constructor(props) {
    super(props);
    let params = this.props.route.params;
    this.state = {
      modalVisible: true,
      data: params.data,
      selectedIndex: null,
      pickupLocation: null,
      dropoffLocation: null,
      travellingDate: null,
      transportationData: [
        {
          id: 1,
          name: 'From Airport To Stay',
        },
        {
          id: 2,
          name: 'From Stay To Airport',
        },
        {
          id: 3,
          name: 'From Airport To Home',
        },
        {
          id: 4,
          name: 'From Home To Airport',
        },
      ],
      baggage: 0,
      sync: false,
      type: params.type,
      val: params.val,
      loading: false,
    };
    this.dateRef = React.createRef();
  }

  componentDidMount() {
    let params = this.props.route.params;
    this.setState({
      data: this.state.transportationData.find(
        (value) => value.id == params.data.id,
      ),
    });
  }

  updateMasterState = (attrName, value) => {
    this.setState({[attrName]: value});
  };

  CommonView = (props) => (
    <View
      style={{
        flexDirection: 'row',
        width: 120,
        justifyContent: 'space-between',
      }}>
      <View
        style={{flex: 1, alignSelf: 'center', justifyContent: 'flex-start'}}>
        <Text
          style={[
            Styles.button_font,
            {width: null, color: COLOR.BLACK, alignSelf: 'flex-start'},
          ]}>
          {props.category}
        </Text>
        <Text
          style={[
            Styles.small_label,
            {width: null, color: COLOR.BLACK, alignSelf: 'flex-start'},
          ]}>
          {props.ageGroup}
        </Text>
      </View>
      <View
        style={{
          flex: 1,
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'flex-end',
        }}>
        <TouchableOpacity
          onPress={() => {
            if (this.state.baggage != 0) {
              this.setState({
                baggage: this.state.baggage - 1,
              });
            }
          }}
          style={{
            height: 30,
            width: 30,
            borderRadius: 15,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Image
            style={{height: 30, width: 30}}
            resizeMode={'contain'}
            source={IMAGES.minus_image}
          />
        </TouchableOpacity>
        <Text
          style={[
            Styles.button_font,
            {width: 40, color: COLOR.BLACK, textAlign: 'center'},
          ]}>
          {this.state.baggage}
        </Text>
        <TouchableOpacity
          onPress={() =>
            this.setState({
              baggage: this.state.baggage + 1,
            })
          }
          style={{
            height: 30,
            width: 30,
            borderWidth: 1,
            borderRadius: 15,
            borderColor: COLOR.GREEN,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Image style={{height: 20, width: 20}} source={IMAGES.plus_image} />
        </TouchableOpacity>
      </View>
    </View>
  );

  addTransportation = async () => {
    if (this.state.pickupLocation == '' || this.state.pickupLocation == null) {
      SimpleToast.show('Enter pickup location');
    } else if (
      this.state.dropoffLocation == '' ||
      this.state.dropoffLocation == null
    ) {
      SimpleToast.show('Enter drop-off Location');
    } else if (this.state.travellingDate == null) {
      SimpleToast.show('Select Date of transportation');
    } else if (this.state.selectedIndex == null) {
      SimpleToast.show('Select Type of transportation');
    } else {
      let that = this;
      let data = JSON.stringify({
        trip_addon_id: 4,
        transportation_mode_id: `${this.state.data.id}`,
        transportation_type_id: `${this.state.selectedIndex}`,
        pickup: this.state.pickupLocation,
        drop_off: this.state.dropoffLocation,
        travelling_date: this.state.travellingDate,
        baggage: `${this.state.baggage}`,
        sync: this.state.sync ? 1 : 0,
      });
      // this.setState({
      //   loading: true,
      // });
      api
        .addAddons(this.state.type, this.state.val, data)
        .then((json) => {
          this.props.getAddons(this.state.type, this.state.val)
          console.log(json.data.response);
          this.setState({modalVisible: false, loading: false}, (val) => {
            this.props.navigation.goBack();
          });
        })
        .catch((error) => {
          that.setState({
            loading: false,
          });
          SimpleToast.show(error.response.data.message);
        });
    }
  };

  render() {
    return (
      <View style={Styles.container}>
        {this.state.openCalendar ? (
          <SelectDate
            allowRangeSelection={false}
            attrName={'travellingDate'}
            updateMasterState={this.updateMasterState}
            props={this.props}
          />
        ) : this.state.loading ? (
          <ActivityIndicator loading={true} />
        ) : (
          <Modal
            animationType={'slide'}
            transparent={true}
            style={{backgroundColor: COLOR.BLACK}}
            visible={this.state.modalVisible}
            onRequestClose={() => {
              console.log('Modal has been closed.');
            }}>
            <View style={styles.modal}>
              <View style={styles.modalBody}>
                <NavigationBar
                  back_image={IMAGES.filter_image}
                  prop={this.props}
                  navHeight={40}
                  name={this.state.data.name}
                />
                <KeyboardAwareScrollView
                  showsHorizontalScrollIndicator={false}
                  showsVerticalScrollIndicator={false}>
                  <View
                    style={{
                      flex: 1,
                      width: wp(90),
                      alignSelf: 'center',
                      marginVertical: 20,
                    }}>
                    <View
                      style={{
                        paddingHorizontal: 10,
                        paddingVertical: 20,
                        backgroundColor: COLOR.WHITE,
                        borderColor: COLOR.GRAY,
                        borderWidth: 1,
                        shadowColor: COLOR.VOILET,
                        shadowOffset: {width: 0.2, height: 0.2},
                        shadowOpacity: 0.2,
                        borderRadius: 10,
                        elevation: 3,
                        zIndex: 777,
                      }}>
                      <View
                        style={{
                          flexDirection: 'row',
                          alignItems: 'center',
                        }}>
                        <TouchableOpacity
                          onPress={() => {
                            this.setState({
                              sync: !this.state.sync,
                            });
                          }}>
                          <View
                            style={{
                              height: 25,
                              width: 25,
                              borderRadius: 4,
                              borderWidth: 1,
                              borderColor: COLOR.GREEN,
                              backgroundColor: this.state.sync
                                ? COLOR.GREEN
                                : COLOR.WHITE,
                              alignItems: 'center',
                              justifyContent: 'center',
                              marginRight: 15,
                            }}>
                            <Image
                              style={{
                                height: 15,
                                width: 15,
                              }}
                              source={IMAGES.tick_image}
                              resizeMode={'contain'}
                            />
                          </View>
                        </TouchableOpacity>
                        <Text style={Styles.body_label}>
                          Sync with Flight / Stay booking information
                        </Text>
                      </View>
                      <View
                        style={[
                          Styles.line_view,
                          {marginVertical: 10, width: wp(85)},
                        ]}
                      />

                      <View
                        style={{
                          flexDirection: 'row',
                          alignItems: 'center',
                          zIndex: 999,
                        }}>
                        <TouchableOpacity>
                          <View
                            style={{
                              height: 30,
                              width: 30,

                              alignItems: 'center',
                              justifyContent: 'center',
                              marginRight: 10,
                            }}>
                            <Image
                              style={{
                                height: 30,
                                width: 30,
                              }}
                              source={IMAGES.pinpoint}
                              re
                              sizeMode={'contain'}
                            />
                          </View>
                        </TouchableOpacity>
                        <View>
                          <Text
                            style={[
                              Styles.extra_small_label,
                              {color: COLOR.LIGHT_TEXT},
                            ]}>
                            Pickup Location
                          </Text>
                          <GooglePlacesTextInput
                            value={this.state.pickupLocation}
                            onValueChange={(text) => {
                              this.setState({
                                pickupLocation: text,
                              });
                            }}
                          />
                        </View>
                      </View>
                      <View
                        style={[
                          Styles.line_view,
                          {marginVertical: 10, width: wp(85)},
                        ]}
                      />
                      <View
                        style={{
                          flexDirection: 'row',
                          alignItems: 'center',
                          zIndex: 888,
                        }}>
                        <TouchableOpacity>
                          <View
                            style={{
                              height: 30,
                              width: 30,
                              alignItems: 'center',
                              justifyContent: 'center',
                              marginRight: 10,
                            }}>
                            <Image
                              style={{
                                height: 30,
                                width: 30,
                              }}
                              source={IMAGES.dropoff}
                              re
                              sizeMode={'contain'}
                            />
                          </View>
                        </TouchableOpacity>
                        <View>
                          <Text
                            style={[
                              Styles.extra_small_label,
                              {color: COLOR.LIGHT_TEXT},
                            ]}>
                            Drop-Off Location
                          </Text>
                          <GooglePlacesTextInput
                            value={this.state.dropoffLocation}
                            onValueChange={(text) => {
                              this.setState({
                                dropoffLocation: text,
                              });
                            }}
                          />
                        </View>
                      </View>
                      <View
                        style={[
                          Styles.line_view,
                          {marginVertical: 10, width: wp(85)},
                        ]}
                      />
                      <View
                        style={{
                          flexDirection: 'row',
                          alignItems: 'center',
                        }}>
                        <TouchableOpacity>
                          <View
                            style={{
                              height: 30,
                              width: 30,

                              alignItems: 'center',
                              justifyContent: 'center',
                              marginRight: 10,
                            }}>
                            <Image
                              style={{
                                height: 30,
                                width: 30,
                                overflow: 'hidden',
                              }}
                              source={IMAGES.calendar_arrow}
                              sizeMode={'contain'}
                            />
                          </View>
                        </TouchableOpacity>
                        <View>
                          <Text
                            style={[
                              Styles.extra_small_label,
                              {color: COLOR.LIGHT_TEXT},
                            ]}>
                            Date of transportation
                          </Text>
                          <TextInput
                          inputAccessoryViewID={'uniqueID'}
                            ref={this.dateRef}
                            style={[
                              Styles.body_label,
                              {marginTop: 0, height: 30},
                            ]}
                            value={this.state.travellingDate}
                            onFocus={() => {
                              this.setState({
                                openCalendar: true,
                              });
                              this.dateRef.current.blur();
                            }}
                            placeholderTextColor={COLOR.LIGHT_TEXT_COLOR}
                            placeholder={'Enter the date of the ride'}
                          />
                          <DoneButtonKeyboard />
                        </View>
                      </View>
                    </View>
                    <View
                      style={{
                        marginVertical: 20,
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                      }}>
                      <Text style={[Styles.subheading_label, {width: null}]}>
                        Baggage
                      </Text>
                      <this.CommonView />
                    </View>
                    <Text
                      style={[
                        Styles.subheading_label,
                        {
                          width: null,
                          alignSelf: 'flex-start',
                          marginBottom: 20,
                        },
                      ]}>
                      Type of Transportation
                    </Text>
                    <TouchableOpacity
                      onPress={() => {
                        this.setState({
                          selectedIndex: 1,
                        });
                      }}>
                      <View
                        style={{
                          flexDirection: 'row',
                          alignItems: 'center',
                          height: 50,
                          borderWidth: 1,
                          borderColor:
                            this.state.selectedIndex == 1
                              ? COLOR.GREEN
                              : COLOR.GRAY,
                          borderRadius: 10,
                          paddingHorizontal: 10,
                          marginBottom: 10,
                        }}>
                        <View
                          style={{
                            height: 30,
                            width: 30,
                            alignItems: 'center',
                            justifyContent: 'center',
                            marginRight: 10,
                          }}>
                          <Image
                            style={{
                              height: 30,
                              width: 30,
                            }}
                            source={IMAGES.train}
                            sizeMode={'contain'}
                          />
                        </View>

                        <Text>Tram Train or Metro</Text>
                      </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                      onPress={() => {
                        this.setState({
                          selectedIndex: 2,
                        });
                      }}>
                      <View
                        style={{
                          flexDirection: 'row',
                          alignItems: 'center',
                          height: 50,
                          borderWidth: 1,
                          borderColor:
                            this.state.selectedIndex == 2
                              ? COLOR.GREEN
                              : COLOR.GRAY,
                          borderRadius: 10,
                          paddingHorizontal: 10,
                          marginBottom: 10,
                        }}>
                        <View
                          style={{
                            height: 30,
                            width: 30,
                            alignItems: 'center',
                            justifyContent: 'center',
                            marginRight: 10,
                          }}>
                          <Image
                            style={{
                              height: 30,
                              width: 30,
                            }}
                            source={IMAGES.bus_front}
                          />
                        </View>

                        <Text>Airport, Shuttle or Bus</Text>
                      </View>
                    </TouchableOpacity>

                    <TouchableOpacity
                      onPress={() => {
                        this.setState({
                          selectedIndex: 3,
                        });
                      }}>
                      <View
                        style={{
                          flexDirection: 'row',
                          alignItems: 'center',
                          height: 50,
                          borderWidth: 1,
                          borderColor:
                            this.state.selectedIndex == 3
                              ? COLOR.GREEN
                              : COLOR.GRAY,
                          borderRadius: 10,
                          paddingHorizontal: 10,
                          marginBottom: 10,
                        }}>
                        <View
                          style={{
                            height: 30,
                            width: 30,
                            alignItems: 'center',
                            justifyContent: 'center',
                            marginRight: 10,
                          }}>
                          <Image
                            style={{
                              height: 30,
                              width: 30,
                            }}
                            source={IMAGES.car_image}
                            re
                            sizeMode={'contain'}
                          />
                        </View>

                        <Text>Taxi</Text>
                      </View>
                    </TouchableOpacity>

                    <TouchableOpacity
                      onPress={() => {
                        this.setState({
                          selectedIndex: 4,
                        });
                      }}>
                      <View
                        style={{
                          flexDirection: 'row',
                          alignItems: 'center',
                          height: 50,
                          borderWidth: 1,
                          borderColor:
                            this.state.selectedIndex == 4
                              ? COLOR.GREEN
                              : COLOR.GRAY,
                          borderRadius: 10,
                          paddingHorizontal: 10,
                          marginBottom: 10,
                        }}>
                        <View
                          style={{
                            height: 30,
                            width: 30,
                            alignItems: 'center',
                            justifyContent: 'center',
                            marginRight: 10,
                          }}>
                          <Image
                            style={{
                              height: 30,
                              width: 30,
                            }}
                            source={IMAGES.car_image}
                            resizeMode={'contain'}
                          />
                        </View>

                        <Text>Private Driver</Text>
                      </View>
                    </TouchableOpacity>
                  </View>
                </KeyboardAwareScrollView>
                <TouchableOpacity
                  style={{
                    width: wp(90),
                    alignSelf: 'center',
                    height: 50,
                    alignItems: 'center',
                    justifyContent: 'center',
                    backgroundColor: COLOR.GREEN,
                    borderRadius: 10,
                    marginBottom: 20,
                  }}
                  onPress={() => {
                    // this.setState({modalVisible: false}, (val) => {
                    //   this.props.navigation.navigate('HomeStack');
                    // });
                    this.addTransportation();
                  }}>
                  <Text
                    style={[
                      Styles.button_font,
                      {
                        color: COLOR.WHITE,
                        width: null,
                        textAlign: 'center',
                      },
                    ]}>
                    Add
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </Modal>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  modal: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
  },
  modalBody: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    backgroundColor: COLOR.WHITE,
    borderWidth: 1,
    width: wp(100),
    marginTop: 50,
    borderColor: COLOR.BORDER,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    flex: 1,
  },
  image: {
    height: 120,
    width: 120,
    alignSelf: 'center',
    marginVertical: 20,
  },
  headingLabel: {
    fontFamily: FONTS.FAMILY_BOLD,
    fontSize: FONTS.MEDIUM,
    color: COLOR.BLACK,
  },
  singleLineView: {
    marginVertical: 20,
    borderBottomColor: COLOR.GRAY,
    width: wp(100),
    alignSelf: 'center',
    borderBottomWidth: 1,
  },
  borderView: {
    borderRadius: 10,
    borderWidth: 3,
    width: wp(90),
    borderColor: COLOR.GRAY,
    alignSelf: 'center',
  },
});

const mapDispatchToProps = (dispatch) => {
  return {
    getAddons: (type, val) => {
      dispatch(RequestAction.getTripAddons(type, val));
    },
  };
};

export default connect(null, mapDispatchToProps)(TransportationAddress);
