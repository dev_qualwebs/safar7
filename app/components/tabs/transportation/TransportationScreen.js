import React from 'react';
import {Modal, Text, View, StyleSheet, Image} from 'react-native';
import {
  FlatList,
  TouchableOpacity,
  TouchableWithoutFeedback,
  TextInput,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import COLOR from '../../styles/Color';
import FONTS from '../../styles/Fonts';
import IMAGES from '../../styles/Images';
import Styles from '../../styles/Styles';
import NavigationBar from '../../commonView/NavigationBar';
import API from '../../../api/Api';
import { DoneButtonKeyboard } from '../../commonView/CommonView';
import { connect } from 'react-redux';
import RequestAction from '../../../redux/action/RequestAction';

const api = new API();
 class TransportationScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: true,
      transportationData: [],
      type: null,
      val: null,
    };
  }

  updateMasterState = (attrName, value) => {
    this.setState({[attrName]: value});
  };

  componentDidMount() {
    if (this.props.route.params) {
      let params = this.props.route.params;
      this.setState({
        type: params.type,
        val: params.val,
      });
    }
    this.unSubscribe = this.props.navigation.addListener('focus', () => {
      setTimeout(() => {
        this.setState({
          modalVisible: true,
        });
        if (this.props.route.params) {
          let params = this.props.route.params;
          this.getTripTransportation(params.val, params.type);
        }
      }, 500);
    });
  }

  componentWillUnmount() {
    this.unSubscribe();
  }

  getTripTransportation = async (val, type) => {
    let that = this;
    api
      .getTripTransportation(val, type)
      .then((json) => {
        console.log('transportation response', json.data.response);
        that.setState({
          transportationData: json.data.response,
        });
      })
      .catch(function (error) {
        console.log(error.response.data.message);
      });
  };

  render() {
    return (
      <View style={Styles.container}>
        <Modal
          animationType={'slide'}
          transparent={true}
          style={{backgroundColor: COLOR.BLACK}}
          visible={this.state.modalVisible}
          onRequestClose={() => {
            console.log('Modal has been closed.');
          }}>
          <View style={styles.modal}>
            <View style={styles.modalBody}>
              <NavigationBar
                prop={this.props}
                left_image={IMAGES.back_arrow}
                navHeight={40}
                name={'Transportation'}
              />
              <View
                style={{
                  width: wp(90),
                  alignSelf: 'center',
                  flex: 1,
                }}>
                <View>
                  <FlatList
                    showsVerticalScrollIndicator={false}
                    data={this.state.transportationData}
                    renderItem={({item}) => (
                      <>
                        <TouchableWithoutFeedback>
                          <View
                            style={{
                              alignSelf: 'center',
                              marginTop: 10,
                              backgroundColor: COLOR.WHITE,
                              width: wp(85),
                            }}>
                            <View
                              style={{
                                height: 50,
                                flex: 1,
                                flexDirection: 'row',
                                justifyContent: 'space-between',
                                alignSelf: 'center',
                                alignItems: 'center',
                              }}>
                              <Text
                                style={[
                                  Styles.body_label,
                                  {width: null, flex: 1},
                                ]}>
                                {item.pickup}
                              </Text>
                            </View>
                          </View>
                        </TouchableWithoutFeedback>

                        <View
                          style={[
                            Styles.line_view,
                            {
                              width: wp(80),
                              alignSelf: 'center',
                            },
                          ]}
                        />
                      </>
                    )}
                    numColumns={1}
                    ListEmptyComponent={() =>
                      !this.state.transportationData.length ? (
                        <Text
                          style={{
                            textAlign: 'center',
                            color: COLOR.VOILET,
                            marginVertical: 40,
                          }}>
                          You did not add any Transportation
                        </Text>
                      ) : null
                    }
                    keyExtractor={(item, index) => index.toString()}
                  />

                  <TouchableOpacity
                    style={{
                      width: wp(85),
                      alignSelf: 'center',
                      height: 50,
                      alignItems: 'center',
                      justifyContent: 'center',
                      backgroundColor: COLOR.WHITE,
                      borderRadius: 10,
                      marginBottom: 20,
                      borderColor: COLOR.LIGHT_TEXT,
                      borderWidth: 1,
                      marginTop: 20,
                    }}
                    onPress={() => {
                      this.setState({modalVisible: false}, (val) => {
                        this.props.navigation.replace('AddTransportation', {
                          type: this.state.type,
                          val: this.state.val,
                        });
                      });
                    }}>
                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                      <Image
                        style={{
                          width: 25,
                          height: 25,
                          borderRadius: 10,
                          marginRight: 5,
                        }}
                        source={IMAGES.add_plus}
                      />
                      <Text
                        style={[
                          Styles.button_font,
                          {
                            color: COLOR.BLACK,
                            width: null,
                            textAlign: 'center',
                          },
                        ]}>
                        Add Transportation
                      </Text>
                    </View>
                  </TouchableOpacity>

                  <Text
                    style={[
                      Styles.subheading_label,
                      {marginTop: 20, marginBottom: 10},
                    ]}>
                    Comments
                  </Text>
                  <View
                    style={[
                      styles.back_view,
                      {
                        height: 120,
                        width: wp(90),
                        alignSelf: 'center',
                        marginBottom: 20,
                      },
                    ]}>
                    <TextInput
                      placeholderTextColor={COLOR.LIGHT_TEXT_COLOR}
                      placeholder={'Write your comments'}
                    />
                    <DoneButtonKeyboard />
                  </View>
                </View>
              </View>

              <TouchableOpacity
                style={{
                  width: wp(90),
                  alignSelf: 'center',
                  height: 50,
                  alignItems: 'center',
                  justifyContent: 'center',
                  backgroundColor: COLOR.GREEN,
                  borderRadius: 10,
                  marginBottom: 20,
                }}
                onPress={() => {
                  this.setState({modalVisible: false}, (val) => {
                    this.props.navigation.replace('AddTransportation', {
                      type: this.state.type,
                      val: this.state.val,
                    });
                  });
                }}>
                <Text
                  style={[
                    Styles.button_font,
                    {
                      color: COLOR.WHITE,
                      width: null,
                      textAlign: 'center',
                    },
                  ]}>
                  Continue
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  modal: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
  },
  modalBody: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    backgroundColor: COLOR.WHITE,
    borderWidth: 1,
    width: wp(100),
    marginTop: 50,
    borderColor: COLOR.BORDER,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    flex: 1,
  },
  image: {
    height: 120,
    width: 120,
    alignSelf: 'center',
    marginVertical: 20,
  },
  headingLabel: {
    fontFamily: FONTS.FAMILY_BOLD,
    fontSize: FONTS.MEDIUM,
    color: COLOR.BLACK,
  },
  singleLineView: {
    marginVertical: 20,
    borderBottomColor: COLOR.GRAY,
    width: wp(100),
    alignSelf: 'center',
    borderBottomWidth: 1,
  },
  borderView: {
    borderRadius: 10,
    borderWidth: 3,
    width: wp(90),
    borderColor: COLOR.GRAY,
    alignSelf: 'center',
  },
  back_view: {
    backgroundColor: COLOR.WHITE,
    borderRadius: 10,
    shadowColor: COLOR.WHITE,
    shadowOffset: {width: 0.2, height: 0.2},
    shadowOpacity: 0.2,
    paddingHorizontal: 10,
    paddingVertical: 20,
    borderWidth: 1,
    borderColor: COLOR.GRAY,
    elevation: 3,
  },
});

const mapDispatchToProps = (dispatch) => {
  return {
    getAddons: (type,val) => {
      dispatch(RequestAction.getTripAddons(type,val));
    },
  };
};

export default connect(null, mapDispatchToProps)(TransportationScreen);
