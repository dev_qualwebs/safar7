import {View, Image, Text} from 'react-native';
import React from 'react';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import COLOR from '../../styles/Color';
import Styles from '../../styles/Styles';
import {
  FlatList,
  TouchableOpacity,
  TouchableWithoutFeedback,
} from 'react-native';
import IMAGES from '../../styles/Images';
import TopTabBar from '../home/trip/TabBarView';
import {T_SCHEDULE_FLIGHT_TAB} from '../../../helper/Constants';
import FONTS from '../../styles/Fonts';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import TripDateView from '../../commonView/TripDateView';
import API from '../../../api/Api';
import SimpleToast from 'react-native-simple-toast';

const api = new API();
export default class TransferScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      transferData: [],
      tripDepart: null,
      tripReturn: null,
      departFlexibility: null,
      returnFlexibility: null,
      isEdit: false,
    };
  }

  componentDidMount() {
    this.props.navigation.addListener('focus', () => {
      this.getTransfers();
    });

    if (this.props.route.params && this.props.route.params.isEdit) {
      this.setState({isEdit: true});
    }

    if (this.props.route.params.depart) {
      let data = this.props.route.params;
      this.setState({
        tripDepart: data.depart,
        tripReturn: data.return,
        departFlexibility: data.departFlexibility,
        returnFlexibility: data.returnFlexibility,
      });
    }
  }

  getTransfers = async () => {
    api
      .getTransfers(this.props.route.params.id)
      .then((json) => {
        console.log(json.data.response);
        this.setState({
          transferData: json.data.response,
        });
      })
      .catch((error) => {
        console.log(error.response.data);
      });
  };

  deleteTransfer = async (id) => {
    api
      .deleteTransfer(id)
      .then((json) => {
        this.getTransfers();
      })
      .catch((error) => {
        SimpleToast.show(error.response.data.message);
      });
  };

  render() {
    const screens = this.props.route.params.screens;
    const index = this.props.route.params.index;
    const id = this.props.route.params.id;
    return (
      <View style={{flex: 1, backgroundColor: COLOR.BACKGROUND}}>
        <View
          style={{
            height: 220,
            backgroundColor: COLOR.GREEN,
            borderBottomRightRadius: 15,
            borderBottomLeftRadius: 15,
          }}>
          <View
            style={[
              Styles.mainView,
              {
                height: 45,
                marginTop: getStatusBarHeight(true),
                justifyContent: 'center',
                alignSelf: 'center',
                alignItems: 'center',
                flexDirection: 'row',
              },
            ]}>
            <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
              <Image
                style={{height: 15, width: 15, tintColor: COLOR.WHITE}}
                source={IMAGES.back_arrow}
              />
            </TouchableOpacity>
            <TouchableOpacity>
              <View>
                <Text
                  style={[
                    Styles.heading_label,
                    {
                      textAlign: 'center',
                      color: COLOR.WHITE,
                      marginVertical: 0,
                      fontFamily: FONTS.FAMILY_BOLD,
                    },
                  ]}>
                  Transfer
                </Text>
                <Image
                  // source={IMAGES.notification_bell}
                  style={{
                    width: 20,
                    height: 20,
                    right: 0,
                    position: 'absolute',
                  }}
                />
              </View>
            </TouchableOpacity>
          </View>
          <TripDateView
            departDate={this.state.tripDepart}
            returnDate={this.state.tripReturn}
            departFlexibility={this.state.departFlexibility}
            returnFlexibility={this.state.returnFlexibility}
          />
        </View>
        <View
          style={{
            marginTop: -50,
            width: wp(90),
            alignSelf: 'center',
            borderRadius: 10,
            shadowColor: COLOR.VOILET,
            shadowOffset: {width: 2, height: 2},
            shadowOpacity: 0.2,
            backgroundColor: COLOR.WHITE,
            elevation: 3,
          }}>
          <View>
            <FlatList
              showsVerticalScrollIndicator={false}
              data={this.state.transferData}
              renderItem={({item}) => (
                <TouchableWithoutFeedback>
                  <View
                    style={{
                      alignSelf: 'center',
                      marginTop: 10,
                      backgroundColor: COLOR.WHITE,
                      width: wp(85),
                    }}>
                    <View
                      style={{
                        height: 50,
                        flex: 1,
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        alignSelf: 'center',
                        alignItems: 'center',
                      }}>
                      <Text style={[Styles.body_label, {width: null, flex: 1}]}>
                        {item.schedules[0].from}
                      </Text>
                      <TouchableOpacity
                        style={{
                          aspectRatio: 1,
                          height: 20,
                          marginHorizontal: 5,
                          alignItems: 'center',
                          justifyContent: 'center',
                        }}>
                        <Image
                          style={{
                            height: 20,
                            width: 20,
                          }}
                          source={IMAGES.edit}
                        />
                      </TouchableOpacity>
                      <TouchableOpacity
                        onPress={() =>
                          this.deleteTransfer(item.schedules[0].id)
                        }
                        style={{
                          aspectRatio: 1,
                          height: 20,
                          marginHorizontal: 5,
                          alignItems: 'center',
                          justifyContent: 'center',
                        }}>
                        <Image
                          style={{
                            height: 20,
                            width: 20,
                          }}
                          source={IMAGES.cross_icon}
                        />
                      </TouchableOpacity>
                    </View>
                  </View>
                </TouchableWithoutFeedback>
              )}
              numColumns={1}
              ListEmptyComponent={() =>
                !this.state.transferData.length ? (
                  <Text
                    style={{
                      textAlign: 'center',
                      color: COLOR.VOILET,
                      marginVertical: 40,
                    }}>
                    You did not add any Transfer
                  </Text>
                ) : null
              }
              keyExtractor={(item, index) => index.toString()}
            />
            <View
              style={[
                Styles.line_view,
                {width: wp(80), alignSelf: 'center', marginVertical: 10},
              ]}
            />
            <TouchableOpacity
              style={{
                width: wp(85),
                alignSelf: 'center',
                height: 50,
                alignItems: 'center',
                justifyContent: 'center',
                backgroundColor: COLOR.WHITE,
                borderRadius: 10,
                marginBottom: 20,
                borderColor: COLOR.VOILET,
                borderWidth: 1,
                marginTop: 40,
              }}
              onPress={() => {
                this.setState({modalVisible: false}, (val) => {
                  this.props.navigation.navigate(
                    'AddnewTransfer',
                    this.props.route.params,
                  );
                });
              }}>
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <Image
                  style={{
                    width: 25,
                    height: 25,
                    borderRadius: 10,

                    marginRight: 5,
                  }}
                  source={IMAGES.add_plus}
                />
                <Text
                  style={[
                    Styles.button_font,
                    {
                      color: COLOR.BLACK,
                      width: null,
                      textAlign: 'center',
                    },
                  ]}>
                  Add New Transfer
                </Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
        <TouchableOpacity
          style={{
            width: wp(90),
            alignSelf: 'center',
            height: 50,
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: COLOR.GREEN,
            borderRadius: 10,
            bottom: 20,
            position: 'absolute',
          }}
          onPress={() => {
            if (!this.state.isEdit) {
              if (screens[index + 1]) {
                this.props.navigation.navigate(screens[index + 1], {
                  screens: screens,
                  index: index + 1,
                  id: id,
                  depart: this.state.depart,
                  return: this.state.return,
                });
              } else if (this.state.isPackage) {
                this.props.navigation.goBack();
              } else {
                this.props.navigation.navigate('HomeStack');
              }
            } else {
              this.props.navigation.goBack()
            }
          }}>
          <Text
            style={[
              Styles.button_font,
              {
                color: COLOR.WHITE,
                width: null,
                textAlign: 'center',
              },
            ]}>
            Next
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}
