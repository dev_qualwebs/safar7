import {View, Image, Text} from 'react-native';
import React from 'react';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import COLOR from '../../styles/Color';
import Styles from '../../styles/Styles';
import {
  FlatList,
  TouchableOpacity,
  TouchableWithoutFeedback,
} from 'react-native';
import IMAGES from '../../styles/Images';
import TopTabBar from '../home/trip/TabBarView';
import {T_SCHEDULE_FLIGHT_TAB} from '../../../helper/Constants';
import FONTS from '../../styles/Fonts';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import TripDateView from '../../commonView/TripDateView';

export default class AddnewTransfer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedIndex: null,
      selectedTransfer: null,
      selectTransport: [
        {
          id: 1,
          name: 'Flight',
          image: IMAGES.plane,
        },
        {
          id: 2,
          name: 'Train',
          image: IMAGES.train,
        },
        {
          id: 3,
          name: 'Bus',
          image: IMAGES.bus_front,
        },
        {
          id: 4,
          name: 'Tram',
          image: IMAGES.train,
        },
        {
          id: 5,
          name: 'Taxi',
          image: IMAGES.car_image,
        },
        {
          id: 6,
          name: 'Subway',
          image: IMAGES.bus_front,
        },
        {
          id: 7,
          name: 'Ferry',
          image: IMAGES.car_image,
        },
      ],
    };
  }

  selectTransport = (item) => {
    this.setState({
      selectedIndex: item.id,
      selectedTransfer: item,
    });
  };

  componentDidMount() {
    if (this.props.route.params.depart) {
      let data = this.props.route.params;
      this.setState({
        tripDepart: data.depart,
        tripReturn: data.return,
        departFlexibility: data.departFlexibility,
        returnFlexibility: data.returnFlexibility,
      });
    }
  }

  render() {
    const {selectedIndex} = this.state;
    return (
      <View style={{flex: 1, backgroundColor: COLOR.BACKGROUND}}>
        <View
          style={{
            height: 220,
            backgroundColor: COLOR.GREEN,
            borderBottomRightRadius: 15,
            borderBottomLeftRadius: 15,
          }}>
          <View
            style={[
              Styles.mainView,
              {
                height: 45,
                marginTop: getStatusBarHeight(true),
                justifyContent: 'center',
                alignSelf: 'center',
                alignItems: 'center',
                flexDirection: 'row',
              },
            ]}>
            <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
              <Image
                style={{height: 15, width: 15, tintColor: COLOR.WHITE}}
                source={IMAGES.back_arrow}
              />
            </TouchableOpacity>
            <TouchableOpacity>
              <View>
                <Text
                  style={[
                    Styles.heading_label,
                    {
                      textAlign: 'center',
                      color: COLOR.WHITE,
                      fontFamily: FONTS.FAMILY_BOLD,
                      marginVertical: 0,
                    },
                  ]}>
                  Transfer
                </Text>
                <Image
                  // source={IMAGES.notification_bell}
                  style={{
                    width: 20,
                    height: 20,
                    right: 0,
                    position: 'absolute',
                  }}
                />
              </View>
            </TouchableOpacity>
          </View>
          <TripDateView
            departDate={this.state.tripDepart}
            returnDate={this.state.tripReturn}
            departFlexibility={this.state.departFlexibility}
            returnFlexibility={this.state.returnFlexibility}
          />
        </View>
        <View
          style={{
            flex: 1,
            marginTop: -50,
            width: wp(90),
            alignSelf: 'center',
            borderRadius: 10,
            shadowColor: COLOR.VOILET,
            shadowOffset: {width: 2, height: 2},
            shadowOpacity: 0.2,
            backgroundColor: COLOR.WHITE,
            elevation: 3,
            paddingVertical: 10,
            alignItems: 'center',
          }}>
          <View>
            <FlatList
              style={{backgroundColor: COLOR.WHITE}}
              data={this.state.selectTransport}
              renderItem={({item}) => (
                <TouchableWithoutFeedback
                  onPress={() => this.selectTransport(item)}>
                  <View
                    style={{
                      borderRadius: 10,
                      borderWidth: 1,
                      height: 120,
                      margin: 10,
                      borderColor:
                        selectedIndex == item.id ? COLOR.GREEN : COLOR.GRAY,
                      backgroundColor:
                        selectedIndex == item.id ? '#05778F10' : COLOR.WHITE,
                      alignSelf: 'center',
                      alignItems: 'center',
                      justifyContent: 'center',
                      width: wp(38),
                    }}>
                    <Image
                      source={item.image}
                      style={{
                        height: 50,
                        width: 50,
                        marginVertical: 10,
                        tintColor:
                          selectedIndex == item.id ? COLOR.GREEN : COLOR.VOILET,
                      }}
                    />
                    <Text style={{marginBottom: 10, textAlign: 'center'}}>
                      {item.name}
                    </Text>
                  </View>
                </TouchableWithoutFeedback>
              )}
              horizontal={false}
              numColumns={2}
              showsVerticalScrollIndicator={false}
              showsHorizontalScrollIndicator={false}
              keyExtractor={(item, index) => index.toString()}
            />
          </View>
        </View>
        <TouchableOpacity
          style={{
            width: wp(85),
            alignSelf: 'center',
            height: 50,
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: COLOR.GREEN,
            borderRadius: 10,
            marginBottom: 20,
            marginTop: 20,
          }}
          onPress={() => {
            this.setState({modalVisible: false}, (val) => {
              this.props.navigation.navigate('TransferBetween', {
                ...this.props.route.params,
                selectedTransfer: this.state.selectedTransfer,
              });
            });
          }}>
          <Text
            style={[
              Styles.button_font,
              {
                color: COLOR.WHITE,
                width: null,
                textAlign: 'center',
              },
            ]}>
            Next
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}
