import {View, Image, Text} from 'react-native';
import React from 'react';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import COLOR from '../../styles/Color';
import Styles from '../../styles/Styles';
import {TextInput, TouchableOpacity} from 'react-native';
import IMAGES from '../../styles/Images';
import FONTS from '../../styles/Fonts';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import TripDateView from '../../commonView/TripDateView';
import Switch from '../../commonView/Switch';
import GooglePlacesTextInput from '../../commonView/GooglePlacesTextInput';
import SelectDate from '../calendar/SelectDate';
import API from '../../../api/Api';
import SimpleToast from 'react-native-simple-toast';

const api = new API();

export default class TransferBetween extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      tripId: null,
      selectedTransfer: null,
      roundTrip: false,
      tripDepart: null,
      tripReturn: null,
      departFlexibility: null,
      returnFlexibility: null,
      selectedTransfer: null,
      from: '',
      to: '',
      checkInDate: null,
      checkOutDate: null,
      roundTrip: false,
      selector: 0,
      openCalendar: false,
    };

    this.updateMasterState = this.updateMasterState.bind(this);
  }

  componentDidMount() {
    if (this.props.route.params.depart) {
      let data = this.props.route.params;
      this.setState({
        tripDepart: data.depart,
        tripReturn: data.return,
        departFlexibility: data.departFlexibility,
        returnFlexibility: data.returnFlexibility,
      });
    }

    if (this.props.route.params) {
      this.setState({
        selectedTransfer: this.props.route.params.selectedTransfer,
        tripId: this.props.route.params.id,
      });
    }
  }

  updateMasterState = (attr, value) => {
    this.setState({
      [attr]: value,
    });
  };

  addTransfer = async () => {
    if (this.state.from == '' || this.state.from == null) {
      SimpleToast.show('Enter From ');
    } else if (this.state.to == '' || this.state.to == null) {
      SimpleToast.show('Enter to');
    } else if (this.state.tripDepart && this.state.checkInDate == null) {
      SimpleToast.show('Enter Checkin date');
    } else if (this.state.roundTrip && this.state.checkOutDate == null) {
      SimpleToast.show('Enter Return Date');
    } else {
      let payload = JSON.stringify({
        transfer_mode_id: this.state.selectedTransfer.id,
        from: this.state.from,
        to: this.state.to,
        round_trip: this.state.roundTrip,
        depart: this.state.checkInDate,
        return: this.state.checkOutDate,
      });
      api
        .addTransfer(this.state.tripId, payload)
        .then((json) => {
          SimpleToast.show('Added Successfully');
          this.props.navigation.pop(2);
        })
        .catch((error) => {
          console.log(error);
          SimpleToast.show(error.response.data.message);
        });
    }
  };

  render() {
    let selectedTransfer = this.state.selectedTransfer;
    return (
      <View style={{flex: 1, backgroundColor: COLOR.BACKGROUND}}>
        <View
          style={{
            height: 220,
            backgroundColor: COLOR.GREEN,
            borderBottomRightRadius: 15,
            borderBottomLeftRadius: 15,
          }}>
          <View
            style={[
              Styles.mainView,
              {
                height: 45,
                marginTop: getStatusBarHeight(true),
                justifyContent: 'center',
                alignSelf: 'center',
                alignItems: 'center',
                flexDirection: 'row',
              },
            ]}>
            <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
              <Image
                style={{height: 15, width: 15, tintColor: COLOR.WHITE}}
                source={IMAGES.back_arrow}
              />
            </TouchableOpacity>
            <TouchableOpacity>
              <View>
                <Text
                  style={[
                    Styles.heading_label,
                    {
                      textAlign: 'center',
                      color: COLOR.WHITE,
                      fontFamily: FONTS.FAMILY_BOLD,
                      marginVertical: 0,
                    },
                  ]}>
                  Transfer
                </Text>
                <Image
                  // source={IMAGES.notification_bell}
                  style={{
                    width: 20,
                    height: 20,
                    right: 0,
                    position: 'absolute',
                  }}
                />
              </View>
            </TouchableOpacity>
          </View>
          <TripDateView
            departDate={this.state.tripDepart}
            returnDate={this.state.tripReturn}
            departFlexibility={this.state.departFlexibility}
            returnFlexibility={this.state.returnFlexibility}
          />
        </View>
        <View
          style={{
            marginTop: -50,
            width: wp(90),
            alignSelf: 'center',
            borderRadius: 10,
            shadowColor: COLOR.VOILET,
            shadowOffset: {width: 2, height: 2},
            shadowOpacity: 0.2,
            backgroundColor: COLOR.WHITE,
            elevation: 3,
          }}>
          <View>
            <View
              style={{
                width: wp(85),
                marginVertical: 20,
                alignSelf: 'center',
                paddingHorizontal: 10,
              }}>
              <Text
                style={[
                  Styles.subheading_label,
                  {
                    marginBottom: 20,
                    textAlign: 'center',
                    color: COLOR.GREEN,
                  },
                ]}>
                {selectedTransfer ? selectedTransfer.name : ''}
              </Text>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  height: 45,
                  zIndex: 1000,
                }}>
                <Image
                  style={{height: 25, width: 25, marginRight: 20}}
                  source={IMAGES.pinpoint}
                  resizeMode={'contain'}
                />
                <View>
                  <Text style={Styles.small_label}>From</Text>
                  <GooglePlacesTextInput
                    value={this.state.from}
                    onValueChange={(text) =>
                      this.setState({
                        from: text,
                      })
                    }
                  />
                </View>
              </View>
              <View
                style={[Styles.line_view, {width: wp(80), marginVertical: 20}]}
              />
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  height: 45,
                  zIndex: 990,
                }}>
                <Image
                  style={{height: 25, width: 25, marginRight: 20}}
                  source={IMAGES.pinpoint}
                  resizeMode={'contain'}
                />
                <View>
                  <Text style={Styles.small_label}>To</Text>
                  <GooglePlacesTextInput
                    value={this.state.to}
                    onValueChange={(text) =>
                      this.setState({
                        to: text,
                      })
                    }
                  />
                </View>
              </View>
              {this.state.tripDepart ? (
                <>
                  <View
                    style={[
                      Styles.line_view,
                      {width: wp(80), marginVertical: 20},
                    ]}
                  />
                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                    }}>
                    <Image
                      style={{height: 25, width: 25, marginRight: 20}}
                      source={IMAGES.return_date}
                      resizeMode={'contain'}
                    />
                    <View style={{flex: 1}}>
                      <Text
                        style={[
                          Styles.small_label,
                          {width: null, alignSelf: 'flex-start'},
                        ]}>
                        Departure Date
                      </Text>
                      <TouchableOpacity
                        onPress={() =>
                          this.setState({
                            selector: 0,
                            openCalendar: true,
                          })
                        }>
                        <Text
                          style={{
                            paddingVertical: 5,
                            fontSize: FONTS.MEDIUM,
                            fontFamily: FONTS.FAMILY_SEMIBOLD,
                          }}>
                          {this.state.checkInDate
                            ? this.state.checkInDate
                            : 'Select Date'}
                        </Text>
                      </TouchableOpacity>
                    </View>
                    <View style={{width: 90}}>
                      <Text
                        style={[
                          Styles.small_label,
                          {
                            width: null,
                            alignSelf: 'flex-end',
                            color: COLOR.VOILET,
                          },
                        ]}>
                        Round Trip?
                      </Text>
                      <Switch
                        isChecked={this.state.roundTrip}
                        onCheckedListener={() =>
                          this.setState({roundTrip: !this.state.roundTrip})
                        }
                      />
                    </View>
                  </View>
                  {this.state.roundTrip ? (
                    <>
                      <View
                        style={[
                          Styles.line_view,
                          {width: wp(80), marginVertical: 20},
                        ]}
                      />
                      <View
                        style={{
                          flexDirection: 'row',
                          justifyContent: 'space-between',
                        }}>
                        <Image
                          style={{height: 30, width: 30, marginRight: 20}}
                          source={IMAGES.departure_date}
                          resizeMode={'contain'}
                        />
                        <View>
                          <Text
                            style={[
                              Styles.small_label,
                              {width: null, alignSelf: 'flex-start'},
                            ]}>
                            Return Date
                          </Text>
                          <TouchableOpacity
                            onPress={() =>
                              this.setState({
                                selector: 1,
                                openCalendar: true,
                              })
                            }>
                            <Text
                              style={[
                                {
                                  width: widthPercentageToDP(90),
                                  paddingVertical: 5,
                                  fontSize: FONTS.MEDIUM,
                                  fontFamily: FONTS.FAMILY_SEMIBOLD,
                                },
                              ]}>
                              {this.state.checkOutDate
                                ? this.state.checkOutDate
                                : 'Select Date'}
                            </Text>
                          </TouchableOpacity>
                        </View>
                      </View>
                    </>
                  ) : null}
                </>
              ) : null}
            </View>
          </View>
        </View>
        <TouchableOpacity
          onPress={this.addTransfer.bind(this)}
          style={{
            width: wp(85),
            alignSelf: 'center',
            height: 50,
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: COLOR.GREEN,
            borderRadius: 10,
            bottom: 20,
            position: 'absolute',
          }}>
          <Text
            style={[
              Styles.button_font,
              {
                color: COLOR.WHITE,
                width: null,
                textAlign: 'center',
              },
            ]}>
            Next
          </Text>
        </TouchableOpacity>
        {this.state.openCalendar ? (
          <SelectDate
            allowRangeSelection={false}
            selector={this.state.selector}
            updateMasterState={this.updateMasterState}
            props={this.props.props}
            modal={true}
            minDate={this.state.tripDepart}
            maxDate={this.state.tripReturn}
          />
        ) : null}
      </View>
    );
  }
}
