import React, {useRef, useState, useEffect} from 'react';
import {
  View,
  StyleSheet,
  Text,
  TouchableOpacity,
  Image,
  Platform,
  Modal,
} from 'react-native';
import Permissions from 'react-native-permissions';
import PDFScanner from '@woonivers/react-native-document-scanner';
import COLOR from '../../styles/Color';
import IMAGES from '../../styles/Images';
import ActivityIndicator from '../../commonView/ActivityIndicator';

export const ScanDocument = (props) => {
  const pdfScannerElement = useRef(null);
  const [data, setData] = useState({});
  const [allowed, setAllowed] = useState(false);
  const [loading, setLoading] = useState(false);
  // = (props)=>

  //   node_modules/@woonivers/react-native-document-scanner/ios
  // Edit the file RNPdfScannerManager.m. Change this line:

  // - (dispatch_queue_t)methodQueue
  // {
  //     return dispatch_get_main_queue();
  // }

  // useEffect(() => {
  //   async function requestCamera() {
  //     const result = await Permissions.request(Platform.OS === "android" ? "android.permission.CAMERA" : "ios.permission.CAMERA")
  //     if (result === "granted") setAllowed(true)
  //   }
  //   requestCamera()
  // }, [])

  function handleOnPressRetry() {
    setData({});
  }
  function handleOnPress() {
    pdfScannerElement.current.capture();
  }
  //   if (!allowed) {
  //     console.log("You must accept camera permission")
  //     return (<View style={styles.permissions}>
  //       <Text>You must accept camera permission</Text>
  //     </View>)
  //   }
  // if (data.croppedImage) {
  //   console.log('data', data);
  //   return (
  //      <React.Fragment>
  //       <Modal
  //     animationType={'slide'}
  //     transparent={!props.modalVisible}
  //     style={{backgroundColor: COLOR.BLACK}}
  //     visible={props.modalVisible}
  //     onRequestClose={() => {
  //       console.log('Modal has been closed.');
  //     }}>
  //     <View>
  //       <Image source={{uri: data.croppedImage}} style={styles.preview} />
  //       <View
  //         style={{
  //           flexDirection: 'row',
  //           alignItems: 'center',
  //           justifyContent: 'space-around',
  //         }}>
  //         <TouchableOpacity onPress={handleOnPressRetry} style={styles.button}>
  //           <Text style={styles.buttonText}>Retry</Text>
  //         </TouchableOpacity>
  //         <TouchableOpacity onPress={props.onDoneAction}
  //           style={[styles.button, {marginLeft: 15}]}>
  //           <Text style={styles.buttonText}>Done</Text>
  //         </TouchableOpacity>
  //       </View>
  //     </View>
  //     </Modal>
  //      </React.Fragment>
  //   );
  // }
  return (
    <React.Fragment>
      {/* <ActivityIndicator loading={loading}/> */}
    <Modal
      animationType={'slide'}
      transparent={!props.modalVisible}
      style={{backgroundColor: COLOR.BLACK}}
      visible={props.modalVisible}
      onRequestClose={() => {
        console.log('Modal has been closed.');
      }}>
        <PDFScanner
          ref={pdfScannerElement}
          style={styles.scanner}
          onPictureTaken={(data)=>{
            setLoading(true);
            props.onDoneAction(data.croppedImage);
          }}
          overlayColor={'rgba(5, 119, 143, 0.6)'}
          enableTorch={false}
          quality={0.5}
          detectionCountBeforeCapture={5}
          detectionRefreshRateInMS={50}
          useBase64
        />
        {/* <TouchableOpacity onPress={handleOnPress} style={styles.button}>
        <Text style={styles.buttonText}>Take picture</Text>
      </TouchableOpacity> */}

<View style={{position:'absolute',top:45,left:15}}>
        <TouchableOpacity
        onPress={()=>props.onBackAction()}
        >
        <Image source={IMAGES.back_arrow} style={{height:25,width:25,tintColor:COLOR.WHITE,}}/>
        </TouchableOpacity>
        </View>
      
    </Modal>
    </React.Fragment>
  );
};

const styles = StyleSheet.create({
  scanner: {
    flex: 1,
    // aspectRatio: undefined
    aspectRatio: 1,
  },
  button: {
    alignSelf: 'center',
    position: 'absolute',
    bottom: 32,
  },
  buttonText: {
    backgroundColor: 'rgba(245, 252, 255, 0.7)',
    fontSize: 32,
  },
  preview: {
    flex: 1,
    width: '100%',
    resizeMode: 'contain',
    backgroundColor: 'rgba(0, 0, 0, 1)',
  },
  permissions: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
