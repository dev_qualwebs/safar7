import React from 'react';
import {
  View,
  Text,
  Image,
  SafeAreaView,
  StyleSheet,
  Share,
  ImageBackground,
  Button,
} from 'react-native';
import {ScrollView, TouchableWithoutFeedback} from 'react-native';
import IMAGES from '../../styles/Images';
import Styles from '../../styles/Styles';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
  widthPercentageToDP,
} from 'react-native-responsive-screen';
import COLOR from '../../styles/Color';
import NavigationBar from '../../commonView/NavigationBar';
import {connect} from 'react-redux';
import ProfileAction from '../../../redux/action/ProfileAction';
import Auth from '../../../auth';
import {AS_USER_DETAIL, AS_USER_TOKEN} from '../../../helper/Constants';
import {CommonActions} from '@react-navigation/native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import FONTS from '../../styles/Fonts';

const auth = new Auth();
class ProfileScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      tripData: [],
      userDetail: null,
    };
    this.logoutUser = this.logoutUser.bind(this);
    this.handleSelection = this.handleSelection.bind(this);
  }

  componentDidMount() {
    this.props.getUserDetail();
  }

  componentWillReceiveProps(props) {
    if (props.userDetail != this.props.userDetail) {
      this.setState({
        userDetail: props.userDetail,
      });
    }
  }

  handleSelection = async (props) => {
    switch (props.name) {
      case 'Profile':
        this.props.navigation.navigate('ProfileDetail', {val: 0});
        break;
      case 'Travellers':
        this.props.navigation.navigate('ProfileTraveller');
        break;
      case 'Share':
        try {
          const result = await Share.share({
            message: 'SAFAR APP URL',
          });

          if (result.action === Share.sharedAction) {
            if (result.activityType) {
              // shared with activity type of result.activityType
            } else {
              // shared
            }
          } else if (result.action === Share.dismissedAction) {
            // dismissed
          }
        } catch (error) {
          alert(error.message);
        }
        break;
      case 'Settings':
        this.props.navigation.navigate('ProfileSetting');
        break;
      case 'Support':
        this.props.navigation.navigate('ProfileSupport');
        break;
      default:
        break;
    }
  };

  logoutUser = () => {
    this.props.logoutUser();
    auth.remove(AS_USER_TOKEN);
    auth.remove(AS_USER_DETAIL);
    auth.remove('Rooms');

    this.props.navigation.dispatch(
      CommonActions.reset({
        index: 0,
        routes: [{name: 'LoginScreen'}],
      }),
    );
  };

  commonView = (props) => {
    return (
      <TouchableWithoutFeedback onPress={() => this.handleSelection(props)}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            width: wp(90),
            alignItems: 'center',
            alignSelf: 'center',
          }}>
          <Image
            style={{height: 25, width: 25, marginRight: 15}}
            source={props.image}
            resizeMode={'contain'}
          />
          <Text
            style={[
              Styles.body_label,
              {width: null, textAlign: 'left', flex: 1},
            ]}>
            {props.name}
          </Text>
          <Image style={{height: 15, width: 15}} source={IMAGES.right_arrow} />
        </View>
      </TouchableWithoutFeedback>
    );
  };

  render() {
    let {userDetail} = this.state;

    return (
      <View style={Styles.container}>
        <SafeAreaView style={Styles.container}>
          <NavigationBar
            prop={this.props}
            // right_image={IMAGES.notification_bell}
            isMainHeading={true}
            navHeight={45}
            name={
              userDetail && userDetail.first_name
                ? userDetail.first_name + ' ' + userDetail.last_name
                : 'Profile'
            }
            subtitle={userDetail ? userDetail.email : ''}
          />
          <ScrollView
            bounces={false}
            showsVerticalScrollIndicator={false}
            contentContainerStyle={{flexGrow: 1}}>
            <View style={[Styles.mainView, {flex: 1}]}>
              <TouchableWithoutFeedback
                onPress={() => this.props.navigation.navigate('CreditRewards')}>
                <View
                  style={{
                    height: 180,
                    width: wp(90),
                    marginBottom: 50,
                    marginTop: 20,
                    alignSelf: 'center',
                    borderRadius: 0,
                    backgroundColor: COLOR.WHITE,
                  }}>
                  <ImageBackground
                    source={IMAGES.credit_card_view}
                    resizeMode={'cover'}
                    style={[Styles.backgroundImage, {borderRadius: 20}]}
                  />
                  <View style={{top: 10, left: 20, position: 'absolute'}}>
                    <Text
                      style={[
                        Styles.small_label,
                        {
                          width: null,
                          color: COLOR.WHITE,
                          alignSelf: 'flex-start',
                        },
                      ]}>
                      CREDIT
                    </Text>
                    <Text
                      style={[
                        Styles.heading_label,
                        {width: null, color: COLOR.WHITE, marginTop: 5},
                      ]}>
                      {userDetail && userDetail.credit
                        ? userDetail.credit
                        : '0'}
                    </Text>
                  </View>
                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      right: 20,
                      bottom: 20,
                      position: 'absolute',
                      alignSelf: 'flex-start',
                    }}>
                    <Text
                      style={[
                        Styles.small_label,
                        {
                          width: null,
                          color: COLOR.WHITE,
                        },
                      ]}>
                      USE CREDIT
                    </Text>
                    <Image
                      source={IMAGES.right_big_arrow}
                      style={{
                        height: 15,
                        width: 15,
                        marginLeft: 5,
                        tintColor: COLOR.WHITE,
                      }}
                    />
                  </View>
                </View>
              </TouchableWithoutFeedback>
              <this.commonView name={'Profile'} image={IMAGES.circleProfile} />
              <View
                style={[Styles.line_view, {marginVertical: 20, width: wp(90)}]}
              />
              <this.commonView name={'Travellers'} image={IMAGES.travellers} />
              <View
                style={[Styles.line_view, {marginVertical: 20, width: wp(90)}]}
              />
              <this.commonView name={'Share'} image={IMAGES.share} />
              <View
                style={[Styles.line_view, {marginVertical: 20, width: wp(90)}]}
              />
              <this.commonView name={'Settings'} image={IMAGES.tool} />
              <View
                style={[Styles.line_view, {marginVertical: 20, width: wp(90)}]}
              />
              <this.commonView name={'Support'} image={IMAGES.support} />
              <View
                style={[Styles.line_view, {marginVertical: 20, width: wp(90)}]}
              />
            </View>

            <Text
              style={[
                {
                  fontFamily: FONTS.FAMILY_REGULAR,
                  fontSize: FONTS.SMALL,
                  alignSelf: 'center',
                  color: COLOR.VOILET,
                  color: COLOR.VOILET,
                  textAlign: 'center',
                  flexDirection: 'row',
                  alignItems: 'center',
                  textAlign: 'center',
                  justifyContent: 'center',
                },
              ]}>
              <TouchableOpacity
                onPress={() => {
                  this.props.navigation.navigate('PolicyPage', {
                    screenTitle: 'Privacy Policy',
                  });
                }}>
                <Text
                  style={{
                    fontFamily: FONTS.FAMILY_REGULAR,
                    fontSize: FONTS.SMALL,
                    alignSelf: 'center',
                    color: COLOR.VOILET,
                    color: COLOR.VOILET,
                    textAlign: 'center',
                    flexDirection: 'row',
                    alignItems: 'center',
                    textAlign: 'center',
                    justifyContent: 'center',
                    marginBottom: -3,
                  }}>
                  Privacy Policy{'  '}
                </Text>
              </TouchableOpacity>
              |
              <TouchableOpacity
                onPress={() => {
                  this.props.navigation.navigate('PolicyPage', {
                    screenTitle: 'Terms and Condition',
                  });
                }}>
                <Text
                  style={{
                    fontFamily: FONTS.FAMILY_REGULAR,
                    fontSize: FONTS.SMALL,
                    alignSelf: 'center',
                    color: COLOR.VOILET,
                    color: COLOR.VOILET,
                    textAlign: 'center',
                    flexDirection: 'row',
                    alignItems: 'center',
                    textAlign: 'center',
                    justifyContent: 'center',
                    marginBottom: -3,
                  }}>
                  {'  '}Terms of Service
                </Text>
              </TouchableOpacity>
            </Text>
            {/* </View> */}
            <Button
              onPress={this.logoutUser}
              source={[Styles.button_font]}
              color={COLOR.RED}
              title={'Sign out'}
              style={{textDecorationLine: 'underline'}}
            />
          </ScrollView>
        </SafeAreaView>
      </View>
    );
  }
}

const classStyle = StyleSheet.create({
  flatlist_view: {
    marginTop: 0,
    width: wp(100),
    marginBottom: 0,
    height: hp(50),
    marginHorizontal: 10,
  },
  flatlist_image_view: {
    height: hp(50),
    width: wp(70),
    marginRight: 0,
  },
  offer_view: {
    marginVertical: 0,
    width: wp(100),
    marginBottom: 20,
    height: 100,
    marginHorizontal: 10,
  },
});

mapStateToProps = (state, ownProps) => {
  return {
    userDetail: state.profileReducer.userDetail,
  };
};

mapDispatchToProps = (dispatch) => {
  return {
    getUserDetail: (val) => {
      dispatch(ProfileAction.getUserInfo(val));
    },
    logoutUser: () => {
      dispatch(ProfileAction.logoutUser());
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ProfileScreen);
