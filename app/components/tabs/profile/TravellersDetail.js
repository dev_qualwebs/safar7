import React, {Component} from 'react';
import {
  SafeAreaView,
  Alert,
  View,
  Image,
  TouchableOpacity,
  Text,
} from 'react-native';
import Styles from '../../styles/Styles';
import {T_PROFILE_TAB, T_TRAVELLER_TAB} from '../../../helper/Constants';
import TopTabBar from '../home/trip/TabBarView';
import IMAGES from '../../styles/Images';
import ProfileAction from '../../../redux/action/ProfileAction';
import {connect} from 'react-redux';
import COLOR from '../../styles/Color';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import FONTS from '../../styles/Fonts';
import {StyleSheet} from 'react-native';
import API from '../../../api/Api';
import SimpleToast from 'react-native-simple-toast';

const api = new API();
class TravellersDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      index: 0,
      modalVisible: false,
      singleTravellerData: null,
      id: null,
    };
  }

  async componentDidMount() {
    let params = this.props.route.params;
    if (params.data) {
      this.getSingleTraveller(params.data.id);
    }
  }

  getSingleTraveller = async (id) => {
    let that = this;
    api
      .getSingleTraveller(id)
      .then((json) => {
        console.log(json.data.response);
        that.setState({
          singleTravellerData: json.data.response,
        });
      })
      .catch(function (error) {
        console.log(error.response.data);
      });
  };

  addTraveller = async (data) => {
    var that = this;
    if (this.state.singleTravellerData) {
      this.updateTraveller(data, this.state.singleTravellerData.id);
    } else {
      api
        .addTraveller(data)
        .then((json) => {
          console.log(json.data.response);
          that.getSingleTraveller(json.data.response.id);
          this.props.getTravellers();
          that.setState({
              id: json.data.response.id,
          });
            this.props.navigation.goBack();
        })
        .catch(function (error) {
          console.log(error.response.data);
          SimpleToast.show(error.response.data.message);
        });
    }
  };

  updateTraveller = async (data, val) => {
    let that = this;
    api
      .updateTravellerDetails(data, val)
      .then((json) => {
        SimpleToast.show('Details updated');
        that.getSingleTraveller(val);
        this.props.getTravellers();
        that.setState({
          id: val,
        });
      })
      .catch(function (error) {
        console.log(error.response.data);
        SimpleToast.show(error.response.data.message);
      });
  };

  render() {
    return (
      <SafeAreaView style={Styles.container}>
        <View style={{marginBottom: 0}}>
          <View
            style={[
              Styles.mainView,
              {
                height: 50,
                justifyContent: 'space-between',
                alignSelf: 'center',
                alignItems: 'center',
                flexDirection: 'row',
                width: wp(98),
              },
            ]}>
            <TouchableOpacity
              style={{
                height: 50,
                width: 45,
                justifyContent: 'center',
                alignItems: 'center',
              }}
              onPress={() => {
                {
                  this.props.navigation.goBack();
                }
              }}>
              <Image
                style={{
                  height: 15,
                  width: 15,
                }}
                source={IMAGES.back_arrow}
              />
            </TouchableOpacity>

            <View>
              <Text
                style={[
                  Styles.subheading_label,
                  {
                    textAlign: 'center',
                    color: COLOR.BLACK,
                    fontSize: 20,
                    fontFamily: FONTS.FAMILY_BOLD,
                    width: null,
                    flex: 1,
                    marginTop: 15,
                  },
                ]}>
                  {'Manage Travellers'}
              </Text>
            </View>
            <View style={styles.skipView} />
          </View>
          <View
            style={{
              borderBottomColor: COLOR.GRAY,
              borderBottomWidth: 1,
            }}
          />
        </View>
        <TopTabBar
          currentTab={T_TRAVELLER_TAB}
          props={this.props}
          id={this.state.id}
          addTraveller={this.addTraveller.bind(this)}
          singleTravellerData={this.state.singleTravellerData}
        />
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  skipView: {
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
    height: 50,
    width: 45,
  },
});

const mapStateToProps = (state, ownProps) => {
  return {
    singleTravellerData: state.profileReducer.singleTravellerData,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getSingleTraveller: (val) => {
      dispatch(ProfileAction.getSingleTraveller(val));
    },
    getTravellers: () => {
      dispatch(ProfileAction.getTravellers());
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(TravellersDetail);
