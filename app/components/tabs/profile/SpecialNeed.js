import Styles from '../../styles/Styles';
import React from 'react';
import {SafeAreaView, View, Text, Image, StyleSheet, Alert} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview';
import NavigationBar from '../../commonView/NavigationBar';
import {
  FlatList,
  TouchableWithoutFeedback,
  TouchableOpacity,
  TextInput,
} from 'react-native';
import IMAGES from '../../styles/Images';
import COLOR from '../../styles/Color';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import {connect} from 'react-redux';
import ProfileAction from '../../../redux/action/ProfileAction';
import SimpleToast from 'react-native-simple-toast';
import { DoneButtonKeyboard } from '../../commonView/CommonView';

class SpecialNeeds extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedItem: [],
      specialNeedData: this.props.route.params.specialNeeds.map((v) => {
        v.comment = '';
        v.nested_need_id = 0;
        return v;
      }),
      motilityData : [{name:'Wheelchair Ramp (WCHR) needed',id:1},{name:'Wheelchair Steps (WCHS) needed',id:2},{name:'Wheelchair Carry (WCHC) needed',id:3}],
      userDetail: this.props.route.params.userDetail,
    };
    this.handleNestedSelection = this.handleNestedSelection.bind(this);
  }

  componentDidMount() {
    if (this.props.route.params.selectedSpecialNeeds) {
      let data = this.props.route.params.selectedSpecialNeeds;
      let selected = this.state.specialNeedData.map((value) => {
        let modifiedValue = value;
        let mIndex = data.findIndex((x) => x.id == value.id);
        if (mIndex != -1) {
          modifiedValue.comment = data[mIndex].comment;
          modifiedValue.nested_need_id = data[mIndex].nested_need_id;
          return modifiedValue;
        } else {
          return value;
        }
      });
      this.setState({
        selectedItem: data,
        specialNeedData: selected,
      });
    }
  }

  componentDidUpdate(prevProps) {
    if (this.props.route.params != prevProps.route.params) {
      let data = this.props.route.params.selectedSpecialNeeds;
      let selected = this.state.specialNeedData.map((value) => {
        let modifiedValue = value;
        let mIndex = data.findIndex((x) => x.id == value.id);
        if (mIndex != -1) {
          modifiedValue.comment = x.comment;
          modifiedValue.nested_need_id = x.nested_need_id;
          return modifiedValue;
        } else {
          return value;
        }
      });
      this.setState({
        selectedItem: data,
        specialNeedData: selected,
      });
    }
  }

  handleAddSpecialNeed = () => {

    let newData = this.state.selectedItem.map((value) => {
      let specialData = this.state.specialNeedData.find(
        (v) => v.id == value.id,
      );
      let newObj = {
        id: value.id,
        comment: specialData ? specialData.comment : '',
        nested_need_id : value.nested_need_id,
      };
      return newObj;
    });

    let validated = true;
    newData.map((val)=>{
      console.log(val)
      if(val.comment == null || val.comment == ''){
        SimpleToast.show('Please enter details for special needs.');
        validated = false;
      } else if (val.id == 2 && val.nested_need_id == 0){
        validated = false;
        SimpleToast.show('Please select one the options from motility disability.');
        return;
      }
    })

if(validated){
    this.props.route.params.updateSpecialNeeds(newData);
    this.props.navigation.goBack();
}
  };

  handleSelection = (item) => {
    let data = this.state.selectedItem;
    let index = data.findIndex((value) => value.id === item.id);
    if (index != -1) {
      data.splice(index, 1);
    } else {
      if(item.id == 2){
      item.nested_need_id = 1;
      data.push(item);
      }else {
      data.push(item);
    }
    }
    this.setState({
      selectedItem: data,
    });
  };

  handleNestedSelection = (item) => {
    let data = this.state.selectedItem;
    let index = data.findIndex((value) => value.nested_need_id === item.id);
    if (index != -1) {
      data[index].nested_need_id = 0;
    } else {
      data.map((val)=>{
        if(val.id == 2){
          val.nested_need_id = item.id;
        }
      })
    }
    this.setState({
      selectedItem: data,
    });
  };

  NestedView = (props) => {
    return(
      <View style={[style.tick_view, {alignItems: 'center',marginLeft:40}]}>
      <TouchableOpacity
          onPress={() => {
          this.handleNestedSelection(props.item)
          }}
          style={{
            height: 25,
            width: 25,
            borderRadius: 5,
            backgroundColor:
                this.state.selectedItem.findIndex(
              (v) => v.nested_need_id == props.item.id,
            ) != -1
              ? COLOR.GREEN
              : COLOR.WHITE,
          borderColor: COLOR.GRAY,
            justifyContent: 'center',
            alignItems: 'center',
            borderWidth: 1,
          }}>
        <Image
            resizeMode={'contain'}
            style={{height: 20, width: 20, alignSelf: 'center'}}
            source={IMAGES.tick_image}
        />
      </TouchableOpacity>
      <Text style={{flex: 1, textAlign: 'left', marginLeft: 15}}>{props.item.name}</Text>
    </View>
    );
  }

  render() {
    return (
      <SafeAreaView style={Styles.container}>
        <KeyboardAwareScrollView
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}>
          <NavigationBar
            prop={this.props}
            left_image={IMAGES.back_arrow}
            height={40}
            name={'Special Needs'}
          />
          <FlatList
            style={{flex: 0}}
            data={this.state.specialNeedData}
            renderItem={({item, index}) => (
              <TouchableWithoutFeedback>
                <View style={style.flatlist_view}>
                  <View style={[style.tick_view, {alignItems: 'center'}]}>
                    <TouchableOpacity
                        onPress={() => {
                          this.handleSelection(item);
                        }}
                        style={{
                          height: 30,
                          width: 30,
                          borderRadius: 5,
                          backgroundColor:
                              this.state.selectedItem.findIndex(
                            (v) => v.id === item.id,
                          ) != -1
                            ? COLOR.GREEN
                            : COLOR.WHITE,
                        borderColor: COLOR.GRAY,
                          justifyContent: 'center',
                          alignItems: 'center',
                          borderWidth: 1,
                        }}>
                      <Image
                          resizeMode={'contain'}
                          style={{height: 20, width: 20, alignSelf: 'center'}}
                          source={IMAGES.tick_image}
                      />
                    </TouchableOpacity>
                    <Image resizeMode='contain' source={item.name == 'Medical conditions' ? IMAGES.lungs : item.name == 'Motility disability' ? IMAGES.motility_disability : item.name == 'Vision and hearing disability' ? IMAGES.specs : item.name == 'Pregnancy' ? IMAGES.pregnant : item.name == 'Dietary requirement' ? IMAGES.raddish : item.name == 'Motion or Sea sickness' ? IMAGES.dizzy : IMAGES.fish_cross} style={{height:20,width:20,marginLeft:15}}/>
                    <Text style={{flex: 1, textAlign: 'left', marginLeft: 15}}>{item.name}</Text>
                  </View>
                  { item.name == 'Motility disability' &&  this.state.selectedItem.findIndex(
                           (v) => v.id === item.id,
                         ) != -1 ? 
                
                <FlatList
                style={{flex: 0}}
                data={this.state.motilityData}
                renderItem={this.NestedView}
                  numColumns={'1'}
                  removeClippedSubviews={false}
                  showsVerticalScrollIndicator={false}
                  showsHorizontalScrollIndicator={false}
                  keyExtractor={(item, index) => index.toString()}
                />
                 : null
                  }
                  <TextInput
                  inputAccessoryViewID={'uniqueID'}
                    style={[Styles.body_label, {paddingHorizontal: 10}]}
                    placeholderTextColor={COLOR.LIGHT_TEXT_COLOR}
                    placeholder={'Specify more'}
                    value={item.comment}
                    onChangeText={(text) => {
                      let data = this.state.specialNeedData;
                      data[index] = {
                        ...this.state.specialNeedData[index],
                        comment: text,
                      };
                      this.setState({
                        specialNeedData: data,
                      });
                    }}
                    multiline={true}
                  />
                  <DoneButtonKeyboard />
                </View>
              </TouchableWithoutFeedback>
            )}
            numColumns={'1'}
            removeClippedSubviews={false}
            showsVerticalScrollIndicator={false}
            showsHorizontalScrollIndicator={false}
            keyExtractor={(item, index) => index.toString()}
          />
          <TouchableOpacity
            onPress={() => this.handleAddSpecialNeed()}
            style={{
              alignItems: 'center',
              justifyContent: 'center',
              height: 50,
              borderRadius: 10,

              width: wp(90),
              marginVertical: 10,
              backgroundColor: COLOR.GREEN,
              alignSelf: 'center',
            }}>
            <Text style={[Styles.button_font, {color: COLOR.WHITE}]}>
              Add Special Needs
            </Text>
          </TouchableOpacity>
        </KeyboardAwareScrollView>
      </SafeAreaView>
    );
  }
}

const style = StyleSheet.create({
  flatlist_view: {
    width: wp(90),
    alignSelf: 'center',
    padding: 15,
    borderRadius: 10,
    borderColor: COLOR.GREEN,
    marginVertical: 10,
    borderWidth: 1,
    backgroundColor: COLOR.WHITE,
  },
  tick_view: {
    flexDirection: 'row',
    marginVertical: 10,
    alignItems: 'center',
  },
});

mapDispatchToProps = (dispatch) => {
  return {
    updateProfile: (data, val, id) => {
      dispatch(ProfileAction.updateProfile(data, val, id));
    },
  };
};

export default connect(null, mapDispatchToProps)(SpecialNeeds);
