import React from 'react';
import {
  View,
  Image,
  Text,
  SafeAreaView,
  StyleSheet,
  Linking,
} from 'react-native';
import {TouchableWithoutFeedback} from 'react-native-gesture-handler';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import NavigationBar from '../../commonView/NavigationBar';
import FONTS from '../../styles/Fonts';
import IMAGES from '../../styles/Images';
import Styles from '../../styles/Styles';

export default class ProfileSupport extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <SafeAreaView style={Styles.container}>
        <NavigationBar
          left_image={IMAGES.back_arrow}
          prop={this.props}
          height={40}
          name={'Support'}
        />
        <Text
          style={[
            Styles.subheading_label,
            {textAlign: 'center', marginTop: 20, marginBottom: 10},
          ]}>
          Travelling 24/7 Support
        </Text>
        <Text
          style={[Styles.small_label, {textAlign: 'center', width: wp(70)}]}>
          Your feedback is important to us. Share with us your comments,
          compliments and feedback to help us improve our services.
        </Text>
        <Text
          style={[
            Styles.small_label,
            {textAlign: 'center', width: wp(70), marginVertical: 20},
          ]}>
          Support@safar.sa{'\n'}92000 00 00
        </Text>
        <View style={{alignSelf: 'center', width: wp(90)}}>
          <View>
            <TouchableWithoutFeedback
              onPress={() => {
                this.props.navigation.navigate('AdminChatScreen');
              }}>
              <View style={style.horizontal_view}>
                <Image
                  style={{height: 20, width: 20, marginRight: 10}}
                  source={IMAGES.black_message}
                />
                <View style={{flex: 1, marginHorizontal: 10}}>
                  <Text style={style.text_label}>Live Chat</Text>
                  <Text
                    style={[
                      Styles.small_label,
                      {width: null, alignSelf: 'flex-start'},
                    ]}>
                    Get support via live chat
                  </Text>
                </View>
                <Image
                  style={{height: 15, width: 15}}
                  source={IMAGES.right_arrow}
                />
              </View>
            </TouchableWithoutFeedback>
            <View
              style={[Styles.line_view, {width: wp(90), marginVertical: 10}]}
            />
          </View>
          <View>
            <View style={style.horizontal_view}>
              <Image
                style={{height: 20, width: 20, marginRight: 10}}
                source={IMAGES.mobile_black}
              />
              <View style={{flex: 1, marginHorizontal: 10}}>
                <Text style={style.text_label}>Call</Text>
                <Text
                  style={[
                    Styles.small_label,
                    {width: null, alignSelf: 'flex-start'},
                  ]}>
                  Our team is available 24/7
                </Text>
              </View>
              <Image
                style={{height: 15, width: 15}}
                source={IMAGES.right_arrow}
              />
            </View>
            <View
              style={[Styles.line_view, {width: wp(90), marginVertical: 10}]}
            />
          </View>

          <View>
            <TouchableWithoutFeedback
              onPress={() => {
                Linking.openURL('mailto:support@example.com');
              }}>
              <View style={style.horizontal_view}>
                <Image
                  style={{height: 20, width: 20, marginRight: 10}}
                  source={IMAGES.email_black}
                  resizeMode={'contain'}
                />
                <View style={{flex: 1, marginHorizontal: 10}}>
                  <Text style={style.text_label}>Email</Text>
                  <Text
                    style={[
                      Styles.small_label,
                      {width: null, alignSelf: 'flex-start'},
                    ]}>
                    Send us a message
                  </Text>
                </View>
                <Image
                  style={{height: 15, width: 15}}
                  source={IMAGES.right_arrow}
                />
              </View>
            </TouchableWithoutFeedback>
            <View
              style={[Styles.line_view, {width: wp(90), marginVertical: 10}]}
            />
          </View>
          <View
            style={{
              alignItems: 'center',
              justifyContent: 'center',
              marginVertical: 50,
            }}>
            <Text
              style={[
                Styles.body_label,
                {textAlign: 'center', marginBottom: 20},
              ]}>
              Socail media
            </Text>
            <View
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                flexDirection: 'row',
              }}>
              <Image
                style={{height: 40, width: 40}}
                source={IMAGES.facebook}
                resizeMode={'contain'}
              />
              <Image
                style={{height: 40, width: 40, marginHorizontal: 30}}
                source={IMAGES.twitter}
                resizeMode={'contain'}
              />
              <Image
                style={{height: 40, width: 40}}
                source={IMAGES.instagram}
                resizeMode={'contain'}
              />
            </View>
          </View>
        </View>
      </SafeAreaView>
    );
  }
}

const style = StyleSheet.create({
  horizontal_view: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginVertical: 10,
  },
  text_label: {
    fontFamily: FONTS.FAMILY_REGULAR,
    fontSize: FONTS.MEDIUM,
  },
});
