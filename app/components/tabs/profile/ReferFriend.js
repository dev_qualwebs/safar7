import React from 'react';
import {View, StyleSheet, Text, Image} from 'react-native';
import {TextInput, TouchableWithoutFeedback} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import {DoneButtonKeyboard} from '../../commonView/CommonView';

import COLOR from '../../styles/Color';
import FONTS from '../../styles/Fonts';
import IMAGES from '../../styles/Images';
import Styles from '../../styles/Styles';

export default class ReferFriend extends React.Component {
  render() {
    return (
      <View style={Styles.container}>
        <View style={style.blue_view}>
          <View style={{flexDirection:'row',alignItems:'center',justifyContent:'space-between',  marginTop: getStatusBarHeight(true),width:widthPercentageToDP(80),alignSelf:'center'}}>
          <TouchableOpacity
          onPress={() => {
this.props.navigation.goBack()
          }}>
            <Image
              style={{
                height: 15,
                width: 15,
                tintColor:COLOR.WHITE
              }}
              source={IMAGES.back_arrow}
            />
        </TouchableOpacity>
          <Text
            style={[
              {
                color: COLOR.WHITE,
                fontSize: FONTS.MEDIUM,
                fontFamily: FONTS.FAMILY_BOLD,
                alignSelf: 'center',
                textAlign: 'center',
                // marginTop: getStatusBarHeight(true),
              },
            ]}>
            Refer a Friend
          </Text>
          <View  style={{
            height: 50,
            width: 45,
            justifyContent: 'center',
            alignItems: 'center',
          }}/>
          </View>
        </View>
        <View style={style.count_view}>
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              flex: 1,
              alignSelf: 'center',
            }}>
            <Text
              style={[
                Styles.small_label,
                {
                  width: null,
                  color: COLOR.BLACK,
                  textAlign: 'center',
                  fontFamily: FONTS.FAMILY_SEMIBOLD,
                },
              ]}>
              PENDING{'\n'}INVITES
            </Text>
            <Text
              style={[
                Styles.heading_label,
                {
                  width: null,
                  color: COLOR.BLACK,
                  marginVertical: 0,
                },
              ]}>
              0
            </Text>
          </View>
          <View
            style={{
              width: 1,
              backgroundColor: COLOR.GRAY,
            }}
          />
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              flex: 1,
              alignSelf: 'center',
            }}>
            <Text
              style={[
                Styles.small_label,
                {
                  width: null,
                  color: COLOR.BLACK,
                  textAlign: 'center',
                  fontFamily: FONTS.FAMILY_SEMIBOLD,
                },
              ]}>
              SUCCESSFUL{'\n'}INVITES
            </Text>
            <Text
              style={[
                Styles.heading_label,
                {
                  width: null,
                  color: COLOR.BLACK,
                  marginVertical: 0,
                },
              ]}>
              0
            </Text>
          </View>
        </View>
        <View
          style={[
            style.count_view,
            {marginTop: 20, padding: 20, flexDirection: 'column', height: 140},
          ]}>
          <Text
            style={[
              Styles.body_label,
              {
                alignSelf: null,
                width: null,
                marginVertical: 0,
                fontFamily: FONTS.FAMILY_BOLD,
              },
            ]}>
            Referal Link
          </Text>
          <TextInput
            inputAccessoryViewID={'uniqueID'}
            placeholder={'Link here'}
            placeholderTextColor={COLOR.LIGHT_TEXT_COLOR}
            style={[
              {
                marginTop: 10,
                width: wp(70),
                textAlign: 'left',
                alignSelf: 'center',
              },
            ]}
            multiline={true}
          />
          <DoneButtonKeyboard />
          <View style={[Styles.line_view, {marginTop: 10, width: wp(75)}]} />
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'flex-end',
              alignItems: 'center',
              marginTop: 20,
            }}>
            <TouchableOpacity style={{height: 40, width: 20, marginRight: 20}}>
              <Image
                style={{height: 20, width: 20, marginRight: 20}}
                resizeMode={'contain'}
                source={IMAGES.copy_image}
              />
            </TouchableOpacity>
            <TouchableOpacity style={{height: 40, width: 20}}>
              <Image
                style={{height: 20, width: 20}}
                resizeMode={'contain'}
                source={IMAGES.share}
              />
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

const style = StyleSheet.create({
  blue_view: {
    height: 200,
    width: wp(100),
    marginTop: 0,

    backgroundColor: COLOR.GREEN,
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
  },
  count_view: {
    height: 110,
    width: wp(80),
    alignSelf: 'center',
    backgroundColor: COLOR.WHITE,
    flexDirection: 'row',
    borderRadius: 10,
    marginTop: -80,
    shadowOffset: {width: 0.2, height: 0.2},
    shadowOpacity: 0.2,
    shadowColor: COLOR.VOILET,
    elevation: 3,
  },
});
