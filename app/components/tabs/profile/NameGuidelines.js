import React from 'react';
import { Modal, Text, View, StyleSheet, Image } from 'react-native';
import {

  ScrollView,
} from 'react-native';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import COLOR from '../../../components/styles/Color';
import FONTS from '../../../components/styles/Fonts';
import IMAGES from '../../../components/styles/Images';
import Styles from '../../../components/styles/Styles';
import NavigationBar from '../../commonView/NavigationBar';
import { FloatingTitleTextInputField } from '../../../helper/FloatingTitleTextInputField';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scrollview';

import { Hoshi } from 'react-native-textinput-effects';
import { connect } from 'react-redux';

class NameGuideline extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: true,
      userDetail : null
    };
  }

  componentDidMount(){
if(this.props.route.params && this.props.route.params.userDetail){
  this.setState({userDetail:this.props.route.params.userDetail});
}
  }

  updateMasterState = (attrName, value) => {
    this.setState({ [attrName]: value });
  };

  render() {
    const {userDetail} = this.state;
    return (
      <View style={Styles.container}>
        <Modal
          animationType={'slide'}
          transparent={true}
          style={{ backgroundColor: COLOR.BLACK }}
          visible={this.state.modalVisible}
          onRequestClose={() => {
            console.log('Modal has been closed.');
          }}>
          <View style={styles.modal}>
            <View style={styles.modalBody}>
              <ScrollView
                showsVerticalScrollIndicator={false}
                showsHorizontalScrollIndicator={false}
                style={{ flex: 1 }}>
                <NavigationBar prop={this.props} height={45} name={''} />
                <Image
                  style={{ height: 150, width: wp(90), marginVertical: 20 }}
                  source={IMAGES.name_guideline}
                  resizeMode={'contain'}
                />

                <View
                  style={{
                    alignSelf: 'center',
                    width: wp(90),
                    marginBottom: 20,
                  }}>
                  <Hoshi
                    label={'Surname (Last Name)'}
                    value={userDetail && userDetail.last_name ? userDetail.last_name : ''}
                    borderColor={'transparent'}
                    borderRadius={0}
                    height={45}
                    returnKeyType={'done'}
                    keyboardType={'email-address'}
                    style={{ marginBottom: 15 }}
                    // onChangeText={this.handleEmailChange}
                    inputStyle={Styles.text_input_text}
                    labelStyle={Styles.text_input_heading}
                  />
                  <Hoshi
                    label={'Given Name (First Name Middle Name)'}
                    value={userDetail &&( userDetail.first_name ? userDetail.first_name : '') + ' ' + (userDetail.middle_name ? userDetail.middle_name : '')} 
                    borderColor={COLOR.BORDER}
                    borderRadius={0}
                    height={45}
                    returnKeyType={'done'}
                    keyboardType={'email-address'}
                    style={{ marginBottom: 15 }}
                    // onChangeText={this.handleEmailChange}
                    inputStyle={Styles.text_input_text}
                    labelStyle={Styles.text_input_heading}
                  />
                  <Text
                    style={[
                      Styles.subheading_label,
                      {
                        marginTop: 20,
                        marginBottom: 10,
                        fontSize: FONTS.REGULAR,
                      },
                    ]}>
                    Important Notice
                  </Text>
                  <Text style={Styles.small_label}>
                    Please enter the passenger’s name, gender, date of birth,
                    and ID number (if the ID includes these) exactly as they
                    appear on the passport to be provided to airline officials
                    at check-in. Modifications of passenger information after
                    submission are often not permitted, or may incur an
                    additional fee. Please ensure the passport is valid for at
                    least 6 months after the date of departure. First and last
                    names should not exceed 50 characters in length, including
                    the passenger title, and should only include alphabetical
                    characters such as A-Z or a-z.
                  </Text>
                  <Text
                    style={[
                      Styles.subheading_label,
                      {
                        marginTop: 20,
                        marginBottom: 10,
                        fontSize: FONTS.REGULAR,
                      },
                    ]}>
                    What about special characters in names?
                  </Text>
                  <Text style={Styles.small_label}>
                    Special characters like accents, hyphens, or other
                    punctuation marks are not accepted for either surnames or
                    given names. Surnames cannot contain spaces.
                  </Text>
                  <Text
                    style={[
                      Styles.subheading_label,
                      {
                        marginTop: 20,
                        marginBottom: 10,
                        fontSize: FONTS.REGULAR,
                      },
                    ]}>
                    There is not enough room to enter my entire name.
                  </Text>
                  <Text style={Styles.small_label}>
                    If your first name is too long, consider using just the
                    initial. If your middle name is too long, consider removing
                    it or just using the initial. If your last name is too long,
                    we advise contacting the airline directly to place your
                    booking. To avoid any security issues, we do not suggest you
                    modify your last name.
                  </Text>
                  <Text
                    style={[
                      Styles.subheading_label,
                      {
                        marginTop: 20,
                        marginBottom: 10,
                        fontSize: FONTS.REGULAR,
                      },
                    ]}>
                    What if my last name is too long / last name has only one
                    letter / I only have one name?
                  </Text>
                  <Text style={Styles.small_label}>
                    We advise you book with the airline directly.
                  </Text>
                </View>
              </ScrollView>
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}

mapStateToProps = (state, ownProps) => {
  return {
    userDetail: state.profileReducer.userDetail,
  };
};



export default connect(mapStateToProps, null)(NameGuideline);


const styles = StyleSheet.create({
  modal: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
  },
  modalBody: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    backgroundColor: COLOR.WHITE,
    borderWidth: 1,
    width: wp(100),
    marginTop: 50,
    borderColor: COLOR.BORDER,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    flex: 1,
  },
  image: {
    height: 120,
    width: 120,
    alignSelf: 'center',
    marginVertical: 20,
  },
  headingLabel: {
    fontFamily: FONTS.FAMILY_BOLD,
    fontSize: FONTS.MEDIUM,
    color: COLOR.BLACK,
  },
  singleLineView: {
    marginVertical: 20,
    borderBottomColor: COLOR.GRAY,
    width: wp(100),
    alignSelf: 'center',
    borderBottomWidth: 1,
  },
  borderView: {
    borderRadius: 10,
    borderWidth: 3,
    width: wp(90),
    borderColor: COLOR.GRAY,
    alignSelf: 'center',
  },
});
