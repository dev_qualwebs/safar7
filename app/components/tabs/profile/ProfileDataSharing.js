import React from 'react';
import {View, Image, Text, SafeAreaView, StyleSheet} from 'react-native';
import {TouchableOpacity} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import NavigationBar from '../../commonView/NavigationBar';
import COLOR from '../../styles/Color';
import FONTS from '../../styles/Fonts';
import IMAGES from '../../styles/Images';
import Styles from '../../styles/Styles';

export default class ProfileDataSharing extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <SafeAreaView style={Styles.container}>
        <NavigationBar
          left_image={IMAGES.back_arrow}
          prop={this.props}
          height={40}
          name={'Data Sharing'}
        />
       
        <Text
          style={[
            Styles.subheading_label,
            {marginTop: 20, marginBottom: 10, fontFamily: FONTS.FAMILY_BOLD},
          ]}>
          Your choices over personal information
        </Text>
        <View
                  style={{flexDirection: 'row', width: null,alignSelf:'center'}}>
        <Text style={[Styles.small_label, {color: COLOR.LIGHT_TEXT_COLOR}]}>
          We share your personal information as described in our
          <TouchableOpacity
          onPress={()=>this.props.navigation.navigate('PolicyPage',{screenTitle:'Privacy Policy'})}
          >
          <Text style={[Styles.small_label, {color: COLOR.GREEN,width:null,marginBottom:-2}]}>
            Privacy Policy </Text></TouchableOpacity>to allow third parties to provide marketing and offers that are
          relevant to you.
        </Text>
        </View>
        <View
          style={{
            marginVertical: 20,
            width: wp(90),
            alignSelf: 'center',
          }}>
          <View>
            <View style={style.horizontal_view}>
              <View>
                <Text style={style.text_label}>
                  Sharing with our Group Company
                </Text>
                <Text
                  style={[
                    Styles.small_label,
                    {
                      width: null,
                      color: COLOR.GREEN,
                      textAlign: 'left',
                      alignSelf: 'flex-start',
                    },
                  ]}>
                  Learn More
                </Text>
              </View>
              <TouchableOpacity>
                <Image
                  style={{height: 30, width: 45}}
                  source={IMAGES.radio_button_on}
                  resizeMode={'contain'}
                />
              </TouchableOpacity>
            </View>
            <View
              style={[Styles.line_view, {width: wp(90), marginVertical: 10}]}
            />
          </View>
          <View>
            <View style={style.horizontal_view}>
              <View>
                <Text style={style.text_label}>
                  Sharing with our Business Partners
                </Text>
                <Text
                  style={[
                    Styles.small_label,
                    {
                      width: null,
                      color: COLOR.GREEN,
                      textAlign: 'left',
                      alignSelf: 'flex-start',
                    },
                  ]}>
                  Learn More
                </Text>
              </View>
              <TouchableOpacity>
                <Image
                  style={{height: 30, width: 45}}
                  source={IMAGES.radio_button_on}
                  resizeMode={'contain'}
                />
              </TouchableOpacity>
            </View>
            <View
              style={[Styles.line_view, {width: wp(90), marginVertical: 10}]}
            />
          </View>
          <View>
            <View style={style.horizontal_view}>
              <View>
                <Text style={style.text_label}>
                  Sharing with our Travel Partners
                </Text>
                <Text
                  style={[
                    Styles.small_label,
                    {
                      width: null,
                      color: COLOR.GREEN,
                      textAlign: 'left',
                      alignSelf: 'flex-start',
                    },
                  ]}>
                  Learn More
                </Text>
              </View>
              <TouchableOpacity>
                <Image
                  style={{height: 30, width: 45}}
                  source={IMAGES.radio_button_on}
                  resizeMode={'contain'}
                />
              </TouchableOpacity>
            </View>
            <View
              style={[Styles.line_view, {width: wp(90), marginVertical: 10}]}
            />
          </View>
        </View>
      </SafeAreaView>
    );
  }
}

const style = StyleSheet.create({
  horizontal_view: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginVertical: 10,
  },
  text_label: {
    fontFamily: FONTS.FAMILY_REGULAR,
    fontSize: FONTS.MEDIUM,
  },
});
