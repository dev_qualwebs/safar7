import React from 'react';
import {
  View,
  Text,
  Image,
  SafeAreaView,
  StyleSheet,
  Alert,
  Modal,
} from 'react-native';
import {TextInput, TouchableOpacity} from 'react-native';
import IMAGES from '../../styles/Images';
import Styles from '../../styles/Styles';
import ImagePicker from 'react-native-image-crop-picker';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
  widthPercentageToDP,
  heightPercentageToDP,
} from 'react-native-responsive-screen';
import {connect} from 'react-redux';
import COLOR from '../../styles/Color';
import ProfileAction from '../../../redux/action/ProfileAction';
import API from '../../../api/Api';
import Toast from 'react-native-simple-toast';
import SelectDate from '../calendar/SelectDate';
import DropDownPicker from 'react-native-dropdown-picker';
import FONTS from '../../styles/Fonts';
import SimpleToast from 'react-native-simple-toast';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview';
import moment from 'moment';
import {ScanDocument} from './ScanDocument';
import { DoneButtonKeyboard } from '../../commonView/CommonView';
import ActivityIndicator from '../../commonView/ActivityIndicator';

class PassportDetail extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      tripData: [],
      actionModalVisible: false,
      country_id: '141',
      issue_date: 'Select Issue date',
      expiry_date: 'Select Expiry date',
      number: '',
      image: null,
      pickerVisible: false,
      nationalityDropdown: false,
      country: 'Select Nationality',
      attrName: null,
      openCalendar: false,
      countryDropDown: false,
      countries: [],
      selectedCountryId: null,
      selectedNationality: '966',
      isPassportVisible: false,
      maxDate: null,
      minDate: null,
      loading:false,
    };
    this.chooseImage = this.chooseImage.bind(this);
    this.props.getCountries();
  }

  updateMasterState = (attrName, value) => {
    this.setState({[attrName]: value});
  };

  componentDidMount() {
    if (this.props.countries) {
      const countries = this.props.countries.map(function (value) {
        let object = {
          label: value.name,
          value: value.id,
          id: value.id,
        };
        return object;
      });
      this.setState({
        countries: countries,
      });
    }
  }

  componentDidUpdate(prevProps) {
    if (this.props.countries != prevProps.countries) {
      const countries = this.props.countries.map(function (value) {
        let object = {
          label: value.name,
          value: value.id,
          id: value.id,
        };
        return object;
      });
      this.setState({
        countries: countries,
      });
    }

    if (
      this.props.props.route.params &&
      this.props.props.route.params.updateProfile
    ) {
    } else {
      if (this.props.userDetail != prevProps.userDetail) {
        let data = this.props.userDetail;

        if (
          data.primary_traveller &&
          data.primary_traveller.traveller_passport
        ) {
          this.setState({
            selectedCountryId:
              data.primary_traveller.traveller_passport.issue_country_id,
            selectedNationality:
              data.primary_traveller.traveller_passport.nationality,
            image: data.primary_traveller.traveller_passport.passport_image,
            issue_date: data.primary_traveller.traveller_passport.issue_date,
            expiry_date: data.primary_traveller.traveller_passport.expiry_date,
            number: data.primary_traveller.traveller_passport.number,
          });
        }
      }
    }
  }

  chooseImage = (val) => {
    let options = {
      includeBase64: true,
      title: 'Select Image',
      width: 1630,
      height: 750,
      compressImageMaxWidth: 1630,
      compressImageMaxHeight: 750,
      multiple: false,
    };

    if (val == 1) {
      ImagePicker.openCamera({
        title: 'Select Image',
        width: 1024,
        height: 472,
        compressImageMaxWidth: 1024,
        compressImageMaxHeight: 472,
        includeBase64: true,
      }).then((response) => {
        console.log('image local reposnse', response);
        this.setState({
          actionModalVisible: false,
        });
        this.callUploadImage(['data:image/jpeg;base64,' + response.data], val);
      });
    } else if (val == 2) {
      ImagePicker.openPicker(options).then((response) => {
        console.log('image local reposnse', response);
        this.setState({
          actionModalVisible: false,
        });
        //   let base64Response = response.map((val)=>{
        //     return 'data:image/jpeg;base64,' + val.data
        // });
        // this.callUploadImage(base64Response, val);
        this.callUploadImage(['data:image/jpeg;base64,' + response.data], val);
      });
    }
  };

  setModalVisibility() {
    this.setState({actionModalVisible: false});
  }

  callUploadImage(res, val) {
    this.setState({actionModalVisible: false});
    // loading:true
    let data = {
      images: res,
      path: 'passport',
    };

    const api = new API();

    api
      .uploadImage(data)
      .then((json) => {
        console.log('upload responce:-', json.data);

        if (json.status == 200) {
          this.setState({image: json.data.response[0],loading:false});
          setTimeout(() => {
            SimpleToast.show('Successfully uploaded');
          }, 500);
        } else if (json.status == 400) {
          setTimeout(() => {
            Toast.show(json.data.message);
          }, 0);
        } else {
          setTimeout(() => {
            Toast.show(json.data.message);
          }, 0);
        }
      })
      .catch((error) => {
        console.log('error:-', error);
      });
  }

  handleSheet = (val) => {
    if (val == 1) {
      this.chooseImage(1);
    } else if (val == 2) {
      this.chooseImage(2);
    } else if (val == 3) {
      this.setState({
        actionModalVisible: false,
      });
    }
  };

  handleCreateTraveller = () => {
    // this.props.nextBtn.current.handleTabNavigation();
    if (this.state.country_id == null) {
      SimpleToast.show('Select Country Id');
    } else if (this.state.issue_date == null) {
      SimpleToast.show('Select Issue date');
    } else if (this.state.expiry_date == null) {
      SimpleToast.show('Select Expiry Date');
    } else if (this.state.number == null || this.state.number == '') {
      SimpleToast.show('Enter Passport Number');
    } else if (this.state.image == null) {
      SimpleToast.show('Scan Passport Image');
    } else if (this.state.selectedCountryId == null) {
      SimpleToast.show('Select Passport Issue Country');
    } else {
      if (this.props.props.route.params) {
        this.props.props.navigation.replace('HomeStack');
      } else {
        this.props.props.navigation.pop(1);
      }
      let passportData = {
        country_id: parseInt(this.state.selectedCountryId),
        nationality: parseInt(this.state.selectedNationality),
        issue_date: this.state.issue_date,
        expiry_date: this.state.expiry_date,
        number: this.state.number,
        image: this.state.image,
      };
      this.props.updateOrCreatePassport(JSON.stringify(passportData));
    }
  };

  commonView = (props) => {
    return (
      <>
        <Text style={[classStyle.title_Label]}>{props.name}</Text>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            width: null,
            alignItems: 'center',
            height: 40,
          }}>
          <TextInput
          inputAccessoryViewID={'uniqueID'}
            placeholderTextColor={COLOR.LIGHT_TEXT_COLOR}
            style={[Styles.body_label, {width: null, flex: 1}]}
            placeholder={props.name}
            onChangeText={(text) => this.setState({[props.label]: text})}
            value={`${props.input}`}
            keyboardType={props.keyboardType ? props.keyboardType : 'default'}
          />
          <DoneButtonKeyboard />
          <Image
            style={{
              height: 10,
              width: 10,
              tintColor: COLOR.VOILET,
              marginHorizontal: 5,
            }}
            source={props.drop_image}
          />
        </View>
      </>
    );
  };

  render() {
    return (
      <SafeAreaView style={Styles.container}>
        <ActivityIndicator loading={this.state.loading} />
        {this.state.openCalendar ? (
          <SelectDate
            allowRangeSelection={false}
            attrName={this.state.attrName}
            // maxDate={moment(Date()).add(20, 'year').format('YYYY-MM-DD')}
            // minDate={new Date(0)}
            maxDate={this.state.maxDate}
            minDate={this.state.minDate}
            updateMasterState={this.updateMasterState}
            props={this.props.props}
          />
        ) : null}
        <KeyboardAwareScrollView
        bounces={false}
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{flexGrow: 1}}>
          <View style={Styles.mainView}>
            <TouchableOpacity
              style={{
                height: 50,
                width: wp(90),
                marginVertical: 20,
                borderRadius: 6,
                borderWidth: 1,
                borderColor: COLOR.LIGHT_TEXT,
                justifyContent: 'center',
                alignItems: 'center',
              }}
              onPress={() => {
                this.setState({
                  actionModalVisible: true,
                });
              }}>
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <Image
                  source={IMAGES.second_camera}
                  style={{
                    height: 20,
                    width: 20,
                    tintColor: COLOR.BLACK,
                    marginRight: 5,
                  }}
                  resizeMode={'contain'}
                />
                <Text
                  style={[
                    Styles.body_label,
                    {textAlign: 'center', width: null},
                  ]}>
                  Scan Passport
                </Text>
              </View>
            </TouchableOpacity>

            {this.state.image ? (
              <View style={{alignItems: 'flex-end'}}>
                <TouchableOpacity
                  style={{marginBottom: 10}}
                  onPress={() => {
                    this.setState({isPassportVisible: true});
                  }}>
                  <Text style={[{color: COLOR.GREEN}]}>
                    view scanned passport
                  </Text>
                </TouchableOpacity>
              </View>
            ) : null}
            <Text style={[classStyle.title_Label]}>
              Select Passport issue country
            </Text>
            <DropDownPicker
              showArrowIcon={true}
              open={this.state.countryDropDown}
              setOpen={() =>
                this.setState({
                  countryDropDown: !this.state.countryDropDown,
                })
              }
              labelStyle={{
                fontSize: FONTS.MEDIUM,
                fontFamily: FONTS.FAMILY_REGULAR,
                color: COLOR.BLACK,
                marginLeft: -5,
              }}
              dropDownMaxHeight={300}
              style={{
                backgroundColor: '#ffffff',
                shadowOpacity: 0,
                borderWidth: 0,
                height: 40,
                alignSelf: 'center',
                borderTopLeftRadius: 0,
                borderTopRightRadius: 0,
                borderBottomLeftRadius: 0,
                borderBottomRightRadius: 0,
                paddingHorizontal: 0,
              }}
              dropDownStyle={{
                backgroundColor: '#fff',
              }}
              containerStyle={{
                borderRadius: 0,
                paddingHorizontal: 0,
              }}
              setValue={(callback) =>
                this.setState((state) => ({
                  selectedCountryId: callback(state.value),
                }))
              }
              value={this.state.selectedCountryId}
              textStyle={{
                fontFamily: FONTS.FAMILY_REGULAR,
              }}
              placeholder={'Select Passport issue country'}
              labelStyle={{
                fontFamily: FONTS.FAMILY_REGULAR,
              }}
              items={this.state.countries}
            />
            <View
              style={[Styles.line_view, {marginVertical: 10, width: wp(90)}]}
            />
            <Text style={[classStyle.title_Label]}>Select issue date</Text>
            <TouchableOpacity
              onPress={() => {
                this.setState({maxDate: Date(), minDate: new Date(0)}, () => {
                  this.setState({openCalendar: true, attrName: 'issue_date'});
                });
              }}
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                width: null,
                alignItems: 'center',
                height: 40,
              }}>
              <Text style={[Styles.body_label, {width: null, flex: 1}]}>
                {this.state.issue_date}
              </Text>
              <Image
                style={{
                  height: 10,
                  width: 10,
                  tintColor: COLOR.VOILET,
                  marginHorizontal: 5,
                }}
                source={IMAGES.down_arrow}
              />
            </TouchableOpacity>

            <View
              style={[Styles.line_view, {marginVertical: 10, width: wp(90)}]}
            />
            <Text style={[classStyle.title_Label]}>Select expiry date</Text>
            <TouchableOpacity
              onPress={() => {
                this.setState(
                  {
                    maxDate: moment(Date())
                      .add(20, 'year')
                      .format('YYYY-MM-DD'),
                    minDate: this.state.issue_date
                      ? this.state.issue_date
                      : new Date(0),
                  },
                  () => {
                    this.setState({
                      openCalendar: true,
                      attrName: 'expiry_date',
                    });
                  },
                );
              }}
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                width: null,
                alignItems: 'center',
                height: 40,
              }}>
              <Text style={[Styles.body_label, {width: null, flex: 1}]}>
                {this.state.expiry_date}
              </Text>
              <Image
                style={{
                  height: 10,
                  width: 10,
                  tintColor: COLOR.VOILET,
                  marginHorizontal: 5,
                }}
                source={IMAGES.down_arrow}
              />
            </TouchableOpacity>

            <View
              style={[Styles.line_view, {marginVertical: 10, width: wp(90)}]}
            />
            <this.commonView
              name={'Passport Number'}
              input={this.state.number}
              label={'number'}
              // keyboardType={'number-pad'}
            />

            <View
              style={[Styles.line_view, {marginVertical: 10, width: wp(90)}]}
            />
            <Text style={[classStyle.title_Label]}>Select Nationality</Text>
            <DropDownPicker
              showArrowIcon={true}
              open={this.state.nationalityDropdown}
              setOpen={() =>
                this.setState({
                  nationalityDropdown: !this.state.nationalityDropdown,
                })
              }
              labelStyle={{
                fontSize: FONTS.MEDIUM,
                fontFamily: FONTS.FAMILY_REGULAR,
                color: COLOR.BLACK,
                marginLeft: -5,
              }}
              dropDownMaxHeight={300}
              style={{
                backgroundColor: '#ffffff',
                shadowOpacity: 0,
                borderWidth: 0,
                height: 40,
                alignSelf: 'center',
                borderTopLeftRadius: 0,
                borderTopRightRadius: 0,
                borderBottomLeftRadius: 0,
                borderBottomRightRadius: 0,
                paddingHorizontal: 0,
              }}
              dropDownStyle={{
                backgroundColor: '#fff',
              }}
              containerStyle={{
                borderRadius: 0,
              }}
              setValue={(callback) =>
                this.setState((state) => ({
                  selectedNationality: callback(state.value),
                }))
              }
              value={this.state.selectedNationality}
              textStyle={{
                fontFamily: FONTS.FAMILY_REGULAR,
              }}
              placeholder={'Select Nationality'}
              labelStyle={{
                fontFamily: FONTS.FAMILY_REGULAR,
              }}
              items={this.state.countries}
            />
          </View>
        </KeyboardAwareScrollView>
        <TouchableOpacity
          onPress={this.handleCreateTraveller.bind(this)} //this.props.navigation.navigate('NameGuideline')}
          style={{
            alignItems: 'center',
            justifyContent: 'center',
            height: 50,
            borderRadius: 10,
            width: wp(90),
            marginVertical: 10,
            backgroundColor: COLOR.GREEN,
            alignSelf: 'center',
          }}>
          <Text style={[Styles.button_font, {color: COLOR.WHITE}]}>Save</Text>
        </TouchableOpacity>
        {/* <ActionSheet
          modalVisible={this.state.actionModalVisible}
          handleSheet={this.handleSheet}
        /> */}
        <ScanDocument
          modalVisible={this.state.actionModalVisible}
          onDoneAction={(res) => this.callUploadImage([res], 1)}
          onBackAction={() => this.setModalVisibility()}
        />
        <Modal
          animationType={'fade'}
          transparent={false}
          style={{backgroundColor: COLOR.BLACK}}
          visible={this.state.isPassportVisible}
          onRequestClose={() => {
            this.setState({isPassportVisible: false});
          }}>
          <View
            style={[
              {
                marginTop: 40,
                alignItems: 'flex-end',
                width: widthPercentageToDP(95),
              },
            ]}>
            <TouchableOpacity
              onPress={() => {
                this.setState({isPassportVisible: false});
              }}>
              <Image source={IMAGES.close} style={{height: 25, width: 25}} />
            </TouchableOpacity>
          </View>
          <View
            style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
            <Image
              resizeMode="contain"
              source={{uri: this.state.image}}
              style={{
                width: widthPercentageToDP(90),
                height: heightPercentageToDP(40),
              }}
            />
          </View>
        </Modal>
      </SafeAreaView>
    );
  }
}

const classStyle = StyleSheet.create({
  flatlist_view: {
    marginTop: 0,
    width: wp(100),
    marginBottom: 0,
    height: hp(50),
    marginHorizontal: 10,
  },
  flatlist_image_view: {
    height: hp(50),
    width: wp(70),
    marginRight: 0,
  },
  offer_view: {
    marginVertical: 0,
    width: wp(100),
    marginBottom: 20,
    height: 100,
    marginHorizontal: 10,
  },
  title_Label: {
    fontFamily: FONTS.FAMILY_REGULAR,
    fontSize: FONTS.SMALL,
    color: COLOR.BLACK,
    marginVertical: 5,
  },
});
const mapDispatchToProps = (dispatch) => {
  return {
    updateOrCreatePassport: (data, val, id) => {
      dispatch(ProfileAction.updateOrCreatePassport(data, val, id));
    },
    getCountries: (val) => {
      dispatch(ProfileAction.getCountries(val));
    },
    updateProfile: (data, val, id) => {
      dispatch(ProfileAction.updateProfile(data, val, id));
    },
  };
};

const mapStateToProps = (state, ownState) => {
  return {
    countries: state.profileReducer.countries,
    userDetail: state.profileReducer.userDetail,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(PassportDetail);
