import React, {Component} from 'react';
import {
  SafeAreaView,
  Alert,
  View,
  Image,
  TouchableOpacity,
  Text,
} from 'react-native';
import Styles from '../../styles/Styles';
import {T_PROFILE_TAB} from '../../../helper/Constants';
import TopTabBar from '../home/trip/TabBarView';
import IMAGES from '../../styles/Images';
import ProfileAction from '../../../redux/action/ProfileAction';
import {connect} from 'react-redux';
import COLOR from '../../styles/Color';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import FONTS from '../../styles/Fonts';
import {StyleSheet} from 'react-native';
import API from '../../../api/Api';

const api = new API();

class ProfileDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      index: 0,
      modalVisible: false,
      personalData: null,
      singleTravellerData: null,
    };
  }

  setData = (data) => {
    this.setState({
      personalData: data,
    });
  };

  render() {
    return (
      <SafeAreaView style={Styles.container}>
        <View style={{marginBottom: 0}}>
          <View
            style={[
              Styles.mainView,
              {
                height: 50,
                justifyContent: 'space-between',
                alignSelf: 'center',
                alignItems: 'center',
                flexDirection: 'row',
                width: wp(98),
              },
            ]}>
            {this.props.route.params?.updateProfile ? (
              <View style={styles.skipView} />
            ) : (
              <TouchableOpacity
                style={{
                  height: 50,
                  width: 45,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}
                onPress={() => {
                  {
                    this.props.navigation.goBack();
                  }
                }}>
                <Image
                  style={{
                    height: 15,
                    width: 15,
                  }}
                  source={IMAGES.back_arrow}
                />
              </TouchableOpacity>
            )}
            <View>
              <Text
                style={[
                  Styles.subheading_label,
                  {
                    textAlign: 'center',
                    color: COLOR.BLACK,
                    fontSize: 20,
                    fontFamily: FONTS.FAMILY_BOLD,
                    width: null,
                    flex: 1,
                    marginTop: 15,
                  },
                ]}>
                Profile
              </Text>
            </View>
            <View style={styles.skipView} />
          </View>
          <View
            style={{
              borderBottomColor: COLOR.GRAY,
              borderBottomWidth: 1,
            }}
          />
        </View>
        <TopTabBar
          currentTab={T_PROFILE_TAB}
          props={this.props}
          data={this.state.personalData}
          singleTravellerData={this.state.singleTravellerData}
          setData={this.setData}
        />
        {this.props.route.params?.updateProfile ? (
          <TouchableOpacity
            onPress={() => this.props.navigation.replace('HomeStack')} 
            style={{
              alignItems: 'center',
              justifyContent: 'center',
              height: 50,
              borderRadius: 10,
              width: wp(90),
              marginTop: 10,
              backgroundColor: COLOR.GREEN,
              alignSelf: 'center',
            }}>
            <Text style={[Styles.button_font, {color: COLOR.WHITE}]}>
              {'Skip'}
            </Text>
          </TouchableOpacity>
        ) : null}
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  skipView: {
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
    height: 50,
    width: 45,
  },
});

const mapStateToProps = (state, ownProps) => {
  return {
    singleTravellerData: state.profileReducer.singleTravellerData,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getSingleTraveller: (val) => {
      dispatch(ProfileAction.getSingleTraveller(val));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ProfileDetail);
