import React from 'react';
import {Modal, Text, View, StyleSheet, Image} from 'react-native';
import {TouchableOpacity} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import COLOR from '../../styles/Color';
import FONTS from '../../styles/Fonts';
import Styles from '../../styles/Styles';
import NavigationBar from '../../commonView/NavigationBar';
import moment from 'moment';
import IMAGES from '../../styles/Images';

export default class CompleteProfile extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: true,
    };
  }

  componentDidMount() {}

  componentDidUpdate(prevProps) {}

  modalBack = () => {
    this.setState({modalVisible: false}, (val) => {
      this.props.goToProfile();
    });
  };

  render() {
    return (
      <View style={Styles.container}>
        <Modal
          animationType={'slide'}
          transparent={true}
          style={{backgroundColor: COLOR.BLACK}}
          visible={this.state.modalVisible}
          onRequestClose={() => {
            console.log('Modal has been closed.');
          }}>
          <View style={styles.modal}>
            <View style={styles.modalBody}>
              <NavigationBar
                handleBack={() =>
                  this.props.modal ? this.props.hideModal() : null
                }
                prop={this.props.props}
                navHeight={40}
                name={'Notice'}
              />
              <View style={{flex: 1, alignItems: 'center'}}>
                <Image
                  resizeMode={'contain'}
                  source={IMAGES.alert}
                  style={{height: 150, width: 80}}
                />

                <Text style={[Styles.subheading_label, {textAlign: 'center'}]}>
                  Complete Your Profile!
                </Text>

                <Text
                  style={[
                    Styles.body_label,
                    {textAlign: 'center', padding: 15},
                  ]}>
                  You should complete your profile Before submitting your
                  request
                </Text>
              </View>
              <View
                style={{
                  marginVertical: 20,
                  height: 50,
                  flexDirection: 'row',
                  width: wp(90),
                  alignSelf: 'center',
                }}>
                <TouchableOpacity
                  onPress={() => this.modalBack()}
                  style={{flex: 1, marginRight: 10}}>
                  <View
                    style={{
                      flex: 1,
                      alignItems: 'center',
                      justifyContent: 'center',
                      borderRadius: 10,
                      height: 50,
                      borderColor: COLOR.BORDER,
                      borderWidth: 1,
                      backgroundColor: COLOR.WHITE,
                    }}>
                    <Text style={[Styles.button_font, {color: COLOR.BLACK}]}>
                      Go to my Profile Page
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  modal: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
  },
  modalBody: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    backgroundColor: COLOR.WHITE,
    borderWidth: 1,
    width: wp(100),
    borderColor: COLOR.BORDER,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    flex: 1,
    marginTop: 50,
  },
  image: {
    height: 120,
    width: 120,
    alignSelf: 'center',
    marginVertical: 20,
  },
  headingLabel: {
    fontFamily: FONTS.FAMILY_BOLD,
    fontSize: FONTS.MEDIUM,
    color: COLOR.BLACK,
  },
  singleLineView: {
    marginVertical: 20,
    borderBottomColor: COLOR.GRAY,
    width: wp(100),
    alignSelf: 'center',
    borderBottomWidth: 1,
  },
  borderView: {
    borderRadius: 10,
    borderWidth: 1,
    width: wp(90),
    borderColor: COLOR.GRAY,
    alignSelf: 'center',
  },
});
