import React from 'react';
import {View, Text, Image, SafeAreaView} from 'react-native';
import {FlatList, TouchableOpacity} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import COLOR from '../../styles/Color';
import IMAGES from '../../styles/Images';
import Styles from '../../styles/Styles';
import NavigationBar from '../../commonView/NavigationBar';
import {connect} from 'react-redux';
import ProfileAction from '../../../redux/action/ProfileAction';

class ProfileTraveller extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedIndex: null,
            travellersData: [],
        };
        this.props.getTravellers();
    }

    componentDidMount() {
        this.willFocusSubscription = this.props.navigation.addListener(
            'focus',
            () => {
                this.props.getTravellers();
                console.log('called');
            },
        );
        if (this.props.travellersData) {
            this.setState({travellersData: this.props.travellersData});
        }
    }

    componentWillReceiveProps(props) {
        if (props.travellersData != this.props.travellersData) {
            this.setState({
                travellersData: props.travellersData,
            });
        }
    }

    render() {
        const {selectedIndex} = this.state;
        return (
            <SafeAreaView style={Styles.container}>
                <NavigationBar
                    left_image={IMAGES.back_arrow}
                    prop={this.props}
                    height={40}
                    name={'Travellers'}
                />
                {this.props.userDetail ?
                    <>
                        <View
                            style={{
                                flexDirection: 'row',
                                alignSelf: 'center',
                                width: wp(90),
                                alignItems: 'center',
                                marginVertical: 10,
                            }}>
                            <Image
                                source={this.props.userDetail.image}
                                style={{height: 20, width: 20, marginRight: 10}}
                                resizeMode={'contain'}
                            />
                            <Text style={[Styles.body_label, {width: null, flex: 1}]}>
                                 {'Me (' + this.props.userDetail.first_name + ' ' + this.props.userDetail.last_name + ')'}
                            </Text>
                        </View>
                        <View style={Styles.line_view}/></> : null}
                <View
                    style={{
                        flex: 1,
                        width: wp(90),
                        alignSelf: 'center',
                        paddingVertical: 10,
                    }}>
                    <FlatList
                    bounces={false}
                        numColumns={1}
                        showsHorizontalScrollIndicator={false}
                        showsVerticalScrollIndicator={false}
                        data={this.state.travellersData}
                        renderItem={({item}) => (
                            <TouchableOpacity
                                onPress={() => {
                                    this.setState({selectedIndex: item.id});
                                }}>
                                <View
                                    style={{
                                        flexDirection: 'row',
                                        alignSelf: 'center',
                                        width: wp(90),
                                        alignItems: 'center',
                                        marginVertical: 10,
                                    }}>
                                    {/* <Image
                                        source={item.image}
                                        style={{height: 25, width: 25, marginRight: 10}}
                                        resizeMode={'contain'}
                                    /> */}
                                     {item.special_needs.length > 0 ? <Image
                                        source={IMAGES.yellow_heart}
                                        style={{height: 20, width: 20,marginRight: 10}}
                                        resizeMode={'contain'}
                                    /> : <Image
                                    source={item.image}
                                    style={{height: 20, width: 20, marginRight: 10}}
                                    resizeMode={'contain'}
                                />}
                                    <Text style={[Styles.body_label, {width: null, flex: 1}]}>
                                        {item.prefix + ' ' + item.first_name}
                                    </Text>
                                    {/* {item.special_needs.length > 0 && <Image
                                        source={IMAGES.yellow_heart}
                                        style={{height: 20, width: 20}}
                                        resizeMode={'contain'}
                                    />} */}
                                    <TouchableOpacity
                                        onPress={() =>
                                            item.is_default_user == 0
                                                ? this.props.navigation.navigate('TravellersDetail', {
                                                    data: item,
                                                })
                                                : this.props.navigation.navigate('ProfileDetail', {
                                                    val: 0,
                                                })
                                        }
                                        style={{
                                            height: 40,
                                            width: 40,
                                            justifyContent: 'center',
                                            alignItems: 'center',
                                        }}>
                                        <Image
                                            source={IMAGES.edit}
                                            style={{height: 20, width: 20}}
                                            resizeMode={'contain'}
                                        />
                                    </TouchableOpacity>

                                    {item.is_default_user == 0 ? (
                                        <TouchableOpacity
                                            onPress={() => {
                                                this.props.deleteTraveller(item.id);
                                            }}
                                            style={{
                                                height: 40,
                                                width: 40,
                                                justifyContent: 'center',
                                                alignItems: 'center',
                                            }}>
                                            {item.id != 1 && (
                                                <Image
                                                    source={IMAGES.cross_icon}
                                                    style={{height: 20, width: 20}}
                                                    resizeMode={'contain'}
                                                />
                                            )}
                                        </TouchableOpacity>
                                    ) : null}
                                </View>
                                <View style={Styles.line_view}/>
                            </TouchableOpacity>
                        )}
                        keyExtractor={(item, index) => index.toString()}
                    />
                    <TouchableOpacity
                        onPress={() => this.props.navigation.navigate('TravellersDetail')}
                        style={{
                            alignItems: 'center',
                            justifyContent: 'center',
                            height: 50,
                            borderRadius: 10,
                            borderColor: COLOR.GRAY,
                            borderWidth: 1,
                            width: wp(90),
                            marginVertical: 10,
                            backgroundColor: COLOR.WHITE,
                            alignSelf: 'center',
                        }}>
                        <View style={{flexDirection: 'row', alignItems: 'center'}}>
                            <View
                                style={{
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    width: 25,
                                    height: 25,
                                    borderRadius: 15,
                                    backgroundColor: COLOR.GREEN,
                                    marginRight: 10,
                                }}>
                                <Image
                                    style={{
                                        width: 15,
                                        height: 15,
                                        borderRadius: 10,
                                        tintColor: COLOR.WHITE,
                                    }}
                                    source={IMAGES.plus_image}
                                />
                            </View>
                            <Text style={[Styles.button_font, {color: COLOR.BLACK}]}>
                                Add New Traveller
                            </Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                      onPress={() => this.props.navigation.goBack()}
                      style={{
                        alignItems: 'center',
                        justifyContent: 'center',
                        height: 50,
                        borderRadius: 10,
                        width: wp(90),
                        marginVertical: 5,
                        backgroundColor: COLOR.GREEN,
                        alignSelf: 'center',
                      }}>
                      <Text style={[Styles.button_font, {color: COLOR.WHITE}]}>Done</Text>
                    </TouchableOpacity>
                </View>
            </SafeAreaView>
        );
    }
}

mapDispatchToProps = (dispatch) => {
    return {
        getTravellers: () => {
            dispatch(ProfileAction.getTravellers());
        },
        deleteTraveller: (val) => {
            dispatch(ProfileAction.deleteTraveller(val));
        },
        getUserInfo: () => {
            dispatch(ProfileAction.getUserInfo())
        }
    };
};

mapStateToProps = (state, ownState) => {
    return {
        travellersData: state.profileReducer.travellersData,
        userDetail: state.profileReducer.userDetail
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(ProfileTraveller);
