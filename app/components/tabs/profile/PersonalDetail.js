import React from 'react';
import {View, Text, Image, SafeAreaView, StyleSheet} from 'react-native';
import {ScrollView, TextInput, TouchableOpacity} from 'react-native';
import IMAGES from '../../styles/Images';
import Styles from '../../styles/Styles';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import COLOR from '../../styles/Color';
import {connect} from 'react-redux';
import ProfileAction from '../../../redux/action/ProfileAction';
import CountryPicker from 'react-native-country-picker-modal';
import SelectDate from '../calendar/SelectDate';
import SimpleToast from 'react-native-simple-toast';
import DropDownPicker from 'react-native-dropdown-picker';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview';
import FONTS from '../../styles/Fonts';
import moment from 'moment';
import API from '../../../api/Api';
import {NameGuideline} from './NameGuidelines';
import { DoneButtonKeyboard } from '../../commonView/CommonView';

const api = new API();
class PersonalDetail extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      prefix: '',
      tripData: [],
      userDetail: null,
      first_name: '',
      middle_name: '',
      last_name: '',
      dob: '',
      email: null,
      country_id: '91',
      phone: null,
      title: '',
      is_primary_traveller: 1,
      id: null,
      openCalendar: false,
      specialNeeds: [],
      selectedSpecialNeeds: [],
      titleDropdown: false,
      openNameGuidelines: false,
    };
    this.props.getSpecialNeeds();
  }

  updateSpecialNeeds = (value) => {
    this.setState({selectedSpecialNeeds: value}, () => {
      this.updateProfile(false);
    });
  };

  componentDidMount() {
    this.props.getUserDetail();
  }

  componentWillReceiveProps(props) {
    if (props.userDetail != this.state.userDetail) {
      this.setState({
        userDetail: props.userDetail,
        first_name: props.userDetail ? props.userDetail.first_name : '',
        last_name: props.userDetail ? props.userDetail.last_name : '',
        middle_name: props.userDetail ? props.userDetail.middle_name : '',
        dob: props.userDetail ? props.userDetail.dob : '',
        email: props.userDetail ? props.userDetail.email : '',
        country_id: props.userDetail
          ? props.userDetail.country_id
            ? props.userDetail.country_id.toString()
            : ''
          : '',
        phone: props.userDetail ? props.userDetail.phone : '',
        title: props.userDetail
          ? props.userDetail.primary_traveller
            ? props.userDetail.primary_traveller.prefix
            : ''
          : '',
        selectedSpecialNeeds: props.userDetail
          ? props.userDetail.primary_traveller
            ? props.userDetail.primary_traveller.special_needs.map((value) => {
                let data = value;
                data.comment = value.pivot.comment;
                return data;
              })
            : ''
          : '',
      });
    }

    if (this.props.specialNeeds != props.specialNeeds) {
      this.setState({
        specialNeeds: props.specialNeeds,
      });
    }
  }

  updateProfile = (val) => {
    if (this.state.first_name == null && this.state.first_name == '') {
      SimpleToast.show('Enter First Name');
    } else if (this.state.last_name == null && this.state.last_name == '') {
      SimpleToast.show('Enter Last Name');
    } else if (this.state.dob == null) {
      SimpleToast.show('Select Date of birth');
    } else if (this.state.title == '') {
      SimpleToast.show('Select title');
    } else if (this.state.email == '') {
      SimpleToast.show('Enter Email Address');
    } else if (this.state.phone == '') {
      SimpleToast.show('Enter Phone number');
    } else if (this.state.prefix == null) {
      SimpleToast.show('Enter Prefix');
    } else {
      const data = JSON.stringify({
        first_name: this.state.first_name,
        middle_name: this.state.middle_name,
        last_name: this.state.last_name,
        dob: this.state.dob,
        country_id: parseInt(this.state.country_id),
        email: this.state.email,
        phone: this.state.phone,
        prefix: this.state.title,
        special_needs: this.state.selectedSpecialNeeds,
        is_primary_traveller: 1,
      });
      this.updateProfileApi(data, val);
    }
  };

  updateProfileApi = (data, val) => {
    let that = this;
    api
      .updateProfile(data)
      .then((json) => {
        console.log('update responce:-', json.data);
        let data = JSON.stringify(json.data);
        that.props.getUserDetail();
        SimpleToast.show('Profile Updated');
        val ? that.props.jumpTo('second') : null;
      })
      .catch((error) => {
        setTimeout(() => {
          console.log('json.data.message', error);
          SimpleToast.show(String(error.response.data.message));
        }, 0);
        console.log('error:-', error.response.data.message);
      });
  };

  commonView = (props) => {
    return (
      <>
        <Text style={[classStyle.title_Label]}>{props.name}</Text>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            width: null,
            alignItems: 'center',
            height: 40,
          }}>
          {console.log('this.props', this.props)}
          <TextInput
          inputAccessoryViewID={'uniqueID'}
            placeholderTextColor={COLOR.LIGHT_TEXT_COLOR}
            style={[Styles.body_label, {width: null, flex: 1}]}
            editable={
              props.label == 'email' &&
              (this.props.userDetail ? this.props.userDetail.email : '')
                ? false
                : true
            }
            placeholder={props.name}
            onChangeText={(text) => this.setState({[props.label]: text})}
            value={props.input}
            keyboardType={props.keyboardType}
          />
          <DoneButtonKeyboard />
          <Image
            style={{
              height: 10,
              width: 10,
              tintColor: COLOR.VOILET,
              marginHorizontal: 5,
            }}
            source={props.drop_image}
          />
        </View>
      </>
    );
  };

  updateMasterState = (attrName, value) => {
    this.setState({[attrName]: value});
  };

  render() {
    return (
      <SafeAreaView style={Styles.container}>
        {this.state.openCalendar ? (
          <SelectDate
            modal={true}
            allowRangeSelection={false}
            attrName={'dob'}
            maxDate={new Date()}
            minDate={new Date(0)}
            updateMasterState={this.updateMasterState}
            props={this.props.props}
          />
        ) : null}
        <KeyboardAwareScrollView
         bounces={false}
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{flexGrow: 1}}>
          <View style={[Styles.mainView, {marginTop: 20}]}>
            <View style={{marginBottom: 20}}>
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <TouchableOpacity
                  onPress={() => {
                    this.props.navigation.navigate('NameGuideline', {
                      userDetail: this.state.userDetail,
                    });
                  }}>
                  <Text
                    style={[
                      Styles.small_label,
                      {color: COLOR.GREEN, width: null, marginRight: 5},
                    ]}>
                    Passenger name guidelines
                  </Text>
                </TouchableOpacity>
                <Image
                  source={IMAGES.info}
                  style={{height: 10, width: 10, resizeMode: 'contain'}}
                />
              </View>
              <Text style={[Styles.small_label, {marginRight: 5}]}>
                *Your name must match your passport
              </Text>
            </View>

            <View style={{flexDirection: 'row', zIndex: 80}}>
              <View style={{width: wp(25), marginRight: 10, zIndex: 90}}>
                <Text style={[classStyle.title_Label]}>Title</Text>
                <DropDownPicker
                  showArrowIcon={true}
                  open={this.state.titleDropdown}
                  setOpen={() =>
                    this.setState({
                      titleDropdown: !this.state.titleDropdown,
                    })
                  }
                  zIndex={100}
                  labelStyle={{
                    fontSize: FONTS.MEDIUM,
                    fontFamily: FONTS.FAMILY_REGULAR,
                    color: COLOR.BLACK,
                    marginLeft: -5,
                  }}
                  dropDownMaxHeight={300}
                  style={{
                    backgroundColor: '#ffffff',
                    shadowOpacity: 0,
                    borderWidth: 0,
                    height: 40,
                    alignSelf: 'center',
                    borderTopLeftRadius: 0,
                    borderTopRightRadius: 0,
                    borderBottomLeftRadius: 0,
                    borderBottomRightRadius: 0,
                  }}
                  dropDownStyle={{
                    backgroundColor: '#ffffff',
                  }}
                  containerStyle={{
                    borderRadius: 0,
                    backgroundColor: COLOR.WHITE,
                  }}
                  dropDownContainerStyle={{
                    backgroundColor: COLOR.WHITE,
                    zIndex: 100,
                  }}
                  setValue={(callback) =>
                    this.setState((state) => ({
                      title: callback(state.value),
                    }))
                  }
                  value={this.state.title}
                  textStyle={{
                    fontFamily: FONTS.FAMILY_REGULAR,
                    fontSize: 16,
                  }}
                  placeholder={'Title'}
                  labelStyle={{
                    fontFamily: FONTS.FAMILY_REGULAR,
                    fontSize: 16,
                  }}
                  items={[
                    {label: 'Mrs', value: 'Mrs'},
                    {label: 'Mr', value: 'Mr'},
                    {label: 'Ms', value: 'Ms'},
                  ]}
                />
                <View
                  style={[
                    Styles.line_view,
                    {marginVertical: 10, width: wp(25)},
                  ]}
                />
              </View>
              <View style={{flex: 1}}>
                <this.commonView
                  name={'First Name'}
                  input={this.state.first_name}
                  label={'first_name'}
                />
                <View
                  style={[
                    Styles.line_view,
                    {marginVertical: 10, width: wp(60)},
                  ]}
                />
              </View>
            </View>

            <this.commonView
              name={'Middle Name'}
              input={this.state.middle_name}
              label={'middle_name'}
            />
            <View
              style={[Styles.line_view, {marginVertical: 10, width: wp(90)}]}
            />
            <this.commonView
              name={'Last/Family Name'}
              input={this.state.last_name}
              image={IMAGES.settings}
              label={'last_name'}
            />
            <View
              style={[Styles.line_view, {marginVertical: 10, width: wp(90)}]}
            />
            <Text style={[classStyle.title_Label]}>Date of birth</Text>
            <TouchableOpacity
              onPress={() => this.setState({openCalendar: true})}
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                width: null,
                alignItems: 'center',
                height: 40,
              }}>
              <Text style={[Styles.body_label, {width: null, flex: 1}]}>
                {this.state.dob == '' || this.state.dob == null
                  ? 'Select Date of birth'
                  : this.state.dob}
              </Text>
              <Image
                style={{
                  height: 10,
                  width: 10,
                  tintColor: COLOR.VOILET,
                  marginHorizontal: 5,
                }}
                source={IMAGES.down_arrow}
              />
            </TouchableOpacity>
            <View
              style={[Styles.line_view, {marginVertical: 10, width: wp(90)}]}
            />

            <View>
              <this.commonView
                name={'Email Address'}
                image={IMAGES.support}
                input={this.state.email}
                label={'email'}
              />
              <View
                style={[Styles.line_view, {marginVertical: 10, width: wp(90)}]}
              />
              <View style={{flexDirection: 'row'}}>
                <View style={{width: wp(20), marginRight: 10}}>
                  <Text style={[classStyle.title_Label]}>Code</Text>
                  <CountryPicker
                  withAlphaFilter={true}
                    withCallingCode={true}
                    withFlag={true}
                    visible={this.state.pickerVisible}
                    onSelect={(country) => {
                      this.setState({
                        country_id: country.callingCode,
                        pickerVisible: false,
                      });
                    }}
                    onClose={()=>{
                      this.setState({pickerVisible:false})
                    }}
                    renderFlagButton={(country) => {
                      return (
                        <TouchableOpacity
                          onPress={() => this.setState({pickerVisible: true})}
                          style={{
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            width: null,
                            alignItems: 'center',
                            height: 40,
                          }}>
                          <Text
                            style={[Styles.body_label, {width: null, flex: 1}]}>
                            {this.state.country_id ? this.state.country_id : ''}
                          </Text>
                          <Image
                            style={{
                              height: 10,
                              width: 10,
                              tintColor: COLOR.VOILET,
                              marginHorizontal: 5,
                            }}
                            source={IMAGES.down_arrow}
                          />
                        </TouchableOpacity>
                      );
                    }}
                  />

                  <View
                    style={[
                      Styles.line_view,
                      {marginVertical: 10, width: wp(20)},
                    ]}
                  />
                </View>
                <View style={{flex: 1}}>
                  <this.commonView
                    name={'Mobile Number'}
                    input={this.state.phone}
                    label={'phone'}
                    keyboardType={'number-pad'}
                  />
                  <View
                    style={[
                      Styles.line_view,
                      {marginVertical: 10, width: wp(65)},
                    ]}
                  />
                </View>
              </View>
            </View>
          </View>
        </KeyboardAwareScrollView>
        <TouchableOpacity
          onPress={() =>
            this.props.navigation.navigate('SpecialNeeds', {
              specialNeeds: this.state.specialNeeds,
              updateSpecialNeeds: this.updateSpecialNeeds,
              selectedSpecialNeeds: this.state.selectedSpecialNeeds,
            })
          }
          style={{
            alignItems: 'center',
            justifyContent: 'center',
            height: 50,
            borderRadius: 10,
            borderColor: COLOR.GRAY,
            borderWidth: 1,
            width: wp(90),
            marginTop: 10,
            marginBottom: 20,
            backgroundColor: COLOR.WHITE,
            alignSelf: 'center',
          }}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Image
              source={IMAGES.yellow_heart}
              style={{height: 20, width: 20, marginRight: 5}}
              resizeMode={'contain'}
            />
            <Text style={[Styles.button_font, {color: COLOR.BLACK}]}>
              Add Special Needs
            </Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => this.updateProfile(true)} //this.props.navigation.navigate('NameGuideline')}
          ref={this.props.nextBtn}
          style={{
            alignItems: 'center',
            justifyContent: 'center',
            height: 50,
            borderRadius: 10,
            width: wp(90),
            marginVertical: 10,
            backgroundColor: COLOR.GREEN,
            alignSelf: 'center',
          }}>
          <Text style={[Styles.button_font, {color: COLOR.WHITE}]}>
            {'Save'}
          </Text>
        </TouchableOpacity>
      </SafeAreaView>
    );
  }
}

const classStyle = StyleSheet.create({
  flatlist_view: {
    marginTop: 0,
    width: wp(100),
    marginBottom: 0,
    height: hp(50),
    marginHorizontal: 10,
  },
  flatlist_image_view: {
    height: hp(50),
    width: wp(70),
    marginRight: 0,
  },
  offer_view: {
    marginVertical: 0,
    width: wp(100),
    marginBottom: 20,
    height: 100,
    marginHorizontal: 10,
  },
  title_Label: {
    fontFamily: FONTS.FAMILY_REGULAR,
    fontSize: FONTS.SMALL,
    color: COLOR.BLACK,
    marginVertical: 5,
  },
});

mapStateToProps = (state, ownProps) => {
  return {
    userDetail: state.profileReducer.userDetail,
    specialNeeds: state.profileReducer.specialNeeds,
  };
};

mapDispatchToProps = (dispatch) => {
  return {
    getUserDetail: (val) => {
      dispatch(ProfileAction.getUserInfo(val));
    },
    updateProfile: (data, val, id) => {
      dispatch(ProfileAction.updateProfile(data, val, id));
    },
    getSpecialNeeds: () => {
      dispatch(ProfileAction.getSpecialNeeds());
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(PersonalDetail);
