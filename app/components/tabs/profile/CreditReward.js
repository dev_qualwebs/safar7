import React from 'react';
import {
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  ImageBackground,
  Alert,
} from 'react-native';
import {FlatList, ScrollView, TouchableWithoutFeedback} from 'react-native';
import IMAGES from '../../styles/Images';
import Styles from '../../styles/Styles';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import COLOR from '../../styles/Color';
import NavigationBar from '../../commonView/NavigationBar';
import {TouchableOpacity} from 'react-native-gesture-handler';
import FONTS from '../../styles/Fonts';
import API from '../../../api/Api';
import {connect} from 'react-redux';
import PackageAction from '../../../redux/action/PackageAction';

const api = new API();

class CreditRewards extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      creditData: '',
    };
  }

  componentDidMount() {
    // this.getCreditData();
    if (this.props.creditData) {
      this.setState({creditData: this.props.creditData});
    } else {
      this.props.getCreditData();
    }
  }

  componentDidUpdate(prevProps, prevState) {
if(this.props.creditData != prevProps.creditData){
this.setState({creditData:this.props.creditData})
}  
  }

  getCreditData() {
    api
      .getCreditData()
      .then((json) => {
        this.setState({creditData: json.data.response}, () => {
          console.log('this.state.creditData', this.state.creditData);
        });
      })
      .catch(function (error) {
        console.log(error.response.data);
      });
  }

  render() {
    return (
      <View style={Styles.container}>
        <SafeAreaView>
          <NavigationBar
            prop={this.props}
            height={40}
            right_image={IMAGES.filter_image}
            left_image={IMAGES.back_arrow}
            rightNav={'ReferFriend'}
          />
          <View style={Styles.mainView}>
            <View
              style={{
                height: 120,
                width: wp(90),
                marginVertical: 20,
                borderRadius: 10,
              }}>
              <ImageBackground
                source={IMAGES.credit_card_view}
                resizeMode={'cover'}
                style={[
                  Styles.backgroundImage,
                  {
                    borderRadius: 10,
                    alignSelf: 'center',
                    alignItems: 'center',
                    backgroundColor: COLOR.LIGHT_TEXT,
                    height: 120,
                    width: wp(90),
                    padding: 15,
                  },
                ]}>
                <Text
                  style={[
                    Styles.body_label,
                    {
                      color: COLOR.WHITE,
                      alignSelf: 'flex-start',
                    },
                  ]}>
                  CREDIT
                </Text>
                <Text
                  style={[
                    Styles.splash_heading_label,
                    {
                      color: COLOR.WHITE,
                      alignSelf: 'flex-start',
                      fontSize: 50,
                    },
                  ]}>
                  {this.props.userDetail && this.props.userDetail.credit
                    ? this.props.userDetail.credit
                    : '0'}
                </Text>
              </ImageBackground>
            </View>
          </View>
          <Text
            style={[Styles.subheading_label, {fontFamily: FONTS.FAMILY_BOLD}]}>
            Use Trip Credits
          </Text>
          <Text style={[Styles.small_label, {marginBottom: 20}]}>
            Credits can be used to save money on your next trip 1 Credit = 1 SAR
          </Text>
          <FlatList
            bounces={false}
            showsVerticalScrollIndicator={false}
            style={[classStyle.flatlist_view, {height: null, width: wp(90)}]}
            data={this.state.creditData}
            renderItem={({item}) => (
              <View style={classStyle.offer_view}>
                <View
                  style={{
                    flexDirection: 'row',
                    width: wp(90),
                    alignItems: 'center',
                    justifyContent: 'space-between',
                  }}>
                  <Text
                    style={[
                      Styles.body_label,
                      {width: 120, fontFamily: FONTS.FAMILY_BOLD, flex: 1},
                    ]}>
                    {item.total_credit} Credits
                  </Text>
                  <Text
                    style={[
                      Styles.small_label,
                      {
                        width: null,
                        flex: 1,
                        textAlign: 'left',
                        alignSelf: 'center',
                        alignItems: 'flex-end',
                        marginRight: 10,
                      },
                    ]}>
                    {item.name}
                  </Text>
                </View>
                <View style={[Styles.line_view, {marginVertical: 20}]} />
              </View>
            )}
            numColumns={1}
            showsHorizontalScrollIndicator={false}
            keyExtractor={(item, index) => index.toString()}
          />
        </SafeAreaView>
      </View>
    );
  }
}

const classStyle = StyleSheet.create({
  flatlist_view: {
    marginTop: 20,
    width: wp(100),
    marginBottom: 0,

    marginHorizontal: 10,
  },
  flatlist_image_view: {
    height: hp(50),
    width: wp(70),
    marginRight: 0,
  },
  offer_view: {
    marginVertical: 0,
    width: wp(100),
    marginHorizontal: 10,
  },
});

mapStateToProps = (state, ownProps) => {
  return {
    userDetail: state.profileReducer.userDetail,
    creditData: state.pacakgeReducer.creditData,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getCreditData: () => {
      dispatch(PackageAction.getCreditsData());
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(CreditRewards);
