import React from 'react';
import {View, Image, Text, SafeAreaView, StyleSheet, Alert} from 'react-native';
import {
  TextInput,
  TouchableOpacity,
  TouchableWithoutFeedback,
} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview';
import {heightPercentageToDP, widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {connect} from 'react-redux';
import ProfileAction from '../../../redux/action/ProfileAction';
import NavigationBar from '../../commonView/NavigationBar';
import {SelectionView} from '../../commonView/SelectionView';
import COLOR from '../../styles/Color';
import FONTS from '../../styles/Fonts';
import IMAGES from '../../styles/Images';
import Styles from '../../styles/Styles';
import {getSettingPrefs} from '../../../helper/Constants';
import API from '../../../api/Api';

const api = new API();

class ProfileSetting extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      language: '',
      currency: '',
      units: '',
      temperature: '',
      navigationApps: '',
      timeFormat: '',
      selectedItem: null,
      selectionData: null,
      modalVisible: false,
      title: '',
      selectedPref: null,
      currentLanguage: 1,
      currentCurrency: null,
      currentTemperature: null,
      currentUnit: null,
      currentStartTime: null,
      currentCalendarSync: null,
      currentWifiOnly: null,
      currentTimeFormat: null,
      currentNavigationApp: null,
    };
    this.props.getLanguages();
    this.props.getTimeFormat();
    this.props.getNavigationApps();
    this.props.getTemperature();
    this.props.getUnits();
    this.props.getCurrencies();
    this.props.getPreferenceSetting();
  }

  handleSelection = (val) => {
    this.setState({
      selectedItem: val,
    });
    switch (this.state.selectedPref) {
      case 1: {
        let index = this.props.languages
          .map(function (value) {
            return value.id;
          })
          .indexOf(val);
        this.setState(
          {
            currentLanguage: val,
            language: this.props.languages[index].name,
          },
          () => this.savePreferenceSetting(),
        );
        break;
      }
      case 2: {
        let index = this.props.currencies
          .map(function (value) {
            return value.id;
          })
          .indexOf(val);
        this.setState(
          {
            currentCurrency: val,
            currency: this.props.currencies[index].name,
          },
          () => {
            this.savePreferenceSetting()},
        );
        break;
      }
      case 3: {
        let index = this.props.units
          .map(function (value) {
            return value.id;
          })
          .indexOf(val);
        this.setState(
          {
            currentUnit: val,
            units: this.props.units[index].name,
          },
          () => this.savePreferenceSetting(),
        );
        break;
      }
      case 4: {
        let index = this.props.temperature
          .map(function (value) {
            return value.id;
          })
          .indexOf(val);
        this.setState(
          {
            currentTemperature: val,
            temperature: this.props.temperature[index].name,
          },
          () => this.savePreferenceSetting(),
        );
        break;
      }
      case 5: {
        let value = this.props.timeFormat.filter(function (value) {
          if (value.id == val) {
            return value;
          }
        });
        this.setState(
          {
            currentTimeFormat: val,
            timeFormat: value[0].name,
          },
          () => this.savePreferenceSetting(),
        );
        break;
      }
      case 6: {
        let index = this.props.navigationApps
          .map(function (value) {
            return value.id;
          })
          .indexOf(val);
        this.setState(
          {
            currentNavigationApp: val,
            navigationApps: this.props.navigationApps[index].name,
          },
          () => this.savePreferenceSetting(),
        );
        console.log('departureTime', this.state.departureTime);
        break;
      }
    }
  };

  handleModalVisibility = () => {
    this.setState({
      modalVisible: false,
      selectedItem: null,
    });
  };

  handlePreferenceSelection = (pref) => {
    let obj = getSettingPrefs(pref);
    obj.selectionData = this.props[obj.label];
    this.setState(obj);
  };

  componentDidUpdate(prevProps) {
    if (prevProps.setting != this.props.setting) {
      this.setState({
        language: this.props.setting.language.name,
        currency: this.props.setting.currency.iso4217_code,
        units: this.props.setting.unit.name,
        temperature: this.props.setting.temperature.name,
        navigationApps: this.props.setting.navigation.name,
        timeFormat: this.props.setting.time_format.name,
        currentLanguage: this.props.setting.language_id,
        currentCurrency: this.props.setting.currency_id,
        currentTemperature: this.props.setting.temperature.id,
        currentUnit: this.props.setting.unit_id,
        currentStartTime: this.props.setting.trip_start_time,
        currentCalendarSync: this.props.setting.calendar_sync,
        currentWifiOnly: this.props.setting.wiFy_only,
        currentTimeFormat: this.props.setting.time_format_id,
        currentNavigationApp: this.props.setting.navigation_app_id,
      });
    }
  }

  savePreferenceSetting = () => {
    let data = JSON.stringify({
      language_id: this.state.currentLanguage,
      currency_id: this.state.currentCurrency,
      temperature_id: this.state.currentTemperature,
      unit_id: this.state.currentUnit,
      trip_start_time: '12:00:00',
      calendar_sync: 0,
      wiFy_only: 1,
      time_format_id: this.state.currentTimeFormat,
      navigation_app_id: this.state.currentNavigationApp,
    });
    this.props.savePreferenceSetting(data);
  };

  render() {
    return (
      <SafeAreaView style={Styles.container}>
     { this.state.modalVisible ? 
        <SelectionView
            title={this.state.title}
            showBottomNote={false}
            showImage={false}
            showFilter={false}
            handleSelection={this.handleSelection}
            selectedItem={this.state.selectedItem}
            data={this.state.selectionData}
            props={this.props}
            handleModalVisibility={() => this.handleModalVisibility()}
            modalVisible={this.state.modalVisible}
            />
      : null}
        <NavigationBar
          left_image={IMAGES.back_arrow}
          prop={this.props}
          height={40}
          name={'Settings'}
        />
        <KeyboardAwareScrollView
        bounces={false}
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}>
          <View style={{width: wp(90), alignSelf: 'center'}}>
            <Text style={[Styles.subheading_label, {marginVertical: 10}]}>
              Password
            </Text>
            <View style={{width: wp(90), flexDirection: 'row', alignSelf: 'center'}}>
              <TouchableOpacity
              onPress={()=>{
                this.props.navigation.navigate('NewPassword');
              }}
              >
              <View style={style.horizontal_view}>
                  <Text style={style.text_label}>Change Password</Text>
                 
                </View>
              </TouchableOpacity>
            </View>
            <View style={[Styles.line_view, {width: wp(90)}]}/>
            <Text style={[Styles.subheading_label, {marginVertical: 10}]}>
              Personal Preference
            </Text>
            <View>
              <TouchableOpacity
                onPress={() => {
                  this.setState({selectedItem : this.state.currentLanguage},()=>{
                    this.handlePreferenceSelection(1);
                  });
                  }}>
                <View style={style.horizontal_view}>
                  <Text style={style.text_label}>Language</Text>
                  <Text style={style.text_label}>{this.state.language}</Text>
                </View>
              </TouchableOpacity>
              <View
                style={[Styles.line_view, {width: wp(90), marginVertical: 10}]}
              />
            </View>
            <View>
              <TouchableOpacity
                onPress={() => {
                  this.setState({selectedItem : this.state.currentCurrency},()=>{
                  this.handlePreferenceSelection(2);
                  });
                }}>
                <View style={style.horizontal_view}>
                  <Text style={style.text_label}>Currency</Text>
                  <Text style={style.text_label}>{this.state.currency}</Text>
                </View>
              </TouchableOpacity>
              <View
                style={[Styles.line_view, {width: wp(90), marginVertical: 10}]}
              />
            </View>
            <View>
              <TouchableOpacity
                onPress={() => {
                  this.setState({selectedItem : this.state.currentUnit},()=>{
                  this.handlePreferenceSelection(3);
                  });
                }}>
                <View style={style.horizontal_view}>
                  <Text style={style.text_label}>Units</Text>
                  <Text style={style.text_label}>{this.state.units}</Text>
                </View>
              </TouchableOpacity>
              <View
                style={[Styles.line_view, {width: wp(90), marginVertical: 10}]}
              />
            </View>
            <View>
              <TouchableOpacity
                onPress={() =>{
                  this.setState({selectedItem : this.state.currentTemperature},()=>{
                  this.handlePreferenceSelection(4);
                  });
                }}>
                <View style={style.horizontal_view}>
                  <Text style={style.text_label}>Temperature</Text>
                  <Text style={style.text_label}>{this.state.temperature}</Text>
                </View>
              </TouchableOpacity>
              <View
                style={[Styles.line_view, {width: wp(90), marginVertical: 10}]}
              />
            </View>
            <View>
              <View style={style.horizontal_view}>
                <View>
                  <Text style={style.text_label}>Calendar Sync</Text>
                  <Text style={[Styles.small_label, {width: null}]}>
                    Sync your trip with your device calendar
                  </Text>
                </View>
                <TouchableOpacity>
                  <Image
                    style={{height: 30, width: 45}}
                    source={IMAGES.radio_button_on}
                    resizeMode={'contain'}
                  />
                </TouchableOpacity>
              </View>
              <View
                style={[Styles.line_view, {width: wp(90), marginVertical: 10}]}
              />
            </View>
            <View>
              <View style={style.horizontal_view}>
                <View>
                  <Text style={style.text_label}>User Wi-Fi only</Text>
                  <Text style={[Styles.small_label, {width: null}]}>
                    Avoid using cellular data while browsing
                  </Text>
                </View>
                <TouchableOpacity>
                  <Image
                    style={{height: 30, width: 45}}
                    source={IMAGES.radio_button_on}
                    resizeMode={'contain'}
                  />
                </TouchableOpacity>
              </View>
              <View
                style={[Styles.line_view, {width: wp(90), marginVertical: 10}]}
              />
            </View>
            <Text style={[Styles.subheading_label, {marginVertical: 10}]}>
              Trip Settings
            </Text>
            <View>
              <View style={style.horizontal_view}>
                <Text style={style.text_label}>Trip Start Time</Text>
                <Text style={style.text_label}>00:00</Text>
              </View>
              <View
                style={[Styles.line_view, {width: wp(90), marginVertical: 10}]}
              />
            </View>
            <View>
              <TouchableOpacity
                onPress={() =>{
                  this.setState({selectedItem : this.state.currentTimeFormat},()=>{
                  this.handlePreferenceSelection(5);
                  });
                }}>
                <View style={style.horizontal_view}>
                  <Text style={style.text_label}>Time Format</Text>
                  <Text style={style.text_label}>{this.state.timeFormat}</Text>
                </View>
              </TouchableOpacity>
              <View
                style={[Styles.line_view, {width: wp(90), marginVertical: 10}]}
              />
            </View>
            <View>
              <TouchableOpacity
                onPress={() =>{
                  this.setState({selectedItem : this.state.currentNavigationApp},()=>{
                  this.handlePreferenceSelection(6);
                  });
                }}>
                <View style={style.horizontal_view}>
                  <Text style={style.text_label}>Navigation App</Text>
                  <Text style={style.text_label}>
                    {this.state.navigationApps}
                  </Text>
                </View>
              </TouchableOpacity>
              <View
                style={[Styles.line_view, {width: wp(90), marginVertical: 10}]}
              />
            </View>
            <Text style={[Styles.subheading_label, {marginVertical: 10}]}>
              Legal Information
            </Text>
            <View>
              <TouchableOpacity 
              onPress={()=>{
                this.props.navigation.navigate('PolicyPage',{screenTitle:'Terms and Condition'})
              }}
              >
              <View style={style.horizontal_view}>
                <Text style={style.text_label}>Terms and Condition</Text>
                <Image
                  style={{height: 15, width: 15}}
                  source={IMAGES.right_arrow}
                />
              </View>
              </TouchableOpacity>
              <View
                style={[Styles.line_view, {width: wp(90), marginVertical: 10}]}
              />
            </View>
            <View>
            <TouchableOpacity 
              onPress={()=>{
                this.props.navigation.navigate('PolicyPage',{screenTitle:'Privacy Policy'})
              }}
              >
              <View style={style.horizontal_view}>
                <Text style={style.text_label}>Privacy Policy</Text>
                <Image
                  style={{height: 15, width: 15}}
                  source={IMAGES.right_arrow}
                />
              </View>
              </TouchableOpacity>
              <View
                style={[Styles.line_view, {width: wp(90), marginVertical: 10}]}
              />
            </View>
            <View>
            <TouchableOpacity 
              onPress={()=>{
                this.props.navigation.navigate('PolicyPage',{screenTitle:'Cookies Policy'})
              }}
              >
              <View style={style.horizontal_view}>
                <Text style={style.text_label}>Cookies Policy</Text>
                <Image
                  style={{height: 15, width: 15}}
                  source={IMAGES.right_arrow}
                />
              </View>
              </TouchableOpacity>
              <View
                style={[Styles.line_view, {width: wp(90), marginVertical: 10}]}
              />
            </View>
            <View>
            <TouchableOpacity 
              onPress={()=>{
                this.props.navigation.navigate('PolicyPage',{screenTitle:'Acknowledgement'})
              }}
              >
              <View style={style.horizontal_view}>
                <Text style={style.text_label}>Acknowledgement</Text>
                <Image
                  style={{height: 15, width: 15}}
                  source={IMAGES.right_arrow}
                />
              </View>
              </TouchableOpacity>
              <View
                style={[Styles.line_view, {width: wp(90), marginVertical: 10}]}
              />
            </View>
            <View>
              <TouchableWithoutFeedback
                onPress={() =>
                  this.props.navigation.navigate('ProfileDataSharing')
                }>
                <View style={style.horizontal_view}>
                  <Text style={style.text_label}>Data Sharing</Text>
                  <Image
                    style={{height: 15, width: 15}}
                    source={IMAGES.right_arrow}
                  />
                </View>
              </TouchableWithoutFeedback>
              <View
                style={[Styles.line_view, {width: wp(90), marginVertical: 10}]}
              />
            </View>
            <View>
            <TouchableOpacity 
              onPress={()=>{
                this.props.navigation.navigate('PolicyPage',{screenTitle:'About Us'})
              }}
              >
              <View style={style.horizontal_view}>
                <Text style={style.text_label}>About Us</Text>
                <Image
                  style={{height: 15, width: 15}}
                  source={IMAGES.right_arrow}
                />
              </View>
              </TouchableOpacity>
              <View
                style={[Styles.line_view, {width: wp(90), marginVertical: 10}]}
              />
            </View>
            <View>
            <TouchableOpacity 
              onPress={()=>{
                this.props.navigation.navigate('PolicyPage',{screenTitle:'Best Price Guarantee'})
              }}
              >
              <View style={style.horizontal_view}>
                <Text style={style.text_label}>Best Price Guarantee</Text>
                <Image
                  style={{height: 15, width: 15}}
                  source={IMAGES.right_arrow}
                />
              </View>
              </TouchableOpacity>
              <View
                style={[Styles.line_view, {width: wp(90), marginVertical: 10}]}
              />
            </View>
          </View>
        </KeyboardAwareScrollView>
      </SafeAreaView>
    );
  }
}

const style = StyleSheet.create({
  horizontal_view: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginVertical: 10,
  },
  text_label: {
    fontFamily: FONTS.FAMILY_REGULAR,
    fontSize: FONTS.REGULAR,
    color: COLOR.VOILET,
  },
});

mapStateToProps = (state, ownProps) => {
  return {
    timeFormat: state.profileReducer.timeFormat,
    languages: state.profileReducer.languages,
    currencies: state.profileReducer.currencies,
    units: state.profileReducer.units,
    temperature: state.profileReducer.temperature,
    navigationApps: state.profileReducer.navigationApps,
    setting: state.profileReducer.setting,
  };
};

mapDispatchToProps = (dispatch) => {
  return {
    getLanguages: (val) => {
      dispatch(ProfileAction.getLanguages(val));
    },
    getUnits: (val) => {
      dispatch(ProfileAction.getUnits(val));
    },
    getCurrencies: (val) => {
      dispatch(ProfileAction.getCurrencies(val));
    },
    getNavigationApps: (val) => {
      dispatch(ProfileAction.getNavigationApps(val));
    },
    getTemperature: (val) => {
      dispatch(ProfileAction.getTemperature(val));
    },
    getTimeFormat: (val) => {
      dispatch(ProfileAction.getTimeFormat(val));
    },
    savePreferenceSetting: (data) => {
      dispatch(ProfileAction.savePreferenceSetting(data));
    },
    getPreferenceSetting: (val) => {
      dispatch(ProfileAction.getPreferenceSetting(val));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ProfileSetting);
