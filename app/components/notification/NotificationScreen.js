import React from 'react';
import {
  View,
  Text,
  SafeAreaView,
  Image,
  StyleSheet,
  ImageBackground,
  Modal,
  Alert,
} from 'react-native';
import {
  FlatList,
  ScrollView,
  TouchableOpacity,
  TouchableWithoutFeedback,
} from 'react-native';
import IMAGES from '../styles/Images';
import Styles from '../styles/Styles';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import COLOR from '../styles/Color';
import NavigationBar from '../../components/commonView/NavigationBar';
import FONTS from '../styles/Fonts';
import NoData from '../commonView/NoData';

export default class NotificationScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: true,
    };
  }

  dismissPopup = () => {
    this.setState(
      {
        modalVisible: false,
      },
      (val) => {
        this.props.navigation.replace('NotificationSettings');
      },
    );
  };

  render() {
    return (
      <View style={Styles.container}>
        <Modal
          animationType={'slide'}
          transparent={true}
          style={{backgroundColor: COLOR.BLACK}}
          visible={this.state.modalVisible}
          onRequestClose={() => {
            console.log('Modal has been closed.');
          }}>
          <View style={styles.modal}>
            <View style={styles.modalBody}>
              <NavigationBar
                prop={this.props}
                navHeight={45}
                name={'Notifications'}
                right_image={IMAGES.tool}
                settingAction={()=> this.dismissPopup()}
                rightNav={'NotificationSettings'}
                isFunctionCall={true}
              />
              <View
                style={{
                  flex: 1,
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <Image
                  style={{height: 150, width: 150, tintColor: COLOR.GRAY}}
                  source={IMAGES.bell_icon}
                  resizeMode={'contain'}
                />
                <Text
                  style={[
                    Styles.small_label,
                    {textAlign: 'center', color: COLOR.LIGHT_TEXT, marginTop: 10},
                  ]}>
                  You have no notifications
                </Text>
              </View>
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  modal: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
  },
  modalBody: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    backgroundColor: COLOR.WHITE,
    borderWidth: 1,
    width: wp(100),
    marginTop: 50,
    borderColor: COLOR.BORDER,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    flex: 1,
  },
  image: {
    height: 120,
    width: 120,
    alignSelf: 'center',
    marginVertical: 20,
  },
  
 
});
