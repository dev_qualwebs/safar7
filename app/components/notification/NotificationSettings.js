import React from 'react';
import {View, Text, SafeAreaView, StyleSheet, Switch, Modal} from 'react-native';

import IMAGES from '../styles/Images';
import Styles from '../styles/Styles';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import COLOR from '../styles/Color';
import FONTS from '../styles/Fonts';
import NoData from '../commonView/NoData';
import NavigationBar from '../commonView/NavigationBar';

export default class NotificationSettings extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: true,
    };
  }

  render() {
    let {notificationData} = this.state;
    return (
      // <View style={Styles.container}>
      //   <SafeAreaView style={Styles.container}>
      <View style={Styles.container}>
      <Modal
        animationType={'slide'}
        transparent={true}
        style={{backgroundColor: COLOR.BLACK}}
        visible={this.state.modalVisible}
        onRequestClose={() => {
          console.log('Modal has been closed.');
        }}>
        <View style={classStyle.modal}>
          <View style={classStyle.modalBody}>
          <NavigationBar
            prop={this.props}
            navHeight={45}
            name={'Notification Settings'}
          />
          <View style={classStyle.mainView}>
            <Text style={classStyle.headingLabel}>Turn on all</Text>
            <Switch />
          </View>
          <View style={classStyle.singleLineView} />
          <View>
            <Text
              style={[
                classStyle.headingLabel,
                {width: wp(90), alignSelf: 'center'},
              ]}>
              Live update on your trip
            </Text>
            <Text
              style={
                ([Styles.small_label],
                {
                  marginTop: 5,
                  width: wp(90),
                  alignSelf: 'center',
                  color: COLOR.VOILET,
                })
              }>
              Stay up to date on live update directly to your phone about your
              journey.
            </Text>
          </View>
          <View style={[classStyle.mainView, {marginTop: 10}]}>
            <Text
              style={[
                Styles.body_label,
                {width: null, fontFamily: FONTS.FAMILY_REGULAR},
              ]}>
              Push Notifications
            </Text>
            <Switch />
          </View>

          <View style={classStyle.mainView}>
            <Text
              style={[
                Styles.body_label,
                {width: null, fontFamily: FONTS.FAMILY_REGULAR},
              ]}>
              Email Notifications
            </Text>
            <Switch />
          </View>
          <View style={classStyle.singleLineView} />
          <View>
            <Text
              style={[
                classStyle.headingLabel,
                {width: wp(90), alignSelf: 'center'},
              ]}>
              Special offers and travel deals
            </Text>
            <Text
              style={
                ([Styles.small_label],
                {
                  marginTop: 5,
                  width: wp(90),
                  alignSelf: 'center',
                  color: COLOR.VOILET,
                })
              }>
              Let me know about Special offers and promotional informations.
            </Text>
          </View>

          <View style={[classStyle.mainView, {marginTop: 10}]}>
            <Text
              style={[
                Styles.body_label,
                {width: null, fontFamily: FONTS.FAMILY_REGULAR},
              ]}>
              Push Notifications
            </Text>
            <Switch />
          </View>
          <View style={classStyle.mainView}>
            <Text
              style={[
                Styles.body_label,
                {width: null, fontFamily: FONTS.FAMILY_REGULAR},
              ]}>
              Email Notifications
            </Text>
            <Switch />
          </View>
          <View style={classStyle.singleLineView} />
        </View>
      </View>
      </Modal>
      </View>
    );
  }
}

const classStyle = StyleSheet.create({
  mainView: {
    height: 50,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: wp(90),
    alignSelf: 'center',
    // flex:1
  },
  headingLabel: {
    fontFamily: FONTS.FAMILY_BOLD,
    fontSize: FONTS.REGULAR,
    color: COLOR.BLACK,
  },
  singleLineView: {
    marginVertical: 15,
    borderBottomColor: COLOR.GRAY,
    width: wp(90),
    alignSelf: 'center',
    borderBottomWidth: 1,
  },
  modal: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
  },
  modalBody: {
    // justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    backgroundColor: COLOR.WHITE,
    borderWidth: 1,
    width: wp(100),
    marginTop: 50,
    borderColor: COLOR.BORDER,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    flex:1
  },
});
