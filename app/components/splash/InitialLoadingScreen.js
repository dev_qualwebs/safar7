import React from 'react';
import {View, ImageBackground, Text} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {AS_INITIAL_ROUTE, AS_USER_TOKEN} from '../../helper/Constants';

import {CommonActions} from '@react-navigation/native';
import COLOR from '../styles/Color';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';
import IMAGES from '../styles/Images';

import SplashScreen from 'react-native-splash-screen';

export default class InitialLoadingScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      initialRoute: 'SplashScreen',
    };
  }

  async componentDidMount() {
    const token = await AsyncStorage.getItem(AS_USER_TOKEN);

    if (token === null || token === '') {
      this.props.navigation.navigate('SplashScreen');
      SplashScreen.hide();
    } else {
      this.props.navigation.dispatch(
        CommonActions.reset({
          index: 0,
          routes: [{name: 'HomeStack'}],
        }),
      );
      SplashScreen.hide();
    }
  }

  // Render any loading content that you like here
  render() {
    return (
      <View
        style={{
          justifyContent: 'center',
          alignItems: 'center',
          alignSelf: 'center',
        }}>
        {/* <ImageBackground style={{ width: widthPercentageToDP('100%'), height: heightPercentageToDP('100%') }} source={IMAGES.gradient_icon} >
				</ImageBackground> */}
      </View>
    );
  }
}
