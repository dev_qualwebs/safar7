import React, {Component} from 'react';
import {
  StatusBar,
  Text,
  View,
  Image,
  SafeAreaView,
  TouchableOpacity,
  ImageBackground,
  StyleSheet,
} from 'react-native';
import {Pages} from 'react-native-pages';
import COLOR from '../styles/Color';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import FONTS from '../styles/Fonts';
import Styles from '../styles/Styles';
import IMAGES from '../styles/Images';

Platform.select({
  ios: () => StatusBar.setBarStyle('default'),
  android: () => StatusBar.setBackgroundColor('#263238'),
})();

export default class SplashScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      index: 0,
    };

    this.updateRef = this.updateRef.bind(this);
  }

  updateRef(ref) {
    this._pages = ref;
  }



  changePage = () => {
    // console.log('reference is:-', index);
    const {index} = this.state;
    if (this._pages) {
      this.setState(
        {
          index: index + 1,
        },
        () => {
          if (index == 2 || index > 2) {
            this.props.navigation.navigate('LoginScreen');
          } else {
            this._pages.scrollToPage(this.state.index);
          }
        },
      );
    }
  };

  render() {
    return (
      <View style={Styles.container}>
        <ImageBackground style={Styles.backgroundImage}>
          <SafeAreaView style={{flex: 1}}>
            <View style={{flex: 3, alignContent: 'center'}}>
              <Pages
                onScrollEnd={(index) => this.setState({index})}
                ref={this.updateRef}
                indicatorColor="#FFFFFF00"
                indicatorOpacity={0}>
                <Splash1 />
                <Splash2 />
                <Splash3 />
              </Pages>
            </View>
            <View style={{flex: 1, alignItems: 'center'}}>
              <TouchableOpacity
                style={{flex: 1, bottom: 20, position: 'absolute'}}
                onPress={this.changePage}>
                <View style={styles.bottomCircle}>
                  <Image
                    style={
                      ([styles.circleImage],
                      {tintColor: COLOR.WHITE, height: 20, width: 20})
                    }
                    source={IMAGES.right_arrow}
                  />
                </View>
              </TouchableOpacity>
            </View>
          </SafeAreaView>
        </ImageBackground>
      </View>
    );
  }
}

const Splash1 = () => {
  return (
    <View
      style={{
        flex: 1,
        alignItems: 'center',
      }}>
      <View style={{marginVertical: 20, position: 'absolute'}}>
        <Image style={styles.image} source={IMAGES.splash1} />
      </View>
      <View style={{bottom: 10, position: 'absolute', alignItems: 'center'}}>
        <Text style={[Styles.splash_heading_label, {textAlign: 'center'}]}>
          Request itinerary
        </Text>
        <Text
          style={[
            Styles.body_label,
            {
              color: COLOR.VOILET,
              marginTop: 10,
              height: 50,
              textAlign: 'center',
              width: null,
            },
          ]}>
          Request your trip package at{'\n'}palm of your hand
        </Text>
        <View
          style={{
            height: 5,
            width: wp(50),
            marginBottom: 10,
            marginTop: 50,
            flexDirection: 'row',
          }}>
          <View style={{flex: 1, backgroundColor: COLOR.GREEN}} />
          <View
            style={{
              flex: 1,
              backgroundColor: COLOR.GRAY,
              marginHorizontal: 20,
            }}
          />
          <View style={{flex: 1, backgroundColor: COLOR.GRAY}} />
        </View>
      </View>
    </View>
  );
};

const Splash2 = () => {
  return (
    <View
      style={{
        flex: 1,
        alignItems: 'center',
      }}>
      <View style={{marginVertical: 20, position: 'absolute'}}>
        <Image style={styles.image} source={IMAGES.splash2} />
      </View>
      <View style={{bottom: 10, position: 'absolute', alignItems: 'center'}}>
        <Text style={[Styles.splash_heading_label, {textAlign: 'center'}]}>
          Direct your Adventure
        </Text>
        <Text
          style={[
            Styles.body_label,
            {
              color: COLOR.VOILET,
              marginTop: 10,
              height: 50,
              textAlign: 'center',
            },
          ]}>
          Experience the activity you need
        </Text>
        <View
          style={{
            height: 5,
            width: wp(50),
            marginBottom: 10,
            marginTop: 50,
            flexDirection: 'row',
          }}>
          <View style={{flex: 1, backgroundColor: COLOR.GRAY}} />
          <View
            style={{
              flex: 1,
              backgroundColor: COLOR.GREEN,
              marginHorizontal: 20,
            }}
          />
          <View style={{flex: 1, backgroundColor: COLOR.GRAY}} />
        </View>
      </View>
    </View>
  );
};

const Splash3 = () => {
  return (
    <View
      style={{
        flex: 1,
        alignItems: 'center',
      }}>
      <View style={{marginVertical: 20, position: 'absolute'}}>
        <Image style={styles.image} source={IMAGES.splash3} />
      </View>
      <View style={{bottom: 10, position: 'absolute', alignItems: 'center'}}>
        <Text style={[Styles.splash_heading_label, {textAlign: 'center'}]}>
          Manage your Trip
        </Text>
        <Text
          style={[
            Styles.body_label,
            {
              color: COLOR.VOILET,
              marginTop: 10,
              height: 50,
              textAlign: 'center',
            },
          ]}>
          Get your timeline trip{'\n'}with best price and quality
        </Text>
        <View
          style={{
            height: 5,
            width: wp(50),
            marginBottom: 10,
            marginTop: 50,
            flexDirection: 'row',
          }}>
          <View style={{flex: 1, backgroundColor: COLOR.GRAY}} />
          <View
            style={{
              flex: 1,
              backgroundColor: COLOR.GRAY,
              marginHorizontal: 20,
            }}
          />
          <View style={{flex: 1, backgroundColor: COLOR.GREEN}} />
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  bottomCircle: {
    height: wp('15%'),
    width: wp('15%'),
    borderRadius: wp('15%') / 2,
    borderWidth: 0.3,
    backgroundColor: COLOR.GREEN,
    shadowOpacity: 1,
    shadowRadius: 2,
    elevation: 3,
    shadowOffset: {width: 1, height: 5},
    shadowColor: '#465A6440',
    alignItems: 'center',
    justifyContent: 'center',
  },
  circleImage: {
    alignSelf: 'center',
    width: 25,
    height: 25,
    resizeMode: 'contain',
  },
  image: {width: wp(100), height: hp(40), resizeMode: 'contain'},
});
