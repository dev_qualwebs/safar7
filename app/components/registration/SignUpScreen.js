'use strict';

import React from 'react';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview';
import {
  Text,
  View,
  SafeAreaView,
  TouchableOpacity,
  StyleSheet,
  Image,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import COLOR from '../styles/Color';
import {FloatingTitleTextInputField} from '../../helper/FloatingTitleTextInputField';
import Styles from '../styles/Styles';
import IMAGES from '../styles/Images';
import Toast from 'react-native-simple-toast';
import CountryPicker from 'react-native-country-picker-modal';
import Auth from '../../auth';
import API from '../../api/Api';
import Indicator from '../commonView/ActivityIndicator';
import {validateEmail} from '../commonView/Helpers';
import {
  AS_FCM_TOKEN,
  AS_INITIAL_ROUTE,
  AS_USER_TOKEN,
} from '../../helper/Constants';
import Action from '../../redux/action/ProfileAction';

let auth = new Auth();
let api = new API();

class SignUpScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedCountry: null,
      password: '',
      confirmPassword: '',
      phoneNumber: '',
      buttonImage: IMAGES.cell_phone,
      placeholderName: 'Mobile number',
      buttonText: 'Continue with Email address',
      isLoading: false,

      secure: true,
    };
  }

  componentDidMount() {}

  _updateMasterState = (attrName, value) => {
    this.setState({[attrName]: value});
  };

  togglePlaceholder = () => {
    if (this.state.placeholderName == 'Mobile number') {
      this.setState({
        placeholderName: 'Email address',
        buttonText: 'Continue with Mobile number',
        buttonImage: IMAGES.cell_phone,
        phoneNumber: '',
      });
    } else {
      this.setState({
        placeholderName: 'Mobile number',
        buttonText: 'Continue with Email address',
        buttonImage: IMAGES.email_black,
        phoneNumber: '',
      });
    }
  };

  handleContinueButton() {
    if (this.state.phoneNumber.length == 0) {
      if (this.state.placeholderName == 'Mobile number') {
        Toast.show('Enter your Mobile Number');
      } else {
        Toast.show('Enter your Email Address');
      }
    } else if (
      this.state.placeholderName != 'Mobile number' &&
      !validateEmail(this.state.phoneNumber)
    ) {
      Toast.show('Enter valid Email Address');
    } else if (
      this.state.placeholderName == 'Mobile number' &&
      this.state.phoneNumber.length > 12
    ) {
      Toast.show('Enter Valid Mobile Number');
    } else if (this.state.password.length == 0) {
      Toast.show('Enter your password');
    } else {
      this.callSignUpApi();
    }
  }

  callSignUpApi = async () => {
    let that = this;
    const {
      phoneNumber,
      password,
      selectedCountry,
      confirmPassword,
    } = that.state;
    const token = await auth.getValue(AS_FCM_TOKEN);
    let data = null;
    if (this.state.placeholderName == 'Mobile number') {
      data = JSON.stringify({
        dial_code: selectedCountry ? selectedCountry.callingCode[0] : '91',
        phone: phoneNumber,
        password: password,
        confirm_password: confirmPassword,
      });
    } else {
      data = JSON.stringify({
        email: phoneNumber,
        password: password,
        confirm_password: confirmPassword,
      });
    }

    that.setState({isLoading: true});
    api
      .signUpApi(data)
      .then((json) => {
        console.log('login response:-', json);
        that.setState({isLoading: false});

        let data = JSON.stringify(json.data);
        if (json.status == 200) {
          console.log('signup data:-', json.data.response);
          this.setState({modalVisible: false}, () => {
            setTimeout(() => Toast.show(json.data.message), 500);
          });

          if (this.state.placeholderName == 'Mobile number') {
            this.props.navigation.navigate('MobileVerification', {
              phoneNumber: this.state.phoneNumber,
              selectedCountry: this.state.selectedCountry
                ? this.state.selectedCountry.callingCode[0]
                : '91',
            });
          } else {
            this.props.navigation.navigate('MobileVerification', {
              email: this.state.phoneNumber,
            });
          }
        } else if (json.status == 400) {
          setTimeout(() => {
            Toast.show(json.data.message);
          }, 0);

          // this.props.navigation.navigate('MobileVerification',{data:{phoneNumber:phoneNumber,}});
        } else {
          setTimeout(() => {
            Toast.show(json.data.message);
          }, 0);
        }
      })
      .catch((error) => {
        console.log('login error:-', error.response.data.message);
        that.setState({isLoading: false});
        setTimeout(() => {
          console.log('json.data.message', error);
          Toast.show(String(error.response.data.message));
        }, 500);
      });
  };

  render() {
    const {selectedCountry, isLoading, phoneNumber} = this.state;
    return (
      <SafeAreaView style={Styles.container}>
        <Indicator loading={isLoading} />
        <KeyboardAwareScrollView
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}>
          <View style={styles.logo_view}>
            <Image
              style={{height: 60, width: 60}}
              source={IMAGES.app_icon}
              resizeMode={'contain'}
            />
          </View>
          <Text style={[Styles.heading_label]}>SignUp to Safar 7</Text>
          <View style={[styles.borderView, {marginTop: 30}]}>
            <View style={{marginVertical: 0, marginTop: 10}}>
              <View
                style={{
                  flex: 1,
                  flexDirection: 'row',
                  alignItems: 'center',
                }}>
                {this.state.placeholderName == 'Mobile number' && (
                  <View style={phoneNumber != '' ? {marginTop: 10} : ''}>
                    <CountryPicker
                    withAlphaFilter={true}
                      containerButtonStyle={{marginLeft: 10}}
                      withCallingCode={true}
                      withEmoji={true}
                      withCallingCodeButton={true}
                      countryCode={
                        selectedCountry ? selectedCountry.cca2 : 'IN'
                      }
                      visible={false}
                      onSelect={(country) => {
                        this.setState({selectedCountry: country});
                      }}
                    />
                  </View>
                )}
                <FloatingTitleTextInputField
                  attrName="phoneNumber"
                  keyboardType={
                    this.state.placeholderName == 'Mobile number'
                      ? 'phone-pad'
                      : 'email-address'
                  }
                  title={this.state.placeholderName}
                  value={this.state.phoneNumber}
                  updateMasterState={this._updateMasterState}
                  textInputStyles={{
                    // here you can add additional TextInput styles
                    color: COLOR.BLACK,
                    fontSize: 16,
                  }}
                />
              </View>
              <View
                style={{
                  borderBottomColor: COLOR.GRAY,
                  marginVertical: 0,
                  borderBottomWidth: 1,
                  marginBottom: 10,
                }}
              />
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <View style={{width: '90%'}}>
                  <FloatingTitleTextInputField
                    attrName="password"
                    title="Password"
                    secureTextEntry={this.state.secure}
                    value={this.state.password}
                    textInputStyles={{
                      // here you can add additional TextInput styles
                      color: COLOR.BLACK,
                      fontSize: 16,
                    }}
                    updateMasterState={this._updateMasterState}
                  />
                </View>
                <TouchableOpacity
                  onPress={() => this.setState({secure: !this.state.secure})}>
                  <Image
                    style={{
                      width: 25,
                      height: 25,
                      resizeMode: 'contain',
                    }}
                    source={
                      this.state.secure
                        ? require('../styles/assets/eye_splash.png')
                        : require('../styles/assets/eye_.png')
                    }
                  />
                </TouchableOpacity>
              </View>
            </View>
            <View
              style={{
                borderBottomColor: COLOR.GRAY,
                marginVertical: 0,
                borderBottomWidth: 1,
                marginBottom: 10,
              }}
            />
            <FloatingTitleTextInputField
              attrName="confirmPassword"
              title="Confirm Password"
              secureTextEntry={this.state.secure}
              value={this.state.confirmPassword}
              textInputStyles={{
                // here you can add additional TextInput styles
                color: COLOR.BLACK,
                fontSize: 16,
              }}
              updateMasterState={this._updateMasterState}
            />
          </View>

          <View style={{marginVertical: 40}}>
            <TouchableOpacity
              onPress={() => this.handleContinueButton()}
              style={Styles.button_content}>
              <Text style={Styles.button_font}>Continue</Text>
            </TouchableOpacity>
          </View>
          <TouchableOpacity
            onPress={this.togglePlaceholder}
            style={[
              Styles.button_content,
              {
                backgroundColor: COLOR.WHITE,
                borderRadius: 10,
                borderWidth: 1,
                borderColor: COLOR.LIGHT_TEXT,
              },
            ]}>
            <View style={{alignItems: 'center', flexDirection: 'row'}}>
              <Image
                source={this.state.buttonImage}
                style={{
                  width: 25,
                  height: 25,
                  marginRight: 5,
                }}
                resizeMode={'contain'}
              />
              <Text style={[Styles.button_font, {color: COLOR.BLACK}]}>
                {this.state.buttonText}
              </Text>
            </View>
          </TouchableOpacity>
        </KeyboardAwareScrollView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  logo_view: {
    backgroundColor: '#d8d8d8',
    marginTop: 20,
    height: 60,
    width: 60,
    borderRadius: 10,
    alignSelf: 'center',
  },
  borderView: {
    borderRadius: 10,
    borderWidth: 1,
    width: wp(90),
    borderColor: COLOR.GRAY,
    alignSelf: 'center',
  },
});

export default SignUpScreen;
