'use strict';

import React from 'react';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview';
import {
  Text,
  View,
  SafeAreaView,
  TouchableOpacity,
  Image,
  KeyboardAvoidingView,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import COLOR from '../styles/Color';
import Styles from '../styles/Styles';
import IMAGES from '../styles/Images';
import API from '../../api/Api';

const api = new API();

export default class ForgotPasswordMiddleScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      phoneNumber: '',
      email: '',
      selectedCountry: null,
      type: 1,
      isForgot: true,
    };
  }

  componentDidMount(): void {
    if (this.props.route.params && this.props.route.params.state) {
      this.setState({
        type: this.props.route.params.state.type ?? 1,
        phoneNumber: this.props.route.params.state.phoneNumber,
        selectedCountry: this.props.route.params.state.selectedCountry,
        email: this.props.route.params.state.email,
        isForgot:true
      });
    }
  }

  _updateMasterState = (attrName, value) => {
    this.setState({[attrName]: value});
  };

  render() {
    const {type} = this.state;
    return (
      <SafeAreaView style={Styles.container}>
        <View style={{alignSelf: 'center', width: wp(90)}}>
          <TouchableOpacity onPress={() => this.props.navigation.goBack(null)}>
            <View style={Styles.navbarView}>
              <Image
                source={IMAGES.back_arrow}
                style={{width: 15, height: 15}}></Image>
            </View>
          </TouchableOpacity>
        </View>
        <Text style={[Styles.heading_label, {marginTop: 50, marginBottom: 10}]}>
          Forgot Password
        </Text>
        <Text
          style={[
            Styles.small_label,
            {textAlign: 'center', width: wp(80), color: COLOR.VOILET},
          ]}>
          {`A code sent to ${type == 1 ? this.state.phoneNumber : this.state.email}`}
        </Text>

        
          <Text
            style={[
              Styles.small_label,
              {
                textAlign: 'center',
                width: wp(80),
                color: COLOR.VOILET,
                marginTop: 20,
              },
            ]}>
            Please check the message and enter the {`\n`} OTP in the next page.
          </Text>
        

        <TouchableOpacity
          onPress={() => {
            this.props.navigation.navigate('NewPassword', {state: this.state});
          }}
          style={[Styles.button_content, {width: wp(90), marginTop: 150}]}>
          <Text style={Styles.button_font}>Next</Text>
        </TouchableOpacity>
      </SafeAreaView>
    );
  }
}
