'use strict';

import React, {useEffect, useState} from 'react';
import {
  Text,
  View,
  SafeAreaView,
  TouchableOpacity,
  StyleSheet,
  Image,
  Alert,
  ActivityIndicator,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import COLOR from '../styles/Color';
import Styles from '../styles/Styles';
import {
  CodeField,
  Cursor,
  useBlurOnFulfill,
  useClearByFocusCell,
} from 'react-native-confirmation-code-field';
import IMAGES from '../styles/Images';
import CountDown from 'react-native-countdown-component';
import {useRoute} from '@react-navigation/core';
import API from '../../api/Api';
import {AS_INITIAL_ROUTE, AS_USER_TOKEN} from '../../helper/Constants';
import Auth from '../../auth';
import {useNavigation} from '@react-navigation/core';
import Action from '../../redux/action/ProfileAction';
import {connect} from 'react-redux';
import SimpleToast from 'react-native-simple-toast';

const api = new API();
let auth = new Auth();

const MobileVerification = (prop) => {
  const [value, setValue] = useState('');
  const route = useRoute();
  const [showResend, setShowResend] = useState(false);
  const ref = useBlurOnFulfill({value, cellCount: CELL_COUNT});
  const navigation = useNavigation();
  const [props, getCellOnLayoutHandler] = useClearByFocusCell({
    value,
    setValue,
  });

  async function resendOtp() {
    let data = null;
    var email, dial_code, phoneNumber;
    if (route.params.email) {
      email = route.params.email;
      data = {
        email: email,
      };
      // JSON.stringify({
      //   email: email,
      // });
    } else {
      dial_code = route.params.selectedCountry;
      phoneNumber = route.params.phoneNumber;
      const otp = value;
      
      data ={
        dial_code: dial_code,
        phone: phoneNumber,
      };
    }

    api
      .resendOtp(data)
      .then((json) => {
        console.log('resend response:-', json);

        // let data = JSON.stringify(json.data);
        if (json.status == 200) {
          SimpleToast.show(json.data.message);
        } else if (json.status == 400) {
          setTimeout(() => {
            console.log(json.data.message);
          }, 0);

          // this.props.navigation.navigate('MobileVerification',{data:{phoneNumber:phoneNumber,}});
        } else {
          setTimeout(() => {
            Toast.show(json.data.message);
          }, 0);
        }
      })
      .catch((error) => {
        setTimeout(() => {
          console.log('json.data.message', error);
        }, 0);
        console.log('login error:-', error.response.data.message);
      });
  }

  async function otpVerification() {
    let data = null;
    let that = this;
    var email, dial_code, phoneNumber;
    if (route.params.email) {
      email = route.params.email;
      // data = JSON.stringify({
      //   email: email,
      //   otp: value,
      // });
      data = {
        email: email,
        otp: value,
      };
    } else {
      dial_code = route.params.selectedCountry;
      phoneNumber = route.params.phoneNumber;
      const otp = value;
      // data = JSON.stringify({
      //   dial_code: dial_code,
      //   phone: phoneNumber,
      //   otp: otp,
      // });
      data = {
        dial_code: dial_code,
        phone: phoneNumber,
        otp: otp,
      };
    }

    api
      .otpVerification(data)
      .then((json) => {
        console.log('otp verification response:-', json);
        SimpleToast.show('Logged in Successfully');
        auth.setValue(AS_USER_TOKEN, json.data.response.token);
        auth.setValue(AS_INITIAL_ROUTE, 'HomeStack');
        prop.saveUserDetail(json.data.response);
        navigation.replace('ProfileDetail', {
          updateProfile: true,
          val: 0,
          data: null,
        });
      })
      .catch((error) => {
        setTimeout(() => {
          console.log('json.data.message', error);
        }, 0);
        SimpleToast.show(error.response.data.message);
        console.log('login error:-', error.response.data.message);
      });
  }

  return (
    <SafeAreaView style={Styles.container}>
      <View style={{alignSelf: 'center', width: wp(90)}}>
        <TouchableOpacity onPress={() => prop.navigation.goBack(null)}>
          <View style={Styles.navbarView}>
            <Image
              source={IMAGES.back_arrow}
              style={{width: 15, height: 15}}></Image>
          </View>
        </TouchableOpacity>
      </View>
      <Text style={[Styles.heading_label, {marginBottom: 10}]}>
        {route.params && route.params.email ? 'Verify your Email' : 'Verify your phone'}
      </Text>
      <Text style={[Styles.small_label, {width: wp(80), textAlign: 'center'}]}>
        {route.params &&  route.params.email
          ? " Enter the verify code we have sent to your email. Didn't receive the code?"
          : " Enter the verify code we have sent to your phone. Didn't receive the code?"}
      </Text>
      {showResend ? (
        <TouchableOpacity
          style={{alignSelf: 'center'}}
          onPress={() => resendOtp()}>
          <Text
            style={{
              alignSelf: 'center',
              color: COLOR.BLUE,
              marginVertical: 10,
            }}>
            Resend OTP
          </Text>
        </TouchableOpacity>
      ) : (
        <View style={{flexDirection:'row',alignItems:'center',alignSelf:'center'}}>
        <ActivityIndicator
            color={COLOR.DARK_BLUE}
            size={'small'}
            animating={true}
          />
        <CountDown
          until={60 * 1 + 59}
          size={16}
          onFinish={() => {
            setShowResend(true);
          }}
          timeToShow={['M', 'S']}
          digitStyle={{backgroundColor: 'transparent'}}
          timeLabels={{m: null, s: null}}
          showSeparator
        />
        </View>
      )}
      <CodeField
        ref={ref}
        {...props}
        value={value}
        onChangeText={setValue}
        cellCount={CELL_COUNT}
        rootStyle={styles.codeFieldRoot}
        keyboardType="number-pad"
        textContentType="oneTimeCode"
        renderCell={({index, symbol, isFocused}) => (
          <View
            // Make sure that you pass onLayout={getCellOnLayoutHandler(index)} prop to root component of "Cell"
            onLayout={getCellOnLayoutHandler(index)}
            key={index}
            style={[styles.cellRoot, isFocused && styles.focusCell]}>
            <Text style={[styles.cellText]}>
              {symbol || (isFocused ? <Cursor /> : null)}
            </Text>
          </View>
        )}
      />
      {/* <View
        style={{
          flexDirection: 'row',
          marginTop: 10,
          alignItems: 'center',
          alignSelf: 'center',
        }}>
        <Text
          style={[
            Styles.small_label,
            {
              textAlign: 'center',
              color: COLOR.RED,
              width: null,
            },
          ]}>
          Incorrect Code
        </Text>
        <Image
          source={IMAGES.attention}
          style={{height: 12, width: 12, marginLeft: 5, alignSelf: 'center'}}
          resizeMode={'contain'}
        />
      </View> */}
      <TouchableOpacity
        onPress={() => otpVerification()}
        style={[
          Styles.button_content,
          {
            bottom: 20,
            marginTop: 20,
            width: wp(90),
            borderRadius: 10,
            position: 'absolute',
          },
        ]}>
        <Text style={Styles.button_font}>Continue</Text>
      </TouchableOpacity>
    </SafeAreaView>
  );
};
const CELL_COUNT = 4;

const styles = StyleSheet.create({
  codeFieldRoot: {
    marginTop: 50,
    width: wp(70),
    marginLeft: 'auto',
    marginRight: 'auto',
  },
  cellRoot: {
    width: 60,
    height: 60,
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomColor: '#ccc',
    borderBottomWidth: 4,
  },
  cellText: {
    color: COLOR.BLACK,
    fontSize: 36,
    textAlign: 'center',
  },
  focusCell: {
    borderBottomColor: COLOR.GREEN,
    borderBottomWidth: 4,
  },
});

const mapDispatchToProps = (dispatch) => {
  return {
    saveUserDetail: (data) => {
      dispatch(Action.saveUserDetail(data));
    },
  };
};

export default connect(null, mapDispatchToProps)(MobileVerification);
