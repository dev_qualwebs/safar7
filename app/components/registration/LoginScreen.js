'use strict';

import React from 'react';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview';
import {
  Text,
  View,
  SafeAreaView,
  TouchableOpacity,
  StyleSheet,
  Image,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import COLOR from '../styles/Color';
import {FloatingTitleTextInputField} from '../../helper/FloatingTitleTextInputField';
import Styles from '../styles/Styles';
import IMAGES from '../styles/Images';
import Toast from 'react-native-simple-toast';
import CountryPicker from 'react-native-country-picker-modal';
import {
  AS_FCM_TOKEN,
  AS_INITIAL_ROUTE,
  AS_USER_TOKEN,
} from '../../helper/Constants';
import {connect} from 'react-redux';
import Action from '../../redux/action/ProfileAction';
import {validateEmail} from '../commonView/Helpers';
import Auth from '../../auth';
import API from '../../api/Api';
import Indicator from '../commonView/ActivityIndicator';

let auth = new Auth();
let api = new API();

class LoginScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedCountry: null,
      password: '',
      phoneNumber: '',
      buttonImage: IMAGES.cell_phone,
      placeholderName: 'Mobile number',
      buttonText: 'Continue with Email address',
      isLoading: false,
      secure: true,
    };
    this.togglePlaceholder = this.togglePlaceholder.bind(this);
    this.handleContinueButton = this.handleContinueButton.bind(this);
  }

  componentDidMount() {}

  _updateMasterState = (attrName, value) => {
    this.setState({[attrName]: value});
  };

  togglePlaceholder = () => {
    if (this.state.placeholderName == 'Mobile number') {
      this.setState({
        placeholderName: 'Email address',
        buttonText: 'Continue with Mobile number',
        buttonImage: IMAGES.cell_phone,
        phoneNumber: '',
      });
    } else {
      this.setState({
        placeholderName: 'Mobile number',
        buttonText: 'Continue with Email address',
        buttonImage: IMAGES.email_black,
        phoneNumber: '',
      });
    }
  };

  handleContinueButton() {
    if (this.state.phoneNumber.length == 0) {
      if (this.state.placeholderName == 'Mobile number') {
        Toast.show('Enter your Mobile Number');
      } else {
        Toast.show('Enter your Email Address');
      }
    } else if (
      this.state.placeholderName != 'Mobile number' &&
      !validateEmail(this.state.phoneNumber)
    ) {
      Toast.show('Enter valid Email Address');
    } else if (
      this.state.placeholderName == 'Mobile number' &&
      this.state.phoneNumber.length > 12
    ) {
      Toast.show('Enter Valid Mobile Number');
    } else if (this.state.password.length == 0) {
      Toast.show('Enter your password');
    } else {
      this.callLoginApi();
    }
  }

  callLoginApi = async () => {
    let that = this;
    const {phoneNumber, password, selectedCountry} = that.state;
    const token = await auth.getValue(AS_FCM_TOKEN);
    let data = null;
    if (this.state.placeholderName == 'Mobile number') {
      // data = JSON.stringify({
      //   dial_code: selectedCountry ? selectedCountry.callingCode[0] : '91',
      //   phone: phoneNumber,
      //   password: password,
      // });
      data = {
        dial_code: selectedCountry ? selectedCountry.callingCode[0] : '91',
        phone: phoneNumber,
        password: password,
      };
    } else {
      // data = JSON.stringify({
      // email: phoneNumber,
      // password: password,
      // });
      data = {
        email: phoneNumber,
        password: password,
      };
    }

    // that.setState({isLoading: true});
    api
      .loginAPI(data)
      .then((json) => {
        console.log('login response:-', json);
        // that.setState({isLoading: false});

        let data = JSON.stringify(json.data);
        if (json.status == 200) {
          Toast.show('Logged in Successfully');
          auth.setValue(AS_USER_TOKEN, json.data.response.token);
          auth.setValue(AS_INITIAL_ROUTE, 'HomeStack');
          this.setState({modalVisible: false});
          that.props.saveUserDetail(data.response);
          if (json.data.response.first_name) {
            this.props.navigation.navigate('HomeStack');
          } else {
            this.props.navigation.navigate('ProfileDetail', {
              updateProfile: true,
              val: 0,
              data: null,
            });
          }
        }
      })
      .catch((error) => {
        setTimeout(() => {
          console.log('json.data.message', error.response.data);
          if (error.response.data.status != 401) {
            Toast.show(String(error.response.data.message));
          }
        }, 0);
        console.log('login error:-', error.response.data.message);
        // that.setState({isLoading: false});
        if (error.response.data.status == 400) {
          if (that.state.placeholderName == 'Mobile number') {
            that.props.navigation.navigate('MobileVerification', {
              phoneNumber: this.state.phoneNumber,
              selectedCountry: this.state.selectedCountry
                ? this.state.selectedCountry.callingCode[0]
                : '91',
            });
          } else {
            that.props.navigation.navigate('MobileVerification', {
              email: this.state.phoneNumber,
            });
          }
        } else if (error.response.data.status == 401) {
          Toast.show('Credentials not matched');
        } else {
          Toast.show('Something went wrong! Please try again later.');
        }
      });
  };

  render() {
    const {selectedCountry, isLoading, phoneNumber} = this.state;
    return (
      <SafeAreaView style={Styles.container}>
        <Indicator loading={isLoading} />
        <KeyboardAwareScrollView
          bounces={false}
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}>
          <View>
            <TouchableOpacity
              style={{height: 25, width: 25, marginLeft: 15}}
              onPress={() => {
                this.props.navigation.goBack();
              }}>
              <Image source={IMAGES.close} style={{height: 25, width: 25}} />
            </TouchableOpacity>
          </View>
          <View style={styles.logo_view}>
            <Image
              style={{height: 60, width: 60}}
              source={IMAGES.app_icon}
              resizeMode={'contain'}
            />
          </View>
          <Text style={[Styles.heading_label]}>Welcome to Safar 7</Text>
          <View style={[styles.borderView, {marginTop: 30}]}>
            <View style={{marginVertical: 0, marginTop: 10}}>
              <View
                style={{flex: 1, flexDirection: 'row', alignItems: 'center'}}>
                {this.state.placeholderName == 'Mobile number' && (
                  <View
                    style={{
                      backgroundColor: '#f2f2f2',
                      width: 100,
                      height: 60,
                      alignItems: 'center',
                      justifyContent: 'center',
                      marginTop: -15,
                      marginBottom: -5,
                    }}>
                    <View style={phoneNumber != '' ? {marginTop: 0} : ''}>
                      <CountryPicker
                        withAlphaFilter={true}
                        containerButtonStyle={{marginLeft: 0}}
                        withCallingCode={true}
                        withEmoji={true}
                        keyboardType={'done'}
                        withCallingCodeButton={true}
                        countryCode={
                          selectedCountry ? selectedCountry.cca2 : 'IN'
                        }
                        visible={false}
                        onSelect={(country) => {
                          this.setState({selectedCountry: country});
                        }}
                      />
                    </View>
                  </View>
                )}
                <View style={{flex: 1}}>
                  <FloatingTitleTextInputField
                    attrName="phoneNumber"
                    keyboardType={
                      this.state.placeholderName == 'Mobile number'
                        ? 'phone-pad'
                        : 'email-address'
                    }
                    title={this.state.placeholderName}
                    value={this.state.phoneNumber}
                    updateMasterState={this._updateMasterState}
                    textInputStyles={{
                      // here you can add additional TextInput styles
                      color: COLOR.BLACK,
                      fontSize: 16,
                    }}
                  />
                </View>
              </View>
              <View
                style={{
                  borderBottomColor: COLOR.GRAY,
                  marginVertical: 0,
                  borderBottomWidth: 1,
                  marginBottom: 10,
                }}
              />
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <View style={{width: '90%'}}>
                  <FloatingTitleTextInputField
                    attrName="password"
                    title="Password"
                    secureTextEntry={this.state.secure}
                    value={this.state.password}
                    textInputStyles={{
                      // here you can add additional TextInput styles
                      color: COLOR.BLACK,
                      fontSize: 16,
                    }}
                    updateMasterState={this._updateMasterState}
                  />
                </View>
                <TouchableOpacity
                  onPress={() => this.setState({secure: !this.state.secure})}>
                  <Image
                    style={{
                      width: 25,
                      height: 25,
                      resizeMode: 'contain',
                    }}
                    source={
                      this.state.secure
                        ? require('../styles/assets/eye_splash.png')
                        : require('../styles/assets/eye_.png')
                    }
                  />
                </TouchableOpacity>
              </View>
            </View>
          </View>
          <TouchableOpacity
            onPress={() =>
              this.props.navigation.navigate('ForgotPassword', {
                type: this.state.placeholderName == 'Mobile number' ? 1 : 2,
              })
            }>
            <Text
              style={[
                Styles.body_label,
                {
                  marginTop: 20,
                  textAlign: 'right',
                  color: COLOR.BLACK,
                },
              ]}>
              Forgot Password?
            </Text>
          </TouchableOpacity>
          <View style={{marginTop: 40}}>
            <TouchableOpacity
              onPress={this.handleContinueButton}
              // onPress={()=>{
              //   this.props.navigation.navigate('MapViewWithDates')
              // }}
              style={Styles.button_content}>
              <Text style={Styles.button_font}>Continue</Text>
            </TouchableOpacity>
            <View
              style={{
                height: 100,
                width: wp(90),
                alignSelf: 'center',
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
              }}>
              <View
                style={{
                  flex: 1,
                  borderBottomColor: COLOR.GRAY,
                  marginVertical: 5,
                  borderBottomWidth: 1,
                }}
              />
              <Text
                style={[Styles.body_label, {width: 30, textAlign: 'center'}]}>
                or
              </Text>
              <View
                style={{
                  flex: 1,
                  borderBottomColor: COLOR.GRAY,
                  marginVertical: 5,
                  borderBottomWidth: 1,
                }}
              />
            </View>
            <TouchableOpacity
              onPress={this.togglePlaceholder}
              style={[
                Styles.button_content,
                {
                  backgroundColor: COLOR.WHITE,
                  borderRadius: 10,
                  borderWidth: 1,
                  borderColor: COLOR.LIGHT_TEXT,
                },
              ]}>
              <View style={{alignItems: 'center', flexDirection: 'row'}}>
                <Image
                  source={this.state.buttonImage}
                  style={{
                    width: 25,
                    height: 25,
                    marginRight: 5,
                  }}
                  resizeMode={'contain'}
                />
                <Text style={[Styles.button_font, {color: COLOR.BLACK}]}>
                  {this.state.buttonText}
                </Text>
              </View>
            </TouchableOpacity>
          </View>
        </KeyboardAwareScrollView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  logo_view: {
    backgroundColor: '#d8d8d8',
    marginTop: 20,
    height: 60,
    width: 60,
    borderRadius: 10,
    alignSelf: 'center',
  },
  borderView: {
    borderRadius: 10,
    borderWidth: 1,
    width: wp(90),
    borderColor: COLOR.GRAY,
    alignSelf: 'center',
  },
});

const mapDispatchToProps = (dispatch) => {
  return {
    saveUserDetail: (data) => {
      dispatch(Action.saveUserDetail(data));
    },
  };
};

export default connect(null, mapDispatchToProps)(LoginScreen);
