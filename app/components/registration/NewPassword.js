'use strict';

import React from 'react';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview';
import {
  Text,
  View,
  SafeAreaView,
  TouchableOpacity,
  Image,
  Alert,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import COLOR from '../styles/Color';
import {Hoshi} from 'react-native-textinput-effects';
import Styles from '../styles/Styles';
import IMAGES from '../styles/Images';
import API from '../../api/Api';
import Toast from 'react-native-simple-toast';
import SimpleToast from 'react-native-simple-toast';

const api = new API();

export default class NewPassword extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      otp: '',
      newPassword: '',
      confirmPassword: '',
      phone: '',
      email: '',
      type: 1,
      selectedCountry: null,
      isForgot: false,
    };

    this.callForgetPassword = this.callForgetPassword.bind(this);
    this.callResetPassword = this.callResetPassword.bind(this);
  }

  componentDidMount(): void {
    if (this.props.route.params && this.props.route.params.state) {
      this.setState({
        isForgot: true,
        type: this.props.route.params.state.type ?? 1,
        phone: this.props.route.params.state.phoneNumber,
        selectedCountry: this.props.route.params.state.selectedCountry,
        email: this.props.route.params.state.email,
      });
    }
  }

  callResetPassword =  () => {
  
    if (this.state.otp == '') {
      SimpleToast.show('Enter current password');
    } else if (this.state.newPassword == '') {
      SimpleToast.show('Enter new password');
    } else if (this.state.newPassword.length < 8) {
      SimpleToast.show('Password must be of min 8 characters.');
    } else if (this.state.confirmPassword == '') {
      SimpleToast.show('Enter confirm password');
    } else if (this.state.newPassword != this.state.confirmPassword) {
      SimpleToast.show('Password not matched');
    } else {
     
      let that = this;

      const {otp, newPassword, confirmPassword} = that.state;

      let data = null;

      data = JSON.stringify({
        current_password: otp,
        new_password: newPassword,
        confirm_password: confirmPassword,
      });
      api
        .resetPassword(data)
        .then((json) => {
          if (json.status == 200) {
            Toast.show(json.data.message);
            this.props.navigation.goBack();
          } else if (json.status == 400) {
            setTimeout(() => {
              Toast.show(json.data.message);
            }, 0);
          } else {
            setTimeout(() => {
              Toast.show(json.data.message);
            }, 0);
          }
        })
        .catch((error) => {
          setTimeout(() => {
            console.log('json.data.message', error);
            Toast.show(String(error.response.data.message));
          }, 0);
          console.log('login error:-', error.response.data.message);
        });
    }
  };

  sendOtp() {
        let data = null;
        let email, dial_code, phoneNumber;
        if (this.state.type == 2) {
            email = this.state.email;
            data = JSON.stringify({
                email: email,
            });
        } else {
            dial_code = this.state.selectedCountry && this.state.selectedCountry.callingCode ? this.state.selectedCountry.callingCode : null ;
            phoneNumber = this.state.phone;
            data = JSON.stringify({
                dial_code: dial_code ? dial_code : '91',
                phone: phoneNumber,
            });
        }

        api
            .resendOtp(data)
            .then((json) => {
                if (json.status == 200) {
                  Toast.show(json.data.message);
                } else if (json.status == 400) {
                    setTimeout(() => {
                        console.log(json.data.message);
                    }, 0);
                } else {
                    setTimeout(() => {
                        Toast.show(json.data.message);
                    }, 0);
                }
            })
            .catch((error) => {
                setTimeout(() => {
                    console.log('json.data.message', error);
                }, 0);
                console.log('login error:-', error.response.data.message);
            });
    
}

  callForgetPassword = async () => {
    if (!this.state.otp) {
      Toast.show('Enter otp');
    } else if (!this.state.newPassword) {
      Toast.show('Enter new password');
    } else if (this.state.newPassword.length < 8) {
      Toast.show('Password must be of min 8 characters.');
    } else if (!this.state.confirmPassword) {
      Toast.show('Enter confirm password');
    } else if (this.state.newPassword != this.state.confirmPassword) {
      Toast.show('Password not matched');
    } else {
      let that = this;
      const {phone, selectedCountry, email} = that.state;
      let data = null;

      if (this.state.type == 1) {
        data = JSON.stringify({
          dial_code: selectedCountry ? selectedCountry.callingCode[0] : '91',
          phone: phone,
          new_password: this.state.newPassword,
          confirm_password: this.state.confirmPassword,
          otp: this.state.otp,
        });
      } else {
        data = JSON.stringify({
          email: email,
          new_password: this.state.newPassword,
          confirm_password: this.state.confirmPassword,
          otp: this.state.otp,
        });
      }
      api
        .forgetPassword(data)
        .then((json) => {
          let data = JSON.stringify(json.data);
          if (json.status == 200) {
            Toast.show('Successfully updated password');
            this.props.navigation.navigate('LoginScreen');
          } else if (json.status == 400) {
            setTimeout(() => {
              Toast.show(json.data.message);
            }, 0);
          } else {
            setTimeout(() => {
              Toast.show(json.data.message);
            }, 0);
          }
        })
        .catch((error) => {
          setTimeout(() => {
            console.log('json.data.message', error);
            Toast.show(String(error.response.data.message));
          }, 0);
          console.log('login error:-', error.response.data.message);
        });
    }
  };

  render() {
    return (
      <SafeAreaView style={Styles.container}>
        <KeyboardAwareScrollView
          bounces={false}
          style={{flex: 1}}
          keyboardShouldPersistTaps={'handled'}
          enableResetScrollToCoords={false}>
          <View style={{alignSelf: 'center', width: wp(90)}}>
            <TouchableOpacity
              onPress={() => this.props.navigation.goBack(null)}>
              <View style={Styles.navbarView}>
                <Image
                  source={IMAGES.back_arrow}
                  style={{width: 15, height: 15}}></Image>
              </View>
            </TouchableOpacity>
          </View>
          <Text
            style={[
              Styles.heading_label,
              {textAlign: 'left', marginBottom: 10},
            ]}>
            New Password
          </Text>
          <Text style={[Styles.small_label]}>Enter your new password.</Text>
          <View
            style={{alignSelf: 'center', width: wp(90), marginVertical: 50}}>
           <View style={{height: this.state.isForgot ? 45 : null,marginBottom:this.state.isForgot ? 30 : null}}>
            <Hoshi
              label={this.state.isForgot ? 'OTP' : 'Current Password'}
              value={this.state.otp}
              borderColor={COLOR.BORDER}
              secureTextEntry={this.state.isForgot ? false : true}
              borderRadius={1}
              height={45}
              returnKeyType={'done'}
              keyboardType={'email-address'}
              style={{marginBottom: 15}}
              onChangeText={(text) => this.setState({otp: text})}
              inputStyle={Styles.text_input_text}
              labelStyle={Styles.text_input_heading}
            />
           {this.state.isForgot ? <View style={{position:'absolute',right:10,bottom:3}}>
            <TouchableOpacity
            onPress={()=>{
this.sendOtp()
            }}
            >
            <Text style={{color:COLOR.RED}}>Resend Code</Text>
            </TouchableOpacity>
            </View> : null}
            </View>
            <Hoshi
              label={'New Password'}
              value={this.state.newPassword}
              borderColor={COLOR.BORDER}
              borderRadius={1}
              secureTextEntry={true}
              height={45}
              returnKeyType={'done'}
              keyboardType={'email-address'}
              style={{marginBottom: 15}}
              onChangeText={(text) => this.setState({newPassword: text})}
              inputStyle={Styles.text_input_text}
              labelStyle={Styles.text_input_heading}
            />
            <Hoshi
              label={'Confirm Password'}
              value={this.state.confirmPassword}
              borderColor={COLOR.BORDER}
              secureTextEntry={true}
              borderRadius={1}
              height={45}
              returnKeyType={'done'}
              keyboardType={'email-address'}
              style={{marginBottom: 15}}
              onChangeText={(text) => this.setState({confirmPassword: text})}
              inputStyle={Styles.text_input_text}
              labelStyle={Styles.text_input_heading}
            />
          </View>
          <TouchableOpacity
            onPress={() => {
              if (this.state.isForgot) {
                this.callForgetPassword();
              } else {
                this.callResetPassword();
              }
            }}
            style={[Styles.button_content, {width: wp(90)}]}>
            <Text style={Styles.button_font}>Reset Password</Text>
          </TouchableOpacity>
        </KeyboardAwareScrollView>
      </SafeAreaView>
    );
  }
}

// const mapDispatchToProps = dispatch => {
//   return {
//     saveUserDetail: data => {
//       dispatch(actions.saveUserDetail(data));
//     },
//   };
// };

// export default connect(
//   null,
//   mapDispatchToProps,
// )(LoginScreen);
