'use strict';

import React from 'react';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scrollview';
import { Text, View, SafeAreaView, TouchableOpacity, Image, KeyboardAvoidingView } from 'react-native';
import {
  widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import COLOR from '../styles/Color';
import {Hoshi} from 'react-native-textinput-effects';
import Styles from '../styles/Styles';
import IMAGES from '../styles/Images';

import CountryPicker from 'react-native-country-picker-modal';
import {FloatingTitleTextInputField} from '../../helper/FloatingTitleTextInputField';
import API from '../../api/Api';
import SimpleToast from "react-native-simple-toast";


const api = new API();


export default class ForgotPassword extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            phoneNumber: '',
            email: '',
            selectedCountry: null,
            type: 1,
            isForgot: true
        };

        this.sendOtp = this.sendOtp.bind(this);
    }

    componentDidMount(): void {
        if (this.props.route.params && this.props.route.params.type) {
            this.setState({type: parseInt(this.props.route.params.type)})
        }
    }

    _updateMasterState = (attrName, value) => {
        this.setState({[attrName]: value});
    };


    sendOtp() {
        if (this.state.type == 1 && !this.state.phoneNumber) {
            SimpleToast.show('Enter phone number')
        } else if (this.state.type == 2 && !this.state.email) {
            SimpleToast.show('Enter email address')
        } else {
            let data = null;
            let email, dial_code, phoneNumber;
            if (this.state.type == 2) {
                email = this.state.email;
                data = JSON.stringify({
                    email: email,
                });
            } else {
                dial_code = this.state.selectedCountry && this.state.selectedCountry.callingCode ? this.state.selectedCountry.callingCode : null ;
                phoneNumber = this.state.phoneNumber;
                // const otp = value;

                data = JSON.stringify({
                    dial_code: dial_code ? dial_code : '91',
                    phone: phoneNumber,
                });
            }

            api
                .resendOtp(data)
                .then((json) => {
                    if (json.status == 200) {
                        // this.props.navigation.navigate('NewPassword', {state: this.state});
                        this.props.navigation.navigate('ForgotPasswordMiddleScreen', {state: this.state});
                    } else if (json.status == 400) {
                        setTimeout(() => {
                            console.log(json.data.message);
                        }, 0);
                    } else {
                        setTimeout(() => {
                            Toast.show(json.data.message);
                        }, 0);
                    }
                })
                .catch((error) => {
                    setTimeout(() => {
                        console.log('json.data.message', error);
                    }, 0);
                    console.log('login error:-', error.response.data.message);
                });
        }
    }

  render() {
      const {selectedCountry, phoneNumber, type} = this.state;
    return (
      <SafeAreaView style={Styles.container}>
        <View style={{ alignSelf: 'center', width: wp(90) }}>
          <TouchableOpacity onPress={() => this.props.navigation.goBack(null)}>
            <View style={Styles.navbarView}>
              <Image
                source={IMAGES.back_arrow}
                style={{ width: 15, height: 15 }}></Image>
            </View>
          </TouchableOpacity>
        </View>
          <Text
              style={[Styles.heading_label, {marginVertical: 0, marginBottom: 10}]}>
              Forgot Password
          </Text>
          <Text
              style={[
                  Styles.small_label,
                  {textAlign: 'center', width: wp(80), color: COLOR.VOILET},
              ]}>
              Enter your {type == 1 ? 'phone number' : 'email address'} below to receive an{'\n'}instruction to reset
              password.
          </Text>
          <View style={{
              flexDirection: 'row', alignItems: 'center', marginVertical: 50, borderRadius: 10,
              borderWidth: 1,
              alignSelf: 'center',
              width: wp(90),
              borderColor: COLOR.GRAY,

          }}>
              {type == 1 && (
                  <View style={{
                      backgroundColor: "#f2f2f2",
                      width: 100,
                      height: 50,
                      alignItems: 'center',
                      justifyContent: 'center',
                      marginTop: -5,
                      marginBottom: -5
                  }}>
                      <View style={phoneNumber != '' ? {marginTop: 0} : ''}>
                          <CountryPicker
                          withAlphaFilter={true}
                              containerButtonStyle={{marginLeft: 0}}
                              withCallingCode={true}
                              withEmoji={true}
                              withCallingCodeButton={true}
                              countryCode={
                                  selectedCountry ? selectedCountry.cca2 : 'IN'
                              }
                              visible={false}
                              onSelect={(country) => {
                                  this.setState({selectedCountry: country});
                              }}
                          />
                      </View>
                  </View>
              )}
              <View style={{flex: 1}}>
                  <FloatingTitleTextInputField
                      attrName={type == 1 ? "phoneNumber" : "email"}
                      keyboardType={type == 1 ? 'phone-pad' : 'email-address'}
                      title={type == 1 ? 'Phone number' : 'Email address'}
                      value={type == 1 ? this.state.phoneNumber : this.state.email}
                      updateMasterState={this._updateMasterState}
                      textInputStyles={{
                          // here you can add additional TextInput styles
                          color: COLOR.BLACK,
                          fontSize: 16,
                      }}
                      otherTextInputProps={{
                          // here you can add other TextInput props of your choice
                          maxLength: type == 1 ? 12 : 50,
                      }}
                  />
              </View>
        </View>
        <TouchableOpacity
          onPress={() => {
              this.sendOtp()
          }}
          style={[Styles.button_content, { width: wp(90) }]}>
            <Text style={Styles.button_font}>Send OTP</Text>
        </TouchableOpacity>
      </SafeAreaView>
    );
  }
}

// const mapDispatchToProps = dispatch => {
//   return {
//     saveUserDetail: data => {
//       dispatch(actions.saveUserDetail(data));
//     },
//   };
// };

// export default connect(
//   null,
//   mapDispatchToProps,
// )(LoginScreen);
