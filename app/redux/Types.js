//AUTH
const USER_DETAIL = 'USERDETAIL';
const USER_LOGOUT = 'USER_LOGOUT';
const ALL_PACAKGES = 'ALL_PACAKGES';

//PAYMENTS
const PAYMENT_METHODS = 'PAYMENT_METHODS';
const PAYMENT_CARDS = 'PAYMENT_CARDS';

//TRIPS
const UPCOMING_TRIPS = 'UPCOMING_TRIPS';
const UPCOMING_REQUEST_TRIP = 'UPCOMING_REQUEST_TRIP';
const PAST_REQUEST_TRIP = 'PAST_REQUEST_TRIP';
const CANCELLED_REQUEST_TRIP = 'CANCELLED_REQUEST_TRIP';
const CREATE_REQUEST_TRIP = 'CREATE_REQUEST_TRIP';
const SINGLE_TRIP_OFFER = 'SINGLE_TRIP_OFFER';

//TRAIN
const TRAIN_CLASSES = 'TRAIN_CLASSES';
const TRAIN_POLICIES = 'TRAIN_POLICIES';
const SCHEDULED_TRAINS = 'SCHEDULED_TRIPS';
const SINGLE_TRAIN_DATA = 'SINGLE_TRAIN_DATA';
const CLEAR_SINGLE_TRAIN = 'CLEAR_SINGLE_TRAIN';

//Car
const CAR_TYPE = 'CAR_TYPE';
const CAR_SPECIFICATION = 'CAR_SPECIFICATION';
const CAR_FINANCE_RESPONSIBILITY = 'CAR_FINANCE_RESPONSIBILITY';
const CAR_SUPPLIER = 'CAR_SUPPLIER';
const CAR_POLICY = 'CAR_POLICY';
const SINLGE_CAR_DATA = 'SINLGE_CAR_DATA';
const CLEAR_SINGLE_CAR_DATA = 'CLEAR_SINGLE_CAR_DATA';
const SCHEDULED_CARS = 'SCHEDULED_CARS';

//FLIGHT
const FLIGHT_AIRLINE = 'FLIGHT_AIRLINE';
const FLIGHT_EQUIPMENTS = 'FLIGHT_EQUIPMENTS';
const FLIGHT_STOPS = 'FLIGHT_STOPS';
const FLIGHT_SEATINGS = 'FLIGHT_SEATINGS';
const FLIGHT_TIMES = 'FLIGHT_TIMES';
const FLIGHT_POLICIES = 'FLIGHT_POLICIES';
const FLIGHT_CLASSES = 'FLIGHT_CLASSES';
const FLIGHT_MODES = 'FLIGHT_MODES';
const SINGLE_FLIGHT_DATA = 'SINGLE_FLIGHT_DATA';
const SCHEDULED_FLIGHTS = 'SCHEDULED_FLIGHTS';
const CLEAR_SINGLE_FLIGHT = 'CLEAR_SINGLE_FLIGHT';

//STAY
const STAY_ROOM_TYPES = 'STAY_ROOM_TYPES';
const STAY_BED_PREFERENCE = 'STAY_BED_PREFERENCE';
const STAY_SERVICES = 'STAY_SERVICES';
const STAY_PROPERTY = 'STAY_PROPERTY';
const STAY_POLICY = 'STAY_POLICY';
const SCHEDULED_STAY = 'SCHEDULED_STAY';
const SINGLE_STAY_DATA = 'SINGLE_STAY_DATA';
const CLEAR_SINGLE_STAY = 'CLEAR_SINGLE_STAY';
//CRUISE
const CRUISE_FEATURE = 'CRUISE_FEATURE';
const CRUISE_SUPPLIER = 'CRUISE_SUPPLIER';
const CRUISE_CABIN_TYPE = 'CRUISE_CABIN_TYPE';
const SCHEDULED_CRUISE = 'SCHEDULED_CRUISE';
const SINGLE_CRUISE_DATA = 'SINGLE_CRUISE_DATA';
const CLEAR_SINGLE_CRUISE = 'CLEAR_SINGLE_CRUISE';

//TRAVELLER
const TRAVELLERS_TYPE = 'TRAVELLERS_TYPE';
const SINGLE_TRAVELLER_DATA = 'SINGLE_TRAVELLER_DATA';

//SETTING
const LANGUAGES = 'LANGUAGES';
const CURRENCIES = 'CURRENCIES';
const UNITS = 'UNITS';
const TEMPERATURE = 'TEMPERATURE';
const TIME_FORMAT = 'TIME_FORMAT';
const NAVIGATION_APPS = 'NAVIGATION_APPS';
const COUNTRIES = 'COUNTRIES';
const SPECIAL_NEEDS = 'SPECIAL_NEEDS';
const SETTINGS = 'SETTINGS';

//PROFILE
const UPDATE_PROFILE = 'UPDATE_PROFILE';

//PACKAGE
const ADMINISTRATIVE_FEES = 'ADMINISTRATIVE_FEES';
const BOOKED_PACKAGE = 'BOOKED_PACKAGE';
const CANCELLED_BOOKED_PACKAGE = 'CANCELLED_BOOKED_PACKAGE';
const PAST_BOOKED_PACKAGE = 'PAST_BOOKED_PACKAGE';
const PACKAGE_SUMMARY = 'PACKAGE_SUMMARY';
const CREATE_PACKAGE_DATA = 'CREATE_PACKAGE_DATA';
const PACKAGE_REQUEST = 'PACKAGE_REQUEST';
const SINGLE_PACKAGE_REQUEST = 'SINGLE_PACKAGE_REQUEST';
const SINGLE_PACKAGE_OFFER = 'SINGLE_PACKAGE_OFFER';

const PACKAGE_P2 = 'PACKAGE_P2';
const TRIP_P2 = 'TRIP_P2';

//TRIP PLAN

const ATTRACTION_SHOW = 'ATTRACTION_SHOW';
const CUISINE_TYPES = 'CUISINE_TYPES';
const ACTIVITY_EXPERIENCES = 'ACTIVITY_EXPERIENCES';

const AIR_ACTIVITIES = 'AIR_ACTIVITIES';
const SEA_ACTIVITIES = 'SEA_ACTIVITIES';
const FOOD_EXPERIENCES = 'FOOD_EXPERIENCES';
const SPORTS = 'SPORTS';
const OUTDOOR_EXPERIENCES = 'OUTDOOR_EXPERIENCES';
const INDOOR_ACTIVITIES = 'INDOOR_ACTIVITIES';
const WORKSHOPS = 'WORKSHOPS';
const PAST_TRIPS = 'PAST_TRIPS';
const CREDIT_DATA = 'CREDIT_DATA';

const ALL_OFFERS = 'ALL_OFFERS';
const TRIP_ADDONS = 'TRIP_ADDONS';

export default {
  USER_DETAIL,
  USER_LOGOUT,

  BOOKED_PACKAGE,
  CANCELLED_BOOKED_PACKAGE,
  PAST_BOOKED_PACKAGE,

  CREDIT_DATA,

  LANGUAGES,
  CURRENCIES,
  UNITS,
  TEMPERATURE,
  TIME_FORMAT,
  NAVIGATION_APPS,
  COUNTRIES,
  SPECIAL_NEEDS,
  SETTINGS,

  ALL_PACAKGES,
  PAYMENT_METHODS,
  PAYMENT_CARDS,
  ADMINISTRATIVE_FEES,
  TRAVELLERS_TYPE,
  SINGLE_TRAVELLER_DATA,

  UPCOMING_TRIPS,
  PAST_TRIPS,
  UPCOMING_REQUEST_TRIP,
  CREATE_REQUEST_TRIP,
  PAST_REQUEST_TRIP,
  CANCELLED_REQUEST_TRIP,
  SINGLE_TRIP_OFFER,
  ALL_OFFERS,
TRIP_ADDONS,

  FLIGHT_AIRLINE,
  FLIGHT_EQUIPMENTS,
  FLIGHT_STOPS,
  FLIGHT_SEATINGS,
  FLIGHT_TIMES,
  FLIGHT_POLICIES,
  FLIGHT_CLASSES,
  FLIGHT_MODES,
  SINGLE_FLIGHT_DATA,
  SCHEDULED_FLIGHTS,
  CLEAR_SINGLE_FLIGHT,

  STAY_ROOM_TYPES,
  STAY_BED_PREFERENCE,
  STAY_SERVICES,
  STAY_PROPERTY,
  STAY_POLICY,
  SCHEDULED_STAY,
  SINGLE_STAY_DATA,
  CLEAR_SINGLE_STAY,

  CRUISE_CABIN_TYPE,
  CRUISE_FEATURE,
  CRUISE_SUPPLIER,
  SCHEDULED_CRUISE,
  SINGLE_CRUISE_DATA,
  CLEAR_SINGLE_CRUISE,

  TRAIN_CLASSES,
  TRAIN_POLICIES,
  SCHEDULED_TRAINS,
  SINGLE_TRAIN_DATA,
  CLEAR_SINGLE_TRAIN,

  CAR_TYPE,
  CAR_SPECIFICATION,
  CAR_FINANCE_RESPONSIBILITY,
  CAR_SUPPLIER,
  CAR_POLICY,
  SINLGE_CAR_DATA,
  CLEAR_SINGLE_CAR_DATA,
  SCHEDULED_CARS,

  UPDATE_PROFILE,
  PACKAGE_SUMMARY,
  PACKAGE_REQUEST,
  CREATE_PACKAGE_DATA,
  SINGLE_PACKAGE_REQUEST,
  SINGLE_PACKAGE_OFFER,

  PACKAGE_P2,
  TRIP_P2,

  ATTRACTION_SHOW,
  CUISINE_TYPES,
  ACTIVITY_EXPERIENCES,
  AIR_ACTIVITIES,
  SEA_ACTIVITIES,
  FOOD_EXPERIENCES,
  SPORTS,
  OUTDOOR_EXPERIENCES,
  INDOOR_ACTIVITIES,
  WORKSHOPS,
};
