import types from '../../redux/Types';
import {AS_USER_DETAIL} from '../../helper/Constants';
import API from '../../api/Api';
import Toast from 'react-native-simple-toast';
import Auth from '../../auth/index';

const api = new API();
const auth = new Auth();

const getUserInfo = () => {
  return async (dispatch, getStore) => {
    try {
      api
        .getUserDetail()
        .then((json) => {
          console.log('user infor is:-', json.data);
          let data = JSON.stringify(json.data.response);
          saveUserDetail(data);
          if (json.data.status == 200) {
            dispatch({
              type: types.USER_DETAIL,
              data: json.data.response,
            });
          } else {
            Toast.show(json.data.message);
          }
        })
        .catch(function (error) {
          console.log(error);
        });
    } catch (error) {
      console.log('error getting user detail');
    }
  };
};

const updateProfile = (data, val, id) => {
  return async (dispatch, getStore) => {
    try {
      api
        .updateProfile(data, val, id)
        .then((json) => {
          let data = JSON.stringify(json.data);
          if (json.status == 200) {
            if (val == 1 && id == null) {
              dispatch(getTravellers());
            } else {
              dispatch(getUserInfo());
            }
            Toast.show('Details Updated');
          } else if (json.status == 400) {
            setTimeout(() => {
              Toast.show(json.data.message);
            }, 0);
          } else {
            setTimeout(() => {
              Toast.show(json.data.message);
            }, 0);
          }
        })
        .catch((error) => {
          setTimeout(() => {
            console.log('json.data.message', error);
            Toast.show(String(error.response.data.message));
          }, 0);
          console.log('error:-', error.response.data.message);
        });
    } catch (error) {
      console.log(error.message);
    }
  };
};

const updateOrCreatePassport = (data, val, id) => {
  return async (dispatch, getStore) => {
    try {
      api
        .updateOrCreatePassport(data, val, id)
        .then((json) => {
          Toast.show('Details Updated');
          dispatch(getTravellers());
        })
        .catch((error) => {
          setTimeout(() => {
            console.log('json.data.message', error);
            Toast.show(String(error.response.data.message));
          }, 0);
          console.log('error:-', error.response.data.message);
        });
    } catch (error) {
      console.log(error.message);
    }
  };
};

const getPreferenceSetting = (val) => {
  return async (dispatch, getStore) => {
    try {
      api
        .getPreferenceSetting(val)
        .then((json) => {
          if (json.status == 200) {
            dispatch({
              type: types.SETTINGS,
              data: json.data.response,
            });
          }
        })
        .catch(function (error) {
          console.log(error);
        });
    } catch (error) {
      console.log(error);
    }
  };
};

const savePreferenceSetting = (data) => {
  return async (dispatch, getStore) => {
    try {
      api
        .savePreferenceSetting(data)
        .then((json) => {
          if (json.status == 200) {
            console.log('save setting', json.data.response);
          }
        })
        .catch(function (error) {
          console.log(error.response.data);
        });
    } catch (error) {
      console.log(error);
    }
  };
};

const getTravellers = (val) => {
  return async (dispatch, getStore) => {
    try {
      api
        .getTravellers()
        .then((json) => {
          console.log('traveller responce:-', json.data);
          if (json.data.status == 200) {
            dispatch({
              type: types.TRAVELLERS_TYPE,
              data: json.data.response,
            });
          } else {
            Toast.show(json.data.message);
          }
        })
        .catch((error) => {
          setTimeout(() => {
            console.log('json.data.message', error);
            Toast.show(String(error.response.data.message));
          }, 0);
          console.log('error:-', error.response.data.message);
        });
    } catch (error) {
      console.log(error);
    }
  };
};

const deleteTraveller = (val) => {
  return async (dispatch, getStore) => {
    try {
      api
        .deleteTraveller(val)
        .then((json) => {
          console.log('delete responce:-', json.data);
          let data = JSON.stringify(json.data);
          if (json.status == 200) {
            dispatch(getTravellers());
          } else if (json.status == 400) {
            setTimeout(() => {
              Toast.show(json.data.message);
            }, 0);
          } else {
            setTimeout(() => {
              Toast.show(json.data.message);
            }, 0);
          }
        })
        .catch((error) => {
          setTimeout(() => {
            console.log('json.data.message', error);
            Toast.show(String(error.response.data.message));
          }, 0);
          console.log('error:-', error.response.data.message);
        });
    } catch (error) {
      console.log(error.message);
    }
  };
};

const getLanguages = () => {
  return async (dispatch, getStore) => {
    let languages = getStore().profileReducer.languages;

    if (!languages) {
      try {
        api
          .getLanguages()
          .then((json) => {
            console.log('languages is:-', json.data);

            if (json.data.status == 200) {
              dispatch({
                type: types.LANGUAGES,
                data: json.data.response,
              });
            } else {
              Toast.show(json.data.message);
            }
          })
          .catch(function (error) {
            Toast.show(error.response.data.message);
          });
      } catch (error) {
        console.log('error getting user detail');
      }
    } else {
      return dispatch({
        type: types.LANGUAGES,
        data: languages,
      });
    }
  };
};

const getCurrencies = () => {
  return async (dispatch, getStore) => {
    let currencies = getStore().profileReducer.currencies;

    console.log(currencies);
    if (!currencies) {
      try {
        api
          .getCurrencies()
          .then((json) => {
            console.log('currencies is:-', json.data);

            if (json.data.status == 200) {
              dispatch({
                type: types.CURRENCIES,
                data: json.data.response,
              });
            } else {
              Toast.show(json.data.message);
            }
          })
          .catch(function (error) {
            Toast.show(error.response.data.message);
          });
      } catch (error) {
        console.log('error getting user detail');
      }
    } else {
      return dispatch({
        type: types.CURRENCIES,
        data: currencies,
      });
    }
  };
};

const getUnits = () => {
  return async (dispatch, getStore) => {
    let units = getStore().profileReducer.units;

    if (!units) {
      try {
        api
          .getUnits()
          .then((json) => {
            console.log('units is:-', json.data);

            if (json.data.status == 200) {
              dispatch({
                type: types.UNITS,
                data: json.data.response,
              });
            } else {
              Toast.show(json.data.message);
            }
          })
          .catch(function (error) {
            Toast.show(error.response.data.message);
          });
      } catch (error) {
        console.log('error getting user detail');
      }
    } else {
      return dispatch({
        type: types.UNITS,
        data: units,
      });
    }
  };
};

const getTimeFormat = () => {
  return async (dispatch, getStore) => {
    let timeFormat = getStore().profileReducer.timeFormat;

    if (!timeFormat) {
      try {
        api
          .getTimeFormat()
          .then((json) => {
            console.log('time format is:-', json.data);

            if (json.data.status == 200) {
              dispatch({
                type: types.TIME_FORMAT,
                data: json.data.response,
              });
            } else {
              Toast.show(json.data.message);
            }
          })
          .catch(function (error) {
            Toast.show(error.response.data.message);
          });
      } catch (error) {
        console.log('error getting user detail');
      }
    } else {
      return dispatch({
        type: types.TIME_FORMAT,
        data: timeFormat,
      });
    }
  };
};

const getTemperature = () => {
  return async (dispatch, getStore) => {
    let temperature = getStore().profileReducer.temperature;

    if (!temperature) {
      try {
        api
          .getTemperature()
          .then((json) => {
            console.log('temperature is:-', json.data);

            if (json.data.status == 200) {
              dispatch({
                type: types.TEMPERATURE,
                data: json.data.response,
              });
            } else {
              Toast.show(json.data.message);
            }
          })
          .catch(function (error) {
            Toast.show(error.response.data.message);
          });
      } catch (error) {
        console.log('error getting user detail');
      }
    } else {
      return dispatch({
        type: types.TEMPERATURE,
        data: temperature,
      });
    }
  };
};

const getNavigationApps = () => {
  return async (dispatch, getStore) => {
    let navigationApps = getStore().profileReducer.navigationApps;

    if (!navigationApps) {
      try {
        api
          .getNavigationApps()
          .then((json) => {
            console.log('navigation Apps is:-', json.data);

            if (json.data.status == 200) {
              dispatch({
                type: types.NAVIGATION_APPS,
                data: json.data.response,
              });
            } else {
              Toast.show(json.data.message);
            }
          })
          .catch(function (error) {
            Toast.show(error.response.data.message);
          });
      } catch (error) {
        console.log('error getting user detail');
      }
    } else {
      return dispatch({
        type: types.NAVIGATION_APPS,
        data: navigationApps,
      });
    }
  };
};

const getCountries = () => {
  return async (dispatch, getStore) => {
    try {
      api
        .getCountries()
        .then((json) => {
          console.log('navigation Apps is:-', json.data);

          if (json.data.status == 200) {
            dispatch({
              type: types.COUNTRIES,
              data: json.data.response,
            });
          } else {
            Toast.show(json.data.message);
          }
        })
        .catch(function (error) {
          Toast.show(error.response.data.message);
        });
    } catch (error) {
      console.log('error getting user detail');
    }
  };
};

const getSpecialNeeds = () => {
  return async (dispatch, getStore) => {
    try {
      api
        .getSpecialNeeds()
        .then((json) => {
          console.log('Special needs is:-', json.data);

          if (json.data.status == 200) {
            dispatch({
              type: types.SPECIAL_NEEDS,
              data: json.data.response,
            });
          } else {
            Toast.show(json.data.message);
          }
        })
        .catch(function (error) {
          Toast.show(error.response.data.message);
        });
    } catch (error) {
      console.log('error getting user detail');
    }
  };
};

const getSingleTraveller = (val) => {
  return async (dispatch, getStore) => {
    api
      .getSingleTraveller(val)
      .then((json) => {
        console.log('get single traveller ', json.data.response);
        dispatch({
          type: types.SINGLE_TRAVELLER_DATA,
          data: json.data.response,
        });
      })
      .catch(function (error) {
        console.log(error.response.data.message);
      });
  };
};

const saveUserDetail = (data) => {
  return async (dispatch, getStore) => {
    try {
      await auth.setValue(AS_USER_DETAIL, data);
      // await dispatch({type: types.USER_DETAIL, data: JSON.parse(data)});
    } catch (error) {
      console.log(error.message);
    }
  };
};
const clearProfile = () => {
  return async (dispatch, getStore) => {
    console.log('Clear Profile');
    dispatch({
      type: 'CLEAR_PROFILE',
    });
  };
};

const logoutUser = () => {
  return async (dispatch, getStore) => {
    dispatch({
      type: types.USER_LOGOUT,
      data: null,
    });
    dispatch(clearProfile());
  };
};

export default {
  getUserInfo,
  logoutUser,
  saveUserDetail,
  updateProfile,
  updateOrCreatePassport,
  getTravellers,
  deleteTraveller,
  getLanguages,
  getCurrencies,
  getUnits,
  getTemperature,
  getTimeFormat,
  getNavigationApps,
  getCountries,
  savePreferenceSetting,
  getPreferenceSetting,
  getSpecialNeeds,
  getSingleTraveller,
};
