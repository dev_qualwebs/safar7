import types from '../../redux/Types';
import API from '../../api/Api';
import Toast from 'react-native-simple-toast';
import RequestAction from './RequestAction';
import PackageAction from './PackageAction';

const api = new API();

const getCruiseCabin = (type) => {
  return async (dispatch, getStore) => {
    let cruiseCabinData = getStore().cruiseReducer.cruiseCabinTypeData;
    if (cruiseCabinData.length == 0 || type == 1) {
      try {
        api
          .getCruiseCabinData()
          .then((json) => {
            console.log('cabin type data is:-', json.data.response);
            if (json.status == 200) {
              dispatch({
                type: types.CRUISE_CABIN_TYPE,
                data: json.data.response,
              });
            } else {
              Toast.show(json.data.message);
            }
          })
          .catch(function (error) {
            Toast.show(error.response.data.message);
          });
      } catch (error) {
        console.log(error);
      }
    } else {
      return dispatch({
        type: types.CRUISE_CABIN_TYPE,
        data: cruiseCabinData,
      });
    }
  };
};

const getCruiseSupplier = (type) => {
  return async (dispatch, getStore) => {
    let cruiseSupplierData = getStore().cruiseReducer.cruiseSupplierData;
    if (cruiseSupplierData.length == 0 || type == 1) {
      try {
        api
          .getCruiseSupplies()
          .then((json) => {
            console.log(' Supplier :-', json.data.response);
            if (json.status == 200) {
              dispatch({
                type: types.CRUISE_SUPPLIER,
                data: json.data.response,
              });
            } else {
              Toast.show(json.data.message);
            }
          })
          .catch(function (error) {
            Toast.show(error.response.data.message);
          });
      } catch (error) {
        console.log(error);
      }
    } else {
      return dispatch({
        type: types.CRUISE_SUPPLIER,
        data: cruiseSupplierData,
      });
    }
  };
};

const getCruiseFeature = (type) => {
  return async (dispatch, getStore) => {
    let cruiseFeatureData = getStore().cruiseReducer.creuiseFeatureData;
    if (cruiseFeatureData.length == 0 || type == 1) {
      try {
        api
          .getCruiseFeatures()
          .then((json) => {
            console.log('feature type data is:-', json.data.response);
            if (json.status == 200) {
              dispatch({
                type: types.CRUISE_FEATURE,
                data: json.data.response,
              });
            } else {
              Toast.show(json.data.message);
            }
          })
          .catch(function (error) {
            Toast.show(error.response.data.message);
          });
      } catch (error) {
        console.log(error);
      }
    } else {
      return dispatch({
        type: types.CRUISE_FEATURE,
        data: cruiseFeatureData,
      });
    }
  };
};

const scheduleCruise = (data, id) => {
  return async (dispatch, store) => {
    try {
      api
        .schduleCruise(data, id)
        .then((json) => {
          console.log(json.data);
          if (json.status == 200) {
            let parsedObject = JSON.parse(data);
            if (parsedObject.package_request_id) {
              dispatch(
                PackageAction.getSinglePackageRequest(
                  parsedObject.package_request_id,
                ),
              );
            } else {
              dispatch(RequestAction.getUpcomingTrips());
              dispatch(PackageAction.getRequestedSummary(id));
            }
            dispatch(getScheduledCruise(id));
            console.log('Success');
            Toast.show(json.data.message);
          } else if (json.status == 400) {
            setTimeout(() => {
              Toast.show(json.data.message);
            }, 0);
          } else {
            setTimeout(() => {
              Toast.show(json.data.message);
            }, 0);
          }
        })
        .catch((error) => {
          console.log('json.data.message', error.response);
          setTimeout(() => {
            Toast.show(String(error.response.data.message));
          }, 0);
          console.log('error:-', error.response.data.message);
        });
    } catch (error) {
      console.log(error.message);
    }
  };
};

const getScheduledCruise = (val) => {
  return (dispatch, getStore) => {
    try {
      api
        .getScheduledCruise(val)
        .then((json) => {
          console.log(json.data);
          if (json.status == 200) {
            console.log('scheduled cruise', json.data.response);
            dispatch({
              type: types.SCHEDULED_CRUISE,
              data: json.data.response,
            });
          } else if (json.status == 400) {
            setTimeout(() => {
              Toast.show(json.data.message);
            }, 0);
          } else {
            setTimeout(() => {
              Toast.show(json.data.message);
            }, 0);
          }
        })
        .catch((error) => {
          setTimeout(() => {
            console.log('json.data.message', error);
            Toast.show(String(error));
          }, 0);
          console.log('error:-', error);
        });
    } catch (error) {
      console.log(error.message);
    }
  };
};

const updateCruise = (data, id) => {
  return async (dispatch, store) => {
    try {
      api
        .updateCruise(data, id)
        .then((json) => {
          console.log(json.data);
          if (json.status == 200) {
            let parsedObject = JSON.parse(data);
            if (parsedObject.package_request_id) {
              dispatch(
                PackageAction.getSinglePackageRequest(
                  parsedObject.package_request_id,
                ),
              );
            } else {
              dispatch(RequestAction.getUpcomingTrips());
              dispatch(PackageAction.getRequestedSummary(parsedObject.tripId));
            }
            console.log('Success');
            Toast.show(json.data.message);
          }
        })
        .catch((error) => {
          setTimeout(() => {
            console.log('json.data.message', error);
            Toast.show(String(error.response.data.message));
          }, 0);
        });
    } catch (error) {
      console.log(error.message);
    }
  };
};

const getSingleCruise = (id) => {
  return async (dispatch, store) => {
    try {
      api
        .getSingleCruise(id)
        .then((json) => {
          console.log(json.data);
          if (json.status == 200) {
            console.log('Single cruise', json.data.response);
            dispatch({
              type: types.SINGLE_CRUISE_DATA,
              data: json.data.response,
            });
          }
        })
        .catch((error) => {
          console.log('json.data.message', error);
        });
    } catch (error) {
      console.log(error.message);
    }
  };
};

const clearSingleCruise = () => {
  return async (dispatch, store) => {
    dispatch({
      type: types.CLEAR_SINGLE_CRUISE,
    });
  };
};
export default {
  getCruiseCabin,
  getCruiseSupplier,
  getCruiseFeature,
  scheduleCruise,
  getScheduledCruise,
  updateCruise,
  getSingleCruise,
  clearSingleCruise,
};
