import types from '../../redux/Types';
import API from '../../api/Api';
import Toast from 'react-native-simple-toast';
import TripAction from './TripAction';
import RequestAction from './RequestAction';
import PackageAction from './PackageAction';

const api = new API();

const getTrainClasses = () => {
  return async (dispatch, getStore) => {
    let trainClasses = getStore().trainReducer.trainClasses;
    if (trainClasses.length == 0) {
      try {
        api
          .getTrainClasses()
          .then((json) => {
            console.log('train classes :-', json.data.response);
            if (json.status == 200) {
              dispatch({
                type: types.TRAIN_CLASSES,
                data: json.data.response,
              });
            } else {
              Toast.show(json.data.message);
            }
          })
          .catch(function (error) {
            Toast.show(error.response.data.message);
          });
      } catch (error) {
        console.log(error);
      }
    } else {
      return dispatch({
        type: types.TRAIN_CLASSES,
        data: trainClasses,
      });
    }
  };
};

const getTrainPolicies = () => {
  return async (dispatch, getStore) => {
    let trainPolicies = getStore().trainReducer.trainPolicies;
    if (trainPolicies.length == 0) {
      try {
        api
          .getTrainPolicies()
          .then((json) => {
            console.log('train policies :-', json.data.response);
            if (json.status == 200) {
              dispatch({
                type: types.TRAIN_POLICIES,
                data: json.data.response,
              });
            } else {
              Toast.show(json.data.message);
            }
          })
          .catch(function (error) {
            Toast.show(error.response.data.message);
          });
      } catch (error) {
        console.log(error);
      }
    } else {
      return dispatch({
        type: types.TRAIN_POLICIES,
        data: trainPolicies,
      });
    }
  };
};

const scheduleTrain = (data, id) => {
  return async (dispatch, store) => {
    try {
      api
        .scheduleTrain(data, id)
        .then((json) => {
          console.log(json.data);
          if (json.status == 200) {
            let parsedObject = JSON.parse(data);
            if (parsedObject.package_request_id) {
              dispatch(
                PackageAction.getSinglePackageRequest(
                  parsedObject.package_request_id,
                ),
              );
            } else {
              dispatch(PackageAction.getRequestedSummary(id));
              dispatch(RequestAction.getUpcomingTrips());
            }

            dispatch(getScheduledTrains(id));
            console.log('Succes');
            Toast.show(json.data.message);
          } else if (json.status == 400) {
            setTimeout(() => {
              Toast.show(json.data.message);
            }, 0);
          } else {
            setTimeout(() => {
              Toast.show(json.data.message);
            }, 0);
          }
        })
        .catch((error) => {
          setTimeout(() => {
            console.log('json.data.message', error);
            Toast.show(String(error.response.data.message));
          }, 0);
          console.log('error:-', error.response.data.message);
        });
    } catch (error) {
      console.log(error.message);
    }
  };
};

const updateTrain = (data, id) => {
  return async (dispatch, store) => {
    try {
      api
        .updateTrain(data, id)
        .then((json) => {
          console.log(json.data);
          if (json.status == 200) {
            let parsedObject = JSON.parse(data);
            if (parsedObject.package_request_id) {
              dispatch(
                PackageAction.getSinglePackageRequest(
                  parsedObject.package_request_id,
                ),
              );
            } else {
              dispatch(getScheduledTrains(parsedObject.tripId));
              dispatch(PackageAction.getRequestedSummary(parsedObject.tripId));
              dispatch(RequestAction.getUpcomingTrips());
            }
            console.log('Success');
            Toast.show(json.data.message);
          } else if (json.status == 400) {
            setTimeout(() => {
              Toast.show(json.data.message);
            }, 0);
          } else {
            setTimeout(() => {
              Toast.show(json.data.message);
            }, 0);
          }
        })
        .catch((error) => {
          setTimeout(() => {
            console.log('json.data.message', error);
            Toast.show(String(error.response.data.message));
          }, 0);
          console.log('error:-', error.response.data.message);
        });
    } catch (error) {
      console.log(error.message);
    }
  };
};

const getScheduledTrains = (id) => {
  return async (dispatch, store) => {
    try {
      api
        .getScheduledTrains(id)
        .then((json) => {
          console.log(json.data);
          if (json.status == 200) {
            console.log('scheduled trains', json.data.response);
            dispatch({
              type: types.SCHEDULED_TRAINS,
              data: json.data.response,
            });
          }
        })
        .catch((error) => {
          setTimeout(() => {
            console.log('json.data.message', error);
            Toast.show(String(error));
          }, 0);
          console.log('error:-', error);
        });
    } catch (error) {
      console.log(error.message);
    }
  };
};

const deleteScheduledTrain = (id, tripId) => {
  return async (dispatch, store) => {
    api
      .deleteScheduledTrain(id)
      .then((json) => {
        if (json.status == 200) {
          Toast.show('Deleted Succesfully');
          dispatch(RequestAction.getUpcomingTrips(tripId));
        }
      })
      .catch(function (error) {
        Toast.show(error.response.data.message);
      });
  };
};

const getSingleTrain = (id) => {
  return async (dispatch, store) => {
    try {
      api
        .getSingleTrain(id)
        .then((json) => {
          if (json.status == 200) {
            console.log('single train', json.data.response);
            dispatch({
              type: types.SINGLE_TRAIN_DATA,
              data: json.data.response,
            });
          }
        })
        .catch((error) => {
          setTimeout(() => {
            console.log('json.data.message', error);
            Toast.show(String(error));
          }, 0);
          console.log('error:-', error);
        });
    } catch (error) {
      console.log(error.message);
    }
  };
};

const clearSingleTrain = () => {
  return async (dispatch, store) => {
    dispatch({
      type: types.CLEAR_SINGLE_TRAIN,
    });
  };
};
export default {
  getTrainClasses,
  getTrainPolicies,
  scheduleTrain,
  getScheduledTrains,
  deleteScheduledTrain,
  getSingleTrain,
  updateTrain,
  clearSingleTrain,
};
