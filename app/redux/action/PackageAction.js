import types from '../../redux/Types';
import API from '../../api/Api';
import Toast from 'react-native-simple-toast';
import Auth from '../../auth/index';
import {Alert} from 'react-native';
import SimpleToast from 'react-native-simple-toast';

const api = new API();

const getAllPackages = (type) => {
  return async (dispatch, getStore) => {
    let packageData = getStore().pacakgeReducer.allPackageData;
    if (packageData.length == 0 || type == 1) {
      try {
        api
          .getAllPacakges()
          .then((json) => {
            console.log('all package data is:-', json.data.response);
            if (json.status == 200) {
              dispatch({
                type: types.ALL_PACAKGES,
                data: json.data.response,
              });
            } else {
              Toast.show(json.data.message);
            }
          })
          .catch(function (error) {
            Toast.show(error.response.data.message);
          });
      } catch (error) {
        console.log(error);
      }
    } else {
      return dispatch({
        type: types.ALL_PACAKGES,
        data: packageData,
      });
    }
  };
};

const getAllOffers = (type) => {
  return async (dispatch, getStore) => {
    let offersData = getStore().pacakgeReducer.allOffersData;
    if (offersData.length == 0 || type == 1) {
      try {
        api
          .getAllOffers()
          .then((json) => {
            console.log('all offers data is:-', json.data.response);
            if (json.status == 200) {
              dispatch({
                type: types.ALL_OFFERS,
                data: json.data.response,
              });
            } else {
              Toast.show(json.data.message);
            }
          })
          .catch(function (error) {
            Toast.show(error.response.data.message);
          });
      } catch (error) {
        console.log(error);
      }
    } else {
      return dispatch({
        type: types.ALL_OFFERS,
        data: offersData,
      });
    }
  };
};

const getBookedPackage = () => {
  return async (dispatch, getStore) => {
    let data = JSON.stringify({status: 0});
    try {
      api
        .getBookedTrips(data)
        .then((json) => {
          console.log('booked trip data is:-', json.data.response);
          if (json.status == 200) {
            dispatch({
              type: types.BOOKED_PACKAGE,
              data: json.data.response,
            });
          } else {
            Toast.show(json.data.message);
          }
        })
        .catch(function (error) {
          Toast.show(error.response.data.message);
        });
    } catch (error) {
      console.log(error);
    }
  };
};

const getCancelledPackage = () => {
  return async (dispatch, getStore) => {
    let data = JSON.stringify({status: 5});
    try {
      api
        .getBookedTrips(data)
        .then((json) => {
          console.log('cancelled package data is:-', json.data.response);
          if (json.status == 200) {
            dispatch({
              type: types.CANCELLED_BOOKED_PACKAGE,
              data: json.data.response,
            });
          } else {
            Toast.show(json.data.message);
          }
        })
        .catch(function (error) {
          Toast.show(error.response.data.message);
        });
    } catch (error) {
      console.log(error);
    }
  };
};

const getPastPackage = () => {
  return async (dispatch, getStore) => {
    let data = JSON.stringify({status: 6});
    try {
      api
        .getBookedTrips(data)
        .then((json) => {
          console.log('cancelled package data is:-', json.data.response);
          if (json.status == 200) {
            dispatch({
              type: types.PAST_BOOKED_PACKAGE,
              data: json.data.response,
            });
          } else {
            Toast.show(json.data.message);
          }
        })
        .catch(function (error) {
          Toast.show(error.response.data.message);
        });
    } catch (error) {
      console.log(error);
    }
  };
};

getAdministrativeFees = (val) => {
  return async (dispatch, getStore) => {
    try {
      api
        .getAdministrativeFees(val)
        .then((json) => {
          console.log('administrative', json.data.response);
          dispatch({
            type: types.ADMINISTRATIVE_FEES,
            data: json.data.response,
          });
        })
        .catch(function (error) {
          console.log(error);
        });
    } catch (e) {
      console.log(e);
    }
  };
};

const requestPackage = (data) => {
  return async (dispatch, getStore) => {
    try {
      api
        .requestPackage(data)
        .then((json) => {
          console.log(json.data.response);
          dispatch({
            type: types.PACKAGE_REQUEST,
            data: json.data.response,
          });
        })
        .catch(function (error) {
          console.log(error.response.data.message);
          SimpleToast.show(error.response.data.message);
        });
    } catch (error) {
      console.log(error);
    }
  };
};

const getRequestedSummary = (val) => {
  return async (dispatch, getStore) => {
    try {
      api
        .getRequestedSummary(val)
        .then((json) => {
          console.log('request summary:-', json.data.response);
          dispatch({
            type: types.PACKAGE_SUMMARY,
            data: json.data.response,
          });
        })
        .catch(function (error) {
          Toast.show(error.response.data.message);
          console.log(error.response.data.message);
        });
    } catch (e) {
      console.log(e);
    }
  };
};

const createPackageRequest = (data) => {
  return async (dispatch, store) => {
    try {
      api
        .createPackageRequest(data)
        .then((json) => {
          console.log('create package', json.data.response);
          dispatch({
            type: types.CREATE_PACKAGE_DATA,
            data: json.data.response,
          });
        })
        .catch(function (error) {
          console.log(error.response.data.message);
        });
    } catch (error) {
      console.log(error);
    }
  };
};

const getSinglePackageOffer = (val) => {
  return async (dispatch, getStore) => {
    api
      .getSinglePackageOffer(val)
      .then((json) => {
        console.log('Single Package Offer', json.data);
        dispatch({
          type: types.SINGLE_PACKAGE_OFFER,
          data: json.data.response,
        });
      })
      .catch(function (error) {
        console.log(error.response.data);
      });
  };
};

const rejectOfferPackage = (val, data) => {
  return async (dispatch, stote) => {
    api
      .rejectOfferPackage(val, data)
      .then((json) => {
        SimpleToast.show('Offer Rejected');
        console.log(json.data.message);
        dispatch(getBookedPackage());
        dispatch(getCancelledPackage());
      })
      .catch(function (error) {
        console.log(error.response.data.message);
      });
  };
};

const getCreditsData = () =>{
  
  return async (dispatch, getStore) => {
    let creditData = getStore().pacakgeReducer.creditData;
    if (creditData == null || (creditData && creditData.length == 0)) {
    api
      .getCreditData()
      .then((json) => {
        dispatch({
          type: types.CREDIT_DATA,
          data: json.data.response,
        });
      })
      .catch(function (error) {
        console.log(error.response.data);
      });
  } else {
    return dispatch({
      type: types.CREDIT_DATA,
      data: json.data.response,
    });
  }
} 
}

export default {
  getAllPackages,
  getBookedPackage,
  getAdministrativeFees,
  requestPackage,
  getRequestedSummary,
  createPackageRequest,
  getPastPackage,
  getCancelledPackage,
  getSinglePackageOffer,
  rejectOfferPackage,
  getAllOffers,
  getCreditsData
};
