import types from '../../redux/Types';
import API from '../../api/Api';
import Toast from 'react-native-simple-toast';
import SimpleToast from 'react-native-simple-toast';
import { Alert } from 'react-native';

const api = new API();

const getUpcomingTrips = (val) => {
  return async (dispatch, getStore) => {
    var data;
    if (val) {
      data = JSON.stringify({status: val});
    } else {
      data = JSON.stringify({status: 0});
    }
    try {
      api
        .getUpcomingTrips(data)
        .then((json) => {
          console.log('upcoming trip data is:-', json.data.response);
          if (json.status == 200) {
            dispatch({
              type: types.UPCOMING_REQUEST_TRIP,
              data: json.data.response,
            });
          } else {
            Toast.show(json.data.message);
          }
        })
        .catch(function (error) {
          Toast.show(error.response.data.message);
          console.log(error.response.data);
        });
    } catch (error) {
      console.log(error);
    }
  };
};

const getPastTrips = (val) => {
  return async (dispatch, getStore) => {
    data = JSON.stringify({status: 6});
    try {
      api
        .getUpcomingTrips(data)
        .then((json) => {
          console.log('past trip data is:-', json.data.response);
          if (json.status == 200) {
            dispatch({
              type: types.PAST_REQUEST_TRIP,
              data: json.data.response,
            });
          } else {
            Toast.show(json.data.message);
          }
        })
        .catch(function (error) {
          Toast.show(error.response.data.message);
        });
    } catch (error) {
      console.log(error);
    }
  };
};

const getCancelledTrips = (val) => {
  return async (dispatch, getStore) => {
    data = JSON.stringify({status: 5});
    try {
      api
        .getUpcomingTrips(data)
        .then((json) => {
          console.log('cancelled trip data is:-', json.data.response);
          if (json.status == 200) {
            dispatch({
              type: types.CANCELLED_REQUEST_TRIP,
              data: json.data.response,
            });
          } else {
            Toast.show(json.data.message);
          }
        })
        .catch(function (error) {
          Toast.show(error.response.data.message);
        });
    } catch (error) {
      console.log(error);
    }
  };
};

const requestTrip = (data) => {
  return async (dispatch, getStore) => {
    try {
      api
        .requestTrip(data)
        .then((json) => {
          if (json.status == 200) {
            console.log('request trip response:-', json.data.response);
            dispatch({
              type: types.CREATE_REQUEST_TRIP,
              data: json.data.response,
            });
            dispatch(getUpcomingTrips());
          } else {
            Toast.show(json.data.message);
            console.log(json.data.message);
          }
        })
        .catch(function (error) {
          Toast.show(error.response.data.message);
        });
    } catch (error) {
      console.log(error);
    }
  };
};

const rejectOffer = (val, data) => {
  return async (dispatch, stote) => {
    api
      .rejectOffer(val, data)
      .then((json) => {
        SimpleToast.show('Offer Rejected');
        console.log(json.data.message);
        dispatch(getUpcomingTrips());
        dispatch(getCancelledTrips());
      })
      .catch(function (error) {
        console.log(error.response.data.message);
      });
  };
};

const getSingleTripOffer = (val) => {
  return async (dispatch, getStore) => {
    api
      .getSingleTripOffer(val)
      .then((json) => {
        dispatch({
          type: types.SINGLE_TRIP_OFFER,
          data: json.data.response,
        });
      })
      .catch(function (error) {
        console.log(error.response.data.message);
      });
  };
};


const getTripAddons = (type,val)=>{
  return async (dispatch, getStore) => {
  api
    .getTripAddons(type, val)
    .then((json) => {
      dispatch({
        type: types.TRIP_ADDONS,
        data: json.data.response,
      });
    })
    .catch((error) => {
      console.log(error.response.data);
    });
  };
}

export default {
  getUpcomingTrips,
  requestTrip,
  getPastTrips,
  getCancelledTrips,
  rejectOffer,
  getSingleTripOffer,
  getTripAddons
};
