import types from '../../redux/Types';
import API from '../../api/Api';
import Toast from 'react-native-simple-toast';
import Auth from '../../auth/index';
import {Alert} from 'react-native';
import RequestAction from './RequestAction';
import SimpleToast from 'react-native-simple-toast';
import PackageAction from './PackageAction';

const api = new API();
const auth = new Auth();

const getPaymentMethods = (type) => {
  return async (dispatch, getStore) => {
    let paymentData = getStore().paymentReducer.paymentMethod;
    if (paymentData.length == 0 || type == 1) {
      try {
        api
          .getPaymentMethods()
          .then((json) => {
            console.log('all payment method data is:-', json.data.response);
            if (json.status == 200) {
              dispatch({
                type: types.PAYMENT_METHODS,
                data: json.data.response,
              });
            } else {
              Toast.show(json.data.message);
            }
          })
          .catch(function (error) {
            Toast.show(error.response.data.message);
          });
      } catch (error) {
        console.log(error);
      }
    } else {
      return dispatch({
        type: types.PAYMENT_METHODS,
        data: paymentData,
      });
    }
  };
};

const getAddedCards = (type) => {
  return async (dispatch, getStore) => {
    let cardData = getStore().paymentReducer.paymentCards;
    if (cardData.length == 0 || type == 1) {
      try {
        api
          .getPaymentCards()
          .then((json) => {
            console.log('all payment card data is:-', json.data.response);
            if (json.status == 200) {
              dispatch({
                type: types.PAYMENT_CARDS,
                data: json.data.response,
              });
            } else {
              Toast.show(json.data.message);
            }
          })
          .catch(function (error) {
            Toast.show(error.response.data.message);
          });
      } catch (error) {
        console.log(error);
      }
    } else {
      return dispatch({
        type: types.PAYMENT_CARDS,
        data: cardData,
      });
    }
  };
};

const servicePayment = (data) => {
  return async (dispatch, getStore) => {
    try {
      api
        .servicePayment(data)
        .then((json) => {
          dispatch({
            type: types.PACKAGE_REQUEST,
            data: json.data.response,
          });
          dispatch(RequestAction.getUpcomingTrips());
        })
        .catch(function (error) {
          console.log(error.response.data.message);
        });
    } catch (error) {
      console.log(error);
    }
  };
};

const tripP2 = (data) => {
  return async (dispatch, getStore) => {
    api
      .tripP2(data)
      .then((json) => {
        console.log(json.data);
        SimpleToast.show(json.data.message);
        dispatch({
          type: types.TRIP_P2,
          data: new Date(),
        });
        dispatch(RequestAction.getUpcomingTrips());
      })
      .catch(function (error) {
        console.log(error.response.data);
        SimpleToast.show(error.response.data.message);
      });
  };
};

const packageP2 = (data) => {
  return async (dispatch, getStore) => {
    api
      .packageP2(data)
      .then((json) => {
        console.log(json.data);
        SimpleToast.show(json.data.message);
        dispatch({
          type: types.PACKAGE_P2,
          data: new Date(),
        });
        dispatch(PackageAction.getBookedPackage());
      })
      .catch(function (error) {
        console.log(error.response.data);
        SimpleToast.show(error.response.data.message);
      });
  };
};

export default {
  getPaymentMethods,
  getAddedCards,
  servicePayment,
  tripP2,
  packageP2,
};
