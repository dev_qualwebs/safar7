import types from '../../redux/Types';
import API from '../../api/Api';
import Toast from 'react-native-simple-toast';
import RequestAction from './RequestAction';
import PackageAction from './PackageAction';

const api = new API();

const getStayRoomType = (type) => {
  return async (dispatch, getStore) => {
    let stayRoomData = getStore().stayReducer.stayRoomTypeData;
    if (stayRoomData.length == 0 || type == 1) {
      try {
        api
          .getRoomTypes()
          .then((json) => {
            console.log('room type data is:-', json.data.response);
            if (json.status == 200) {
              dispatch({
                type: types.STAY_ROOM_TYPES,
                data: json.data.response,
              });
            } else {
              Toast.show(json.data.message);
            }
          })
          .catch(function (error) {
            Toast.show(error.response.data.message);
          });
      } catch (error) {
        console.log(error);
      }
    } else {
      return dispatch({
        type: types.STAY_ROOM_TYPES,
        data: stayRoomData,
      });
    }
  };
};

const getStayBedPreference = (type) => {
  return async (dispatch, getStore) => {
    let stayBedData = getStore().stayReducer.stayBedPreferenceData;
    if (stayBedData.length == 0 || type == 1) {
      try {
        api
          .getBedPreferences()
          .then((json) => {
            console.log('bed preference data is:-', json.data.response);
            if (json.status == 200) {
              dispatch({
                type: types.STAY_BED_PREFERENCE,
                data: json.data.response,
              });
            } else {
              Toast.show(json.data.message);
            }
          })
          .catch(function (error) {
            Toast.show(error.response.data.message);
          });
      } catch (error) {
        console.log(error);
      }
    } else {
      return dispatch({
        type: types.STAY_BED_PREFERENCE,
        data: stayBedData,
      });
    }
  };
};

const scheduleStay = (data, id) => {
  return async (dispatch, getStore) => {
    try {
      api
        .scheduleStay(data, id)
        .then((json) => {
          console.log('schedule responce:-', json.data);

          if (json.status == 200) {
            let parsedObject = JSON.parse(data);
            if (parsedObject.package_request_id) {
              dispatch(
                PackageAction.getSinglePackageRequest(
                  parsedObject.package_request_id,
                ),
              );
            } else {
              dispatch(PackageAction.getRequestedSummary(id));
              dispatch(RequestAction.getUpcomingTrips());
            }

            dispatch(getScheduledStay(id));
            Toast.show(json.data.message);
          } else if (json.status == 400) {
            setTimeout(() => {
              Toast.show(json.data.message);
            }, 0);
          } else {
            setTimeout(() => {
              Toast.show(json.data.message);
            }, 0);
          }
        })
        .catch((error) => {
          setTimeout(() => {
            console.log('json.data.message', error);
            Toast.show(String(error.response.data.message));
          }, 0);
          console.log('error:-', error.response.data.message);
        });
    } catch (error) {
      console.log(error.message);
    }
  };
};

const updateStay = (data, id) => {
  return async (dispatch, getStore) => {
    try {
      api
        .updateStay(data, id)
        .then((json) => {
          console.log('update responce:-', json.data);
          console.log('Success');
          let parsedObject = JSON.parse(data);
          if (parsedObject.package_request_id) {
            dispatch(
              PackageAction.getSinglePackageRequest(
                parsedObject.package_request_id,
              ),
            );
          } else {
            dispatch(PackageAction.getRequestedSummary(parsedObject.tripId));
            dispatch(RequestAction.getUpcomingTrips());
          }
          dispatch(getScheduledStay(id));
          Toast.show(json.data.message);
        })
        .catch((error) => {
          setTimeout(() => {
            console.log('json.data.message', error);
            Toast.show(String(error.response.data.message));
          }, 0);
          console.log('error:-', error.response.data.message);
        });
    } catch (error) {
      console.log(error.message);
    }
  };
};

const getStayPolicy = () => {
  return async (dispatch, getStore) => {
    let stayPolicy = getStore().stayReducer.stayPolicy;
    if (stayPolicy.length == 0) {
      try {
        api
          .getStayPolicy()
          .then((json) => {
            console.log('stay policy is:-', json.data.response);
            if (json.status == 200) {
              dispatch({
                type: types.STAY_POLICY,
                data: json.data.response,
              });
            } else {
              Toast.show(json.data.message);
            }
          })
          .catch(function (error) {
            Toast.show(error.response.data.message);
          });
      } catch (error) {
        console.log(error);
      }
    } else {
      return dispatch({
        type: types.STAY_POLICY,
        data: stayPolicy,
      });
    }
  };
};

const getStayServices = () => {
  return async (dispatch, getStore) => {
    let stayServices = getStore().stayReducer.stayServices;
    if (stayServices.length == 0) {
      try {
        api
          .getStayServices()
          .then((json) => {
            console.log('stay services is:-', json.data.response);
            if (json.status == 200) {
              dispatch({
                type: types.STAY_SERVICES,
                data: json.data.response,
              });
            } else {
              Toast.show(json.data.message);
            }
          })
          .catch(function (error) {
            Toast.show(error.response.data.message);
          });
      } catch (error) {
        console.log(error);
      }
    } else {
      return dispatch({
        type: types.STAY_SERVICES,
        data: stayServices,
      });
    }
  };
};

const getStayProperty = () => {
  return async (dispatch, getStore) => {
    let stayProperty = getStore().stayReducer.stayProperty;
    if (stayProperty.length == 0) {
      try {
        api
          .getStayProperty()
          .then((json) => {
            console.log('stay property is:-', json.data.response);
            if (json.status == 200) {
              dispatch({
                type: types.STAY_PROPERTY,
                data: json.data.response,
              });
            } else {
              Toast.show(json.data.message);
            }
          })
          .catch(function (error) {
            Toast.show(error.response.data.message);
          });
      } catch (error) {
        console.log(error);
      }
    } else {
      return dispatch({
        type: types.STAY_PROPERTY,
        data: stayProperty,
      });
    }
  };
};

const getScheduledStay = (id) => {
  return async (dispatch, store) => {
    try {
      api
        .getScheduledStay(id)
        .then((json) => {
          console.log(json.data);
          if (json.status == 200) {
            console.log('scheduled stay', json.data.response);
            dispatch({
              type: types.SCHEDULED_STAY,
              data: json.data.response,
            });
          } else if (json.status == 400) {
            setTimeout(() => {
              Toast.show(json.data.message);
            }, 0);
          } else {
            setTimeout(() => {
              Toast.show(json.data.message);
            }, 0);
          }
        })
        .catch((error) => {
          setTimeout(() => {
            console.log('json.data.message', error);
            Toast.show(String(error));
          }, 0);
          console.log('error:-', error);
        });
    } catch (error) {
      console.log(error.message);
    }
  };
};

const deleteScheduledStay = (id, tripId) => {
  return async (dispatch, store) => {
    api
      .deleteScheduleStay(id)
      .then((json) => {
        if (json.status == 200) {
          Toast.show('Deleted Succesfully');
          dispatch(RequestAction.getUpcomingTrips(tripId));
        }
      })
      .catch(function (error) {
        Toast.show(error.response.data.message);
      });
  };
};

const getSingleStay = (id) => {
  return async (dispatch, store) => {
    try {
      api
        .getSingleStay(id)
        .then((json) => {
          console.log(json.data);
          if (json.status == 200) {
            console.log('Single cruise', json.data.response);
            dispatch({
              type: types.SINGLE_STAY_DATA,
              data: json.data.response,
            });
          }
        })
        .catch((error) => {
          console.log('json.data.message', error);
        });
    } catch (error) {
      console.log(error.message);
    }
  };
};

const clearSingleStay = () => {
  return async (dispatch, store) => {
    dispatch({
      type: types.CLEAR_SINGLE_STAY,
    });
  };
};
export default {
  getStayRoomType,
  getStayBedPreference,
  scheduleStay,
  getStayPolicy,
  getStayServices,
  getStayProperty,
  getScheduledStay,
  deleteScheduledStay,
  getSingleStay,
  updateStay,
  clearSingleStay,
};
