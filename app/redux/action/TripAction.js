import types from '../../redux/Types';
import API from '../../api/Api';
import Toast from 'react-native-simple-toast';
import Auth from '../../auth/index';
import {goBack} from '../../components/routes/RootNavigation';

const api = new API();

const getManualTrips = (type) => {
  return async (dispatch, getStore) => {
    let tripData = getStore().tripReducer.upcomingTrips;

    try {
      api
        .getManualTrips({"status":type})
        .then((json) => {
          console.log('manual trip data is:-', json.data.response);
          if (json.status == 200) {
            if(type == 1){
            dispatch({
              type: types.UPCOMING_TRIPS,
              data: json.data.response,
            });
          }else {
            dispatch({
              type: types.PAST_TRIPS,
              data: json.data.response,
            });
          }
          } else {
            Toast.show(json.data.message);
          }
        })
        .catch(function (error) {
          Toast.show(error.response.data.message);
        });
    } catch (error) {
      console.log(error);
    }
  };
};

const requestTrip = (data) => {
  return async (dispatch, getStore) => {
    try {
      api
        .requestTrip(data)
        .then((json) => {
          if (json.status == 200) {
            console.log('request trip response:-', json.data.message);
          } else {
            Toast.show(json.data.message);
            console.log(json.data.message);
          }
        })
        .catch(function (error) {
          Toast.show(error.response.data.message);
        });
    } catch (error) {
      console.log(error);
    }
  };
};

const scheduleManualTrip = (data, val) => {
  return (async = (dispatch, getStore) => {
    try {
      api
        .scheduleManualTrip(data, val)
        .then((json) => {
          if (json.status == 200) {
            Toast.show(json.data.response.message);
            console.log(json.data);
            goBack();
          } else {
            Toast.show(json.data.response.message);
            console.log(json.data);
          }
        })
        .catch((error) => {
          console.log(error);
        });
    } catch (error) {
      console.log(error);
    }
  });
};

export default {
  getManualTrips,
  requestTrip,
  scheduleManualTrip,
};
