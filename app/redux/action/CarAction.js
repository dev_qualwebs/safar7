import API from '../../api/Api';
import types from '../Types';
import Toast from 'react-native-simple-toast';
import RequestAction from './RequestAction';
import SimpleToast from 'react-native-simple-toast';
import PackageAction from './PackageAction';
const api = new API();

const getCarType = () => {
  return async (dispatch, getStore) => {
    let carTypes = getStore().carReducer.carTypes;
    if (carTypes.length == 0) {
      try {
        api
          .getCarType()
          .then((json) => {
            console.log('car types :-', json.data.response);
            if (json.status == 200) {
              dispatch({
                type: types.CAR_TYPE,
                data: json.data.response,
              });
            } else {
              Toast.show(json.data.message);
            }
          })
          .catch(function (error) {
            Toast.show(error.response.data.message);
          });
      } catch (error) {
        console.log(error);
      }
    } else {
      return dispatch({
        type: types.CAR_TYPE,
        data: carTypes,
      });
    }
  };
};

const getCarSpecifications = () => {
  return async (dispatch, getStore) => {
    let carSpecifications = getStore().carReducer.carSpecifications;
    if (carSpecifications.length == 0) {
      try {
        api
          .getCarSpecifications()
          .then((json) => {
            console.log('car specifications :-', json.data.response);
            if (json.status == 200) {
              dispatch({
                type: types.CAR_SPECIFICATION,
                data: json.data.response,
              });
            } else {
              Toast.show(json.data.message);
            }
          })
          .catch(function (error) {
            Toast.show(error.response.data.message);
          });
      } catch (error) {
        console.log(error);
      }
    } else {
      return dispatch({
        type: types.CAR_SPECIFICATION,
        data: carSpecifications,
      });
    }
  };
};

const getCarFinanceResponsibility = () => {
  return async (dispatch, getStore) => {
    let carFinanceResponsibility =
      getStore().carReducer.carFinanceResponsibility;
    if (carFinanceResponsibility.length == 0) {
      try {
        api
          .getCarFinanceResponsibility()
          .then((json) => {
            console.log('car finance :-', json.data.response);
            if (json.status == 200) {
              dispatch({
                type: types.CAR_FINANCE_RESPONSIBILITY,
                data: json.data.response,
              });
            } else {
              Toast.show(json.data.message);
            }
          })
          .catch(function (error) {
            Toast.show(error.response.data.message);
          });
      } catch (error) {
        console.log(error);
      }
    } else {
      return dispatch({
        type: types.CAR_FINANCE_RESPONSIBILITY,
        data: carFinanceResponsibility,
      });
    }
  };
};

const getCarSupplier = () => {
  return async (dispatch, getStore) => {
    let carSupplier = getStore().carReducer.carSupplier;
    if (carSupplier.length == 0) {
      try {
        api
          .getCarSupplier()
          .then((json) => {
            console.log('car carSupplier :-', json.data.response);
            if (json.status == 200) {
              dispatch({
                type: types.CAR_SUPPLIER,
                data: json.data.response,
              });
            } else {
              Toast.show(json.data.message);
            }
          })
          .catch(function (error) {
            Toast.show(error.response.data.message);
          });
      } catch (error) {
        console.log(error);
      }
    } else {
      return dispatch({
        type: types.CAR_SUPPLIER,
        data: carSupplier,
      });
    }
  };
};

const getCarPolicy = () => {
  return async (dispatch, getStore) => {
    let carPolicy = getStore().carReducer.carPolicy;
    if (carPolicy.length == 0) {
      try {
        api
          .getCarPolicy()
          .then((json) => {
            console.log('car Policy :-', json.data.response);
            if (json.status == 200) {
              dispatch({
                type: types.CAR_POLICY,
                data: json.data.response,
              });
            } else {
              Toast.show(json.data.message);
            }
          })
          .catch(function (error) {
            Toast.show(error.response.data.message);
          });
      } catch (error) {
        console.log(error);
      }
    } else {
      return dispatch({
        type: types.CAR_POLICY,
        data: carPolicy,
      });
    }
  };
};

const scheduleCar = (data, id) => {
  return async (dispatch, store) => {
    try {
      api
        .scheduleCar(data, id)
        .then((json) => {
          console.log(json.data);

          Toast.show(json.data.message);
          let parsedObject = JSON.parse(data);
          if (parsedObject.package_request_id) {
            dispatch(
              PackageAction.getSinglePackageRequest(
                parsedObject.package_request_id,
              ),
            );
          } else {
            dispatch(RequestAction.getUpcomingTrips());
            dispatch(PackageAction.getRequestedSummary(id));
            dispatch(getCarSchedules(id));
          }
        })
        .catch((error) => {
          console.log('error:-', error.response.data.message);
          SimpleToast.show(error.response.data.message);
        });
    } catch (error) {
      console.log(error.message);
    }
  };
};

const getCarSchedules = (val) => {
  return (dispatch, getStore) => {
    try {
      api
        .getCarSchedules(val)
        .then((json) => {
          console.log(json.data);
          if (json.status == 200) {
            console.log('scheduled cars', json.data.response);
            dispatch({
              type: types.SCHEDULED_CARS,
              data: json.data.response,
            });
          }
        })
        .catch((error) => {
          console.log('error:-', error);
        });
    } catch (error) {
      console.log(error.message);
    }
  };
};

const getSingleCar = (id) => {
  return async (dispatch, store) => {
    try {
      api
        .getSingleCar(id)
        .then((json) => {
          console.log(json.data);
          if (json.status == 200) {
            dispatch({
              data: json.data.response,
              type: types.SINLGE_CAR_DATA,
            });
          }
        })
        .catch((error) => {
          setTimeout(() => {
            console.log('json.data.message', error);
            console.log(String(error.response.data.message));
          }, 0);
          console.log('error:-', error.response.data.message);
        });
    } catch (error) {
      console.log(error.message);
    }
  };
};

const deleteCar = (id, tripId) => {
  return async (dispatch, store) => {
    api
      .deleteCar(id)
      .then((json) => {
        if (json.status == 200) {
          Toast.show('Deleted Succesfully');
          dispatch(RequestAction.getUpcomingTrips(tripId));
        }
      })
      .catch(function (error) {
        Toast.show(error.response.data.message);
      });
  };
};

const updateCar = (data, id) => {
  return async (dispatch, store) => {
    try {
      api
        .updateCar(data, id)
        .then((json) => {
          console.log(json.data);
          console.log(JSON.parse(data));
          let parsedObject = JSON.parse(data);
          if (parsedObject.package_request_id) {
            dispatch(
              PackageAction.getSinglePackageRequest(
                parsedObject.package_request_id,
              ),
            );
          } else {
            dispatch(PackageAction.getRequestedSummary(parsedObject.tripId));
            dispatch(RequestAction.getUpcomingTrips());
            dispatch(getCarSchedules(parsedObject.tripId));
          }

          Toast.show(json.data.message);
        })
        .catch((error) => {
          Toast.show(error.response.data.message);
          console.log('error:-', error.response.data.message);
        });
    } catch (error) {
      console.log(error.message);
    }
  };
};

const clearSingleCar = () => {
  return (async, dispatch) => {
    dispatch({
      type: types.CLEAR_SINGLE_CAR_DATA,
    });
  };
};
export default {
  getCarType,
  getCarSpecifications,
  getCarFinanceResponsibility,
  getCarSupplier,
  getCarPolicy,
  scheduleCar,
  getSingleCar,
  updateCar,
  clearSingleCar,
  getCarSchedules,
  deleteCar,
};
