import types from '../../redux/Types';
import API from '../../api/Api';
import Toast from 'react-native-simple-toast';
import RequestAction from './RequestAction';
import SimpleToast from 'react-native-simple-toast';
import PackageAction from './PackageAction';
const api = new API();

const getAirlinesData = (type) => {
  return async (dispatch, getStore) => {
    let airlineData = getStore().flightReducer.flightAirlineData;
    if (airlineData.length == 0 || type == 1) {
      try {
        api
          .getAirlineData()
          .then((json) => {
            console.log('airline data is:-', json.data.response);
            if (json.status == 200) {
              dispatch({
                type: types.FLIGHT_AIRLINE,
                data: json.data.response,
              });
            } else {
              Toast.show(json.data.message);
            }
          })
          .catch(function (error) {
            Toast.show(error.response.data.message);
          });
      } catch (error) {
        console.log(error);
      }
    } else {
      return dispatch({
        type: types.FLIGHT_AIRLINE,
        data: airlineData,
      });
    }
  };
};

const getEquipmentData = (type) => {
  return async (dispatch, getStore) => {
    let equipmentData = getStore().flightReducer.flightEquipmentData;
    if (equipmentData.length == 0 || type == 1) {
      try {
        api
          .getEquipmentData()
          .then((json) => {
            console.log('flight equipment data is:-', json.data.response);
            if (json.status == 200) {
              dispatch({
                type: types.FLIGHT_EQUIPMENTS,
                data: json.data.response,
              });
            } else {
              Toast.show(json.data.message);
            }
          })
          .catch(function (error) {
            Toast.show(error.response.data.message);
          });
      } catch (error) {
        console.log(error);
      }
    } else {
      return dispatch({
        type: types.FLIGHT_EQUIPMENTS,
        data: equipmentData,
      });
    }
  };
};

const getFlightStopData = (type) => {
  return async (dispatch, getStore) => {
    let stopData = getStore().flightReducer.flightStopData;
    if (stopData.length == 0 || type == 1) {
      try {
        api
          .getFlightStops()
          .then((json) => {
            console.log('flight stop data is:-', json.data.response);
            if (json.status == 200) {
              dispatch({
                type: types.FLIGHT_STOPS,
                data: json.data.response,
              });
            } else {
              Toast.show(json.data.message);
            }
          })
          .catch(function (error) {
            Toast.show(error.response.data.message);
          });
      } catch (error) {
        console.log(error);
      }
    } else {
      return dispatch({
        type: types.FLIGHT_STOPS,
        data: stop(),
      });
    }
  };
};

const getFlightSeatings = (type) => {
  return async (dispatch, getStore) => {
    let seatingData = getStore().flightReducer.flightSeatingData;
    if (seatingData.length == 0 || type == 1) {
      try {
        api
          .getFlightSeatings()
          .then((json) => {
            console.log('flight seating data is:-', json.data.response);
            if (json.status == 200) {
              dispatch({
                type: types.FLIGHT_SEATINGS,
                data: json.data.response,
              });
            } else {
              Toast.show(json.data.message);
            }
          })
          .catch(function (error) {
            Toast.show(error.response.data.message);
          });
      } catch (error) {
        console.log(error);
      }
    } else {
      return dispatch({
        type: types.FLIGHT_SEATINGS,
        data: seatingData,
      });
    }
  };
};

const getFlightTimes = (type) => {
  return async (dispatch, getStore) => {
    let flightTimeData = getStore().flightReducer.flightTimeData;
    if (flightTimeData.length == 0 || type == 1) {
      try {
        api
          .getFlightTimes()
          .then((json) => {
            console.log('airline data is:-', json);
            if (json.status == 200) {
              dispatch({
                type: types.FLIGHT_TIMES,
                data: json.data.response,
              });
            } else {
              Toast.show(json.data.message);
            }
          })
          .catch(function (error) {
            console.log(error);
            Toast.show(error.response.data.message);
          });
      } catch (error) {
        console.log(error);
      }
    } else {
      return dispatch({
        type: types.FLIGHT_TIMES,
        data: flightTimeData,
      });
    }
  };
};

const getFlightPolicies = (type) => {
  return async (dispatch, getStore) => {
    let policyData = getStore().flightReducer.flightPolicyData;
    if (policyData.length == 0 || type == 1) {
      try {
        api
          .getFlightPolicies()
          .then((json) => {
            console.log('airline data is:-', json.data.response);
            if (json.status == 200) {
              dispatch({
                type: types.FLIGHT_POLICIES,
                data: json.data.response,
              });
            } else {
              Toast.show(json.data.message);
            }
          })
          .catch(function (error) {
            Toast.show(error.response.data.message);
          });
      } catch (error) {
        console.log(error);
      }
    } else {
      return dispatch({
        type: types.FLIGHT_POLICIES,
        data: policyData,
      });
    }
  };
};

const getFlightClasses = (type) => {
  return async (dispatch, getStore) => {
    let flightClassData = getStore().flightReducer.flightClassData;
    if (flightClassData.length == 0 || type == 1) {
      try {
        api
          .getFlightClasses()
          .then((json) => {
            console.log('airline data is:-', json.data.response);
            if (json.status == 200) {
              dispatch({
                type: types.FLIGHT_CLASSES,
                data: json.data.response,
              });
            } else {
              Toast.show(json.data.message);
            }
          })
          .catch(function (error) {
            Toast.show(error.response.data.message);
          });
      } catch (error) {
        console.log(error);
      }
    } else {
      return dispatch({
        type: types.FLIGHT_CLASSES,
        data: flightClassData,
      });
    }
  };
};

const getFlightModes = (type) => {
  return async (dispatch, getStore) => {
    let flightModeData = getStore().flightReducer.flightModeData;
    if (flightModeData.length == 0 || type == 1) {
      try {
        api
          .getEquipmentData()
          .then((json) => {
            console.log('airline data is:-', json.data.response);
            if (json.status == 200) {
              dispatch({
                type: types.FLIGHT_MODES,
                data: json.data.response,
              });
            } else {
              Toast.show(json.data.message);
            }
          })
          .catch(function (error) {
            Toast.show(error.response.data.message);
          });
      } catch (error) {
        console.log(error);
      }
    } else {
      return dispatch({
        type: types.FLIGHT_MODES,
        data: flightModeData,
      });
    }
  };
};

const scheduleFlight = (data, id) => {
  return async (dispatch, store) => {
    try {
      api
        .scheduleFlight(data, id)
        .then((json) => {
          console.log(json.data);
          if (json.status == 200) {
            let parsedObject = JSON.parse(data);
            if (parsedObject.package_request_id) {
              dispatch(
                PackageAction.getSinglePackageRequest(
                  parsedObject.package_request_id,
                ),
              );
            } else {
              dispatch(PackageAction.getRequestedSummary(id));
              dispatch(RequestAction.getUpcomingTrips());
            }
            dispatch(getScheduledFlights(id));

            console.log('Succes');
            Toast.show(json.data.message);
          } else if (json.status == 400) {
            setTimeout(() => {
              Toast.show(json.data.message);
            }, 0);
          } else {
            setTimeout(() => {
              Toast.show(json.data.message);
            }, 0);
          }
        })
        .catch((error) => {
          setTimeout(() => {
            console.log('json.data.message', error);
            Toast.show(String(error.response.data.message));
          }, 0);
          console.log('error:-', error.response.data.message);
        });
    } catch (error) {
      console.log(error.message);
    }
  };
};

const getSingleFlight = (id) => {
  return async (dispatch, store) => {
    try {
      api
        .getSingleFlight(id)
        .then((json) => {
          console.log(json.data);
          if (json.status == 200) {
            console.log('Single flight', json.data.response);
            dispatch({
              type: types.SINGLE_FLIGHT_DATA,
              data: json.data.response,
            });
          } else if (json.status == 400) {
            setTimeout(() => {
              Toast.show(json.data.message);
            }, 0);
          } else {
            setTimeout(() => {
              Toast.show(json.data.message);
            }, 0);
          }
        })
        .catch((error) => {
          setTimeout(() => {
            console.log('json.data.message', error);
            Toast.show(String(error));
          }, 0);
          console.log('error:-', error);
        });
    } catch (error) {
      console.log(error.message);
    }
  };
};

const getScheduledFlights = (id) => {
  return async (dispatch, store) => {
    try {
      api
        .getScheduledFlight(id)
        .then((json) => {
          console.log(json.data);
          if (json.status == 200) {
            console.log('scheduled flights', json.data.response);

            dispatch({
              type: types.SCHEDULED_FLIGHTS,
              data: json.data.response,
            });
          } else if (json.status == 400) {
            setTimeout(() => {
              Toast.show(json.data.message);
            }, 0);
          } else {
            setTimeout(() => {
              Toast.show(json.data.message);
            }, 0);
          }
        })
        .catch((error) => {
          setTimeout(() => {
            console.log('json.data.message', error);
            Toast.show(String(error));
          }, 0);
          console.log('error:-', error);
        });
    } catch (error) {
      console.log(error.message);
    }
  };
};

const updateScheduledFlight = (data, id) => {
  return async (dispatch, store) => {
    try {
      api
        .updateSheduledFlight(data, id)
        .then((json) => {
          console.log(json.data);
          if (json.status == 200) {
            SimpleToast.show('Updated');
            dispatch({
              type: types.SCHEDULED_FLIGHTS,
              data: json.data.response,
            });
            let parsedObject = JSON.parse(data);
            if (parsedObject.package_request_id) {
              dispatch(
                PackageAction.getSinglePackageRequest(
                  parsedObject.package_request_id,
                ),
              );
            } else {
              dispatch(PackageAction.getRequestedSummary(parsedObject.tripId));
              dispatch(RequestAction.getUpcomingTrips());
            }
          }
        })
        .catch((error) => {
          setTimeout(() => {
            console.log('json.data.message', error);
            Toast.show(String(error));
          }, 0);
          console.log('error:-', error.response.data.message);
        });
    } catch (error) {
      console.log(error.message);
    }
  };
};

const clearSingleFlight = () => {
  return (async, dispatch) => {
    dispatch({
      type: types.CLEAR_SINGLE_FLIGHT,
    });
  };
};

export default {
  getAirlinesData,
  getEquipmentData,
  getFlightModes,
  getFlightStopData,
  getFlightSeatings,
  getFlightTimes,
  getFlightPolicies,
  getFlightClasses,
  scheduleFlight,
  getSingleFlight,
  getScheduledFlights,
  updateScheduledFlight,
  clearSingleFlight,
};
