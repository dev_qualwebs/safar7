import API from '../../api/Api';
import types from '../Types';
const api = new API();

const getAttractionShow = () => {
  return async (dispatch, getStore) => {
    let data = getStore().tripPlanReducer.attractionShow;
    if (data.length == 0 || data == null) {
      api
        .getAttractionShow()
        .then((json) => {
          console.log(json.data.response);
          dispatch({
            type: types.ATTRACTION_SHOW,
            data: json.data.response,
          });
        })
        .catch(function (error) {
          console.log(error);
        });
    } else {
      console.log(data);
      dispatch({
        type: types.ATTRACTION_SHOW,
        data: data,
      });
    }
  };
};

const getCuisines = () => {
  return async (dispatch, getStore) => {
    let data = getStore().tripPlanReducer.cuisineTypes;
    if (data.length == 0 || data == null) {
      api
        .getCuisines()
        .then((json) => {
          console.log(json.data.response);
          dispatch({
            type: types.CUISINE_TYPES,
            data: json.data.response,
          });
        })
        .catch(function (error) {
          console.log(error);
        });
    } else {
      console.log(data);
      dispatch({
        type: types.CUISINE_TYPES,
        data: data,
      });
    }
  };
};

const getActivityExperiences = () => {
  return async (dispatch, getStore) => {
    let data = getStore().tripPlanReducer.activityExperiences;
    if (data.length == 0 || data == null) {
      api
        .getActivityExperiences()
        .then((json) => {
          console.log(json.data.response);
          dispatch({
            type: types.ACTIVITY_EXPERIENCES,
            data: json.data.response,
          });
        })
        .catch(function (error) {
          console.log(error);
        });
    } else {
      console.log(data);
      dispatch({
        type: types.ACTIVITY_EXPERIENCES,
        data: data,
      });
    }
  };
};

const getAirActivities = () => {
  return async (dispatch, getStore) => {
    let data = getStore().tripPlanReducer.airActivities;
    if (data.length == 0 || data == null) {
      api
        .getAirActivities()
        .then((json) => {
          console.log(json.data.response);
          dispatch({
            type: types.AIR_ACTIVITIES,
            data: json.data.response,
          });
        })
        .catch(function (error) {
          console.log(error);
        });
    } else {
      dispatch({
        type: types.AIR_ACTIVITIES,
        data: data,
      });
    }
  };
};

const getSeaActivities = () => {
  return async (dispatch, getStore) => {
    let data = getStore().tripPlanReducer.seaActivities;
    if (data.length == 0 || data == null) {
      api
        .getSeaActivities()
        .then((json) => {
          console.log(json.data.response);
          dispatch({
            type: types.SEA_ACTIVITIES,
            data: json.data.response,
          });
        })
        .catch(function (error) {
          console.log(error);
        });
    } else {
      dispatch({
        type: types.SEA_ACTIVITIES,
        data: data,
      });
    }
  };
};

const getWorkshops = () => {
  return async (dispatch, getStore) => {
    let data = getStore().tripPlanReducer.workshops;
    if (data.length == 0 || data == null) {
      api
        .getWorkshops()
        .then((json) => {
          console.log(json.data.response);
          dispatch({
            type: types.WORKSHOPS,
            data: json.data.response,
          });
        })
        .catch(function (error) {
          console.log(error);
        });
    } else {
      dispatch({
        type: types.WORKSHOPS,
        data: data,
      });
    }
  };
};

const getFoodExperiences = () => {
  return async (dispatch, getStore) => {
    let data = getStore().tripPlanReducer.foodExperience;
    if (data.length == 0 || data == null) {
      api
        .getFoodExperiences()
        .then((json) => {
          console.log(json.data.response);
          dispatch({
            type: types.FOOD_EXPERIENCES,
            data: json.data.response,
          });
        })
        .catch(function (error) {
          console.log(error);
        });
    } else {
      console.log(data);
      dispatch({
        type: types.FOOD_EXPERIENCES,
        data: data,
      });
    }
  };
};

const getSports = () => {
  return async (dispatch, getStore) => {
    let data = getStore().tripPlanReducer.sports;
    if (data.length == 0 || data == null) {
      api
        .getSports()
        .then((json) => {
          console.log(json.data.response);
          dispatch({
            type: types.SPORTS,
            data: json.data.response,
          });
        })
        .catch(function (error) {
          console.log(error);
        });
    } else {
      dispatch({
        type: types.SPORTS,
        data: data,
      });
    }
  };
};

const getOutdoorActivities = () => {
  return async (dispatch, getStore) => {
    let data = getStore().tripPlanReducer.outdoorExperience;
    if (data.length == 0 || data == null) {
      api
        .getOutdoorActivities()
        .then((json) => {
          console.log(json.data.response);
          dispatch({
            type: types.OUTDOOR_EXPERIENCES,
            data: json.data.response,
          });
        })
        .catch(function (error) {
          console.log(error);
        });
    } else {
      dispatch({
        type: types.OUTDOOR_EXPERIENCES,
        data: data,
      });
    }
  };
};

const getIndoorActivities = () => {
  return async (dispatch, getStore) => {
    let data = getStore().tripPlanReducer.indoorExperience;
    if (data.length == 0 || data == null) {
      api
        .getIndoorActivities()
        .then((json) => {
          console.log(json.data.response);
          dispatch({
            type: types.INDOOR_ACTIVITIES,
            data: json.data.response,
          });
        })
        .catch(function (error) {
          console.log(error);
        });
    } else {
      dispatch({
        type: types.INDOOR_ACTIVITIES,
        data: data,
      });
    }
  };
};

export default {
  getAttractionShow,
  getCuisines,
  getActivityExperiences,
  getAirActivities,
  getSeaActivities,
  getWorkshops,
  getFoodExperiences,
  getSports,
  getOutdoorActivities,
  getIndoorActivities,
};
