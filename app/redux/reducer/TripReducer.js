import { Alert } from 'react-native';
import types from '../Types';

let newState = {
    upcomingTrips: [],
    pastTrips:[],
};

let cloneObject = function (obj) {
    return JSON.parse(JSON.stringify(obj));
};

export default function reducer(state, action) {
    switch (action.type) {
        case types.UPCOMING_TRIPS:
            newState = cloneObject(state);
            if (action.data) {
                newState = Object.assign({}, newState, {
                    upcomingTrips: action.data,
                });
            }
            return newState;
            case types.PAST_TRIPS:
                newState = cloneObject(state);
                if (action.data) {
                    newState = Object.assign({}, newState, {
                        pastTrips: action.data,
                    });
                }
                return newState;
        default:
            return state || newState;
    }
}
