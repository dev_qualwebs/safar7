import types from '../Types';

let newState = {
  carTypes: [],
  carSpecifications: [],
  carFinanceResponsibility: [],
  carSupplier: [],
  carPolicy: [],
  singleCarData: null,
  scheduledCars: [],
};

let cloneObject = function (obj) {
  return JSON.parse(JSON.stringify(obj));
};

export default function reducer(state, action) {
  switch (action.type) {
    case types.CAR_TYPE:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {
          carTypes: action.data,
        });
      }
      return newState;
    case types.CAR_SPECIFICATION:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {
          carSpecifications: action.data,
        });
      }
      return newState;
    case types.CAR_FINANCE_RESPONSIBILITY:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {
          carFinanceResponsibility: action.data,
        });
      }
      return newState;
    case types.CAR_SUPPLIER:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {
          carSupplier: action.data,
        });
      }
      return newState;
    case types.CAR_POLICY:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {
          carPolicy: action.data,
        });
      }
      return newState;
    case types.SINLGE_CAR_DATA:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {
          singleCarData: action.data,
        });
      }
      return newState;
    case types.CLEAR_SINGLE_CAR_DATA:
      newState = cloneObject(state);
      newState = Object.assign({}, newState, {
        singleCarData: null,
      });
      return newState;
    case types.SCHEDULED_CARS:
      newState = cloneObject(state);
      newState = Object.assign({}, newState, {
        scheduledCars: action.data,
      });
      return newState;
    default:
      return state || newState;
  }
}
