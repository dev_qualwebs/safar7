import types from '../Types';

let newState = {
  attractionShow: [],
  cuisineTypes: [],
  activityExperiences: [],
  airActivities: [],
  seaActivities: [],
  workshops: [],
  foodExperience: [],
  sports: [],
  outdoorExperience: [],
  indoorExperience: [],
};

let cloneObject = function (obj) {
  return JSON.parse(JSON.stringify(obj));
};

export default function Reducer(state, action) {
  switch (action.type) {
    case types.ATTRACTION_SHOW:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {
          attractionShow: action.data,
        });
      }
      return newState;
    case types.CUISINE_TYPES:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {
          cuisineTypes: action.data,
        });
      }
      return newState;
    case types.ACTIVITY_EXPERIENCES:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {
          activityExperiences: action.data,
        });
      }
      return newState;
    case types.WORKSHOPS:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {
          workshops: action.data,
        });
      }
      return newState;
    case types.AIR_ACTIVITIES:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {
          airActivities: action.data,
        });
      }
      return newState;
    case types.SEA_ACTIVITIES:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {
          seaActivities: action.data,
        });
      }
      return newState;
    case types.FOOD_EXPERIENCES:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {
          foodExperience: action.data,
        });
      }
      return newState;
    case types.SPORTS:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {
          sports: action.data,
        });
      }
      return newState;
    case types.INDOOR_ACTIVITIES:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {
          indoorExperience: action.data,
        });
      }
      return newState;
    case types.OUTDOOR_EXPERIENCES:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {
          outdoorExperience: action.data,
        });
      }
      return newState;
    default:
      return state || newState;
  }
}
