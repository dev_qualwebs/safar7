import {Alert} from 'react-native';
import types from '../Types';

let newState = {
  userDetail: null,
  
};

let cloneObject = function (obj) {
  return JSON.parse(JSON.stringify(obj));
};

export default function reducer(state, action) {
  switch (action.type) {
    case types.USER_DETAIL:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {
          userDetail: action.data,
        });
      }
      return newState;
  
    default:
      return state || newState;
  }
}
