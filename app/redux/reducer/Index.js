import {combineReducers} from 'redux';
import AuthenticationReducer from './AuthenticationReducer';
import PackageReducer from './PackagesReducer';
import PaymentReducer from './PaymentReducer';
import ProfileReducer from './ProfileReducer';
import TripReducer from './TripReducer';
import FlightReducer from './FlightReducer';
import StayReducer from './StayReducer';
import CruiseReducer from './CruiseReducer';
import TrainReducer from './TrainReducer';
import CarReducer from './CarReducer';
import RequestReducer from './RequestReducer';
import TripPlanReducer from './TripPlanReducer';
import types from '../../redux/Types';

const appReducer = combineReducers({
  reducer: AuthenticationReducer,
  pacakgeReducer: PackageReducer,
  paymentReducer: PaymentReducer,
  profileReducer: ProfileReducer,
  tripReducer: TripReducer,
  flightReducer: FlightReducer,
  stayReducer: StayReducer,
  cruiseReducer: CruiseReducer,
  trainReducer: TrainReducer,
  carReducer: CarReducer,
  requestReducer: RequestReducer,
  tripPlanReducer: TripPlanReducer,
});

const rootReducer = (state, action) => {
  if (action.type === types.USER_LOGOUT) {
    state = undefined;
  }
  return appReducer(state, action);
};

export default rootReducer;
