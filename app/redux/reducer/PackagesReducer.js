import {Alert} from 'react-native';
import types from '../Types';

let newState = {
  allPackageData: [],
  allOffersData: [],
  bookedPackages: [],
  cancelledPackages: [],
  pastPackages: [],
  packageSummary: [],
  administrativeFees: null,
  requestPackage: null,
  createPackageData: null,
  singlePackageRequest: null,
  singlePackageOffer: null,
  creditData:null,
};

let cloneObject = function (obj) {
  return JSON.parse(JSON.stringify(obj));
};

export default function reducer(state, action) {
  switch (action.type) {
    case types.ALL_PACAKGES:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {
          allPackageData: action.data,
        });
      }
      return newState;

    case types.ALL_OFFERS:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {
          allOffersData: action.data,
        });
      }
      return newState;

    case types.BOOKED_PACKAGE:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {
          bookedPackages: action.data,
        });
      }
      return newState;
    case types.PAST_BOOKED_PACKAGE:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {
          pastPackages: action.data,
        });
      }
      return newState;
    case types.CANCELLED_BOOKED_PACKAGE:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {
          cancelledPackages: action.data,
        });
      }
      return newState;
    case types.PACKAGE_SUMMARY:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {
          packageSummary: action.data,
        });
      }
      return newState;
    case types.ADMINISTRATIVE_FEES:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {
          administrativeFees: action.data,
        });
      }
      return newState;
    case types.PACKAGE_REQUEST:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {
          requestPackage: action.data,
        });
      }
      return newState;
    case types.CREATE_PACKAGE_DATA:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {
          createPackageData: action.data,
        });
      }
      return newState;
    case types.SINGLE_PACKAGE_REQUEST:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {
          singlePackageRequest: action.data,
        });
      }
      return newState;
    case types.SINGLE_PACKAGE_OFFER:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {
          singlePackageOffer: action.data,
        });
      }
      return newState;
      case types.CREDIT_DATA:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {
          creditData: action.data,
        });
      }
      return newState;
    default:
      return state || newState;
  }
}
