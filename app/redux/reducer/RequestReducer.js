import {Alert} from 'react-native';
import types from '../Types';

let newState = {
  upcomingTrips: [],
  pastTrips: [],
  cancelledTrips: [],
  requestTrip: null,
  singleTripOffer: null,
  tripAddons : null,
};

let cloneObject = function (obj) {
  return JSON.parse(JSON.stringify(obj));
};

export default function reducer(state, action) {
  switch (action.type) {
    case types.UPCOMING_REQUEST_TRIP:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {
          upcomingTrips: action.data,
        });
      }
      return newState;
    case types.CREATE_REQUEST_TRIP:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {
          requestTrip: action.data,
        });
      }
      return newState;
    case types.PAST_REQUEST_TRIP:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {
          pastTrips: action.data,
        });
      }
      return newState;
    case types.CANCELLED_REQUEST_TRIP:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {
          cancelledTrips: action.data,
        });
      }
      return newState;
    case types.SINGLE_TRIP_OFFER:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {
          singleTripOffer: action.data,
        });
      }
      return newState;

      case types.TRIP_ADDONS:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {
          tripAddons: action.data,
        });
      }
      return newState;

    default:
      return state || newState;
  }
}
