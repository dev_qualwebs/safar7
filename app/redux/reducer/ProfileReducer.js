import {Alert} from 'react-native';
import types from '../Types';

let newState = {
  userDetail: null,
  travellersData: null,
  languages: null,
  currencies: null,
  units: null,
  temperature: null,
  navigationApps: null,
  timeFormat: null,
  updateDetails: null,
  countries: null,
  setting: null,
  specialNeeds: null,
  singleTravellerData: null,
};

let cloneObject = function (obj) {
  return JSON.parse(JSON.stringify(obj));
};

export default function reducer(state, action) {
  switch (action.type) {
    case types.USER_DETAIL:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {
          userDetail: action.data,
        });
      }
      return newState;
    case types.TRAVELLERS_TYPE:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {
          travellersData: action.data,
        });
      }
      return newState;
    case types.UNITS:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {
          units: action.data,
        });
      }
      return newState;

    case types.LANGUAGES:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {
          languages: action.data,
        });
      }
      return newState;

    case types.CURRENCIES:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {currencies: action.data});
      }
      return newState;

    case types.TEMPERATURE:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {temperature: action.data});
      }
      return newState;

    case types.NAVIGATION_APPS:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {navigationApps: action.data});
      }
      return newState;

    case types.TIME_FORMAT:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {timeFormat: action.data});
      }
      return newState;
    case types.UPDATE_PROFILE:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {updateDetails: action.data});
      }
      return newState;
    case types.COUNTRIES:
      newState = cloneObject(newState);
      if (action.data) {
        newState = Object.assign({}, newState, {
          countries: action.data,
        });
      }
      return newState;
    case types.SETTINGS:
      newState = cloneObject(newState);
      if (action.data) {
        newState = Object.assign({}, newState, {
          setting: action.data,
        });
      }
      return newState;
    case types.SPECIAL_NEEDS:
      newState = cloneObject(newState);
      if (action.data) {
        newState = Object.assign({}, newState, {
          specialNeeds: action.data,
        });
      }
      return newState;
    case types.SINGLE_TRAVELLER_DATA:
      newState = cloneObject(newState);
      if (action.data) {
        newState = Object.assign({}, newState, {
          singleTravellerData: action.data,
        });
      }
      return newState;
    case 'CLEAR_PROFILE':
      return {
        userDetail: null,
        travellersData: null,
        languages: null,
        currencies: null,
        units: null,
        temperature: null,
        navigationApps: null,
        timeFormat: null,
        updateDetails: null,
        countries: null,
        setting: null,
        specialNeeds: null,
        singleTravellerData: null,
      };
    default:
      return state || newState;
  }
}
