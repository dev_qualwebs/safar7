import {Alert} from 'react-native';
import types from '../Types';

let newState = {
  paymentMethod: [],
  paymentCards: [],
  tripP2: null,
  packageP2: null,
};

let cloneObject = function (obj) {
  return JSON.parse(JSON.stringify(obj));
};

export default function reducer(state, action) {
  switch (action.type) {
    case types.PAYMENT_METHODS:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {
          paymentMethod: action.data,
        });
      }
      return newState;
    case types.PAYMENT_CARDS:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {
          paymentCards: action.data,
        });
      }
      return newState;
    case types.TRIP_P2:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {
          tripP2: action.data,
        });
      }
      return newState;
    case types.PACKAGE_P2:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {
          packageP2: action.data,
        });
      }
      return newState;
    default:
      return state || newState;
  }
}
