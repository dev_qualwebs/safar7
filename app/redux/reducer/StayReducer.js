import types from '../Types';

let newState = {
  stayRoomTypeData: [],
  stayBedPreferenceData: [],
  stayProperty: [],
  stayPolicy: [],
  stayServices: [],
  scheduledStay: [],
  singleStay: null,
};

let cloneObject = function (obj) {
  return JSON.parse(JSON.stringify(obj));
};

export default function reducer(state, action) {
  switch (action.type) {
    case types.STAY_ROOM_TYPES:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {
          stayRoomTypeData: action.data,
        });
      }
      return newState;
    case types.STAY_BED_PREFERENCE:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {
          stayBedPreferenceData: action.data,
        });
      }
      return newState;
    case types.STAY_POLICY:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {
          stayPolicy: action.data,
        });
      }
      return newState;
    case types.STAY_PROPERTY:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {
          stayProperty: action.data,
        });
      }
      return newState;
    case types.STAY_SERVICES:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {
          stayServices: action.data,
        });
      }
      return newState;
    case types.SCHEDULED_STAY:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {
          scheduledStay: action.data,
        });
      }
      return newState;
    case types.SINGLE_STAY_DATA:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {
          singleStay: action.data,
        });
      }
      return newState;
    case types.CLEAR_SINGLE_STAY:
      newState = cloneObject(newState);
      newState = Object.assign({}, newState, {
        singleStay: null,
      });
      return newState;
    default:
      return state || newState;
  }
}
