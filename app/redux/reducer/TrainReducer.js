import types from '../Types';

let newState = {
  trainClasses: [],
  trainPolicies: [],
  scheduledTrains: [],
  singleTrainData: null,
};

let cloneObject = function (obj) {
  return JSON.parse(JSON.stringify(obj));
};

export default function reducer(state, action) {
  switch (action.type) {
    case types.TRAIN_CLASSES:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {
          trainClasses: action.data,
        });
      }
      return newState;
    case types.TRAIN_POLICIES:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {
          trainPolicies: action.data,
        });
      }
      return newState;
    case types.SCHEDULED_TRAINS:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {
          scheduledTrains: action.data,
        });
      }
      return newState;
    case types.SINGLE_TRAIN_DATA:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {
          singleTrainData: action.data,
        });
      }
      return newState;
    case types.CLEAR_SINGLE_TRAIN:
      newState = cloneObject(newState);
      newState = Object.assign({}, newState, {
        singleTrainData: null,
      });
      return newState;
    default:
      return state || newState;
  }
}
