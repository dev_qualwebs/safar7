import types from '../Types';

let newState = {
  cruiseCabinTypeData: [],
  cruiseSupplierData: [],
  creuiseFeatureData: [],
  scheduledCruise: null,
  singleCruise: null,
};

let cloneObject = function (obj) {
  return JSON.parse(JSON.stringify(obj));
};

export default function reducer(state, action) {
  switch (action.type) {
    case types.CRUISE_CABIN_TYPE:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {
          cruiseCabinTypeData: action.data,
        });
      }
      return newState;
    case types.CRUISE_SUPPLIER:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {
          cruiseSupplierData: action.data,
        });
      }
      return newState;
    case types.CRUISE_FEATURE:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {
          creuiseFeatureData: action.data,
        });
      }
      return newState;
    case types.SCHEDULED_CRUISE:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {
          scheduledCruise: action.data,
        });
      }
      return newState;
    case types.SINGLE_CRUISE_DATA:
      newState = cloneObject(newState);
      if (action.data) {
        newState = Object.assign({}, newState, {
          singleCruise: action.data,
        });
      }
      return newState;
    case types.CLEAR_SINGLE_CRUISE:
      newState = cloneObject(newState);
      newState = Object.assign({}, newState, {
        singleCruise: null,
      });
      return newState;
    default:
      return state || newState;
  }
}
