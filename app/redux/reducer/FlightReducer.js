import {request} from 'react-native-permissions';
import types from '../Types';

let newState = {
  flightAirlineData: [],
  flightEquipmentData: [],
  flightStopData: [],
  flightSeatingData: [],
  flightTimeData: [],
  flightPolicyData: [],
  flightClassData: [],
  flightModeData: [],
  singleFlightData: null,
  scheduledFlights: null,
};

let cloneObject = function (obj) {
  return JSON.parse(JSON.stringify(obj));
};

export default function reducer(state, action) {
  switch (action.type) {
    case types.FLIGHT_AIRLINE:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {
          flightAirlineData: action.data,
        });
      }
      return newState;
    case types.FLIGHT_EQUIPMENTS:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {
          flightEquipmentData: action.data,
        });
      }
      return newState;
    case types.FLIGHT_STOPS:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {
          flightStopData: action.data,
        });
      }
      return newState;
    case types.FLIGHT_SEATINGS:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {
          flightSeatingData: action.data,
        });
      }
      return newState;
    case types.FLIGHT_TIMES:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {
          flightTimeData: action.data,
        });
      }
      return newState;
    case types.FLIGHT_POLICIES:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {
          flightPolicyData: action.data,
        });
      }
      return newState;
    case types.FLIGHT_CLASSES:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {
          flightClassData: action.data,
        });
      }
      return newState;
    case types.FLIGHT_MODES:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {
          flightModeData: action.data,
        });
      }
      return newState;
    case types.SINGLE_FLIGHT_DATA:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {
          singleFlightData: action.data,
        });
      }
      return newState;
    case types.SCHEDULED_FLIGHTS:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {
          scheduledFlights: action.data,
        });
      }
      return newState;
    case types.CLEAR_SINGLE_FLIGHT:
      newState = cloneObject(state);
      newState = Object.assign({}, newState, {
        singleFlightData: null,
      });
      return newState;
    default:
      return state || newState;
  }
}
