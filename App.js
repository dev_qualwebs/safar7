/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {LogBox, Text} from 'react-native';
import RootStack from './app//components/routes/Routes';

export default class App extends React.Component {
  constructor()   {
    super();
    Text.defaultProps = Text.defaultProps || {};
    Text.defaultProps.allowFontScaling = false;
  }

  render() {
    LogBox.ignoreAllLogs();
    return <RootStack />;
  }
}
