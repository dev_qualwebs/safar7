module.exports = {
	project: {
	  ios: {},
	  android: {}, // grouped into "project"
	},
	dependencies: {
	  '<dependency>': {
		platforms: {
		  android: null, // disable Android platform, other platforms will still autolink
		},
	  },
	},
	assets: ['../Safar7/app/components/styles/fonts/'], // stays the same
  };
  